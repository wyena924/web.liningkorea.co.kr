<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<div class="container main">
	<div class="top_wrap">
		<div class="bn_wrap list_wrap">
			<div class="bn_cont bn01">
				<a href="#none">
					<img src="/front/img/banner/visual_bn_w01.jpg" alt=""/>
				</a>
			</div>
			<div class="bn_cont bn02">
				<a href="/product/samsung_list.asp">
					<img src="/front/img/banner/visual_bn_w02.jpg" alt=""/>
				</a>
			</div>
			<div class="bn_cont bn03">
				<a href="pmaa/product/md_list.asp">
					<img src="/front/img/banner/visual_bn_w03.jpg" alt=""/>
				</a>
			</div>
		</div><!-- bn_wrap e -->
	</div><!-- top_wrap e -->
	<div class="bottom_wrap">
		<div class="list_wrap">
			<div class="list_box">
				<ul>
					<%
						strqry4 = "		SELECT TOP 4 SEQ "
						strqry4 = strqry4 & "			,GD_TP "
						strqry4 = strqry4 & "			,GD_NM "
						strqry4 = strqry4 & "			,GD_GRP_BRAND "
						strqry4 = strqry4 & "			,GD_GRP_BRAND_NM "
						strqry4 = strqry4 & "			,END_PRICE "
						strqry4 = strqry4 & "			,round(PRICE + 40 ,-2) GD_PRICE3 "
						strqry4 = strqry4 & "			,(round(PRICE + 40 ,-2) * GD_MIN_VOL) TOT_PRICE "
						strqry4 = strqry4 & "			,IMG_PATH GD_IMG_PATH "
						strqry4 = strqry4 & "			,GD_UNI_GRP_CD "
						strqry4 = strqry4 & "			,GD_MIN_VOL "
						strqry4 = strqry4 & "			,GD_GRP_FIRST "
						strqry4 = strqry4 & "			,GD_GRP_MID "
						strqry4 = strqry4 & "			,GD_GRP_DETL "
						strqry4 = strqry4 & "			,GD_OPEN_YN "
						strqry4 = strqry4 & "	FROM (SELECT MAX(AA.SEQ) SEQ "
						strqry4 = strqry4 & "		        ,AA.GD_TP GD_TP "
						strqry4 = strqry4 & "		        ,MAX(AA.GD_NM) GD_NM "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_BRAND GD_GRP_BRAND "
						strqry4 = strqry4 & "		        ,ITEMCENTER.dbo.IC_FN_BRAND_NM(AA.GD_GRP_BRAND) GD_GRP_BRAND_NM "
						strqry4 = strqry4 & "		        ,ISNULL(MIN(AA.GD_PRICE8),0) END_PRICE "
						strqry4 = strqry4 & "		        ,MIN(ISNULL(CASE WHEN ISNULL(BB.GD_TAX_YN,'N') = 'N' THEN ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) " 
						strqry4 = strqry4 & "		        	ELSE (ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) * 1.1)END,0)) PRICE "
						strqry4 = strqry4 & "		        ,CASE WHEN RTRIM(LTRIM(MAX(AA.GD_IMG_PATH))) = '' OR MAX(AA.GD_IMG_PATH) IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
						strqry4 = strqry4 & "		        	ELSE MAX(AA.GD_IMG_PATH) END IMG_PATH "
						strqry4 = strqry4 & "		        ,AA.UNI_GRP_CD GD_UNI_GRP_CD "
						strqry4 = strqry4 & "		        ,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "		        ,MAX(BB.GD_NUM) GD_NUM "
						strqry4 = strqry4 & "		        ,AA.GD_OPEN_YN GD_OPEN_YN "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_FIRST GD_GRP_FIRST "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_MID GD_GRP_MID "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_DETL GD_GRP_DETL "
						strqry4 = strqry4 & "	FROM ITEMCENTER.dbo.IC_T_GDS_INFO AA "
						strqry4 = strqry4 & "	 ,ITEMCENTER.dbo.IC_T_ONLINE_GDS BB "
						strqry4 = strqry4 & "	WHERE AA.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND BB.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.SEQ = BB.GD_SEQ "
						strqry4 = strqry4 & "	AND AA.GD_BUY_END_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_OPEN_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_PRICE3 > 0 "
						strqry4 = strqry4 & "	AND bb.ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"'"
						strqry4 = strqry4 & "	GROUP BY		 AA.GD_TP "
						strqry4 = strqry4 & "					,AA.GD_GRP_BRAND "
						strqry4 = strqry4 & "					,AA.UNI_GRP_CD "
						strqry4 = strqry4 & "					,BB.GD_PER "
						strqry4 = strqry4 & "					,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "					,BB.GD_TAX_YN "
						strqry4 = strqry4 & "					,AA.GD_OPEN_YN "
						strqry4 = strqry4 & "					,AA.GD_GRP_FIRST "
						strqry4 = strqry4 & "					,AA.GD_GRP_MID "
						strqry4 = strqry4 & "					,AA.GD_GRP_DETL ) A1 "
						strqry4 = strqry4 & "	ORDER BY SEQ DESC "
						'Response.write strqry4
						DBOpen()
							Set rs_product = Dbcon.Execute(strqry4)
						DBClose()
					%>
					<% If rs_product.bof Or rs_product.eof Then %>
					<% Else %>
					<% 
						Do Until rs_product.EOF 
						
						PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
					%>
					<li>
						<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
							<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
							<div class="product_info">
								<dl>
									<dt class="name"><%=rs_product("GD_NM")%></dt>
									<dd class="price">
										<span><b><%= FormatNumber(rs_product("GD_PRICE3"),0)%></b>원</span>
										<span><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span>
										<span class="percent"><b><%=PERCENT%></b>%</span>
									</dd>
								</dl>
							</div>
							<span class="mark"><b><%=PERCENT%></b>%<br/>SALE</span>
						</a>
					</li>
					<%
						rs_product.MoveNext
						x = x + 1
						Loop
						rs_product.Close
						Set rs_product = Nothing
						End If 
					%>
				</ul>
			</div><!-- list_box e -->
		</div><!-- list_wrap e -->
	</div><!-- cont_bottom_wrap e -->
</div><!-- container main e -->
<!-- #include virtual = "include/footer.asp" -->
