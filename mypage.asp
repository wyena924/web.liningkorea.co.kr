<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>
	
	function email_ch(thisis)
	{
		document.getElementById("email2").value = thisis.value

	}
  function execDaumPostCode(input_type) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }
                // 우편번호와 주소 정보를 해당 필드에 넣는다.

				if(data.userSelectedType == 'R')
				{
					document.getElementById("cust_zip").value = data.zonecode
					
					document.getElementById("cust_addr").value = fullRoadAddr;					
				}
				 
				else if(data.userSelectedType == 'J'){

					 if (data.postcode1 =='')
						 {
							document.getElementById("cust_zip").value = data.zonecode
						 }
						 else
						 {
							document.getElementById("cust_zip").value = data.postcode1 + data.postcode2
						 }
					
					document.getElementById("cust_addr").value = data.jibunAddress;
				}


				document.getElementById("cust_addr2").focus()			

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
    //                document.getElementById("guide").innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
  //                  document.getElementById("guide").innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById("guide").innerHTML = '';
                }
            }
        }).open();
	}


	function id_checked()
		{
		  var  online_id =document.getElementById('online_id').value 
		  if (online_id  =='')
			 {

				alert("ID 명을 입력해주시기 바랍니다..");
				document.getElementById('online_id').focus();
				return;
			 }

			 var strAjaxUrl="/ajax/order_id_ok.asp?online_id="+online_id;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 ID입니다.');
									//document.bform.id_ok.value = ''
									return;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 ID입니다..');
									document.getElementById('id_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}


function chk_info()
{
	var f = document.meminfo;
	if(f.now_online_pw.value==""){
		alert("현재 비밀번호를 입력해 주세요.");
		f.now_online_pw.focus()
		return ;
	}

	//변경비밀번호 미입력시에는 체크하지 않음
	if(f.new_online_pw1.value!="" || f.new_online_pw2.value!=""){
		if(f.new_online_pw1.value==""){
			alert("변경하실 비밀번호를 입력해 주세요.");
			f.new_online_pw1.focus();
			return ;
		}
		if(f.new_online_pw2.value==""){
			alert("변경하실 비밀번호 확인을 입력해 주세요.");
			f.new_online_pw2.focus();
			return;
		}

		if(f.new_online_pw1.value!=f.new_online_pw2.value){
			alert("변경하실 비밀번호가 일치하지 않습니다.");
			f.new_online_pw1.value="";
			f.new_online_pw2.value="";
			f.new_online_pw1.focus();
			return;
		}

		if(f.now_online_pw.value==f.new_online_pw1.value){
			alert("변경하려는 비밀번호가 현재 비밀번호와 동일합니다.");
			return;
		}
	}

	if(f.cm_tel1.value==""){
		alert("전화번호를 입력해 주세요.");
		f.cm_tel1.focus();
		return;
	}
	
	if(f.cm_tel2.value==""){
		alert("전화번호를 입력해 주세요.");
		f.cm_tel2.focus();
		return;
	}

	if(f.cm_tel3.value==""){
		alert("전화번호를 입력해 주세요.");
		f.cm_tel3.focus();
		return;
	}

	if(f.email1.value==""){
		alert("이메일을 입력해 주세요.");
		f.email1.focus();
		return;
	}

	if(f.email2.value==""){
		alert("이메일을 입력해 주세요.");
		f.email2.focus();
		return;
	}

	if(f.cust_zip.value==""){
		alert("주소찾기를 이용해 주소를 입력해 주세요.");
		return;
	}

	if(f.cust_addr.value==""){
		alert("주소를 입력해 주세요.");
		f.cust_addr.focus();
		return;
	}

	f.target = "iSQL";
	f.action = "mypage_edit_ok.asp";
	f.submit();
}

function member_delete()
{
	var f = document.meminfo;
	if(f.now_online_pw.value==""){
		alert("현재 비밀번호를 입력해 주세요.");
		f.now_online_pw.focus()
		return ;
	}

	if(confirm("정말 회원 탈퇴를 하시겠습니까?")){
		f.target = "iSQL";
		f.action = "member_delete_ok.asp";
		f.submit();
	}
}
</script>
<%
	strqry = "		SELECT		 ORDER_SEQ "
	strqry = strqry & "			,ONLINE_ID "
	strqry = strqry & "			,ONLINE_PWD "
	strqry = strqry & "			,ORDER_NM "
	strqry = strqry & "			,CM_EMAIL "
	strqry = strqry & "			,CM_ZIP	"
	strqry = strqry & "			,CM_ZIP2 "
	strqry = strqry & "			,CM_ADDR "
	strqry = strqry & "			,CM_ADDR_DETL "
	strqry = strqry & "			,CM_HP "
	strqry = strqry & "			,RE_EMAIL_YN "
	strqry = strqry & "	FROM IC_T_ORDER_CUST  "
	strqry = strqry & "	WHERE GRP1 = '"&GLOBAL_GRP1&"'  "
	strqry = strqry & "	AND GRP2 = '"&GLOBAL_GRP2&"'  "
	strqry = strqry & "	AND ONLINE_ID = '"&GetsLOGINID()&"'  "
	strqry = strqry & "	AND DEL_YN ='N' "
	
	DBOpen()
		Set RS_MEMBER = Dbcon.Execute(strqry)
	DBClose()
%>
<form name="meminfo" method="post" >
<input type="hidden" name="id_ok" id="id_ok">
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">회원정보수정</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
	<div class="order_page sub_bg">
		<form method="post" action="">
		<div class="order_wrap">
			<div class="order_form">
				<div class="cont">
					<dl>
						<dt>아이디</dt>
						<dd>
							<%=RS_MEMBER("ONLINE_ID")%>
						</dd>
					</dl>
					<dl>
						<dt>현재비밀번호</dt>
						<dd>
							<label for="" class="hide">현재비밀번호</label>
							<input type="password" id="now_online_pw" class="wid_257" title=""  name="now_online_pw"/>
							<span>특수문자 외 / 영문조합 6~10자리</span>
						</dd>
					</dl>
					<dl>
						<dt>변경비밀번호</dt>
						<dd>
							<label for="" class="hide">현재비밀번호</label>
							<input type="password" id="new_online_pw1" class="wid_257" title=""  name="new_online_pw1" maxlength="25"/>
							<span>특수문자 외 / 영문조합 6~10자리</span>
						</dd>
					</dl>
					<dl>
						<dt class="pw_check">비밀번호 확인</dt>
						<dd>
							<label for="" class="hide">비밀번호 확인</label>
							<input type="password" id="new_online_pw2" class="wid_257" title=""  name="new_online_pw2" maxlength="25"/>
						</dd>
					</dl>
					<dl>
						<dt>성명</dt>
						<dd>
							<label for="" class="hide">성명</label>
								<%=RS_MEMBER("ORDER_NM")%>
						</dd>
					</dl>
					<%
						If InStr(RS_MEMBER("CM_EMAIL"),"@") > 0 Then 
							If GetsCUSTEMAIL() = "" Or GetsCUSTEMAIL() = "@" Then 
								cm_email1 = ""
								cm_email2 = ""
							Else 
								Array_Email = Split(GetsCUSTEMAIL(),"@")
								cm_email1 = Array_Email(0)
								cm_email2 = Array_Email(1)
							End If 
						Else
							cm_email1 = ""
							cm_email2 = ""
						End If 
					%>
					<dl class="mail_cont">
						<dt>메일주소</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" id="email1" class="" title=""  name="email1" value="<%=cm_email1%>" /><b class="mg">@</b>
							<label for="" class="hide"></label>
							<input type="text" id="email2" class="" title=""  name="email2" value="<%=cm_email2%>"/>&nbsp;&nbsp;
							<div class="select_box">
								<label for="mail01">직접입력</label>
								<select id="mail01" name="email_on" id="email_on" onchange="javascript:email_ch(this)" >
									<option value ="" selected="selected">직접입력</option>
									<option value="naver.com">naver.com</option>
									<option value="nate.com">nate.com</option>
									<option value="daum.com">daum.net</option>
									<option value="gmail.com">gmail.com</option>
								</select>
							</div><!-- select_box e -->
						</dd>
					</dl>
					<%
						cm_zip  = RS_MEMBER("CM_ZIP")
						cm_zip2 = RS_MEMBER("CM_ZIP2")
						cm_addr = RS_MEMBER("CM_ADDR")
					%>
					<dl class="address_cont">
						<dt>주소</dt>
						<dd>
							<label for="" class="hide">우편번호</label>
							<input type="text" class="post" title="" name="cust_zip" id="cust_zip" maxlength="6" value="<%= cm_zip%><%=cm_zip2%>" readonly/>
							<input type="button" id="" class="" title="" value="주소검색" name="" onclick="execDaumPostCode('1');" />
							<label for="" class="hide">메인주소</label>
							<input type="text" class="wid_l mg" title="" name="cust_addr" id="cust_addr" maxlength="200"  value="<%= cm_addr%>" />
						</dd>
					</dl>
					<%
						If RS_MEMBER("CM_HP") = "" Or RS_MEMBER("CM_HP") = "-" Or RS_MEMBER("CM_HP") = "--" Then 
							cm_tel1 = ""
							cm_tel2 = ""
							cm_tel3 = ""
						Else

							Array_CM_TEL = Split(RS_MEMBER("CM_HP"),"-") 
							cm_tel1 = Array_CM_TEL(0)
							cm_tel2 = Array_CM_TEL(1)
							cm_tel3 = Array_CM_TEL(2)
						End If  
					%>
					<dl class="phone_cont">
						<dt >연락처</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" id="cm_tel1" class="" title=""  name="cm_tel1" value="<%=cm_tel1%>"/><b class="mg">-</b>
							<label for="" class="hide"></label>
							<input type="text" id="cm_tel2" class="" title=""  name="cm_tel2"  value="<%=cm_tel2%>"/><b class="mg">-</b>
							<label for="" class="hide"></label>
							<input type="text" id="cm_tel3" class="" title=""  name="cm_tel3"  value="<%=cm_tel3%>"/>
						</dd>
					</dl>
					<dl class="phone_cont">
						<dt >마케팅정보수신동의</dt>
						<dd>
							<div class="term_cont">
							
							1. 주) 위드라인은 서비스 이용 중 필요하다고 인정되는 다양한 마케팅 정보 등을 전자우편이나 LMS 등의 방법으로 회원에게 제공할 수 있습니다.<br /><br />
							2. 회원은 관련법에 따른 거래관련정보 및 고객문의 등에 대한 답변 등을 제외하고 언제든지 전항의 전자우편등에 대하여 수신을 거절할수 있으며이 경우 ㈜ 위드라인은 즉시 전항의 마케팅 정보 등을 제공하는 행위를 중단합니다
							</div>
							<input type="checkbox" id="RE_EMAIL_YN" name="RE_EMAIL_YN" class="input_chk" VALUE="Y" <% If RS_MEMBER("RE_EMAIL_YN") = "Y" Then %> CHECKED<% End If %>/>
							<label for="RE_EMAIL_YN">마케팅정보수신동의</label>
						
						</dd>
					</dl>
				</div>
			</div><!-- order_form e -->
			<div class="btn_wrap">
				<a href="/" class="btn btn_b">취소</a>
				<a href="javascript:chk_info();" class="btn btn_b btn_agree">수정하기</a>
				<a href="javascript:member_delete();" class="btn btn_b btn_agree">회원탈퇴</a>
			</div><!-- btn_wrap e -->
		</div><!-- order_wrap e -->
		</form>
	</div><!-- order_page e -->
	
			
			</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
</form>
 <iframe name="iSQL" id="iSQL" width="0" height="0" frameborder="0"></iframe>
<!-- #include virtual = "include/footer.asp" -->
