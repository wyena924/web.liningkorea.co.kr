<!DOCTYPE HTML>
<html lang="en">
<head>
<title>아이디찾기</title>
<meta charset="utf-8" />
<link rel="stylesheet" type="text/css" href="/common/css/dh_main.css"  />
<link rel="stylesheet" type="text/css" href="/common/css/dh_include.css" ></link>
<!-- #include virtual="include/common.asp" -->
<script>
function CheckInfo(){

	//이메일
	if(document.CheckForm.selectCheck[0].checked == true){
		if (document.CheckForm.Email_UserName.value == "")
		{
			alert("이름을 입력하시기 바랍니다.");
			document.CheckForm.Email_UserName.focus();
			return;
		}

		if (document.CheckForm.Email.value == "")
		{
			alert("이메일 주소를 입력하시기 바랍니다.");
			document.CheckForm.Email.focus();
			return;
		}
		
	}
	//전화번호
	else if(document.CheckForm.selectCheck[1].checked == true)
	{
		if (document.CheckForm.Phone_UserName.value == "")
		{
			alert("이름을 입력하시기 바랍니다.");
			document.CheckForm.Phone_UserName.focus();
			return;
		}

		if (document.CheckForm.Phone.value == "")
		{
			alert("전화번호를 입력하시기 바랍니다1.");
			document.CheckForm.Phone.focus();
			return;
		}

	}

	else{
		alert("인증 방식을 선택하시기 바랍니다.");
		return;
	}

		document.CheckForm.target = "CheckFrame";
		document.CheckForm.action = "./search_id_ok.asp";
		document.CheckForm.submit();
}
</script>
</head>
<body>
<div id="idpwSch">  
  <div class="topLogo"><span>아이디 찾기</span></div>
  
  <!-- 아이디찾기 시작 --> 
	<form name="CheckForm" method="post">
  <div class="idpwSchBox" id="DIV_input" style="display:block;">   
      <p>- 회원가입시 작성하신 정보를 입력하시면 아이디를 찾을 수 있습니다.<br>
         - 입력정보를 잊으신 경우 고객센터로 문의해주세요. 고객센터( T.1599-6744 )</p>
      <div class="idpwSchform">     
          <div class="gubun"> 
             <ul>
                <li class="radio"><input name="selectCheck" type="radio" value="Email" checked> 이메일 주소로 찾기</li>
                <li class="idpw">이름&nbsp;&nbsp;&nbsp;<input name="Email_UserName" type="text"  class="input" placeholder=""  maxlength="25"/>
                </li>
                <li class="idpw">이메일 주소&nbsp;&nbsp;&nbsp;<input name="Email" type="email" class="input" placeholder="aaaa@bbbb.com"  maxlength="40"/>
                </li>
             </ul>
           </div> 
           <div class="gubun" style="border-left:1px solid #ccc"> 
             <ul>
                <li class="radio"><input name="selectCheck" type="radio" value="Phone"> 전화번호로 찾기</li>
                <li class="idpw">이름&nbsp;&nbsp;&nbsp;<input name="Phone_UserName" type="text" class="input" placeholder="" maxlength="25"/>
                </li>
                <li class="idpw">일반전화/휴대폰&nbsp;&nbsp;&nbsp;<input name="Phone" type="number" class="input" placeholder="" maxlength="16"/>
                </li>
                <li style="line-height:30px; color:#666; font-size:12px;">[ 등록하신 전화번호 또는 핸드폰번호를 &minus; 없이 입력해주세요. ]</li>
             </ul>
           </div>          
           <div class="idpwSchbtn">
             <a href="javascript:CheckInfo()"><img src="../images/idsearch.png" title="아이디찾기"/></a>
           </div>
       </div>
   </div>
	 </form>
   <!--// 아이디찾기 끝 -->

  <!-- 확인 시작 --> 
  <div class="idpwSchBox" id="DIV_Result" style="display:none;">    
      <div class="idpwSchform">     
					 <div class="result" id="DIV_ResultText">
           </div>    
           <div class="idpwSchbtn">
             <a href="gate.asp"><img src="../images/login_btn2.png" title="로그인"/></a>
           </div>
       </div>
   </div>
   <!--// 확인 끝 -->   
  
   <div class="clear"></div>

   <!-- footer start -->
   <div class="gft">    
       <div class="g_info">
			고객센터<BR>
			1544-4268<BR>
			<div class="g_info_2">
			업무시간 오전 09:00 ~ 오후06:00<BR>
			점심시간 오후 12:00~오후01:00<BR>
			토요일, 일요일, 공휴일 휴무

	   </div>
		</div>
		<br>
       
       <div class="g_info2">
          <p>
			 경찰공제회상시몰 고객센터| 서울시 마포구 삼개로 16 근신빌딩 본관 5층 
			 <br>사업자등록번호 108-81-66493|
			 <br>(주)위드라인 대표이사 : 김영석|
			 <br>E-mail : online@widline.co.kr| 
          </p>              
       </div>
   </div> 
   <!-- footer end -->  
<iframe id="CheckFrame" name="CheckFrame" border="0" width="0" height="0"></iframe>
</div> 
</body>
</html>

