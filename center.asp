<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	page			= fInject(request("page"))
	search_key		= fInject(request("search_key"))
	search_keyword	= fInject(request("search_keyword"))

	if page = "" then page = 1

	Dim page_size, block_page, total_cnt, total_page, h_number

	page_size = 10
	block_page = 10	

	goParam = "?tmp=null"

	Dbopen()

	sql = "USP_PMAA_USER_BOARD_NOTICE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size & ", @SEARCH_KEY='" & search_key & "', @SEARCH_KEYWORD='" & search_keyword &"'"

	'Response.write sql
	'Response.end
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numList = -1
	Else
		total_cnt = rs(0)
		total_page = rs(1)
			
		h_number = total_cnt - ((page -1) * page_size)
				
		arrList = rs.GetRows
		numList = Ubound(arrList,2)
	End If

	sql = "USP_PMAA_BOARD_NOTICE_VIEW @mode='TOP'"
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numTopList = -1
	Else
		arrTopList = rs.GetRows
		numTopList = Ubound(arrTopList,2)
	End If

	TotalPage=Int((CInt(numTopList)-1)/CInt(page_size)) +1

%>
<script language='javascript'>
	
	function serch()
	{
		document.bform.submit();	
	}


	function center_search(NowPage,ViewCnt,paging_gb)
		{

			var search_key = document.getElementById("search_key"); //서치키 
			var search_keyword = document.getElementById("search_keyword"); //서치키워드			

			if (paging_gb !='paging')
			{
				if (search_key.value=='')
				{
					alert('주문조건을 화인해주시기 바랍니다');
					search_key.focus();
					return;
				}
				if (search_keyword.value=='')
				{
					alert('검색어를 입력해주시기 바랍니다.');
					search_keyword.focus();
					return;
				}
			}
			//alert(search_key.value);
			//alert(search_keyword.value);
			var strAjaxUrl="/ajax/center_ajax_list.asp"
			var retDATA="";
			//alert(strAjaxUrl);
			$.ajax({
				 type: 'POST',
				 url: strAjaxUrl,
				 dataType: 'html',
				 data: {
				    page:NowPage,
					pagesize:ViewCnt,
					search_key:search_key.value,
					search_keyword:search_keyword.value
				},						  
				success: function(retDATA) {
					if(retDATA)
						{

							if(retDATA!="")
							{
								document.getElementById("center_ajax_list").innerHTML= retDATA;
							}
						}
				 }
			}); //close $.ajax(
		}

</script>
<form name="bform" method="post" action="center.asp">
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">공지사항</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page">
					<div class="sch_box">
						<div class="select_box select02">
							<label for="search_key" >
							<% If search_key = "all" Then %>
								전체
							<%ElseIf search_key = "subject" Then %>
								제목
							<%ElseIf search_key = "contents" Then %>
								내용
							<%ElseIf search_key = "admin_name" Then %>
								작성자
							<% Else %>
								전체
							<% End If %>
							</label>
							<select  name="search_key" id="search_key">
								<option value="all" selected="selected" <% If search_key = "all" Then %>selected<% End If %>>전체</option>
								<option value="subject"<% If search_key = "subject" Then %>selected<% End If %>>제목</option>
								<option value="contents"<% If search_key = "contents" Then %>selected<% End If %>>내용</option>
								<option value="admin_name"<% If search_key = "admin_name" Then %>selected<% End If %>>작성자</option>
							</select>
						</div><!-- select_box e -->
						<input type="text" name="search_keyword" id="search_keyword" class="sch_txt"/>
						<input type="button" id="btn_sch" value="검색" onclick="javascript:serch();"/>
					</div>
					<div class="sc_table" ID="center_ajax_list">
						<table cellpadding="0" cellspacing="0" class="t_type01" summary="">
							<colgroup>
								<col class="num"/>
								<col class="tit"/>
								<col class="name hide_s"/>
								<col class="date"/>
								<col class="see hide_s"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="" class="num">코드</th>
									<th scope="" class="tit">제목</th>
									<th scope="" class="hide_s">작성자</th>
									<th scope="">작성일</th>
									<th scope="" class="hide_s">조회</th>
								</tr>
							</thead>
							<tbody>
								<%
									for i = (page-1) * page_size To numList
									  seq			= arrList(2,i)
									  subject		= arrList(3,i)
									  writer		= arrList(4,i)
									  content		= arrList(5,i)
									  filename		= arrList(6,i)
									  isview		= arrList(7,i)
									  hit			= arrList(8,i)
									  create_date	= arrList(9,i)
								%>
								<tr class="first">
									<td class="num"><%=seq%></td>
									<td class="tit"><a href="center_view.asp?seq=<%=seq%>"><%=subject%></a></td>
									<td class="hide_s"><%=writer%></td>
									<td><%=create_date%></td>
									<td class="hide_s"><%=hit%></td>
								</tr>
								<%
									h_number = h_number - 1
									next
								%>	
							</tbody>
						</table>
						<%=PT_center_Ajax_PageLink(page,page_size,"paging")%>
					</div>
				</div>
			</div>
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
</form>
<!-- #include virtual = "include/footer.asp" -->
