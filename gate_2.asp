<!-- #include virtual = "/include/common.asp" -->

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta name="Generator" content="EditPlus®">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=2, user-scalable=no">
<title>위드라인B2B몰</title>
<style type="text/css">
/* nanumgothic */
@import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);
/*Noto Sans KR*/
@import url(http://fonts.googleapis.com/earlyaccess/notosanskr.css);
body,html,div,form,label,input,button,a,p,table,tr,th,td,textarea,dl,dt,dd,ul,li{margin:0;padding:0;line-height:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;letter-spacing:-1px;font-family:'Noto Sans KR','Nanum Gothic',sans-serif;color:#333;}
input[type="checkbox"]{display:none;}
input[type="checkbox"] + label span{display:inline-block;width:15px;height:15px;margin:-2px 5px 0 0;vertical-align:middle;border:1px solid #c3c3c3;cursor:pointer;}
input[type="checkbox"]:checked + label span{background:url('http://emall.halla.com/mobile/images/chk_on.png') no-repeat left top/100%;}
ul,li{list-style:none;}
.hide{width:1px;height:1px;overflow:hidden;visibility:hidden;line-height:0;font-size:0;position:absolute;left:-999px;}
a[href^=tel]{color:inherit;text-decoration:inherit;font-size:inherit;font-style:inherit;font-weight:inherit;}/* 사파리 밑줄제거 */

.login_wrap input::-webkit-input-placeholder,
.login_wrap textarea::-webkit-input-placeholder{color:#666;font-size:15px;}/* Chrome/Opera/Safari */
.login_wrap input::-moz-placeholder,
.login_wrap textarea::-moz-placeholder{color:#666;font-size:15px;}/* Firefox 19+ */
.login_wrap input:-ms-input-placeholder,
.login_wrap textarea:-ms-input-placeholder{color:#666;font-size:15px;}/* IE 10+ */
.login_wrap input:-moz-placeholder,
.login_wrap textarea:-moz-placeholder{color:#666;font-size:15px;}/* Firefox 18- */

.login_wrap{position:relative;width:100%;margin:0 auto;overflow:hidden;}
.login_wrap h1{text-align:center;width:1200px;margin:0 auto 5%;}
.login_wrap h1 img{width:100%;max-width:700px;}
@media screen and (max-width: 1200px){
	.login_wrap h1{width:100%;}
}
.login_wrap .login_form{width:100%;max-width:640px;margin:0 auto;padding:10px;}
.login_wrap .login_form .login_box{float:left;width:75%;}
.login_wrap .login_form .login_box input{border:1px solid #e7e7e7;background:#eee;width:100%;height:50px;padding-left:3%;margin-top:1%;}
.login_wrap .login_form .login_box input#get_login_id{margin-top:0;}
.login_wrap .login_form .login_box .hide{width:0;height:0;overflow:hidden;visibility:hidden;line-height:0;font-size:0;position:absolute;left:-999px;}
.login_wrap .login_form .login_btn{float:right;width:24%;}
.login_wrap .login_form .login_btn .button{text-align:center;color:#fff;border:none;background:#0e4194;width:100%;height:104px;font-size:15px;-webkit-appearance:none;/* 사파리버튼리셋 */}
.login_wrap .login_form .save_id{clear:both;position:relative;padding:20px 0 30px;border-bottom:1px solid #333;}
.login_wrap .login_form .save_id label{font-size:17px;color:#333;}
.login_wrap .login_form .find_wrap{margin-top:30px;color:#c3c3c3;}
.login_wrap .login_form .find_wrap p{color:#666;font-size:15px;margin-bottom:10px;}
.login_wrap .login_form .find_wrap a{color:#333;font-size:15px;text-decoration:none;}
.login_wrap .cs_wrap{margin:5% 0;}
.login_wrap .cs_wrap p{text-align:center;font-size:12px;color:#333;margin-bottom:5px;text-decoration:none;}
/*.login_wrap .cs_wrap p:first-child{font-size:15px;margin-bottom:25px;}*/


@media screen and (max-width:740px){
	.login_wrap{position:relative;max-width:640px;margin:0 auto;overflow:hidden;}
}

/* popup */
.popup_wrap{left:0px;top:0px;width:100%;height:100%;position:absolute;z-index:999;background:rgba(0,0,0,0.5);display:none;}
.popup_wrap button{cursor:pointer;border:none;background:none;}
.popup_wrap .point{color:blue;text-indent:-14px;line-height:1.2;padding-left:15px;font-size:13px;}
.popup_wrap input{width:177px;height:29px;line-height:29px;padding-left:5px;border:1px solid #c3c3c3;}
.popup_wrap input[type="number"]{text-align:center;padding-left:0;}
.popup_wrap input[type="checkbox"]{border:1px solid #c3c3c3;background:none;}
.popup_wrap button,.popup_wrap input{vertical-align:middle;}
.popup_wrap label{margin-top:5px;font-size:13px;display:block;}

.popup_wrap .box{background-color:#fff;border:solid 3px #027cc5;border-top:none;position:absolute;left:50%;z-index:10000;;width:95%;max-width:600px;height:auto;top:50%;transform:translate(-50%,-50%);-webkit-transform:translate(-50%,-50%);-moz-transform:translate(-50%,-50%);-ms-transform:translate(-50%,-50%);-o-transform:translate(-50%,-50%);}
.popup_wrap h2{position:relative;margin:0;padding:20px 10px;background:#027cc5;color:#fff;font-size:15px;}
.popup_wrap #close_x{font-weight:800;position:absolute;top:0;right:0;padding:15px 10px;font-size:25px;color:#fff;z-index:99991;}

/* 회원가입 */
.join_wrap dl{width:100%;padding:15px 10px;border-bottom:1px solid #eee;}
.join_wrap dt{text-align:left;font-weight:400;letter-spacing:-1px;margin-bottom:10px;}
.join_wrap .btn_join{padding:8px;background:#555;color:#fff;margin-left:5px;}
.join_wrap .num_short{width:50px;}
.join_wrap .num_long{width:100%;margin-top:5px;}
.join_wrap .term{padding-left:10px;}
.join_wrap .term h3{margin-top:0;font-size:15px;font-weight:400;}
.join_wrap form{height:450px;overflow-y:scroll;}
.join_wrap .term .term_cont{font-size:13px;height:100px;padding:5px;overflow-y:scroll;border:1px solid #c3c3c3;}
.join_wrap .term .term_cont p{margin-bottom:15px;line-height:1.3;}

/* 아이디&비번찾기 */
.popup_wrap .find_info{text-indent:-6px;margin-left:6px;padding:10px;}
.popup_wrap .find_info li{font-size:14px;line-height:1.3;margin-top:5px;}
.popup_wrap .find_box{}
.popup_wrap .find_box .find_cont{text-align:center;padding:10px 10px 0;}
.popup_wrap .find_box .find_cont:first-child{margin-top:0;}
.popup_wrap .find_box .find_cont label{display:inline-block;text-align:right;width:81px;margin-right:10px;vertical-align:middle;}
.popup_wrap .find_box .find_cont input{margin-top:10px;}
.popup_wrap .find_box .find_cont input[type="radio"]{width:auto;height:auto;}
#find_id .find_cont > label{width:auto;text-align:left;}
#find_id label{width:100px;}
#find_pw label{width:100px;}

.login_wrap .btn_wrap{margin:30px 0;text-align:center;}
.login_wrap .btn_wrap a{display:inline-block;background:#eee;color:#333;border:1px solid #c3c3c3;width:127px;padding:10px 0;text-decoration:none;}
.login_wrap .btn_wrap a.btn_agree{background:#027cc5;border:1px solid #027cc5;color:#fff;}
.login_wrap .btn_wrap a.btn_agree_button{background:#555;border:1px solid #027cc5;color:#fff;}


.popup_wrap .end_box{left:50%;}
.popup_wrap .end_box span{color:#027cc5;}
.popup_wrap .end_box p{text-align:center;padding:50px 0 0;line-height:1.5;}
.popup_wrap .end_box p.end_box_info{padding-top:20px;line-height:1.3;font-size:13px;color:#555;}

.popup_cont{position:absolute;top:50%;left:50%;transform:translate(-50%,-50%);z-index:1;}
.popup_cont > a{display:block;}
.popup_cont img{width:100%;}
.popup_cont .btn_close{background:#333;padding:10px;text-align:right;}
.popup_cont .btn_close button{background:none;border:none;color:#fff;margin-left:10px;}

/*********** media *************/
/*360(브라우저 가로폭)*/
@media screen and (max-width: 360px){
.login_wrap .login_form .login_box input{height:40px;}
.login_wrap .login_form .login_btn .button{height:82px;}
.login_wrap .login_form .save_id input[type="checkbox"] + label:before{width:15px;height:15px;line-height:15px;}
.login_wrap .login_form .find_wrap{margin-top:15px;}
.login_wrap .login_form .save_id label{font-size:12px;}
/*.login_wrap .login_form .find_wrap p,.login_wrap .login_form .find_wrap a,.login_wrap .cs_wrap p{font-size:15px;}*/
/*.login_wrap .cs_wrap p:first-child{font-size:15px;margin-bottom:20px;}*/
}
/*320(브라우저 가로폭)*/
@media screen and (max-width: 320px){
#popup01{width:87%;margin-left:-43%;}
#popup01 .btn_close{width:100%;}
}
.write{text-align:center;width:100px;margin:15px 0;color:#898989;font-size:15px;padding:10px;background:#0e4194;color:white;}
.write .write_div{cursor:pointer;color:white;}
.iframe_terms{width:100%;}


.dl_open {}

.div2 {line-height:20px;font-size:12px;margin:10px auto;color:blue;}

.div3{ position:absolute; font-size:12px; line-height:20px;font-size:17px;margin:-45px; margin-left:120px; auto;color:blue;}

.chk_all{margin-left:20px; font-size:15px;font-style:inherit;font-weight:bold;}

</style>
<script type="text/javascript" src="/common/js/jquery-2.1.3.min.js" ></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>
	
	//alert('방문해주셔서 감사합니다. 회원가입은 4월 25일 14:30분 이후부터 가능합니다.');
	var random_number = 0;
	var number_math = Math.floor(Math.random() * 10);

	if (number_math >= 0 && number_math <= 3)
	{
		random_number = 1;
	}
	if (number_math >= 4 && number_math <= 6)
	{
		random_number = 2;
	}
	if (number_math >= 7)
	{
		random_number = 3;
	}


	function ddd()
	{
		alert('해당 행사는 4월 11일 15시 부터 종료 되었습니다.\n많은 관심 감사합니다. ');
		self.close();
		return;
	}
	function chk_login(){
		//alert('서버 점검중입니다. 10시35부터 ~ 11시5분까지');
		//return;
		
		var f = document.login_frm;
		
			if(f.get_login_id.value==""){
			alert("아이디를 입력해 주세요.");
			f.get_login_id.focus();
			return false;
			}
			if(f.online_pw.value==""){
				alert("비밀번호를 입력해 주세요.");
				f.online_pw.focus();
				return false;
			}	
		
		
		f.action="login_ok.asp?location_gb=Y";
		//f.target = "iSQL";
		f.submit();
		
	}



	//체크박스 클릭시
	function chk_saveid() {
		var f = document.login_frm;
	  var expdate = new Date();
	  // 30일동안 아이디 저장 
	  if (f.saveid.checked)
		expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30);// 30일
	  else
		expdate.setTime(expdate.getTime() - 1);// 쿠키 삭제조건
	  setCookie("saveid", f.get_login_id.value, expdate);
	}

	//폼 로드시 쿠키 아이디 가지고 오기
	function chk_getid() {
		var f = document.login_frm;
	  f.saveid.checked = ((f.get_login_id.value = getCookie("saveid")) != "");
	}

	//쿠키 정보 가지고 오기
	function getCookie(Name) {
	  var search = Name + "="
	  if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
		offset = document.cookie.indexOf(search)
		if (offset != -1){// 쿠키가 존재하면
		  offset += search.length
		  // set index of beginning of value
		  end = document.cookie.indexOf(";", offset)
		  // 쿠키 값의 마지막 위치 인덱스 번호 설정
		  if (end == -1)
			end = document.cookie.length
		  return unescape(document.cookie.substring(offset, end))
		}
	 }
	  return "";
	}
	//쿠키정보 저장
	function setCookie( name, value, expirehours ) { 
		var todayDate = new Date(); 
		todayDate.setHours( todayDate.getHours() + expirehours ); 
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
	} 




	function CheckInfo(){
		
		//이메일
		if(document.id_search_form.selectCheck[0].checked == true){
			if (document.id_search_form.Email_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Email_UserName.focus();
				return;
			}

			if (document.id_search_form.Email.value == "")
			{
				alert("이메일 주소를 입력하시기 바랍니다.");
				document.id_search_form.Email.focus();
				return;
			}
			
		}
		//전화번호
		else if(document.id_search_form.selectCheck[1].checked == true)
		{
			if (document.id_search_form.Phone_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Phone_UserName.focus();
				return;
			}

			if (document.id_search_form.Phone.value == "")
			{
				alert("전화번호를 입력하시기 바랍니다1.");
				document.id_search_form.Phone.focus();
				return;
			}

		}
		if (document.id_search_form.selectCheck[0].checked == true)
		{
			selectCheck= "Email"
		}
		else
		{
			selectCheck= "Phone"
		}

		var strAjaxUrl="/mypage/search_id_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				Email_UserName: escape(document.id_search_form.Email_UserName.value),
				Email: document.id_search_form.Email.value,
				Phone_UserName: escape(document.id_search_form.Phone_UserName.value),
				Phone: document.id_search_form.Phone.value,
				selectCheck: selectCheck
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else if (retDATA !='null')
				{
					document.getElementById("id_search_div").style.display ='block'
					document.getElementById("sarch_id_span").innerHTML = retDATA;
					return;
				}
			}
		});

	}
	

	function CheckEmail()
	{	
		var PWD_UserID = document.pwd_search_form.PWD_UserID.value;
		var PWD_UserEmail = document.pwd_search_form.PWD_UserEmail.value;
		
		var strAjaxUrl="/mypage/search_pw_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				UserID: document.pwd_search_form.PWD_UserID.value,
				Email: document.pwd_search_form.PWD_UserEmail.value
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else 
				{
					document.getElementById("pwd_sarch_div").style.display ='block'		
					document.getElementById("sarch_pwdid_span").innerHTML = PWD_UserID;
					document.getElementById("sarch_pwd_span").innerHTML = retDATA;
					return;
				}
			}
		});
	}

	
	function id_checked()
		{
		  var  online_id =document.getElementById('online_id').value 
		   var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		  if (online_id  =='')
			 {

				alert("ID 명을 입력해주시기 바랍니다..");
				document.getElementById('online_id').focus();
				return;
			 }

		  if ( !regExp.test( online_id ) ) 
			 {
				alert("잘못된 이메일 주소 형식입니다.");
				document.getElementById('online_id').focus();
				return;
			 }
			

			 var strAjaxUrl="order_id_ok.asp?online_id="+online_id;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 ID입니다.');
									//document.bform.id_ok.value = ''
									return false;
									

								}
								else if(retDATA=='NO')
								{
									
									//document.getElementById("id_checked_span").style.display = 'block'
									//init();
									//document.getElementById('id_ok').value  = 'ok'
									var  online_id =document.getElementById('online_id').value;
									var strAjaxUrl="order_id_email_ch.asp";
									$.ajax({
										url: strAjaxUrl,
										type: 'POST',
										dataType: 'html',
										data: 
										{
											email: online_id
										},

										success: function(retDATA) {
											if (retDATA =='kt_email')
											{
												alert('kt 직원분들에 한해서 이메일 인증이 아닌 휴대폰 인증을 진행하고 있습니다.\n이동하는 페이지를 통해 인증을 진행해주시기 바랍니다');
												location.href="gate_kt.asp?online_id="+online_id
												return;

											}
											else if (retDATA!='no_email')
											{
												alert('인증코드가 Email을 통해 발송되었습니다.');
												document.bform.target="email_iframe"
												document.getElementById("get_j_email").value = online_id;
												document.getElementById("get_j_email_code").value = retDATA
												
												//if (random_number==1)
												//{
													document.bform.action="http://www.sdamall.co.kr/pub/sendmail.asp";
												//}
												//if (random_number==2)
												//{
												//	document.bform.action="http://mail.sportsdiary.co.kr/mail.asp"
												//}
												//if (random_number==3)
												//{
												//	document.bform.action="http://mail.itemcenter.co.kr/mail.asp"
												//}
												

												document.bform.submit();
												document.getElementById('signKey').focus();
												document.getElementById("dl_open").style.display = 'block'
												
												
											}
											else if (retDATA =='no_email')
											{
												alert('회원으로 가입하실수 없는 Email 입니다');
												return;
											}
											else
											{
												alert('error');
												return;
												
											}
										}
									});

									return;
								}
							}
					 }
			 }); //close $.ajax(
		}

	
	function id_checked_ok()
		{
			var  online_id =document.getElementById('online_id').value;
			var  signKey   =document.getElementById('signKey').value;
			var strAjaxUrl="order_id_email_ch_ok.asp";
			$.ajax({
				url: strAjaxUrl,
				type: 'POST',
				dataType: 'html',
				data: 
				{
					email: online_id,
					signKey: signKey
				},

				success: function(retDATA) {

					if (retDATA=='OK')
					{
						document.getElementById('id_ch_ok').value ="Y"
						alert('인증이 완료 되었습니다');
					}
					else
					{
						alert('인증코드가 다릅니다 확인하여 주시기 바랍니다.');
						return;
					}
				}
			});
		}

	//var tObj;
	//var sec = 60*5; //현재 5일..(초*분*시간*일자)
	//function init(){
	//	tObj = setInterval("timer()",1000); //1초간격으로 호출
	//}

	//function timer(){
	//	document.getElementById('id_ch_span').innerHTML = lpad(parseInt(sec/60))+":"+lpad(parseInt(sec%60)); //분:초만 표시
	//	sec--;
	//	if(sec < 0){
			//alert("인증번호 받기를 다시하시기 바랍니다.");
	//		clearInterval(tObj);
			//ontime();
	//	}
	//}

	//function lpad(str){
	//	return str > 9 ? str : "0" + str;
	//}

	//function ontime(){
		//document.getElementById('id_ch_span').innerHTML = "05:00"; //분:초만 표시
	//

	//아이디 있는데 사번을 또 받음? 그럼 멀로 받음?
	//그냥 메모를 활용해서 해야할듯!!!!!
	function memo_checked()
		{
		  var  memo =document.getElementById('memo').value 

		  var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 

		  if (memo  =='')
			 {
				alert("사내이메일를 입력해주시기 바랍니다..");
				document.getElementById('memo').focus();
				return;
			 }
		  if ( !regExp.test( memo ) ) 
			 {
				alert("잘못된 이메일 주소 형식입니다.");
				document.getElementById('memo').focus();
				return;
			 }
			 var strAjaxUrl="/Main/memo_id_ok.asp?memo="+memo;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 사내이메일입니다.');
									//document.bform.id_ok.value = ''
									return;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 사내이메일입니다.');
									document.getElementById('memo_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}

		function write_ok()
		{ 			
			var custseq = '<%=GetsCUSTSEQ%>'; 	
			var f				= document.reg_frm;
			var online_id		= f.online_id;
			var custnm			= f.cust_cust_custnm;
			var zip1			= f.cust_zip1;
			var zip2			= f.cust_zip2;
			var addr			= f.cust_addr;
			var cm_hp1			= f.cust_cm_hp1;
			var cm_hp2			= f.cust_cm_hp2;
			var cm_hp3			= f.cust_cm_hp3;
			var checkboxcpy		= f.cust_checkboxcpy;
			var checkboxcpy2	= f.cust_checkboxcpy2;
			var checkboxcpy3	= f.cust_checkboxcpy2;
			var online_pwd		= f.cust_online_pwd;
			var re_online_pwd	= f.cust_re_online_pwd
			var id_ok			= f.id_ok
			var memo_ok			= f.memo_ok
			var regExp			= /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 
			var special_pattern = /[`~!@#$%^&*|\\\'\";:\/?]/gi; 
			//alert(online_id.value	);
			//alert(custnm.value	);
			//alert(zip1.value		  );
			//alert(zip2.value		  );
			//alert(addr.value		  );
			//alert(cm_tel1.value	);
			//alert(cm_tel2.value	);
			//alert(cm_tel3.value	);
			//alert(cm_hp1.value	);
			//alert(cm_hp2.value	);
			//alert(cm_hp3.value	);
			//alert(cm_email.value	  );
			//alert(checkboxcpy.value);
			//alert(online_pwd.value);
			//alert(re_online_pw.value);
			//alert(id_ok.value		  );
			//필수입력사항체크
			if (online_id.value == "") {
				alert('아이디를 입력해주시기 바랍니다.!');	
				online_id.focus();
				return;
			}
			
			if (document.getElementById('id_ch_ok').value =="N")
			{
				alert('아이디 인증체크를 해주시기 바랍니다.');
				return;
			}

			if (online_pwd.value == "") {
				alert('비밀번호를 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value.length < 6) {
				alert('비밀번호를 4자 이상 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value != re_online_pwd.value) {
				alert('비밀번호가 일치 하지 않습니다.');
				re_online_pwd.focus();
				return;
			}	
		
			if(special_pattern.test(online_pwd.value) == true) 
			{ 
				alert('비밀번호는 특수문자를 제외하고 입력해주시기 바랍니다.');
				online_pwd.focus();
				return;
			} 
			
			if (custnm.value == "") {
				alert('성명을 입력해주시기 바랍니다.!');
				custnm.focus();
				return;
			}



			if (cm_hp1.value == "") {
				alert('휴대전화1을 입력해 주시기 바랍니다.');
				cm_hp1.focus();
				return;
			}	

			if (cm_hp2.value == "") {
				alert('휴대전화2를 입력해 주시기 바랍니다.');
				cm_hp2.focus();
				return;
			}	
			if (cm_hp3.value == "") {
				alert('휴대전화3을 입력해 주시기 바랍니다.');
				cm_hp3.focus();
				return;
			}	
			
			if (checkboxcpy.checked ==false)
			{
				alert('약관동의에 체크해 주시기 바랍니다.');
				return;
			}
			
			if (checkboxcpy3.checked ==false)
			{
				alert('마케팅 수신동의에 체크해 주시기 바랍니다.');
				return;
			}
			if (checkboxcpy2.checked ==false)
			{
				alert('제3자 정보동의에 체크해 주시기 바랍니다.');
				return;
			}
			f.action = "join_ok_new.asp"
			f.submit();		
		}

	function execDaumPostCode(input_type) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }
                // 우편번호와 주소 정보를 해당 필드에 넣는다.
				if(data.userSelectedType == 'R')
				{
					document.getElementById("cust_zip1").value = left(data.zonecode,3)
					document.getElementById("cust_zip2").value = right(data.zonecode,2)
					
					document.getElementById("cust_addr").value = fullRoadAddr;					
				}
				 
				else if(data.userSelectedType == 'J'){

					 if (data.postcode1 =='')
						 {
							document.getElementById("cust_zip1").value = left(data.zonecode,3)
							document.getElementById("cust_zip2").value = right(data.zonecode,2)
						 }
						 else
						 {
							document.getElementById("cust_zip1").value = data.postcode1;
							document.getElementById("cust_zip2").value = data.postcode2;
						 }
					
					document.getElementById("cust_addr").value = data.jibunAddress;
				}


				document.getElementById("cust_addr").focus()			

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
    //                document.getElementById("guide").innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
  //                  document.getElementById("guide").innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById("guide").innerHTML = '';
                }
            }
        }).open();
	}

	function left(s,c){
	  return s.substr(0,c);
	}//left("abcd",2)
	function right(s,c){
	  return s.substr(-c);
	}//right("abcd",2)
	function mid(s,c,l){
	  return s.substring(c,l);
	}//mid("abcd",1,2)
	function copy(s,c,l){
	  return s.substr(c,l);
	}//copy("abcd",1,2)



	function enter_key()
	{
		if (window.event.keyCode == 13) {
	 
				 chk_login()
			}
	}


	function check_click()
	{
		document.getElementById("cust_checkboxcpy").checked = true;
		document.getElementById("cust_checkboxcpy2").checked = true;
		document.getElementById("cust_checkboxcpy3").checked = true;
	
	}
</script>
</head>
<body>
<iframe src="" id="email_iframe" name="email_iframe" style="display:none;"></iframe>
<form name="bform" method="post">
	<input type="hidden" id="get_j_email" name="get_j_email">
	<input type="hidden" id="get_j_email_code" name="get_j_email_code">
</form>
<div class="login_wrap">
	<h1>
		<img src="/front/img/mobile_banner.jpg" alt="logo" class="logo_img"/>
	</h1>
	<form method="post" name="login_frm" id="login_frm" action="">
		<input type="hidden" id="id_ch_ok" value="N">
		<div class="login_form">
			<%
				If Gets_saved_id() = "Y" Then
					user_id = gate_GetsLOGINID()
					user_pwd = gate_GetsLOGINPWD()
				Else
					user_id  = ""
					user_pwd = ""
					
				End If 
			%> 

				<div class="login_box">
					<label for="input_id" class="hide">id</label>
					<input type="text" id="get_login_id" name="get_login_id"  placeholder="아이디/이메일주소"  onkeypress="javascript:enter_key();"/>
					<label for="input_pw" class="hide">password</label>
					<input type="password" id="online_pw" name="online_pw" class=""  placeholder="비밀번호/특수문자제외"   onkeypress="javascript:enter_key();"/>
				</div>
			
			<div class="login_btn">
				<input type="button" class="button" onclick="javascript:chk_login();" value="로그인">
			</div>
			<div class="save_id">
				<input type="checkbox"  name="saveid" id="saveid" value="Y" <% If Gets_saved_id() = "Y" Then %> checked<% End If %> />
				
			</div>
			<div class="write" >
				<div class="write_div" onclick="javascript:div_open();">회원가입</div>	
			</div>
			<div class="div3">
				고객센터 : 070-7437-8142
			</div>
			
			<div  class="div2">
				문의/상담시간 안내 : 평일 09:00 ~ 18:00 (점심시간 12:00~13:00 제외)<br>
				근로자의날 행사모델 및 가격  안내 사이트 조회를 위한 회원가입으로 회사 이메일로 인증코드가 발송됩니다.<br>
				위드라인 B2B고객사 임직원을 확인하는 과정으로<br>
				회원가입 고객만 행사 정보를 확인할 수 있습니다.
			</div>
			<div class="find_wrap">
				<p>로그인 정보를 잊으셨나요?</p>
				<a href="#" onclick="javascript:find_id_open();">아이디찾기</a> | 
				<a href="#" onclick="javascript:find_pw_open();">비밀번호찾기</a>
			</div>
		</div>
		<div class="cs_wrap">
			<p>삼성 B2B 고객사의 임직원을 대상으로 하는 행사입니다.</p>
		</div>
	</form>

	<div class="popup_wrap join_wrap" id="join_layer">
		<div class="box">
			<h2>회원가입<button onClick="javascript:div_close();" id="close_x" type="button">X</button></h2>
			<form  name="reg_frm" method="post" >
				<input type="hidden" name="id_ok" id="id_ok">
				<input type="hidden" name="memo_ok" id="memo_ok">
				<dl>
					<dt>
						아이디
					</dt>
					<dd>
						<label for="id" class="hide">id</label>
						<input type="text" id="online_id" class="" title="" value="" name="online_id"/>
						<button class="btn_join" type="button" onclick="javascript:id_checked();">인증하기</button>ex)sample@samsung.com
						<label for="pw" class="point">※) 아이디는 사내이메일을 입력하여 주시기 바랍니다.<br>
							아이디 입력후 인증하기 버튼을 누르시면 인증코드가 발송됩니다.<br>
							행사대상 고객사 이메일만 인증코드가 발송됩니다.<br>
							인증코드 발신자 : online@widline.co.kr<br><br><br>
							
						</label>
						<label for="pw" class="point" style="font-size:15px;">
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b># 현재 동시접속자수가 많아 인증메일 발송이 지연될 수 있습니다.
							<br>이점 참고하여 주시기 바랍니다.</b>
						</label>
						
						
					</dd>
				</dl>
				<dl id="dl_open" class="dl_open">
					<dd>
						<label for="id" class="hide">id</label>
						<input type="text" id="signKey" class="" title="" value="" name="signKey"/>
						<button class="btn_join" type="button" onclick="javascript:id_checked_ok();" style="background:blue;">인증확인</button>
						<!--div id="id_ch_span" class="id_ch_span" style="color:blue;">05:00</div-->
					</dd>
				</dl>
				<dl>
					<dt>비밀번호</dt>
					<dd>
						<input type="password" id="cust_online_pwd" class="" title="" value="" name="cust_online_pwd" maxlength="10"/>
						<label for="pw" class="point" style="font-size:17px;"><b>특수문자 제외 / 영문숫자조합 6~10자리</b></label>
					</dd>
				</dl>
				<dl>
					<dt>비밀번호 확인</dt>
					<dd>
						<input type="password" id="cust_re_online_pwd" class="" title="" value="" name="cust_re_online_pwd" maxlength="10"/>
						<label for="pwc" class="hide"></label>
					</dd>
				</dl>
				<dl>
					<dt>성명</dt>
					<dd>
						<label for="name" class="hide">name</label>
						<input type="text" id="cust_cust_custnm" class="" title="" value="" name="cust_cust_custnm"/>
					</dd>
				</dl>
				<dl>
					<dt>휴대폰번호</dt>
					<dd>
						<label for="phone01" class="hide">첫자리</label>
						<input type="number" id="cust_cm_hp1" class="num_short" title="" value="" name="cust_cm_hp1" maxlength="3"/> - 
						<label for="phone02" class="hide">가운데자리</label>
						<input type="number" id="cust_cm_hp2" class="num_short" title="" value="" name="cust_cm_hp2" maxlength="4"/> - 
						<label for="phone03" class="hide">끝자리</label>
						<input type="number" id="cust_cm_hp3" class="num_short" title="" value="" name="cust_cm_hp3" maxlength="4"/>
					</dd>
				</dl>
				<dl class="term">
					<dt>개인정보 취급방침</dt>
					<dd>
						<div class="term_cont">
							㈜위드라인 서비스 이용 약관<br />
									㈜위드라인 서비스 이용 약관(&quot;이용 약관&quot;)에는 서비스 공급자인 ㈜위드라인 와 서비스 사용자인 고객(&quot;고객&quot; 또는 &quot;사용자&quot;) 간의 법적 관계를 관할하고 규정하는 조항이 자세히 설명되어 있습니다.<br />
									&nbsp;<br />
									사용자는 본 이용 약관을 자세히 읽은 후 이용 약관에 동의하지 않을 경우, 본 이용 약관에 동의하거나 서비스에 등록 또는 액세스하거나 이를 이용(서비스 &quot;이용&quot;으로 통칭)하지 말아야 합니다. (주)위드라인은 고객과 체결한 본 이용 약관의 개별 사본을 저장하지 않으므로 고객이 본인 기록용으로 본 약관의 사본을 저장해두는 것이 좋습니다.<br />
									&nbsp;<br />
									고객이 (a) 민법상 미성년자이거나(단, 서비스에 따라 가입 또는 서비스 가능 연령은 다를 수 있습니다). (b) 대한민국 법률에 따라 서비스를 받을 수 없도록 금지된 사람인 경우에는 일부 서비스 이용이 제한될 수 있습니다.<br />
									&nbsp;<br />
									본 이용 약관에서 &quot;서비스&quot;란 (주)위드라인의 http://b2bc.samsungbizmall.com에 자세히 설명되어 있고 경우에 따라 업데이트되는 모든 서비스를 의미하며, 별도의 계약에 따라 고객에게 제시 또는 제공되는 서비스는 제외됩니다.<br />
									&nbsp;<br />
									[이용 약관 동의 방법]<br />
									(주)위드라인의 이용 약관 변경 방법 및<br />
									(주)위드라인 또는 고객의 이용 약관 해지 방법<br />
									&nbsp;<br />
									&nbsp;<br />
									1. 이용 약관 동의<br />
									&nbsp;<br />
									1.1. 고객이 서비스 이용 허가를 받으려면 이용 약관 및 경우에 따라 특별 약관에 동의해야 합니다. 일반적으로 이용 약관 및 특별 약관은 각 서비스의 사용자 인터페이스를 통해 구현된 &quot;동의&quot; 버튼을 클릭하시거나, 이와 유사하게 구현된 기능을 활용하여 동의합니다.<br />
									&nbsp;<br />
									1.2. 또한 고객이 서비스를 실제로 이용하기 시작하면 본 이용 약관 및 특별 약관에 동의하는 것이 됩니다. 이 경우 고객은 서비스를 이용하기 시작하는 동시에 본 이용 약관이 적용되며 (주)위드라인이 본 이용 약관에 따라 고객을 대우한다는 사실을 이해합니다.<br />
									&nbsp;<br />
									2. 이용 약관에 대한 변경<br />
									&nbsp;<br />
									2.1. ㈜위드라인은 수시로 본 이용 약관을 수정하거나 변경할 수 있습니다. 약관이 변경되는 경우 ㈜위드라인은 새로운 약관의 사본 및 변경사유를 시행 10일 전(사용자에게 불리하거나 중대한 사항의 변경은 30일 전)에 http://b2bc.samsungbizmall.com에서 공지하고, 기존 회원에게는 필요시 이메일 발송 등 적절한 방법으로 통지합니다. 만약, 회원의 연락처 미기재, 변경 등으로 인하여 개별 통지가 어려운 경우에는 http://b2bc.samsungbizmall.com에 변경 약관을 공지함으로써 개별 통지한 것으로 봅니다.<br />
									&nbsp;<br />
									2.2. 고객이 변경된 약관의 효력 발생일까지 약관 변경에 대한 거부의사를 명시적으로 표시하지 않거나 약관 변경 이후에 서비스를 이용할 경우 변경된 약관에 동의한 것으로 봅니다.고객이 변경된 약관에 동의하지 않는 경우, 고객이 서비스 이용이 제한되거나 정지될 수 있습니다.<br />
									&nbsp;<br />
									3. 이용 약관 해지 및 해지의 결과<br />
									&nbsp;<br />
									3.1. 본 이용 약관은 고객이나 (주)위드라인이 해지하지 않는 한 해지 시까지 계속 유효합니다.<br />
									&nbsp;<br />
									3.2. 고객은 ㈜위드라인 계정 홈페이지(http://b2bc.samsungbizmall.com)를 방문, 고객 계정으로 로그인 후 프로필에 접속하여 언제든지 계정 탈퇴를 할 수가 있습니다.<br />
									&nbsp;<br />
									3.3. ㈜위드라인은 등록 데이터의 일부로 ㈜위드라인에 제공된 이메일 주소를 사용하거나 고객이 해지 통지를 받을 수 있는 기타 적절한 방식을 통해 30일 이전에 해지에 관한 서면 통지를 제공하여 언제든지 본 이용 약관을 해지할 수 있습니다.<br />
									&nbsp;<br />
									3.4. 다음과 같은 경우 (주)위드라인은 통지 기간을 지키지 않고 언제든지 본 계약을 해지할 수 있습니다.<br />
									&nbsp;<br />
									a. 고객이 이용 약관을 위반한 경우<br />
									&nbsp;<br />
									b. 고객이 이용 약관을 준수할 의도가 없음을 명백히 표시한 경우(직접 또는 행동이나 진술 또는 기타 방식을 통하든 관계 없음)<br />
									&nbsp;<br />
									c. ㈜위드라인 또는 ㈜위드라인에게 또는 ㈜위드라인과 함께 서비스를 제공하는 ㈜위드라인의 공급자 또는 파트너가 서비스 또는 그 일부의 제공을 종료하기로 결정하거나(전 세계 또는 고객의 거주 국가 또는 서비스 이용 국가에서), (주)위드라인의 공급자나 파트너가 (주)위드라인과의 모든 관계를 해지하기로 결정하는 경우 (주)위드라인 또는 (주)위드라인의 공급자나 파트너가 서비스 또는 그 일부를 고객이나 (주)위드라인에 제공하거나 (주)위드라인과 함께 제공하는 일이 더 이상 상업적으로 가능하지 않다고 판단하는 경우를 포함하여 이러한 해지의 사유와 관계 없음)<br />
									&nbsp;<br />
									d. (주)위드라인 또는 (주)위드라인에게 또는 (주)위드라인과 함께 서비스를 제공하는 (주)위드라인의 공급자나 파트너가 관련 법률에 따라 서비스나 그 일부의 제공을 종료해야 하는 경우(예를 들어 관련 법률의 변경 또는 법원의 판결이나 판정에 따라 서비스 또는 그 일부가 불법이 되거나 불법으로 간주되는 경우)<br />
									&nbsp;<br />
									3.5. 본 이용 약관의 해지는 본 이용약관의 유효 기간 중 발생하거나 초래된 고객 또는 (주)위드라인의 권리, 의무 및 책임에 아무런 영향을 미치지 않습니다.<br />
									&nbsp;<br />
									3.6. 고객은 수정된 이용 약관에 동의하지 않거나, 고객이나 (주)위드라인에 의한 해지 등으로 본 이용 약관이 종료된 후에는 서비스를 이용할 수 없습니다.<br />
									&nbsp;<br />
									3.7. 그러나 고객이 수정된 이용 약관에 동의하지 않기로 결정하거나 3.2항에 따라 고객이 본 이용 약관을 해지한 경우 고객은 서비스를 이용하는 과정에서 (주)위드라인에 저장된 사용자 콘텐츠를 백업할 수 있습니다. 고객은 (주)위드라인이 합리적인 백업 기간(&quot;백업 허용 기간&quot;)이 경과한 후 사용자 콘텐츠를 삭제할 수 있다는 점을 이해합니다. 각 서비스 및 특정 사용자 콘텐츠 백업에 필요한 노력에 따라 다른 백업 허용 기간이 적용될 수 있습니다.<br />
									&nbsp;<br />
									&nbsp;<br />
									[고객의 서비스 이용 방법 및<br />
									(주)위드라인의 고객 콘텐츠 사용 방법 또는 고객의 (주)위드라인 콘텐츠 사용 방법]<br />
									&nbsp;<br />
									4. 서비스 제공 및 이용 제한<br />
									&nbsp;<br />
									4.1. 서비스는 (주)위드라인이 고객에게 제공합니다.<br />
									&nbsp;<br />
									4.2. 본 이용 약관에 달리 명시되지 않는 한 고객은 개인적이며 비상업적인 용도와 목적으로만 서비스를 이용할 수 있으며 모든 서비스 또는 그 일부를 가공, 복제, 복사, 판매, 거래 또는 재판매할 수 없습니다.<br />
									&nbsp;<br />
									4.3. 서비스 이용 시 고객은 항상 본 이용 약관 및 거주지 또는 서비스 이용 관할지를 포함한 관련 관할지의 관련 법률이나 규정을 준수해야 합니다.<br />
									&nbsp;<br />
									4.4. (주)위드라인은 언제든지 자신의 독자적인 판단에 따라 사전 경고 또는 통지 없이 다음을 수행할 수 있습니다.<br />
									&nbsp;<br />
									a. 서비스 변경 또는 서비스나 그 일부의 제공을 일시 중단 및/또는 종료<br />
									&nbsp;<br />
									b. 고객의 사용자 계정 및 고객의 계정에 포함된 파일 또는 기타 콘텐츠에 대한 액세스를 포함한 서비스 이용을 일시적 또는 영구적으로 비활성화 또는 중단<br />
									&nbsp;<br />
									c. 서비스를 통해 고객이 보내거나 받는 전송 횟수 또는 서비스나 그 일부를 고객에게 제공하는 데 사용되는 저장 공간의 크기 제한<br />
									&nbsp;<br />
									d. 서비스를 통해 제공되는 일부 또는 모든 콘텐츠를 사전 심사, 검토, 플래그 표시, 필터링, 수정, 거부, 거절, 액세스 차단 또는 제거<br />
									&nbsp;<br />
									4.5. (주)위드라인은 (a) 부당한 중단, 장애 또는 지연 없이 고객에게 서비스가 제공되도록 하며 (b) 서비스의 중단, 장애 또는 지연을 최소화하기 위해 상업적으로 합리적인 노력을 기울입니다.<br />
									&nbsp;<br />
									4.6. 고객은 (주)위드라인이 인터페이스를 통해 고객에게 제공하는 것 이외의 다른 서비스를 이용할 수 없으며(이용하려는 시도 포함) 자동화된 도구(소프트웨어 및/또는 하드웨어 포함), 기능, 서비스 또는 기타 방식(스크립트 또는 웹 크롤러 포함)을 통해 서비스를 이용하지 않습니다(이용하려는 시도 포함).<br />
									&nbsp;<br />
									4.7. 고객은 서비스와 관련하여 (주)위드라인이 고객에게 제공하는 모든 지침을 준수하고 따르며 서비스 또는 서비스에 연결된 모든 서버, 네트워크 또는 기타 장비에 장애나 중단을 초래할 수 있는 행위에 관여하지 않습니다.<br />
									&nbsp;<br />
									4.8. 일부 서비스는 모바일 네트워크를 통해 이용할 수 있거나 모바일 네트워크를 통해 이용할 때 특히 유용합니다. 고객은 네트워크 공급자가 네트워크 액세스, 휴대폰/모바일 장치의 네트워크 연결 기간 및 서비스 이용을 위해 사용되는 데이터 양에 대해 요금을 부과할 수 있다는 점을 알고 있어야 합니다. 이와 관련하여 서비스 이용 전에 이러한 요금이 적용될 수 있는지 네트워크 공급자에게 확인할 책임은 전적으로 고객에게 있습니다.<br />
									&nbsp;<br />
									4.9. 고객은 본 이용약관에 따라 (주)위드라인이 제공하는 서비스 또는 기능, 관련 정보 일체를 제3자의 지식재산권, 인격권 등 일체의 권리를 침해하는 방식으로 이용할 수 없으며, (주)위드라인이 제공하는 서비스 또는 기능, 관련 정보 일체를 이용하여 정당한 권한 없이 제3자가 권한을 보유하고 있는 소프트웨어 또는 콘텐츠, 기타 저작물 등에 접근하거나 이를 수정&bull;임대&bull;대여&bull;판매&bull;배포 또는 이에 기반한 파생물을 생성하거나 기타 부당하게 이용할 수 없습니다.<br />
									&nbsp;<br />
									※ 가령, (주)위드라인이 제공하는 서비스인 빅스비(Bixby)를 이용하여 제3자 서비스를 작동시키는 경우, 해당 제3자에 의해 허용된 방식인지 여부를 제3자 서비스 이용약관 등을 통해 확인해 보시고, 허용된 경우라 하더라도 제3자가 제시하는 방법 또는 기능에 부합하게 이용하여야 함을 유의하시기 바랍니다.<br />
									&nbsp;<br />
									5. 광고<br />
									&nbsp;<br />
									5.1. 고객은 (주)위드라인이 서비스의 일부로 고객에게 프로모션 목적의 광고, 프로모션 자료 또는 기타 콘텐츠 및 자료나 제품을 배치하거나 제시한다는 데 동의합니다.<br />
									&nbsp;<br />
									5.2. (주)위드라인은 고객이 서비스 등록 시점 등에 마케팅 정보 수신을 명시적으로 선택한 경우에 한해 특별한 제안, 멤버십 혜택, 뉴스레터를 보내거나, 전화 연락을 시도할 것입니다.<br />
									&nbsp;<br />
									&nbsp;
						</div>
						<input type="checkbox" id="cust_checkboxcpy" name="cust_checkboxcpy" class="input_chk"/><label for="cust_checkboxcpy"><span></span>개인정보 수집 및 이용 동의</label>
					</dd>
				</dl>

				<dl class="term">
					<dt>마케팅 수신동의</dt>
					<dd>
						<div class="term_cont">
							개인정보 수집&bull;이용 동의서<br />
							&nbsp;<br />
							㈜위드라인 (이하 &lsquo;회사&rsquo;라 합니다)는 최초 회원 가입 또는 서비스 이용 시 이용자로부터 아래와 같은 개인정보를 수집하고 있습니다.<br />
							이용자는 본 개인정보 수집&bull;이용 동의서에 따른 동의 시, &#39;필요한 최소한의 정보 외의 개인정보&#39; 수집&bull;이용에 동의하지 않을 권리가 있습니다.<br />
							개인정보 취급 및 처리에 대한 상세한 사항은 삼성비즈몰 홈페이지 (http://b2bc.samsungbizmall.com)에 공개한 &#39;개인정보 취급(처리) 방침&#39;을 참조하십시오. 다만, 본 동의서 내용과 상충되는 부분은 본 동의서의 내용이 우선합니다.<br />
							&nbsp;<br />
							1. 수집하는 개인정보 항목<br />
							① 회원가입을 위해 필요한 개인정보<br />
							ㆍID(이메일 주소/휴대폰 번호), 비밀번호, 성명, 성별, 생년월일, 국가정보, 사진(선택)<br />
							ㆍ휴대폰 번호, 중복가입확인정보(DI), 암호화된 동일인 식별정보(CI), 만 14세 아동의 경우 법정대리인 정보(법정대리인의 성명, 생년월일, CI, DI)<br />
							&nbsp;<br />
							② 각 서비스 이용을 위해 필요한 정보, 서비스 이용기록/구매 또는 AS 관련 정보<br />
							ㆍ 각 서비스 이용 기록/ 구매 또는 AS 관련 정보<br />
							ㆍ 로그데이터, 쿠키 및 웹비콘, 이용시간, 앱 설치 및 삭제 기록, 이용자가 입력한 검색어, 이용자가 업로드한 정보<br />
							ㆍ 기기 정보 (생산 및 판매 과정에서 기기에 부여된 정보 및 기기의 구동 과정에서 부여되는 정보) : IMEI, DUID, 단말모델정보, 단말OS정보, Mac Address, 접속IP정보, 시리얼번호, 단말 Software 버전, Mobile Country/Network Code, 사업자 코드 등이 포함됨<br />
							ㆍ 전화번호, 연락처 목록, 내 프로필, 수발신 전화번호, 메시지 본문, 첨부파일<br />
							ㆍ 위치정보: 서비스를 제공하기 위하여 필수적인 경우(예를 들어, &quot;내 디바이스 찾기&quot; 서비스 등)에만 별도 동의를 받아 수집됩니다.<br />
							ㆍ 음성정보: 서비스를 제공하기 위하여 필수적인 경우(예를 들어, &ldquo;S Translator&rdquo; 서비스 등)에만 별도 동의를 받아 수집됩니다.<br />
							ㆍ 삼성전자 멤버십 서비스 가입 및 이용 과정을 위해 필요한 정보 : 자세한 내용은 삼성전자 멤버십 개인정보 수집 이용 동의서에서 확인 가능합니다.<br />
							ㆍ홈페이지, 멤버십사이트, 삼성전자 스토어, 기타 이벤트진행 사이트 활동 정보 [아이디, 비밀번호, 개인식별정보, 접속로그, 이용기록, 쿠키]<br />
							&nbsp;<br />
							③ 삼성 클라우드 (삼성 클라우드 서비스를 이용하는 경우에만 수집)<br />
							ㆍ 일정, 연락처, 메시지, 갤러리, S노트, 메모, 리마인더, 인터넷브라우저/키보드/이메일의 설정 등 삼성 클라우드에 백업 또는 동기화한 내용<br />
							&nbsp;<br />
							2. 개인정보의 수집 및 이용목적<br />
							① 회원관리 및 본인확인<br />
							- 본인 확인, 연령확인, 만 14세 미만 아동 개인정보 수집 시 법정 대리인 동의여부 확인, 불만 처리 등 민원처리, 고지사항 전달, 혜택 제공 및 안내, 불량회원의 부정이용 방지와 비인가 사용방지<br />
							- 회원제 상품 구매 및 서비스 이용 시/ 개인정보 처리 및 정보주체의 개인정보 열람, 정정, 삭제, 처리정지 요구 시 본인확인, 개인식별<br />
							② 서비스 제공에 관한 계약 이행 및 요금 정산<br />
							- 상품, 서비스 및 콘텐츠의 제공, 구매 및 요금 결제, 상품 배송<br />
							- 이용자에게 최적화된 서비스 제공 및 기능 개선(이용자의 과거 이용 행태에 기반한 맞춤형 콘텐츠 및 추천 정보 등 제공)<br />
							- 이용자가 구매한 상품에 대한 AS 및 업데이트 정보 등 제공<br />
							③ 삼성어카운트를 통한 통일된 서비스 제공<br />
							- 삼성 어카운트로 연결된 다양한 기기에서 동일한 사용환경 및 기능 제공<br />
							- 삼성 어카운트 기반 다양한 서비스 이용 및 서비스 간 기본 데이터 연계[예: 내 프로필(My Profile)]<br />
							- 프로필 공유, 메시지와 첨부파일 송수신 및 간편 공유, 영상통화 등의 커뮤니케이션 기능 제공<br />
							④ 기존서비스 개선, 신규서비스 개발 및 마케팅∙광고 등에 활용<br />
							- 고객만족 조사, 상품&bull;서비스 개발연구, 신규 서비스 연구&bull;개발&bull;특화<br />
							- 이벤트∙프로모션 등 마케팅 정보 제공 및 경품 배송, 인구통계학적 특성에 따른 서비스 제공 및 광고 게재, 접속 빈도 파악 또는 회원의 상품 구매 및 서비스 이용에 대한 분석 및 통계, 서비스&bull;제품&bull;정보의 추천, 시장 트렌드 유추, 시장조사, 통계분석자료 활용<br />
							&nbsp;<br />
							3. 개인정보의 보유 및 이용기간<br />
							이용자의 개인정보는 원칙적으로 개인정보의 수집 및 이용목적이 달성되면 지체 없이 파기합니다.<br />
							다만, 회사는 관련법령의 규정에 의하여 개인정보를 보유할 필요가 있는 경우, 해당 법령에서 정한 바에 의하여 개인정보를 보유할 수 있습니다.<br />
							&nbsp;<br />
							&nbsp;<br />
							※ 귀하께서는 개인정보 수집‧이용에 대한 동의를 거부하실 수 있으나, 이상의 정보는 서비스 제공에 필수적으로 필요한 정보이므로, 동의를 거부하실 경우 회원가입, 서비스 이용 등을 하실 수 없습니다.<br />
							※ 회원가입 후 서비스 이용과정에서 필요에 따라 요청되는 정보는 서비스 이용과정에서 별도로 안내하고 동의 받도록 하겠습니다.<br />
							&nbsp;<br />
							&nbsp;
						</div>
						<input type="checkbox" id="cust_checkboxcpy3" name="cust_checkboxcpy3" class="input_chk"/><label for="cust_checkboxcpy3"><span></span>마케팅 수신동의</label>
					</dd>
				</dl>


				<dl class="term">
					<dt>제3자 정보제공</dt>
					<dd>
						<div class="term_cont">
							특별한 제안, 멤버십 혜택, 뉴스레터를 받아 보려면 이 옵션을 실행하세요.<br><br>

							* 삼성 마케팅 활동은 위탁 업체에서 담당합니다. 마케팅 정보를 받는 데 동의하면 위탁 업체에서 마케팅 정보를 받는 것에도 동의하는 것으로 간주합니다.<br><br>


							① 개인정보를 제공받는 자 : ㈜ 위드라인( 삼성전자 B2B몰 운영대행사)<br>
							② 개인정보를 제공받는 자의 개인정보 이용 목적 : 고객 관리 및 활용(마케팅)<br>
							③ 제공하는 개인정보의 항목 : 성명, 연락처, 이메일<br>
							④ 개인정보를 제공받는 자의 개인정보 보유 및 이용 기간 : 제공 후 3년<br>
							⑤ 동의를 거부할 수 있으며, 동의 거부시 서비스가 제공되지 않습니다.<br>


						</div>
						<input type="checkbox" id="cust_checkboxcpy2" name="cust_checkboxcpy2" class="input_chk"/><label for="cust_checkboxcpy2"><span></span>제3자 정보제공 동의</label>
					</dd>
				</dl>
				<div class="chk_all">
					<input type="checkbox" id="cust_checkboxcpy5" name="cust_checkboxcpy5" class="input_chk_all" onclick="javascript:check_click();"/><label for="cust_checkboxcpy5"><span></span>모두 동의하기</label>
				</div>
			<div class="btn_wrap">
				<a href="javascript:write_ok();" class="btn_agree">등록하기</a>
				<a href="javascript:div_close();" >취소</a>
			</div>
			</form>
		</div>
	</div><!-- join_layer e -->
	
	<form name="id_search_form"  method="post">
		<div class="popup_wrap popup03" id="find_id">
			<div class="box">
				<h2>아이디 찾기<button onClick="javascript:find_id_close();" id="close_x"  type="button">X</button></h2>
				<ul class="find_info">
					<li>- 회원가입시 작성하신 정보를 입력하시면 아이디를 찾을 수 있습니다.</li>
					<li>- 입력정보를 잊으신 경우 고객센터로 문의해주세요. 고객센터( T.070-7437-8145 )</li>
				</ul>
				<div class="find_box">
					<form method="post" action="">
					<div class="find_cont">
						<input type="radio" id="email" class="input_radio" title="" value="Email" name="selectCheck" checked="checked"/>
						<label for="email">이메일 주소로 찾기</label>
						<div>
							<label for="input_name">이름</label>
							<input type="text" id="input_name" class="input_txt" title="" value="" name="Email_UserName" maxlength="25"/><br/>
							<label for="input_email">이메일 주소</label>
							<input type="text" id="input_email" class="input_txt" title="" value="" name="Email" placeholder="" maxlength="40"/>
						</div>
					</div>
					<div class="find_cont">
						<input type="radio" id="number" class="input_radio" title="" value="Phone" name="selectCheck"/>
						<label for="number">전화번호로 찾기</label>
						<div>
							<label for="input_name">이름</label>
							<input type="text" id="input_name" class="input_txt" title="" value="" name="Phone_UserName"/><br/>
							<label for="input_num">일반전화/휴대폰</label>
							<input type="text" id="input_num" class="input_txt" title="" value="" name="Phone" placeholder="- 없이 입력해 주세요."/>
						</div>
					</div>
					<div class="btn_wrap">
						<a href="javascript:CheckInfo();" class="btn_agree">아이디 찾기</a>
					</div>
					</form>
				</div>
			</div>
			<div class="box end_box" style="display:none;" id="id_search_div">
				<h2>아이디 찾기 완료<button onClick="javascript:id_search_div_close();" id=""  type="button">X</button></h2>
				<p class="end_box_cont">고객님의 아이디는<br/><span id="sarch_id_span">id</span><br/>입니다.</p>
				<div class="btn_wrap">
					<a href="#" class="btn_agree" onClick="javascript:id_search_div_close();">닫기</a>
				</div>
			</div>
		</div><!-- find_id e -->
	</form>
	<form name="pwd_search_form" method="post">
		<div class="popup_wrap popup03" id="find_pw">
			<div class="box">
				<h2>비밀번호 찾기<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
				<ul class="find_info">
					<li>- 회원가입시 작성하신 아이디와 이메일 주소로 비밀번호가 발송됩니다.</li>
					<li>- 비밀번호로 로그인하신 후 마이페이지 > 회원정보 페이지에서 비밀번호를 변경하시기 바랍니다.</li>
					<li>- 등록되어 있는 이메일 주소와 일치하지 않을 경우 고객센터로 문의해주세요. ( T.070-7437-8145 )</li>
				</ul>
				<div class="find_box">
					<form method="post" action="">
					<div class="find_cont">
						<label for="input_id">아이디</label>
						<input type="text" id="input_id" class="" title="" value="" name="PWD_UserID"/><br/>
						<label for="input_email">이메일 주소</label>
						<input type="text" id="input_email" class="" title="" value="" name="PWD_UserEmail" placeholder=""/>
					</div>
					<div class="btn_wrap">
						<a href="javascript:CheckEmail();" class="btn_agree" style="width:auto;padding:10px;">비밀번호 이메일 발송</a>
					</div>
					</form>
				</div>
			</div>
			<div class="box end_box" style="display:none;" id="pwd_sarch_div">
				<h2>비밀번호 찾기완료<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
				<p class="end_box_cont"><span id="sarch_pwdid_span">id</span>님의 비밀번호가<br/><span id="sarch_pwd_span">id@nate.com</span>으로<br/> 발송되었습니다.</p>
				<p class="end_box_info">발송 된 비밀번호로 로그인 후<br/>"마이페이지 > 회원정보" 페이지에서<br/>비밀번호를 변경해주세요.</p>
				<div class="btn_wrap">
					<a href="#" class="btn_agree" onClick="javascript:find_pw_close();">닫기</a>
				</div>
			</div>
		</div><!-- find_pw e -->
	</form>
</div>
</body>
</html>

<script language="javascript" type="text/javascript">
	function pwd_sarch_div_close(){	
		document.getElementById("pwd_sarch_div").style.display ='none'
	}
	function div_close(){
		document.getElementById("join_layer").style.display ='none'
	}
	function div_open(){
		document.getElementById("join_layer").style.display ='block'
	}
	function find_id_close(){
		document.getElementById("find_id").style.display ='none'
	}
	function find_id_open(){
		document.getElementById("id_search_div").style.display ='none' 
		document.getElementById("find_id").style.display ='block'
	}
	function find_pw_close(){
		document.getElementById("find_pw").style.display ='none'
	}
	function find_pw_open(){
		document.getElementById("find_pw").style.display ='block'
	}

	function id_search_div_close(){
		document.getElementById("id_search_div").style.display ='none' 
		return;
	}
</script>

<!--팝업1-->
<!--div id="popup01" class="popup_cont" style="border:1px solid #555;">
	<a href="javascript:;" onclick="onCommonOs();"><img src="/front/img/popup/popup_email.jpg" usemap="#pop"/></a>
	<div class="btn_close">
		<button type="button" onclick="todaycloseWin();">오늘 하루 열지 않기</button>
		<button type="button" onclick="closeWin();">닫기</button>
	</div>
</div>
<script language="javascript" type="text/javascript">
	$(function(){
		/*팝업1*/
		cookiedata = document.cookie; 
		if ( cookiedata.indexOf("ncookie=done") < 0 ){ 
			document.getElementById('popup01').style.display = "block";
		} else {
			document.getElementById('popup01').style.display = "none"; 
		}
	});
	/*팝업1*/
	function closeWin(){
	document.getElementById('popup01').style.display = "none";
	}
	function todaycloseWin(){
	setCookie( "ncookie", "done" , 24 ); 
	document.getElementById('popup01').style.display = "none";
	}

	function onCommonOs() { // 함수처리
		var userAgent = navigator.userAgent;
		if (userAgent.match(/iPhone|iPad|iPod/)) { // ios
			var appstoreUrl = "http://itunes.apple.com/kr/app/id393499958?mt=8";
			var clickedAt = +new Date;
			location.href = appstoreUrl; 
		} else { // 안드로이드
			location.href = "Intent://#Intent;scheme=naversearchapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nhn.android.search;end"
		}
	}
	//쿠키정보 저장
	function setCookie( name, value, expirehours ) { 
		var todayDate = new Date(); 
		todayDate.setHours( todayDate.getHours() + expirehours ); 
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
	} 
	//쿠키 정보 가지고 오기
	function getCookie(Name) {
		var search = Name + "="
		if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
		offset = document.cookie.indexOf(search)
		if (offset != -1){// 쿠키가 존재하면
			offset += search.length
			// set index of beginning of value
			end = document.cookie.indexOf(";", offset)
			// 쿠키 값의 마지막 위치 인덱스 번호 설정
			if (end == -1)
			end = document.cookie.length
			return unescape(document.cookie.substring(offset, end))
		}
	 }
		return "";
	}-->
</script>
</body>
</html>

