<!--#include virtual="/Library/ajax_config.asp"-->
<%
			
	NowPage = Request("NowPage")


	If NowPage = "" Then
		NowPage = "1"
	End If 

	ViewCnt = Request("ViewCnt")			
			
	If ViewCnt = "" Then 
		ViewCnt = "100"
	End If 	
	
	Order_type = Request("Order_type")
	
	'정렬순서가 존재하지 않으면 최신 등록순으로 
	If Order_type = ""  Then 
		Order_type = "date_desc" 
	End If 

	strqry4 = "	sp_b2bc_product_sale_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Order_type='"&Order_type&"' "
	strqry4 = strqry4 & "	,@GD_CATE='A' "

	'Response.write strqry4
	DBOpen()
		Set rs_product = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = 1
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1
%>
<div class="list_wrap">
	<div class="price_tab">
		<a href="javascript:product_list('<%=NowPage%>','<%=ViewCnt%>','price_asc');" <% If Order_type = "price_asc" Then %>class="active"<% End If %>>낮은가격순</a>
		<a href="javascript:product_list('<%=NowPage%>','<%=ViewCnt%>','price_desc')"<% If Order_type = "price_desc" Then %>class="active"<% End If %>>높은가격순</a>
		<a href="javascript:product_list('<%=NowPage%>','<%=ViewCnt%>','date_desc')"<% If Order_type = "date_desc" Then %>class="active"<% End If %>>최근등록순</a>
	</div>
	<div class="list_box">
		<ul>
	
		<% If rs_product.bof Or rs_product.eof Then %>
		<% Else %>
		<% 
			Do Until rs_product.EOF 
			
			PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
		%>
		<li>
			<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
		
			<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
			<div class="product_info ss_order_mall">
				<dl>
					<dt class="name"><%=rs_product("GD_NM")%></dt>
					<dd class="price">
<!-- 						<span><em>소비자가</em><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span> -->
						<span>
							<strike>표시가 : <%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike><br/>
							<b>혜택가 : <%= FormatNumber(rs_product("GD_PRICE3"),0)%>원</b>
							<!--b class="percent"><%=PERCENT%>%</b-->
							<!--strike>표시가 : 0원</strike><br/>
							<b>혜택가 : 0원</b>
							<!--strike>표시가 : 0원</strike><br/>
							<b>혜택가 : 0원</b-->
						</span>
					</dd>
				</dl>
			</div><!-- product_info e -->
			</a>
		</li>
		<%
			rs_product.MoveNext
			x = x + 1
			Loop
			rs_product.Close
			Set rs_product = Nothing
			End If 
		%>
		</ul>
	</div><!-- list_box e -->            
</div><!-- list_wrap e -->