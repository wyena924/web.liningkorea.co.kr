<!--#include virtual="/Library/ajax_config.asp"-->
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="now">
<meta http-equiv="Expires" content=0>
<%
			
	NowPage = Request("NowPage")


	If NowPage = "" Then
		NowPage = "1"
	End If 

	ViewCnt = Request("ViewCnt")			
			
	If ViewCnt = "" Then 
		ViewCnt = "1"
	End If 	
	
	Order_type = Request("Order_type")

	'Response.write Order_type
	'정렬순서가 존재하지 않으면 최신 등록순으로 
	If Order_type = ""  Then 
		Order_type = "date_desc" 
	End If 

	strqry4 = "	sp_pmaa_product_academi_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Order_type='"&Order_type&"' "

	DBOpen()
		Set rs_product = Dbcon.Execute(strqry4)
	DBClose()

	strqry4 = "		sp_pmaa_product_academi_cnt  "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	'Response.write strqry4
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1	
%>
<% If rs_product.bof Or rs_product.eof Then %>
<div class="list_wrap">
	<div class="price_tab">
		<div class="list_box" style="float:left;">
			검색된 상품이 없습니다.
		</div>
	</div>

<% Else %>
<div class="list_wrap">
	<div class="price_tab">
		<a href="/product/academi_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=price_asc" <% If Order_type = "price_asc" Then %>class="active"<% End If %>>낮은가격순</a>
		<a href="/product/academi_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=price_desc" <% If Order_type = "price_desc" Then %>class="active"<% End If %>>높은가격순</a>
		<a href="/product/academi_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=date_desc"<% If Order_type = "date_desc" Then %>class="active"<% End If %>>최근등록순</a>
	</div>
	<div class="list_box">
		<ul>
		<% 
			Do Until rs_product.EOF 
			
			PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
		%>
		<li>
			<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
				<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
				<div class="product_info ss_order_mall">
					<dl>
						<dt class="name"><%=rs_product("GD_NM")%></dt>
						<dd class="price">
<!-- 											<span><em>소비자가</em><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span> -->
							<span>
								<strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike><br/>
								<b><%= FormatNumber(rs_product("GD_PRICE3"),0)%>원</b>
								<b class="percent"><%=PERCENT%>%</b>
							</span>
						</dd>
					</dl>
				</div><!-- product_info e -->
				<span class="mark"><b><%=PERCENT%></b>%<br/>SALE</span>
			</a>
		</li>
		<%
			rs_product.MoveNext
			x = x + 1
			Loop
			rs_product.Close
			Set rs_product = Nothing
			
		%>
		</ul>
	</div><!-- list_box e -->
	<%=PT_Ajax_PageLink(NowPage,ViewCnt,Order_type,"academi_list.asp")%>                 
	<!--div class="paging_wrap">
		<div class="paging">
			<a href="#" class="btn_paging prev"><img src="/front/img/common/icon_paging_prev.png" alt=""/></a>
			<a href="#" class="active">1</a>
			<a href="#">2</a>
			<a href="#">3</a>
			<a href="#">4</a>
			<a href="#">5</a>
			<a href="#" class="btn_paging next"><img src="/front/img/common/icon_paging_next.png" alt=""/></a>
		</div>
	</div-->
</div><!-- list_wrap e -->
<%
	End If 
%>