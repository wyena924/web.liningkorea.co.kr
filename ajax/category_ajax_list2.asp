<!--#include virtual="/Library/ajax_config.asp"-->
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="now">
<meta http-equiv="Expires" content=0>
<%
	SearchText  = Request("SearchText")
	NowPage	    = Request("NowPage")
	Order_type	= Request("Order_type")
	GD_GRP_MID	= Request("GD_GRP_MID")
	GD_GRP_DETL	= Request("GD_GRP_DETL")
	GD_GRP_DETL2= Request("GD_GRP_DETL2")
	
	If NowPage = "" Then
		NowPage = "1"
	End If 

	ViewCnt = Request("ViewCnt")			
			
	If ViewCnt = "" Then 
		ViewCnt = "9"
	End If 
	
	If GD_GRP_MID = "006" Then 
		Order_type = ""
	End If 


	strqry4 = "	sp_sangpan_product_category_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Order_type='"&Order_type&"' "
	strqry4 = strqry4 & "	,@GD_GRP_MID='"&GD_GRP_MID&"' "
	strqry4 = strqry4 & "	,@GD_GRP_DETL='"&GD_GRP_DETL&"' "
	strqry4 = strqry4 & "	,@GD_GRP_DETL2='"&GD_GRP_DETL2&"' "
	'Response.write strqry4
	DBOpen()
		Set rs_product = Dbcon.Execute(strqry4)
	DBClose()

	strqry4 = "		sp_sangpan_product_category_cnt "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@GD_GRP_MID='"&GD_GRP_MID&"' "
	strqry4 = strqry4 & "	,@GD_GRP_DETL='"&GD_GRP_DETL&"' "
	strqry4 = strqry4 & "	,@GD_GRP_DETL2='"&GD_GRP_DETL2&"' "

	'Response.write strqry4
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1

	'Response.write "<br><br>"&Order_type
%>
<% If rs_product.bof Or rs_product.eof Then %>
<div class="list_wrap">
	<div class="price_tab">
		<div class="list_box" style="float:left;">
			검색된 데이터가 존재하지 않습니다.
		</div>
	</div>

<% Else %>
<div class="list_wrap">
	<div class="price_tab">

		<a href="/product/category_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=price_asc&gd_grp_mid=<%=gd_grp_mid%>&gd_grp_detl=<%=gd_grp_detl%>&gd_grp_detl2=<%=gd_grp_detl2%>" <% If order_type = "price_asc" Then %>class="active"<% End If %>>낮은가격순</a>
		<a href="/product/category_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=price_desc&gd_grp_mid=<%=gd_grp_mid%>&gd_grp_detl=<%=gd_grp_detl%>&gd_grp_detl2=<%=gd_grp_detl2%>" <% If order_type = "price_desc" Then %>class="active"<% End If %>>높은가격순</a>
		<a href="/product/category_list.asp?NowPage=<%=NowPage%>&ViewCnt=<%=ViewCnt%>&order_type=date_desc&gd_grp_mid=<%=gd_grp_mid%>&gd_grp_detl=<%=gd_grp_detl%>&gd_grp_detl2=<%=gd_grp_detl2%>"<% If order_type = "date_desc" Then %>class="active"<% End If %>>최근등록순</a>

	</div>
	<div class="list_box">
		<ul>
		<% 
			Do Until rs_product.EOF 
			
			gd_nm = ""

			PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
		%>
		<li>
				<% If Not (rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006") Then %>
					<a href="/product/detail2.asp?seq=<%=rs_product("SEQ")%>">
				<% Else %>
					<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
				<% End If %>
				<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
				<div class="product_info ss_order_mall">
					<dl>
						<dt class="name">
							<% If gd_grp_mid = "006" Then 
								
								gd_nm_split = Split(rs_product("GD_NM"),"/") 
								
								For i = 0 To ubound(gd_nm_split) 
									If i >= 1 Then 
										gd_nm = gd_nm &"/"&"<label style='font-weight:bold;'>"&gd_nm_split(i)&"</label>"
									Else
										gd_nm = gd_nm &"/"&gd_nm_split(i)
									End If 
								next
							
							%>
								<%=GD_NM%>
							<% Else %>
								<%=rs_product("GD_NM")%>
							<% End If %>
						</dt>
						<% If Not (rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006") Then %>
							<% If CLng(rs_product("GD_PRICE3")) >= 1000000 Then %>
								<dd class="test">
									<!--span>해당상품은 지정행사매장에 문의 부탁드립니다.</span-->
									<span>삼성카드 플러스페이 100만원이상 결제시 추가혜택 증정</span>
								</dd>
							<% End If %>
						<% End If %>
						<dd class="price">
<!-- 						<span><em>소비자가</em><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span> -->
							<span>
								<strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike><br/>
								<b><%= FormatNumber(rs_product("GD_PRICE3"),0)%>원</b>
								<!--b class="percent"><%=PERCENT%>%</b-->
							</span>
						</dd>

					</dl>
				</div><!-- product_info e -->
				<!--span class="mark"><b><%=PERCENT%></b>%<br/>SALE</span-->
			</a>
			<div class="mark">
				<% If (rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006") Then %>
					<span class="online">온라인<br/>구매가능</span>
				<% Else %>
					<span>매장판매<br/>전용상품</span>
				<% End If %>
			</div>
		</li>
		<%
			rs_product.MoveNext
			x = x + 1
			Loop
			rs_product.Close
			Set rs_product = Nothing
			
		%>
		</ul>
	</div><!-- list_box e -->
	<%=PT_Ajax_Category_PageLink(NowPage,ViewCnt,Order_type,gd_grp_mid,gd_grp_detl,gd_grl_detl2)%>                 
	<!--div class="paging_wrap">
		<div class="paging">
			<a href="#" class="btn_paging prev"><img src="/front/img/common/icon_paging_prev.png" alt=""/></a>
			<a href="#" class="active">1</a>
			<a href="#">2</a>
			<a href="#">3</a>
			<a href="#">4</a>
			<a href="#">5</a>
			<a href="#" class="btn_paging next"><img src="/front/img/common/icon_paging_next.png" alt=""/></a>
		</div>
	</div-->
</div><!-- list_wrap e -->
<%
	End If 
%>