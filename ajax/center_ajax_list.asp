<!--#include virtual="/Library/ajax_config.asp"-->
<%
	page = request("page")
	
	search_key = request("search_key")
	search_keyword = request("search_keyword")

	if page = "" then page = 1

	Dim page_size, block_page, total_cnt, total_page, h_number

	page_size = 10
	block_page = 10	

	goParam = "?tmp=null"

	Dbopen()

	sql = "USP_PMAA_USER_BOARD_NOTICE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size & ", @SEARCH_KEY='" & search_key & "', @SEARCH_KEYWORD='" & search_keyword &"'"
	'Response.write sql
	'Response.end
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numList = -1
	Else
		total_cnt = rs(0)
		total_page = rs(1)
			
		h_number = total_cnt - ((page -1) * page_size)
				
		arrList = rs.GetRows
		numList = Ubound(arrList,2)
	End If

	sql = "USP_PMAA_BOARD_NOTICE_VIEW @mode='TOP'"
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numTopList = -1
	Else
		arrTopList = rs.GetRows
		numTopList = Ubound(arrTopList,2)
	End If


	TotalPage=Int((CInt(numTopList)-1)/CInt(page_size)) +1


%>
<table cellpadding="0" cellspacing="0" class="t_type01" summary="">
	<colgroup>
		<col class="num"/>
		<col class="tit"/>
		<col class="name hide_s"/>
		<col class="date"/>
		<col class="see hide_s"/>
	</colgroup>
	<thead>
		<tr>
			<th scope="" class="num">번호</th>
			<th scope="" class="tit">제목</th>
			<th scope="" class="hide_s">작성자</th>
			<th scope="">작성일</th>
			<th scope="" class="hide_s">조회</th>
		</tr>
	</thead>
	<tbody>
		<%
			for i = (page-1) * page_size To numList
			  seq			= arrList(2,i)
			  subject		= arrList(3,i)
			  writer		= arrList(4,i)
			  content		= arrList(5,i)
			  filename		= arrList(6,i)
			  isview		= arrList(7,i)
			  hit			= arrList(8,i)
			  create_date	= arrList(9,i)
		%>
		<tr class="first">
			<td class="num"><%=seq%></td>
			<td class="tit"><a href="center_view.asp?seq=<%=seq%>"><%=subject%></a></td>
			<td class="hide_s"><%=writer%></td>
			<td><%=create_date%></td>
			<td class="hide_s"><%=hit%></td>
		</tr>
		<%
			h_number = h_number - 1
			next
		%>	
	</tbody>
</table>
<%=PT_center_Ajax_PageLink(page,page_size,"paging")%>