<!--#include virtual="/Library/ajax_config.asp"-->
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Expires" content="now">
<meta http-equiv="Expires" content=0>
<%
			
	NowPage			= Request("NowPage")
	ViewCnt			= Request("ViewCnt")	
	Order_type		= Request("Order_type")
	Search_key		= Request("Search_key")
	Search_keyWord	= Request("Search_keyWord")
	user_seq		= GetsCUSTSEQ()

	'Response.write user_seq
	If NowPage = "" Then
		NowPage = "1"
	End If 

			
	If ViewCnt = "" Then 
		ViewCnt = "10"
	End If 	
	
	
	'정렬순서가 존재하지 않으면 최신 등록순으로 
	If Order_type = ""  Then 
		Order_type = "date_desc" 
	End If 

	strqry4 = "	sp_mall_delivery_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Search_key='"&Search_key&"' "
	strqry4 = strqry4 & "	,@Search_keyword='"&Search_keyWord&"' "
	strqry4 = strqry4 & "	,@user_seq='"&user_seq&"' "
	strqry4 = strqry4 & "	,@OR_WRITE_ID='"&GLOBAL_VAR_PEID&"' "
	
	'Response.write strqry4 &"<br>"
	DBOpen()
		Set rs = Dbcon.Execute(strqry4)
	DBClose()

	strqry4 = "	sp_mall_delivery_cnt "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Search_key='"&Search_key&"' "
	strqry4 = strqry4 & "	,@Search_keyword='"&Search_keyWord&"' "
	strqry4 = strqry4 & "	,@user_seq='"&user_seq&"' "
	strqry4 = strqry4 & "	,@OR_WRITE_ID='"&GLOBAL_VAR_PEID&"' "
	'Response.write strqry4
	'Response.End 
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1

	'Response.write TotalPage
%>
<% If rs.bof Or rs.eof Then %>
<div class="list_wrap" >
		<div class="list_box" >
			<div class="delivery_list">
				<div class="non_search">
					주문/배송조회 데이터가 없습니다.
				</div>
			</div><!-- delivery_list -->
		</div><!-- list_box e -->
	</div>
<% Else %>
	<div class="list_wrap" >
		<div class="list_box" >
			<div class="delivery_list">
				<ol class="tit delivery_table">
					<li class="date">날짜<br>[주문번호]</li>
					<li class="goods">구매내역</li>
					<li class="state">상태</li>
				</ol><!-- tit e -->
				<div class="list">

					<% 
						Do Until rs.EOF 
					%>
					<a href="#" class="delivery_table">
						<div class="date">
							<span><%=rs("or_dt")%></span>
							<span><%=rs("or_num")%></span>
						</div>
						<div class="goods" onclick="javascript:order_ok('delivery_detail.asp?or_num=<%=rs("or_num")%>','1');">
							<span class="name"><%=HLeft(rs("gd_nm"),20)%> <b>
								<%If rs("gd_cnt") > 1  Then%>
									외 <%=rs("gd_cnt") %>개
								<% End If %>
							</b></span>
							<span class="price"><b><%=FormatNumber(rs("gd_price") ,0)%></b>원</span>
							<span class="etc">수령인 : <%=rs("or_in_pe2")%> / 연락처 : <%=rs("or_in_tel")%></span>
							<!--span class="name">갤럭시 워치 46 mm (블루투스)... <b>외 2개</b></span>
							<span class="price"><b>2,602,700</b>원</span>
							<span class="etc">수령인 : 김동현 / 연락처 : 010-1234-5678</span-->
						</div>
						<div class="state">
							<%

								CancleCount = 0

							  '주문상태가 미확정일때
							 ' If rs("OR_STATE") = "00" Then

									'해당내역이 출고반품된적 있는지 확인
									CancleSQL = "SELECT COUNT(*) AS CancleCount"
									CancleSQL = CancleSQL & " FROM IC_T_ORDER_GD "
									CancleSQL = CancleSQL & " Where DEL_YN = 'N'"
									CancleSQL = CancleSQL & " AND CNCL_ORDER_GD_SEQ = '" & rs("SEQ") & "'"

									DBopen()
									Set CancleRs = Dbcon.Execute(CancleSQL)
									DBclose()
									If CancleRs.Eof Or CancleRs.Bof Then
										CancleCount = 0
									Else
										CancleCount = CInt(CancleRs("CancleCount"))
									End If

									'출고반품 된적이 없고 AND (해당내역이 출고반품내역이 아니라면: 출고반품아닌내역은 0 이나 NUll값임)
									If Cint(CancleCount) > 0 Or rs("CNCL_ORDER_GD_SEQ") <> "0" Then
							%>
									<span>주문취소</span>
							<%
									Else
							%>
									<span><%=rs("or_state_nm")%>
									
										<br>
												
										<% If rs("gd_memo") <> "" Then %>
											<span onclick="javascript:order_ok('<%=replace(rs("GD_Memo"),"＆","&") %>','2'); return;">[가입신청서]</span>
										<% End If %>
									</span>
							<%
									End If
								'End If
							%>
							
						</div>
					</a><!-- delivery_table 1 e -->
					<%
						rs.MoveNext
						x = x + 1
						Loop
						rs.Close
						Set rs = Nothing 
					%>
				</div><!-- list e -->
			</div><!-- delivery_list -->
		</div><!-- list_box e -->
		<%=PT_Order_Ajax_PageLink(NowPage,ViewCnt,"paging")%>
	</div>
<% End If %>