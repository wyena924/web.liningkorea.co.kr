<!-- #include virtual = "/include/head.asp" -->
<!-- #include virtual = "/include/header.asp" -->
<%
page			= request("page")
branch_group	= request("branch_group")
branch_name		= request("branch_name")
branch_addr		= request("branch_addr")
branch_tel		= request("branch_tel")		


NowPage = request("page")


if page = "" then page = 1

if NowPage = "" then NowPage = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 20

goParam = "branch_dev.asp?tmp=null"

If ViewCnt = "" Then 
	ViewCnt = "20"
End If 

Dbopen()
sql = "USP_MALL_ADMIN_BRANCH_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size &" ,@branch_group= '"&branch_group&"',@branch_name='"&branch_name&"',@branch_addr='"&branch_addr&"',@branch_tel='"&branch_tel&"'"

'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	'TotalPage = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)

	TotalPage=Int((CInt(total_cnt)-1)/CInt(ViewCnt)) +1

End If
%>
<script language='javascript'>
	
	function serch() 
	{
		var branch_group = document.getElementById("branch_group").value;
		var branch_name  = document.getElementById("branch_name").value;
		
		document.bform.submit();
	}
</script>
<div class="container sub">
	<div class="list_page branch_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">행사매장</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page">
					<form name="bform" id="bform" method="post">
						<div class="sch_box">
							<div class="select_box select02">
								<label for="search_key">
									<% If branch_group <> "" Then %>
										<%= branch_group %>	
									<% Else %>
										전체
									<% End If %></label>
								<%
									sql2 = " SELECT BRANCH_GROUP "
									sql2 = sql2 & " FROM IC_T_SAMPAN_BRANCH_LIST "
									sql2 = sql2 & " GROUP BY BRANCH_GROUP order by max(seq) asc "

									Set rs2 = DBcon.Execute(sql2)

								%>
								<select id="branch_group" name="branch_group">
									<option value="" selected="selected">전체</option>
									<% If rs2.bof Or rs2.eof Then %>	
									
									<% Else %>
									<%
										Do Until rs2.EOF
									%>
									<option VALUE="<%=rs2("BRANCH_GROUP")%>" <%If rs2("BRANCH_GROUP") = branch_group Then %> selected<% End if%>><%=rs2("BRANCH_GROUP")%></option>
									<%
										rs2.MoveNext
										x = x + 1
										Loop
										rs2.Close
										Set rs2 = Nothing
										End If
									%>
								</select>
							</div><!-- select_box e -->
							<input type="text" name="branch_name" id="branch_name" class="sch_txt" placeholder="매장명을 입력해 주세요." />
							<input type="button" id="btn_sch" value="검색" onclick="javascript:serch();"/>
						</div><!-- sch_box e -->
					</form>
					<!-- 웹 화면 일때 -->
					<div class="sc_table w_table">
						<table cellpadding="0" cellspacing="0" class="t_type01" summary="">
							<colgroup>
								<col class="local"/>
								<col class="name"/>
								<col class="addr"/>
								<col class="num"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="" class="local">지역</th>
									<th scope="" class="name">매장명</th>
									<th scope="" class="addr">주소</th>
									<th scope="" class="num">매장번호</th>
								</tr>
							</thead>
							<tbody>
								<!-- 매장검색 결과가 없을 때 -->
								<!--tr>
									<td colspan="4" class="result_none">검색 결과가 없습니다.</td>
								</tr-->
								<!-- 매장검색 결과가 있을 때 -->
								<%
									for i = (page-1) * page_size To numList
									  seq		    = arrList(2,i)
									  branch_group	= arrList(3,i)
									  branch_name	= arrList(4,i)
									  branch_addr	= arrList(5,i)
									  branch_tel	= arrList(6,i)
								%>
								<tr>
									<td><%=branch_group%></td>
									<td><%=branch_name%></td>
									<td><%=branch_addr%></td>
									<td><%=branch_tel%></td>
								</tr>
								<%

									h_number = h_number - 1

									next
								%>
							</tbody>
						</table>
					</div><!-- sc_table e -->
					
					<!-- 모바일 화면 일때 -->
					<div class="m_table">
						<ul>
							<!-- 매장검색 결과가 없을 때 -->
							<li class="result_none">검색 결과가 없습니다.</li>
							<!-- 매장검색 결과가 있을 때 -->
							<li>
								<div class="first">
									<span class="place">서울</span>
									<span class="shop">위드라인</span>
								</div>
								<div class="addr">서울 마포구 삼개로 16 근신빌딩 본관 501호(도화동 250-4)서울 마포구 삼개로 16 근신빌딩 본관 501호</div>
								<div class="tel">02-715-0424</div>
							</li>
						</ul>
					</div><!-- m_table e -->

					<!-- 과장님! 페이징은 5개씩 노출되는게 좋을것 같아요~ -->
					<%=PT_PageLink2("branch_dev.asp",ViewCnt)%>                 
				</div><!-- sc_page e -->
			</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
</form>
<script language="javascript" type="text/javascript">
	//category
	$('.branch_cate a').click(function(e){
		e.preventDefault();

		$('.branch_cate a').each(function(i){
			$(this).removeClass('active');
		});
		$(this).addClass('active');

		var category = $(this).attr('title');
		$('.branch_cont dl').hide();
		if( category == 'all'){
			$('.branch_cont dl').show();
		}else{
			$('.branch_cont dl[class*='+category+']').show();
		}
	});

</script>
<!-- #include virtual = "/include/footer.asp" -->
