<!-- #include virtual = "/include/head.asp" -->
<!-- #include virtual = "/include/header.asp" -->

<div class="container sub">
	<div class="list_page etc_page branch_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<div class="branch_cate">
					<a href="#" title="all" class="active">[전체보기]</a>
					<a href="#" title="a" id="active1">[서울 강북]</a>
					<a href="#" title="b">[서울 강남]</a>
					<a href="#" title="c">[서울 강서]</a>
					<a href="#" title="d">[경기북부]</a>
					<a href="#" title="e">[경기서부]</a>
					<a href="#" title="f">[경기남부]</a>
					<a href="#" title="g">[인천]</a>
					<a href="#" title="h">[충북]</a>
					<a href="#" title="i">[충남]</a>
					<a href="#" title="j">[대전]</a>
					<a href="#" title="k">[강원]</a>
					<a href="#" title="l">[전북]</a>
					<a href="#" title="m">[전남]</a>
					<a href="#" title="n">[광주]</a>
					<a href="#" title="o">[경북]</a>
					<a href="#" title="p">[경남]</a>
					<a href="#" title="q">[대구]</a>
					<a href="#" title="r">[울산]</a>
					<a href="#" title="s">[부산]</a>
					<a href="#" title="t">[제주]</a>
				</div><!-- branch_tab e -->
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="branch_cont">
					<dl class="a">
						<img src="/front/img/branch/a.jpg" style="width:100%;">
					</dl>
					
					<dl class="b">
						<img src="/front/img/branch/a.jpg" style="width:100%;">
					</dl>

					<dl class="c">
						<img src="/front/img/branch/c.jpg" style="width:100%;">
					</dl>
					<dl class="d">
						<img src="/front/img/branch/d.jpg" style="width:100%;">
					</dl>
					<dl class="e">
						<img src="/front/img/branch/e.jpg" style="width:100%;">
					</dl>
					<dl class="f">
						<img src="/front/img/branch/f.jpg" style="width:100%;">
					</dl>
					<dl class="g">
						<img src="/front/img/branch/g.jpg" style="width:100%;">
					</dl>
					<dl class="h">
						<img src="/front/img/branch/h.jpg" style="width:100%;">
					</dl>
					<dl class="i">
						<img src="/front/img/branch/i.jpg" style="width:100%;">
					</dl>
					<dl class="j">
						<img src="/front/img/branch/j.jpg" style="width:100%;">
					</dl>
					<dl class="k">
						<img src="/front/img/branch/k.jpg" style="width:100%;">
					</dl>
					<dl class="l">
						<img src="/front/img/branch/l.jpg" style="width:100%;">
					</dl>
					<dl class="m">
						<img src="/front/img/branch/m.jpg" style="width:100%;">
					</dl>
					<dl class="n">
						<img src="/front/img/branch/n.jpg" style="width:100%;">
					</dl>
					<dl class="o">
						<img src="/front/img/branch/o.jpg" style="width:100%;">
					</dl>
					<dl class="p">
						<img src="/front/img/branch/p.jpg" style="width:100%;">
					</dl>
					<dl class="q">
						<img src="/front/img/branch/q.jpg" style="width:100%;">
					</dl>
					<dl class="r">
						<img src="/front/img/branch/r.jpg" style="width:100%;">
					</dl>
					<dl class="s">
						<img src="/front/img/branch/s.jpg" style="width:100%;">
					</dl>
					<dl class="t">
						<img src="/front/img/branch/t.jpg" style="width:100%;">
					</dl>

				</div><!-- branch_cont e -->
				</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
<script language="javascript" type="text/javascript">
	//category
	$('.branch_cate a').click(function(e){
		e.preventDefault();

		$('.branch_cate a').each(function(i){
			$(this).removeClass('active');
		});
		$(this).addClass('active');

		var category = $(this).attr('title');
		$('.branch_cont dl').hide();
		if( category == 'all'){
			$('.branch_cont dl').show();
		}else{
			$('.branch_cont dl[class*='+category+']').show();
		}
	});

</script>
<!-- #include virtual = "/include/footer.asp" -->
