<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	redirect_url = Request.ServerVariables("HTTP_REFERER") 
%>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>
	
	function email_ch(thisis)
	{
		document.getElementById("email2").value = thisis.value

	}
  function execDaumPostCode(input_type) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }
                // 우편번호와 주소 정보를 해당 필드에 넣는다.

				if(data.userSelectedType == 'R')
				{
					document.getElementById("cust_zip").value = data.zonecode
					
					document.getElementById("cust_addr").value = fullRoadAddr;					
				}
				 
				else if(data.userSelectedType == 'J'){

					 if (data.postcode1 =='')
						 {
							document.getElementById("cust_zip").value = data.zonecode
						 }
						 else
						 {
							document.getElementById("cust_zip").value = data.postcode1 + data.postcode2
						 }
					
					document.getElementById("cust_addr").value = data.jibunAddress;
				}


				document.getElementById("cust_addr2").focus()			

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
    //                document.getElementById("guide").innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
  //                  document.getElementById("guide").innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById("guide").innerHTML = '';
                }
            }
        }).open();
	}



	
	function chk_login(){
		//alert('서버 점검중입니다. 10시35부터 ~ 11시5분까지');
		//return;
		var f = document.login_frm;
			 var strAjaxUrl="/ajax/ajax_login_ch.asp"
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'POST',
					 url: strAjaxUrl,
					 dataType: 'html',
					 data: {
						login_id:f.get_login_id.value,
						online_pw:f.online_pw.value
					},				
					 success: function(retDATA) {
						if(retDATA)
							{
								if (retDATA =='no_id')
								{
									alert('없는 아이디 입니다');
									return;
								}
								else if (retDATA == 0 )
								{
									alert('비밀번호가 일치하지 않습니다.');
									return;
								}
								else
								{
									
										if(f.get_login_id.value==""){
										alert("아이디를 입력해 주세요.");
										f.get_login_id.focus();
										return false;
										}
										if(f.online_pw.value==""){
											alert("비밀번호를 입력해 주세요.");
											f.online_pw.focus();
											return false;
										}	
										f.action="login_ok2.asp?location_gb=Y";
										//f.target = "iSQL";
										f.submit();
								}
							}
					 }
			 }); //close $.ajax(
	}


	function enter_key()
	{
		if (window.event.keyCode == 13) {
	 
				 chk_login()
			}
	}

</script>
<input type="hidden" name="id_ok" id="id_ok">
	<div class="container sub">
		<div class="page_tit">
			<h2>로그인</h2>
		</div><!-- page_tit e -->
		<div class="login_page sub_bg">
			<div class="bottom_wrap">
				<div class="login_wrap">
					<form method="post" name="login_frm" id="login_frm" action="">
						
						<input type="hidden" id="redirect_url" name="redirect_url" value="<%=redirect_url%>">
						<input type="hidden" id="id_ch_ok" value="N">
						<div class="login_form">
							<div class="list_wrap">
								<h1 class="search_category_tit2"><font color="#1428a0">온라인구매몰 이용시 별도 회원가입이 필요합니다.</font></h1>
							</div><!-- list_wrap e -->
							<br>
							<div class="login_box">
								<label for="input_id" class="hide">id</label>
								<input type="text" id="get_login_id" name="get_login_id" class="ip_login" placeholder="아이디/이메일주소" onkeypress="javascript:enter_key();"/>
								<label for="input_pw" class="hide">password</label>
								<input type="password" id="online_pw" name="online_pw" class="ip_login" placeholder="비밀번호" onkeypress="javascript:enter_key();"/>
								<input type="button" class="btn_login" onclick="javascript:chk_login();" style="height:110px;" value="로그인">
							<div class="find_wrap">
								<p>로그인 정보를 잊으셨나요?</p>
								<a href="javascript:find_id_open();">아이디찾기</a><a href="javascript:find_pw_open();">비밀번호찾기</a><a href="javascript:div_open();">회원가입</a>
							</div>
						</div>
					</form>
				</div>
			</div><!-- bottom_wrap e -->
		</div><!-- list_page e -->
	</div><!-- list_page e -->
</div>

<style type="text/css">
</style>
<!-- #include virtual = "include/footer.asp" -->
