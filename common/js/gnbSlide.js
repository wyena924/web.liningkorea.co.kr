(function($){
	function SlideGnb(){
		this._$nav = null;

		this._$officeBtn = null;
		this._$officeSubMenu = null;

		this._$navBtn = null;
		this._$navOtherBtn = null;
		// this._$selectNavBtn = null;

		this._$subMenu = null;
		this._$subMenuBg = null;
		this._$subMenuList = null;
		this._$subMenuTitle = null;
		this._$subMenuTitleAll = null;

		this._$officeSubMenuList = null;
		this._$officeSubMenuTitle = null;

		this._$gnbTotalSubMenu = null;

		this._$subMenuH = [];

		this._init();
		this._initEvent();
		this._bgH = this._pushHeight();
	};

	SlideGnb.prototype._init = function() {
		this._$nav = $('nav');
		this._$officeBtn = $('.office-list > a');
		this._$officeSubMenu = $('.office-sub-menu');
		this._$officeSubDl = this._$officeSubMenu.find('dl');
		this._$navBtn = $('#nav2 li > a');
		this._$navOtherBtn = $('#nav2 li').not('.office-list').find('a');
		this._$subMenu = this._$navBtn.next();
		this._$subMenuBg = $('<div class="sub-menu-bg"></div>');
		
		this._$subMenuList = this._$nav.find('li').find('dl').filter('.cbp-hssubmenu');
		this._$subMenuTitle = this._$nav.find('li').not('.office-list').find('dl dt a');

		this._$subMenuTitleAll = this._$nav.find('li').children().not('.office-sub-menu').find('dt a');

		this._$officeSubMenuList = $('.office-sub-menu').find('dl');
		this._$officeSubMenuTitle = $('.office-sub-menu').find('dt a');

		this._$gnbTotalSubMenu = $('#nav2 dl dd a');

		this._initSubBg();
	};

	SlideGnb.prototype._initEvent = function() {
		var that = this;
		this._$navOtherBtn.bind('mouseenter', this._$navBtn, function(e){
			that._createSubBg(that._bgH);
			// that._showSubMenu();
			// that._hideOfficeMenu();
			e.preventDefault();
		});
		this._$nav.bind('mouseleave', function(e){
			that._delSubBg();
			that._hideSubMenu();
			that._hideOfficeMenu();
			that._removeTriangle();
			e.preventDefault();
		});
		this._$navBtn.bind('mouseenter', function(e){
			var index = that._$navBtn.index(this);
			that._markTitle(index);
			// 사무탕비 카테고리 있을 때 하단 ▼ 표시
			// that._markTriangle(index);
			e.preventDefault();
		});
		this._$subMenuList.bind('mouseenter', function(e){
			var index = that._$subMenuList.index(this);
			that._markTitle(index);
			e.preventDefault();
		});
		this._$officeBtn.bind('mouseenter', function(e){
			// that._createSubBg();
			that._createOfficeSubBg(that._bgH);
			// that._hideSubMenu();
			// that._showOfficeMenu();
			e.preventDefault();
		});
		/* 첫번째 메뉴 > 하위 메뉴 오버시 title에 붉은 색 표시 */
		this._$officeSubMenuList.bind('mouseenter', function(e){
			var index = $(this).index();
			that._redyTitle(index);
		});
		/* 전체 소 카테고리 */
		this._$gnbTotalSubMenu.bind('mouseenter', function(e){
			that._$gnbTotalSubMenu.css({
				color: '#fff'
			})
			$(this).css({
				// color: '#eb347e'
				color: '#ffc600'
			})
		});
	};

	SlideGnb.prototype._markTriangle = function(index) {
		// 메인 메뉴 밑에 모두 삼각형 표시
		// if (this._$selectNavBtn)
		// 	this._$selectNavBtn.removeClass('on');
		// this._$selectNavBtn = this._$navBtn.eq(index);
		// this._$selectNavBtn.addClass('on');

		if (index == 0){
			this._$navBtn.eq(index).addClass('on');
		} else {
			this._$navBtn.removeClass('on');
		}
	};

	SlideGnb.prototype._removeTriangle = function(index) {
		this._$navBtn.removeClass('on');
	};

	SlideGnb.prototype._markTitle = function(index) {
		this._$subMenuTitleAll.css({
			'color': '#fff',
			'border-bottom': '1px solid #fff'
		}).removeClass('active');
		this._$subMenuTitleAll.eq(index).css({
			'color': '#f54f62',
			'border-bottom': '1px solid #f54f62'
		}).addClass('active');
	};

	SlideGnb.prototype._initSubBg = function() {
		this._$nav.append(this._$subMenuBg);
		
		this._$subMenuBg.css({
			'position': 'absolute',
			'top': 0,
			'left': 0,
			'background-color': 'rgba(63, 93, 61, 0.9)',
			'width' : '100%',
			'height' : 0
		});
	};

	SlideGnb.prototype._pushHeight = function() {
		for (var i = 0; i < this._$subMenuList.length; i++) {
			this._$subMenuH.push(this._$subMenuList.eq(i).height());
		};
		var subHListArr = this._$subMenuH.sort(function(a,b){return a-b;});
		var last = subHListArr.length-1;
		var bgH = this._$subMenuH[last];
		return bgH
	};

	SlideGnb.prototype._createSubBg = function(bgH) {
		var that = this;
		this._$subMenuBg.stop().animate({
			'height': bgH+100
		}, 150, function(){
			that._showSubMenu();
			that._hideOfficeMenu();
		});
		this._$subMenuList.css({
			'height': bgH
		});
		this._$officeSubDl.css({
			'height': bgH + 20
		});
	};

	SlideGnb.prototype._createOfficeSubBg = function(bgH) {
		var that = this;
		this._$subMenuBg.stop().animate({
			'height': bgH+100
		}, 150, function(){
			that._hideSubMenu();
			that._showOfficeMenu();
		});
		this._$subMenuList.css({
			'height': bgH
		});
		this._$officeSubDl.css({
			'height': bgH + 20
		});
	};

	SlideGnb.prototype._delSubBg = function() {
		this._$subMenuBg.stop().animate({
			'height': 0
		});
	};

	SlideGnb.prototype._showSubMenu = function() {
		this._$subMenuList.css({
			'display': 'block'
		});
		this._$subMenuList.stop().animate({
			'opacity': 1
		}, 100);
	};

	SlideGnb.prototype._hideSubMenu = function() {
		this._$subMenuList.stop().animate({
			'opacity': 0
		}, 100);
		this._$subMenuList.css({
			'display': 'none'
		});
	};

	SlideGnb.prototype._showOfficeMenu = function() {
		this._$officeSubMenu.css({
			'display': 'block'
		}).stop().animate({
			'opacity': 1
		}, 100);
	};

	SlideGnb.prototype._hideOfficeMenu = function() {
		this._$officeSubMenu.stop().animate({
			'opacity': 0
		}, 100).css({
			'display': 'none'
		});
	};

	SlideGnb.prototype._redyTitle = function(index) {
		this._$officeSubMenuTitle.css({
			color: '#fff'
		});
		this._$officeSubMenuTitle.eq(index).css({
			color: '#f54f62'
			// color: '#eb347e'
			// color: 'rgba(245, 79, 98, 1)'
		});
		this._$gnbTotalSubMenu.css({
			color: '#fff'
		})
	};

	$.fn.slideGnb = function(){
		this.each(function(){
			var slideGnb = new SlideGnb();
		});
		return this;
	};
	$('nav').slideGnb();
})(jQuery);