<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<style type="text/css">

.popup_wrap .end_box{left:50%;}
.popup_wrap .end_box span{color:#027cc5;}
.popup_wrap .end_box p{text-align:center;padding:50px 0 0;line-height:1.5;}
.popup_wrap .end_box p.end_box_info{padding-top:20px;line-height:1.3;font-size:13px;color:#555;}

.popup_cont{position:absolute;top:127px;left:50%;transform:translateX(-50%);z-index:1;}
.popup_cont > a{display:block;}
.popup_cont img{width:100%;min-width:300px;}
.popup_cont .btn_close{background:#333;padding:10px;text-align:right;}
.popup_cont .btn_close a{color:#fff;margin-left:10px;}

.popup_cont2{position:absolute;top:50px;left:0;z-index:2;}
.popup_cont2 > a{display:block;}
.popup_cont2 img{width:100%;min-width:300px;}
.popup_cont2 .btn_close{background:#333;padding:10px;text-align:right;}
.popup_cont2 .btn_close a{color:#fff;margin-left:10px;}
.popup_cont2 .btn_close button{color:#fff;margin-left:10px;}


/*320(브라우저 가로폭)*/
@media screen and (MIN-width: 320px){
#popup01{width:87%;margin-left:10%;}
#popup01 .btn_close{width:100%;}
}

</style>
<script language="javascript">
	
	
	function default_ajax(start_price, end_price,tab_gb)
	{
		if (tab_gb=='tab01')
		{
			var strAjaxUrl="/ajax/default_ajax_list.asp"
			var retDATA="";
			//alert(strAjaxUrl);
			
			 $.ajax({
				 type: 'POST',
				 url: strAjaxUrl,
				 dataType: 'html',
				 data: {
					start_price:start_price,
					end_price:end_price
				},						  
				success: function(retDATA) {
					if(retDATA)
						{
							if(retDATA!="")
							{
								if (tab_gb=='tab01')
								{
									document.getElementById("default_ajax_list").innerHTML= retDATA;
								}
								else
								{
									document.getElementById("default_ajax_list").innerHTML= "";
								}
								
							}
						}
				 }
			 }); //close $.ajax(
		 }
		 else
		{
			document.getElementById("default_ajax_list").innerHTML= "";
		}
	}
	
	function b2b2c_location()
	{
		if(confirm("웹카달로그로 이동합니다."))
		{
			location.href="http://b2b2c.samsungbizmall.com/default.asp"
		}
	}



</script>

<div class="container main">
	<div class="top_wrap">
		<ul class="list_wrap visual_wrap">
			<li class="slide main_banner">
				<img src="/front/img/main_banner.png" alt="" class="bn_main_m"/>
			</li>
		</ul>
		<div class="bn_wrap" >
			<div class="bn_cont bn02">
				<div class="main_txt">
					<p>Autumn/Winter 2020</p>
					<p>Preminum Collection</p>
				</div>
				<div class="sub_txt">
					<p>겨울을 맞이하여 새롭게 출시된 리닝을 시작해 보세요.</p>
				</div>
				<a href="#" class="main_btn black">SHOP NEW</a>
			</div>
		</div><!-- bn_wrap e -->
	</div>
		<%
			sql_text = " select memo from ic_t_order_cust where online_id = '"&GetsLOGINID()&"' and grp1 = '77' and grp2 = '101' and del_yn = 'N' "
			DBOpen()
				Set re_memo = Dbcon.Execute(sql_text)
			DBClose()
		%>

	<!-- section_wrap first -->
	<section class="section_wrap first">
		<div class="section_banner">
			<a href="#" class="main_link">
				<img src="/front/img/main_banner_man.png" alt="" />
				<div class="content">
					<div class="content_txt">
						<h3>MAN</h3>
						<p>새로운 스타일의 남성 데일리&스포츠웨어</p>
					</div>
					<img src="/front/img/main_arrow.png" alt="" width="13" height="21"/>
				</div>
			</a>
		</div>
		<div class="section_banner">
			<a href="#" class="main_link">
				<img src="/front/img/main_banner_woman.png" alt="" />
				<div class="content">
					<div class="content_txt">
						<h3>WOMAN</h3>
						<p>새로운 스타일의 여성 데일리&스포츠웨어</p>
					</div>
					<img src="/front/img/main_arrow.png" alt="" width="13" height="21"/>
				</div>
			</a>
		</div>
	</section><!-- section_wrap first e -->
	
	<!-- section_wrap first -->
	<section class="section_wrap second">
		<div>
			<h1>SPORTS COLLECTION</h1>
			<div class="section_banner">
				<a href="#">
					<div class="content">
						<img src="/front/img/main_banner_basketball.png" alt="" />
						<div class="caption">
							<p>Basketball</p>
						</div>
					</div>
					<div class="content_txt">
						<p>BASKETBALL SERIES</p>
						<div class="more">
							<p>MORE</p>
							<img src="/front/img/main_more_arrow.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			<div class="section_banner">
				<a href="#">
					<div class="content">
						<img src="/front/img/main_banner_badminton.png" alt="" />
						<div class="caption">
							<p>Badminton</p>
						</div>
					</div>
					<div class="content_txt">
						<p>BADMINTON SERIES</p>
						<div class="more">
							<p>MORE</p>
							<img src="/front/img/main_more_arrow.png" alt="" />
						</div>
					</div>
				</a>
			</div>
			<div class="section_banner">
				<a href="#">
					<div class="content">
						<img src="/front/img/main_banner_wade.png" alt="" />
						<div class="caption">
							<p>WADE</p>
						</div>
					</div>
					<div class="content_txt">
						<p>WADE BLACK LABEL SERIES</p>
						<div class="more">
							<p>MORE</p>
							<img src="/front/img/main_more_arrow.png" alt="" />
						</div>
					</div>
				</a>
			</div>
		</div>
	</section><!-- section_wrap first e -->

	<!-- </div> --><!-- top_wrap e -->
	<div class="bottom_wrap" id="default_ajax_list">
		<div class="list_wrap">
			<div class="list_box">
				<ul>
					<%
						strqry4 = "		SELECT			 SEQ "
						strqry4 = strqry4 & "			,ORDER_SEQ "
						strqry4 = strqry4 & "			,GD_TP "
						strqry4 = strqry4 & "			,GD_NM "
						strqry4 = strqry4 & "			,GD_GRP_BRAND "
						strqry4 = strqry4 & "			,END_PRICE "
						strqry4 = strqry4 & "			,round(PRICE + 40 ,-2) GD_PRICE3 "
						strqry4 = strqry4 & "			,(round(PRICE + 40 ,-2) * GD_MIN_VOL) TOT_PRICE "
						strqry4 = strqry4 & "			,IMG_PATH GD_IMG_PATH "
						strqry4 = strqry4 & "			,GD_UNI_GRP_CD "
						strqry4 = strqry4 & "			,GD_MIN_VOL "
						strqry4 = strqry4 & "			,GD_GRP_FIRST "
						strqry4 = strqry4 & "			,GD_GRP_MID "
						strqry4 = strqry4 & "			,GD_GRP_DETL "
						strqry4 = strqry4 & "			,GD_OPEN_YN "
						strqry4 = strqry4 & "			,GD_NUM2 "
						strqry4 = strqry4 & "	FROM (SELECT MIN(AA.SEQ) SEQ "
						strqry4 = strqry4 & "		        ,MIN(bb.seq) ORDER_SEQ "
						strqry4 = strqry4 & "		        ,AA.GD_TP GD_TP "
						strqry4 = strqry4 & "		        ,AA.GD_NM GD_NM "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_BRAND GD_GRP_BRAND "
						strqry4 = strqry4 & "		        ,ISNULL(MIN(AA.GD_PRICE8),0) END_PRICE "
						strqry4 = strqry4 & "		        ,MIN(ISNULL(CASE WHEN ISNULL(BB.GD_TAX_YN,'N') = 'N' THEN ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) "
						strqry4 = strqry4 & "		        	ELSE (ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) * 1.1)END,0)) PRICE "
						strqry4 = strqry4 & "		        ,CASE WHEN RTRIM(LTRIM( (SELECT GD_IMG_PATH FROM IC_T_GDS_INFO WHERE  SEQ = MIN(AA.SEQ)))) = '' OR  (SELECT GD_IMG_PATH FROM IC_T_GDS_INFO WHERE  SEQ = MIN(AA.SEQ)) IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
						strqry4 = strqry4 & "		        	ELSE  (SELECT GD_IMG_PATH FROM IC_T_GDS_INFO WHERE  SEQ = MIN(AA.SEQ)) END IMG_PATH "
						strqry4 = strqry4 & "		        ,AA.UNI_GRP_CD GD_UNI_GRP_CD "
						strqry4 = strqry4 & "		        ,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "		        ,isnull(MIN(BB.GD_NUM2),9999999)  GD_NUM2 "
						strqry4 = strqry4 & "		        ,AA.GD_OPEN_YN GD_OPEN_YN "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_FIRST GD_GRP_FIRST "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_MID GD_GRP_MID "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_DETL GD_GRP_DETL "
						strqry4 = strqry4 & "	FROM ITEMCENTER.dbo.IC_T_GDS_INFO AA "
						strqry4 = strqry4 & "	 ,ITEMCENTER.dbo.IC_T_ONLINE_GDS BB "
						strqry4 = strqry4 & "	WHERE AA.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_GRP_FIRST = 'ZD' "
						strqry4 = strqry4 & "	AND BB.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.SEQ = BB.GD_SEQ "
						strqry4 = strqry4 & "	AND AA.GD_BUY_END_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_OPEN_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_PRICE3 > 0 "
						strqry4 = strqry4 & "	AND GD_CATE = 'E'  "
						strqry4 = strqry4 & "	AND bb.ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"'"
						strqry4 = strqry4 & "	GROUP BY		 AA.GD_TP "
						strqry4 = strqry4 & "					,AA.GD_GRP_BRAND "
						strqry4 = strqry4 & "					,AA.UNI_GRP_CD "
						strqry4 = strqry4 & "					,BB.GD_PER "
						strqry4 = strqry4 & "					,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "					,BB.GD_TAX_YN "
						strqry4 = strqry4 & "					,AA.GD_OPEN_YN "
						strqry4 = strqry4 & "					,AA.GD_GRP_FIRST "
						strqry4 = strqry4 & "					,AA.GD_GRP_MID "
						strqry4 = strqry4 & "					,AA.GD_GRP_DETL	"
						strqry4 = strqry4 & "					,AA.GD_NM  "		
						strqry4 = strqry4 & "					having not min(aa.seq) in ('368898','392722') "
						strqry4 = strqry4 & ") A1 "
						strqry4 = strqry4 & "	ORDER BY GD_NUM2 asc "
						'Response.write strqry4
						DBOpen()
							Set rs_product = Dbcon.Execute(strqry4)
						DBClose()
					%>
					<% If rs_product.bof Or rs_product.eof Then %>
					<% Else %>
					<%
						Do Until rs_product.EOF

						PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
					%>
					<li>
							<div class="best_sale" style="z-index:99999;">
								<img src="/front/img/sale_button.jpg">
							</div>
							<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
					
							<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
							<div class="product_info ss_order_mall">
								<dl>
									<dt class="name"><%=rs_product("GD_NM")%></dt>
									<dd class="price">
<!-- 										<span><em>소비자가</em><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span> -->
										<span>
											<strike>표시가 : <%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike><br/>
											<b>혜택가 : <%= FormatNumber(rs_product("GD_PRICE3"),0)%>원</b>
											<!--b class="percent"><%=PERCENT%>%</b-->
											<!--strike>표시가 : 0원</strike><br/>
											<b>혜택가 : 0원</b>
											<!--strike>표시가 : 0원</strike><br/>
											<b>혜택가 : 0원</b-->
										</span>
									</dd>
								</dl>
							</div><!-- product_info e -->
						</a>
					</li>
					<%
						rs_product.MoveNext
						x = x + 1
						Loop
						rs_product.Close
						Set rs_product = Nothing
						End If
					%>
				</ul>

			</div>
			<!-- list_box e -->
		</div><!-- list_wrap e -->
	</div><!-- cont_bottom_wrap e -->
</div><!-- container main e -->

<!-- #include virtual = "include/footer.asp" -->

<script>
$(function(){
	/*팝업1*/
	cookiedata = document.cookie; 
	if ( cookiedata.indexOf("ncookie=done1") < 0 ){ 
		document.getElementById('popup').style.display = "block";
	} else {
		document.getElementById('popup').style.display = "none"; 
	}

	if ( cookiedata.indexOf("ncookie2=done2") < 0 ){ 
		document.getElementById('popup2').style.display = "block";
	} else {
		document.getElementById('popup2').style.display = "none"; 
	}
});
/*팝업1*/
function closeWin(){
document.getElementById('popup').style.display = "none";
}

function closeWin2(){
document.getElementById('popup2').style.display = "none";
}


function todaycloseWin(){
setCookie( "ncookie", "done1" , 24 ); 
document.getElementById('popup').style.display = "none";
}

function todaycloseWin2(){
setCookie( "ncookie2", "done2" , 24 ); 
document.getElementById('popup2').style.display = "none";
}

function onCommonOs() { // 함수처리
	var userAgent = navigator.userAgent;
	if (userAgent.match(/iPhone|iPad|iPod/)) { // ios
		var appstoreUrl = "http://itunes.apple.com/kr/app/id393499958?mt=8";
		var clickedAt = +new Date;
		location.href = appstoreUrl; 
	} else { // 안드로이드
		location.href = "Intent://#Intent;scheme=naversearchapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nhn.android.search;end"
	}
}
//쿠키정보 저장
function setCookie( name, value, expirehours ) { 
	var todayDate = new Date(); 
	todayDate.setHours( todayDate.getHours() + expirehours ); 
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
} 
//쿠키 정보 가지고 오기
function getCookie(Name) {
	var search = Name + "="
	if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
	offset = document.cookie.indexOf(search)
	if (offset != -1){// 쿠키가 존재하면
		offset += search.length
		// set index of beginning of value
		end = document.cookie.indexOf(";", offset)
		// 쿠키 값의 마지막 위치 인덱스 번호 설정
		if (end == -1)
		end = document.cookie.length
		return unescape(document.cookie.substring(offset, end))
	}
 }
	return "";
}
</script>

<!--팝업1-->
<div id="popup" class="popup_cont" style="border:1px solid #555;">
	<a href="javascript:;" onclick="onCommonOs();">
		<img src="/front/img/popup_naver_m2.jpg" usemap="#pop"/>
	</a>
	<div class="btn_close">
		<button type="button" onclick="todaycloseWin();">오늘 하루 열지 않기</button>
		<button type="button" onclick="closeWin();">닫기</button>
	</div>
</div>

<!--팝업2-->
<!--div id="popup2" class="popup_cont2" style="border:1px solid #555;">
	<a href="javascript:;" onclick="closeWin2()">
		<img src="/front/img/popup/POPUP_banner.jpg" usemap="#pop"/>
	</a>
	<div class="btn_close" >
		<button type="button" onclick="todaycloseWin2();">오늘 하루 열지 않기</button>
		<button type="button" onclick="closeWin2();">닫기</button>
	</div>
</div-->