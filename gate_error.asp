<!-- #include virtual = "include/common.asp" -->
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta name="Generator" content="EditPlus®">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=2, user-scalable=no">
<title>배달의민족</title>
<link type="text/css" rel="stylesheet" media="screen" href="/front/css/gate.css"/>
<script type="text/javascript" src="/common/js/jquery-2.1.3.min.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>

	function chk_login(){
		var f = document.login_frm;
		if(f.get_login_id.value==""){
			alert("아이디를 입력해 주세요.");
			f.get_login_id.focus();
			return false;
		}
		if(f.online_pw.value==""){
			alert("비밀번호를 입력해 주세요.");
			f.online_pw.focus();
			return false;
		}
		f.action="login_ok.asp?location_gb=Y";
		//f.target = "iSQL";
		f.submit();
		
	}



	//체크박스 클릭시
	function chk_saveid() {
		var f = document.login_frm;
	  var expdate = new Date();
	  // 30일동안 아이디 저장 
	  if (f.saveid.checked)
		expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30);// 30일
	  else
		expdate.setTime(expdate.getTime() - 1);// 쿠키 삭제조건
	  setCookie("saveid", f.get_login_id.value, expdate);
	}

	//폼 로드시 쿠키 아이디 가지고 오기
	function chk_getid() {
		var f = document.login_frm;
	  f.saveid.checked = ((f.get_login_id.value = getCookie("saveid")) != "");
	}

	//쿠키 정보 가지고 오기
	function getCookie(Name) {
	  var search = Name + "="
	  if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
		offset = document.cookie.indexOf(search)
		if (offset != -1){// 쿠키가 존재하면
		  offset += search.length
		  // set index of beginning of value
		  end = document.cookie.indexOf(";", offset)
		  // 쿠키 값의 마지막 위치 인덱스 번호 설정
		  if (end == -1)
			end = document.cookie.length
		  return unescape(document.cookie.substring(offset, end))
		}
	 }
	  return "";
	}
	//쿠키정보 저장
	function setCookie( name, value, expirehours ) { 
		var todayDate = new Date(); 
		todayDate.setHours( todayDate.getHours() + expirehours ); 
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
	} 




	function CheckInfo(){
		
		//이메일
		if(document.id_search_form.selectCheck[0].checked == true){
			if (document.id_search_form.Email_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Email_UserName.focus();
				return;
			}

			if (document.id_search_form.Email.value == "")
			{
				alert("이메일 주소를 입력하시기 바랍니다.");
				document.id_search_form.Email.focus();
				return;
			}
			
		}
		//전화번호
		else if(document.id_search_form.selectCheck[1].checked == true)
		{
			if (document.id_search_form.Phone_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Phone_UserName.focus();
				return;
			}

			if (document.id_search_form.Phone.value == "")
			{
				alert("전화번호를 입력하시기 바랍니다1.");
				document.id_search_form.Phone.focus();
				return;
			}

		}
		if (document.id_search_form.selectCheck[0].checked == true)
		{
			selectCheck= "Email"
		}
		else
		{
			selectCheck= "Phone"
		}

		var strAjaxUrl="/mypage/search_id_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				Email_UserName: escape(document.id_search_form.Email_UserName.value),
				Email: document.id_search_form.Email.value,
				Phone_UserName: escape(document.id_search_form.Phone_UserName.value),
				Phone: document.id_search_form.Phone.value,
				selectCheck: selectCheck
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else if (retDATA !='null')
				{
					document.getElementById("id_search_div").style.display ='block'
					document.getElementById("sarch_id_span").innerHTML = retDATA;
					return;
				}
			}
		});

	}
	

	function CheckEmail()
	{	
		var PWD_UserID = document.pwd_search_form.PWD_UserID.value;
		var PWD_UserEmail = document.pwd_search_form.PWD_UserEmail.value;
		
		var strAjaxUrl="/mypage/search_pw_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				UserID: document.pwd_search_form.PWD_UserID.value,
				Email: document.pwd_search_form.PWD_UserEmail.value
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else 
				{
					document.getElementById("pwd_sarch_div").style.display ='block'		
					document.getElementById("sarch_pwdid_span").innerHTML = PWD_UserID;
					document.getElementById("sarch_pwd_span").innerHTML = retDATA;
					return;
				}
			}
		});
	}

	
	function id_checked()
		{
		  var  online_id =document.getElementById('online_id').value 
		  if (online_id  =='')
			 {

				alert("ID 명을 입력해주시기 바랍니다..");
				document.getElementById('online_id').focus();
				return;
			 }

			 var strAjaxUrl="order_id_ok.asp?online_id="+online_id;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 ID입니다.');
									//document.bform.id_ok.value = ''
									return false;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 ID입니다..');
									document.getElementById('id_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}
	//아이디 있는데 사번을 또 받음? 그럼 멀로 받음?
	//그냥 메모를 활용해서 해야할듯!!!!!
	function memo_checked()
		{
		  var  memo =document.getElementById('memo').value 

		  var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 

		  if (memo  =='')
			 {
				alert("사내이메일를 입력해주시기 바랍니다..");
				document.getElementById('memo').focus();
				return;
			 }
		  if ( !regExp.test( memo ) ) 
			 {
				alert("잘못된 이메일 주소 형식입니다.");
				document.getElementById('memo').focus();
				return;
			 }
			 var strAjaxUrl="/Main/memo_id_ok.asp?memo="+memo;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 사내이메일입니다.');
									//document.bform.id_ok.value = ''
									return;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 사내이메일입니다.');
									document.getElementById('memo_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}

		function write_ok()
		{ 			
			var custseq = '<%=GetsCUSTSEQ%>'; 	
			var f				= document.reg_frm;
			var online_id		= f.online_id;
			var memo			= f.memo;
			var custnm			= f.cust_cust_custnm;
			var zip1			= f.cust_zip1;
			var zip2			= f.cust_zip2;
			var addr			= f.cust_addr;
			var cm_tel1			= f.cust_cm_tel1;
			var cm_tel2			= f.cust_cm_tel2;
			var cm_tel3			= f.cust_cm_tel3;
			var cm_hp1			= f.cust_cm_hp1;
			var cm_hp2			= f.cust_cm_hp2;
			var cm_hp3			= f.cust_cm_hp3
			var cm_email		= f.cust_cm_email
			var checkboxcpy		= f.cust_checkboxcpy
			var checkboxcpy2		= f.cust_checkboxcpy2
			var online_pwd		= f.cust_online_pwd;
			var re_online_pwd	= f.cust_re_online_pwd
			var id_ok			= f.id_ok
			var memo_ok			= f.memo_ok
			var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 

			//alert(online_id.value	);
			//alert(custnm.value	);
			//alert(zip1.value		  );
			//alert(zip2.value		  );
			//alert(addr.value		  );
			//alert(cm_tel1.value	);
			//alert(cm_tel2.value	);
			//alert(cm_tel3.value	);
			//alert(cm_hp1.value	);
			//alert(cm_hp2.value	);
			//alert(cm_hp3.value	);
			//alert(cm_email.value	  );
			//alert(checkboxcpy.value);
			//alert(online_pwd.value);
			//alert(re_online_pw.value);
			//alert(id_ok.value		  );
			//필수입력사항체크
			if (online_id.value == "") {
				alert('아이디를 입력해주시기 바랍니다.!');	
				online_id.focus();
				return;
			}
			//if (memo.value == "") {
			//	alert('사내이메일을 입력해주시기 바랍니다.!');	
			//	memo.focus();
			//	return;
			//}
	
			//if ( !regExp.test( memo.value ) ) {

			//	  alert("잘못된 이메일 주소 형식입니다.");
			//	  memo.focus();
			//	  return;
			//}


			if (online_pwd.value == "") {
				alert('비밀번호를 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value.length < 6) {
				alert('비밀번호를 4자 이상 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value != re_online_pwd.value) {
				alert('비밀번호가 일치 하지 않습니다.');
				re_online_pwd.focus();
				return;
			}	
			


			if (id_ok.value=='')
			{
				alert('아이디 중복확인을 해주시기 바랍니다.');
				return;
			}
			//if (memo_ok.value=='')
			//{
			//	alert('사내이메일를 중복확인을 해주시기 바랍니다.');
			//	return;
			//}


			if (custnm.value == "") {
				alert('성명을 입력해주시기 바랍니다.!');
				custnm.focus();
				return;
			}

			if (zip1.value == "") {
				alert('우편번호1을 입력해 주시기 바랍니다..');
				zip1.focus();
				return;
			}	
			if (zip2.value == "") {
				alert('우편번호2를 입력해 주시기 바랍니다..');
				zip2.focus();
				return;
			}	


			if (addr.value == "") {
				alert('주소를 입력해 주시기 바랍니다.');
				addr.focus();
				return;
			}	

			if (cm_hp1.value == "") {
				alert('휴대전화1을 입력해 주시기 바랍니다.');
				cm_hp1.focus();
				return;
			}	

			if (cm_hp2.value == "") {
				alert('휴대전화2를 입력해 주시기 바랍니다.');
				cm_hp2.focus();
				return;
			}	
			if (cm_hp3.value == "") {
				alert('휴대전화3을 입력해 주시기 바랍니다.');
				cm_hp3.focus();
				return;
			}	
			
			if (cm_tel1.value == "") {
				alert('전화번호1을 입력해 주시기 바랍니다.');
				cm_tel1.focus();
				return;
			}	

			if (cm_tel2.value == "") {
				alert('전화번호1를 입력해 주시기 바랍니다.');
				cm_tel2.focus();
				return;
			}	
			if (cm_tel3.value == "") {
				alert('전화번호3을 입력해 주시기 바랍니다.');
				cm_tel3.focus();
				return;
			}	
			if (cm_email.value == "") {
				alert('이메일을 입력해 주시기 바랍니다..');
				cm_email.focus();
				return;
			}	


			if (checkboxcpy.checked ==false)
			{
				alert('약관동의에 체크해 주시기 바랍니다.');
				return;
			}

			if (checkboxcpy2.checked ==false)
			{
				alert('제3자 정보동의에 체크해 주시기 바랍니다.');
				return;
			}
			f.action = "join_ok_new.asp"
			f.submit();		
		}

	function execDaumPostCode(input_type) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }
                // 우편번호와 주소 정보를 해당 필드에 넣는다.
				if(data.userSelectedType == 'R')
				{
					document.getElementById("cust_zip1").value = left(data.zonecode,3)
					document.getElementById("cust_zip2").value = right(data.zonecode,2)
					
					document.getElementById("cust_addr").value = fullRoadAddr;					
				}
				 
				else if(data.userSelectedType == 'J'){

					 if (data.postcode1 =='')
						 {
							document.getElementById("cust_zip1").value = left(data.zonecode,3)
							document.getElementById("cust_zip2").value = right(data.zonecode,2)
						 }
						 else
						 {
							document.getElementById("cust_zip1").value = data.postcode1;
							document.getElementById("cust_zip2").value = data.postcode2;
						 }
					
					document.getElementById("cust_addr").value = data.jibunAddress;
				}


				document.getElementById("cust_addr").focus()			

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
    //                document.getElementById("guide").innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
  //                  document.getElementById("guide").innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById("guide").innerHTML = '';
                }
            }
        }).open();
	}

	function left(s,c){
	  return s.substr(0,c);
	}//left("abcd",2)
	function right(s,c){
	  return s.substr(-c);
	}//right("abcd",2)
	function mid(s,c,l){
	  return s.substring(c,l);
	}//mid("abcd",1,2)
	function copy(s,c,l){
	  return s.substr(c,l);
	}//copy("abcd",1,2)


	function enter_key()
	{
		if (window.event.keyCode == 13) {
	 
				 chk_login()
			}
	}
</script>
</head>
<body>
	<div style="width:100%; text-align:center; margin-top:10%;">
	<img src="/front/img/gate_default_new.png" alt="logo"/>
	</div>
</body>
</html>


	

