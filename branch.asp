<!-- #include virtual = "/include/head.asp" -->
<!-- #include virtual = "/include/header.asp" -->
<%
page			= request("page")
branch_group	= request("branch_group_text")
branch_name		= request("branch_name_text")
branch_addr		= request("branch_addr")
branch_tel		= request("branch_tel")		

'response.write "branch_name : " &branch_name
NowPage = request("page")


if page = "" then page = 1

if NowPage = "" then NowPage = 1


Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10

goParam = "branch_dev.asp?tmp=null"

If ViewCnt = "" Then 
	ViewCnt = "10"
End If 

Dbopen()
sql = "USP_MALL_ADMIN_BRANCH_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size &" ,@branch_group= '"&branch_group&"',@branch_name='"&branch_name&"',@branch_addr='"&branch_addr&"',@branch_tel='"&branch_tel&"',@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"'"

'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	'TotalPage = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)

	TotalPage=Int((CInt(total_cnt)-1)/CInt(ViewCnt)) +1

End If

link_text = "branch.asp?branch_group_text="&branch_group&"&branch_name_text="&branch_name

%>
<script language='javascript'>
	
	function serch() 
	{	
		document.action="branch.asp"
		document.bform2.submit();
	}

	function iframe_id(seq)
	{
		document.getElementById("iframe_id").src="http://b2bc.samsungbizmall.com/map_iframe.asp?seq="+seq  
	}

	function branch_type(this_is)
	{
		document.getElementById("div_text").innerHTML = this_is.value
	}
</script>
<style type="text/css">
	.popup_layer .cont_box{padding:0 0 5px;overflow:hidden;}
	.popup_layer .cont_box iframe{width:100%;height:100%;}
</style>
<div class="container sub">
	<div class="list_page branch_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">행사매장</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">

			<div class="list_wrap">
				
				<div class="sc_page">
					<form name="bform2" id="bform2" method="post" action="branch.asp">
						
						
						<div class="sch_box">
							<div class="branch_text_div" >
								선택된 지역 : <span class="div_sub"  id="div_text">
								<% If branch_group <> "" Then %>
									<%=branch_group%>
								<% Else %>
									전체지역
								<% End If %>
								</span>
							</div>
							<div class="select_box select02">

								<label for="search_key">
									<% If branch_group <> "" Then %>
										<%= branch_group %>	
									<% Else %>
										전체
									<% End If %></label>
								<%
									sql2 = " SELECT BRANCH_GROUP "
									sql2 = sql2 & " FROM IC_T_SAMPAN_BRANCH_LIST "
									sql2 = sql2 & " WHERE online_cd='1117'  "
									sql2 = sql2 & " and del_yn ='N'  "
									sql2 = sql2 & " and branch_group <> '"&branch_group&"' "
									sql2 = sql2 & " GROUP BY BRANCH_GROUP order by max(seq) asc "

									Set rs2 = DBcon.Execute(sql2)

								%>
								<select id="branch_group" name="branch_group_text" onchange="javascript:branch_type(this);">
									<% If branch_group <> "" Then %>
										<option value="" selected="selected">전체</option>
										<option value="<%=branch_group%>" selected="selected"><%=branch_group%></option>
									<% Else %>
										<option value="" selected="selected">전체</option>
									<% End If %>
									
									<% If rs2.bof Or rs2.eof Then %>	
									
									<% Else %>
									<%
										Do Until rs2.EOF
									%>
									<option VALUE="<%=rs2("BRANCH_GROUP")%>" <%If rs2("BRANCH_GROUP") = branch_group Then %> selected="selected"<% End if%>><%=rs2("BRANCH_GROUP")%></option>
									<%
										rs2.MoveNext
										x = x + 1
										Loop
										rs2.Close
										Set rs2 = Nothing
										End If
									%>
								</select>
							</div><!-- select_box e -->
							<input type="text" name="branch_name_text" id="branch_name" class="sch_txt" placeholder="매장명을 입력해 주세요." />
							<input type="button" id="btn_sch" value="검색" onclick="javascript:serch();"/>
						</div><!-- sch_box e -->
					</form>
					<!-- 웹 화면 일때 -->
					<div class="sc_table w_table">
						<table cellpadding="0" cellspacing="0" class="t_type01" summary="">
							<colgroup>
								<col class="local"/>
								<col class="name"/>
								<col class="addr"/>
								<col class="num"/>
								<col class="shop"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="" class="local">지역</th>
									<th scope="" class="name">매장명</th>
									<th scope="" class="addr">주소</th>
									<th scope="" class="num">매장번호</th>
									<th scope="" class="shop">매장보기</th>
								</tr>
							</thead>
							<tbody>
								<% If numList = -1 Then %>

								<!-- 매장검색 결과가 없을 때 -->
								<tr>
									<td colspan="4" class="result_none">검색 결과가 없습니다.</td>
								</tr>
								<!-- 매장검색 결과가 있을 때 -->
								<% Else %>
								<%
									for i = (page-1) * page_size To numList
									  seq		    = arrList(2,i)
									  branch_group	= arrList(3,i)
									  branch_name	= arrList(4,i)
									  branch_addr	= arrList(5,i)
									  branch_tel	= arrList(6,i)
								%>
								<tr>
									<td><%=branch_group%></td>
									<td><%=branch_name%></td>
									<td><%=branch_addr%></td>
									<td><%=branch_tel%></td>
									<td>
										<a href="#pop_map" class="open" onclick="javascript:iframe_id('<%=seq%>');"><img src="/front/img/common/search_shop.png" alt="" style="width:30px;"/></a>
									</td>
								</tr>
								<%

									h_number = h_number - 1

									next
								%>
								<% End If %>
							</tbody>
						</table>
					</div><!-- sc_table e -->
					
					<!-- 모바일 화면 일때 -->
					<div class="m_table">
						<ul>
							<% If numList = -1 Then %>
								<!-- 매장검색 결과가 없을 때 -->
								<li class="result_none">검색 결과가 없습니다.</li>
								<!-- 매장검색 결과가 있을 때 -->
							<%Else %>
								<%
									for i = (page-1) * page_size To numList
									  seq		    = arrList(2,i)
									  branch_group	= arrList(3,i)
									  branch_name	= arrList(4,i)
									  branch_addr	= arrList(5,i)
									  branch_tel	= arrList(6,i)
								%>
									<li>
										<div class="first">
											<span class="place"><%=branch_group%></span>
											<span class="shop"><%=branch_name%>
												<a href="#pop_map" class="open" onclick="javascript:iframe_id('<%=seq%>');"><img src="/front/img/common/search_shop.png" alt="" style="width:30px;"/></a>
											</span>
										</div>
										<div class="addr"><%=branch_addr%></div>
										<div class="tel"><%=branch_tel%></div>
									</li>
								<%

									h_number = h_number - 1

									next
								%>
							<% End If %>
						</ul>
					</div><!-- m_table e -->

					<!-- 과장님! 페이징은 5개씩 노출되는게 좋을것 같아요~ -->
					<%=PT_PageLink2(link_text,"10")%>                 
				</div><!-- sc_page e -->
			</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
<!-- #include virtual = "/include/footer.asp" -->
<!-- #include virtual = "/include/popup/shop_map.asp" -->
