<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width">
<title>HYUNDAI</title>
<script type="text/javascript" src="./js/jquery-1.11.3.min.js"></script> 
<style type="text/css">
html,body,h1,h2,div,img,span,form,input,label,ul,li,a,p{margin:0;padding:0;}
body{ font-family:'NanumGothic', sans-serif;box-sizing:border-box;}
.gate_wrap{min-width:320px;width:100%;max-width:640px;margin:0 auto;padding:50px 0;}
.gate_wrap h1{width:300px;margin:0 auto;}
.gate_wrap h1 img{width:100%;}
.gate_cont{margin-top:50px;padding:0 10px;}
.gate_cont h2{font-size:20px;font-weight:500;margin-bottom:20px;}
.gate_cont .gate_form{}
.gate_cont .gate_form li{list-style:none;margin-bottom:15px;}
.gate_cont .gate_form li label{vertical-align:top;}
.gate_cont .gate_form li label.info{display:inline-block;width:60px;vertical-align:middle;}
.gate_cont .gate_form li input[type="text"]{width:300px;height:30px;padding:0 5px;border:none;border-bottom:1px solid #ddd;font-size:15px;outline:none;}
.gate_cont .gate_form li input[type="text"]:focus{border-bottom:1px solid #11296f;color:#11296f;}
.gate_cont .gate_form li input[type="checkbox"]{width:20px;height:20px;}
.gate_cont .gate_form li .box{width:100%;height:100px;margin-bottom:5px;padding:10px;border:1px solid #ddd;overflow-y:scroll;}
.gate_cont .agree01{}
.gate_cont .agree02{}
.gate_cont .btn_go{text-align:center;}
.gate_cont .btn_go a{display:inline-block;width:200px;height:40px;margin-top:30px;padding-top:15px;font-size:20px;font-weight:500;background:#11296f;color:#fff;text-decoration:none;}
</style>
</head>
<script language='javascript'>
	
	
	
	function trim(str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}


	function gate_ok()
	{
		var user_name = document.getElementById("user_name")
		var user_phone = document.getElementById("user_phone")
		var agree_01 = document.getElementById("agree_chk01")
		var agree_02 = document.getElementById("agree_chk02")
		var reg_email = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;
		var reg_phone = /^\d{3}-\d{3,4}-\d{4}$/;
			
		if (trim(user_name.value).length  == 0 )
		{
			alert('이름을 입력해 주시기 바랍니다.');
			user_name.focus();
			return;
		}

		if (trim(user_phone.value).length  == 0 )
		{
			alert('연락처를 입력해 주시기 바랍니다.');
			user_phone.focus();
			return;
		}

		if(!reg_phone.test(user_phone.value))
		{
			alert('핸드폰번호 형식에 맞게 입력해 주시기 바랍니다.');
			user_email.focus();
			return;
		}

	
		if (agree_01.checked == false)
		{
			alert('마케팅 수신동의에 체크하여 주시기 바랍니다.');
			agree_01.focus();
			return;

		}
		if (agree_02.checked == false)
		{
			alert('제3자정보제공에 체크하여 주시기 바랍니다.');
			agree_02.focus();
			return;
		}
		
		document.bform.action = "gate_ok.asp"
		document.bform.submit();
	}

	


</script>
<body>
	<form name="bform" method="post">
	<div class="gate_wrap">
		<h1>
			<img src="/rol/img/hun_logo.jpg" alt="HYUNDAI GLOVIS"/>
		</h1>
		<div class="gate_cont">
			<h2>이벤트 정보 입력</h2>
			<form method="post" action="">
				<ul class="gate_form">
					<li>
						<label for="user_name" class="info">이&nbsp;&nbsp;&nbsp;름</label>
						<input type="text" id="user_name" name="user_name" class="draw" autocomplete="user_name"/>
					</li>
					<li>
						<label for="user_phone" class="info">연락처</label>
						<input type="text" id="user_phone" name="user_phone" class="draw" onKeyup="inputTelNumber(this);" maxlength="13"/>
					</li>
					<li class="agree01">
						<div class="box"></div>
						<input type="checkbox" id="agree_chk01" name="agree_01" />
						<label for="agree_chk01">마케팅 수신동의</label>
					</li>
					<li class="agree02">
						<div class="box">
							<p>특별한 제안, 멤버십 혜택, 뉴스레터를 받아 보려면 이 옵션을 실행하세요.</p><br/>
							<p>* 삼성 마케팅 활동은 위탁 업체에서 담당합니다. 마케팅 정보를 받는 데 동의하면 위탁 업체에서 마케팅 정보를 받는 것에도 동의하는 것으로 간주합니다.</p><br/>
							<p>① 개인정보를 제공받는 자 : ㈜ 위드라인( 삼성전자 B2B몰 운영대행사)</p>
							<p>② 개인정보를 제공받는 자의 개인정보 이용 목적 : 고객 관리 및 활용(마케팅)</p>
							<p>③ 제공하는 개인정보의 항목 : 성명, 연락처, 이메일</p>
							<p>④ 개인정보를 제공받는 자의 개인정보 보유 및 이용 기간 : 제공 후 3년</p>
							<p>⑤ 동의를 거부할 수 있으며, 동의 거부시 서비스가 제공되지 않습니다.</p>
						</div>
						<input type="checkbox" id="agree_chk02"  name="agree_02"/>
						<label for="agree_chk02">제3자 정보제공 동의</label>
					</li>
				</ul>
				<div class="btn_go">
					<a href="#" onclick="javascript:gate_ok();">이벤트 참여하기</a>
				</div>
			</form>
			</div>
		</div>
	</div>
	</form>
<script>
function inputTelNumber(obj) {
	var number = obj.value.replace(/[^0-9]/g, "");
	var tel = "";

	if(number.substring(0, 2).indexOf('02') == 0) { // 서울 지역번호(02)가 들어오는 경우
			if(number.length < 3) {
					return number;
			} else if(number.length < 6) {
					tel += number.substr(0, 2);
					tel += "-";
					tel += number.substr(2);
			} else if(number.length < 10) {
					tel += number.substr(0, 2);
					tel += "-";
					tel += number.substr(2, 3);
					tel += "-";
					tel += number.substr(5);
			} else {
					tel += number.substr(0, 2);
					tel += "-";
					tel += number.substr(2, 4);
					tel += "-";
					tel += number.substr(6);
			}
	} else { // 서울 지역번호(02)가 아닌경우
			if(number.length < 4) {
					return number;
			} else if(number.length < 7) {
					tel += number.substr(0, 3);
					tel += "-";
					tel += number.substr(3);
			} else if(number.length < 11) {
					tel += number.substr(0, 3);
					tel += "-";
					tel += number.substr(3, 3);
					tel += "-";
					tel += number.substr(6);
			} else {
					tel += number.substr(0, 3);
					tel += "-";
					tel += number.substr(3, 4);
					tel += "-";
					tel += number.substr(7);
			}
	}
	obj.value = tel;
}
</script>


</body>
</html>