<!--#include virtual="/Library/config.asp"-->
<%
	Refer_Url			= fInject(Request("refer_url"))
	get_login_id		= fInject(Request("get_login_id"))
	Online_Pw			= Request("online_pw")
	get_parameter_gubun = fInject(Request("get_parameter_gubun"))
	saveid				= fInject(Request("saveid"))
	redirect_url		= fInject(Request("redirect_url"))	

	If saveid = "" Then 
		saveid = "N"
	End If 

	If get_login_id = ""  Then
		 Response.Write "<script>alert('아이디 또는 패스워드를 입력 후 로그인 버튼을 눌러주세요.');</script>"
		Response.End
	End If 	


  LoginSQL = "SELECT GL_PE,ORDER_SEQ, ORDER_NM, ISNULL(CM_ZIP,'') AS CM_ZIP, ISNULL(CM_ZIP2,'') AS CM_ZIP2, ISNULL(CM_ADDR,'') AS CM_ADDR, ISNULL(CM_TEL,'') AS CM_TEL, ISNULL(CM_EMAIL,'') AS CM_EMAIL, ISNULL(CM_HP,'') AS CM_HP, GRP1, GRP2, GRP3, GRP4, GRP5, GRP6, Update_GuBun FROM IC_T_ORDER_CUST "
	LoginSQL = LoginSQL&" WHERE DEL_YN='N' AND ISNULL(IN_OK,'Y') = 'Y'"
	LoginSQL = LoginSQL&" AND ONLINE_ID = '"&get_login_id&"'"
	LoginSQL = LoginSQL&" AND GL_PE = 'mall_b2bc'"
	LoginSQL = LoginSQL&" AND CUST_TP='2' "

	DBopen()
	Set LoginRs = dbcon.Execute(LoginSQL)
	
	If Not (LoginRs.Eof Or LoginRs.Bof) Then 
		'로그인 로그 생성=======================================================================================================================
		LogSQL = "INSERT INTO tblLoginLog "
		LogSQL = LogSQL&"(UserID, UserGubun, Referer, Agent, UserIp, WriteDate, ONLINECD, GRP1, GRP2, GRP3, GRP4, GRP5, GRP6)"
		LogSQL = LogSQL&"	VALUES "
		LogSQL = LogSQL&"('"&get_login_id&"','"&LoginRs("GRP1")&"','"&Request.ServerVariables("HTTP_REFERER")&"','"&Request.ServerVariables("HTTP_USER_AGENT")&"','"&Request.ServerVariables("REMOTE_HOST")&"',Getdate(),'"&GLOBAL_VAR_ONLINECD&"','"&LoginRs("GRP1")&"','"&LoginRs("GRP2")&"','"&LoginRs("GRP3")&"','"&LoginRs("GRP4")&"','"&LoginRs("GRP5")&"','"&LoginRs("GRP6")&"')"
'		Response.Write LogSQL
'		Response.End
		Dbcon.Execute(LogSQL)				
		'로그인 로그 생성=======================================================================================================================

		'세션 셋팅
		SetSession get_login_id,LoginRs("ORDER_SEQ"),LoginRs("GL_PE"),LoginRs("ORDER_NM"),LoginRs("CM_ZIP"),LoginRs("CM_ZIP2"),LoginRs("CM_ADDR"),LoginRs("CM_TEL"),LoginRs("CM_EMAIL"),LoginRs("CM_HP"),saveid, Online_Pw



		'SQL 스케쥴러에 걸려있었지만 한시간마다 업데이트 반영이라 로그인시 처리 
		'상품검색 INDEX 테이블 정보 업데이트
		'해당몰의 상품검색 테이블 삭제

		'관리자 아이디 로그인시 상품 업데이트
		If get_login_id = "kdbadmin" Then 
			GD_DEL = "DELETE FROM IC_T_ONLINE_GD_SEARCH WHERE ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"'"
			Dbcon.Execute(GD_DEL)

			'해당몰의 상품검색 테이블 새로 인서트
			GD_In = "INSERT INTO IC_T_ONLINE_GD_SEARCH SELECT (SELECT TOP 1 SEQ FROM IC_T_GDS_INFO where GD_NM=GI.GD_NM) AS SEQ,GI.GD_NM,'0013' AS ONLINE_CD,(SELECT TOP 1 GD_GRP_FIRST FROM IC_T_GDS_INFO where GD_NM=GI.GD_NM),'' "
			GD_In = GD_In&" FROM IC_T_GDS_INFO GI join IC_T_ONLINE_GDS OG on GI.SEQ=OG.GD_SEQ where GI.DEL_YN='N' AND GI.GD_BUY_END_YN='N' AND OG.DEL_YN='N' AND OG.ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' GROUP BY GI.GD_NM"
			Dbcon.Execute(GD_In)
		End If 

		'상단카테고리를 위한 쿠키값 설정
		If redirect_url <> "" Then 
			Response.Redirect redirect_url
			Response.End 
		Else 
			Response.Write "<script>location.href='http://b2bc.samsungbizmall.com/default.asp'</script>"	
			Response.End 
		End If 

	Else

		Response.Write "<script>alert('등록되지 않은 인식번호입니다. 확인 후 다시 로그인 해주세요.'); location.href='/gate.asp' </script>"
		Response.End

	End If 



	Set LoginRs = Nothing 
	dbclose()

%>