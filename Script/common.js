//온라인 몰 선택
function Change_Online_Mall(obj){
	location.href='/Main/Default.asp?Online_Cd='+obj;
}


//고객분류 선택2
function chk_group2(group_code2){
	var group_code1 = document.getElementById("Grp1").value;
	//그룹3,4번 초기화
//	document.getElementById("Group3").innerHTML="";
//	document.getElementById("Group4").innerHTML="";
	if(group_code2!=""){
		location.href="?Grp1="+group_code1+"&Grp2="+group_code2;
	}
}

//고객분류 선택3
function chk_group3(group_code3){
	var group_code1 = document.getElementById("Grp1").value
	var group_code2 = document.getElementById("Grp2").value
	//4번 그룹 초기화
//	document.getElementById("Group4").innerHTML="";
	if(group_code3!=""){
		location.href="?Grp1="+group_code1+"&Grp2="+group_code2+"&Grp3="+group_code3;
	}
}

//고객분류 선택4
function chk_group4(group_code4){
	var group_code1 = document.getElementById("Grp1").value
	var group_code2 = document.getElementById("Grp2").value
	var group_code3 = document.getElementById("Grp3").value
	//4번 그룹 초기화
	if(group_code4!=""){
		location.href="?Grp1="+group_code1+"&Grp2="+group_code2+"&Grp3="+group_code3+"&Grp4="+group_code4;
	}
}

/*===============================================================================
'Description : trim 함수와 동일한 기능 
'===============================================================================*/
function trim(str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

/*===============================================================================
'Description : formatnumber 함수와 동일한 기능 
'===============================================================================*/
function commify(n) {    
	var reg = /(^[+-]?\d+)(\d{3})/;    n += '';     
	while (reg.test(n))        
	n = n.replace(reg, '$1' + ',' + '$2');    
	return n;
} 

function init(){	
	backtoTop();	
}

function backtoTop() {
	var $topButton = $(".backtoTop");

	$(window).on("scroll", function(){
		if ($(window).scrollTop() > 500) {
			buttonOn();
		}else{
			buttonOff();
		}
	});
	function buttonOn() {
		$topButton.on("click", "a", function(b) {
			b.preventDefault();
			$("html, body").scrollTop(0);
			buttonOff();
		});
		$topButton.addClass("show");
	}
	function buttonOff() {
		$topButton.removeClass("show");
	}
}
