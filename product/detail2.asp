<!-- #include virtual = "/include/head.asp" -->
<!-- #include virtual = "/include/header.asp" -->
<%
	SEQ    = fInject(Request("SEQ"))
	
	If SEQ = "" Then
		Response.Write "<script>alert('해당 상품의 상세페이지가 존재하지 않습니다.');history.back();</script>"
		Response.End
	Else
		ChkSQL = "SELECT Count(GI.SEQ) FROM IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
		ChkSQL = ChkSQL & " ON GI.SEQ = OG.GD_SEQ "
		ChkSQL = ChkSQL & " WHERE "
		'몰코드
		ChkSQL = ChkSQL & " OG.ONLINE_CD = '" & GLOBAL_VAR_ONLINECD & "' "
		ChkSQL = ChkSQL & " AND GI.GD_BUY_END_YN = 'N' "
		'ChkSQL = ChkSQL & " AND GI.GD_OPEN_YN = 'N' "
		ChkSQL = ChkSQL & " AND GI.DEL_YN='N' "
		ChkSQL = ChkSQL & " AND OG.DEL_YN='N' "
		ChkSQL = ChkSQL & " AND OG.UNI_GRP_CD = '2001' "
		ChkSQL = ChkSQL & " AND GI.SEQ = '" & SEQ & "' "
		'Res	ponse.write ChkSQL
		DBopen()
			Set ChkRs = DBcon.Execute(ChkSQL)
		
		If ChkRs(0) = 0 Then 
			Response.Write "<script>alert('판매종료 되었거나 삭제된 상품입니다.');history.back();</script>"
			Response.End
		End If

	End If


	Set PRs = Product_View(GLOBAL_VAR_ONLINECD,"",SEQ,"")

	GD_NM = PRs("GD_NM")

	If GD_NM = "" Then
		Response.Write "<script>alert('해당 상품의 상세페이지가 존재하지 않습니다.');history.back();</script>"
		Response.End
	End If


	'상품 카운팅 증가 IC_T_ONLINE_GDS
	Product_View_COUNT(PRs("OG_SEQ"))

	'상품 옵션리스트 함수
	'몰코드,상품명
	OptionRs = Option_View(GLOBAL_VAR_ONLINECD,GD_NM,"")


	DBclose()

	strqry61 = " insert into tblProduct_mall_ViewLog ( "
	strqry61 = strqry61 & " UserID,UserGubun,PageName,GD_CATE,PRICE_GB, "
	strqry61 = strqry61 & " GD_GRP_MID,GD_GRP_DETL,Depth,ProductNo,WriteDate,WriteIP ,online_cd"
	strqry61 = strqry61 & " ) values ( "
	strqry61 = strqry61 & " '"& GetsLOGINID &"', "
	strqry61 = strqry61 & " '', "
	strqry61 = strqry61 & " 'sub_detail.asp', "
	strqry61 = strqry61 & " '"& GD_CATE &"', "
	strqry61 = strqry61 & " '', "
	strqry61 = strqry61 & " '"& GD_GRP_MID &"', "
	strqry61 = strqry61 & " '"& GD_GRP_DETL &"', "
	strqry61 = strqry61 & " '3', "
	strqry61 = strqry61 & " '"& seq &"', "
	strqry61 = strqry61 & " getdate(), "
	strqry61 = strqry61 & " '"& Request.ServerVariables("REMOTE_HOST") &"', "
	strqry61 = strqry61 & " '"& GLOBAL_VAR_ONLINECD &"' "
	strqry61 = strqry61 & " ) "

	DBopen()

	DBcon.Execute(strqry61)

	DBclose()
%>
<script language='javascript'>
	function minus(Seq,Min_Vol)
	{
		if (document.getElementById("GD_VOL").value <= 1 )
		{
			alert('최소 주문수량 이하의 수량은 선택하실 수 없습니다.');
			return;
		}
		document.getElementById("GD_VOL").value = Number(document.getElementById("GD_VOL").value) - 1 

		P_Sum(Seq,Min_Vol)
	}

	function plut(Seq,Min_Vol)
	{
		document.getElementById("GD_VOL").value = Number(document.getElementById("GD_VOL").value) + 1
		
		P_Sum(Seq,Min_Vol)
	}


	//상품 옵션 클릭 변경
	//상품번호,상품가격,옵션1,옵션2최소주문수량,대표이미지,대표이미지2,대표이미지3,대표이미지4,상세이미지
	function Gd_Change(Obj){
		ArrayObj = Obj.split(",")
		Gd_Seq        = ArrayObj[0]
		Gd_Price      = ArrayObj[1]
		Gd_Option1    = ArrayObj[2]
		Gd_Option2    = ArrayObj[3]
		Gd_Min_Vol    = ArrayObj[4]
		Gd_Img_Path   = ArrayObj[5]
		Gd_Img_Path_2 = ArrayObj[6]
		Gd_Img_Path_3 = ArrayObj[7]
		Gd_Img_Path_4 = ArrayObj[8]
		Gd_Img_Path2  = ArrayObj[9]
		//상품코드변경
	//	document.getElementById("GD_SEQ").value = Gd_Seq+"|";
		document.getElementById("GD_SEQ").value = Gd_Seq;

		//최소구매수량
		document.getElementById("GD_MIN_VOL").value = Gd_Min_Vol;

		//구매갯수 최소 수량상품으로 셋팅
		//document.getElementById("GD_VOL").value = Gd_Min_Vol;

		//가격변경
		document.getElementById("inprice").innerHTML = addcomma(Gd_Price);
		document.getElementById("Price").value = Gd_Price;
		//구매합계수량 변경
		var Total_Price = Gd_Price * Gd_Min_Vol

		//document.getElementById("ToTal_Price").innerHTML = addcomma(Total_Price);
		

		//히든옵션1변경
		document.getElementById('spnopt1').value = Gd_Option1;
		//히든옵션2변경
		document.getElementById('spnopt2').value = Gd_Option2;

		document.getElementById('image_point').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path;
		<% If PRs("gd_img_path") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img1').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path;
		<% end if  %>
		<% If PRs("gd_img_path_2") <> "gdsimage/listgdsnull1.jpg" Then %>  
			document.getElementById('s_img2').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_2;
		<% end if%>
		<% If PRs("gd_img_path_3") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img3').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_3;
		<% end if%>
		<% If PRs("gd_img_path_4") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img4').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_4;
		<% end if%>
		document.getElementById('imglistdetl').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path2;
		
		//image_p('1');
	}

	//천단위 콤마
	function addcomma(n) {
	  var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
	  n += '';                          // 숫자를 문자열로 변환

	  while (reg.test(n))
		n = n.replace(reg, '$1' + ',' + '$2');

	  return n;
	}


	//상품수량변경
	function P_Sum(Seq,Min_Vol){
		/*
		'============================================================================================
		'============================================================================================
		상품의 수량은 배열로 받기때문에 getElementsByName를 써야함.
		//해당 상품의 수량
		var P_EA = document.getElementsByName("P_EA")[i];
		'============================================================================================
		'============================================================================================
		*/
		//해당 상품의 수량
		var GD_VOL = document.getElementById("GD_VOL");
		//해당 상품의 개당 단가
		var Price = document.getElementById("Price");
		//해당상품의 합계금액
		var P_Sum     = document.getElementById("ToTal_Price");

		/*
		if(GD_VOL.value==0 || GD_VOL.value==""){ 
			GD_VOL.value = Min_Vol;
		}
		*/
		//합계
		P_Sum.innerHTML     = addcomma(GD_VOL.value*Price.value);
		//총 결제금액 구하기
	//	Total_Sum()	
	}


	function chk_min_vol(Seq,Min_Vol){
		var GD_VOL = document.getElementById("GD_VOL");
		if(Number(GD_VOL.value)<Number(Min_Vol)){
			GD_VOL.value = Min_Vol;
			alert("최소 주문수량은 ["+Min_Vol+"]개입니다.");
			//상품금액 재계산
			P_Sum(Seq,Min_Vol)
		}
	}

	//숫자입력체크
	function chk_Number(){
		if ((event.keyCode<48)||(event.keyCode>57)) event.returnValue=false;
	}

	//장바구니 담기==============================================================
	function inCart(){ //장바구니
		var f = document.Orderfrm;
		f.target = "iSQL"
		//f.target = "_blank"
		f.action = "/order/cart_ok.asp"
		f.submit();				
	}
	//관심상품 담기=============================================================	

	//상품주문체크
	function inOrder(){

		var f = document.Orderfrm;
		//최소주문수량보다 적게 주문할경우
		if(Number(f.GD_MIN_VOL.value)>Number(f.GD_VOL.value)){
			alert("최소 주문수량보다 적은 수량은 구매하실수 없습니다");
			return false;
		}
		//ie11 새로고침 오류로 변경
		location.href="../order/order.asp?Cart_YN=N&GD_SEQ="+encodeURI(f.GD_SEQ.value)+"&GD_VOL="+encodeURI(f.GD_VOL.value);
		/*f.action="../order/order.asp"
		f.submit();*/
	}

function originimg(obj) {
//document.all.img.src="./images/products_img/content_img/<%=p_common_img%>";
document.getElementById("image_point").src=obj.src;
}

function swapimg(obj) {
//document.all.img.src=obj.src;
document.getElementById("image_point").src=obj.src;
}


function qna_write()
{
	document.getElementById("qna_list_div").style.display = 'none';
	document.getElementById("qna_write_div").style.display = 'block';

}

function qna_write_ok(seq)
{
	var ch01 = document.getElementById("ch01").checked;
	var ch02 = document.getElementById("ch02").checked;
	var ch03 = document.getElementById("ch03").checked;
	var ch04 = document.getElementById("ch04").checked;
	var open_yn = document.getElementById("open_yn").checked; 
	var con_text = document.getElementById("con_text").value;  

	if(trim(con_text).length == 0)
		{
			alert('내용을 입력해주시기 바랍니다.');
			document.getElementById("con_text").focus();
			return;
		}


	if (ch01 == true)
	{
		ch_gb = "product"
	}
	if (ch02 == true)
	{
		ch_gb = "order"
	}
	if (ch03 == true)
	{
		ch_gb = "cencle"
	}
	if (ch04 == true)
	{
		ch_gb = "etc"
	}

	if (open_yn== true)
	{
		open_yn = "Y"
	}
	else
	{
		open_yn = "N"
	}
	var strAjaxUrl="/ajax/qna_write_ok.asp"
	var retDATA="";
	//alert(strAjaxUrl);
	
	 $.ajax({
		 type: 'POST',
		 url: strAjaxUrl,
		 dataType: 'html',
		 data: {
			qna_type:ch_gb,
			open_yn:open_yn	,
			con_text:con_text,
			gd_seq:seq
		},						  
		success: function(retDATA) {
			qna_search('1','5','PAGING','no');
			con_text = "";
		 }
	 }); //close $.ajax(
}

function qna_search(blockpage,ViewCnt,PAGING,gb)
{
	var strAjaxUrl="/ajax/qna_ajax_list.asp"
	var retDATA="";
	//alert(strAjaxUrl);
	
	 $.ajax({
		 type: 'POST',
		 url: strAjaxUrl,
		 dataType: 'html',
		 data: {
			 now_page:blockpage
			,GD_SEQ:'<%= SEQ %>'
		},						  
		success: function(retDATA) {
			 document.getElementById("qna_list_div").style.display = 'block';
			 document.getElementById("qna_write_div").style.display = 'none';
			 document.getElementById("qna_list_div").innerHTML = retDATA;				 
		 }
	 }); //close $.ajax(	
}
function qna_delete(seq)
{	
	if(confirm("삭제 하시겠습니까?"))
	{
		var strAjaxUrl="/ajax/qna_delete_ok.asp"
		var retDATA="";
		//alert(strAjaxUrl);
		
		 $.ajax({
			 type: 'POST',
			 url: strAjaxUrl,
			 dataType: 'html',
			 data: {
				 seq:seq
			},						  
			success: function(retDATA) {
				qna_search('1','5','PAGING','no') 
			 }
		 }); //close $.ajax(
	}
}
</script>
<form method="post" name="Orderfrm">
<!--현재 선택 상품 번호-->
<input id="GD_SEQ" type="hidden" name="GD_SEQ" value="<%=SEQ%>" >
<!--현재 상품 최수 구매 수량-->
<input type="hidden" id="GD_MIN_VOL" name="GD_MIN_VOL" value="<%=PRs("GD_MIN_VOL")%>">
<!--현재 상품 카트에 담을시 카트 타입-->
<input type="hidden" id="CART_TYPE" name="CART_TYPE" value="Cart_Add">
<!--해당 상품의 현재 단가-->
<input type="hidden" id="Price" name="Pirce" value="<%=PRs("GD_PRICE3")%>">
<div class="container sub">
	<div class="location_wrap">
		<span><a href="#">HOME</a></span><i></i>
		<span><%=PRs("gd_nm")%></span>
	</div><!-- location_wrap e -->
	<div class="detail_page detail_page02">
		<div class="product_wrap">
			<div class="img_cont">
				<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" alt="" id="image_point" name="image_point"/>
				<% If PRs("GD_OPEN_YN") ="Y"  Then %>
					<div class="soldout_detail_2">
						<span class="online">
							<label>SOLD OUT</label>
						</span>	
					</div>
				<% End If %>	
			 <!--큰 이미지, 관련이미지 4개 및 상품삭제,상품미공개 시작-->
				<div class="img_wrap">
				  <div class="sml_wrap">
						<ul>
							<% If PRs("gd_img_path") <> "gdsimage/listgdsnull1.jpg" Then %>
								<li><img id="s_img1" style="cursor:pointer; width:60px; height:60px;" <% If left(PRs("GD_IMG_PATH"),8) = "gdsimage" or left(PRs("GD_IMG_PATH"),9) = "/gdsimage"  Then %> src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" <% Else %> src="<%=PRs("GD_IMG_PATH")%>" <% End if%> onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End If %>
							<% If PRs("gd_img_path_2") <> "gdsimage/listgdsnull1.jpg" Then %>  
								<li><img id="s_img2" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_2")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End If %>
							
							<% If PRs("gd_img_path_3") <> "gdsimage/listgdsnull1.jpg" Then %> 
								<li><img id="s_img3" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_3")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End if%>
							
							<% If PRs("gd_img_path_4") <> "gdsimage/listgdsnull1.jpg" Then %>  
								<li><img id="s_img4" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_4")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End if%>
								<!--
								<% if GetsLOGINID = "changbom0" or GetsLOGINID = "yskim" or GetsLOGINID = "lgtnt00" or GetsLOGINID = "soworld" or GetsLOGINID = "vicks1215" or GetsLOGINID = "jasmine123" or GetsLOGINID = "mhkim" or GetsLOGINID = "psyche6927" or GetsLOGINID = "shkim" or GetsLOGINID = "lgtnt00" or GetsLOGINID = "vicks1215" or GetsLOGINID = "hjbae" or GetsLOGINID = "imsuzie2" or GetsLOGINID = "yoonjin712"  or GetsLOGINID = "yyj" or GetsLOGINID = "starsu72" or GetsLOGINID = "csg3268"   then %>
										<li><img style="display:inline-block; cursor:pointer;" src="images/sub/de_btn_delete.jpg" onclick="product_process('del');"></li>
										<li><img style="display:inline-block; cursor:pointer;" src="images/sub/de_btn_close.jpg" onclick="product_process('open_no');"></li>
										<% end if %>
								-->
						</ul>
				  </div>
				  <!--제안서폴더담기-->
				  <% If GetsUNIGRPCD = "2001" Then %>
				  <div>
						<div>
							<a href="javascript:folderCopy();"><img style="margin-left:30px;" src="http://www.itemcenter.co.kr/renew_ichp/images/bt_pro_input.jpg"></a>
							</div>
							<div style="margin-top:6px;">
							<a href="shopping_basket2.asp"><img style="margin-left:30px;" src="http://www.itemcenter.co.kr/renew_ichp/images/bt_pro_open.jpg"></a>
					  </div>
				  </div>
				  <% End If %>
				</div>
			</div><!-- img_cont e -->
			<div class="product_cont">
				<form method="post" action="">
				<div class="product_option">
					<% If PRs("gd_grp_mid") ="005" And PRs("gd_grp_detl") ="0001" And Right(PRs("gd_nm"),2) = "AP"  Then %>
						<div style="font-size:14px;">
							<b>BESPOKE 냉장고는 하기 제품 이외에 다양한 색상 조합으로 구매 가능합니다!</b>
						</div>
						<br>
					<% End If %>
					<% If PRs("gd_grp_mid") ="005" And PRs("gd_grp_detl") ="0002" And Right(PRs("gd_nm"),2) = "AP"  Then %>
						<div style="font-size:14px;">
							<b>BESPOKE 냉장고는 하기 제품 이외에 다양한 색상 조합으로 구매 가능합니다!</b>
						</div>
						<br>
					<% End If %>
					<p class="tit"><%=PRs("gd_nm")%><br>
					<font size="2" color="#1428a0">
						
					</font>
					</p>
					<% If PRs("GD_OPEN_YN") ="Y"  Then %>
						<div class="soldout_detail" style="font-size:30px;">
							※ 지정된 수량 판매 완료입니다.
						</div>
					<% ElseIf  PRs("GD_GRP_MID") = "006" then %>
						<div class="soldout_detail" style="font-size:20px;">
							해당상품은 지정행사매장에 문의 부탁드립니다.
						</div>
						<dl class="shipping">
							<dt>추가혜택</dt>
							<dd>
								<% If seq ="392719" Then %>
									※ BESPOKE 키친핏 냉장고 (RF61R91C301) 동시 구매시 60만원 추가 혜택<br>
								<% End If %>
								<% if CLng(PRs("GD_PRICE3")) >= 1000000 Then %>
								※ 삼성카드 플러스페이 100만원이상 결제시 추가혜택 증정<BR>
								<% End if%>
								<!--※ 삼성전자 멤버십 회원가입 및 삼성카드 발급은 지정 행사매장에서 신청 가능합니다.-->
							</dd>
						</dl>
					<% Else %>
					<br>
					
					<dl class="price">
						<dt>표시가</dt>
						<dd><strike><b><%=FormatNumber(PRs("GD_PRICE8"),0)%></b>원</strike></dd>
					</dl>
					<dl class="sale_price">
						<dt>
							<% If seq="392722" Then %>
								한정특가	
							<% Else %>
								혜택가	
							<% End If %>
						</dt>
						<% If seq ="368898" Then %>
							<dd><b id="inprice" name="inprice">2,434,000</b>원 <!--span>(<%=FormatNumber(CLng(PRs("GD_PRICE8"))-CLng(PRs("GD_PRICE3")),0)%>원 할인)</span-->
						<% Else %>
							<dd><b id="inprice" name="inprice"><%=FormatNumber(PRs("GD_PRICE3"),0)%></b>원 <!--span>(<%=FormatNumber(CLng(PRs("GD_PRICE8"))-CLng(PRs("GD_PRICE3")),0)%>원 할인)</span-->
						<% End if%>
						<br>
							<% If seq="368898" Then %>
								<div style="line-height:18px; font-size:15px;">임직원우대할인+멤버십포인트적용<br>삼성카드결제조건(플러스페이적용가)</div>
							<% Else %>
								<font size="2">(임직원 우대 할인+멤버십포인트 적용)</font></b>
							<% End If %>
						</dd>
					</dl>

					<!--dl class="price">
						<dt>표시가</dt>
						<dd><strike><b>0</b>원</strike></dd>
					</dl>
					<dl class="sale_price">
						<dt>혜택가</dt>
						<dd><b id="inprice" name="inprice">0</b>원 <span>(0원 할인)</span></dd>
					</dl-->
						<!--dl class="shipping">
							<dt>구매혜택</dt>
							<dd><%=FormatNumber(CLng(PRs("GD_PRICE3"))/1000,0) %> P (구매금액의 0.1% 포인트적립)</dd>
						</dl-->
					
					<dl class="shipping">
						
						<% If seq<> "368898" Then %>
							<% If seq ="392719" Then %>
								<dt>추가혜택</dt>
								<dd>
									※ BESPOKE 키친핏 냉장고 (RF61R91C301) 동시 구매시 60만원 추가 혜택<br>
									<!--※ 삼성전자 멤버십 회원가입 및 삼성카드 발급은 지정 행사매장에서 신청 가능합니다-->
								</dd>
							<% End If %>
							<% if CLng(PRs("GD_PRICE3")) >= 1000000 Then %>
								<dt>추가혜택</dt>
								<dd>
									※ 삼성카드 플러스페이 100만원이상 결제시 추가혜택 증정<BR>
									<!--※ 삼성전자 멤버십 회원가입 및 삼성카드 발급은 지정 행사매장에서 신청 가능합니다-->
								</dd>
							<% End If %>
						<% End If %>


					</dl>
					<% End If %>
<!-- 					<dl> -->
<!-- 						<dt>주문수량</dt> -->
<!-- 						<dd>1개</dd> -->
<!-- 					</dl> -->
					<%
					  If OptionRs(0,0) <> "NODATA" Then
					%>
					<dl class="option_wrap">
						<dt>옵션</dt>
						<dd>
							<input type="hidden" name="spnopt1" id="spnopt1" value="<%=PRs("GD_OPTION1")%>">
							<input type="hidden" name="spnopt2" id="spnopt2" value="<%=PRs("GD_OPTION2")%>">
							<div class="select_box">
								<label for="sch">
								<% If OptionRs(13,t) = "" And OptionRs(14,t) <> "" Then %>
									<%=OptionRs(14,t)%>
								<% ElseIf OptionRs(13,t) <> "" And OptionRs(14,t) = ""  Then %>
									<%=OptionRs(13,t)%>
								<% Else %>
									<%=OptionRs(13,t)%> / <%=OptionRs(14,t)%>
								<% End If %>
								
								</label>

								<select name="ViewOption" id="ViewOption" onChange="javascript:Gd_Change(this.value);">
								<%
									For t = 0 To Ubound(OptionRs,2)
										GD_SEQ = OptionRs(0,t)
										GD_IMG_PATH = OptionRs(8,t)
										GD_IMG_PATH_2 = OptionRs(9,t)
										GD_IMG_PATH_3 = OptionRs(10,t)
										GD_IMG_PATH_4 = OptionRs(11,t)
										GD_IMG_PATH2 = OptionRs(12,t)
										GD_OPTION1 = OptionRs(13,t)
										GD_OPTION2 = OptionRs(14,t)
										GD_MIN_VOL = OptionRs(15,t)
										GD_OPEN_YN = OptionRs(16,t)
										GD_PRICE = OptionRs(17,t)
										Sell_Price = OptionRs(18,t)
										GD_PER = OptionRs(21,t)
								%>
									
									<option value="<%=GD_SEQ%>,<%=GD_PRICE%>,<%=GD_OPTION1%>,<%=GD_OPTION2%>,<%=GD_MIN_VOL%>,<%=GD_IMG_PATH%>,<%=GD_IMG_PATH_2%>,<%=GD_IMG_PATH_3%>,<%=GD_IMG_PATH_4%>,<%=GD_IMG_PATH2%>" <%If  CLng(GD_SEQ) = CLng(SEQ) Then %>selected<%End If %>>
										<% If OptionRs(13,t) = "" And OptionRs(14,t) <> "" Then %>
											<%=OptionRs(14,t)%>
										<% ElseIf OptionRs(13,t) <> "" And OptionRs(14,t) = ""  Then %>
											<%=OptionRs(13,t)%>
										<% Else %>
											<%=OptionRs(13,t)%> / <%=OptionRs(14,t)%>
										<% End If %>
									</option>
								<%
									Next
								%>
								</select>
								
							</div><!-- select_box e -->
						</dd>
					</dl>
					<% End If %>
					<!--dl class="amount_wrap">
						<dt>수량</dt>
						<dd>
							<div class="amount_cont">
								<button class="minus" type="button" onclick="javascript:minus('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');"><span></span></button>
								<label for="" class="hide">수량</label>
								<input type="text" name="GD_VOL" id="GD_VOL" class="input_amount" title="" value="<%=PRs("GD_MIN_VOL")%>"  onKeyUp="javascript:P_Sum('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');" onBlur="chk_min_vol('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>')" onKeyPress="chk_Number();"/>
								<button class="plus"  type="button" onclick="javascript:plut('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');">+</button>
							</div>
						</dd>
					</dl-->
				
				</div><!-- product_option e -->
				<%
					Total_Price = CDbl(PRs("GD_MIN_VOL")) * CDbl(PRs("GD_PRICE3"))
				%>
				<% If PRs("GD_OPEN_YN") ="Y"  Then %>
				
				<% Else %>
				
					<div class="total_price">
						<dl class="total_txt">
							<dd>
								<% If PRs("GD_NM") ="DM530ABE-L34" Or PRs("GD_NM") ="NT950SBE-X716" Or PRs("GD_NM") ="NT950SBE-X58" Or PRs("GD_NM") ="NT950SBE-K716" Or PRs("GD_NM") ="NT930SBE-K716"  Or PRs("GD_NM") ="NT930SBE-K58" Or PRs("GD_NM") ="NT950SBE-K58" Then %>
								<b id="ToTal_Price" style="font-size:20px;">※사은품은 제품 배송완료 2~3주 후 별도 발송됩니다.</b><br>
								<% End If %>

								<% If PRs("seq") ="393148" Or PRs("seq") ="393149" Then %>
									<b id="ToTal_Price" style="font-size:20px;">※해당상품은 10월 중순부터 순차적으로 발송됩니다.</b><br>
						
								<% End If %>


								<b id="ToTal_Price">
									※ 행사매장限 구매 가능, 상단 ‘지정행사 매장찾기’ 확인 <BR>
									※ 매장 방문하시면 더많은 혜택이 있습니다.<BR>
									※ 삼성전자멤버십 회원가입 및 삼성카드 발급은 행사매장에서 신청<BR>									
									※ 삼성로지텍 전문기사님이 배송 및 설치해드립니다.
									<% If seq="390656" Then %>
									<BR>※ 삼성카드 플러스페이 100만원이상 결제시 추가혜택 증정
									<% End If %>
									<% If seq="392722" Then %>
									<BR>※ 삼성카드 플러스페이 적용가입니다.
									<% End If %>
								</b>
							</dd>
							
						</dl>
					</div><!-- total_price e -->
				
				<% End If %>
				<!--div class="btn_wrap">
					<a href="javascript:inCart();"  class="btn btn_b">장바구니</a>
					<a href="javascript:inOrder();" class="btn btn_b btn_agree">바로주문</a>
				</div--><!-- btn_wrap e -->
				</form>
			</div><!-- product_cont e -->

		</div><!-- product_wrap e -->

		<div class="detail_info_wrap">
			<div class="tab_wrap">
				<a href="#none" rel="tab01" class="active">제품상세정보</a>
				<!--a href="javascript:void(0); " rel="tab03">상품 Q&A</a-->
				<!--a href="#none" rel="tab02">배송/교환/반품/환불</a-->
			</div>
			<div id="tab01" class="tab_cont detail_cont">
				<% If  seq= "379101" Or  seq= "366322"  Then %>
					<iframe class="iframe_terms" src="/youtube_ver1.asp"  style="height:400px;" name=ce frameborder=0></iframe>
					<iframe class="iframe_terms" src="/youtube_ver2.asp"  style="height:400px;" name=ce frameborder=0></iframe>
				<% End If %>
				<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH2")%>" alt="" id="imglistdetl"/>
			</div>
			<div id="tab02" class="tab_cont delivery_cont">
				<div class="dbt">
          <div class="dbti"><a name="go_info">배송안내 &gt;</a></div>
          <div>
            <ul class="row">
              <li class="dbt_title col">배송지역</li>
              <li class="dbt_cont col">전국</li>
			  
            </ul>
          </div>
          <!--div>
            <ul class="row">
              <li class="dbt_title col">배송비</li>
              <li class="dbt_cont col">택배배송 기준 30,000원 미만 상품 구매 시에는 배송비 2,500원이 고객부담이며, 30,000원 이상 상품 구매 시에는 배송비가 무료입니다.   (구매합계액 기준 / 제주도 및 기타 도서,산간 지역은 별도 배송비가 발생합니다.) 해외배송 / 퀵 배송일 경우 배송비는 배송지와 중량에 따라 별도 요금이 추가 계산됩니다. </li>                     
            </ul>
          </div-->                      
          <div>
            <ul class="row">
              <li class="dbt_title col">배송기간</li>
              <li class="dbt_cont col">결제일 다음날부터 평균 1~3일 (토요일, 일요일 및 공휴일 제외) 소요며, 산간지방 및 섬 지역은 토요일, 일요일 및 공휴일 제외 
              평균 3~7일 소요됩니다. 대금지급 방법이 무통장 입금일 경우, 입금 확인된 날부터 계산됩니다. 주문폭주, 천재지변, 택배사 사정으로 배송과정에 차질이 발생한 경우 등에는 배송이 지연될 수 있음을 양지해 주시기 바랍니다. 고객님의 배송정보 오류기재로 발생한 배송 지연은 책임질 수 없으니, 정확한 정보를 기재바랍니다.
              </li>                      
            </ul>
          </div>              
          <div>
            <ul class="row">
              <li class="dbt_title col">주의사항</li>
              <li class="dbt_cont col">퀵 / 해외배송 / 희망 배송일 지정 등 고객의 별도 요청에 따른 배송 시 배송 기일과 배송비는 변경될 수 있습니다.</li>                      
            </ul>
          </div>                   
          <div>
            <ul class="row">
              <li class="dbt_title col">배송조회</li>
              <li class="dbt_cont col">상단 주문배송조회 에서 주문한 상품의 현재 배송상황을 확인할 수 있습니다
              </li>
            </ul>
          </div>             
        </div>

				<div class="dbt">
          <div class="dbti">교환/반품 안내 &gt;</div>
          <div>
            <ul class="row">
              <li class="dbt_title col">반품기준</li>
              <li class="dbt_cont col">‘전자상거래 등에서의 소비자보호에 관한 법률’에 의거, 상품 인도 후 7일 이내에 다음의 사유에 의한 교환, 반품 및 환불을 보장하고 있습니다. 단, 고객의 단순한 변심으로 교환,반품 및 환불을 요구할 때 수반되는 배송비는 고객님께서 부담하셔야 합니다. 상품을 개봉했거나 설치한 후에는 상품의 재표시가 불가능하므로 고객님의 변심에 의한 교환,반품이 불가능함을 양지해 주시기 바랍니다 
              </li>
            </ul>
          </div>
          <div>
            <ul class="row">
              <li class="dbt_title col">교환 및 반품이 가능한 경우</li>
              <li class="dbt_cont col">배송된 상품이 주문내용과 상이하거나 본 사이트에서 제공한 정보와 상이할 경우 배송된 상품 자체의 이상 및 결함이 있을 경우. 배송된 상품이 파손, 손상되었거나 오염되었을 경우
              </li>                     
            </ul>
          </div>                      
          <div>
            <ul class="row">
              <li class="dbt_title col">교환 및 반품이 불가능한 경우</li>
              <li class="dbt_cont col">고객님의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우. 상품의 고유 포장이 훼손되어 상품가치가 상실된 경우 ( 제품손상 , 스크래치 등) 고객님의 사용, 또는 일부 소비에 의하여 상품의 가치가 감소한 경우. 재표시가 곤란할 정도로 상품 등의 가치가 감소한 경우. 한라로고 인쇄 상품 및 주문제작 상품일 경우.
              </li>                      
           </ul>
          </div>              
          <div>
            <ul class="row">
              <li class="dbt_title col">교환/반품 배송비 배송기간</li>
              <li class="dbt_cont col">고객님의 단순변심으로 인한 교환/반품인 경우, 왕복 배송비 및 상품 회수/배송에 필요한 비용을 고객님께서 부담하셔야 합니다.
              상품의 색상 및 모델 변경과 반품 교환 시에는 고객님이 왕복 배송비를 부담하셔야 하며, 배송비는 상품에 따라 다를 수 있습니다.
              상품의 불량/하자, 표시 광고 및 내용과 다르게 이행되어 교환/반품을 하시는 경우 교환/반품 비용은 무료입니다. 
              </li>                      
            </ul>
          </div>                   
          <div>
            <ul class="row">
              <li class="dbt_title col">보상/분쟁 처리 기준</li>
              <li class="dbt_cont col">고객님께서 가지신 정당한 의견이나 불만을 반영하고, 피해보상을 처리해드리기 위해 고객센터를 운영하고 있으며, 
              불만사항 및 의견은 우선적으로 처리합니다. 다만, 신속히 처리가 곤란한 경우에는 고객님께 그 사유와 처리일정을 즉시 통보해 드립니다.
              </li>
            </ul>
          </div>             
        </div>
				<div class="dbt">
          <div class="dbti">환불안내 &gt;</div>
          <div>
            <ul class="row">
              <li class="dbt_title col">환불기준</li>
              <li class="dbt_cont col">소비자 보호 규정에 의거하여 주문의 취소일 또는 재화 등을 반환 받은 날로부터 영업일 기준 7일 이내에 결제 금액을 환불해 드립니다. 신용카드로 결제하신 경우 신용카드 승인을 취소하여 결제 대금이 청구되지 않게 합니다. 단, 신용카드 결제일자에 맞추어 대금이 청구될 경우, 익월 신용카드 대금 청구 시 카드사에서 환급 처리 됩니다. 무통장 입금의 경우에는 주문의 취소 혹은 제품 회수 후, 입금계좌가 확인되면 7일 이내에 환불해 드립니다. (토요일, 일요일 및 공휴일 제외) 
             </li>
            </ul>
          </div>
        </div>			
			</div>


		</div><!-- product_info_wrap e -->
	</div><!-- detail_page e -->
</div><!-- container sub e -->
</form>
<!-- #include virtual = "/include/footer.asp" -->
