<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	SEQ			= fInject(Request("SEQ"))
	gd_grp_mid	= fInject(Request("gd_grp_mid"))
	gd_grp_detl	= fInject(Request("gd_grp_detl"))

	If SEQ = "" Then
		Response.Write "<script>alert('해당 상품의 상세페이지가 존재하지 않습니다.');history.back();</script>"
		Response.End
	Else
		ChkSQL = "SELECT Count(GI.SEQ) FROM IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
		ChkSQL = ChkSQL & " ON GI.SEQ = OG.GD_SEQ "
		ChkSQL = ChkSQL & " WHERE "
		'몰코드
		ChkSQL = ChkSQL & " OG.ONLINE_CD = '" & GLOBAL_VAR_ONLINECD & "' "
		ChkSQL = ChkSQL & " AND GI.GD_BUY_END_YN = 'N' "
		'ChkSQL = ChkSQL & " AND GI.GD_OPEN_YN = 'N' "
		ChkSQL = ChkSQL & " AND GI.DEL_YN='N' "
		ChkSQL = ChkSQL & " AND OG.DEL_YN='N' "
		ChkSQL = ChkSQL & " AND OG.UNI_GRP_CD = '2001' "
		ChkSQL = ChkSQL & " AND GI.SEQ = '" & SEQ & "' "
		'Res	ponse.write ChkSQL
		DBopen()
			Set ChkRs = DBcon.Execute(ChkSQL)
		
		If ChkRs(0) = 0 Then 
			Response.Write "<script>alert('판매종료 되었거나 삭제된 상품입니다.');history.back();</script>"
			Response.End
		End If

	End If

	Set PRs = Product_View(GLOBAL_VAR_ONLINECD,"",SEQ,"")

	GD_NM = PRs("GD_NM")
	GD_Memo = replace(trim(replace(Prs("GD_Memo"),"＆","&")) ," ","")

	If GD_NM = "" Then
		Response.Write "<script>alert('해당 상품의 상세페이지가 존재하지 않습니다.');history.back();</script>"
		Response.End
	End If


	'상품 카운팅 증가 IC_T_ONLINE_GDS
	Product_View_COUNT(PRs("OG_SEQ"))

	'상품 옵션리스트 함수
	'몰코드,상품명
	OptionRs = Option_View(GLOBAL_VAR_ONLINECD,GD_NM,"")

	DBclose()

	strqry61 = " insert into tblProduct_mall_ViewLog ( "
	strqry61 = strqry61 & " UserID,UserGubun,PageName,GD_CATE,PRICE_GB, "
	strqry61 = strqry61 & " GD_GRP_MID,GD_GRP_DETL,Depth,ProductNo,WriteDate,WriteIP ,online_cd"
	strqry61 = strqry61 & " ) values ( "
	strqry61 = strqry61 & " '"& GetsLOGINID &"', "
	strqry61 = strqry61 & " '', "
	strqry61 = strqry61 & " 'sub_detail.asp', "
	strqry61 = strqry61 & " '"& GD_CATE &"', "
	strqry61 = strqry61 & " '', "
	strqry61 = strqry61 & " '"& GD_GRP_MID &"', "
	strqry61 = strqry61 & " '"& GD_GRP_DETL &"', "
	strqry61 = strqry61 & " '3', "
	strqry61 = strqry61 & " '"& seq &"', "
	strqry61 = strqry61 & " getdate(), "
	strqry61 = strqry61 & " '"& Request.ServerVariables("REMOTE_HOST") &"', "
	strqry61 = strqry61 & " '"& GLOBAL_VAR_ONLINECD &"' "
	strqry61 = strqry61 & " ) "

	DBopen()

	DBcon.Execute(strqry61)

	DBclose()
%>
<script language='javascript'>
	function minus(Seq,Min_Vol)
	{
		if (document.getElementById("GD_VOL").value <= Min_Vol )
		{
			alert('최소 주문수량 이하의 수량은 선택하실 수 없습니다.');
			return;
		}
		document.getElementById("GD_VOL").value = Number(document.getElementById("GD_VOL").value) - 1 

		P_Sum(Seq,Min_Vol)
	}

	function plut(Seq,Min_Vol)
	{
		//alert('준비중입니다');
		//return;
		document.getElementById("GD_VOL").value = Number(document.getElementById("GD_VOL").value) + 1
		
		P_Sum(Seq,Min_Vol)
	}


	//상품 옵션 클릭 변경
	//상품번호,상품가격,옵션1,옵션2최소주문수량,대표이미지,대표이미지2,대표이미지3,대표이미지4,상세이미지
	function Gd_Change(Obj){
		ArrayObj = Obj.split(",")
		Gd_Seq        = ArrayObj[0]
		Gd_Price      = ArrayObj[1]
		Gd_Option1    = ArrayObj[2]
		Gd_Option2    = ArrayObj[3]
		Gd_Min_Vol    = ArrayObj[4]
		Gd_Img_Path   = ArrayObj[5]
		Gd_Img_Path_2 = ArrayObj[6]
		Gd_Img_Path_3 = ArrayObj[7]
		Gd_Img_Path_4 = ArrayObj[8]
		Gd_Img_Path2  = ArrayObj[9]
		//상품코드변경
	//	document.getElementById("GD_SEQ").value = Gd_Seq+"|";
		document.getElementById("GD_SEQ").value = Gd_Seq;

		//최소구매수량
		document.getElementById("GD_MIN_VOL").value = Gd_Min_Vol;

		//구매갯수 최소 수량상품으로 셋팅
		//document.getElementById("GD_VOL").value = Gd_Min_Vol;

		//가격변경
		document.getElementById("inprice").innerHTML = addcomma(Gd_Price) ;
		document.getElementById("Price").value = Gd_Price;
		//구매합계수량 변경
		var Total_Price = Gd_Price * Gd_Min_Vol

		document.getElementById("ToTal_Price").innerHTML = addcomma(Total_Price) ;
		

		//히든옵션1변경
		document.getElementById('spnopt1').value = Gd_Option1;
		//히든옵션2변경
		document.getElementById('spnopt2').value = Gd_Option2;

		document.getElementById('image_point').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path;
		<% If PRs("gd_img_path") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img1').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path;
		<% end if  %>
		<% If PRs("gd_img_path_2") <> "gdsimage/listgdsnull1.jpg" Then %>  
			document.getElementById('s_img2').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_2;
		<% end if%>
		<% If PRs("gd_img_path_3") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img3').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_3;
		<% end if%>
		<% If PRs("gd_img_path_4") <> "gdsimage/listgdsnull1.jpg" Then %>
			document.getElementById('s_img4').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path_4;
		<% end if%>
		document.getElementById('imglistdetl').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path2;
		
		//image_p('1');
	}

	//천단위 콤마
	function addcomma(n) 
	{
	  var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
	  n += '';                          // 숫자를 문자열로 변환

	  while (reg.test(n))
		n = n.replace(reg, '$1' + ',' + '$2');

	  return n;
	}

	function Gd_Change2(Obj){
		ArrayObj	  = Obj.split("!!")
		Gd_Seq        = ArrayObj[0]
		Gd_Price      = ArrayObj[1]
		Gd_Option1    = ArrayObj[2]
		Gd_Option2    = ArrayObj[3]
		Gd_Min_Vol    = ArrayObj[4]
		Gd_Img_Path   = ArrayObj[5]
		Gd_Img_Path_2 = ArrayObj[6]
		Gd_Img_Path_3 = ArrayObj[7]
		Gd_Img_Path_4 = ArrayObj[8]
		Gd_Img_Path2  = ArrayObj[9]

		//상품코드변경
	//	document.getElementById("GD_SEQ").value = Gd_Seq+"|";
		document.getElementById("GD_SEQ").value = Gd_Seq;

		//최소구매수량
		document.getElementById("GD_MIN_VOL").value = Gd_Min_Vol;

		//구매갯수 최소 수량상품으로 셋팅
		document.getElementById("GD_VOL").value = Gd_Min_Vol;

		//가격변경
		document.getElementById("inprice").innerHTML = addcomma(Gd_Price) ;
		document.getElementById("Price").value = Gd_Price;
		//구매합계수량 변경
		var Total_Price = Gd_Price * Gd_Min_Vol

		document.getElementById("ToTal_Price").innerHTML = addcomma(Total_Price) ;
		

		//히든옵션1변경
		document.getElementById('spnopt1').value = Gd_Option1;
		//히든옵션2변경
		document.getElementById('spnopt2').value = Gd_Option2;
		document.getElementById('imglistdetl').src = "http://www.itemcenter.co.kr/" + Gd_Img_Path2;
		document.getElementById('option_gd_nm').innerHTML = Gd_Option1; 
		//image_p('1');
	}

	//천단위 콤마
	function addcomma(n) {
	  var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
	  n += '';                          // 숫자를 문자열로 변환

	  while (reg.test(n))
		n = n.replace(reg, '$1' + ',' + '$2');

	  return n;
	}

	//상품수량변경
	function P_Sum(Seq,Min_Vol){
		/*
		'============================================================================================
		'============================================================================================
		상품의 수량은 배열로 받기때문에 getElementsByName를 써야함.
		//해당 상품의 수량
		var P_EA = document.getElementsByName("P_EA")[i];
		'============================================================================================
		'============================================================================================
		*/
		//해당 상품의 수량
		var GD_VOL = document.getElementById("GD_VOL");
		//해당 상품의 개당 단가
		var Price = document.getElementById("Price");
		//해당상품의 합계금액
		var P_Sum     = document.getElementById("ToTal_Price");

		/*
		if(GD_VOL.value==0 || GD_VOL.value==""){ 
			GD_VOL.value = Min_Vol;
		}
		*/
		//합계
		P_Sum.innerHTML     = addcomma(GD_VOL.value*Price.value) 
		//총 결제금액 구하기
	//	Total_Sum()	
	}


	function chk_min_vol(Seq,Min_Vol){
		var GD_VOL = document.getElementById("GD_VOL");
		if(Number(GD_VOL.value)<Number(Min_Vol)){
			GD_VOL.value = Min_Vol;
			alert("최소 주문수량은 ["+Min_Vol+"]개입니다.");
			//상품금액 재계산
			P_Sum(Seq,Min_Vol)
		}
	}

	//숫자입력체크
	function chk_Number(){
		if ((event.keyCode<48)||(event.keyCode>57)) event.returnValue=false;
	}

	//장바구니 담기==============================================================
	function inCart(){ //장바구니
		
		var f = document.Orderfrm;
		f.target = "iSQL"
		//f.target = "_blank"
		f.action = "/order/cart_ok.asp"
		f.submit();				
	}
	//관심상품 담기=============================================================	
	//상품주문체크

//주문서만 작성을 다시함(주문 안들어가는 버전)
function order_re_tel()
{
	alert('휴대폰 가입창으로 이동합니다.');
	document.Orderfrm.target="_blank"
	document.Orderfrm.action = '<%=GD_Memo%>';
	document.Orderfrm.submit();
	confirm_close();
	hp_close_2();
}

function inOrder_dm(){
	
		//alert('준비중입니다');
		//return;
		var f = document.hp_frm2;
		var f2 = document.Orderfrm;
		var user_name = f.user_name2.value;
		var tel_user_tel1 = f.tel_user_tel12.value;
		var tel_user_tel2 = f.tel_user_tel22.value;
		var tel_user_tel3 = f.tel_user_tel32.value;
		var or_else_memo  = f.or_else_memo.value;
		var checkboxcpy	  = f.cust_checkboxcpy_dm2;
		//최소주문수량보다 적게 주문할경우
		if(Number(f2.GD_MIN_VOL.value)>Number(f2.GD_VOL.value)){
			alert("최소 주문수량보다 적은 수량은 구매하실수 없습니다");
			return false;
		}

		if (trim(user_name).length ==0 )
		{
			alert('성명을 입력해 해주시기 바랍니다');
			f.user_name.focus();
			return;
		}
		if (trim(tel_user_tel1).length ==0 )
		{
			alert('전화번호1을 입력해 해주시기 바랍니다');
			f.tel_user_tel1.focus();
			return;
		}
		if (trim(tel_user_tel2).length ==0 )
		{
			alert('전화번호1을 입력해 해주시기 바랍니다');
			f.tel_user_tel2.focus();
			return;
		}
		if (trim(tel_user_tel3).length ==0 )
		{
			alert('전화번호1을 입력해 해주시기 바랍니다');
			f.tel_user_tel3.focus();
			return;
		}
		if (trim(or_else_memo).length ==0 )
		{
			alert('소속회사를 입력해 주시기 바랍니다.');
			f.or_else_memo.focus();
			return;
		}
		
		if (checkboxcpy.checked ==false)
			{
				alert('약관동의에 체크해 주시기 바랍니다.');
				return;
			}


		var strAjaxUrl="/ajax/hp_count_ajax.asp"
		var retDATA="";
		//alert(strAjaxUrl);
		
		 $.ajax({
			 type: 'POST',
			 url: strAjaxUrl,
			 dataType: 'html',
			 data: {
				GD_SEQ:encodeURI(f2.GD_SEQ.value),
				GD_VOL:encodeURI(f2.GD_VOL.value),
				tel_user_name:user_name,
				user_name:user_name,
				tel_user_tel1:tel_user_tel1,
				tel_user_tel2:tel_user_tel2,
				tel_user_tel3:tel_user_tel3,
				company_file:document.getElementById("company_file_2").value
			},						  
			success: function(retDATA) {
				if (retDATA == 0 )
				{
					if(confirm("대명상조 가입을 진행하시겠습니까?"))
					{
						//ie11 새로고침 오류로 변경
						f2.encoding = "application/x-www-form-urlencoded"; 
						f2.target="order_frame"
						f2.action="/order/order_tel.asp?Cart_YN=N&GD_SEQ="+encodeURI(f2.GD_SEQ.value)+"&GD_VOL="+encodeURI(f2.GD_VOL.value)+"&tel_user_name="+user_name+"&tel_user_tel1="+tel_user_tel1+"&tel_user_tel2="+tel_user_tel2+"&tel_user_tel3="+tel_user_tel3+ "&or_else_memo="+document.getElementById("or_else_memo").value;
						f2.submit();
						/*f.action="../order/order.asp"
						f.submit();*/	
						
						return;
					}
				}
				else
				{
					//if(confirm("휴대폰 가입하기를 이미 진행하신 이력이 있습니다.\n한번더 가입하시겠습니까?"))
					//{
						document.getElementById("dm_popup02").style.display="block"
						location.href="#dm_popup02"
						////ie11 새로고침 오류로 변경
						//f2.target="order_frame"
						//f2.action="/order/order_tel.asp?Cart_YN=N&GD_SEQ="+encodeURI(f2.GD_SEQ.value)+"&GD_VOL="+encodeURI(f2.GD_VOL.value)+"&tel_user_name="+user_name+"&tel_user_tel1="+tel_user_tel1+"&tel_user_tel2="+tel_user_tel2+"&tel_user_tel3="+tel_user_tel3;
						//f2.submit();
						///*f.action="../order/order.asp"
						//f.submit();*/
						//return;
					//}
				}
				
			 }
		 }); //close $.ajax(
	}

	function inOrder_dm_ok()
	{
		var f = document.hp_frm;
		var f2 = document.Orderfrm;
		var user_name = f.user_name.value;
		var tel_user_tel1 = f.tel_user_tel1.value;
		var tel_user_tel2 = f.tel_user_tel2.value;
		var tel_user_tel3 = f.tel_user_tel3.value;
		var checkboxcpy	  = f.cust_checkboxcpy_hp;
		f2.target="order_frame"
		f2.action="https://b2bc.samsungbizmall.com/order/order_tel.asp?Cart_YN=N&GD_SEQ="+encodeURI(f2.GD_SEQ.value)+"&GD_VOL="+encodeURI(f2.GD_VOL.value)+"&tel_user_name="+user_name+"&tel_user_tel1="+tel_user_tel1+"&tel_user_tel2="+tel_user_tel2+"&tel_user_tel3="+tel_user_tel3;
		f2.submit();
		//location.reload();
		confirm_close();
		document.getElementById("dm_popup").style.display ='none'
	}

	function inOrder(){
		//alert('준비중입니다');
		//return;
		var f = document.Orderfrm;
		//최소주문수량보다 적게 주문할경우
		if(Number(f.GD_MIN_VOL.value)>Number(f.GD_VOL.value)){
			alert("최소 주문수량보다 적은 수량은 구매하실수 없습니다");
			return false;
		}
		//ie11 새로고침 오류로 변경
		f.action="https://b2bc.samsungbizmall.com/order/order.asp?Cart_YN=N&GD_SEQ="+encodeURI(f.GD_SEQ.value)+"&GD_VOL="+encodeURI(f.GD_VOL.value);
		f.submit();
		/*f.action="../order/order.asp"
		f.submit();*/
	}
function originimg(obj) {
//document.all.img.src="./images/products_img/content_img/<%=p_common_img%>";
document.getElementById("image_point").src=obj.src;
}

function swapimg(obj) {
//document.all.img.src=obj.src;
document.getElementById("image_point").src=obj.src;
}


function qna_write()
{
	document.getElementById("qna_list_div").style.display = 'none';
	document.getElementById("qna_write_div").style.display = 'block';

}

function trim(str) {
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}


function qna_write_ok(seq)
{
	var ch01 = document.getElementById("ch01").checked;
	var ch02 = document.getElementById("ch02").checked;
	var ch03 = document.getElementById("ch03").checked;
	var ch04 = document.getElementById("ch04").checked;
	var open_yn = document.getElementById("open_yn").checked; 
	var con_text = document.getElementById("con_text").value;  
			
	if(trim(con_text).length == 0)
		{
			alert('내용을 입력해주시기 바랍니다.');
			document.getElementById("con_text").focus();
			return;
		}


	if (ch01 == true)
	{
		ch_gb = "product"
	}
	if (ch02 == true)
	{
		ch_gb = "order"
	}
	if (ch03 == true)
	{
		ch_gb = "cencle"
	}
	if (ch04 == true)
	{
		ch_gb = "etc"
	}

	if (open_yn== true)
	{
		open_yn = "Y"
	}
	else
	{
		open_yn = "N"
	}
	var strAjaxUrl="/ajax/qna_write_ok.asp"
	var retDATA="";
	//alert(strAjaxUrl);
	
	 $.ajax({
		 type: 'POST',
		 url: strAjaxUrl,
		 dataType: 'html',
		 data: {
			qna_type:ch_gb,
			open_yn:open_yn	,
			con_text:con_text,
			gd_seq:seq
		},						  
		success: function(retDATA) {
			qna_search('1','5','PAGING','no');
			con_text = "";
		 }
	 }); //close $.ajax(
}

function qna_search(blockpage,ViewCnt,PAGING,gb)
{
	var strAjaxUrl="/ajax/qna_ajax_list.asp"
	var retDATA="";
	//alert(strAjaxUrl);
	
	 $.ajax({
		 type: 'POST',
		 url: strAjaxUrl,
		 dataType: 'html',
		 data: {
			 now_page:blockpage
			,GD_SEQ:'<%= SEQ %>'
		},						  
		success: function(retDATA) {
			 document.getElementById("qna_list_div").style.display = 'block';
			 document.getElementById("qna_write_div").style.display = 'none';
			 document.getElementById("qna_list_div").innerHTML = retDATA;				 
		 }
	 }); //close $.ajax(	
}
function qna_delete(seq)
{	
	if(confirm("삭제 하시겠습니까?"))
	{
		var strAjaxUrl="/ajax/qna_delete_ok.asp"
		var retDATA="";
		//alert(strAjaxUrl);
		
		 $.ajax({
			 type: 'POST',
			 url: strAjaxUrl,
			 dataType: 'html',
			 data: {
				 seq:seq
			},						  
			success: function(retDATA) {
				qna_search('1','5','PAGING','no') 
			 }
		 }); //close $.ajax(
	}
}
</script>
<iframe src="" name="order_frame"  id="order_frame" frameborder=0 style="display:none;"></iframe>
<iframe src="" name="file_frame"  id="file_frame" frameborder=0 style="display:none;"></iframe>
<form method="post" name="Orderfrm">

<input type="hidden" id="company_file_2" name="company_file_2" value="">
<input type="hidden" id="gd_grp_mid" name="gd_grp_mid" value="<%=gd_grp_mid%>">
<input type="hidden" id="gd_grp_detl" name="gd_grp_detl" value="<%=gd_grp_detl%>">
<!--현재 선택 상품 번호-->
<input id="GD_SEQ" type="hidden" name="GD_SEQ" value="<%=SEQ%>" >
<!--현재 상품 최수 구매 수량-->
<input type="hidden" id="GD_MIN_VOL" name="GD_MIN_VOL" value="<%=PRs("GD_MIN_VOL")%>">
<!--현재 상품 카트에 담을시 카트 타입-->
<input type="hidden" id="CART_TYPE" name="CART_TYPE" value="Cart_Add">
<!--해당 상품의 현재 단가-->
<input type="hidden" id="Price" name="Pirce" value="<%=PRs("GD_PRICE3")%>">
<div class="container sub">
	<div class="location_wrap">
		<span><a href="/default.asp">HOME</a></span><i></i>
		<span><%=PRs("gd_nm")%></span>
	</div><!-- location_wrap e -->
	<div class="detail_page">
		<div class="product_wrap">
			<div class="img_cont">
				<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" alt="" id="image_point" name="image_point"/>
				<% If PRs("GD_OPEN_YN") ="Y"  Then %>
					<div class="soldout_detail_2">
						<span class="online">
							<label>SOLD OUT</label>
						</span>	
					</div>	
				<% End If %>	
				<div class="img_wrap">
				  <div class="sml_wrap">
						<ul>
							<% If PRs("gd_img_path") <> "gdsimage/listgdsnull1.jpg" Then %>
								<li><img id="s_img1" style="cursor:pointer; width:60px; height:60px;" <% If left(PRs("GD_IMG_PATH"),8) = "gdsimage" or left(PRs("GD_IMG_PATH"),9) = "/gdsimage"  Then %> src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" <% Else %> src="<%=PRs("GD_IMG_PATH")%>" <% End if%> onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End If %>
							<% If PRs("gd_img_path_2") <> "gdsimage/listgdsnull1.jpg" Then %>  
								<li><img id="s_img2" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_2")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End If %>
							
							<% If PRs("gd_img_path_3") <> "gdsimage/listgdsnull1.jpg" Then %> 
								<li><img id="s_img3" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_3")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End if%>
							
							<% If PRs("gd_img_path_4") <> "gdsimage/listgdsnull1.jpg" Then %>  
								<li><img id="s_img4" style="cursor:pointer; width:60px; height:60px;" src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH_4")%>" onMouseOver="swapimg(this)" onMouseOut="originimg(this)" /></li>
							<% End if%>
								<!--
								<% if GetsLOGINID = "changbom0" or GetsLOGINID = "yskim" or GetsLOGINID = "lgtnt00" or GetsLOGINID = "soworld" or GetsLOGINID = "vicks1215" or GetsLOGINID = "jasmine123" or GetsLOGINID = "mhkim" or GetsLOGINID = "psyche6927" or GetsLOGINID = "shkim" or GetsLOGINID = "lgtnt00" or GetsLOGINID = "vicks1215" or GetsLOGINID = "hjbae" or GetsLOGINID = "imsuzie2" or GetsLOGINID = "yoonjin712"  or GetsLOGINID = "yyj" or GetsLOGINID = "starsu72" or GetsLOGINID = "csg3268"   then %>
										<li><img style="display:inline-block; cursor:pointer;" src="images/sub/de_btn_delete.jpg" onclick="product_process('del');"></li>
										<li><img style="display:inline-block; cursor:pointer;" src="images/sub/de_btn_close.jpg" onclick="product_process('open_no');"></li>
										<% end if %>
								-->
						</ul>
				  </div>
				  <!--제안서폴더담기-->
				  <% If GetsUNIGRPCD = "2001" Then %>
				  <div>
						<div>
							<a href="javascript:folderCopy();"><img style="margin-left:30px;" src="http://www.itemcenter.co.kr/renew_ichp/images/bt_pro_input.jpg"></a>
							</div>
							<div style="margin-top:6px;">
							<a href="shopping_basket2.asp"><img style="margin-left:30px;" src="http://www.itemcenter.co.kr/renew_ichp/images/bt_pro_open.jpg"></a>
					  </div>
				  </div>
				  <% End If %>
				</div>
			</div><!-- img_cont e -->
			<div class="product_cont">
				<form method="post" action="">
				<div class="product_option">
					<p class="tit"><%=PRs("gd_nm")%></p>
					<% If PRs("GD_GRP_MID") <>  "011"  Then %> 
					<dl class="price">
						<dt>표시가</dt>
						<dd><strike><b><%=FormatNumber(PRs("GD_PRICE8"),0)%></b>원</strike></dd>
					</dl>
					<% End If %>
					<% If seq ="392722" Then %>
						<dl class="sale_price">
							<dt>한정특가</dt>
							<dd><b id="inprice" name="inprice"><%=FormatNumber(PRs("GD_PRICE3"),0)%></b>원 <!--span>(<%=FormatNumber(CLng(PRs("GD_PRICE8"))-CLng(PRs("GD_PRICE3")),0)%>원 할인)</span-->
							</dd>
						</dl>
					<%Else %>
						<dl class="sale_price">
							<dt>혜택가</dt>
							<% If PRs("GD_GRP_MID")  ="001" Then %>
								<dd><b id="inprice" name="inprice">100</b>원</dd>
							<% Else %>
								<dd><b id="inprice" name="inprice"><%=FormatNumber(PRs("GD_PRICE3"),0)%></b>원</dd>
							<% End If %>
						</dl>
					<% End If %>
					<% If PRs("GD_GRP_MID") <>  "011"  Then %> 
						<dl class="shipping">
							<dt>배송비</dt>
							<dd>무료배송</dd>
						</dl>
					<% End If %>
<!-- 					<dl> -->
<!-- 						<dt>주문수량</dt> -->
<!-- 						<dd>1개</dd> -->
<!-- 					</dl> -->
					<%
					  If OptionRs(0,0) <> "NODATA" Then
					%>
					<dl class="option_wrap">
						<dt>옵션</dt>
						<dd>
							<input type="hidden" name="spnopt1" id="spnopt1" value="<%=PRs("GD_OPTION1")%>">
							<input type="hidden" name="spnopt2" id="spnopt2" value="<%=PRs("GD_OPTION2")%>">
							<div class="select_box">
								<label for="sch">
								<% If OptionRs(13,t) = "" And OptionRs(14,t) <> "" Then %>
									<%=OptionRs(14,t)%>
								<% ElseIf OptionRs(13,t) <> "" And OptionRs(14,t) = ""  Then %>
									<%=OptionRs(13,t)%>
								<% Else %>
									<%=OptionRs(13,t)%> / <%=OptionRs(14,t)%>
								<% End If %>
								
								</label>

								<select name="ViewOption" id="ViewOption" onChange="javascript:Gd_Change(this.value);">
								<%
									For t = 0 To Ubound(OptionRs,2)
										GD_SEQ = OptionRs(0,t)
										GD_IMG_PATH = OptionRs(8,t)
										GD_IMG_PATH_2 = OptionRs(9,t)
										GD_IMG_PATH_3 = OptionRs(10,t)
										GD_IMG_PATH_4 = OptionRs(11,t)
										GD_IMG_PATH2 = OptionRs(12,t)
										GD_OPTION1 = OptionRs(13,t)
										GD_OPTION2 = OptionRs(14,t)
										GD_MIN_VOL = OptionRs(15,t)
										GD_OPEN_YN = OptionRs(16,t)
										GD_PRICE = OptionRs(17,t)
										Sell_Price = OptionRs(18,t)
										GD_PER = OptionRs(21,t)
								%>
									<option value="<%=GD_SEQ%>,<%=GD_PRICE%>,<%=GD_OPTION1%>,<%=GD_OPTION2%>,<%=GD_MIN_VOL%>,<%=GD_IMG_PATH%>,<%=GD_IMG_PATH_2%>,<%=GD_IMG_PATH_3%>,<%=GD_IMG_PATH_4%>,<%=GD_IMG_PATH2%>" <%If  CLng(GD_SEQ) = CLng(SEQ) Then %>selected<%End If %>>
										<% If OptionRs(13,t) = "" And OptionRs(14,t) <> "" Then %>
											<%=OptionRs(14,t)%>
										<% ElseIf OptionRs(13,t) <> "" And OptionRs(14,t) = ""  Then %>
											<%=OptionRs(13,t)%>
										<% Else %>
											<%=OptionRs(13,t)%> / <%=OptionRs(14,t)%>
										<% End If %>
									</option>
								<%
									Next
								%>
								</select>
								
							</div><!-- select_box e -->
						</dd>
					</dl>
					<% End If %>

					
					<dl class="amount_wrap" <% If PRs("GD_GRP_MID") = "011"  Then %> style="margin-top:20px;" <% End If %>>
						<dt>수량</dt>
						<dd>
							<div class="amount_cont">
								<button class="minus" type="button" onclick="javascript:minus('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');"><span></span></button>
								<label for="" class="hide">수량</label>
								<input type="text" name="GD_VOL" id="GD_VOL" class="input_amount" title="" value="<%=PRs("GD_MIN_VOL")%>"  onKeyUp="javascript:P_Sum('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');" onBlur="chk_min_vol('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>')" onKeyPress="chk_Number();"/>
								<button class="plus"  type="button" onclick="javascript:plut('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>');">+</button>
							</div>
						</dd>
					</dl>
				</div><!-- product_option e -->
				<% If PRs("GD_NM") ="DM530ABE-L34" Or PRs("GD_NM") ="NT950SBE-X716" Or PRs("GD_NM") ="NT950SBE-X58" Or PRs("GD_NM") ="NT950SBE-K716" Or PRs("GD_NM") ="NT930SBE-K716"  Or PRs("GD_NM") ="NT930SBE-K58" Or PRs("GD_NM") ="NT950SBE-K58" Then %>
				<div class="total_price">
					<dl class="total_txt">
						<dd>
							<b id="ToTal_Price" style="font-size:20px;">※사은품은 제품 배송완료 2~3주 후 별도 발송됩니다.</b>
						</dd>
					</dl>
				</div><!-- total_price e -->
				<% End If %>
				<% If PRs("seq") ="393148" Or PRs("seq") ="393149" Then %>
				<div class="total_price">
					<dl class="total_txt">
						<dd>
							<b id="ToTal_Price" style="font-size:20px;">※해당상품은 10월 중순부터 순차적으로 발송됩니다.</b>
						</dd>
					</dl>
				</div><!-- total_price e -->
				<% End If %>
				<%
					Total_Price = CDbl(PRs("GD_MIN_VOL")) * CDbl(PRs("GD_PRICE3"))
				%>
				<% If PRs("GD_GRP_MID") <>  "011"  Then %> 
				<div class="total_price">
					<dl>
						<dt>총 상품금액</dt>
						<dd><b id="ToTal_Price"><%=FormatNumber(Total_Price,0)%></b>원</dd>
						<!--dd><b id="ToTal_Price">0</b>원</dd-->
					</dl>
				</div><!-- total_price e -->
				<% End If %>
				<% If cust_tp_rs(User_SEQ) = "N" Then %>
					<div class="btn_wrap">
						<% If PRs("GD_GRP_MID") = "011"  Then %>
							<a href="#dm_popup2" class="btn btn_b btn_agree open" >
								가입상담신청
							</a>
						<% Else %>
							<a href="javascript:alert('온라인구매몰 이용시 별도 회원가입 및 로그인이 필요합니다.'); location.href='/login.asp'"  class="btn btn_b">장바구니</a>
							<a href="javascript:alert('온라인구매몰 이용시 별도 회원가입 및 로그인이 필요합니다.'); location.href='/login.asp'" class="btn btn_b btn_agree">바로주문</a>
						<% End If %>
					</div><!-- btn_wrap e -->
				<% Else %>
					<div class="btn_wrap">
						<% If PRs("GD_GRP_MID") = "011"  Then %>
							<a href="#dm_popup" class="btn btn_b btn_agree open">
								가입상담신청
							</a>
						<% Else %>
							<a href="javascript:inCart();"  class="btn btn_b">장바구니</a>
							<a href="javascript:inOrder();" class="btn btn_b btn_agree">바로주문</a>
						<% End If %>
					</div><!-- btn_wrap e -->
				<% End If %>
				</form>
			</div><!-- product_cont e -->
		</div><!-- product_wrap e -->
		<div class="detail_info_wrap">
			<div class="tab_wrap">
				<div class="tab">
					<a href="javascript:void(0);" rel="tab01" class="active">제품상세정보</a>
					<a href="javascript:void(0);" rel="tab02">배송/교환/반품/환불</a>
					<a href="javascript:void(0);" rel="tab03">상품 Q&A</a>
					<a href="javascript:void(0);" rel="tab04">신용카드할부안내</a>
				</div>
			</div>
			
			<div id="tab01" class="tab_cont detail_cont">
				
				
				<% If seq="396881" Or seq="396890" Or seq="394343" Or seq="396897" Or seq= "396898" Or seq="396899"Then %>
					<img src="/front/img/event_banner_20191115_2.jpg" alt=""/><br><br>
				<% End If %>

				<% If PRs("gd_grp_mid")="007" And PRs("gd_grp_detl")="0001" And PRs("gd_grp_detl2")="0001" Then %>
					<div class="top_wrap">
						<div class="bn_wrap list_wrap">
							

							<div class="visual_bn visual_bn01"><img src="/front/img/led_banner.jpg" alt=""/></div>
						</div><!-- bn_wrap e -->
					</div><!-- top_wrap e -->
				<% End If %>
				<% If seq <> "390714" Then %>
					<% If PRs("GD_GRP_MID") = "007" And PRs("GD_GRP_DETL") = "0005"  Then %>
						<img src="/front/img/default_top_img.jpg" alt=""/><br><br>
					<% End If %>
				
				<% End If %>

				<% If seq = "390900" Or seq ="390902" Then %>
					<map id="imgmap20197210516" name="imgmap20197210516">
						<area shape="rect" alt="" title="" coords="69,7838,380,7898" href="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH2")%>" target="_blank" />
					</map>
				<% End If %>
					
				<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH2")%>" alt="" id="imglistdetl" usemap="#imgmap20197210516"/>
				
				<% If gd_grp_mid="002" And gd_grp_detl="0001" Then %>
					<% If seq <> "" Then %>
						<img src="/front/img/b2bc_detail_bottom_img.jpg" alt=""/>
					<% End If %>
				<% End If %>



				<% If PRs("gd_grp_mid")="007" And PRs("gd_grp_detl")="0001" And PRs("gd_grp_detl2")="0001" Then %>
					<div class="top_wrap">
						<div class="bn_wrap list_wrap">
							<div class="visual_bn visual_bn01"><img src="/front/img/led_footer.jpg" alt=""/></div>
						</div><!-- bn_wrap e -->
					</div><!-- top_wrap e -->
				<% End If %>
			</div>
			<div id="tab02" class="tab_cont delivery_cont">
				<div class="dbt">
					<div class="dbti"><a name="go_info">배송안내 &gt;</a></div>
          <div>
            <ul class="row">
              <li class="dbt_title col">배송지역</li>
              <li class="dbt_cont col">전국</li>
            </ul>
          </div>
          <div>
            <ul class="row">
              <li class="dbt_title col">배송비</li>
              <li class="dbt_cont col">무료</li>                     
            </ul>
          </div>                      
          <div>
            <ul class="row">
              <li class="dbt_title col">배송기간</li>
              <li class="dbt_cont col">
				삼성전자 상품은 로지텍 물류에서 직배송되며
				주문결제 후 평균 2~3주 소요됩니다.
				기타상품은 영업일 기준 4~5일 소요됩니다.
				가입상품의 경우 가입 신청 후 영업일 기준 2일 이내
				전화를 드립니다.
			  </li>                      
            </ul>
          </div>                 
        </div>

				<div class="dbt">
          <div class="dbti">교환/반품 안내 &gt;</div>
          <div>
            <ul class="row">
              <li class="dbt_title col">반품기준</li>
              <li class="dbt_cont col">‘전자상거래 등에서의 소비자보호에 관한 법률’에 의거, 상품 인도 후 7일 이내에 다음의 사유에 의한 교환, 반품 및 환불을 보장하고 있습니다. 단, 고객의 단순한 변심으로 교환,반품 및 환불을 요구할 때 수반되는 배송비는 고객님께서 부담하셔야 합니다. 상품을 개봉했거나 설치한 후에는 상품의 재판매가 불가능하므로 고객님의 변심에 의한 교환,반품이 불가능함을 양지해 주시기 바랍니다 
              </li>
            </ul>
          </div>
          <div>
            <ul class="row">
              <li class="dbt_title col">교환 및 반품이 가능한 경우</li>
              <li class="dbt_cont col">배송된 상품이 주문내용과 상이하거나 본 사이트에서 제공한 정보와 상이할 경우 배송된 상품 자체의 이상 및 결함이 있을 경우. 배송된 상품이 파손, 손상되었거나 오염되었을 경우
              </li>                     
            </ul>
          </div>                      
          <div>
            <ul class="row">
              <li class="dbt_title col">교환 및 반품이 불가능한 경우</li>
              <li class="dbt_cont col">고객님의 책임 있는 사유로 상품 등이 멸실 또는 훼손된 경우. 상품의 고유 포장이 훼손되어 상품가치가 상실된 경우 ( 제품손상 , 스크래치 등) 고객님의 사용, 또는 일부 소비에 의하여 상품의 가치가 감소한 경우. 재판매가 곤란할 정도로 상품 등의 가치가 감소한 경우.</li>                      
           </ul>
          </div>              
          <div>
            <ul class="row">
              <li class="dbt_title col">교환/반품 배송비 배송기간</li>
              <li class="dbt_cont col">고객님의 단순변심으로 인한 교환/반품인 경우, 왕복 배송비 및 상품 회수/배송에 필요한 비용을 고객님께서 부담하셔야 합니다.
              상품의 색상 및 모델 변경과 반품 교환 시에는 고객님이 왕복 배송비를 부담하셔야 하며, 배송비는 상품에 따라 다를 수 있습니다.
              상품의 불량/하자, 표시 광고 및 내용과 다르게 이행되어 교환/반품을 하시는 경우 교환/반품 비용은 무료입니다. 
              </li>                      
            </ul>
          </div>                   
          <div>
            <ul class="row">
              <li class="dbt_title col">보상/분쟁 처리 기준</li>
              <li class="dbt_cont col">고객님께서 가지신 정당한 의견이나 불만을 반영하고, 피해보상을 처리해드리기 위해 고객센터를 운영하고 있으며, 
              불만사항 및 의견은 우선적으로 처리합니다. 다만, 신속히 처리가 곤란한 경우에는 고객님께 그 사유와 처리일정을 즉시 통보해 드립니다.
              </li>
            </ul>
          </div>             
        </div>
				<div class="dbt">
          <div class="dbti">환불안내 &gt;</div>
          <div>
            <ul class="row">
              <li class="dbt_title col">환불기준</li>
              <li class="dbt_cont col">소비자 보호 규정에 의거하여 주문의 취소일 또는 재화 등을 반환 받은 날로부터 영업일 기준 7일 이내에 결제 금액을 환불해 드립니다. 신용카드로 결제하신 경우 신용카드 승인을 취소하여 결제 대금이 청구되지 않게 합니다. 단, 신용카드 결제일자에 맞추어 대금이 청구될 경우, 익월 신용카드 대금 청구 시 카드사에서 환급 처리 됩니다. 무통장 입금의 경우에는 주문의 취소 혹은 제품 회수 후, 입금계좌가 확인되면 7일 이내에 환불해 드립니다. (토요일, 일요일 및 공휴일 제외) 
             </li>
            </ul>
          </div>
        </div>			
			</div>
			<%
	
				If NowPage = "" Then
					NowPage = "1"
				End If 

				ViewCnt = Request("ViewCnt")			
						
				If ViewCnt = "" Then 
					ViewCnt = "5"
				End If 
				
				
				strqry4 = "	sp_mall_qna_list "
				strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
				strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
				strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
				strqry4 = strqry4 & "	,@GD_SEQ='"&SEQ&"' "
			'Response.write strqry4

				DBOpen()
					Set rs_qna = Dbcon.Execute(strqry4)
				DBClose()

				strqry4 = "		sp_mall_qna_cnt "
				strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
				strqry4 = strqry4 & "	,@GD_SEQ='"&SEQ&"' "


				'Response.write strqry4
				DBOpen()
					Set rs_qna_cnt = Dbcon.Execute(strqry4)
				DBClose()

				CountRs = rs_qna_cnt(0)
				
				TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1

			%>
			<div id="tab03" class="tab_cont qna_cont">
				<!-- 문의내역 s -->
				<div class="qna_view" id="qna_list_div">
					<button type="button" class="btn_write" onclick="javascript:qna_write();">문의하기</button>
					<% If CountRs = "0" Then %>
					<div class="question_none">
						문의 글이 없습니다.
					</div>
					<% Else %>
						<% 
							number = 0
							Do Until rs_qna.EOF 
						%>
						<!-- 질문이 없을때 s -->
						
						<!-- 질문이 없을때 e -->
						<% If rs_qna("open_yn") ="Y" Then %>
						<!-- 비밀글 s -->
							<% If rs_qna("work_id") = GetsLOGINID() Then %>
								<div class="question_cont">
									<div class="info_box">
										<span class="state"><%=rs_qna("qna_type")%></span><br class="sbr"/>
										<span class="name"><%=rs_qna("work_id")%></span>
										<span class="date"><%=rs_qna("reg_date")%></span>
									</div>
									<div class="title_box"><%=rs_qna("con_text")%></div>
								</div>
								<% If rs_qna("con_re_text") <> "답변없음" Then %>
									<div class="anwser_cont" id="qna_ok_list_<%=number%>">
										<div class="info_box">
											<span class="state">답변</span><br class="sbr"/>
											<span class="name">관리자</span>
											<span class="date"><%=rs_qna("reg_re_date")%></span>
										</div>
										<div class="title_box">
										<% If rs_qna("con_re_text") = "" Then %>
											미답변
										<% else %>
											<%=rs_qna("con_re_text")%>
										<% End If %>
										</div>
									</div>
								<% End IF%>
							<% Else %>
								<div class="question_cont">
									<div class="info_box">
										<span class="state">배송</span><br class="sbr"/>
										<span class="name">등록자</span>
										<span class="date">2019-06-24 16:26:02</span>
										<% If rs_qna("work_id") = GetsLOGINID() Then %>
											<span class="btn_change" onclick="javascript:qna_delete('<%=rs_qna("seq")%>');">삭제</span>
										<% End if%>
									</div>
									<div class="title_box lock">비밀글입니다.</div><!-- 비밀글일 경우 .lock 붙음 -->
								</div>
								<% If rs_qna("con_re_text") <> "답변없음" Then %>
									<div class="anwser_cont" id="qna_ok_list_<%=number%>">
										<div class="info_box">
											<span class="state">답변</span><br class="sbr"/>
											<span class="name">관리자</span>
											<span class="date"><%=rs_qna("reg_re_date")%></span>
										</div>
										<div class="title_box">
										<% If rs_qna("con_re_text") = "" Then %>
											미답변
										<% else %>
											<%=rs_qna("con_re_text")%>
										<% End If %>
										</div>
									</div>
								<% End IF%>
							<% End If %>
						<!-- 비밀글 e -->
						<% Else %>
						<!-- 공개글 s -->
						<div class="question_cont">
							<div class="info_box">
								<span class="state"><%=rs_qna("qna_type")%></span><br class="sbr"/>
								<span class="name"><%=rs_qna("work_id")%></span>
								<span class="date"><%=rs_qna("reg_date")%></span>
								<% If rs_qna("work_id") = GetsLOGINID() Then %>
									<span class="btn_change" onclick="javascript:qna_delete('<%=rs_qna("seq")%>');">삭제</span>
								<% End if%>
							</div>
							<div class="title_box"><%=rs_qna("con_text")%></div>
						</div>
						<% If rs_qna("con_re_text") <> "답변없음" Then %>
							<div class="anwser_cont" id="qna_ok_list_<%=number%>">
								<div class="info_box">
									<span class="state">답변</span><br class="sbr"/>
									<span class="name">관리자</span>
									<span class="date"><%=rs_qna("reg_re_date")%></span>
								</div>
								<div class="title_box">
								<% If rs_qna("con_re_text") = "" Then %>
									미답변
								<% else %>
									<%=rs_qna("con_re_text")%>
								<% End If %>
								</div>
							</div>
						<% End IF%>
						<!-- 공개글 e -->
						<% End If %>
						<%
							rs_qna.MoveNext
							number = number = 0 + 1 
							Loop
							rs_qna.Close
							Set rs_qna = Nothing
							
						%>
						<%=PT_qna_Ajax_PageLink(NowPage,ViewCnt,"paging")%>
					<% End If %>
				</div>
				<!-- 문의내역 e -->
			
				<!-- 문의하기 s -->
				<div class="qna_write" style="display:none;" id="qna_write_div">
					<button type="button" class="btn_write" onclick="javascript:qna_write_ok('<%=seq%>');">등록하기</button>
					<h2>문의내용등록</h2>
					<div class="cont">
						<p>문의유형을 선택해주세요.</p>
						<div class="chioce_cont">
							<input type="radio" id="ch01" class="ch_box" name="chioce" checked="checked"/>
							<label for="ch01">상품</label>
							<input type="radio" id="ch02" class="ch_box" name="chioce"/>
							<label for="ch02">배송</label>
							<input type="radio" id="ch03" class="ch_box" name="chioce"/>
							<label for="ch03">반품/취소</label>
							<input type="radio" id="ch04" class="ch_box" name="chioce"/>
							<label for="ch04">기타</label>
						</div>
						<div class="chioce_cont_ch_div">
							<input type="checkbox" class="chioce_cont_ch" id="open_yn" name="open_yn">
							<div class="chioce_cont_text">
								비밀글
							</div>
							<div class="chioce_cont_text2">
								등록하신 문의내용은 등록자만 확인 가능합니다.
							</div>

						</div>
						<div class="q_write_box">
							<textarea name="" placeholder="상품에 대해 궁금하신 내용을 작성해주세요."  id="con_text"></textarea>
						</div>
					</div>
				</div>
				             
				<!-- 문의하기 e -->
			</div>

			<div id="tab04" class="tab_cont card_info">
				<p>※ 전자결제(PG) 서비스(KSPAY)를 이용하는 고객들에 대한 카드 무이자 할부개월 에 대한  안내입니다.</p>
				<ol>
					<li>1. 대상카드  : 기존 신한카드/BC/삼성/현대/KB국민/NH농협/하나카드(법인, 체크, 선불, 기프트카드 제외)</li>
					<li>2. 대상기간  : 2019.11.01 ~ 2019.11.30</li>
					<li>3. 할부조건  : 별첨 자료 참고 요망.</li>
				</ol>
				<br/>
				<p>[별첨] 할부 조건</p>
				<table cellpadding="0" cellspacing="0">
					<colgroup>
						<col style="width:20%;"/>
						<col style="width:auto;"/>
						<col style="width:auto;"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="">대상 카드사</th>
							<th scope="">무이자할부</th>
							<th scope="">부분무이자 - 고객부담</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="">NH농협카드</th>
							<td rowspan="5">2~6개월</td>
							<td>
								7-12개월 부분무이자는	ARS 사전 등록시에만 적용됩니다.<br/>
								(NH농협카드 : 1644-2009)<br/>
								7~10개월 : 1,2회차 부담<br/>
								11~12개월 : 1,2,3회차 부담
							</td>
						</tr>
						<tr>
							<th scope="">BC카드</th>
							<td>
								7-12개월 부분무이자는	ARS 사전 등록시에만 적용됩니다.<br/>
								(BC카드 : 1899-5772)<br/>
								7~10개월 : 1,2회차 부담<br/>
								11~12개월 : 1,2,3회차 부담
							</td>
						</tr>
						<tr>
							<th scope="">삼성카드</th>
							<td>
								10개월 : 1,2,3회차 부담<br/>
								12개월 : 1,2,3,4회차 부담<br/>
								18개월 : 1,2,3,4,5,6회차 부담<br/>
								24개월 : 1,2,3,4,5,6,7,8회차 부담							
							</td>
						</tr>
						<tr>
							<th scope="">현대카드</th>
							<td>없음</td>
						</tr>
						<tr>
							<th scope="">하나카드</th>
							<td>없음</td>
						</tr>
						<tr>
							<th scope="">신한카드</th>
							<td>2~7개월</td>
							<td>10개월:1,2회차 부담</td>
						</tr>
						
						<tr>
							<th scope="">KB국민카드</th>
							<td>2~5개월</td>
							<td>10개월 : 1,2회차 부담</td>
						</tr>
						<tr>
							<th scope="">롯데카드</th>
							<td>없음</td>
							<td>없음</td>
						</tr>
					</tbody>
				</table>
				<br/>
				<p>* 5만원 이상 할부 결제 시 적용 됨.</p>
				<p>* 온라인 PG업종에 해당되는 무이자로 일부 업종은 제외 됨.</p>
				<p>* 제외 업종 : 홈쇼핑, 유류, 제세공과금, 등록금, 우편요금, 상품권, 도시가스요금 등.</p>
				<p>* 제약 업종은 일부 카드사만 적용 가능 : BC카드(2~6개월), 국민카드(2~3개월), 현대카드(2~3개월)</p>
				<p>* BC카드 : 해당월 사전 등록한 회원만 부분무이자(7~12개월) 적용 가능.</p>
				<p>* NH농협카드 : 해당월 사전 등록한 회원만 부분무이자(7~12개월) 적용 가능.</p>
				<p>* 하나카드 : 환금성 가맹점 및 학원업종은 제외 (온라인 PG업종만 해당)</p>
				<p>* 현대카드 : 하이브리드 카드제외 (PG쇼핑몰/환금성/상품권 업종만해당)</p>
				<p>* 삼성카드 : 소셜/오픈마켓 등 포함 (세금, 병원업종 제외)</p>
			</div>
			<% If seq="396909" Then %>
				<div class="ex_info" style="text-align:left;">
					<table border="1" cellpadding="0" cellspacing="0" style="text-align:left;">
						<colgroup>
							<col style="width:auto;"/>
						</colgroup>
						<thead>
						</thead>
						<tbody>
							<tr>
								<th style="text-align:left; margin-left:20px;">■ 제조사 : 더블에이치</th>	
							</tr>
							<tr>
								<th style="text-align:left; margin-left:20px;">■ A/S센터 : 더블에이치 070-7603-8122</th>
								
							</tr>
							<tr>
								<th style="text-align:left; margin-left:20px;">
									경기도 안양시 동안구 시민대로 365번길 39, B동 221호
								</th>
							</tr>
							<tr>
								<th style="text-align:left; margin-left:20px;">
									■ 본 제품의 제조 및 A/S는 삼성전자가 아니라 더블에이치에 책임이 있음을 알려드립니다.
								</th>
							</tr>
						</tbody>
					</table>
				</div>
			<% End If %>
		</div><!-- product_info_wrap e -->
	</div><!-- detail_page e -->

</div><!-- container sub e -->
</form>
<!-- #include virtual = "include/footer.asp" -->
<script language='javascript'>
	
	
</script>