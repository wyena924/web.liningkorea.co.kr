<!-- #include virtual = "/include/head.asp" -->
<!-- #include virtual = "/include/header.asp" -->
<%
	gubun	= Request("gubun")
	
	NowPage = Request("NowPage")
	
	If NowPage = "" Then
		NowPage = "1"
	End If 

	ViewCnt = Request("ViewCnt")			
			
	If ViewCnt = "" Then 
		ViewCnt = "9"
	End If 

	If Order_type = ""  Then 
		Order_type = "date_desc" 
	End If 

	strqry4 = "	sp_pmaa_product_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Order_type='"&Order_type&"' "
	'Response.write strqry4
	DBOpen()
		Set rs_product = Dbcon.Execute(strqry4)
	DBClose()


	strqry4 = "		sp_pmaa_product_cnt "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	'Response.write strqry4
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1
%>
<script language="javascript">
	
	product_list("<%=NowPage%>","<%=ViewCnt%>")

	function product_list(NowPage,ViewCnt,Order_type)
	{
		//alert(NowPage);
		//alert(ViewCnt);
		var strAjaxUrl="/pmaa/ajax/samsung_ajax_list.asp"
		var retDATA="";
		//alert(strAjaxUrl);
		
		 $.ajax({
			 type: 'POST',
			 url: strAjaxUrl,
			 dataType: 'html',
			 data: {
				NowPage:NowPage,
				ViewCnt:ViewCnt,
				Order_type:Order_type
			},						  
			success: function(retDATA) {
				if(retDATA)
					{
						if(retDATA!="")
						{
							document.getElementById("product_ajax_list").innerHTML= retDATA;
						}
					}
			 }
		 }); //close $.ajax(
	}

</script>
<div class="container sub">
	<div class="location_wrap">
		<span><a href="#">HOME</a></span><i></i>
		<span>임직원 행사매장</span>
		<% If gubun = "1" Then %>
			<i></i><span>서울 매장</span>
		<% End If %>
		<% If gubun = "2" Then %>
			<i></i><span>경기 북부 매장</span>
		<% End If %>
		<% If gubun = "3" Then %>
			<i></i><span>경기 남부 매장</span>
		<% End If %>
		<% If gubun = "4" Then %>
			<i></i><span>경기서부/인천 매장</span>
		<% End If %>
		<% If gubun = "5" Then %>
			<i></i><span>강원도 매장</span>
		<% End If %>
		<% If gubun = "6" Then %>
			<i></i><span>충청북도 매장</span>
		<% End If %>
		<% If gubun = "7" Then %>
			<i></i><span>충청남도 매장</span>
		<% End If %>
		<% If gubun = "8" Then %>
			<i></i><span>전라북도 매장</span>
		<% End If %>
		<% If gubun = "9" Then %>
			<i></i><span>전라남도 매장</span>
		<% End If %>

		<% If gubun = "10" Then %>
			<i></i><span>경상북도 매장</span>
		<% End If %>
		<% If gubun = "11" Then %>
			<i></i><span>경상남도 매장</span>
		<% End If %>
		<% If gubun = "12" Then %>
			<i></i><span>제주 매장</span>
		<% End If %>
	</div><!-- location_wrap e -->
	<div class="list_page">
		<div class="top_wrap">
			<div class="bn_wrap list_wrap">
				<div class="visual_bn visual_bn01">
					<% If gubun = "1" Then %>
						<img src="/front/img/banner/mj_seoul2.jpg" alt=""/>
					<% End If %>
					<% If gubun = "2" Then %>
						<img src="/front/img/banner/mj_bookbu.jpg" alt=""/>
					<% End If %>
					<% If gubun = "3" Then %>
						<img src="/front/img/banner/mj_nambu.jpg" alt=""/>
					<% End If %>
					<% If gubun = "4" Then %>
						<img src="/front/img/banner/mj_inchon.jpg" alt=""/>
					<% End If %>
					<% If gubun = "5" Then %>
						<img src="/front/img/banner/mj_gangwon.jpg" alt=""/>
					<% End If %>
					<% If gubun = "6" Then %>
						<img src="/front/img/banner/mj_chungbook.jpg" alt=""/>
					<% End If %>
					<% If gubun = "7" Then %>
						<img src="/front/img/banner/mj_chungnam.jpg" alt=""/>
					<% End If %>
					<% If gubun = "8" Then %>
						<img src="/front/img/banner/mj_jenrabookdo.jpg" alt=""/>
					<% End If %>
					<% If gubun = "9" Then %>
						<img src="/front/img/banner/mj_jenranamdo.jpg" alt=""/>
					<% End If %>
					<% If gubun = "10" Then %>
						<img src="/front/img/banner/mj_kungsangbookdo.jpg" alt=""/>
					<% End If %>
					<% If gubun = "11" Then %>
						<img src="/front/img/banner/mj_kungsangsamdo.jpg" alt=""/>
					<% End If %>
					<% If gubun = "12" Then %>
						<img src="/front/img/banner/mj_jeju.jpg" alt=""/>
					<% End If %>

				</div>
			</div><!-- bn_wrap e -->
		</div><!-- top_wrap e -->
		<!-- 상품 리스트는 /pmaa/ajax/samsung_ajax_list.asp 페이지에 있음-->
	</div><!-- list_page e -->

<!-- #include virtual = "/include/footer.asp" -->
