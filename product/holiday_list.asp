<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%	
	NowPage = Request("NowPage")


	If NowPage = "" Then
		NowPage = "1"
	End If 

	ViewCnt = Request("ViewCnt")			
			
	If ViewCnt = "" Then 
		ViewCnt = "9"
	End If 	
	
	Order_type = Request("Order_type")

	'Response.write Order_type
	'정렬순서가 존재하지 않으면 최신 등록순으로 
	If Order_type = ""  Then 
		Order_type = "price_asc" 
	End If 

	strqry4 = "	sp_pmaa_product_holiday_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Order_type='"&Order_type&"' "

	DBOpen()
		Set rs_product = Dbcon.Execute(strqry4)
	DBClose()

	strqry4 = "		sp_pmaa_product_holiday_cnt  "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	'Response.write strqry4
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1	

%>
<script language="javascript">
	
	function product_list(Order_type)
	{
		location.href="holiday_list.asp?
	}

</script>
<div class="container sub">
	<div class="location_wrap">
		<span><a href="#">HOME</a></span><i></i>
		<span>설맞이 선물대전</span>
	</div><!-- location_wrap e -->
	<div class="list_page">
		<div class="top_wrap">
			<div class="bn_wrap list_wrap">
				<div class="visual_bn visual_bn01"><img src="/front/img/banner/ppma_sul_banner.jpg" alt=""/></div>
			</div><!-- bn_wrap e -->
		</div><!-- top_wrap e -->
		<!-- 상품 리스트는 /ajax/samsung_ajax_list.asp 페이지에 있음-->
		<div class="bottom_wrap" id="product_ajax_list">
			<div class="list_wrap">
				<div class="price_tab">
					<a href="holiday_list.asp?order_type=price_asc" <% If Order_type = "price_asc" Then %>class="active"<% End If %>>낮은가격순</a>
					<a href="holiday_list.asp?order_type=price_desc" <% If Order_type = "price_desc" Then %>class="active"<% End If %>>높은가격순</a>
					<a href="holiday_list.asp?order_type=date_desc" <% If Order_type = "date_desc" Then %>class="active"<% End If %>>최근등록순</a>
				</div>
				<div class="list_box">
					<ul>
				
					<% If rs_product.bof Or rs_product.eof Then %>
					<% Else %>
					<% 
						Do Until rs_product.EOF 
						
						PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
					%>
					<li>
						<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
							<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
							<div class="product_info ss_order_mall">
								<dl>
									<dt class="name"><%=rs_product("GD_NM")%></dt>
									<dd class="price">
										<span><b><%= FormatNumber(rs_product("GD_PRICE3"),0)%></b>원</span>
										<span><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span>
										<span class="percent"><b><%=PERCENT%></b>%</span>
									</dd>
								</dl>
							</div>
							<span class="mark"><b><%=PERCENT%></b>%<br/>SALE</span>
						</a>
					</li>
					<%
						rs_product.MoveNext
						x = x + 1
						Loop
						rs_product.Close
						Set rs_product = Nothing
						End If 
					%>
					</ul>
				</div><!-- list_box e -->
				<div style="height:100px;">
					<%=PT_Ajax_PageLink(NowPage,ViewCnt,Order_type,"holiday_list.asp")%>       
				</div>
				
			</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->

<!-- #include virtual = "include/footer.asp" -->
