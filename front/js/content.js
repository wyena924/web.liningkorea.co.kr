$(document).ready(function(){
	//팝업창
	$('.open').click(function(){
		var $href = $(this).attr('href');
		layer_popup($href);
	});

	//체크박스
	var status = "";
	$(".cart_page input[id='AllCheck']").change(function(){
	  if ( $(this).is(":checked")) { // if(.is())에만 들어감 - 있는지없는지 체크해줌
		  status = true;
	  }else {
		  status = false;
	  }
	  $(".cart_page input[id^='chkidx_']").prop("checked", status);
	});

	var ckNum = $("input[id^='chkidx_']").length; //5
	$(".cart_page input[id^='chkidx_']").change(function(){
	  var ckinput = $("input[id^='chkidx_']:checked").length; // 체크할때마다 변하니까 .change 안에 위치
	  if (ckNum == ckinput) {
		status = true;
	  }else {
		status = false;
	  }
	  $(".cart_page input[id='AllCheck']").prop("checked", status);
	});
	$('.grid_list button').each('click', function(){
		if($(this).hasClass('grid1')){
			$(this).removeClass('on');
			$('.grid_list .grid1').addClass('on');
			$('.list_box ul').removeClass('grid_two grid_three').addClass('grid_one');
		}else if($(this).hasClass('grid2')){
			$(this).removeClass('on');
			$('.grid_list .grid2').addClass('on');
			$('.list_box ul').removeClass('grid_one grid_three').addClass('grid_two');
		}else if($(this).hasClass('grid3')){
			$(this).removeClass('on');
			$('.grid_list .grid3').addClass('on');
			$('.list_box ul').removeClass('grid_one grid_two').addClass('grid_three');
		}
	});

});

//팝업창
	function layer_popup(pop){
		var $pop = $(pop); //레이어의 id를 $pop 변수에 저장
		$pop.fadeIn();
		$('html,body').addClass('fix');
		$pop.find('.btn_close, .pop_close').click(function(){
			$pop.fadeOut(); // 닫기 버튼을 클릭하면 레이어가 닫힌다.
			$('html,body').removeClass('fix');
			return false;
		});
		$pop.find('.reload').click(function(){
			location.reload();
		});
	}

//버거
	$('.btn_burger').on('click', function(){
		$('.hd_nav').toggleClass('active');
		$(this).toggleClass('close');
	});

//헤더 서치
	var searchWrap = $('.search_cont');
	var btnSearch = $('.btn_search');
	var inputBox = $('#search_box');
	var isOpen = false;

	function searching() {
		var searchBox = inputBox.val();
		if(searchBox !=''){ //검색어 입력시
		//alert('검색어: '+ searchBox);
		f= document.search_form
		f.submit();
		searchWrap.removeClass('active');
			inputBox.val('');
	  } else { //검색어 미입력시
		alert('검색어를 입력해주세요');
	  }
	  isOpen = false;
	}

	// 검색 버튼
	// 11.07 update :: wyn
	btnSearch.on('click', function(){
	  if(isOpen != false){
		searching();
	  } else {
		isOpen = true;
		searchWrap.addClass('active');
			setTimeout(function(){
			inputBox.focus();
		},1000);
	  }
	})

	// 11.07 update :: wyn
	$(document).mouseleave(function(){
		if(isOpen != true){
			searchWrap.removeClass('active');
		}
	});

//마이페이지-미니
	$('.mypage_box .mypage').click(function(){
		$('.mypage_mini').toggleClass('active');
	});

//bxslider
	$('#bxslider').bxSlider({
		auto: true,
		pager: false,
		controls: true,
		adaptiveHeight: true,
		responsive:true,
		touchEnabled: false, //터치 스와이프 기능 끔(크롬에서 링크 안먹힘)
	});

	//$("#bxslider").load(function(){
	//	var height = $(this).height(); 
	//	$(".bx-viewport").css("height", height);
	//});
	//

//탭메뉴
	$(".tab_cont").hide();
	$(".tab_cont:first").show();

	$(".tab_wrap a").click(function () {
		$(".tab_wrap a").removeClass("active");
		$(this).addClass("active");
		$(".tab_cont").hide()
		var activeTab = $(this).attr("rel");
		$("#" + activeTab).fadeIn()
	});


//셀렉트박스
	$('.select_box select').change(function() {
		var select_name = $(this).children("option:selected").html();
		$(this).siblings("label").html(select_name);
		$(this).toggleClass("active");
	});

//q&a
//	$('.question_cont').click(function(){
//		$(this).toggleClass('active');
//		$(this).next('.anwser_cont').toggleClass('active');
//	});




//resize
$(window).resize(function (){
//gnb
	var winWid = $(window).width();

	if( winWid > 1200 ){
		$('.gnb_wrap .menu > li,.lnb_wrap').hover(
		  function(){
			$('.lnb_wrap').addClass('active');
		  },function(){
			$('.lnb_wrap').removeClass('active');
		  }
		);
		/*$('.gnb_wrap .menu > li:last-child').mouseenter(function(){
			$('.lnb_wrap').removeClass('active');
			$('.gnb_wrap .menu li:last-child > .list').addClass('active');
		});
		$('.gnb_wrap .menu > li:last-child').mouseleave(function(){
			$('.gnb_wrap .menu > li:last-child > .list').removeClass('active');
		});*/

		$('.lnb_wrap .menu > li').mouseenter(function(){
			var idx = $(this).index();
			$('.gnb_wrap .menu > li').eq(idx).addClass('active');
			$(this).find('.lnb_tit').addClass('active');
		});
		$('.lnb_wrap .menu > li').mouseleave(function(){
			var idx = $(this).index();
			$('.gnb_wrap .menu > li').eq(idx).removeClass('active');
			$(this).find('.lnb_tit').removeClass('active');
		});
		$('.gnb_wrap .menu > li:last-child li').click(function(){ //전부 다 토글메뉴면 $('.lnb_wrap .list li') 로 바꿔주기
			$(this).each(function(){
				if($('.gnb_wrap .menu > li:last-child li').height('21px')){
					$(this).css('height','auto');
				}else{
					$(this).css({'height':'21px'});
				}
			});
		});
		return false;
	}else{
		var gnbHei = $('.hd_nav').height();
		$('.gnb_wrap li').removeClass('active');
//		$('.gnb_wrap li').eq(0).addClass('active');
		$('.lnb_wrap .list').eq(0).addClass('active');

		$('.gnb_wrap li').click(function(){
			var gnbIdx = $(this).index();

			$('.lnb_wrap .list').removeClass('active');
			$('.lnb_wrap .list').eq(gnbIdx).addClass('active');
//			$('.lnb_wrap .btn_back').css('z-index','99999');
			setTimeout(function(){
				$('.gnb_wrap').addClass('active');
				$('.lnb_wrap').addClass('active');
			},500);
		});
		$('.btn_back').click(function(){
			$('.lnb_wrap .list').removeClass('active');
			$('.lnb_wrap').removeClass('active');
			$('.gnb_wrap').removeClass('active');
		});
		$('.lnb_wrap .menu li:last-child li').click(function(){
			$(this).each(function(){
				$(this).height('21px');
				if($('.lnb_wrap .list li').height('21px')){
					$(this).css('height','auto');
				}else{
					$(this).css('height','21px');
				}
			});
		});
	}

	// 탭배너
	var $tab_list = $('.tab_list');
	$tab_list.removeClass('jx').find('ul ul').hide();
	$tab_list.find('li li.active').parents('li').addClass('active');
	$tab_list.find('li.active>ul').show();
	$tab_list.each(function(){
		var $this = $(this);
		$this.height($this.find('li.active>ul').height()+40);
	});
	function listTabMenuToggle(event){
		var $this = $(this);
		$this.next('ul').show().parent('li').addClass('active').siblings('li').removeClass('active').find('>ul').hide();
		$this.closest('.tab_list').height($this.next('ul').height()+40);
		if($this.attr('href') === '#'){
			return false;
		}
	}
	$tab_list.find('>ul>li>a').click(listTabMenuToggle).focus(listTabMenuToggle);
}).resize();



	$('img[usemap]').rwdImageMaps();
