var customizingRef = (function(d, w, $) {
    var customizingRef = customizingRef || {};

    var sMYEDITION = '.my-edition',
        sMODEL = sMYEDITION + '-making-selection',
        sSTEP01 = '.step-box_type',
        sSTEP02 = '.step-box_module',
        sSTEP03 = '.step-box_color',
        sSTEP04 = '.step-box_function',
        sVOLUME = sMYEDITION + '-making_volume',
        sTABLE = sSTEP04 + ' .my-edition-model-tbl table'
        sACTIVE = 'active',
        sINACTIVE = 'inactive', // 20190620 추가
        sTABBTN = sMYEDITION + '_tab',
        sTABVIEW = sTABBTN + '-view',
        sREF = sMODEL + ' li[data-type] .making_doors_btns:visible',
        sDATACOLOR = 'data-color',
        sDATAPRICE = 'data-price',
        sBTN = 'button',
        aDOORLENG4 = ['6door', '5door', '4door', '4door_kimch', '4door_kf'],
        sSAVEBTN = '.save_btn',
        sDELETEBTN = 'my-pick_btn_del',
        aSelectedOpt = [],
        aSelectedOpt2 = [],
        $ARPOPUP = $('#layer05'),
        mobileFlag = null,
        prevSize = 0,
        currentSize = 0;

     var sImgPath =  './images/';
 

    //숫자 처리 기능 모음
    var controlNumber = {
        //3자리 단위 콤마
        numberWithCommas: function(str) {
            return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        },
        //숫자만
        onlyNumber: function(str) {
            var res;
            res = str.replace(/[^0-9]/g, "");
            return parseInt(res);
        },
        //가격 합산
        getSumPrice: function(array) {
            var sum = array.reduce(function(previousValue, currentValue, currentIndex, array1) {
                return previousValue + currentValue;
            });

            return sum;
        }
    }

    //리사이즈
    /**
     * @param fnPc 해상도 768px초과 실행할 내용
     * @param fnMo 해상도 768px이하 실행할 내용
     */

    var resizeFunc = function(fnPc, fnMo) {
        var rtime;
        var timeout = false;
        var delta = 200;

        $(window).resize(function() {
            rtime = new Date();
            if (timeout === false) {
                timeout = true;
                setTimeout(resizeend, delta);
            }
            mobileFlag = window.innerWidth <= 768 ? true : false;
            currentSize = mobileFlag == true ? "mobile" : "pc";
            if (prevSize != currentSize) {
                prevSize = currentSize;
                selectionOrderChange(mobileFlag);
            }
            //190620
            if (mobileFlag) {
                selectionFixedFunc.selectionFixedMo();
            } else {
                selectionFixedFunc.selectionFixedPc();
            }
        });

        function resizeend() {
            if (new Date() - rtime < delta) {
                setTimeout(resizeend, delta);
            } else {
                timeout = false;
                var isMobile = window.innerWidth <= 768 ? true : false;

                if (isMobile) {
                    if (fnMo !== undefined) {
                        fnMo();
                    }
                } else {
                    fnPc();
                }
            }
        }
    };

    // 20190619 scroll fixed
    var FixedTop = 'secFixedTop',
        FixedBtm = 'secFixedBtm';

    var selectionFixedFunc = {
        selectionFixedPc: function() {
            $(window).on('scroll resize', function() {
                var isMobile = window.innerWidth <= 768 ? true : false;
                if (!isMobile) {
                    var FixMenuH = $(".promo-nav-outer").outerHeight();
                    var curScroll = $(this).scrollTop(),
                        makeSection = $(sMYEDITION + '-making-section'),
                        makeSectionTop = $(sSTEP01).offset().top,
                        makeSectionHeight = makeSection.outerHeight(),
                        makeSectionBtm = makeSection.offset().top + makeSectionHeight - $(window).height(),
                        makeSectionEnd = $('.my-edition-btn-area').offset().top,
                        imgH = $(sMODEL).height();

                    if (curScroll >= $('.my-edition-making-wrap')[0].offsetTop - 40) {
                        if (curScroll + imgH >= makeSectionEnd + $('.my-edition_btn').outerHeight() + 140) {
                            $(sMODEL).removeClass(FixedTop).addClass(FixedBtm);
                        } else {
                            $(sMODEL).removeClass(FixedBtm).addClass(FixedTop);
                        }
                    } else {
                        $(sMODEL).removeClass(FixedBtm).removeClass(FixedTop);
                    }
                }
            });
        },
        selectionFixedMo: function() {
            $(sMODEL).removeClass(FixedTop).removeClass(FixedBtm);
        }
    }


    //주요 토글 기능 모음
    var toggleView = {
        trBg: {
            selector: {
                sHoverClass: 'hoverBg'
            },
            hover: {
                show: function($this) {
                    $this.addClass(toggleView.trBg.selector.sHoverClass);
                },
                hide: function($this) {
                    $this.removeClass(toggleView.trBg.selector.sHoverClass);
                }
            },
            click: {
                show: function($this) {
                    $this.parents('tr').addClass(sACTIVE).siblings().removeClass(sACTIVE);

                },
                hide: function($this) {
                    $this.parents('tr').removeClass(sACTIVE);
                }
            }
        },
        showModel: {
            show: function() {
                $(sMYEDITION + '-making-default').hide();
                $(sMYEDITION + '-making-ref-wrap').show();
                $(sMYEDITION + '-making-info').show();
            },
            hide: function() {
                $(sMYEDITION + '-making-default').show();
                $(sMYEDITION + '-making-ref-wrap').hide();
                $(sMYEDITION + '-making-info').hide();
            }
        }
    }

    //쿠키에 저장될 내가 만든 BESPOKE 리스트
    var prodList = [],
        cookieName = 'bpproduct',
        savedLength,
        pushProdList = function() {
            prodList = JSON.parse($.cookie(cookieName));
        };

    var init = function(data) {
        //리셋
        var reset = {
            model: function() {
                toggleView.showModel.hide();
            },
            myEditionDoorSelection: function() {
                $(sMODEL + ' li[data-type] .door_btn').find(sBTN).each(function() {
                    $(this).removeAttr(sDATACOLOR).removeAttr(sDATAPRICE).removeAttr('data-img').css('background-image', '').find('.color').text(''); // 20190620
                    $(this).find('strong').show();
                    $(this).removeClass(sACTIVE)
                });
            },
            saveBtn: function() {
                $(sSAVEBTN).removeClass(sACTIVE);
            },
            stepList: {
                step1: function() {
                     $(sSTEP01 + ' ' + '.type-select_btn').removeClass(sACTIVE).removeClass(sINACTIVE); // 20190620
                },
                step2: function() {
                    choiceDep.act();
                },
                step3: function() {
                    $(sTABVIEW).find(sBTN + '.' + sACTIVE).removeClass(sACTIVE); // 20190624 수정 20190620
                    settingPrice.resetPrice();
                },
                act: function() {
                    this.step1();
                    this.step2();
                    this.step3();
                }
            },
            act: function(caseType) {
                reset.myEditionDoorSelection();
                reset.saveBtn();

                switch (caseType) {
                    case 'tab':
                        reset.stepList.step3();
                        break;
                    /* 20190620 */
                    case 'remake':
                        moveNextStep($(sSTEP04).find('.my-edition-step_cont'));     
                    default:
                        reset.model();
                        reset.stepList.act();
                      

                        break;
                }
            }
        }

        //step 전 후 alert
        /**
         * @param step 'prev' - 이전 스텝 체크 / 'next' - 다음 스텝 체크 
         * @param $el 선택되었는지 체크할 선택자
         * @param caseType sBTN/'.type-select_btn'/'a'/'tr' case에 따라 체크될 대상
         * @param yesCallback '예' 버튼 클릭시 실행할 내용
         * @param noCallback '아니오' 버튼 클릭시 실행할 내용
         * @param coCallback '예/아니오'에 관계없이 공통으로 실행할 내용
         */
        var stepAlert = function(step, $el, caseType, yesCallback, noCallback, coCallback) {
            var bool, txt, isChecked;
            switch (caseType) {
                case sBTN:
                    txt = sBTN;
                    break;
                case '.type-select_btn':
                    txt = '.type-select_btn'
                    break;
                case 'a':
                    txt = '.slider-item';

                    break;
                case 'tr':
                    txt = 'tbody tr';
                    break;
            }

            switch (step) {
                case 'prev':
                    if ($el.find(txt + '.' + sACTIVE).length !== 0) {
                        popup.confirm_(1, '지금 선택중인 단계보다 이전 단계를 선택하면<br/>기존에 선택한 내용이 모두 초기화 됩니다.<br/>선택 내용을 변경 하시겠습니까?', yesCallback, noCallback, coCallback)
                    } else {
                        if (coCallback !== undefined) {
                            coCallback();
                        }
                    }
                    break;
                case 'next':
                    if ($el.find(txt + '.' + sACTIVE).length === 0) {
                        popup.confirm_(2, '이전 단계를 선택하지 않았습니다.<br/>이전 단계를 먼저 선택해 주세요.')
                        isChecked = false;
                    }
                    break;
            }

            if (isChecked === false) {
                return true;
            }
        };

        //step 클릭시 다음 step으로 화면 이동
        var moveNextStep = function($el) {
            var idx = $el.parents('.my-edition-making-step-box').index();
            switch (idx) {
                case 0:
                    var step = '.step-box_module';
                    break;
                case 1:
                    var step = '.step-box_color';
                    break;
                case 2:
                    var step = '.step-box_function';
                    break;
               // 20190620
                case 3:
                    var step = '.step-box_type';
                    break;     
            }

           // 20190620 
            var offset = $(step).offset().top + $('.my-edition-making-step-wrap').scrollTop() - $('.promo-nav-inner').outerHeight(),
                isMobile = window.innerWidth <= 768 ? true : false,
                moving = offset;
            if ( idx == 1 && !isMobile ){
                moving = moving - 80;
            } 
            $('html,body').animate({ scrollTop: moving }, 300);
        };

        //쿠키에 변동이 생길 경우 initialize
        var initCookieHTML = function() {
            prodList.length !== 0 ? $('.my-edition-pick-section').show() : $('.my-edition-pick-section').hide()
            $('.cookie_length').text(prodList.length);
            var aProdHtml = [],
                aProdHtmlPop = [],
                panelCode = [],
                prod = settingCookie.loadProd(),
                $modelList = $('.my-edition-pick-section .my-edition-pick-list');

            var makeHTML = function() {
                var doorName;
                var setDoorFrame = function(doorType) {
                    switch (doorType) {
                        case '3door':
                            doorName = ['상칸', '중칸', '하칸'];
                            doorClass = ['door_t', 'door_m', 'door_b'];
                            doorIdx = 3;
                            break;
                        case '2door':
                            doorName = ['상칸', '하칸'];
                            doorClass = ['door_t', 'door_b'];
                            doorIdx = 2;
                            break;
                        case '1door_refrigerator':
                        case '1door_freezer':
                        case '1door_kimch':
                        case '1door_slim':
                            doorName = ['도어'];
                            doorClass = ['door_f'];
                            doorIdx = 1;
                            break;
                        case '4door_kimch':
                            doorName = ['상칸좌', '상칸우', '중칸', '하칸'];
                            doorClass = ['door_t_l', 'door_t_r', 'door_m', 'door_b'];
                            doorIdx = 1;
                            break;
                        default:
                            doorName = ['상칸좌', '상칸우', '하칸좌', '하칸우'];
                            doorClass = ['door_t_l', 'door_t_r', 'door_b_l', 'door_b_r'];
                            doorIdx = 4;
                    }
                };

                //내가 만든 BESPOKE 태그 만들기
                var making = {
                    //프레임
                    frame: function(type, list) {
                        var aButtonDom = [];

                        setDoorFrame(type);

                        $.each(list.panel, function(k, panel) {
                            aButtonDom.push('<div class="my_door ' + doorClass[k] + '" style="background-image:url(' + sImgPath + 'door/' + panelCode[k] + '.png?$ORIGIN_PNG$)"><span class="position">' + doorName[k] + ' </span><span class="color">' + panel.color + '</span></div>')
                        });

                        return aButtonDom;
                    },
                    //선택 사항 보기 팝업 테이블
                    panelDom: function(type, list) {
                        var aPanelDom = [];


                        var makeDoorFrame = function(arr) {
                            $('.pd-pop-content__table .door').each(function(i) {
                                $(this).find('th').text('디자인 패널 ' + arr[i]);
                            })
                           // 20190620 수정
                            $.each(list.panel, function(k, panel) {
                                // aPanelDom.push('<tr class="door"><th scope="row">디자인 패널 ' + arr[k] + '</th><td>' + panel.color + '</td><td>' + '원' + '</td></tr>') 
                                 aPanelDom.push('<tr class="door"><th scope="row">디자인 패널 ' + arr[k] + '</th><td>' + panel.color + '</td><td>' + controlNumber.numberWithCommas(panel.panelprice) + '원' + '</td></tr>') 
                            });
                        };

                        setDoorFrame(type)
                        makeDoorFrame(doorName);

                        return aPanelDom;
                    }
                }

                $.each(prod, function(i, item) {
                    panelCode = [];

                    var nModelPrice = 0,
                        aPanel = [],
                        filteredType = data.filter(function(type) {
                            return type.dataType === item.type;
                        })[0];

                   // 2019-06-21 2차 수정 S
                    var filteredModelPrice = filteredType.product.filter(function(model) {
                            return model.modelName === item.name;
                        })[0].price;
                    nModelPrice = filteredModelPrice * 1;
                    // 2019-06-21 2차 수정 E

                    var getIdx = function(i) {
                        var idx = i + 1;
                        if (idx < 10) {
                            idx = '0' + idx;
                        } else {
                            idx = idx
                        }
                        return 'chk' + idx;
                    };

                    $.each(item.panel, function(j, panel) {
                        if (aDOORLENG4.indexOf(item.type) > -1) {

                            if (j % 2 === 0) {
                                panelCode.push(panel.code + '_left')
                            } else {
                                panelCode.push(panel.code + '_right')
                            }
                        } else {
                            panelCode.push(panel.code)
                        }
                    });

                    //내가 만든 BESPOKE, 모바일 AR내 공간에서 체험하기 공통
                    var commonHTML = '<div class="my-pick_img" data-type="' + item.type + '">' +
                        '<span class="pick_img"><img src="' + sImgPath + 'frame/' + (function() {
                            if (item.type.indexOf('1door') > -1 && item.type !== '1door_slim') {
                                return '1door';
                            } else {
                                return item.type;
                            }
                        })() + '.png?$ORIGIN_PNG$" alt="' + item.type + '"></span>' +
                        '<span class="pick_img_mo"><img src="' + sImgPath + 'frame/mo_' + (function() {
                            if (item.type.indexOf('1door') > -1 && item.type !== '1door_slim') {
                                return '1door';
                            } else {
                                return item.type;
                            }
                        })() + '.png?$ORIGIN_PNG$" alt="' + item.type + '"></span>' +
                        '<div class="pick_doors">' +
                        making.frame(item.type, item).join('') +
                        '</div>' +
                        '</div>' +
                        '<ul class="my-pick_info">' +
                        '<li class="pick_name"><a href="' + item.url + '" title="새창 열림" target="_blank">' + item.name + '</a></li>' +
                        '<li class="pick_price">' + controlNumber.numberWithCommas(item.price) + '원' + '</li>' +
                        '</ul>';

                    //내가 만든 BESPOKE
                    var newHTML = '<li class="my-edition-pick-item" id=model_' + item.id + '>' +
                        commonHTML +
                        '<button type="button" class="pick_btn_detail js-popup">선택 사항 보기</button>' +
                        '<div class="my-pick-btns">' +
                        '<a href="javascript:;" class="my-pick_btn_buy" data-omni-type="microsite" data-omni="sec:bespoke:buy:buynow_' + item.name + '">바로 구매하기</a>' +
                        '<button type="button" class="' + sDELETEBTN + '" data-omni-type="microsite" data-omni="sec:bespoke:buy:delete_' + item.name + '"><span class="delete">삭제하기</span></button>' +
                        '</div>' +
                        '</li>';

                    //모바일 AR내 공간에서 체험하기
                    var popHtml = '<li class="my-edition-pick-item">' +
                        '<label for="' + getIdx(i) + '">' +
                        commonHTML +
                        '<div class="chkbox-wrap">' +
                        '<input type="checkbox" id="' + getIdx(i) + '" name="chkAr" class="ar-check" title="체크">' +
                        '<i class="ar-check-mark"></i>' +
                        '</div>' +
                        '</label>' +
                        '</li>';

                    aProdHtml.push($(newHTML).get(0));
                    aProdHtmlPop.push($(popHtml).get(0));

                    var typeKr = data.filter(function(dataItem) {
                        return dataItem.dataType === item.type;
                    })[0].type;
        
                    /* 20190620 추가 s */
                    choicedOpt = making.panelDom(item.type, item).join('') +'<tr class="total">' + 
                        '<td>총 구매가격</td>' +
                        '<td colspan="2" class="right">' + controlNumber.numberWithCommas(item.price) + '원' + '</td>' //2019-06-21 2차 수정
                        '</tr>'; 

                    
                    choicedOpt2 = '<tr>' + 
                        '<th>' + item.name + '</th>' +
                        '<th>' + typeKr + '</th>' +
                        '<th>' + controlNumber.numberWithCommas(filteredModelPrice) + '원' + '</th>' //2019-06-21 2차 수정
                        '</tr>';
                    

                    aSelectedOpt[i] = choicedOpt;
                    aSelectedOpt2[i] = choicedOpt2;
                    /* // 20190620 추가 e */

                });

                $modelList.html('').append(aProdHtml);

                $ARPOPUP.find('.my-edition-pick-list').html('')
                $.each(aProdHtmlPop, function(i, item) {
                    $ARPOPUP.find('.my-edition-pick-list').append(item);
                });

                $(sSTEP03).find('.cookie_length').text(settingCookie.loadProd().length);
                pushProdList();

            };

            //내가 만든 BESPOKE 제거
            var deleteChoicedLi = function() {
                var $btnDelete = $('.' + sDELETEBTN);
                $btnDelete.off('click').on('click', function() {
                    settingCookie.deleteProd($(this).parents('.my-edition-pick-item'))
                })
            };

            //내가 만든 BESPOKE PC/MO 노출 개수 설정
            var exposureList = function() {
                var $moreBtn = $('.more_btn'),
                    nMaxLength = 6,
                    sItem = '.my-edition-pick-section .my-edition-pick-item';

                var fnPc = function() {
                    $(sItem).show();
                    if ($moreBtn.css('display') === 'none') {
                        $moreBtn.show();
                    }
                };

                var fnMo = function() {
                    $(sItem).hide();
                    if ($(sItem).length > nMaxLength) {
                        $(sItem + ':nth-child(-n+' + nMaxLength + ')').show();
                    } else {
                        $(sItem).show();
                        $moreBtn.hide();
                    }
                };

                $moreBtn.click(function() {
                    var nVisibleLength = $(sItem + ':visible').length + 1,
                        nAddLeng = 2;
                    $(sItem + ':nth-child(n+' + nVisibleLength + '):nth-child(-n+' + (nVisibleLength + nAddLeng) + ')').show();

                    if ($(sItem + ':visible').length === prodList.length) {
                        $moreBtn.hide();
                    }
                });

                fnPc();
                var isMobile = window.innerWidth <= 720 ? true : false;
                if (isMobile) { fnMo() }
                resizeFunc(fnPc, fnMo)
            };


            makeHTML();
            deleteChoicedLi();
            exposureList();
        };

        //내가 만든 BESPOKE 제거 후 prodList에 제거되지 않은 나머지 담고 쿠키에 저장 후 initCookieHTML 호출
        var afterDelete = function($this) {
            var resetId = function($this) {
                var thisId = $this.attr('id');
                $this.remove();
                cleanProdList = prodList.filter(function(item) {
                    return item.id !== controlNumber.onlyNumber(thisId);
                })
                prodList = cleanProdList;
                $.each(prodList, function(i, item) {
                    item.id = (i + 1);
                })
            };
            resetId($this);
            settingCookie.saveCookie(prodList);
            initCookieHTML();
        };

        //쿠키 설정
        var settingCookie = {
            saveCookie: function(arr) {

                $.cookie(cookieName, JSON.stringify(arr), { expires: 7, path: '/' });
                savedLength = prodList.length;
            },
            loadProd: function() {
                if ($.cookie(cookieName) !== undefined) {
                    var choiceList = JSON.parse($.cookie(cookieName));
                    return choiceList;
                }
            },
            makeProd: function(type, model, panelData, totalPrice, url) {
                var newId = prodList.length + 1;
                var listObj = {
                    id: newId,
                    name: model,
                    type: type,
                    panel: panelData,
                    price: totalPrice,
                    url: url
                }

                prodList.push(listObj);
                this.saveCookie(prodList);
                savedLength = prodList;
            },
            deleteProd: function($this, caseType) {
                var cleanProdList;
                if (caseType === 'last') {
                    afterDelete($this);
                } else {
                    popup.confirm_(1, '선택한 제품을 내가 만든 BESPOKE 목록에서<br/>삭제 하시겠습니까?', function() {
                        afterDelete($this);
                    })
                }
            }
        }

        //'product' 쿠키가 없으면 '.my-edition-pick-section' 영역 제거, 있으면 지정 함수 호출
        var checkCookie = function() {
            if ($.cookie(cookieName) !== undefined) {
                pushProdList();
                initCookieHTML();
            } else {
                $('.my-edition-pick-section').hide();
            };
        };

        var modelData = {};
        var aType = [];
        var check1Door = function(type) {
            switch (type) {
                case '1door_refrigerator':
                case '1door_freezer':
                case '1door_kimch':
                    return '1door';
                    break;
                default:
                    return type;
            }
        };

        //Step 1 선택시 변경될 step2 내용
        var choiceModel = {
            init: {
                makeHTML: function() {
                    $.each(data, function(i, item) {
                        aType.push([]);

                        if (item.product.length !== 0) {
                            var typeHTML = '<div class="module_type slider-item">' +
                                '<a href="javascript:;" data-type=' + item.dataType + ' data-dep="' + item.option[0].department + '" data-omni-type="microsite" data-omni="sec:bespoke:buy:step2:select doortype_' + item.dataType + '">' +
                                '<div class="product-item_img"><img src="' + sImgPath + 'frame/thumb_' + check1Door(item.dataType) + '.png?$ORIGIN_PNG$"></div>' +
                                '<div class="product-item_txt">' +
                                '<strong>' + item.type + '</strong>' +
                                '<p>' + item.subject + '</p>' +
                                '</div>' +
                                '</a>' +
                                '</div>';
                        }

                        aType[i].push($(typeHTML).get(0));
                    });
                },
                appendHTML: function() {
                    $.each(aType, function(i, item) {
                        $('.ref-module-list').append(aType[i]);
                    })
                },
                act: function() {
                    this.makeHTML();
                    this.appendHTML();
                }
            },
            //step2 선택시 변경될 내용 모음
            afterChoice: {
                saveModelData: function(self_) {
                    var $this = this;
                    modelData.type = self_.attr('data-type');
                    modelData.dataType = data.filter(function(item) {
                        return item.dataType === modelData.type;
                    })[0].type;
                    modelData.department = self_.attr('data-dep');
                },
                //step2 선택시 변경될 step3 내용
                gridColor: {
                    selector: {
                        $TABBTN: $(sSTEP03).find(sTABBTN),
                        $TABVIEW: $(sSTEP03).find(sTABVIEW),
                        aMaterialBtn: [],
                        aFilteredMaterial: [],
                        aFilteredColor: []
                    },
                    makeHTML: function() {
                        var self_ = this.selector,
                            filterColor = data.filter(function(i, item) { return i.dataType === modelData.type })[0];

                        var hasColorList = filterColor.option[0].list.filter(function(item) {
                            return item.color.length > 0;
                        });

                        var cleanArr = function() {
                            self_.aMaterialBtn = [];
                            self_.aFilteredMaterial = [];
                            self_.aFilteredColor.length = [];
                        };

                        cleanArr();

                        $.each(hasColorList, function(i, item) {
                            self_.aFilteredColor.push([]);
                            var materialHTML =
                                '<li data-id="tab_0' + (i + 1) + '">' +
                                '<div class="my-edition-color-box">' +
                                '<ul class="my-edition-color_list"></ul>' +
                                '</div>' +
                                '</li>';

                            var materialBtn = '<li rel="tab_0' + (i + 1) + '" class="tab_0' + (i + 1) + ' ' + (function() {
                                    if (i === 0) {
                                        return sACTIVE;
                                    } else {
                                        return '';
                                    }
                                })() + '">' +
                                '<a href=""  data-omni-type="microsite" data-omni="sec:bespoke:buy:step3:tab_0' + (i + 1) + '">' +
                                item.material +
                                '<div class="info-tooltip">' +
                                '<button type="button" class="tooltip_btn">' +
                                '<span class="blind">컬러 상세 설명 팝업</span>' +
                                '</button>' +
                                '</div>' +
                                '</a>' +
                                '</li>';


                            $.each(item.color, function(j, prod) {
                                var colorHtml = '<li><button type="button" class="color_btn" data-color="' + prod.name.en + '" data-omni-type="microsite" data-omni="sec:bespoke:buy:step3:select color_' + prod.name.en + '">' + prod.name.kr + '</button></li>';
                                self_.aFilteredColor[i].push($(colorHtml).get(0));
                            });
                            self_.aMaterialBtn.push(materialBtn);
                            self_.aFilteredMaterial.push($(materialHTML).get(0));
                        })
                    },
                    appendHtml: function() {
                        var self_ = this.selector,
                            sColorList = sMYEDITION + '-color_list';

                        var tabBtn = function() {
                            self_.$TABBTN.html('');
                            $.each(self_.aMaterialBtn, function(i, item) {
                                self_.$TABBTN.append(item);
                            })
                        };

                        var tabView = function() {
                            $.each(self_.aFilteredColor, function(i, item) {
                                self_.$TABVIEW.contents().remove();
                                self_.$TABVIEW.append(self_.aFilteredMaterial)
                                    .find('>li')
                                    .eq(i)
                                    .find(sColorList)
                                    .html('')
                                    .append(item);
                            });
                        };

                        tabBtn();
                        tabView();
                    },
                    reStyle: function() {
                        var $el = this.selector.$TABBTN.find('li');
                        $el.css('width', 'calc(100% / ' + $el.length + ')')
                    },
                    act: function() {
                        this.makeHTML();
                        this.appendHtml();
                        this.reStyle();
                    }
                },
                //step2 선택시 변경될 step4 내용
                gridModel: {
                    selector: {
                        choicedType: '',
                        aModel: []
                    },
                    makeHTML: function() {
                        var self_ = this;
                        this.selector.choicedType = data.filter(function(item) {
                            return item.dataType === modelData.type;
                        })[0];
                        self_.selector.aModel = [];

                        var haveFunc = function(convenience) {
                            if (convenience === 'O') {
                                return 'class="have_function"';
                            } else {
                                return '';
                            }
                        };

                        $.each(this.selector.choicedType.product, function(i, item) {
                            var type = '<td scope="col"' + haveFunc(item.convenience['빅아이스메이커']) + '></td>' +
                                '<td scope="col"' + haveFunc(item.convenience['이온살균청정기']) + '></td>' +
                                '<td scope="col"' + haveFunc(item.convenience['UV청정탈취']) + '></td>';

                            var modelHTML = '<tr>' +
                                '<td scope="col"><input type="radio" name="model" id="model' + i + '" value="" class="model_rdobox" data-url=' + item.url + ' data-omni-type="microsite" data-omni="sec:bespoke:buy:step4:select model_' + item.modelName + '"><label for="model' + i + '">' + item.modelName + '</label></td>' +
                                '<td scope="col">' + item.energy + '</td>' +
                                (function() {
                                    if (aDOORLENG4.indexOf(modelData.type) > -1) {
                                        return type;
                                    } else {
                                        return '';
                                    }
                                })() +
                               '<td scope="col">' + controlNumber.numberWithCommas(item.price) + '원</td>' +
                                '</tr>';

                            self_.selector.aModel.push($(modelHTML).get(0));
                        })
                    },
                    appendHtml: function() {
                        //20190620 
                        $('.my-edition-model-tbl').removeClass('is_show');
                        $(sTABLE).find('tbody').html('');
                        $(sSTEP04 + ' .my-edition-model-tbl.is_active table').find('tbody').append(this.selector.aModel);
                    },
                    tableCase: function() {
                        var $table = $('.step-box_function');
                        //20190620 
                        if (aDOORLENG4.indexOf(modelData.type) > -1) {
                            $table.find('.type1').addClass('is_active');
                            $table.find('.type2').removeClass('is_active');
                        } else {
                            $table.find('.type2').addClass('is_active');
                            $table.find('.type1').removeClass('is_active');
                        }
                    },
                    act: function() {
                        this.tableCase();
                        this.makeHTML();
                        this.appendHtml();
                    }
                },
                //step2 선택시 변경될 .my-edition-making-selection 내용
                gridRef: {
                    selector: {
                        stypeImg: sMODEL + ' ' + '.type-door_img'
                    },
                    changeModel: function() {
                        /* [D] 타입 선택시 첫번째 문짝 선택 : 2019-06-13 */
                        var el = $('.my-edition-making-ref[data-type=' + check1Door(modelData.type) + ']');
                        el.addClass(sACTIVE).siblings().removeClass(sACTIVE);
                        // el.find('button').eq(0).trigger('click');
                        window.setTimeout(function() {
                            el.find('button').eq(0).addClass(sACTIVE);
                        }, 500);
                        /* //[D] 타입 선택시 첫번째 문짝 선택 : 2019-06-13 */
                    },
                    act: function() {
                        this.changeModel();

                    }
                },
                act: function(e, $el, time) {
                    e.preventDefault();
                    var $el = $el || $(this)
                    choiceModel.afterChoice.saveModelData($el);
                    choiceModel.afterChoice.gridColor.act();
                    choiceModel.afterChoice.gridModel.act();
                    choiceModel.afterChoice.gridRef.act();
                }
            }
        }

        // Step 1 선택시
        var choiceDep = {
            selector: {
                aDepartmentedModel: []
            },
            cleanArr: function() {
                this.selector.aDepartmentedModel = [];
            },
            checkDep: function($el) {
                var self_ = this;

                this.cleanArr();

                aType = [];
                choiceModel.init.act();

                if ($el === undefined) {
                    self_.selector.aDepartmentedModel = aType;
                } else {
                    var department = $el.attr('data-dep');
                    $.each(aType, function(i, item) {
                        if ($(aType[i]).find('a').attr('data-dep') === department) {
                            self_.selector.aDepartmentedModel.push($(this));
                        }
                    })
                }

            },
            changeList: function() {
                var self_ = this;

                $('.ref-module-list .slider-item').remove();
                $.each(this.selector.aDepartmentedModel, function(i, item) {
                    $('.ref-module-list').append(item);
                })
            },
            resetChoice: function() {
                $(sSTEP03).find('.my-edition-color_list').html('');
              //20190620
                $('.my-edition-model-tbl').removeClass('is_show');
                $(sTABLE).find('tbody').html('');
            },
            act: function($el) {
                this.checkDep($el);
                this.changeList();
                this.resetChoice();
                aTablePrice = [];
                if ($el === undefined) {
                    slickAct.act('init');
                } else {
                    slickAct.act();
                }
            }
        }

        //.my-edition-making-selection의 냉장고 타입과 용량
        var changeContent = function($this) {
            var dataType = $($this).attr('data-type');
            var filteredType = data.filter(function(type) {
                return type.dataType === dataType;
            })[0];


            $(sVOLUME).show().find('span').each(function(i) {
                $(this).text(controlNumber.numberWithCommas(filteredType.volume[i]));
            });

            $('.my-edition-making_name').text(filteredType.type + ' : ' + filteredType.subject);
        };

        //스탭별 클릭시 실행할 내용
        var gridStep = function() {
            choiceModel.init.act();
            slickAct.act('init');

            $(sSTEP01).find('.my-edition-step_cont .type-select_btn').click(function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $self = $(this);

                var common = function() {
                    moveNextStep($self);
                    reset.act();
                    choiceDep.act($self);
                $self.addClass(sACTIVE).removeClass(sINACTIVE).parents('.my-edition-cont-inner').siblings().find('.type-select_btn').removeClass(sACTIVE).addClass(sINACTIVE); // 20190624 수정, 20190620 추가
                }

                stepAlert('prev', $('.step-box_module'), 'a', function() {}, function() {}, common)
            });

            $(document).on('click', '.ref-module-list .slider-item a', function(e) {
                e.preventDefault();
                var $self = $(this);

                var common = function() {
                    moveNextStep($self);
                    toggleView.showModel.show();
                    changeContent($self);
                    changeClass($self, '.module_type', '.module_type');
                    reset.act('tab');
                    choiceModel.afterChoice.act(e, $self);
                }

                if (stepAlert('next', $(sSTEP01), '.type-select_btn')) return false;

                if ($('.color_btn.' + sACTIVE).length > 0) {
                    stepAlert('prev', $('.step-box_color'), 'button', function() { flag = false }, function() {}, common)
                } else {
                    common();
                    flag = true;
                }

                if (flag) {
                    stepAlert('prev', $('.step-box_color'), 'button', function() { flag = false; }, function() {}, common)
                }
            });

            $(document).on('click', sTABLE + ' tbody input[type=radio]', function(e) {
                if (stepAlert('next', $('.step-box_module'), 'a')) return false;

                if ($(this).parents('tr').hasClass('clickBg')) {
                    toggleView.trBg.click.hide($(this));
                } else {
                    toggleView.trBg.click.show($(this));

                }
            });
        };

        //가격정보 컨트롤
        var aPanelPrice = [],
            aTotalPrice = [],
            settingPrice = {
                selector: {
                    filteredType: function() {
                        var type = data.filter(function(type) {
                            return type.dataType === modelData.type;
                        })[0];
                        return type;
                    }
                },
                changePrice: function() {
                    var filteredType = this.selector.filteredType();
                    $(sTABLE).find(' tbody tr').each(function(i) {
                        var modelName = $(this).find('td:first-child label').text(),
                            filteredModel = filteredType.product.filter(function(model) {
                                return model.modelName === modelName;
                            })[0];

                        var totalPrice = parseInt(filteredModel.price) + controlNumber.getSumPrice(aPanelPrice);
                        aTotalPrice[i] = totalPrice;
                        $(this).find('td:last-child').text(controlNumber.numberWithCommas(totalPrice) + '원');
                    })
                },
                resetPrice: function() {
                    var filteredType = this.selector.filteredType();
                    aPanelPrice = [];
                    aTotalPrice = [];
                    totalPrice = 0,
                        $(sTABLE).find(' tbody tr').each(function() {
                            var modelName = $(this).find('td:first-child label').text(),
                                filteredModel = filteredType.product.filter(function(model) {
                                    return model.modelName === modelName;
                                })[0];

                            var modelPrice = controlNumber.numberWithCommas(parseInt(filteredModel.price));
                            $(this).find('td:last-child').text(modelPrice + '원');
                        })
                }
            }

        //냉장고 버튼(.door_btn) 누를시
        var choiceColorBtn = function() {
            var addActive = function() {
                $(document).on('click', sREF + ' ' + sBTN, function() {
                    if ($(this).hasClass(sACTIVE)) {
                        $(this).removeClass(sACTIVE)
                    } else {
                        changeClass($(this), sBTN);
                    }
                });

                $('html').click(function(e) {
                    if (e.target.tagName === 'DIV') {
                        $(sREF + ' ' + sBTN).removeClass(sACTIVE);
                    }
                });
            };

            var changeColor = function() {
                var $COLORBTN = $(sSTEP03).find('.my-edition-color_list ' + sBTN),
                    filteredPanel;

                var setDoorData = function($active, colorData) {
                    var i = $active.parent().index(),
                        doorIdx = $active.parent().index() + 1;

                    var filteredPanel = data.filter(function(panel) {
                        return modelData.type === panel.dataType;
                    })[0].panel;

                    if (aDOORLENG4.indexOf(modelData.type) > -1) {
                        if (i < 2) {
                            i = 0;
                        } else {
                            i = 1;
                        }
                    }

                    $.each(filteredPanel[i], function(k, panelCode) {
                        if (panelCode[colorData] !== undefined) {
                            var idx = $active.parent().index();
                                $active.attr({'data-img' : panelCode[colorData].panelCode, 'data-price': panelCode[colorData].price}) // 20190620
                                .css('background-image', 'url(' + sImgPath + 'door/' + (function() {

                                    aPanelPrice[idx] = parseInt(panelCode[colorData].price);
                                    $active.find('strong').hide();

                                    if (aDOORLENG4.indexOf(modelData.type) > -1) {
                                        if (doorIdx % 2 === 0) {
                                            return panelCode[colorData].panelCode + '_right';
                                        } else {
                                            return panelCode[colorData].panelCode + '_left';
                                        }
                                    } else {
                                        return panelCode[colorData].panelCode;
                                    }
                                })() + '.png?$ORIGIN_PNG$)');

                            return false;
                        }
                    });

                };

                //컬러탭의 컬러 선택시
                $(document).on('click', '.my-edition-color_list ' + sBTN, function() {
                    var $activeBtn = $(sREF).find(sBTN + '.' + sACTIVE);
                    if (stepAlert('next', $('.step-box_module'), 'a')) return false;
                    var pushColorName = function($active, colorData, colorName) {
                        if ($active.find('.color').length === 0) $active.append('<span class="color"></span>')
                        $active.attr(sDATACOLOR, colorData, colorName).find('.color').text(colorName);
                    };

                    if ($(sREF).find(sBTN).hasClass(sACTIVE)) {
                        var colorAttr = $(this).attr(sDATACOLOR),
                            colorText = $(this).text();

                        pushColorName($activeBtn, colorAttr, colorText);
                        setDoorData($activeBtn, colorAttr);
                        settingPrice.changePrice();
                    } else {
                        popup.confirm_(2, '냉장고의 도어를 선택해 주세요.')
                        return false;
                    }
                    changeClass($(this), sBTN);
                   // 20190620 step04로 자동 이동
                    if($(sREF).find(sBTN + ':visible').length == $(sREF).find(sBTN + '[data-img]').length){
                        $('.my-edition-model-tbl.is_active').addClass('is_show');
                        // moveNextStep($(this));
                    }
                });

                //validation 체크 후 저장
                var saveBtn = {
                    getModelName: function() {
                        return $(sTABLE).find(' input[name=model]:checked + label').text();
                    },
                    flag: function() {
                        if ($(sREF).find(sBTN + ':visible').length !== $(sREF).find(sBTN + '[data-img]').length) {
                            popup.confirm_(2, '모든 컬러가 선택되지 않았습니다.<br/>선택 완료 후 저장해 주시기 바랍니다.')
                            return false;
                        } else if ($(sTABLE).find('input[name=model]:checked').length === 0) {
                            popup.confirm_(2, '모델이 선택되지 않았습니다.<br/>선택 완료 후 저장해 주시기 바랍니다.')
                            return false;
                        } else {
                            return true;
                        }
                    },
                    valid: function($this_) {
                        if (this.flag()) {
                            var panelData = [],
                                selectedModelIdx = $(sTABLE).find('tbody tr.active').index(),
                                totalPrice = aTotalPrice[selectedModelIdx];

                            $(sREF).find(sBTN).each(function(i) {
                                panelData.push({});

                                var dataImg = $(this).attr('data-img'),
                                dataprice = $(this).attr('data-price'),
                                dataColor = $(this).find('.color').text();
                                    panelData[i].code = dataImg;
                                    panelData[i].color = dataColor;
                                    panelData[i].panelprice = dataprice;
                            })

                            modelData.model = this.getModelName();
                            modelData.url = $("input:radio[name=model]:checked").attr('data-url');
                            settingCookie.makeProd(modelData.type, modelData.model, panelData, totalPrice, modelData.url);
                            initCookieHTML();

                            if ($('.my-edition-pick-section .my-edition-pick-item').length > 10) {
                                popup.confirm_(2, '10개까지 담을 수 있습니다.<br/>삭제하고 담아주세요.')
                                settingCookie.deleteProd($('.my-edition-pick-item:last-child'), 'last')
                                return false;
                            }

                            if (savedLength.length !== $('.my-edition-pick-section .my-edition-pick-item').length) {
                                popup.confirm_(2, '쿠키에 공간이 부족하여 더 담을 수 없습니다.<br/>삭제하고 담아주세요.');
                                $('.cookie_length').text(prodList.length);
                                return false;
                            }

                            reset.act();

                            var scrollPosMy = $('.my-edition-pick-section').offset().top - $('.promo-nav-inner').outerHeight();
                            $('html,body').animate({ scrollTop: scrollPosMy }, 300);

                        }
                    },
                    init: function() {
                        var self_ = this;

                        $(sSAVEBTN).click(function() {
                            self_.valid($(this));
                        })

                    }
                };

                //AR
                var arBtn = {
                    arr_checkdIndex: [],
                    getModelName: function() {
                        return $(sTABLE).find(' input[name=model]:checked + label').text();
                    },
                    flag: function(boolean) {
                        if (boolean) {
                            return true;
                        }

                        if ($(sREF).find(sBTN + ':visible').length !== $(sREF).find(sBTN + '[data-img]').length) {
                            popup.confirm_(2, '모든 컬러가 선택되지 않았습니다.<br/>선택 완료 후 저장해 주시기 바랍니다.')
                            return false;
                        } else if ($(sTABLE).find('input[name=model]:checked').length === 0) {
                            popup.confirm_(2, '모델이 선택되지 않았습니다.<br/>선택 완료 후 저장해 주시기 바랍니다.')
                            return false;
                        } else {
                            return true;
                        }
                    },
                    checkbox_changedIndex: function() {
                        var _ns = this;
                        /* 체크박스 순서를 기록 */
                        $('body').on('change', '.ar-check', function() {
                            var id = $(this).attr('id');
                            var idx = _ns.arr_checkdIndex.indexOf(id);

                            if ($(this).is(':checked')) {
                                /* 배열 추가 */
                                if (idx == -1) {
                                    _ns.arr_checkdIndex.push(id);
                                }
                            } else {
                                /* 배열 삭제 */
                                if (idx !== -1) {
                                    _ns.arr_checkdIndex.splice(idx, 1);
                                }
                            }
                             // console.log(_ns.arr_checkdIndex);
                        });
                    },

                    valid: function($this_, boolean) {

                        if (this.flag(boolean)) {

                            var panelData = [],
                                selectedModelIdx = $(sTABLE).find('tbody tr.active').index(),
                                totalPrice = aTotalPrice[selectedModelIdx],
                                txt = '';

                            //AR내 공간에서 체험하기 팝업
                            if (boolean) {


                                var modelData = [];
                                var els = [];
                                for (var j in this.arr_checkdIndex) {
                                    els.push($('#' + this.arr_checkdIndex[j]).closest('.my-edition-pick-item'));
                                }
                            
                                $.each(els, function(i) {

                                    var idx = $(this).index();
                                    var modelName = prodList[idx].name;
                                    modelData.push([]);
                                    modelData[i].push(modelName);
                                    $.each(prodList[idx].panel, function(j, item) {
                                        modelData[i].push(item.code.toUpperCase());
                                    });

                                    modelData[i] = modelData[i].join('|');
                                    txt = modelData.join('.');
                                });

                                /*  $('.ar-check:checked').closest('.my-edition-pick-item').each(function(i) {
                                      var idx = $(this).index();
                                      var modelName = prodList[idx].name;
                                      modelData.push([]);
                                      modelData[i].push(modelName);
                                      $.each(prodList[idx].panel, function(j, item) {
                                          modelData[i].push(item.code.toUpperCase());
                                      });

                                      modelData[i] = modelData[i].join('|');
                                      txt = modelData.join('.');
                                      // txt = modelData.serialize();
                                  });*/
                                // console.log(modelData)
                            } else {

                                var modelName = arBtn.getModelName();
                                panelData = [];
                                $(sREF).find(sBTN).each(function(i) {
                                    var dataImg = $(this).attr('data-img').toUpperCase();
                                    panelData[i] = dataImg;
                                });
                                txt = modelName + '|' + panelData.join('|');
                            }


                            if ($('.ar-check:checked').length > 3) {
                                alert('한번에 3개까지 선택 가능합니다.');
                                return false;
                            }
                            var arurl= 'Intent://action123?id=' + txt + '#Intent;scheme=schemearshowroom;package=com.samsung.sec.arshowroom;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;S.browser_fallback_url=https://play.google.com/store/apps/details?id=com.samsung.sec.arshowroom;end'
                             // console.log(arurl);
                            location.href = arurl;      
                        }
                    },
                    init: function() {
                        var self_ = this;


                        self_.checkbox_changedIndex();

                        //.my-edition-making-step-wrap
                        $('.my-edition-making-step-wrap .ar_btn').click(function() {
                            self_.valid($(this));
                            // console.log('ar  ')
                        });

                        //AR내 공간에서 체험하기 팝업
                        $ARPOPUP.find('.btn').click(function() {
                            // console.log('체험  ')

                            self_.valid($(this), true);
                        })

                    }
                };

                saveBtn.init();
                arBtn.init();
            };

             // 20190620 다시만들기
            var resetProduct = function() {
                $('.remake_btn').click(function() {
                    popup.confirm_(1, '선택된 내용을 다시 시작하시겠습니까?',  function(){ 
                        reset.act('remake'); 
                    }); 
                });
            };

            addActive();
            changeColor();
            resetProduct();
        };

        //step3 탭
        var tab = {
            init: function($this_) {
                $(sTABBTN).find('>li').removeClass(sACTIVE);
                $this_.parent().addClass(sACTIVE);

                var tab = $this_.parent().attr("rel");
                $(sTABVIEW).find('>li').hide();
                $(sTABVIEW).find('>li[data-id=' + tab + ']').show();
            },
            reset: function($this_) {
                var self_ = this;
                if ($(sTABVIEW).find('button.' + sACTIVE).length) {
                    popup.confirm_(1, '같은 재질끼리만 조합이 가능합니다.<br />다른 재질과 컬러로 변경하시겠습니까?', function() {
                        reset.act('tab');
                        self_.init($this_);
                    });
                } else {
                    self_.init($this_);
                }

            },
            act: function() {
                var self_ = this;
                if ($(sTABBTN).length) {
                    $(document).on('click', sTABBTN + ' >li a', function(e) {
                        e.preventDefault();
                        self_.reset($(this));
                    });
                }
            }
        }

        //Step 4 테이블 bg
        var bgHover = function() {
            $(document).on('mouseenter', sTABLE + ' tbody tr', function() {
                toggleView.trBg.hover.show($(this));
            }).on('mouseleave', sTABLE + ' tbody tr', function() {
                toggleView.trBg.hover.hide($(this));
            });
        };

        var slickAct = {
            sEl: '.ref-module-list',
            option: {
                arrows: true,
                pauseOnHover: false,
                pauseOnDotsHover: false,
                pauseOnFocus: false,
                infinite: false,
                slidesToShow: 4,
                slidesToScroll: 1,
                prevArrow: '<button type="button" class="slick-prev"><span>이전</span></button>',
                nextArrow: '<button type="button" class="slick-next"><span>다음</span></button>',
                responsive: [{
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3
                    }
                }]
            },
            act: function(check) {
                if ($('.module_type').length <= 4) {
                    $('.ref-module-list').addClass('default')
                }

                function createNewEvent(eventName) {
                    var event;
                    if (typeof(Event) === 'function') {
                        event = new Event(eventName);
                    } else {
                        event = document.createEvent('Event');
                        event.initEvent(eventName, true, true);
                    }
                    return event;
                }

              // 20190620
                var prodChk = $('.module_type > a[data-type="2door"]')

                if (prodChk.length){
                    choiceModel.afterChoice.act(createNewEvent(), $('.module_type > a[data-type="2door"]'), check)
                } else {
                    choiceModel.afterChoice.act(createNewEvent(), $('.module_type > a[data-type="6door"]'), check)
                }

            },
            destroy: function() {
                $(this.sEl).slick('unslick');
            }
        }

        //키엔호 파라미터 있을 경우 해당 위치로 이동
        var kienhoMove = function() {
            var getParamVal = function(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            };

            var paramId = getParamVal('id');
            if (paramId === 'kienho-more') {
                var scmove = $('#' + paramId).offset().top;
                $('html, body').animate({ scrollTop: scmove }, 400);
            }
            else if (paramId === 'bespoke-make') {
                var scmove = $('#' + paramId).offset().top - 50;
                $('html, body').animate({ scrollTop: scmove }, 400);
            }
            else if (paramId === 'bespoke-benefit') {
                var scmove = $('#' + paramId).offset().top - $(".promo-nav-outer").outerHeight() - 50;
                $('html, body').animate({ scrollTop: scmove }, 400);
            }else{

            }
        }

        gridStep();
        choiceColorBtn();
        checkCookie();
        tab.act();
        bgHover();
        setTimeout(kienhoMove, 1000);
    }

     // 20190620
    var changeClass = function($el, btn, parents) {
        if (parents === undefined) {
            $el.addClass(sACTIVE).parent().eq(0).siblings().find(btn).removeClass(sACTIVE); //20190624 수정
        } else if (btn == '.module_type') { //190624 추가
            $el.parents(parents).addClass(sACTIVE).removeClass(sINACTIVE).siblings().removeClass(sACTIVE).addClass(sINACTIVE);//190624 추가
        } else {
            $el.parents(parents).addClass(sACTIVE).siblings().removeClass(sACTIVE) //20190624 수정
        }
    };


    //팝업
    var popup = {
        //팝업 공통 동작
        commonAct: {
            open: function($el) {
                $el.addClass(sACTIVE);
                $('.my-edition-ly-dim').show();
            },
            close: function() {
                $('.my-edition-ly-popup').removeClass(sACTIVE);
                $('.my-edition-ly-dim').hide();
            },
        },
        //선택사항보기
        besPoke: function() {
            var self_ = this;
            var popupAction = {
                selector: {
                    popup: $('#layer01')
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var modelIdx = $(this).parents('li').index();

                    self_.commonAct.open(popupAction.selector.popup);
                    popupAction.selector.popup.find('table tbody').html(aSelectedOpt[modelIdx]);
                    popupAction.selector.popup.find('table thead').html(aSelectedOpt2[modelIdx]); //20190620 추가
                },
                close: function(e) {
                    e.preventDefault();
                    popupAction.selector.popup.find('table tbody').html('');
   
                },
            }

            $(document).on('click', '.js-popup', popupAction.open)
                .on('click', '.my-edition-ly-popup .btn-close', popupAction.close);
        },

      
        //step3 컬러 상세설명

        // 20190613 추가 
        // 1. src, title02, content02, list02, text02 키값 추가         
        // 2. 1415 ~ 1461 추가 
        colorTooltip: function() {
            var self_ = this;
            var color = [
                {
                    title: '코타 메탈 (Cotta Metal)',
                    content: '에폭시 분말을 원료로 사용하여 철, 알루미늄 등에 정전기를 이용해 분체 도장을 한 소재로 메탈에 컬러를 입혀 구워낸 듯한 질감이 특징입니다. 매트한 표면이 차분한 공간을 연출하며 생활 스크래치에 강하고 지문이 잘 묻지 않습니다.',
                    list: ['deepEspresso', 'pocelainW_M', 'pistachio'],
                    text: ['Cotta<br>Charcoal', 'Cotta<br>White', 'Cotta<br>Mint'],
                    src: ['img_cotta01', 'img_cotta02', 'img_cotta03', 'img_cotta04']
                },
                {
                    title: '글램 글래스 (Glam Glass)',
                    title02: '새틴 글래스 (Satin Glass)',
                    content: '진공증착법으로 유리에 고굴절 금속산화물을 코팅한 소재로 거울처럼 반사되는 고광택의 영롱한 느낌이 특징입니다. 스크래치에 강하며 유성 볼펜도 물로 쉽게 제거됩니다.',
                    content02: '유리를 매끈하게 연마하여 나노 단위의 초박막 내지문 코팅으로 부드러운 광택을 내며 시크하면서도 벨벳 같은 느낌이 특징입니다. 스크래치에 강하며 유성 볼펜도 물로 쉽게 제거됩니다.',
                    list: ['mysticPink', 'pocelainW_G'],
                    list02: ['stoneGray', 'silkyNavy', 'coral', 'yellow'],
                    text: ['Glam<br>Pink', 'Glam<br>White'],
                    text02: ['Satin<br>Gray', 'Satin<br>Navy', 'Satin<br>Coral', 'Satin<br>Yellow'],
                    src: ['img_glam01', 'img_glam02', 'img_glam03', 'img_glam04', 'img_satin01', 'img_satin02', 'img_satin03', 'img_satin04']
                }
            ]

            var popupAction = {
                selector: {
                    popup: $('#layer04')
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var idx = $(this).parents('li').index();

                    self_.commonAct.open(popupAction.selector.popup);
                    popupAction.selector.popup.find('.feature_tit').text(color[idx].title);
                    popupAction.selector.popup.find('.feature_txt').text(color[idx].content);

                    popupAction.selector.popup.find('.slick-slider').removeClass("slick-initialized").empty();

                    $.each(color[idx].src, function(i, item) {


                        var innerHtml = '<div>'
                        innerHtml += '<div class="img">'
                        innerHtml += '   <img src="' + sImgPath +'v2/' + item + '.jpg?$ORIGIN_PNG$" >';
                        innerHtml += '</div>'
                        innerHtml += '</div>'


                        popupAction.selector.popup.find('.slick-slider').append(innerHtml);


                    });



                    var slickOption = {
                        arrows : true,
                        autoplay: true, // 20190621 
                        autoplaySpeed : 2200, // 20190621 
                        pauseOnHover : false,
                        pauseOnDotsHover : false,
                        pauseOnFocus : false,
                        infinite: true, // 20190621
                        dots : true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        prevArrow: '<button type="button" class="slick-prev"><span>이전</span></button>',
                        nextArrow: '<button type="button" class="slick-next"><span>다음</span></button>',
                    }


                    popupAction.selector.popup.find('.slick-slider').slick(slickOption);

                    popupAction.selector.popup.find('.slick-slider').on('afterChange', function(event, slick, currentSlide) {
                        var $this = $(this);
                        if ($this.find('.slick-active img').attr('src').indexOf('satin') > -1) {
                            popupAction.selector.popup.find('.feature_tit').text(color[idx].title02);
                            popupAction.selector.popup.find('.feature_txt').text(color[idx].content02);
                        } else {
                            popupAction.selector.popup.find('.feature_tit').text(color[idx].title);
                            popupAction.selector.popup.find('.feature_txt').text(color[idx].content);
                        }
                    });

                    popupAction.selector.popup.find('.slick-slider').slick(slickOption);
                }
            }


            $(document).on('click', '.step-box_color .tooltip_btn', popupAction.open)

        },

        //step4 특장점 상세보기
        featurestTooltip: function() {
            var self_ = this;
            var features = [{
                    title: "빅 아이스메이커",
                    contents: "무더운 여름에 온 가족이 수시로 얼음을 꺼내먹으면 금방 얼음이 동나서 얼음 채우는 것도 번거로운 일이 되는데요. <br>빅 아이스메이커는 이런 수고로움을 줄여줍니다. 한번에 2 L의 <br>물을 넣으면 자동으로 얼음을 얼려 얼음 박스에 채워줍니다.",
                    caution: ["해당 모델에 한함"]
                },
                {
                    title: "이온 살균 청정기",
                    contents: "냉장고 내부에 떠다니는 균은 물론, 벽면에 <br>붙어있는 부착균까지 99 %이상 살균되므로 식재료를 안심하고 보관 할 <br>수 있습니다. 필터의 교체나 세척 없이 반영구적으로 쓸 수있어 <br>더욱 편리합니다.",
                    caution: ["살균력 99% KTR 인증 (시험대상균 대장균, 황색포도상구균 도포 24시간 후 잔여 제균 검증 조건)", "해당 모델에 한함"]
                },
                {
                    title: "UV 청정 탈취기",
                    contents: "90 % 이상 탈취가 가능하므로 냉장고 안의 김치 냄새, <br>생선 냄새 등을 효율적으로 제거하여 줍니다. <br>필터 성능이 약 10년간 유지되므로 번거롭게 필터를 교체할 <br>필요가 없습니다.",
                    caution: ["UV 청정탈취기 단품 살균 성능 99% 이상 (인터넥 성적서 확보, 대상균 대장균, 황색포도상구균)", "탈취력 90 % 이상, 인터텍 인증 (냉장고 상실에 시험 표준 gas를 주입 20시간 후 내부 가스 농도 측정 시험 결과)", "해당 모델에 한함 / 실사용 환경에 따라 달라질 수 있음."]
                }
            ];

            var popupAction = {
                selector: {
                    popup: $('#layer03')
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var idx = $(this).parents('th').index() - 2;
                    self_.commonAct.open(popupAction.selector.popup);
                    popupAction.selector.popup.find('.feature_desc').html('');
                    popupAction.selector.popup.find('.feature_tit').text(features[idx].title);
                    popupAction.selector.popup.find('.feature_img img').attr('src', '//images.samsung.com/is/image/samsung/p5/sec/bespoke/buy/v1/img_feature0' + (idx + 1) + '.jpg?$ORIGIN_PNG$');
                    popupAction.selector.popup.find('.feature_txt').html(features[idx].contents);
                    $.each(features[idx].caution, function(i, item) {
                        popupAction.selector.popup.find('.feature_desc').append('<p>' + item + '</p>');
                    });

                }
            }

            $('.step-box_function .tooltip_btn').click(popupAction.open);
        },
        //유의사항
        benefitLink: function() {
            var self_ = this;
            var benefit = [{
                    title: "삼성전자 포인트 유의 사항",
                    content: [
                        "각종 할인 및 쿠폰이 반영된 최종 결제금액 기준",
                        "멤버십포인트는 최종 결제금액 기준으로 적립되며,<br />기본 0.2%포인트는 연간적립 한도인 60,000P 내에서 적립",
                        "기본 0.2%, 각 모델 별 포인트, 500만원 이상 결제 시<br />결제금액의 4%(멤버십 회원대상) 모두 중복 적용되어 적립"
                    ]
                },
                {
                    title: "삼성 카드 결제시 유의 사항",
                    content: [
                        "삼성카드 : 통합 월 할인한도 / 월20만원 1인 기준",
                        "할인혜택의 경우 카카오페이,PAYCO를 통한 간편결제 건은 제외"
                    ]
                }
            ]

            var collabo = [{
                title: "배송 및 사은품 신청",
                content: [
                    "배송 기간 : 구매 결제 확정 후 1개월 이내 순차 발송 예정",
                    "사은품 신청 방법 : 주문 결제 시, 사은품 3종 중 1종 선택",
                    "신청 대상 : 삼성닷컴에서 BESPOKE 냉장고 구매 고객 전원",
                    "사은품 선택 후 변경은 상품이 배송되기 이전까지만 가능합니다"
                ]
            }]

            var popupAction = {
                selector: {
                    popup: $('#layer06')
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var idx, caution;
                    if ($(this).parents('div').hasClass('my-edition-content')) {
                        caution = benefit;
                        idx = $(this).parents('li').index();
                    } else if ($(this).parents('div').hasClass('my-edition-collabo-section')) {
                        caution = collabo;
                        idx = 0;
                    }
                    self_.commonAct.open(popupAction.selector.popup)
                    popupAction.selector.popup.addClass(sACTIVE);
                    popupAction.selector.popup.find('.hd-popup_tit').text(caution[idx].title);
                    popupAction.selector.popup.find('.notice-list').html('');
                    $.each(caution[idx].content, function(i, item) {
                        popupAction.selector.popup.find('.notice-list').append('<li>' + item + '</li>');
                    });
                }
            }
            $('.data_popup').click(popupAction.open);
        },
        // 타입선택 팝업
        typeChoice: function() {
            var self_ = this;
            var typeChoice = [{
                    title: "키친핏형 냉장고",
                    content: [
                        "주방의 냉장고 표준장/싱크대 기준으로 전면부가 돌출되지 않고 딱 맞게 설치됩니다.",
                        "좌우 설치폭 10 mm",
                        "키친핏 냉장고 2대 이상 구매 시 냉장고간 연결을 위한 설치용 자재 (RA-C07KAAAA) 별도 구매 필요합니다."
                    ]
                },
                {
                    title: "프리스탠딩형 냉장고",
                    content: [
                        "주방에 설치된 표준장/싱크대 기준으로 냉장고 전면부가 돌출된 형태로 설치됩니다.",
                        "깊이 약 90mm / 좌우 설치폭 50mm"
                    ]
                }
            ]
            
            var popupAction = {
                selector: {
                    popup: $('#layer07')
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    var idx, caution;
                    if ($(this).parents('div').hasClass('my-edition-content')) {
                        caution = typeChoice;
                        idx = $(this).closest('.my-edition-cont-inner').index();
                    } else if ($(this).parents('div').hasClass('my-edition-collabo-section')) {
                        caution = collabo;
                        idx = 0;
                    }
                    self_.commonAct.open(popupAction.selector.popup)
                    popupAction.selector.popup.addClass(sACTIVE);
                    popupAction.selector.popup.find('.hd-popup_tit').text(caution[idx].title);
                    popupAction.selector.popup.find('.notice-list').html('');
                    $.each(caution[idx].content, function(i, item) {
                        popupAction.selector.popup.find('.notice-list').append('<li>' + item + '</li>');
                    });
                }
            }
            $('.type_popup').click(popupAction.open);
        },
        //AR내 공간에서 체험하기
        arExperience: function() {
            var self_ = this;
            var aList = [];
            var popupAction = {
                selector: {
                    popup: $ARPOPUP
                },
                open: function(e) {
                    e.preventDefault();
                    e.stopPropagation();
                    self_.commonAct.open(popupAction.selector.popup)
                }
            }
            $('.my-edition-pick-section .ar_btn').click(popupAction.open);
        },
        /**
        //경고문
        * @param type 1-'예/아니오' 버튼이 있는 팝업/2-'예' 버튼만 있는 팝업 
        * @param txt 경고문
        * @param yesFn '예' 버튼 클릭시 실행할 내용
        * @param noFn '아니오' 버튼 클릭시 실행할 내용
        * @param coFn '예/아니오'에 관계없이 공통으로 실행할 내용
        */
        confirm_: function(type, txt, yesFn, noFn, coFn) {
            var self_ = this;
            var value;
            var popupAction = {
                selector: {
                    popup: $('#layer02')
                },
                open: function() {
                    popupAction.selector.popup.find('.popup_txts').html('<p>' + txt + '</p>');
                    self_.commonAct.open(popupAction.selector.popup);
                    $('.btn-deny').remove();
                    switch (type) {
                        case 1:
                            popupAction.selector.popup.find('.btn_wrap').prepend('<button type="button" class="btn btn-deny">아니오</button>')
                            break;
                    }
                },
                clickBtn: function() {
                    popupAction.selector.popup.find('.btn').off('click').click(function(e) {
                        $('.my-edition-ly-popup').removeClass(sACTIVE);
                        $('.my-edition-ly-dim').hide();

                        if ($(this).hasClass('btn-deny')) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (noFn !== undefined) {
                                noFn();
                                return false;
                            }
                        } else {
                            if (yesFn !== undefined) {
                                yesFn();
                            }
                        }

                        if (coFn !== undefined) {
                            coFn();
                        }
                    })
                }
            }
            popupAction.open()
            popupAction.clickBtn();
        },
        act: function() {
            var self_ = this;
            $(document).on('click', '.my-edition-ly-popup .btn-close', function() {
                self_.commonAct.close();
            });
            this.besPoke();
            this.colorTooltip();
            this.featurestTooltip();
            this.benefitLink();
            this.arExperience();
             this.typeChoice();
        }
    }

    //바로 구매하기
    var buyNow = function() {
        $(document).on('click', '.my-pick_btn_buy', function() {
            var idx = $(this).parents('.my-edition-pick-item').index(),
                choicedProd = prodList[idx],
                data = choicedProd.name + ':::1';


            $.each(choicedProd.panel, function(i, item) {
                var panel = item.code.toUpperCase();
                data += '&productCode=' + panel + ':::1';
            });

            //프레임 + 상단 좌 + 상단 우 + 하단 좌 + 하단 우
            var option = {
                url: 'https://store.samsung.com/sec/ng/p4v1/buyNowTogether?productCode=' + data,
                dataType: "jsonp",
                type: "GET",
                success: function(result) {
                    if (result.resultCode == '0000') {
                        // 장바구니 페이지로 이동
                        location.href = 'https://store.samsung.com/sec/cart/buynow';
                    } else {
                        // resultMessage 화면에 노출
                        alert(result.resultMessage);
                    }
                },
                error: function(response, status, error) {
                    alert('오류');
                },
                beforeSend: function(xhr) {
                    xhr.setRequestHeader("ajax", true);
                },
                complete: function() {}
            };

            $.ajax(option);
        })
    };

    var dirbuyNow = function() {
        $(document).on('click', '.buy_btn', function() {
            if ($(sREF).find(sBTN + ':visible').length !== $(sREF).find(sBTN + '[data-img]').length) {
                popup.confirm_(2, '모든 컬러가 선택되지 않았습니다.<br/>선택 완료 후 구매해 주시기 바랍니다.')
                return false;
            } else if ($(sTABLE).find('input[name=model]:checked').length === 0) {
                popup.confirm_(2, '모델이 선택되지 않았습니다.<br/>선택 완료 후 구매해 주시기 바랍니다.');
            } else {
                var dirbuyName = $(sTABLE).find(' input[name=model]:checked + label').text();
                data = dirbuyName + ':::1';
                $('.my-edition-making-ref.active button').each(function() {
                    var panel = $(this).attr('data-img').toUpperCase();
                    data += '&productCode=' + panel + ':::1';
                });
                //프레임 + 상단 좌 + 상단 우 + 하단 좌 + 하단 우
                var option = {
                    url: 'https://store.samsung.com/sec/ng/p4v1/buyNowTogether?productCode=' + data,
                    dataType: "jsonp",
                    type: "GET",
                    success: function(result) {
                        if (result.resultCode == '0000') {
                            // 장바구니 페이지로 이동
                            location.href = 'https://store.samsung.com/sec/cart/buynow';
                        } else {
                            // resultMessage 화면에 노출
                            alert(result.resultMessage);
                        }
                    },
                    error: function(response, status, error) {
                        alert('오류');
                    },
                    beforeSend: function(xhr) {
                        xhr.setRequestHeader("ajax", true);
                    },
                    complete: function() {}
                };
                $.ajax(option);
            }
        })
    };

    //AR내 공간에서 체험하기 팝업 pc 숨김
    var showArPopup = function() {
        var pc = function() {
            if ($ARPOPUP.is(':visible')) {
                $ARPOPUP.remove(sACTIVE);
                popup.commonAct.close();
            }
        };
        resizeFunc(pc);
    };

    //samsung X kienho collaboration 슬라이드
    var collaboSlick = function() {
        var option = {
            autoplay: true,
            arrows: false,
            pauseOnHover: false,
            pauseOnDotsHover: false,
            pauseOnFocus: false,
            infinite: true,
            dots: true,
            prevArrow: '<button type="button" class="slick-prev"><span>이전</span></button>',
            nextArrow: '<button type="button" class="slick-next"><span>다음</span></button>',
        }
        $('.my-edition-collabo-kienho').slick(option);
    };
    // 20190619 : 최상단 냉장고 이미지 영역 순서 바꾸기
    var selectionOrderChange = function(mobileFlag) {
        var $sMODEL = $(sMODEL);

        if (mobileFlag) {
            $(sSTEP03).prepend($sMODEL);
        } else {
            $('.my-edition-making-wrap').prepend($sMODEL);
        }
    };
    $('.promo-tab-1dep-li.on a').click(function() {
        var scrollPosMake = $(".my-edition-making-wrap").offset().top;
        var body = $("html, body");
        body.stop().animate({ scrollTop: scrollPosMake - 50 }, 500, 'swing', function() {
            $('.my-edition-making-wrap').trigger('focus');
        });
    });

    var json_host = location.host.split(".")[0];
    customizingRef.init = function() {
        mobileFlag = window.innerWidth <= 768 ? true : false;
        prevSize = mobileFlag == true ? "mobile" : "pc";
        currentSize = mobileFlag == true ? "mobile" : "pc";
        selectionOrderChange(mobileFlag);
        $.getJSON('data.json', init);

        popup.act();
        buyNow();
        dirbuyNow();
        collaboSlick();
        showArPopup();
       //190619
        if(!mobileFlag){
            selectionFixedFunc.selectionFixedPc();
        }
    };

    return customizingRef;
})(document, window, jQuery);

$(document).ready(function() {
    customizingRef.init();
});