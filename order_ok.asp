<!--#include virtual="/Library/ajax_config.asp"-->
<%

	Set Upload = Server.CreateObject("TABSUpload4.Upload")
	Upload.CodePage = 65001
	Upload.MaxBytesToAbort = 10 * 1024 * 1024

	root_path = Server.MapPath("/")
	save_path = root_path & "\upload\"

	'Upload.Start "c:\Temp"
	Upload.Start save_path

	month_type = Month(Now)
	day_type   = Day(Now)

	If Len(month_type) = 1 Then 
		month_type = "0"&month_type
	End If 

	If Len(day_type) = 1 Then 
		day_type = "0"&day_type
	End If 

	GD_SEQ			= Upload.Form("GD_SEQ")			 '상품코드
	P_EA			= Upload.Form("P_EA")			 '수량
	user_name		= Upload.Form("user_name")		 '수령인명
	cust_email1		= Upload.Form("cust_email1")	 '수령인우편번호1
	cust_email2		= Upload.Form("cust_email2")	 '수령인우편번호1
	tel1			= Upload.Form("tel1")			 '수령인전화지역번호
	tel2			= Upload.Form("tel2")			 '수령인전화국번
	tel3			= Upload.Form("tel3")			 '수령인전화국번
	zip				= Upload.Form("zip")			 '수령인우편번호1
	addr1			= Upload.Form("addr1")			 '수령인 주소1
	addr2			= Upload.Form("addr2")			 '수령인 주소2
	delivery_memo	= Upload.Form("delivery_memo")	 '배송메모 
	gdorderlist		= Upload.Form("gdorderlist")	 '상품구매내역 
	addr			= addr1 &" "&addr2
	Item_Count		= Upload.Form("Item_Count") '	주문한 상품수 예)3가지 상품
	isscdnm			= Upload.Form("isscdnm")	'카드
	trno			= Upload.Form("strtrno")
	cust_email		= cust_email1&"@"&cust_email2
	S_DATE			= Year(Now)&month_type & day_type '주문일 계산
	mobile_gb		= Upload.Form("mobile_gb")
	POINT			= Upload.Form("POINT")
	or_else_memo	= Upload.Form("or_else_memo")
	
	
	Set File = Upload.Form("company_file")

	org_filename = File.FileName
	save_filename = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename,3)
	ext = LCase(mid(org_filename,InstrRev(org_filename,".")+1))
	
	if org_filename <> "" then
		chkext = array("jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf")
		chk = 0
		for i = 0 to UBound(chkext)
		if ext = chkext(i) then
			chk = chk + 1
			exit for
		end if
		next

		if chk = 0 then  
			%>
			<script>
				parent.alertBox("JPG, GIF, PNG, DOC(DOCX), PPT(PPTX), XLS(XLSX), HWP, ZIP, PDF 파일만 <br>업로드가 가능합니다.");
			</script>
			<%
			response.end
		end if

		if org_filename = "" then
			tmp_save_filename= "||"
		else
			tmp_save_filename = org_filename & "|" & save_filename & "|" & ext
		end if
			
		File.SaveAs save_path & save_filename , False
	else
		tmp_save_filename= "||"
	end If
	

	DBOpen()

	If Item_Count > 1 Then
		Array_GD_SEQ = Split(GD_SEQ,",")
		Array_P_EA = Split(P_EA,",")
	End If

	'상품갯수만큼 for 문
	No_Tax_Price = 0
	For i=0 To (Item_Count-1)
	'주문 상품이 여러개일때 배열상품과 갯수를 처리
	If Item_Count > 1 Then
		GD_SEQ = Array_GD_SEQ(i)
		P_EA = Array_P_EA(i)
	Else
		GD_SEQ = GD_SEQ
		P_EA = P_EA
	End If
	
	Set PRs = Product_View(GLOBAL_VAR_ONLINECD,"",GD_SEQ,"")
	
	'상품원가
	GD_PRICE = PRs("GD_PRICE")

	'================== 개별 상품 부가세 면세 관련 금액 합계 처리 =======================================================
	If PRs("Tax_Free") = "Y" Then
		'공급가액
		Product_Provide = CLng(PRs("GD_PRICE3")) * CLng(P_EA)
		Product_Tax = "0"
		'비과세 상품 합계
		No_Tax_Price = CDbl(No_Tax_Price) + CDbl(Product_Provide)
	Else 
		'공급가액은 반올림 부가세는 버림
		'공급가액
		Product_Provide = Round(CLng(PRs("GD_PRICE3")) * CLng(P_EA) / 1.1)
		'부가세
		Product_Tax = FIX(CLng(Product_Provide) * 0.1)
		'부가세 금액이 소수점이 나오는 것을 방지 상품원가에서 공급가액을 뺀금액을 부가세로계산
		Product_Tax = (CLng(PRs("GD_PRICE3"))* CLng(P_EA))-CLng(Product_Provide)
	End If
	'================== 상품 부가세 면세 관련 금액 합계 처리 =======================================================

	'합계금액
	Product_Sum = CLng(PRs("GD_PRICE3"))* CLng(P_EA)

	GD_TP = PRs("GD_TP")

	'셋트 상품인경우 셋트 배열 담기
	If GD_TP = "셋트" Then
		GD_SET = PRs("GD_SET")
	End If

	Delivery_TP = "EndUser배송"

	O_Info = O_Info&GD_SEQ&"ㅸ"&PRs("UNI_GRP_CD")&"ㅸ"&PRs("GD_PRICE3")&"ㅸ"&P_EA&"ㅸ"&Product_Provide&"ㅸ"&Product_Tax&"ㅸ"&GD_TP&"ㅸ"&GD_SET&"ㅸ"&GD_PRICE&"ㅸ"&Delivery_TP&"ㅸ"&File_Type&"ㅸㆄ"
	
	Next

	'배송비 관련 처리
	If Delivery_YN = "Y" Then
		O_Info = O_Info&"2851ㅸ"&PRs("UNI_GRP_CD")&"ㅸ2500ㅸ1ㅸ2250ㅸ250ㅸ일반ㅸㅸ2500ㅸEndUser배송ㅸNㅸNㅸㆄ"
	End If


	'Response.write O_Info
	'Response.End 

	'주문번호 생성(주문번호 먼저 생성되는 버전)	
	Dim rndValue, rndSeed,or_num,sub_gubun_Upload.Form
	rndSeed = 20 '1~40
	 
	Randomize ' 난수 발생기 초기화
	rndValue = Int((rndSeed * Rnd) + 1) ' 1에서 40까지 무작위 값 발생
	 

	or_num = mobile_gb&"2001"&Year(Now())&Month(Now())&day(Now())&Hour(Now())&minute(Now())&second(Now())&rndValue

	'response.write "user_name		:"&  user_name		&"<br>"
	'response.write "cust_email1		:"&  cust_email1	&"<br>"
	'response.write "cust_email2		:"&  cust_email2	&"<br>"
	'response.write "tel1			:"&      tel1		&"<br>"
	'response.write "tel2			:"&      tel2		&"<br>"
	'response.write "tel3			:"&      tel3		&"<br>"
	'response.write "zip				:"&  zip			&"<br>"
	'response.write "addr1			:"&  addr1			&"<br>"
	'response.write "addr2			:"&  addr2			&"<br>"
	'response.write "delivery_memo	:"&  delivery_memo	&"<br>"
	'Response.write "gdorderlist		:"&   gdorderlist&"<br>"

	'카드결제에서 넘어온 카드명과 카드번호
	
	If InStr(isscdnm, "No Information") > 0 Then 
			sndPaymethod = "35" 
			point_acntnm = "휴대폰결제"
			point_acntnum = "0001"
			INAMT_result	= "Y"
	ElseIf isscdnm <> "" Then
			sndPaymethod = "02" 
			point_acntnm = isscdnm
			point_acntnum = trno
			INAMT_result	= "Y"
	Else 
		If POINT > 0 Then 
			sndPaymethod = "Z6" 
			point_acntnm =  "경찰공제회상시몰 적립포인트"
			point_acntnum = "9990"
			INAMT_result	= "Y"
		End If 
	End If 
	

	If POINT = "" Then 
		POINT = 0
	End If 
	
	If or_else_memo <> "" Then 

		or_else_memo = "현금영수증 : "&or_else_memo

	End If 

	strqry =  "Exec itemcenter.dbo.IC_PROC_ORDER_NEW_PMAA "
	strqry = strqry & "  @OR_WRITE_ID         = '" & GLOBAL_VAR_PEID & "' " 
	strqry = strqry & " ,@OR_WRITE_NM         = '" & GLOBAL_VAR_PENM & "' "
	strqry = strqry & " ,@OR_ORDER_UNI_GRP_CD = '" & GLOBAL_VAR_UNIGRPCD & "' "
	strqry = strqry & " ,@OR_ORDER_PE         = '" & GLOBAL_VAR_PEID & "' "
	strqry = strqry & " ,@OR_DT               = '" & S_DATE &"' "
	strqry = strqry & " ,@OR_SEND_COM         = '" & GLOBAL_VAR_PENM & "' "		'--발송업체명(VARCHAR:100) *필수             
	strqry = strqry & " ,@OR_SEND_PE          = '" & user_name & "' "  			'--발송자명(VARCAR;50) *필수                 
	strqry = strqry & " ,@OR_SEND_TEL1        = '" & tel1 & "' "				'--발송자전화지역번호(VARCHAR:4)             
	strqry = strqry & " ,@OR_SEND_TEL2        = '" & tel2 & "' "				'--발송자전화국번(VARCHAR:4) *필수           
	strqry = strqry & " ,@OR_SEND_TEL3        = '" & tel3 & "' "				'--발송자전화번호(VARCHAR:4) *필수           
	strqry = strqry & " ,@OR_SEND_ZIP         = '" & zip & "' "					'--발송자우편번호1(VARCHAR:3) *필수          
	strqry = strqry & " ,@OR_SEND_ZIP2        = '' "							'--발송자우편번호2(VARCHAR:3) *필수          
	strqry = strqry & " ,@OR_SEND_ADDR        = '" & addr & "' "				'--발송자주소(VARCHAR:200) *필수             
	strqry = strqry & " ,@OR_KIND_TP = '01' "											                                            
	strqry = strqry & " ,@OR_KIND_TP2 = '06' "		
	strqry = strqry & " ,@OR_IN_COM_SEQ       = '"&GetsCUSTSEQ()&"' " 	'--고객전산코드(NUMERIC:18,0) *필수          
	strqry = strqry & " ,@OR_IN_PE = ' ' "												                                            
	strqry = strqry & " ,@OR_IN_PE_NM         = '" & user_name & "' "  			'--고객담당자명(VARCHAR:50) *필수            
	strqry = strqry & " ,@OR_IN_PE_TEL1       = '" & tel1 & "' "  				'--고객담당자전화번호1                       
	strqry = strqry & " ,@OR_IN_PE_TEL2       = '" & tel2 & "' "   				'--고객담당자전화번호2                       
	strqry = strqry & " ,@OR_IN_PE_TEL3       = '" & tel3 & "' "				'--고객담당자전화번호                        
	strqry = strqry & " ,@OR_IN_PE_HAND_TEL1  = '" & tel1 & "' "  				'--고객담당자핸드폰번호1                     
	strqry = strqry & " ,@OR_IN_PE_HAND_TEL2  = '" & tel2 & "' "  				'--고객담당자핸드폰번호2                     
	strqry = strqry & " ,@OR_IN_PE_HAND_TEL3  = '" & tel3 & "' "  				'--고객담당자핸드폰번호3                     
	strqry = strqry & " ,@OR_IN_PE_EMAIL      = '" & cust_email & "' "  		'--고객담당자이메일                          
	strqry = strqry & " ,@OR_IN_PE_ACNT_EMAIL = '" & cust_email & "' "			'--고객계산서수신이메일                      
	strqry = strqry & " ,@OR_IN_TEL1          = '" & tel1 & "' "			'--수령인전화지역번호(VARCHAR:4)                                               
	strqry = strqry & " ,@OR_IN_TEL2          = '" & tel2 & "' "			'--수령인전화국번(VARCHAR:4) *필수         
	strqry = strqry & " ,@OR_IN_TEL3          = '" & tel3 & "' "			'--수령인전화번호(VARCHAR:4) *필수        
	strqry = strqry & " ,@OR_IN_PE2           = '" & user_name & "' " 			'--수령인명(VARCHAR:50) *필수             
	strqry = strqry & " ,@OR_IN_ZIP           = '" & Left(zip,3) & "' " 				'--수령인우편번호1(VARCHAR:3) *필수       
	strqry = strqry & " ,@OR_IN_ZIP2          = '" & Mid(zip,4,3) & "' " 				'--수령인우편번호2(VARCHAR:3) *필수       
	strqry = strqry & " ,@OR_IN_ADDR          = '" & addr & "' "				'--수령인주소(VARCHAR:200) *필수          
	strqry = strqry & " ,@OR_IN_MEMO          = '" & delivery_memo & "' "		'--배송메모(VARCHAR:200)                  
	strqry = strqry & " ,@OR_IN_TP            = '01' "							'--배송방법코드(VARCHAR:2) *필수          															       
	strqry = strqry & " ,@OR_WANT_IN_DT       = '" & S_DATE  & "' "				'--희망배송일자(VARCHAR:8) *필수
	strqry = strqry & " ,@OR_IN_END_DT        = ' ' "
	strqry = strqry & " ,@OR_BOX_MEMO         = '' "				'--포장메모(VARCHAR:500)
	strqry = strqry & " ,@OR_ELSE_MEMO		  = '" &or_else_memo&"' "				'--기타메모(VARCHAR:500)
	strqry = strqry & " ,@OR_STATE            = '00' "
	strqry = strqry & " ,@DATA_IN_TP          = 'ONLINE' "
	strqry = strqry & " ,@CNCL_ORDER_GD_SEQ   = 0 "
	strqry = strqry & " ,@CNCL_ORDER_OR_NUM   = '0' "
	strqry = strqry & " ,@ORDER_TP            = '1' "
	strqry = strqry & " ,@IN_PAY_DT           = '" & replace(DateAdd("D", 7, Formatdatetime(Year(Now) & "-" & Month(Now) & "-" & Day(Now))),"-","")  &"' "
	strqry = strqry & " ,@WORK_NM             = '" & GLOBAL_VAR_PEID & "' "
	strqry = strqry & " ,@UNIGRPCD            = '" & GLOBAL_VAR_UNIGRPCD & "' "
	strqry = strqry & " ,@ORDERGD			  = '" & O_Info & "' "				'--주문상품취합리스트 (위취합방법을 참고)
	strqry = strqry & " ,@AGTID               = '" & GLOBAL_VAR_PEID & "' "
	strqry = strqry & " ,@INAMT               = '" & INAMT_result & "' "
	strqry = strqry & " ,@INOUT               = 'N' "
	strqry = strqry & " ,@ACNTTP              = '" & sndPaymethod & "' "    
	strqry = strqry & " ,@ACNTNM              = '" & point_acntnm & "' "      
	strqry = strqry & " ,@ACNTNO              = '" & point_acntnum & "' "       
	strqry = strqry & " ,@TP= 'ONE'			   " 
	strqry = strqry & " ,@ORNUM				  =	'"&or_num&"' " 	
	strqry = strqry & " ,@POINT				  =	'"&POINT&"' " 		
	'esponse.WRITE strqry

	'먼저 데이터 로그를 남겨놓습니다.
	order_log_qry = "insert Into itemcenter_website_order_log "
	order_log_qry = order_log_qry & " ( "
	order_log_qry = order_log_qry & " 	 mobile_gb "
	order_log_qry = order_log_qry & " 	,order_log "
	order_log_qry = order_log_qry & " 	,reg_date "
	order_log_qry = order_log_qry & " 	,order_nm "
	order_log_qry = order_log_qry & " ) "
	order_log_qry = order_log_qry & " values "
	order_log_qry = order_log_qry & " ( "
	order_log_qry = order_log_qry & " 	'"&mobile_gb&"', "
	order_log_qry = order_log_qry & " 	'"&Replace(strqry,"'","''''")&"', "
	order_log_qry = order_log_qry & " 	getdate(), "
	order_log_qry = order_log_qry & " 	'"&user_name&"' "
	order_log_qry = order_log_qry & " ) "

	
		
	Dbcon.Execute(order_log_qry)
		Set order_rs = Dbcon.Execute(strqry)
	DBClose()
 
	Set Smtp = Server.CreateObject("TABSUpload4.Smtp")

	strseemail = Upload.Form("seemail")
	 strsenm = Upload.Form("senm")	 	 
	 strsecomnm = Upload.Form("secomnm")
	 strsecompe = Upload.Form("secompe")
	 strsecomemail = Upload.Form("secomemail")
	 strsecomtel = Upload.Form("secomtel")
	 strsecomfax = Upload.Form("secomfax")
	 strsecomaddr = Upload.Form("secomaddr")
	 strsecomnum = Upload.Form("secomnum")
	 
	 strreemail = Upload.Form("reemail")
	 strrenm = Upload.Form("renm")

	 strurl = Upload.Form("bodyurl")	 
	
	Set Smtp = Server.CreateObject("TABSUpload4.Smtp")


	strContent = "http://b2b.sampsungbizmall.com 에서 전송한 " + user_name + "님의 주문메일입니다.<BR><BR>"
	strContent = strContent & "안녕하세요 b2b.sampsungbizmall.com 입니다. 주문을 해주셔서 감사드립니다. <BR><BR>"
	strContent = strContent & "주문번호는 : " & order_rs(0)  & " 입니다.<BR><BR>" 
	strContent = strContent & "사이트에 접속 하시어 주문내역을 확인해주시기 바랍니다. " 


	Smtp.ServerName = "49.247.7.60"
	Smtp.ServerPort = 6700
	Smtp.AddHeader "X-TABS-Campaign", "63E3F849-267F-4B6B-BC6E-496295D789B3"
	Smtp.FromAddress = "online@samsungbizmall.com"
	Smtp.AddToAddr cust_email, ""
	Smtp.Subject = user_name & "님께 b2b.sampsungbizmall.com 에서 발송한 주문내역입니다."	          
	Smtp.Encoding = "base64"
	Smtp.Charset = "euc-kr"
	Smtp.BodyHtml = strContent

	Set Result = Smtp.Send()
	If Result.Type = SmtpErrorSuccess Then
		'Response.Write "메일이 올바르게 전달되었습니다.<p>"
	Else
		'Response.Write "오류 종류:" & Result.Type & "<br>"
		'Response.Write "오류 코드:" & Result.Code & "<br>"
		'Response.Write "오류 설명:" & Result.Description
	End If

%>
<script language='javascript'>
	alert('주문이 완료 되었습니다');
	alert(parent.parent.location.href);
	alert(parent.location.href);
	alert(top.location.href);
	alert(top.parent.location.href);
	alert(parent.top..location.href);
	//parent.parent.location.href="order_finish.asp?or_num=<%=order_rs(0)%>"
</script>