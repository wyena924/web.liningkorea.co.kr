<!--#include virtual="/admin/Library/config.asp"-->
<%

	Dim cookie_id,cookie_pwd

	cookie_id	=  request.cookies("admin_login")("cookie_id") '쿠키에 저장된 이름
	cookie_pwd	=  request.cookies("admin_login")("cookie_pwd") '쿠키에 저장된 이름
	
%>
<HTML>
<HEAD>
<TITLE>SDMALL ADMIN LOGIN</TITLE>
<link rel="stylesheet" type="text/css" href="include/style.css" />

<META NAME="Pragma" CONTENT="no-cache">
<META http-equiv="Content-Type" content="text/html; charset=euc-kr">
<META NAME="IC" CONTENT="MAIN">
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=2, user-scalable=no">

</HEAD>

<style type="text/css">
/* nanumgothic */
@import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);
/*Noto Sans KR*/
@import url(http://fonts.googleapis.com/earlyaccess/notosanskr.css);
body,html,h1,img,div,form,label,input,button,a,p{margin:0;padding:0;line-height:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;letter-spacing:-1px;font-family:'Nanum Gothic',sans-serif;}
.login_wrap input::-webkit-input-placeholder,
.login_wrap textarea::-webkit-input-placeholder{color:#666;font-size:13px;}/* Chrome/Opera/Safari */
.login_wrap input::-moz-placeholder,
.login_wrap textarea::-moz-placeholder{color:#666;font-size:13px;}/* Firefox 19+ */
.login_wrap input:-ms-input-placeholder,
.login_wrap textarea:-ms-input-placeholder{color:#666;font-size:13px;}/* IE 10+ */
.login_wrap input:-moz-placeholder,
.login_wrap textarea:-moz-placeholder{color:#666;font-size:13px;}/* Firefox 18- */

.login_wrap{max-width:640px;margin:0 auto 50px;color:#333; }
.login_wrap .logo{background:#3990fd;color:#fff; font-size:10px; font-size:13px; padding:20px;}
.login_wrap h1{width:600px;}
.login_wrap h1 img{width:100%;}
.login_wrap .login_cont{padding:20px;}
.login_wrap .login_form{width:100%;margin:50px auto 0;background:url('../images/logo_login.png') no-repeat center 22% / 80% auto;}
.login_wrap .login_form .login_box{float:left;width:75%;}
.login_wrap .login_form .login_box input{border:1px solid #e7e7e7;background:#eee;width:100%;height:50px;padding-left:5%;margin-bottom:1%;}
.login_wrap .login_form .login_box .hide{width:0;height:0;overflow:hidden;visibility:hidden;line-height:0;font-size:0;position:absolute;left:-999px;}
.login_wrap .login_form .login_btn{float:right;width:24%;}
.login_wrap .login_form .login_btn .button{color:#fff;border:none;background:#3990fd;;width:100%;height:104px;font-size:15px;}
.login_wrap .login_form .save_id{clear:both;position:relative;padding:20px 0 30px 7px;border-bottom:1px solid #333;}
.login_wrap .login_form .save_id input[type="checkbox"]{position:absolute;width:1px;height:1px;padding:0;margin:-1px;overflow:hidden;clip:rect(0,0,0,0);border:0;}
.login_wrap .login_form .save_id input[type="checkbox"] + label{display:inline-block;position:relative;cursor:pointer;-webkit-user-select:none;-moz-user-select:0;-ms-user-select:none;}
.login_wrap .login_form .save_id input[type="checkbox"] + label:before{/* 가짜체크박스 */content:'';display:inline-block;width:21px;height:21px;line-height:21px;margin:-2px 8px 0 0;text-align:center;vertical-align:middle;background:#fafafa;border:1px solid #cacece;border-radius:3px;}
.login_wrap .login_form .save_id input[type="checkbox"]:checked + label:before{/* 체크박스를 체크했을때 */content:'\2714';/* 체크표시 유니코드 사용 */color:#99a1a7;background:#e9ecee;border-color:#adb8c0;}
.login_wrap .login_form .save_id label{font-size:13px;color:#666;}
.login_wrap .login_form .find_wrap{margin-top:30px;padding-left:7px;font-size:13px;}
.login_wrap .login_form .find_wrap p{color:#666;margin-bottom:10px;}
.login_wrap .login_form .find_wrap a{color:#333;text-decoration:none;}
.login_wrap .cs_wrap{margin-top:30%;}
.login_wrap .cs_wrap p{font-size:12px;margin-bottom:5px;color:#555;}
.login_wrap .cs_wrap p:first-child{font-size:20px;margin-bottom:20px;}

/*********** media *************/

/*360(브라우저 가로폭)*/
@media screen and (max-width: 360px){
.login_wrap .login_form .login_box input{height:40px;}
.login_wrap .login_form .login_btn .button{height:82px;}
.login_wrap .login_form .save_id{padding:15px 0 15px 7px;}
.login_wrap .login_form .save_id input[type="checkbox"] + label:before{width:15px;height:15px;line-height:15px;}
.login_wrap .login_form .find_wrap{margin-top:15px;}
.login_wrap .login_form .save_id label{font-size:12px;}
.login_wrap .cs_wrap p:first-child{margin-bottom:20px;}
}
</style>
<script type="text/javascript" src="/admin/elibCom/js/jquery-1.6.2.min.js"></script>
<script language='javascript'>
	function login_ok()
		{
				
			var loginid		= document.getElementById("inlogin").value
			var loginpw		= document.getElementById("inloginpwd").value
			var uni_grp_cd	= document.getElementById("uni_grp_cd").value

			if (loginid =='')
			{
				alert('아이디를 입력해 주시기 바랍니다.');
				return;
			}
			if (loginpw =='')
			{
				alert('비밀번호를 입력해 주시기 바랍니다.');
				return;
			}
			if (uni_grp_cd =='')
			{
				alert('업체코드를 입력해 주시기 바랍니다.');
				return;
			}
			var strAjaxUrl="login_ok.asp?loginid="+loginid+"&uni_grp_cd="+uni_grp_cd+"&loginpw="+loginpw
			//alert(strAjaxUrl);
				var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								alert(retDATA);
								if(retDATA =="YES")
								{
									//alert('정상적으로 로그인 되었습니다.');
									location.href="index.asp"

								}
								else if(retDATA =="SK_YES")
								{
									//alert('정상적으로 로그인 되었습니다.');
									location.href="/admin/index.asp"

								}
								
								else
								{
									alert('로그인 정보가 잘못되었습니다. 다시 확인해 주시기 바랍니다.');
									//location.href="index.asp"

								}
								
								

							}
					 }
				 }); //close $.ajax(
		}

		function fnKeypress2()
			{
				
				if (event.keyCode == 13) {

					login_ok();
				}
			}	

			
			
	function notice_getCookie( name )

	{

	var nameOfCookie = name + "=";

	var x = 0;

	while ( x <= document.cookie.length )

	{

	var y = (x+nameOfCookie.length);

	if ( document.cookie.substring( x, y ) == nameOfCookie ) {

	if ( (endOfCookie=document.cookie.indexOf( ";", y )) == -1 )

	endOfCookie = document.cookie.length;

	return unescape( document.cookie.substring( y, endOfCookie ) );

	}

	x = document.cookie.indexOf( " ", x ) + 1;

	if ( x == 0 )

	break;

	}

	return "";

	}
</script>
<script language='javascript'>

	function chk_login(){
		var f = document.login_frm;
		var agt_id = document.getElementById("agt_id") 
		var agt_pwd	 = document.getElementById("agt_pwd") 
		if(agt_id.value==""){
			alert("아이디를 입력해 주세요.");
			agt_id.focus();
			return ;
		}
		if(agt_pwd.value==""){
			alert("비밀번호를 입력해 주세요.");
			agt_pwd.focus();
			return ;
		}
		f.action="login_ok.asp";
		//f.target = "iSQL";
		f.submit();
		
	}

	//체크박스 클릭시
	function chk_saveid() {
		var f = document.login_frm;
	  var expdate = new Date();
	  // 30일동안 아이디 저장 
	  if (f.saveid.checked)
		expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30); // 30일
	  else
		expdate.setTime(expdate.getTime() - 1); // 쿠키 삭제조건
	  setCookie("saveid", f.agt_id.value, expdate);
	}

	//폼 로드시 쿠키 아이디 가지고 오기
	function chk_getid() {
		var f = document.login_frm;
	  f.saveid.checked = ((f.agt_id.value = getCookie("saveid")) != "");
	}

	//쿠키 정보 가지고 오기
	function getCookie(Name) {
	  var search = Name + "="
	  if (document.cookie.length > 0) { // 쿠키가 설정되어 있다면
		offset = document.cookie.indexOf(search)
		if (offset != -1) { // 쿠키가 존재하면
		  offset += search.length
		  // set index of beginning of value
		  end = document.cookie.indexOf(";", offset)
		  // 쿠키 값의 마지막 위치 인덱스 번호 설정
		  if (end == -1)
			end = document.cookie.length
		  return unescape(document.cookie.substring(offset, end))
		}
	  }
	  return "";
	}
	//쿠키정보 저장
	function setCookie (name, value, expires) {
	  document.cookie = name + "=" + escape (value) +
		"; path=/; expires=" + expires.toGMTString();
	}
	
	function enter()
	{
		if (event.keyCode == 13) {
			chk_login();
		}
	}
</script>
<BODY topmargin="0" leftmargin="0" bottommargin="0">
<div class="login_wrap">
	<div class="logo"><h1>삼성삼판 어드민</h1></div>
	<div class="login_cont">
		<form method="post" name="login_frm" id="login_frm" action="">
			<div class="login_form">
				<div class="login_box">
					<label for="input_id" class="hide">id</label>
					<input type="text" id="agt_id" name="agt_id"  placeholder="아이디"/>
					<label for="input_pw" class="hide">password</label>
					<input type="password" id="agt_pwd" name="agt_pwd" class="" onkeypress="javascript:enter();" placeholder="비밀번호"/>
				</div>
				<div class="login_btn">
					<input type="button" class="button" onclick="javascript:chk_login();"   value="로그인">
				</div>
			</div>
		</form>
		<div class="cs_wrap">
			<p>고객센터 02-715-0199</p>
			<p>업무시간 오전 09:00 ~ 오후 18:00</p>
			<p>토요일.일요일.공휴일 휴무</p>
		</div>
	</div>
</div>
</BODY>
</HTML>
