<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page = request("page")

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	
kbn = "SUB"

goParam = "?tmp=null"

Dbopen()

sql = "USP_A_MAIN_BANNER_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size & ", @KBN='" & kbn & "'"
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If

DBclose()
%>
  <script>
    function addForm() {
      var num = 0;
      var arr = [];
      $("input[name=seq]").each(function() {
          var val = $(this).val();
          arr.push(val);
      });
      if(arr.length > 0) {
        num = Math.min.apply(null, arr);
        if(num > 0) num = 0;
      }
      var url = "sub_banner_form.asp?num=" + num;
      ajax.get(url, function(r) {
        //console.log(r);
        $("#banner_contents").append(r);
      });
    }
    function deleteForm(seq) {
      if(seq < 0) {
          confirmBox("삭제하시겠습니까?", function() {
            $("#addform"+seq).remove();
          });
      } else {
          // DB delete
          confirmBox("삭제하시겠습니까?", function() {
            hiddenFrame.location.href = "sub_banner_delete_proc.asp?seq=" + seq;
          });
      }
    }
    function checkSaveForm() {
      var frmchk = 0;

      $("input[name=seq]").each(function() {
        var val = $(this).val();

        if($("#view_no"+val).val() == "") {
          alertBoxFocus("출력순서를 입력해 주세요.","view_no"+val);
          frmchk++;
          return false;
        }

        if($("#banner_img"+val).val() == "") {
          alertBoxFocus("배너이미지를 등록해 주세요.","banner_img"+val);
          return false;
        } else {
          var filename = $("#banner_img_filename"+val).val().toLowerCase();
          var index = filename.lastIndexOf(".");
          var ext = filename.substring(index + 1);
          var chk = 0;
          var chkext = ["jpg","jpeg","gif","png"];
          var i = 0;
          for(i = 0; i < chkext.length; i++) {          
            if(chkext[i]== ext) {
              chk++;
              break;
            }
          }
          if(chk == 0) {
            alertBoxFocus("배너이미지는 JPG,GIF,PNG만 가능합니다.","banner_img"+val);
            frmchk++;
            return false;
          }
        }

        if($("#start_date"+val).val() == "" || $("#end_date"+val).val() == "") {
          alertBoxFocus("기간을 선택해 주세요.","start_date"+val);
          frmchk++;
          return false;
        } else {
          if($("#start_date"+val).val() > $("#end_date"+val).val()) {
            $("#start_date"+val).val("");
            $("#end_date"+val).val("");
            alertBoxFocus("날짜가 잘못 입력되었습니다.<br>확인 후 다시 입력해 주세요.","start_date"+val);
            frmchk++;
            return false;
          }
        }
      });

      if(frmchk > 0) {
        return false;
      } else {
        return true;
      }
    }
    function saveForm() {

      if($("input[name=seq]").length == 0) {
        alertBox("데이터가 없습니다.");
        return;
      }
      
      if( checkSaveForm() ) {
        confirmBox("저장하시겠습니까?",function() {
            var f = document.thisform;
            f.target = 'hiddenFrame';
            f.action = "sub_banner_write_proc.asp";
            f.submit();
        });      
      }
    }
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">메인관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">소배너 관리</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
              <h3 class="table-caption">소배너 목록</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="90">
                  <col width="50">
                  <col width="195">
                  <col width="300">
                  <col width="370">
                  <col width="100">
                  <col width="70">
                  <col width="200">
                  <col width="80">
                  <col width="70">
                </colgroup>
                <tbody>
                  <tr>
                    <th>순번</th>
                    <th>상태</th>
                    <th>제목</th>
                    <th>이미지등록</th>
                    <th>기간</th>
                    <th>전시</th>
                    <th>새창</th>
                    <th>연결URL</th>
                    <th>클릭수</th>
                    <th>삭제</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="sub_banner_write_proc.asp" enctype="multipart/form-data">
                <table class="table table-striped table-content">
                  <colgroup>
                    <col width="90">
                    <col width="50">
                    <col width="195">
                    <col width="300">
                    <col width="370">
                    <col width="100">
                    <col width="70">
                    <col width="200">
                    <col width="80">
                    <col width="70">
                  </colgroup>
                  <tbody id="banner_contents">
<%
for i = (page-1) * page_size To numList
  seq = arrList(2,i)
  view_no = arrList(3,i)
  banner_img = arrList(4,i)
  start_date = arrList(5,i)
  end_date = arrList(6,i)
  isview = arrList(7,i)
  ispop = arrList(8,i)
  url = arrList(9,i)
  hit = arrList(10,i)
  title = arrList(11,i)

  num = seq

  arr = split(banner_img,"|")
  org_filename = arr(0)
  save_filename = arr(1)

  if CDate(start_date) > date then
    status = "대기"
  elseif CDate(start_date) <= date and CDate(end_date) >= date then
    status = "진행"
  elseif CDate(ed_date) < date then
    status = "종료"
  end if
%>
                    <tr id="addform<%=num%>">
                      <td>
                        <input type="hidden" name="seq" value="<%= num %>">
                        <input type="text" class="form-control" placeholder="" ID="view_no<%= num %>" NAME="view_no<%= num %>" value="<%= view_no %>">
                      </td>
                      <td><%= status %></td>
                      <td>
                        <input type="text" class="form-control" placeholder="" ID="title<%= num %>" NAME="title<%= num %>" value="<%= title %>">
                      </td>
                      <td>
                        <input type="text" name="banner_img_filename<%= num %>" id="banner_img_filename<%= num %>" value="<%= org_filename %>" class="form-control" placeholder="파일을 선택하세요" style="width:73%;display:inline-block;background:#ffffff;" readonly>
                        <label type="button" for="banner_img<%= num %>" class="btn btn-light active" style="width:70px;">Browse</label>
                        <input type="file" name="banner_img<%= num %>" id="banner_img<%= num %>" style="position: absolute; width: 1px;  height: 1px;  padding: 0;  margin: -1px;  overflow: hidden;  clip:rect(0,0,0,0);  border: 0;">

                        <script>
                            var fileTarget<%= replace(num,"-","_") %> = $('#banner_img<%= num %>');

                            fileTarget<%= replace(num,"-","_") %>.on('change', function(){  // 값이 변경되면
                                var filename = "";

                                if(window.FileReader){  // modern browser
                                    filename = $(this)[0].files[0].name;
                                } else {  // old IE
                                    filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
                                }

                                // 추출한 파일명 삽입
                                $('#banner_img_filename<%= num %>').val(filename);
                            });
                        </script>
                      </td>
                      <td>
                        <div class="col-md-5">
                          <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=date%>" name="start_date<%= num %>" id="start_date<%= num %>" value="<%= start_date %>">
                        </div>
                        <div class="col-md-1">
                        ~
                        </div>
                        <div class="col-md-5">
                          <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="2099-12-31" name="end_date<%= num %>" id="end_date<%= num %>" value="<%= end_date %>">
                        </div>
                      </td>
                      <td>
                        <select name="isview<%= num %>" id="isview<%= num %>" style="height:25px;">
                          <option value="Y" <% if isview = "Y" then response.write " selected " end if %> >Y</option>
                          <option value="N" <% if isview = "N" then response.write " selected " end if %> >N</option>
                        </select>
                      </td>
                      <td>
                        <label class="chk-box">
                        <input type="checkbox" name="ispop<%= num %>" id="ispop<%= num %>" value="Y" <% if ispop = "Y" then response.write " checked " end if %> >
                        <span></span>
                      </div>
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="" ID="url<%= num %>" NAME="url<%= num %>" value="<%= url %>">
                      </td>
                      <td><%= hit %></td>
                      <td>
                        <a href="#" class="btn btn-bloc btn-danger" style="width:50px;" onclick="deleteForm(<%= num %>); return false;">삭제</a>
                      </td>
                    </tr>
<%
next
%>
                  </tbody>
                </table>
                <!-- E: table-content -->
                </form>
              </div>
              <!-- E: scroll-body -->			          
                <a href="#" class="btn btn-primary" onclick="saveForm(); return false;">저장</a>
                <a href="#" class="btn btn-primary" onclick="addForm(); return false;">추가</a>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->



    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>