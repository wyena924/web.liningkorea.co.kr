<!--#include virtual="/pmaa/admin/Library/unicode_lhs.asp"-->
<%
num = request("num")

num = num - 1
%>
                    <tr id="addform<%=num%>">
                      <td>
                        <input type="hidden" name="seq" value="<%= num %>">
                        <input type="text" class="form-control" placeholder="" ID="view_no<%= num %>" NAME="view_no<%= num %>">
                      </td>
                      <td>진행</td>
                      <td>
                        <input type="text" class="form-control" placeholder="" ID="title<%= num %>" NAME="title<%= num %>" value="">
                      </td>
                      <td>
                        <input type="text" name="banner_img_filename<%= num %>" id="banner_img_filename<%= num %>" class="form-control" placeholder="파일을 선택하세요" style="width:73%;display:inline-block;background:#ffffff;" readonly>
                        <label type="button" for="banner_img<%= num %>" class="btn btn-light active" style="width:70px;">Browse</label>
                        <input type="file" name="banner_img<%= num %>" id="banner_img<%= num %>" style="position: absolute; width: 1px;  height: 1px;  padding: 0;  margin: -1px;  overflow: hidden;  clip:rect(0,0,0,0);  border: 0;">

                        <script>
                            var fileTarget<%= replace(num,"-","_") %> = $('#banner_img<%= num %>');

                            fileTarget<%= replace(num,"-","_") %>.on('change', function(){  // 값이 변경되면
                                var filename = "";

                                if(window.FileReader){  // modern browser
                                    filename = $(this)[0].files[0].name;
                                } else {  // old IE
                                    filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
                                }

                                // 추출한 파일명 삽입
                                $('#banner_img_filename<%= num %>').val(filename);
                            });
                        </script>
                      
                      </td>
                      <td>
                        <div class="col-md-5">
                          <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=date%>" name="start_date<%= num %>" id="start_date<%= num %>" value="<%= date %>">
                        </div>
                        <div class="col-md-1">
                        ~
                        </div>
                        <div class="col-md-5">
                          <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="2050-12-31" name="end_date<%= num %>" id="end_date<%= num %>" value="2050-12-31">
                        </div>
                      </td>
                      <td>
                        <select name="isview<%= num %>" id="isview<%= num %>" style="height:25px;">
                          <option value="Y">Y</option>
                          <option value="N">N</option>
                        </select>

                      </td>
                      <td>
                        <label class="chk-box">
                        <input type="checkbox" name="ispop<%= num %>" id="ispop<%= num %>" value="Y">
                        <span></span>
                      </div>
                      </td>
                      <td>
                        <input type="text" class="form-control" placeholder="" ID="url<%= num %>" NAME="url<%= num %>">
                      </td>
                      <td>0</td>
                      <td>
                        <a href="#" class="btn btn-bloc btn-danger" style="width:50px;" onclick="deleteForm(<%= num %>); return false;">삭제</a>
                        <script>
                            $('#start_date<%=num%>').datepicker({
                                language : 'ko'
                            });
                            $('#start_date<%=num%>').data('datepicker');

                            $('#end_date<%=num%>').datepicker({
                                language : 'ko'
                            });
                            $('#end_date<%=num%>').data('datepicker');
                        </script>
                      </td>
                    </tr>
                    