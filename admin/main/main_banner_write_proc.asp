<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
Set Upload = Server.CreateObject("TABSUpload4.Upload")
Upload.CodePage = 65001
Upload.MaxBytesToAbort = 10 * 1024 * 1024

root_path = Server.MapPath("/")
save_path = root_path & "\upload\main_banner\"

Upload.Start "d:\kkmall\admin\upload\"

'response.write save_path&"<br>"
'response.write "seq1 = " & Upload.Form("seq")&"<br>"

DBopen()

for i = 1 to Upload.Form("seq").Count
	seq = Upload.Form("seq")(i)
	view_no = Upload.Form("view_no" & seq)
	title = Upload.Form("title" & seq)
	banner_img_filename = Upload.Form("banner_img_filename" & seq)
	'banner_img = Upload.Form("view_no" & seq)
	start_date = Upload.Form("start_date" & seq)
	end_date = Upload.Form("end_date" & seq)
	isview = Upload.Form("isview" & seq)
	ispop = Upload.Form("ispop" & seq)
	url = Upload.Form("url" & seq)
	kbn = "MAIN"

	if ispop = "" then ispop = "N"

	Set File = Upload.Form("banner_img" & seq)
	
	org_filename = File.FileName
	save_filename = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename,3)

	if org_filename = "" then
		tmp_save_filename= "|"
	else
		tmp_save_filename = org_filename & "|" & save_filename
	end if
	
	File.SaveAs save_path & save_filename , False

	'Response.Write File.FileName & "<br>"
	'Response.Write File.SaveName

	Set File = nothing

	if seq < 0 then
		'신규 등록
		mode = "ADD"
	else
		' 수정 등록
		mode = "EDIT"
	end if

	sql = "USP_A_MAIN_BANNER_ACCESS " & _
				" @MODE			= '" & mode & "'" & _
				",@SEQ			=  " & seq & _
				",@KBN			= '" & kbn & "'" & _
				",@VIEW_NO		=  " & view_no & _
				",@TITLE		= '" & title & "'" & _
				",@BANNER_IMG	= '" & tmp_save_filename & "'" & _
				",@START_DATE	= '" & start_date & "'" & _
				",@END_DATE		= '" & end_date & "'" & _
				",@ISVIEW		= '" & isview & "'" & _
				",@ISPOP		= '" & ispop & "'" & _
				",@URL			= '" & url & "'"
				'response.write sql&"<br>"
	Set rs = DBcon.Execute(sql)
	if rs(0) = 0 then
		%>
		<script>
			parent.alertBox("데이터 처리에 문제가 발생했습니다.");
		</script>
		<%
		response.end
	end if
next

DBclose()

%>

<script>
	parent.alertBox("정상 처리되었습니다.",function() {
		parent.location.reload();	
	});
</script>