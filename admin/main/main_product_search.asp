<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
  <script>
    function getCategory(mode) {
      $("#mode").val(mode);

      var cat1 = $("#cat1").val();
      var cat2 = $("#cat2").val();
      var cat3 = $("#cat3").val();
      var msg = "";

      if(mode == "CAT1") {
        msg = "대분류";
        $("#cat1 option").remove();
        $("#cat2 option").remove();
        $("#cat3 option").remove();

        $("#cat1").append("<option value=''>대분류를 선택해 주세요.</option>");
        $("#cat2").append("<option value=''>중분류를 선택해 주세요.</option>");
        $("#cat3").append("<option value=''>소분류를 선택해 주세요.</option>");
      } else if(mode == "CAT2") {
        msg = "중분류";
        $("#cat2 option").remove();
        $("#cat3 option").remove();

        $("#cat2").append("<option value=''>중분류를 선택해 주세요.</option>");
        $("#cat3").append("<option value=''>소분류를 선택해 주세요.</option>");
      } else if(mode == "CAT3") {
        msg = "소분류";
        $("#cat3 option").remove();

        $("#cat3").append("<option value=''>소분류를 선택해 주세요.</option>");
      }  

      var url = "main_product_search_category.asp";
      var params = $("#searchform").serialize();
      ajax.post(url, params, function(r) {
        if(mode == "CAT1") {
          $("#cat1").append(r);
        } else if(mode == "CAT2") {
          $("#cat2").append(r);
        } else if(mode == "CAT3") {
          $("#cat3").append(r);
        }

        $("#good_contents").html("");
      });
    }
    function search() {
      $("#mode").val("SEARCH_PRODUCT");

      var cat1 = $("#cat1").val();
      var cat2 = $("#cat2").val();
      var cat3 = $("#cat3").val();
      var good_name = $("#good_name").val();

      if(!cat1 && !good_name) {
        alertBoxFocus("대분류를 선택해 주세요.");
        return;
      }

      var url = "main_product_search_data.asp";
      var params = $("#searchform").serialize();
      ajax.post(url, params, function(r) {
        //console.log(r);
        $("#good_contents").html(r);
      });
    }
    function cancelData() {
      parent.searchProductClose();
    }
    function saveData() {
        var chk = 0;
        $("input[name='good_data']").each(function() {
          if($(this).is(":checked")) chk++;
        });
        if(chk == 0) {
          alertBox("상품을 선택후 확인 버튼을 눌러주세요.");
          return;
        }
        
        $("#num").val(parent.addForm());
        var url = "main_product_form.asp";
        var params = $("#searchform").serialize();
        ajax.post(url, params, function(r) {
          parent.addData(r);
        });
    }

    $(function() {
      getCategory('CAT1');
    });

  </script>
</head>
<body>
<!-- S: main-panel -->
    <div class="main-panel">
      
      <!-- S: page-content -->
      <div class="page-content" style="margin-top:0px;">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: comp-part -->
          <div class="comp-part">
            
            <!-- S: fixed-table -->
            <div class="fixed-table">
              <h3 class="table-caption">상품등록/검색</h3>
              <!-- S: fix-head-table -->
              <form name="searchform" id="searchform" method="post" action="">
              <input type="hidden" name="mode" id="mode" value="">
              <input type="hidden" name="num" id="num" value="">

              <table class="table fix-head-table">
                <colgroup>
                  <col width="100%">
                </colgroup>
                <tbody>
                  <tr>
                    <td>
                        <div class="col-sm-12">
                          <select class="form-control" name="cat1" id="cat1" onchange="getCategory('CAT2');">
                            <option value="">대분류를 선택해 주세요</option>
                          </select>

                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                        <div class="col-sm-12">
                          <select class="form-control" name="cat2" id="cat2" onchange="getCategory('CAT3');">
                            <option value="">중분류를 선택해 주세요</option>
                          </select>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                        <div class="col-sm-12">
                          <select class="form-control" name="cat3" id="cat3">
                            <option value="">소분류를 선택해 주세요</option>
                          </select>
                        </div>
                    </td>
                  </tr>
                  <tr>
                    <td>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" name="good_name" id="good_name" value="">
                        </div>
                        <div class="col-sm-4">
                          <button class="form-control btn-primary" onclick="search();return false;">검색</button>
                        </div>
                    </td>
                  </tr>
                </tbody>
              </table>
              
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body" style="max-height:500px;">
                <!-- S: table-content -->
                <table class="table table-striped table-content">
                  <colgroup>
                    <col width="100%">                    
                  </colgroup>
                  <tbody id="good_contents">
                    <!--
                    <tr>
                      <td>
                        <div class="col-sm-1">
                          <input type="checkbox">
                        </div>
                        <div class="col-sm-4">
                          <img src="http://www.itemcenter.co.kr/gdsimage/listimg/2014321/16_NEM2SF23_00.jpg" alt="" width="150">
                        </div>
                        <div class="col-sm-7">
                          <div>상품코드 : 1234 </div>
                          <div>상품명 : 노스페이스 점퍼 </div>
                          <div>실재고수량 : 134 </div>
                          <div>소비자가 : 1,234 </div>
                        </div>
                      </td>
                    </tr>
                    -->
                    
                  </tbody>
                </table>
                <!-- E: table-content -->
              </div>
              </form>
              <!-- E: scroll-body -->
              <div class="col-sm-6">
                <button class="form-control btn-danger" onclick="cancelData(); return false;">취소</button>
              </div>
              <div class="col-sm-6">
                <button class="form-control btn-success" onclick="saveData(); return false;">확인</button>
              </div>
              
            </div>
            <!-- E: fixed-table -->
          </div>
          <!-- E: comp-part -->
        </section>
      </div>
    </div>
</body>
</html>