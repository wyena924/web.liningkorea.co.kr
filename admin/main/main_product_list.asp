<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page = request("page")

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_A_MAIN_PRODUCT_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size 
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If

DBclose()
%>
  <script>
    function addForm() {
      var num = 0;
      var arr = [];
      $("input[name=seq]").each(function() {
          var val = $(this).val();
          arr.push(val);
      });
      if(arr.length > 0) {
        num = Math.min.apply(null, arr);
        if(num > 0) num = 0;
      }
      return num;
    }
    function addData(data) {
      $("#product_contents").append(data);
      $.colorbox.close();
    }
    function deleteForm(seq) {
      if(seq < 0) {
          confirmBox("삭제하시겠습니까?", function() {
            $("#addform"+seq).remove();
          });
      } else {
          // DB delete
          confirmBox("삭제하시겠습니까?", function() {
            hiddenFrame.location.href = "main_product_delete_proc.asp?seq=" + seq;
          });
      }
    }
    function checkSaveForm() {
      var frmchk = 0;

      $("input[name=seq]").each(function() {
        var val = $(this).val();

        if($("#view_no"+val).val() == "") {
          alertBoxFocus("출력순서를 입력해 주세요.","view_no"+val);
          frmchk++;
          return false;
        }

      });

      if(frmchk > 0) {
        return false;
      } else {
        return true;
      }
    }
    function saveForm() {

      if($("input[name=seq]").length == 0) {
        alertBox("데이터가 없습니다.");
        return;
      }
      
      if( checkSaveForm() ) {
        confirmBox("저장하시겠습니까?",function() {
            var f = document.thisform;
            f.target = 'hiddenFrame';
            f.action = "main_product_write_proc.asp";
            f.submit();
        });      
      }
    }
    function searchProduct() {
      $.colorbox({
        width: "850",
        height: "100%",
        overlayClose: true,
        escKey: false,
        arrowKey: false,
        inline: false,
        iframe: true,
        open: true,
        closeButton: false,
        href: "main_product_search.asp",
        onOpen: function() {			
          
        },
        onClosed: function() {
          
        }
      });
    }
    function searchProductClose() {
      $.colorbox.close();
    }
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">메인관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">메인상품 리스트 관리</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
              <h3 class="table-caption">메인상품 목록</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="90">
                  <col width="150">
                  <col width="210">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="110">
                  <col width="110">
                  <col width="100">
                  <col width="70">
                </colgroup>
                <tbody>
                  <tr>
                    <th>순번</th>
                    <th>상품이미지</th>
                    <th>상품명</th>
                    <th>상품코드</th>
                    <th>상품업체코드</th>
                    <th>판매가</th>
                    <th>소비자가</th>
                    <th>실재고수량</th>
                    <th>판매수량</th>
                    <th>전시</th>
                    <th>새창</th>
                    <th>클릭수</th>
                    <th>삭제</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content">
                  <colgroup>
                    <col width="90">
                  <col width="150">
                  <col width="220">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="110">
                  <col width="110">
                  <col width="100">
                  <col width="70">
                  </colgroup>
                  <tbody id="product_contents">
<%
for i = (page-1) * page_size To numList
  seq = arrList(2,i)
  view_no = arrList(3,i)
  isview = arrList(4,i)
  ispop = arrList(5,i)
  hit = arrList(6,i)
  good_seq = arrList(7,i)
  good_name = arrList(8,i)
  com_seq = arrList(9,i)
  good_img = arrList(10,i)
  sale_price = arrList(11,i)
  good_price = arrList(12,i)
  good_vol = arrList(13,i)
  sale_vol = arrList(14,i)
    
  num = seq

%>
                    <tr id="addform<%=num%>">
                      <td>
                        <input type="hidden" name="seq" value="<%= num %>">
                        <input type="hidden" name="good_seq<%= num %>" value="<%= good_seq %>">
                        <input type="text" class="form-control" placeholder="" ID="view_no<%= num %>" NAME="view_no<%= num %>" value="<%= view_no %>">
                      </td>
                      <td><img src="http://www.itemcenter.co.kr/<%= good_img %>" alt="" width="80"></td>
                      <td><%= good_name %></td>
                      <td><%= good_seq %></td>
                      <td><%= com_seq %></td>
                      <td><%= FormatNumber(good_price,0) %></td>
                      <td><%= FormatNumber(sale_price,0) %></td>
                      <td><%= good_vol %></td>
                      <td><%= sale_vol %></td>                      
                      <td>
                        <select name="isview<%= num %>" id="isview<%= num %>" style="height:25px;">
                          <option value="Y" <% if isview = "Y" then response.write " selected " end if %> >Y</option>
                          <option value="N" <% if isview = "N" then response.write " selected " end if %> >N</option>
                        </select>
                      </td>
                      <td>
                        <label class="chk-box">
                        <input type="checkbox" name="ispop<%= num %>" id="ispop<%= num %>" value="Y" <% if ispop = "Y" then response.write " checked " end if %> >
                        <span></span>
                      </td>
                      <td><%= hit %></td>
                      <td>
                        <a href="#" class="btn btn-bloc btn-danger" style="width:50px;" onclick="deleteForm(<%= num %>); return false;">삭제</a>
                      </td>
                    </tr>
<%
next
%>
                  </tbody>
                </table>
                <!-- E: table-content -->
                </form>
              </div>
              <!-- E: scroll-body -->			          
                <a href="#" class="btn btn-primary" onclick="saveForm(); return false;">저장</a>
                <a href="#" class="btn btn-primary" onclick="searchProduct(); return false;">상품검색/등록</a>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>