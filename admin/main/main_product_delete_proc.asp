<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
seq = request("seq")

if seq = "" then
	%>
	<script>
		parent.alertBox("출력번호가 존재하지 않습니다.");
	</script>
	<%
	response.end
end if

mode = "DEL"

DBopen()

sql = "USP_A_MAIN_PRODUCT_ACCESS " & _
			" @MODE			= '" & mode & "'" & _
			",@SEQ			=  " & seq 
Set rs = DBcon.Execute(sql)
if rs(0) = 0 then
	%>
	<script>
		parent.alertBox("데이터 처리에 문제가 발생했습니다.");
	</script>
	<%
else
	%>
	<script>
		parent.alertBox("정상 처리되었습니다.",function() {
			parent.location.reload();	
		});
	</script>
	<%
end if

DBclose()

%>