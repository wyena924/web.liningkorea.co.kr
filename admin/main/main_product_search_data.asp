<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
mode = request("mode")
cat1 = request("cat1")
cat2 = request("cat2")
cat3 = request("cat3")
good_name = request("good_name")

wh = ""

if cat1 <> "" then
    wh = wh & " ,@CAT1='" & cat1 & "'"
end if

if cat2 <> "" then
    wh = wh & " ,@CAT2='" & cat2 & "'"
end if

if cat3 <> "" then
    wh = wh & " ,@CAT3='" & cat3 & "'"
end if

if good_name <> "" then
    wh = wh & " ,@GOOD_NAME='" & good_name & "'"
end if

data = ""

DBopen()
sql = "USP_A_MAIN_PRODUCT_VIEW @mode = '" & mode & "' " & wh
Set rs = DBcon.Execute(sql)
do until rs.eof
    good_seq = rs(0)
    good_name = rs(1)
	com_seq = rs(2)
	good_img = rs(3)
	sale_price = rs(4)
	good_price = rs(5)
	good_vol = rs(6)
	sale_vol = rs(7)

	good_data = good_seq & "|" & good_name & "|" & com_seq & "|" & good_img & "|" & good_price & "|" & sale_price & "|" & good_vol & "|" & sale_vol
    %>
		<tr>
			<td>
			<div class="col-sm-1">
				<input type="checkbox" name="good_data" value="<%= good_data %>">
			</div>
			<div class="col-sm-4">
				<img src="http://www.itemcenter.co.kr/<%= good_img %>" alt="" width="150">
			</div>
			<div class="col-sm-7">
				<div>상품코드 : <%= good_seq %> </div>
				<div>상품명 : <%= good_name %> </div>
				<div>실재고수량 : <%= FormatNumber(good_vol,0) %> </div>
				<div>소비자가 : <%= FormatNumber(good_price,0) %> </div>
			</div>
			</td>
		</tr>
	<%

    rs.movenext
loop
DBclose()

%>