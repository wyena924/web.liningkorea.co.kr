<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>위드라인 홈페이지 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page		= request("page")
start_date	= request("start_date")
end_date	= request("end_date")
RE_YN		= request("RE_YN")
QNA_TYPE	= request("QNA_TYPE")


If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_MALL_ADMIN_QNA_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size  &", @start_date='" &start_date&"', @end_date='" & end_date &"', @RE_YN='" &RE_YN &"' ,@QNA_TYPE='"&QNA_TYPE&"',@online_cd='"&GLOBAL_VAR_ONLINECD&"'"
'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If



%>
  <script>
	function alliance_view(seq)
	{
		document.getElementById("content_div_"+ seq).style.display ='block'
	}

	function open_popup(or_seq)
	{
		window.open("http://www.itemcenter.co.kr/WEB/ic_order_cust.asp?seq="+or_seq+"&unigrpcd=2001&tp=01&emailkey=&gdscusttp=notview&DEL_YN=N", "window", "scrollbars=YES,width=800,height=800");
	}

	
	
	function cncl_change(ornum,seq,py_in_acnt) {
		document.getElementById("trno").value = py_in_acnt;

		if(confirm("주문번호"+ornum+" 을(를) 취소하시겠습니까?")==false)
			return 1;
		//parent.fBottom.popupOpen("","","처리중입니다!");

		var strAjaxUrl="/admin/ajax/order_cncl_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: {
					ornum:ornum,
					seq:seq ,	
			},

			success: function(retDATA) {
				if(retDATA){
					//var strcut = retDATA.split("|>");
					if (retDATA == "TRUE") {
						alert("취소가 완료되었습니다! 재조회 후 확인하십시오.");
						document.bform.target="cancle_iframe"
						document.bform.action="http://ksnet.itemcenter.co.kr/KSPayCancelPost.asp"
						document.bform.submit();
						location.reload();
					}

					if (retDATA == "CNCL") {

						alert("이미 취소 상태입니다!");
						parent.fBottom.popupClose("","","");
					}

					if (retDATA != "TRUE" && retDATA != "CNCL") {
						parent.fBottom.popupClose("","","");
						alert ("취소중에 오류가 발생하였습니다!");
					}
				}
			}, error: function(xhr, status, error){
				
				parent.fBottom.popupClose("","","");
				alert ("취소중 에러발생 - 시스템관리자에게 문의하십시오!"+' ['+error+']');
			}
		});
	}
	
	function search()
	{
		document.search_form.submit();
	}

	function search_keypress()
	{
		if (window.event.keyCode == 13) {
 
             search()
        }
	}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">게시판관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">상품QNA리스트</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
	  <iframe id="cancle_iframe" name="cancle_iframe" width=0 height=0 style="display:none;"></iframe>
      <!-- E: page-title -->
	  <form method="post" action="" name="bform" id="bform">
			<input type="hidden" id="storeid" name="storeid" value="2001105535">
			<input type="hidden" id="storepasswd" name="storepasswd" value="">
			<input type="hidden" id="authty" name="authty" value="1010">	
			<input type="hidden" id="trno" name="trno" value="">
	  </form>
	  <form name="search_form" method="post">
      <!-- S: page-search -->
	  <table class="page-search">
		<tr>
			<td><div class="Palal">등록일</div></td>
			<td class="date_td">
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=start_date%>" name="start_date" id="start_date" value="<%= start_date %>" readonly>
				<div>~</div>
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=end_date%>" name="end_date" id="end_date" value="<%= end_date %>" readonly>
			</td>
			<td><div class="Palal">답변여부</div></td>
			<td>
				<select class="lis01" name="re_yn" id="re_yn" >
					<option VALUE="">전체</option>
					<option VALUE="Y" VALUE="Y" <% If re_yn = "Y" Then %> SELECTED <% End If %>>Y : 답변있음</option>
					<option VALUE="N" VALUE="Y" <% If re_yn = "N" Then %> SELECTED <% End If %>>N : 답변없음</option>
				</select>

			</td>
			<td>
				<div class="Palal">문의유형</div>
			</td>
			<td>
				<select class="lis01" name="qna_type" id="qna_type">
					<option value="">전체</option>
					<option value="product"  <% if qna_type = "product" then %> selected <% end if %> >상품</option>
					<option value="order"  <% if qna_type = "order" then %> selected <% end if %> >배송</option>
					<option value="cencle"  <% if qna_type = "cencle" then %> selected <% end if %> >반품/취소</option>					
					<option value="etc"  <% if qna_type = "etc" then %> selected <% end if %> >기타</option>
				</select>
			</td>
			
		</tr>
	  </table>
	  <input class="search_button"  type="button" value="Search" onclick="javascript:search();">
	  </form>
	  <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
			  <DIV class="total_div" >TOTAL : <%=total_cnt%></div>
              <h3 class="table-caption">상품QNA리스트</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="80">
				  <col width="120">
				  <col width="200">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">   
				  <col width="120"> 
				  <col width="120"> 
                </colgroup>
                <tbody>
                  <tr>
                    <th width="80">No</th>
					<th width="120" style="text-align:center;">상품명</th>
					<th width="200" style="text-align:center;">내용</th>
                    <th width="120" style="text-align:center;">답변</th>
                    <th width="120" style="text-align:center;">문의유형</th>
                    <th width="120" style="text-align:center;">등록자</th>
                    <th width="120" style="text-align:center;">답변자</th>
                    <th width="120" style="text-align:center;">문의일자</th>
					<th width="120" style="text-align:center;">답변일자</th>
					<th width="120" style="text-align:center;">관리</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="float:center;">
                  <colgroup>
                    <col width="80">
					<col width="120">
					<col width="200">
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">        
					<col width="120">         
					<col width="120">         
                  </colgroup>
                  <tbody>
					<%
						for i = (page-1) * page_size To numList
						  seq			= arrList(2,i)
						  con_text		= arrList(3,i)
						  con_re_text	= arrList(4,i)
						  reg_date		= arrList(5,i)
						  reg_re_date		= arrList(6,i)
						  work_id		= arrList(7,i)
						  re_work_nm	= arrList(8,i)
						  qna_type		= arrList(9,i)
						  GD_NM			= arrList(11,i)
						  GD_SIZE		= arrList(12,i)
						  GD_COLOR		= arrList(13,i)
					%>
					 <tr>
						<th width="80"><%=SEQ%></th>
						<th width="120" style="text-align:center;"><%=GD_NM%></th>
						<th width="200" style="text-align:center;">
							<%=con_text%>
						</th>
						<th width="120" style="text-align:center;">
							<%=con_re_text %>
						</th>
						<th width="120" style="text-align:center;"><%=qna_type %></th>
						<th width="120" style="text-align:center;"><%=work_id%></th>
						<th width="120" style="text-align:center;">
							<%=re_work_nm %>
						</th>
						<th width="120" style="text-align:center;">
							<%=reg_date %>
						</th>
						<th width="120" style="text-align:center;"><%=reg_re_date%></th>
						<th width="120" style="text-align:center;">
							<input type="button" value="답변달기" onclick="javascript:location.href='product_qna_update.asp?seq=<%=seq%>';">		
						</th>
					  </tr>
					<%

						h_number = h_number - 1

						next
					%>	
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>