<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>위드라인 홈페이지 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page		= request("page")
start_date	= request("start_date")
end_date	= request("end_date")
or_num		= request("or_num")
or_nm		= request("or_nm")
or_tel		= request("or_tel")
or_state	= request("or_state")	


If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_MALL_ADMIN_ORDER_FILE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size  &", @start_date='" &start_date&"', @end_date='" & end_date &"',@OR_WRITE_ID='"&GLOBAL_OR_WRITE_ID&"' , @or_num='" &or_num &"' ,@order_nm='"&or_nm&"' "
'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If



%>
  <script>
	function alliance_view(seq)
	{
		document.getElementById("content_div_"+ seq).style.display ='block'
	}

	function open_popup(or_seq)
	{
		window.open("http://www.itemcenter.co.kr/WEB/ic_order_cust.asp?seq="+or_seq+"&unigrpcd=2001&tp=01&emailkey=&gdscusttp=notview&DEL_YN=N", "window", "scrollbars=YES,width=800,height=800");
	}

	
	
	function cncl_change(ornum,seq,py_in_acnt) {
		document.getElementById("trno").value = py_in_acnt;

		if(confirm("주문번호"+ornum+" 을(를) 취소하시겠습니까?")==false)
			return 1;
		//parent.fBottom.popupOpen("","","처리중입니다!");

		var strAjaxUrl="/admin/ajax/order_cncl_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: {
					ornum:ornum,
					seq:seq ,	
			},

			success: function(retDATA) {
				if(retDATA){
					//var strcut = retDATA.split("|>");
					if (retDATA == "TRUE") {
						alert("취소가 완료되었습니다! 재조회 후 확인하십시오.");
						document.bform.target="cancle_iframe"
						document.bform.action="http://ksnet.itemcenter.co.kr/KSPayCancelPost.asp"
						document.bform.submit();
						location.reload();
					}

					if (retDATA == "CNCL") {

						alert("이미 취소 상태입니다!");
						parent.fBottom.popupClose("","","");
					}

					if (retDATA != "TRUE" && retDATA != "CNCL") {
						parent.fBottom.popupClose("","","");
						alert ("취소중에 오류가 발생하였습니다!");
					}
				}
			}, error: function(xhr, status, error){
				
				parent.fBottom.popupClose("","","");
				alert ("취소중 에러발생 - 시스템관리자에게 문의하십시오!"+' ['+error+']');
			}
		});
	}
	
	function search()
	{
		document.search_form.submit();
	}

	function search_keypress()
	{
		if (window.event.keyCode == 13) {
 
             search()
        }
	}

	function excel_down()
	{
		document.bform.action="excel_order_file_list.asp"
		document.bform.submit();
	}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">주문관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">인증자료리스트</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
	  <iframe id="cancle_iframe" name="cancle_iframe" width=0 height=0 style="display:none;"></iframe>
      <!-- E: page-title -->
	  <form method="post" action="" name="bform" id="bform">
			<input type="hidden" id="storeid" name="storeid" value="2001105535">
			<input type="hidden" id="storepasswd" name="storepasswd" value="">
			<input type="hidden" id="authty" name="authty" value="1010">	
			<input type="hidden" id="trno" name="trno" value="">
	  </form>
	  <form name="search_form" method="post">
      <!-- S: page-search -->
	  <table class="page-search">
		<tr>
			<td><div class="Palal">주문일</div></td>
			<td class="date_td">
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=start_date%>" name="start_date" id="start_date" value="<%= start_date %>" readonly>
				<div>~</div>
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=end_date%>" name="end_date" id="end_date" value="<%= end_date %>" readonly>
			</td>
			<td><div class="Palal">주문번호</div></td>
			<td>
				<input type="text" class="textBox" id="or_num" name="or_num" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">회원명/아이디</div>
			</td>
			<td>
				<input type="text" class="textBox" id="or_nm" name="or_nm" onkeypress="javascript:search_keypress();">
			</td>
		</tr>
	  </table>
	  <input class="search_button"  type="button" value="Search" onclick="javascript:search();">
	  <input class="exel_button"  type="button" value="Excel" onclick="javascript:excel_down();">
	  </form>
	  <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
			  <DIV class="total_div" >TOTAL : <%=total_cnt%></div>
              <h3 class="table-caption">주문리스트</h3>
              <!-- S: fix-head-table -->
              <table class="table table-striped table-hover table-content" style="float:center;">
                <colgroup>
                  <col width="80">
				  <col width="120">
				  <col width="120">
                  <col width="120">
                  <col width="120">
				  <col width="120">
                </colgroup>
                <tbody>
                  <tr>
                    <th width="80">전산코드</th>
					<th width="120" style="text-align:center;">주문일</th>
					<th width="120" style="text-align:center;">주문번호</th>
					<th width="120" style="text-align:center;">주문자명</th>
                    <th width="120" style="text-align:center;">주문자아이디</th>
                    <th width="120" style="text-align:center;">인증파일</th>
                    
                  </tr>
				 <%
						for i = (page-1) * page_size To numList
						  seq			= arrList(2,i)
						  or_dt			= arrList(3,i)
						  or_num		= arrList(4,i)
						  order_nm		= arrList(5,i)
						  online_id		= arrList(6,i)
						  upload_file	= arrList(7,i) 
					%>
					 <tr>
						<th width="80"><%=seq%></th>
						<th width="120" style="text-align:center;"><%=Left(or_dt,4)%>-<%=mid(or_dt,5,2)%>-<%=mid(or_dt,7,2)%></th>
						<th width="120" style="text-align:center;"><%=or_num%></th>
						<th width="120" style="text-align:center;"><%=order_nm %></th>
						<th width="120" style="text-align:center;"><%=online_id %></th>
						<th width="120" style="text-align:center;">
						<% If upload_file <> "" Then %>
							<a href="../../upload/<%=upload_file%>" target="_blank"><%=upload_file%></a>
						<% Else %>
							<font color="red">인증파일없음</font>
						<% End If %>
						</th>
					  </tr>
					<%

						h_number = h_number - 1

						next
					%>	
                </tbody>
              </table>
            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>