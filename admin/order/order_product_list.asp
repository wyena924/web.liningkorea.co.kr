<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>위드라인 홈페이지 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page		= request("page")
start_date	= request("start_date")
end_date	= request("end_date")
or_num		= request("or_num")
or_nm		= request("or_nm")
or_tel		= request("or_tel")
or_state	= request("or_state")	


If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_MALL_ADMIN_PRODUCT_ORDER_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size  &", @start_date='" &start_date&"', @end_date='" & end_date &"', @or_num='" &or_num &"' ,@or_nm='"&or_nm&"' , @or_tel='"&or_tel&"' ,@or_state='"&or_state &"',@OR_WRITE_ID='mall_sangpan'"
'Response.WRITE sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If



%>
  <script>
	function alliance_view(seq)
	{
		document.getElementById("content_div_"+ seq).style.display ='block'
	}

	function open_popup(or_seq)
	{
		window.open("http://www.itemcenter.co.kr/WEB/ic_order_cust.asp?seq="+or_seq+"&unigrpcd=2001&tp=01&emailkey=&gdscusttp=notview&DEL_YN=N", "window", "scrollbars=YES,width=800,height=800");
	}

	
	
	function cncl_product_change(ornum,seq,py_in_acnt,gd_price,row_number) {

		document.getElementById("trno").value = py_in_acnt;
		document.getElementById("canc_amt").value = gd_price;
		document.getElementById("canc_seq").value = row_number;
		if(confirm("주문번호"+ornum+" 을(를) 취소하시겠습니까?")==false)
			return 1;
		//parent.fBottom.popupOpen("","","처리중입니다!");

		var strAjaxUrl="/admin/ajax/order_cncl_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: {
					ornum:ornum,
					seq:seq ,	
			},

			success: function(retDATA) {
				if(retDATA){
					//var strcut = retDATA.split("|>");
					if (retDATA == "TRUE") {
						alert("취소가 완료되었습니다! 재조회 후 확인하십시오.");
						document.bform.target="cancle_iframe"
						document.bform.action="http://ksnet.itemcenter.co.kr/KSPayCancelPost_price.asp"
						document.bform.submit();
						location.reload();
					}

					if (retDATA == "CNCL") {

						alert("이미 취소 상태입니다!");
						parent.fBottom.popupClose("","","");
					}

					if (retDATA != "TRUE" && retDATA != "CNCL") {
						parent.fBottom.popupClose("","","");
						alert ("취소중에 오류가 발생하였습니다!");
					}
				}
			}, error: function(xhr, status, error){
				
				parent.fBottom.popupClose("","","");
				alert ("취소중 에러발생 - 시스템관리자에게 문의하십시오!"+' ['+error+']');
			}
		});
	}
	
	function search()
	{
		document.search_form.submit();
	}

	function search_keypress()
	{
		if (window.event.keyCode == 13) {
 
             search()
        }
	}

	function excel_down()
	{
		document.bform.action="excel_order_product_list.asp"
		document.bform.submit();
	}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">주문관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">주문리스트</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
	  <iframe id="cancle_iframe" name="cancle_iframe" width=0 height=0 style="display:none;"></iframe>
      <!-- E: page-title -->
	  <form method="post" action="" name="bform" id="bform">
			<input type="hidden" id="storeid" name="storeid" value="2001105535">
			<input type="hidden" id="storepasswd" name="storepasswd" value="">
			<input type="hidden" id="authty" name="authty" value="1010">	
			<input type="hidden" id="trno" name="trno" value="">
			<input type="hidden" id="start_date" name="start_date" value="">
			<input type="hidden" id="end_date" name="end_date" value="">
			<input type="hidden" id="or_num" name="or_num" value="">
			<input type="hidden" id="or_nm" name="or_nm" value="">
			<input type="hidden" id="or_tel" name="or_tel" value="">
			<input type="hidden" id="or_state" name="or_state" value="">
			<input type="hidden" id="canc_amt" name="canc_amt" value="">
			<input type="hidden" id="canc_seq" name="canc_seq" value="">		
	  </form>
	  <form name="search_form" method="post">
      <!-- S: page-search -->
	  <table class="page-search">
		<tr>
			<td><div class="Palal">주문일</div></td>
			<td class="date_td">
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=start_date%>" name="start_date" id="start_date" value="<%= start_date %>" readonly>
				<div>~</div>
				<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=end_date%>" name="end_date" id="end_date" value="<%= end_date %>" readonly>
			</td>
			<td><div class="Palal">주문번호</div></td>
			<td>
				<input type="text" class="textBox" id="or_num" name="or_num" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">수령인</div>
			</td>
			<td>
				<input type="text" class="textBox" id="or_nm" name="or_nm" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">휴대전화</div>
			</td>
			<td>
				<input type="text" class="textBox" id="or_tel" name="or_tel" onkeypress="javascript:search_keypress();">
			</td>
			<td><div class="Palal">처리상태</div></td>
			<td>
				<select class="textBox" id="or_state" name="or_state">
					<option value="">선택해주세요</option>
					<option value="00">미확정</option>
					<option value="01">진행중</option>
					<option value="02">배송완료</option>
					<option value="99">취소</option>
				</select>
			</td>	
		</tr>
	  </table>
	  
	  <input class="search_button"  type="button" value="Search" onclick="javascript:search();">
	  <input class="exel_button"  type="button" value="Excel" onclick="javascript:excel_down();">
	  </form>
	  <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
			  <DIV class="total_div" >TOTAL : <%=total_cnt%></div>
              <h3 class="table-caption">주문리스트</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">

                <colgroup>
                  <col width="80">
				  <col width="200">
                  <col width="200">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
                  
                </colgroup>
                <tbody>
                  <tr>
                    <th width="80">주문일자</th>
					<th width="200" style="text-align:center;">주문번호</th>
					<th width="200" style="text-align:center;">주문상품</th>
                    <th width="120" style="text-align:center;">옵션1</th>
                    <th width="120" style="text-align:center;">옵션2</th>
                    <th width="120" style="text-align:center;">수량</th>
                    <th width="120" style="text-align:center;">주문금액</th>
                    <th width="120" style="text-align:center;">주문자</th>
					<th width="120" style="text-align:center;">인도처명</th>
					<th width="120" style="text-align:center;">전화번호</th>
					<th width="120" style="text-align:center;">우편번호</th>
					<th width="120" style="text-align:center;">주소</th>
					<th width="120" style="text-align:center;">상세주소</th>
					<th width="120" style="text-align:center;">특이사항</th>
					<th width="120" style="text-align:center;">주문상태</th>
					<th width="120" style="text-align:center;">배송완료일</th>
					<th width="120" style="text-align:center;">주문상태변경</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="float:center;">
                  <colgroup>
                  <col width="80">
				  <col width="200">
                  <col width="200">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">
				  <col width="120">       
                  </colgroup>
                  <tbody>
					<%
						for i = (page-1) * page_size To numList
						  seq		= arrList(2,i)
						  or_dt		= arrList(3,i)
						  or_num	= arrList(4,i)
						  gd_nm		= arrList(5,i)
						  gd_size		= arrList(6,i)
						  gd_color		= arrList(7,i)
						  gd_vol		= arrList(8,i)
						  gd_price		= arrList(9,i)
						  online_id		= arrList(10,i)
						  or_in_pe2		= arrList(11,i)
						  or_in_tel		= arrList(12,i)
						  or_in_zip		= arrList(13,i)
						  addr1			= arrList(14,i)
						  addr2			= arrList(15,i)	
						  or_in_memo	= arrList(16,i)	
						  gd_state_nm	= arrList(17,i)	
						  gd_send_end_dt= arrList(18,i)
						  ORDER_SEQ		= arrList(19,i)	
						  gd_state		= arrList(20,i)
						  py_in_acnt	= arrList(21,i)
						  row_number_cnt= arrList(22,i)
					%>
					 <tr>
						<th width="80"><%=or_dt%></th>
						<th width="200" style="text-align:center;">

							<input type="button" onclick="javascript:open_popup('<%=seq%>')" value="<%=or_num%>">
						</th>
						<th width="200" style="text-align:center;"><%=gd_nm %></th>
						<th width="120" style="text-align:center;"><%=gd_size %></th>
						<th width="120" style="text-align:center;"><%=gd_color %></th>
						<th width="120" style="text-align:center;"><%=gd_vol %></th>
						<th width="120" style="text-align:center;"><%=FormatNumber(gd_price,0) %></th>
						<th width="120" style="text-align:center;"><%=online_id %></th>
						<th width="120" style="text-align:center;"><%=or_in_pe2 %></th>
						<th width="120" style="text-align:center;"><%=or_in_tel %></th>
						<th width="120" style="text-align:center;"><%=or_in_zip %></th>
						<th width="120" style="text-align:center;"><%=addr1 %></th>
						<th width="120" style="text-align:center;"><%=addr2 %></th>
						<th width="120" style="text-align:center;"><%=or_in_memo %></th>
						<th width="120" style="text-align:center;"><%=gd_state_nm %></th>
						<th width="120" style="text-align:center;"><%=gd_send_end_dt %></th>
						<th width="120" style="text-align:center;">
							<% If gd_state = "00" Then %>
								<input type="button" onclick="cncl_product_change('<%=OR_NUM%>','<%=ORDER_SEQ%>','<%=py_in_acnt%>','<%=gd_price%>','<%=row_number_cnt%>');" value="주문취소">
							<% Else %>
								<%= gd_state_nm%>
							<%		
								End If
							%>				
						</th>
					  </tr>
					<%

						h_number = h_number - 1

						next
					%>	
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>