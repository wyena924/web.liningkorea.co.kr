<!--#include virtual="/admin/Library/config.asp"-->
<title>위드라인 홈페이지 어드민</title>
<%
filename = "order_excel"  & Year(now()) & Month(now()) & Day(now())
Response.Buffer = TRUE
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-disposition","attachment;filename=" & filename & ".xls"

page		= request("page")
start_date	= request("start_date")
end_date	= request("end_date")
or_num		= request("or_num")
or_nm		= request("or_nm")
or_tel		= request("or_tel")
or_state	= request("or_state")	


If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 1000000
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_MALL_ADMIN_PRODUCT_ORDER_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size  &", @start_date='" &start_date&"', @end_date='" & end_date &"', @or_num='" &or_num &"' ,@or_nm='"&or_nm&"' , @or_tel='"&or_tel&"' ,@or_state='"&or_state &"',@OR_WRITE_ID='mall_sangpan'"
'Response.WRITE sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If



%>
<table BORDER=1 style="font-style: normal; font-weight: normal; ">

  <colgroup>	
	  <col width="80">
      <col width="80">
	  <col width="200">
	  <col width="200">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
	  <col width="120">
    
  </colgroup>
  <tbody>
    <tr>
		<th width="80">전산코드</th>
		<th width="80">주문일자</th>
		<th width="200" style="text-align:center; mso-number-format:\@">주문번호</th>
		<th width="200" style="text-align:center;">주문상품</th>
		<th width="120" style="text-align:center;">옵션1</th>
		<th width="120" style="text-align:center;">옵션2</th>
		<th width="120" style="text-align:center;">수량</th>
		<th width="120" style="text-align:center; mso-number-format:\@">주문금액</th>
		<th width="120" style="text-align:center;">주문자</th>
		<th width="120" style="text-align:center;">인도처명</th>
		<th width="120" style="text-align:center;">전화번호</th>
		<th width="120" style="text-align:center;">우편번호</th>
		<th width="120" style="text-align:center;">주소</th>
		<th width="120" style="text-align:center;">상세주소</th>
		<th width="120" style="text-align:center;">특이사항</th>
		<th width="120" style="text-align:center;">주문상태</th>
		<th width="120" style="text-align:center;">배송완료일</th>
    </tr>
	  <%
			for i = (page-1) * page_size To numList
			  seq		= arrList(2,i)
			  or_dt		= arrList(3,i)
			  or_num	= arrList(4,i)
			  gd_nm		= arrList(5,i)
			  gd_size		= arrList(6,i)
			  gd_color		= arrList(7,i)
			  gd_vol		= arrList(8,i)
			  gd_price		= arrList(9,i)
			  online_id		= arrList(10,i)
			  or_in_pe2		= arrList(11,i)
			  or_in_tel		= arrList(12,i)
			  or_in_zip		= arrList(13,i)
			  addr1			= arrList(14,i)
			  addr2			= arrList(15,i)	
			  or_in_memo	= arrList(16,i)	
			  gd_state_nm	= arrList(17,i)	
			  gd_send_end_dt= arrList(18,i)
			  order_seq		= arrList(19,i)
	  %>
	   <tr>
		<th width="80"><%=order_seq%></th>
	  	<th width="80"><%=or_dt%></th>
	  	<th width="200" style="text-align:center;mso-number-format:\@"><%=or_num%></th>
	  	<th width="200" style="text-align:center;"><%=gd_nm %></th>
	  	<th width="120" style="text-align:center;"><%=gd_size %></th>
	  	<th width="120" style="text-align:center;"><%=gd_color %></th>
	  	<th width="120" style="text-align:center;"><%=gd_vol %></th>
	  	<th width="120" style="text-align:center;mso-number-format:\@"><%=FormatNumber(gd_price,0) %></th>
	  	<th width="120" style="text-align:center;"><%=online_id %></th>
	  	<th width="120" style="text-align:center;"><%=or_in_pe2 %></th>
	  	<th width="120" style="text-align:center;"><%=or_in_tel %></th>
	  	<th width="120" style="text-align:center;"><%=or_in_zip %></th>
	  	<th width="120" style="text-align:center;"><%=addr1 %></th>
	  	<th width="120" style="text-align:center;"><%=addr2 %></th>
	  	<th width="120" style="text-align:center;"><%=or_in_memo %></th>
	  	<th width="120" style="text-align:center;"><%=gd_state_nm %></th>
	  	<th width="120" style="text-align:center;"><%=gd_send_end_dt %></th>
	    </tr>
	  <%
	     
	  		h_number = h_number - 1
	     
	  		next
	  %>	
  </tbody>
</table>
<!-- E: fix-head-table -->
