<!--#include virtual="/admin/Library/config.asp"-->

<%
g_excelSubject = request("g_excelSubject")

excelFileName = "order_file_excel"

Response.ContentType = "application/vnd.ms-excel"

response.addheader "Pragma", "no-cache" 

response.addheader "content-disposition", "attachment; filename=" & excelFileName & ".xls"

response.charset = "utf-8"

response.write "<meta http-equiv='Content-Type' content='text/html;charset=utf-8'>"

response.write "<style>.txt {mso-number-format:'\@'}</style>"


page		= request("page")
start_date	= request("start_date")
end_date	= request("end_date")
or_num		= request("or_num")
or_nm		= request("or_nm")
or_tel		= request("or_tel")
or_state	= request("or_state")	


If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 1000000
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_MALL_ADMIN_ORDER_FILE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size  &", @start_date='" &start_date&"', @end_date='" & end_date &"',@OR_WRITE_ID='"&GLOBAL_OR_WRITE_ID&"' , @or_num='" &or_num &"' ,@order_nm='"&or_nm&"' "
'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If



%>
<table BORDER=1 style="font-style: normal; font-weight: normal; ">
  <colgroup>
		<col width="80">
		<col width="120">
		<col width="120">
		<col width="120">
		<col width="120">
	    <col width="500">
  </colgroup>
  <tbody>
    <tr>
      <th width="80">전산코드</th>
		<th width="120" style="text-align:center;">주문일</th>
		<th width="120" style="text-align:center;">주문번호</th>
		<th width="120" style="text-align:center;">주문자명</th>
      <th width="120" style="text-align:center;">주문자아이디</th>
      <th width="500" style="text-align:center;">인증파일</th>
      
    </tr>
	 <%
			for i = (page-1) * page_size To numList
			  seq			= arrList(2,i)
			  or_dt			= arrList(3,i)
			  or_num		= arrList(4,i)
			  order_nm		= arrList(5,i)
			  online_id		= arrList(6,i)
			  upload_file	= arrList(7,i) 
		%>
		 <tr>
			<th width="80"><%=seq%></th>
			<th width="120" style="text-align:center;"><%=Left(or_dt,4)%>-<%=mid(or_dt,5,2)%>-<%=mid(or_dt,7,2)%></th>
			<th width="120" style="text-align:center;"><%=or_num%></th>
			<th width="120" style="text-align:center;"><%=order_nm %></th>
			<th width="120" style="text-align:center;"><%=online_id %></th>
			<th width="500" style="text-align:center;">
			<% If upload_file <> "" Then %>
				<a href="../../upload/<%=upload_file%>">http://b2bc.samsungbizmall.com/upload/<%=upload_file%></a>
			<% Else %>
				<font color="red">인증파일없음</font>
			<% End If %>
			</th>
		  </tr>
		<%

			h_number = h_number - 1

			next
		%>	
  </tbody>
</table>