<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
Set Upload = Server.CreateObject("TABSUpload4.Upload")
Upload.CodePage = 65001
Upload.MaxBytesToAbort = 10 * 1024 * 1024

root_path = Server.MapPath("/pmaa/admin")


save_path = root_path & "\upload\notice\"

'Upload.Start "c:\Temp"
Upload.Start save_path

'response.write save_path&"<br>"
'response.write "seq1 = " & request("seq")&"<br>"

mode = Upload.Form("mode")
page = Upload.Form("page")
seq = Upload.Form("seq")

goParam = "?page=" & page

writer = Upload.Form("writer")
subject = Upload.Form("subject")
content = Upload.Form("content")
news_gb = Upload.Form("news_gb")
isview = Upload.Form("isview")

if seq = "" then seq = 0

if mode <> "DEL" then
	if subject = "" then
		%>
		<script>
			parent.alertBox("제목을 입력해 주세요.");
		</script>
		<%
		response.end
	end if
	if writer = "" then
		%>
		<script>
			parent.alertBox("작성자를 입력해 주세요.");
		</script>
		<%
		response.end
	end if
	if content = "" then
		%>
		<script>
			parent.alertBox("내용을 입력해 주세요.");
		</script>
		<%
		response.end
	end if

	Set File = Upload.Form("file")
		
	org_filename = File.FileName
	save_filename = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename,3)
	ext = LCase(mid(org_filename,InstrRev(org_filename,".")+1))

	if org_filename <> "" then
		chkext = array("jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf")
		chk = 0
		for i = 0 to UBound(chkext)
		if ext = chkext(i) then
			chk = chk + 1
			exit for
		end if
		next

		if chk = 0 then  
			%>
			<script>
				parent.alertBox("JPG, GIF, PNG, DOC(DOCX), PPT(PPTX), XLS(XLSX), HWP, ZIP, PDF 파일만 <br>업로드가 가능합니다.");
			</script>
			<%
			response.end
		end if

		if org_filename = "" then
			tmp_save_filename= "||"
		else
			tmp_save_filename = org_filename & "|" & save_filename & "|" & ext
		end if
			
		File.SaveAs save_path & save_filename , False
	else
		tmp_save_filename= "||"
	end if

	'Response.Write File.FileName & "<br>"
	'Response.Write File.SaveName

	Set File = nothing
end if

DBopen()

subject = replace(subject,"'","''")
writer = replace(writer,"'","''")
content = replace(content,"'","''")

sql = "USP_PMAA_BOARD_NOTICE_ACCESS " & _
			" @MODE				= '" & mode & "'" & _
			",@SEQ				=  " & seq & _
			",@SUBJECT			= '" & subject & "'" & _
			",@WRITER			= '" & writer & "'" & _
			",@CONTENT			= '" & content & "'" & _
			",@FILENAME			= '" & tmp_save_filename & "'" & _
			",@ISVIEW			= '" & isview & "'" & _
			",@ONLINE_CD		= '" & global_var_onlinecd & "'" 
			response.write sql&"<br>"
			'Response.end
Set rs = DBcon.Execute(sql)
if rs(0) = 0 then
	%>
	<script>
		parent.alertBox("데이터 처리에 문제가 발생했습니다.");
	</script>
	<%
	response.end
end if

DBclose()

%>

<script>
	parent.alertBox("정상 처리되었습니다.",function() {
		parent.location.href = "notice_list.asp<%= goParam %>";
	});
</script>