<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
Response.buffer = true
Response.Expires=0

kbn = request("kbn")
seq = request("seq")
fnum = request("fnum")

if fnum = "" then fnum = 1

if kbn = "notice" then
	tbl = "USP_A_BOARD_NOTICE_VIEW"
elseif kbn = "qna" then
	tbl = "USP_A_BOARD_QNA_VIEW"
end if

if seq = "" then
	PrintMsg("데이터 번호가 존재하지 않습니다.")
	response.end
end if

DBopen()
sql = tbl &" @MODE='VIEW', @SEQ=" & seq
Set rs = DBcon.execute(sql)
if rs.eof or rs.bof then
	PrintMsg("데이터가 존재하지 않습니다.")
	response.end
else
	if kbn = "notice" then
		filename = rs(3)
		
		arr = split(filename,"|")
		org_filename = arr(0)
		save_filename = arr(1)

		save_filepath = Server.MapPath("/") & "\upload\notice\" & save_filename
	else
		if fnum = 1 then
			filename = rs(6)
		elseif fnum = 2 then
			filename = rs(10)
		end if

		arr = split(filename,"|")
		org_filename = arr(0)
		save_filename = arr(1)

		save_filepath = Server.MapPath("/") & "\upload\qna\" & save_filename
	end if

end if
DBclose()

Response.Clear    

Response.ContentType = "application/unknown"
Response.charset = "utf-8"

Response.AddHeader "Pragma", "no-cache"
Response.AddHeader "Expires", "0"
Response.AddHeader "Content-Transfer-Encoding","binary"
  
Response.AddHeader "Content-Disposition","attachment;filename=" & Server.URLPathEncode(org_filename)
'헤더값이 첨부파일을 선언합니다.

Set objStream = Server.CreateObject("ADODB.Stream")    
'Stream 을 이용합니다.

objStream.Open    
'무엇이든 Set 으로 정의했으면 열어야 겠지요^^

objStream.Type = 1

objStream.LoadFromFile save_filepath
'절대경로 입니다.  

download = objStream.Read
Response.BinaryWrite download 
'이게 보통 Response.Redirect 로 파일로 연결시켜주는 부분을 대신하여 사용된 것입니다.

Set objstream = nothing 
'초기화시키구요.

Response.End

%>