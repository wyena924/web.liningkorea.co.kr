;(function($){
  /**
   * 위드라인 utils.js
   */
  
  var WD = {};

  /**
   * tg = $(".sel-box");
   */
  WD.SelectArrange = function(tg) {
    this._$tg = $(tg); /* sel-box */
    this._$select = null; /* sel-box 안의 select */
    this._selectText = ""; /* 선택된 text를 담을 프로퍼티 */

    this._init();
    this._evt();
  }

  WD.SelectArrange.prototype._init = function () {
    this._$select = $("select", this._$tg);
  }

  WD.SelectArrange.prototype._evt = function () {
    var that = this;
    this._$select.on("change", function(e) {
      that._selectText = $(this).children("option:selected").text();
      $(this).siblings(".txt").text(that._selectText);
    });  // this._$select change end
  }

  $.fn.selectArr = function(tg) {
    this.each(function() {
      var selectArr = new WD.SelectArrange(tg);
    });
    return this;
  }

  /**
   * 파일등록 input type="file" 기능 보완
   * tg = .file-browser
   */
  WD.FileInputCtr = function(tg) {
    this._$tg = $(tg);
    this._$fileIpt = null; /* .file-browser 안의 파일type input */

    this._init();
    this._evt();
  }

  WD.FileInputCtr.prototype._init = function() {
    this._$fileIpt = $("input[type='file']", this._$tg);
  }

  WD.FileInputCtr.prototype._evt = function() {
    var that = this;
    this._$fileIpt.on("change", function(){
      var $file = $(this).val();
      $(this).siblings(".ipt-view").find(".placeholder").text($file);
    })
  }

  $.fn.fileInputCtr = function(tg) {
    this.each(function() {
      var fileInputCtr = new WD.FileInputCtr(tg);
    });
    return this;
  }


  /**
   * 실행 구문 모음
   */
  $(".sel-box").selectArr(".sel-box");  /* sel-box 실행 */
  $(".file-browser").fileInputCtr(".file-browser");  /*file-browser 실행*/



})(jQuery);