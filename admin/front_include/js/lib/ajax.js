

var agent = navigator.userAgent.toLowerCase();
if(agent.indexOf("chrome") != -1) {
	// chrome
} else {
	// ie
	var addConsoleNoOp =  function (window) {
	    var names = ["log", "debug", "info", "warn", "error",
	        "assert", "dir", "dirxml", "group", "groupEnd", "time",
	        "timeEnd", "count", "trace", "profile", "profileEnd"],
	        i, l = names.length,
	        noOp = function () {};
	    window.console = {};
	    for (i = 0; i < l; i = i + 1) {
	        window.console[names[i]] = noOp;
	    }
	};

	if (!window.console || !window.development_mode) {
	    this.addConsoleNoOp(window);
	}
}


var ajax = {

	openModal: function() {

		$('body').loadingModal({
			position: 'auto',
			text: 'Please wait loading...',
			color: '#fff',
			opacity: '0.7',
			backgroundColor: 'rgba(0, 0, 0, 0.7)',
			animation: 'threeBounce'
		});
	},

	errorModal: function(result, txtStatus, error) {
		ajax.closeModal();
		if(result.status == "900") {
			location.href = "/login";
		} else {
			alert("데이터 처리에 문제가 발생했습니다.");
		}
	},

	closeModal: function() {
		$('body').loadingModal('destroy');
	},

	get: function(url, callback, mode, modal) {

		modal = (typeof modal === 'undefined') ? true : modal;
		mode = (typeof mode === 'undefined') ? "data" : mode;

		if(mode == "data") {
			$.ajax({
				type: 'get',
				url: url,
				cache: false,
				headers: {
					"Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8",
					"X-HTTP-Method-Override" : "GET"
				},
				dataType: 'html',
				beforeSend: function() {
					if(modal) ajax.openModal();
				},
				error: function(result, txtStatus, error) {
					ajax.errorModal(result, txtStatus, error);
				},
				success: function(data) {
					if(modal)  ajax.closeModal();
					callback(data);
				}
			});
		} else if(mode == "json") {
			$.ajax({
				type: 'get',
				url: url,
				cache: false,
				headers: {
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "GET"
				},
				dataType: 'text',
				beforeSend: function() {
					if(modal) ajaxjson.openModal();
				},
				error: function(result, txtStatus, error) {
					ajaxjson.errorModal(result, txtStatus, error);
				},
				success: function(data) {
					if(modal)  ajaxjson.closeModal();
					callback(data);
				}
			});
		}
	},

	post: function(url, params, callback, mode, modal) {

		modal = (typeof modal === 'undefined') ? true : modal;
		mode = (typeof mode === 'undefined') ? "data" : mode;

		if(mode == "data") {
			$.ajax({
				type: 'post',
				url: url,
				cache: false,
				headers: {
					"Content-Type" : "application/x-www-form-urlencoded; charset=UTF-8",
					"X-HTTP-Method-Override" : "POST"
				},
				//contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
				dataType: 'html',	
				data: params,
				beforeSend: function() {
					if(modal)  ajax.openModal();
				},
				error: function(result, txtStatus, error) {
					ajax.errorModal(result, txtStatus, error);
				},
				success: function(data) {
					if(modal)  ajax.closeModal();
					callback(data);
				}
			});
		} else if(mode == "json") {
			$.ajax({
				type: 'post',
				url: url,
				cache: false,
				headers: {
					"Content-Type" : "application/json",
					"X-HTTP-Method-Override" : "POST"
				},
				dataType: 'text',
				data: JSON.stringify(params),
				beforeSend: function() {
					if(modal)  ajaxjson.openModal();
				},
				error: function(result, txtStatus, error) {
					ajaxjson.errorModal(result, txtStatus, error);
				},
				success: function(data) {
					if(modal)  ajaxjson.closeModal();
					callback(data);
				}
			});
		}
	},

	submit: function(url, id, callback, modal) {

		modal = (typeof modal === 'undefined') ? true : modal;

		$("#" + id).ajaxSubmit({
			url: url,
			type: 'post',
			dataType: 'html',
			beforeSend: function() {
				if(modal)  ajax.openModal();
			},
			error: function(result, txtStatus, error) {
				ajax.errorModal(result, txtStatus, error);
			},
			success: function(data) {
				if(modal)  ajax.closeModal();
				callback(data);
			}
		});
	},

	frame: function(url, frm) {
		ajax.openModal();
		$("#temp_hidden_frame").remove();
		var frame = "<iframe name='temp_hidden_frame' id='temp_hidden_frame' onload='ajax.frameComplete();' scrollborder='0' border='0' scrolling='no' style='width:0px;height:0px;'></iframe>";
		$("body").append(frame);
		frm.target = "temp_hidden_frame";
		frm.action = url;
		frm.submit();
	},

	frameComplete: function() {
		ajax.closeModal();
		$("#temp_hidden_frame").remove();
	}
};

