<%

  if nDepth = 0 then
    global_depth = "."
  elseif nDepth = 1 then
    global_depth = ".."
  elseif nDepth = 2 then
    global_depth = "../.."
  elseif nDepth = 3 then
    global_depth = "../../.."
  elseif nDepth = 4 then
    global_depth = "../../../.."
  elseif nDepth = 5 then
    global_depth = "../../../../.."
  else
    global_depth = "."
  end if

 %>