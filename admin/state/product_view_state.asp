<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%

start_date	= request("start_date")
end_date	= request("end_date")

If start_date = "" Then
	start_date = Date - 30
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 


%>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script src="/admin/library/fusioncharts.js"></script>
<script src="/admin/library/fusioncharts.theme.fint.js"></script>
<script src="/admin/library/fusioncharts.charts.js"></script>
<script>
	
	state();
	state2();

	function state()
		{
			var strJsonUrl = "/admin/ajax/product_view_cnt_state.asp"
			//location.href= strJsonUrl
			FusionCharts.ready(function () {
			  var demographicsChart = new FusionCharts({
				type: 'Column3D',
				renderAt: 'chart-container',
				width: '100%',
				height: '400',
				dataFormat: 'jsonurl',
				dataSource: strJsonUrl

			  });
			  demographicsChart.render("chart_view");
				
			});
		}

	function state2()
		{
			var strJsonUrl = "/admin/ajax/product_view_order_state.asp"
			//location.href= strJsonUrl
			FusionCharts.ready(function () {
			  var demographicsChart = new FusionCharts({
				type: 'Column3D',
				renderAt: 'chart-container',
				width: '100%',
				height: '400',
				dataFormat: 'jsonurl',
				dataSource: strJsonUrl

			  });
			  demographicsChart.render("chart_view2");
				
			});
		}
	function search()
	{
		document.search_form.submit();
	}

	function search_keypress()
	{
		if (window.event.keyCode == 13) {
 
             search()
        }
	}
</script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">통계</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">상품별뷰</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
	  <form name="search_form" method="post">
	  </form>
	  <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table" >
              <h3 class="table-caption">상품별 VIEW</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="30%">
				 
                </colgroup>
              </table>
              <!-- E: fix-head-table -->
              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="float:center;">
                  <colgroup>
                    <col width="30%">        
                  </colgroup>
                  <tbody>
				  <tr>
					<th width="30%">
						<div class="state-cont" id="chart_view" >
							
						</div>
					</th>	
				  </tr>
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->
			<!-- S: table -->
			<div class="fixed-table" >
              <h3 class="table-caption">상품별 판매</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="30%">
				 
                </colgroup>
              </table>
              <!-- E: fix-head-table -->
              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="float:center;">
                  <colgroup>
                    <col width="30%">        
                  </colgroup>
                  <tbody>
				  <tr>
					<th width="30%">
						<div class="state-cont" id="chart_view2" >
							
						</div>
					</th>	
				  </tr>
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>