<!--#include virtual="/admin/Library/config.asp"-->
<%
filename = "member_login_excel"  & Year(now()) & Month(now()) & Day(now())
Response.Buffer = TRUE
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-disposition","attachment;filename=" & filename & ".xls"

page		= request("page")
order_nm	= request("order_nm")
online_id	= request("online_id")
cm_email	= request("cm_email")
cm_addr		= request("cm_addr")		

date_type	= request("date_type")		

start_date	= request("start_date")
end_date	= request("end_date")

If start_date = "" Then
	start_date = Date - 10
End If 

If END_DATE = "" Then
	End_DATE = Date 
End If 



if date_type = "" then 
	date_type="member"
end if 


if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 9999999
block_page = 10	

goParam = "?tmp=null"

Dbopen()
sql = " USP_MALL_ADMIN_MEMBER_LOGIN_STATE @PAGE = '"&page&"',@PAGE_SIZE='"&page_size&"',@start_date='"&start_date&"',@end_date='"&end_date&"',@date_type='"&date_type&"',@online_id='"&online_id&"',@order_nm='"&order_nm&"',@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"',@GRP1='"&GLOBAL_VAR_GRP1&"',@GRP2='"&GLOBAL_VAR_GRP2&"'"

'Response.write sql
'Response.end

Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If
%>
<table BORDER=1 style="font-style: normal; font-weight: normal; width:2000px; ">
  <colgroup>
	  <col width="80">
	  <col width="200">
	  <col width="200">
	  <col width="200">
	  <col width="200">
	  <col width="200">
  </colgroup>
  <tr>
    <th width="80">일련번호</th>
	<th width="200" style="text-align:center;">회원아이디</th>
	<th width="200" style="text-align:center;">회원가입일</th>
	<th width="200" style="text-align:center;">로그인일자</th>
    <th width="200" style="text-align:center;">로그인횟수</th>
	<th width="200" style="text-align:center;">상품뷰횟수</th>
  </tr>
  <tbody>
	<%
		for i = (page-1) * page_size To numList
		  order_seq	    = arrList(2,i)
		  online_id		= arrList(3,i) & "(" & arrList(4,i) &")"
		  reg_date		= arrList(5,i)
		  login_date	= arrList(6,i)
		  login_cnt		= arrList(7,i)
		  product_cnt	= arrList(9,i)
		  del_yn		= arrList(10,i)
	%>
	 <tr>
		<th width="80">
			<%=order_seq%>
		</th>
		<th width="200" style="text-align:center;">
			<%=online_id%>
		</th>
		<th width="200" style="text-align:center;">
			<%=reg_date%>				
		</th>
		<th width="200" style="text-align:center;">
			<%=login_date %>
		</th>
		<th width="200" style="text-align:center;">
			<%=login_cnt %>
		</th>
		<th width="200" style="text-align:center;">
			<%=product_cnt%>
		</th>
		<th width="200" style="text-align:center;">
			<%=del_yn%>
		</th>
		
	  </tr>
	<%
		h_number = h_number - 1

		next
	%>	
</table>