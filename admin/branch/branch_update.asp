<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDeptd = 1 'css 및 img, DOM deptd'
    virtual_Deptd = "" '무조건 최상단'

	seq = request("seq")

	seql_text = " SELECT SEQ		"
	seql_text = seql_text &"  	  ,BRANCH_GROUP	"
	seql_text = seql_text &"  	  ,BRANCH_NAME	"
	seql_text = seql_text &"  	  ,BRANCH_ADDR	"
	seql_text = seql_text &"  	  ,BRANCH_TEL	"
	seql_text = seql_text &"  	  ,REG_DATE	"
	seql_text = seql_text &"  	  ,DEL_YN	"
	seql_text = seql_text &"  FROM dbo.IC_T_SAMPAN_BRANCH_LIST	"
	seql_text = seql_text &"  where seq = '"&seq&"'	"
	
	DBopen()

	Set rs = DBcon.Execute(seql_text)

	DBclose()
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
  <script>

	function trim(str) {
		return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}



	function writer()
	{
		var branch_group	= document.getElementById("branch_group").value
		var branch_name		= document.getElementById("branch_name").value
		var branch_addr		= document.getElementById("branch_addr").value
		var branch_tel		= document.getElementById("branch_tel").value
		
		if (branch_group =='')
		{
			alert('지역을 선택해주시기 바랍니다');
			document.getElementById("branch_group").focus();
			return;
		}

		if (branch_name =='')
		{
			alert('매장명을 입력해주시기 바랍니다');
			document.getElementById("branch_name").focus();
			return;
		}

		if (branch_addr =='')
		{
			alert('매장주소를 입력해주시기 바랍니다');
			document.getElementById("branch_addr").focus();
			return;
		}

		if (branch_tel =='')
		{
			alert('연락처를 입력해주시기 바랍니다');
			document.getElementById("branch_addr").focus();
			return;
		}
		document.bfrom.submit();
	}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">매장</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">매장등록</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
	  <form name="excel_form">
		<input type="hidden" id="e_order_nm" name="e_order_nm" value="<%=ORDER_NM%>">
		<input type="hidden" id="e_cm_tel" name="e_cm_tel" value="<%=CM_TEL%>">
		<input type="hidden" id="e_cm_email" name="e_cm_email" value="<%=CM_EMAIL%>">
		<input type="hidden" id="e_cm_addr" name="e_cm_addr" value="<%=CM_ADDR%>">
	  </form>
	  <form name="bfrom" method="post" action="branch_update_ok.asp">
	  <input type="hidden" name="seq" id="seq" value="<%=seq%>">
      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table" style="width:500px; float:center;">	 
              <h3 class="table-caption">매장등록</h3>
              <!-- S: fix-head-table -->
                <table class="table table-striped table-content" style="width:500px; float:center;">
					  <tr>
						<td width="80">지역</td>
						<td width="80">
							<select class="textBox" ID="branch_group" name="branch_group">
								<option value="">선택</option>
								<option VALUE="강북" <% If rs("BRANCH_GROUP") = "강북" Then %>selected<% End if%>>강북</option>	
								<option VALUE="서울 강남/강동"<% If rs("BRANCH_GROUP") = "서울 강남/강동" Then %>selected<% End if%>>서울 강남/강동</option>	
								<option VALUE="서울 강서"<% If rs("BRANCH_GROUP") = "서울 강서" Then %>selected<% End if%>>서울 강서</option>	
								<option VALUE="경기 북부"<% If rs("BRANCH_GROUP") = "경기 북부" Then %>selected<% End if%>>경기 북부</option>	
								<option VALUE="경기 남부"<% If rs("BRANCH_GROUP") = "경기 남부" Then %>selected<% End if%>>경기 남부</option>	
								<option VALUE="인천" <% If rs("BRANCH_GROUP") = "인천" Then %>selected<% End if%>>인천</option>	
								<option VALUE="충북" <% If rs("BRANCH_GROUP") = "충북" Then %>selected<% End if%>>충북</option>	
								<option VALUE="충남" <% If rs("BRANCH_GROUP") = "충남" Then %>selected<% End if%>>충남</option>	
								<option VALUE="대전" <% If rs("BRANCH_GROUP") = "대전" Then %>selected<% End if%>>대전</option>	
								<option VALUE="강원" <% If rs("BRANCH_GROUP") = "강원" Then %>selected<% End if%>>강원</option>	
								<option VALUE="전북" <% If rs("BRANCH_GROUP") = "전북" Then %>selected<% End if%>>전북</option>	
								<option VALUE="전남" <% If rs("BRANCH_GROUP") = "전남" Then %>selected<% End if%>>전남</option>	
								<option VALUE="광주" <% If rs("BRANCH_GROUP") = "광주" Then %>selected<% End if%>>광주</option>	
								<option VALUE="경북" <% If rs("BRANCH_GROUP") = "경북" Then %>selected<% End if%>>경북</option>	
								<option VALUE="경남" <% If rs("BRANCH_GROUP") = "경남" Then %>selected<% End if%>>경남</option>	
								<option VALUE="대구" <% If rs("BRANCH_GROUP") = "대구" Then %>selected<% End if%>>대구</option>	
								<option VALUE="울산" <% If rs("BRANCH_GROUP") = "울산" Then %>selected<% End if%>>울산</option>	
								<option VALUE="부산" <% If rs("BRANCH_GROUP") = "부산" Then %>selected<% End if%>>부산</option>	
								<option VALUE="제주" <% If rs("BRANCH_GROUP") = "제주" Then %>selected<% End if%>>제주</option>	
							</select>

						</td>
					  </tr>
					  <tr>
						<td width="80">매장명</td>
						<td width="80">
							<input type="text" class="textBox" ID="branch_name" name="branch_name"  value="<%=rs("branch_name")%>">
						</td>
					  </tr>
					   <tr>
						<td width="80">매장주소</td>
						<td width="80">
							<input type="text" class="textBox" ID="branch_addr" name="branch_addr" value="<%=rs("branch_addr")%>">
						</td>
					  </tr>
					   <tr>
						<td width="80">매장연락처</td>
						<td width="80">
							<input type="text" class="textBox" ID="branch_tel" name="branch_tel" value="<%=rs("branch_tel")%>">
						</td>
					  </tr>
                  <tbody>
					
                </table>
				<br>
				<input type="button" value="수정완료" style="float:right;" onclick="javascript:writer();">
                <!-- E: table-content -->
                </form>
            </div>
            <!-- E: fixed-table -->
			</form>
            <!-- S: comp-part -->
          <div class="comp-part">
			
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <!-- E: pagination -->
            </div>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>