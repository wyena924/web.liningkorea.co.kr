<%
'=========================================================================================================================================================
'============================================================* injection 처리  ===========================================================================
'=========================================================================================================================================================
Function fInject(argData)
 	Dim strCheckArgSQL
	Dim arrSQL
  	Dim i

	strCheckArgSQL = LCase(Trim(argData))	
	
	arrSQL = Array("exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt","</div>")

	For i=0 To ubound(arrSQL) Step 1
		If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

		If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

	Next

	'Xss 필터링	
	argData = Replace(argData,"&","&amp;")
	argData = Replace(argData,"\","&quot;")
	argData = Replace(argData,"<","&lt;")
	argData = Replace(argData,">","&gt;")
	argData = Replace(argData,"'","&#39;")
	argData = Replace(argData,"""","&#34;")

    fInject = argData
End Function 
%>
