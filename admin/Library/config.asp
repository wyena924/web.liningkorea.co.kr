<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual="/admin/Library/dbcon.asp"-->
<!--#include virtual="/admin/Library/common_function.asp"-->
<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=utf-8"
Response.CodePage = "65001"
Response.CharSet = "utf-8"
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1


GLOBAL_VAR_GRP1     					  =	"77"
GLOBAL_VAR_GRP2     					  =	"113"
GLOBAL_OR_WRITE_ID						  = "mall_b2bc"
Const GLOBAL_VAR_ONLINECD				  =	"1128"


' 암호화
Function encode(str, chipVal)
  Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
  
  chipVal = CInt(chipVal)
  str = StringToHex(str)
  For i = 0 To Len(str) - 1
    TempChar = Mid(str, i + 1, 1)
    Conv = InStr(Ref, TempChar) - 1
    Cipher = Conv Xor chipVal
    Cipher = Mid(Ref, Cipher + 1, 1)
    Temp = Temp + Cipher
  Next

	encode = Temp
  'encode = replace(replace(replace(replace(replace(Temp,"C","!)"),"F","@("),"N","#*"),"1","$&"),"7","%^")
End Function

' 복호화
Function decode(str, chipVal)	      
	Dim Temp, TempChar, Conv, Cipher, i: Temp = ""

	'str = replace(replace(replace(replace(replace(str,"!)","C"),"@(","F"),"#*","N"),"$&","1"),"%^","7")

  chipVal = CInt(chipVal)
  For i = 0 To Len(str) - 1
    TempChar = Mid(str, i + 1, 1)
    Conv = InStr(Ref, TempChar) - 1
    Cipher = Conv Xor chipVal
    Cipher = Mid(Ref, Cipher + 1, 1)
    Temp = Temp + Cipher
  Next
  Temp = HexToString(Temp)
  decode = Temp
End Function

' 문자열 -> 16진수
Function StringToHex(pStr)
  Dim i, one_hex, retVal
  For i = 1 To Len(pStr)
    one_hex = Hex(Asc(Mid(pStr, i, 1)))
    retVal = retVal & one_hex
  Next
  StringToHex = retVal
End Function

' 16진수 -> 문자열
Function HexToString(pHex)
  Dim one_hex, tmp_hex, i, retVal
  For i = 1 To Len(pHex)
    one_hex = Mid(pHex, i, 1)
    If IsNumeric(one_hex) Then
            tmp_hex = Mid(pHex, i, 2)
            i = i + 1
    Else
            tmp_hex = Mid(pHex, i, 4)
            i = i + 3
    End If
    retVal = retVal & Chr("&H" & tmp_hex)
  Next
  HexToString = retVal
End Function	


Function GetsLOGINID()
	GetsLOGINID = Request.Cookies("widline_admin_login")("AGT_ID")
End Function

Function GetsCUSTNM()
	GetsCUSTNM  = Request.Cookies("widline_admin_login")("AGT_NM")
End Function


Function GetsSeq()
	ORDER_SEQ	= Request.Cookies("widline_admin_login")("ORDER_SEQ")
End Function	



'전화번호에 하이픈(-) 추가 함수


'<script src="/Script/jquery-1.12.4.js"></script>
'<script src="/Script/jquery-ui.js"></script>
'<script src="/Script/jquery.form.js"></script>

'<script type="text/javascript" src="/script/common.js?ver=8"></script>
'<script type="text/javascript" src="/script/layerpopup.js?ver=5"></script>
'<script type="text/javascript" src="/script/msg.js?ver=4"></script'>
'<script type="text/javascript" src="/script/input.js?ver=4"></script>
'<script type="text/javascript" src="/script/inputck.js?ver=4"></script>
'<script type="text/javascript" src="/script/error.js?ver=4"></script>
'<script type="text/javascript" src="/Script/jquery.tablesorter/jquery.tablesorter.js?ver=4"></script> 

Function addHyphen(fmemtel)

	Select Case Len(fmemtel)

    Case 8    '1588-xxxx

t1 = Mid(fmemtel,1,4)

t2 = Mid(fmemtel,5,4)

response.write t1 & "-" &t2

		Case 9	'02-xxx-xxxx

			t1 = Mid(fmemtel,1,2)

			t2 = Mid(fmemtel,3,3)

			t3 = Mid(fmemtel,6,4)

			response.write t1 & "-" &t2 & "-" &t3

		Case 10	'휴대전화 010-xxx-xxxx

			If Mid(fmemtel,1,2) = "01" Then	'휴대전화 010-xxx-xxxx

				t1 = Mid(fmemtel,1,3)

				t2 = Mid(fmemtel,4,3)

				t3 = Mid(fmemtel,7,4)

				response.write t1 & "-" &t2 & "-" &t3

			Else	'일반전화

				If Mid(fmemtel,1,2) = "02" Then

					t1 = Mid(fmemtel,1,2)

					t2 = Mid(fmemtel,3,4)

					t3 = Mid(fmemtel,7,4)

					response.write t1 & "-" &t2 & "-" &t3

				Else

					t1 = Mid(fmemtel,1,3)

					t2 = Mid(fmemtel,4,3)

					t3 = Mid(fmemtel,7,4)

					response.write t1 & "-" &t2 & "-" &t3

				End If

			End If

		Case 11	'xxx-xxxx-xxxx(휴대전화,070)

			t1 = Mid(fmemtel,1,3)

			t2 = Mid(fmemtel,4,4)

			t3 = Mid(fmemtel,8,4)

			response.write t1 & "-" &t2 & "-" &t3




		Case Else

		response.write fmemtel

	End Select

End Function

%>


