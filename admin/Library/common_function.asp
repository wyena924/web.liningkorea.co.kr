<%
'=========================================================================================================================================================
'============================================================* 금일 날짜   ===========================================================================
'=========================================================================================================================================================
	NowDT = Year(now)&"."&AddZero(Month(now))&"."&AddZero(Day(now))'주문일자 예)2015.06.09
	NowDT2 = Year(now)&AddZero(Month(now))&AddZero(Day(now))'주문일자 예)20150609
	NowDTQuery = Year(now)&"-"&AddZero(Month(now))&"-"&AddZero(Day(now))'주문일자 예)2015.06.09
'=========================================================================================================================================================
'============================================================* 금일 날짜   ===========================================================================
'=========================================================================================================================================================
weekDate = weekDay(now())
Select Case weekDate
	Case "1" weekDate = "일요일"
	Case "2" weekDate = "월요일"
	Case "3" weekDate = "화요일"
	Case "4" weekDate = "수요일"
	Case "5" weekDate = "목요일"
	Case "6" weekDate = "금요일"
	Case "7" weekDate = "토요일"
End Select
NowWeek = weekDate

'요일구하기
Function WeekNm(InputDt)
	Select Case InputDt
		Case "1" WeekNm = "(일)"
		Case "2" WeekNm = "(월)"
		Case "3" WeekNm = "(화)"
		Case "4" WeekNm = "(수)"
		Case "5" WeekNm = "(목)"
		Case "6" WeekNm = "(금)"
		Case "7" WeekNm = "(토)"
	End Select
End Function 

'=========================================================================================================================================================
'============================================================* 한자리수 숫자 0붙이기  ===========================================================================
'=========================================================================================================================================================
Function AddZero(Str)
 IF len(Str)=1 Then
  AddZero="0"&Str
 Else
  AddZero=Str
 End IF
End Function

'=========================================================================================================================================================
'============================================================* 한자리수 숫자 0붙이기  ====================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================* 입금확인발주 카운트  =============================================================================
'=========================================================================================================================================================
Function State1(Mall_Code)
	CntSQL = "SELECT Count(SEQ) AS Cnt FROM IC_T_ORDER_GD WHERE OR_ORDER_PE='"&Mall_Code&"' AND DEL_YN='N' AND OR_KIND_TP='01' AND OR_STATE='00'"
	Set CntRs = Dbcon.Execute(CntSQL)
	State1 = CntRs("Cnt")
	CntRs.close
	set CntRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 입금확인발주 카운트  =============================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================* 상품준비중발주 카운트  ============================================================================
'=========================================================================================================================================================
Function State2(Mall_Code)
	CntSQL = "SELECT Count(SEQ) AS Cnt FROM IC_T_ORDER_GD WHERE OR_ORDER_PE='"&Mall_Code&"' AND DEL_YN='N' AND OR_KIND_TP='01' AND OR_STATE='01'"
	Set CntRs = Dbcon.Execute(CntSQL)
	State2 = CntRs("Cnt")
	CntRs.close
	set CntRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 상품준비중발주 카운트  ============================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================* 발송완료 발주 카운트  ============================================================================
'=========================================================================================================================================================
Function State3(Mall_Code,SDate,EDate)
	CntSQL = "SELECT Count(SEQ) AS Cnt FROM IC_T_ORDER_GD WHERE OR_ORDER_PE='"&Mall_Code&"' AND DEL_YN='N' AND OR_STATE='02' AND (OR_IN_END_DT>='"&SDate&"' AND OR_IN_END_DT<='"&EDate&"') AND OR_KIND_TP='01'"
	Set CntRs = Dbcon.Execute(CntSQL)
	State3 = CntRs("Cnt")
	CntRs.close
	set CntRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 발송완료 발주 카운트  ============================================================================
'=========================================================================================================================================================

'=========================================================================================================================================================
'============================================================* 발송완료 발주 카운트  ============================================================================
'=========================================================================================================================================================
Function State4(Mall_Code,InputDate)
	CntSQL = "SELECT Count(SEQ) AS Cnt FROM IC_T_ORDER_GD WHERE OR_ORDER_PE='"&Mall_Code&"' AND DEL_YN='N' AND OR_KIND_TP='01' AND OR_STATE='02' AND OR_DT='"&InputDate&"'"
	Set CntRs = Dbcon.Execute(CntSQL)
	State4 = CntRs("Cnt")
	CntRs.close
	set CntRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 발송완료 발주 카운트  ========================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================* 오늘 등록 게시글 전체 카운트  ================================================================
'=========================================================================================================================================================
Function BBSAll(Online_Cd)
		BBSAllSQL = "Select Count(B_Idx) AS Cnt from IC_T_ONLINE_BOARD WHERE Online_CD='"&Online_Cd&"' AND B_Up_IDX=0 and CONVERT(varchar(10),b_regdate,102)='"&NowDT&"' and B_Type<>'notice'"
		Set BBsAllRs = Dbcon.Execute(BBSAllSQL)
	  BBSAll = BBsAllRs("Cnt")
	  BBsAllRs.close
		set BBsAllRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 등록 게시글 전체 카운트  ================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================* 오늘 답변 등록 게시글 전체 카운트  ===================================================================
'=========================================================================================================================================================
Function BBSReply(Online_Cd)
		BBSReplySQL = "Select Count(B_Idx) AS Cnt from IC_T_ONLINE_BOARD WHERE Online_CD='"&Online_Cd&"' AND B_Up_IDX<>0 and CONVERT(varchar(10),b_regdate,102)='"&NowDT&"' and B_Type<>'notice'"
		Set BBSReplyRs = Dbcon.Execute(BBSReplySQL)
	  BBSReply = BBSReplyRs("Cnt")
	  BBSReplyRs.close
		set BBSReplyRs=nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 총주문금액              ===================================================================
'=========================================================================================================================================================
Function NowOrder(Mall_Code)
	SQL = "SELECT ISNULL(Sum(OGS.GD_Price*OGS.GD_VOL),0) AS NowOrder FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_GD_SUB OGS	ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OR_DT='"&NowDT2&"'  AND OG.DEL_YN='N' ANd OGS.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	NowOrder = Rs("NowOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 총주문금액 카운트  ===================================================================
'=========================================================================================================================================================
Function NowOrder_Cnt(Mall_Code)
	SQL = "SELECT Count(SEQ) AS NowOrder_Cnt FROM IC_T_ORDER_GD "
	SQL = SQL&" WHERE OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' And OR_DT='"&NowDT2&"'  AND DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	NowOrder_Cnt = Rs("NowOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 일주일 총주문금액       ===================================================================
'=========================================================================================================================================================
Function WeekOrder(Mall_Code)
	SQL = "SELECT ISNULL(Sum(OGS.GD_Price*OGS.GD_VOL),0) AS WeekOrder FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_GD_SUB OGS	ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' AND OR_DT >= convert(varchar(8),dateadd(d,-6,GETDATE()),112) AND OR_DT<='"&NowDT2&"'  AND OG.DEL_YN='N' AND OGS.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	WeekOrder = Rs("WeekOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 일주일 총주문금액 카운트  ===================================================================
'=========================================================================================================================================================
Function WeekOrder_Cnt(Mall_Code)
	SQL = "SELECT Count(SEQ) AS WeekOrder_Cnt FROM IC_T_ORDER_GD "
	SQL = SQL&" WHERE OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' AND OR_DT >= convert(varchar(8),dateadd(d,-6,GETDATE()),112) AND OR_DT<='"&NowDT2&"'  AND DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	WeekOrder_Cnt = Rs("WeekOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 이번달 총주문금액       ===================================================================
'=========================================================================================================================================================
Function MonthOrder(Mall_Code)
	SQL = "SELECT ISNULL(Sum(OGS.GD_Price*OGS.GD_VOL),0) AS MonthOrder FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_GD_SUB OGS	ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OR_DT >= '"&Left(NowDT2,6)&"01' AND OR_DT<='"&NowDT2&"'  AND OG.DEL_YN='N' AND OGS.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	MonthOrder = Rs("MonthOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 이번달 총주문금액 카운트  ===================================================================
'=========================================================================================================================================================
Function MonthOrder_Cnt(Mall_Code)
	SQL = "SELECT Count(SEQ) AS MonthOrder_Cnt FROM IC_T_ORDER_GD "
	SQL = SQL&" WHERE OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' AND OR_DT >= '"&Left(NowDT2,6)&"01' AND OR_DT<='"&NowDT2&"'  AND DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	MonthOrder_Cnt = Rs("MonthOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 결제완료 총 금액          ===================================================================
'=========================================================================================================================================================
Function NowPayOrder(Mall_Code)	
	SQL = "SELECT ISNULL(Sum(OP.PY_IN_PAY),0) AS NowPayOrder FROM IC_T_ORDER_GD OG "
'	SQL = SQL&" JOIN IC_T_ORDER_GD_SUB OGS "
'	SQL = SQL&" ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" JOIN IC_T_ORDER_PAY OP ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OG.OR_DT='"&NowDT2&"'"
'	Response.Write SQL
	Set Rs = Dbcon.Execute(SQL)
	NowPayOrder = Rs("NowPayOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 결제완료 카운트    ===================================================================
'=========================================================================================================================================================
Function NowPayOrder_Cnt(Mall_Code)	
	SQL = "SELECT COUNT(OP.PY_IN_PAY) AS NowPayOrder_Cnt "
	SQL = SQL&" FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_PAY OP "
	SQL = SQL&" ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OG.OR_DT='"&NowDT2&"' AND OP.DEL_YN='N' AND OG.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	NowPayOrder_Cnt = Rs("NowPayOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 일주일 결제완료 총 금액     ===============================================================
'=========================================================================================================================================================
Function WeekPayOrder(Mall_Code)	
	SQL = "SELECT ISNULL(Sum(OP.PY_IN_PAY),0) AS WeekPayOrder FROM IC_T_ORDER_GD OG "
'	SQL = SQL&" JOIN IC_T_ORDER_GD_SUB OGS "
'	SQL = SQL&" ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" JOIN IC_T_ORDER_PAY OP ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' AND OR_DT >= convert(varchar(8),dateadd(d,-6,GETDATE()),112) AND OR_DT<='"&NowDT2&"' AND OG.DEL_YN='N' AND OP.DEL_YN='N'"
'	Response.Write SQL
	Set Rs = Dbcon.Execute(SQL)
	WeekPayOrder = Rs("WeekPayOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 일주일 결제완료 카운트  ===================================================================
'=========================================================================================================================================================
Function WeekPayOrder_Cnt(Mall_Code)	
	SQL = "SELECT COUNT(OP.PY_IN_PAY) AS WeekPayOrder_Cnt "
	SQL = SQL&" FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_PAY OP "
	SQL = SQL&" ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OR_DT >= convert(varchar(8),dateadd(d,-6,GETDATE()),112) AND OR_DT<='"&NowDT2&"' AND OG.DEL_YN='N' AND OP.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	WeekPayOrder_Cnt = Rs("WeekPayOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 이번달 결제완료 총 금액     ===============================================================
'=========================================================================================================================================================
Function MonthPayOrder(Mall_Code)	
	SQL = "SELECT ISNULL(Sum(OP.PY_IN_PAY),0) AS MonthPayOrder FROM IC_T_ORDER_GD OG "
	SQL = SQL&" JOIN IC_T_ORDER_PAY OP ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OR_KIND_TP='01' AND OG.OR_DT >= '"&Left(NowDT2,6)&"01' AND OG.OR_DT<='"&NowDT2&"' AND OG.DEL_YN='N' AND OP.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	MonthPayOrder = Rs("MonthPayOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 이번달 결제완료 카운트  ===================================================================
'=========================================================================================================================================================
Function MonthPayOrder_Cnt(Mall_Code)	
	SQL = "SELECT COUNT(OP.PY_IN_PAY) AS MonthPayOrder_Cnt "
	SQL = SQL&" FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_PAY OP "
	SQL = SQL&" ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OG.OR_KIND_TP='01' AND OG.OR_DT >= '"&Left(NowDT2,6)&"01' AND OG.OR_DT<='"&NowDT2&"' AND OG.DEL_YN='N' AND OP.DEL_YN='N'"
	Set Rs = Dbcon.Execute(SQL)
	MonthPayOrder_Cnt = Rs("MonthPayOrder_Cnt")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 취소반품금액 총 금액          ===================================================================
'=========================================================================================================================================================
Function NowCancelOrder(Mall_Code)	
	SQL = "SELECT ISNULL(Sum(OP.PY_IN_PAY),0) AS NowCancelOrder FROM IC_T_ORDER_GD OG JOIN IC_T_ORDER_GD_SUB OGS "
	SQL = SQL&" ON OG.SEQ = OGS.OR_SEQ "
	SQL = SQL&" JOIN IC_T_ORDER_PAY OP ON OG.SEQ = OP.ORDER_GD_SEQ "
	SQL = SQL&" WHERE OG.OR_ORDER_PE='"&Mall_Code&"' AND OR_IN_TP='02' AND OR_DT='"&NowDT2&"'"
	Set Rs = Dbcon.Execute(SQL)
	NowCancelOrder = Rs("NowCancelOrder")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 신규회원가입 현황  ===========================================================================
'=========================================================================================================================================================
Function NowJoin(Mall_Code)
	SQL = "SELECT COUNT(Order_Seq) AS NowJoin FROM IC_T_ORDER_CUST WHERE gl_pe='"&Mall_Code&"' AND order_dt='"&NowDT2&"' AND del_yn='N'"
	Set Rs = Dbcon.Execute(SQL)
	NowJoin = Rs("NowJoin")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 탈퇴신청 현황  ===========================================================================
'=========================================================================================================================================================
Function NowDrop(Mall_Code)
	SQL = "SELECT COUNT(IDX) AS NowDrop FROM IC_T_ONLINE_Drop_Member WHERE Mall_Code='"&Mall_Code&"' AND DT='"&NowDT2&"'"
	Set Rs = Dbcon.Execute(SQL)
	NowDrop = Rs("NowDrop")
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* 오늘 UV 현황  ===========================================================================
'=========================================================================================================================================================
Function NowUV(Mall_Code,DT)
	SQL = "SELECT Count(Distinct(Cust_SEQ)) AS Cnt FROM IC_T_ONLINE_Login_Log WHERE Mall_Code='"&Mall_Code&"' AND InDate='"&DT&"'"
	Set Rs = Dbcon.Execute(SQL)
	NowUV = Rs("Cnt")
	Rs.close
	set Rs = nothing
End Function 

Function GetUserNM(Cust_Seq)
	SQL = "SELECT Order_Nm FROM IC_T_ORDER_CUST WHERE Order_SEQ='"&Cust_Seq&"'"
	Set Rs = Dbcon.Execute(SQL)
	If Not(Rs.Eof Or Rs.Bof) Then 
		GetUserNM = Rs("Order_Nm")
	Else
		GetUserNM = "회원정보삭제"
	End If 
	Rs.close
	set Rs = nothing
End Function 

Function GetUserID(Cust_Seq)
	SQL = "SELECT Order_Nm FROM IC_T_ORDER_CUST WHERE Order_SEQ='"&Cust_Seq&"'"
	Set Rs = Dbcon.Execute(SQL)
	If Not(Rs.Eof Or Rs.Bof) Then 
		GetUserID = Rs("Order_Nm")
	Else
		GetUserID = "회원정보삭제"
	End If 
	Rs.close
	set Rs = nothing
End Function 

'=========================================================================================================================================================
'============================================================* injection 처리  ===========================================================================
'=========================================================================================================================================================
Function fInject(argData)
 	Dim strCheckArgSQL
	Dim arrSQL
  	Dim i

	strCheckArgSQL = LCase(Trim(argData))	
	
	arrSQL = Array("exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt","</div>")

	For i=0 To ubound(arrSQL) Step 1
		If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

		If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

	Next

	'Xss 필터링	
	'argData = Replace(argData,"&","&amp;")
	argData = Replace(argData,"\","&quot;")
	argData = Replace(argData,"<","&lt;")
	argData = Replace(argData,">","&gt;")
	argData = Replace(argData,"'","&#39;")
	argData = Replace(argData,"""","&#34;")

    fInject = argData
End Function 
%>
