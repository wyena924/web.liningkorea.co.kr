<!--METADATA TYPE= "typelib"  NAME= "ADODB Type Library" FILE="C:\Program Files\Common Files\SYSTEM\ADO\msado15.dll"  -->
<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual="/Library/db_helper.asp"-->

<!--#include virtual="/static/static_erp.asp"-->
<!--#include virtual="/static/static_visit_erp.asp"-->

<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=utf-8"
Response.CodePage = "65001"
Response.CharSet = "utf-8"
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

'암호화 모듈-----------------------------------------------------------------------------
	set crypt = Server.CreateObject("Chilkat_9_5_0.Crypt2")
	success = crypt.UnlockComponent("YJKMKR.CB10118_vNZ9zq4wnw7P")
	crypt.CryptAlgorithm = "aes"

	'CipherMode may be "ecb", "cbc", "ofb", "cfb", "gcm", etc.
	crypt.CipherMode = "cbc"

	'KeyLength may be 128, 192, 256
	crypt.KeyLength = 256

	crypt.PaddingScheme = 0
	crypt.EncodingMode = "hex"
	  
	ivHex = "000167856675020A506Y0708090R7YA"
	crypt.SetEncodedIV ivHex,"hex"

	keyHex = "000167856675020A506Y0708090R7YA101411A2131D6415K16171H8191A"
	crypt.SetEncodedKey keyHex,"hex"
'----------------------------------------------------------------------------------------------

Const Ref = "GPQRSATWXVYBCHL640MN598OIJKZ12D7EF3U"

Function URLDecodeUTF8(byVal str)
	Dim B,ub
    Dim UtfB
    Dim UtfB1, UtfB2, UtfB3
    Dim i, n, s
    n=0
    ub=0
    For i = 1 To Len(str)
        B=Mid(str, i, 1)
        Select Case B
            Case "+"
                s=s & " "
            Case "%"
                ub=Mid(str, i + 1, 2)
                UtfB = CInt("&H" & ub)
                If UtfB<128 Then 
                    i=i+2
                    s=s & ChrW(UtfB)
                Else 
                    UtfB1=(UtfB And &H0F) * &H1000 
                    UtfB2=(CInt("&H" & Mid(str, i + 4, 2)) And &H3F) * &H40
                    UtfB3=CInt("&H" & Mid(str, i + 7, 2)) And &H3F
                    s=s & ChrW(UtfB1 Or UtfB2 Or UtfB3)
                    i=i+8
                End If 
            Case Else
                s=s & B
        End Select 
    Next
  	URLDecodeUTF8 = s
End Function

' 암호화
Function encode(str, chipVal)
	Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
	
	chipVal = CInt(chipVal)
	str = StringToHex(str)
	For i = 0 To Len(str) - 1
	  TempChar = Mid(str, i + 1, 1)
	  Conv = InStr(Ref, TempChar) - 1
	  Cipher = Conv Xor chipVal
	  Cipher = Mid(Ref, Cipher + 1, 1)
	  Temp = Temp + Cipher
	Next        
	encode = Temp
	'encode = replace(replace(replace(replace(replace(Temp,"C","!)"),"F","@("),"N","#*"),"1","$&"),"7","%^")
End Function

' 복호화
Function decode(str, chipVal)	      
	Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
	
	'str = replace(replace(replace(replace(replace(str,"!)","C"),"@(","F"),"#*","N"),"$&","1"),"%^","7")
	
	    chipVal = CInt(chipVal)
	    For i = 0 To Len(str) - 1
	      TempChar = Mid(str, i + 1, 1)
	      Conv = InStr(Ref, TempChar) - 1
	      Cipher = Conv Xor chipVal
	      Cipher = Mid(Ref, Cipher + 1, 1)
	      Temp = Temp + Cipher
	    Next
	    Temp = HexToString(Temp)
	    decode = Temp
End Function

' 문자열 -> 16진수
Function StringToHex(pStr)
  Dim i, one_hex, retVal
  For i = 1 To Len(pStr)
    one_hex = Hex(Asc(Mid(pStr, i, 1)))
    retVal = retVal & one_hex
  Next
  StringToHex = retVal
End Function

' 16진수 -> 문자열
Function HexToString(pHex)
  Dim one_hex, tmp_hex, i, retVal
  For i = 1 To Len(pHex)
    one_hex = Mid(pHex, i, 1)
    If IsNumeric(one_hex) Then
            tmp_hex = Mid(pHex, i, 2)
            i = i + 1
    Else
            tmp_hex = Mid(pHex, i, 4)
            i = i + 3
    End If
    retVal = retVal & Chr("&H" & tmp_hex)
  Next
  HexToString = retVal
End Function	
	
Function fInject(argData)
 	Dim strCheckArgSQL
	Dim arrSQL
  Dim i

	strCheckArgSQL = LCase(Trim(argData))	
	
	arrSQL = Array("exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt","</div>")

	For i=0 To ubound(arrSQL) Step 1
		If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
			Select Case  arrSQL(i)
		  	Case "'"
					arrSQL(i) ="홑따옴표"
		  	Case "char("
				arrSQL(i) ="char"
		  End SELECT
		  
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

		If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

	Next

	'Xss 필터링	
	argData = Replace(argData,"&","&amp;")
	argData = Replace(argData,"\","&quot;")
	argData = Replace(argData,"<","&lt;")
	argData = Replace(argData,">","&gt;")
	argData = Replace(argData,"'","&#39;")
	argData = Replace(argData,"""","&#34;")

    fInject = argData
End Function 

Function fInject2(argData)
 	Dim strCheckArgSQL
	Dim arrSQL
  Dim i

	strCheckArgSQL = LCase(Trim(argData))	
	
	arrSQL = Array("exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt","</div>")

	For i=0 To ubound(arrSQL) Step 1
		If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
			Select Case  arrSQL(i)
		  	Case "'"
					arrSQL(i) ="홑따옴표"
		  	Case "char("
				arrSQL(i) ="char"
		  End SELECT
		  
			response.write "허용되지 않은 문자열이 있습니다."
			response.end
		End If

		If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "허용되지 않은 문자열이 있습니다."
			response.end
		End If

	Next

	'Xss 필터링	
	argData = Replace(argData,"&","&amp;")
	argData = Replace(argData,"\","&quot;")
	argData = Replace(argData,"<","&lt;")
	argData = Replace(argData,">","&gt;")
	argData = Replace(argData,"'","&#39;")
	argData = Replace(argData,"""","&#34;")

    fInject2 = argData
End Function 

Function StrConvert(StrConData)
	StrConvert = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(StrConData, vbNewLine, "ㆍ"), "'", "＇"), "<", "＜"), ">", "＞"), ",", "－"), "&", "＆"), """", "¨"), "|", " | "),".","ㄱ")
End Function

Function xsvi(argData)
	argData = Replace(argData,"&amp;","&")
	argData = Replace(argData,"&quot;","\")
	argData = Replace(argData,"&lt;","<")
	argData = Replace(argData,"&gt;",">")
	argData = Replace(argData,"&#39;","'")
	argData = Replace(argData,"&#34;","""")
	
	xsvi = argData
End Function

	
	'로그인체크 (개선)
Function ChkblemrsAjaxLogin()	
	Dim rs,SQL,db

	'로그인 만료시 사용이 불가능
	if trim(Request.Cookies("blemrsAgtID")) = "" then				 
		Response.write "mulogin"	
		Response.end
	end if
	
	'보안을 위하여 로그인시 생성된 KEY를 확인한다
	Set db = new clsDBHelper
	
	SQL = "SELECT COUNT(1) CNT "
	SQL = SQL & " FROM BLEMRS.DBO.T_AGENT_INFO "
	SQL = SQL & " WHERE DEL_YN = 'N' "	
	SQL = SQL & " AND AGT_ID = '" & trim(crypt.DecryptStringENC(Request.Cookies("blemrsAgtID"))) & "' "
	SQL = SQL & " AND AGT_KEY = '" & trim(crypt.DecryptStringENC(Request.Cookies("blemrsagtkey"))) & "' "
	Set rs = db.ExecSQLReturnRS(SQL , null, ConStr)

	ckagtcnt2 = rs("CNT")
	rs.close
	set rs = Nothing
	Call db.Dispose()
	Set rs = Nothing
	Set db = Nothing
	
	if trim(ckagtcnt2) = 0  or trim(ckagtcnt2) = "0" then
		response.write "mulogin"
		response.end
	end if
end function		

Function GetUniName(byRef strFileName, DirectoryPath)
    Dim strName, strExt
    strName = Mid(strFileName, 1, InstrRev(strFileName, ".") - 1) ' 확장자를 제외한 파일명을 얻는다.
    strExt = Mid(strFileName, InstrRev(strFileName, ".") + 1) '확장자를 얻는다
    Dim fso
    Set fso = Server.CreateObject("Scripting.FileSystemObject")
    Dim bExist : bExist = True
    '우선 같은이름의 파일이 존재한다고 가정
    Dim strFileWholePath : strFileWholePath = DirectoryPath & "\" & strName & "." & strExt
    'Dim strFileWholePath : strFileWholePath = strName & "." & strExt
    '저장할 파일의 완전한 이름(완전한 물리적인 경로) 구성
    Dim countFileName : countFileName = 0
    '파일이 존재할 경우, 이름 뒤에 붙일 숫자를 세팅함.
    Do While bExist ' 우선 있다고 생각함.
        If (fso.FileExists(strFileWholePath)) Then ' 같은 이름의 파일이 있을 때
            countFileName = countFileName + 1 '파일명에 숫자를 붙인 새로운 파일 이름 생성
            strFileName = strName & "(" & countFileName & ")." & strExt
            strFileWholePath = DirectoryPath & "\" & strFileName
            'strFileWholePath = strFileName
        Else
            bExist = False
        End If
    Loop
    GetUniName = strFileWholePath
End Function

'strFileWholePath1 = GetUniName(strFileName1, DirectoryPath)

'#####################동일한 이름이 있을때 (1)을 붙인다#######################

'-------------파일명 추출
function FilePathName(str)
	tmp_ary3 = split(str,"\")
	if isArray(tmp_ary3) then
		len99 = ubound(tmp_ary3)
		FilePathName = tmp_ary3(len99)
	end if
end function

'------------------------
%>

<%
	'Dbopen()
%>