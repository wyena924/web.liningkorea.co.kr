<%
    '/////////////////////////////////////////////////////////////////////////////////
	Sub GoBack(msg)
		%>
		<script language="javascript">
		<!--
			alert("<%=msg%>");
			history.back();
		//-->
		</script>
		<%
		response.end
	End Sub

	'/////////////////////////////////////////////////////////////////////////////////
	Sub PageBack()
		%>
		<script language="javascript">
		<!--
			history.back();
		//-->
		</script>
		<%
		response.end
	End Sub

	'/////////////////////////////////////////////////////////////////////////////////
	Sub PrintMsg(msg)
		%>
		<script language="javascript">
		<!--
			alert("<%=msg%>");
		//-->
		</script>
		<%
	End Sub

	'/////////////////////////////////////////////////////////////////////////////////
	Sub WindowClose()
		%>
		<script language="javascript">
		<!--
			window.close();
		//-->
		</script>
		<%
		response.end
	End Sub

	'/////////////////////////////////////////////////////////////////////////////////
	Sub GoUrl(url)
		response.write "<meta http-equiv='refresh' content='0;url="+url+"'>"
		response.end
	End Sub

    '/////////////////////////////////////////////////////////////////////////////////
	'// 제로체크 ==> data : 원본데이터 , chknum : 체크할개수
	Function ZeroCheck(data,chknum)
		dim temp,temp_len,i
		temp=""
		temp_len=len(data)
		if temp_len < chknum then
			for i=1 to chknum-temp_len
				temp=temp+"0"
			next
			temp=temp+CStr(data)
		else
			temp=CStr(data)
		end if
		ZeroCheck=temp
	End Function

    '/////////////////////////////////////////////////////////////////////////////////
	'//	리스트 페이징
	Dim blockpage

	Sub Pageing(filename)
		blockpage = Int((page - 1) / 10) * 10 + 1

		response.write "<ul class=""pagination"">"

		If blockpage = 1 then
			response.write "<li class=""page-item""><a href=""#"" class=""page-link"">이전</a></li>"
		Else
			response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage - 1 & """ class=""page-link"">이전</a></li>"
		End if

		i = 1
		Do Until i > 10 or blockpage > total_page
			if blockpage = int(page) then
				response.write "<li class=""page-item active""><a href=""#"" class=""page-link"">" & blockpage & "</a></li>"
			Else
				response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage & """ class=""page-link"">" & blockpage & "</a></li>"
			End if

			blockpage = blockpage + 1
			i = i + 1
		Loop

		If blockpage > total_page then
			response.write "<li class=""page-item""><a href=""#"" class=""page-link"">다음</a></li>"
		Else
			response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage & """ class=""page-link"">다음</a></li>"
		End if

		response.write "</ul>"
	End Sub
%>
