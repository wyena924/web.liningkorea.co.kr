<% @CODEPAGE="65001" language="vbscript" %>

<!--#include virtual="/Library/db_helper.asp"-->
<!--#include virtual="/Library/common_function.asp"-->

<!--#include virtual="/static/static_erp.asp"-->
<!--#include virtual="/static/static_visit_erp.asp"-->


<%
Response.ContentType = "text/html"
Response.AddHeader "Content-Type", "text/html;charset=utf-8"
Response.CodePage = "65001"
Response.CharSet = "utf-8"
Response.CacheControl = "no-cache"
Response.AddHeader "Pragma", "no-cache"
Response.Expires = -1

'암호화 모듈-----------------------------------------------------------------------------
set crypt = Server.CreateObject("Chilkat_9_5_0.Crypt2")
success = crypt.UnlockComponent("YJKMKR.CB10118_vNZ9zq4wnw7P")
crypt.CryptAlgorithm = "aes"

'CipherMode may be "ecb", "cbc", "ofb", "cfb", "gcm", etc.
crypt.CipherMode = "cbc"

'KeyLength may be 128, 192, 256
crypt.KeyLength = 256

crypt.PaddingScheme = 0
crypt.EncodingMode = "hex"
  
ivHex = "000167856675020A506Y0708090R7YA"
crypt.SetEncodedIV ivHex,"hex"

keyHex = "000167856675020A506Y0708090R7YA101411A2131D6415K16171H8191A"
crypt.SetEncodedKey keyHex,"hex"
'----------------------------------------------------------------------------------------------
		

Function URLDecode(Expression)
	Dim strSource, strTemp, strResult, strchr
 	Dim lngPos, AddNum, IFKor
	strSource = Replace(Expression, "+", " ")
	For lngPos = 1 To Len(strSource)
		AddNum = 2
	  	strTemp = Mid(strSource, lngPos, 1)
	  	If strTemp = "%" Then
	   		If lngPos + AddNum < Len(strSource) + 1 Then
	    		strchr = CInt("&H" & Mid(strSource, lngPos + 1, AddNum))
	    		If strchr > 130 Then 
	     			AddNum = 5
	     			IFKor  = Mid(strSource, lngPos + 1, AddNum)
	     			IFKor  = Replace(IFKor, "%", "")
	     			strchr = CInt("&H" & IFKor )
	    		End If
	    		strResult = strResult & Chr(strchr)
	    		lngPos = lngPos + AddNum
	   		End If
		Else
			strResult = strResult & strTemp
		End If
	Next
	URLDecode = strResult
End Function  

Function URLDecodeUTF8(byVal str)
	Dim B,ub
    Dim UtfB
    Dim UtfB1, UtfB2, UtfB3
    Dim i, n, s
    n=0
    ub=0
    For i = 1 To Len(str)
        B=Mid(str, i, 1)
        Select Case B
            Case "+"
                s=s & " "
            Case "%"
                ub=Mid(str, i + 1, 2)
                UtfB = CInt("&H" & ub)
                If UtfB<128 Then 
                    i=i+2
                    s=s & ChrW(UtfB)
                Else 
                    UtfB1=(UtfB And &H0F) * &H1000 
                    UtfB2=(CInt("&H" & Mid(str, i + 4, 2)) And &H3F) * &H40
                    UtfB3=CInt("&H" & Mid(str, i + 7, 2)) And &H3F
                    s=s & ChrW(UtfB1 Or UtfB2 Or UtfB3)
                    i=i+8
                End If 
            Case Else
                s=s & B
        End Select 
    Next
  	URLDecodeUTF8 = s
End Function

Const Ref = "GPQRSATWXVYBCHL640MN598OIJKZ12D7EF3U"

' 암호화
Function encode(str, chipVal)
  Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
  
  chipVal = CInt(chipVal)
  str = StringToHex(str)
  For i = 0 To Len(str) - 1
    TempChar = Mid(str, i + 1, 1)
    Conv = InStr(Ref, TempChar) - 1
    Cipher = Conv Xor chipVal
    Cipher = Mid(Ref, Cipher + 1, 1)
    Temp = Temp + Cipher
  Next

	encode = Temp
  'encode = replace(replace(replace(replace(replace(Temp,"C","!)"),"F","@("),"N","#*"),"1","$&"),"7","%^")
End Function

' 복호화
Function decode(str, chipVal)	      
	Dim Temp, TempChar, Conv, Cipher, i: Temp = ""

	'str = replace(replace(replace(replace(replace(str,"!)","C"),"@(","F"),"#*","N"),"$&","1"),"%^","7")

  chipVal = CInt(chipVal)
  For i = 0 To Len(str) - 1
    TempChar = Mid(str, i + 1, 1)
    Conv = InStr(Ref, TempChar) - 1
    Cipher = Conv Xor chipVal
    Cipher = Mid(Ref, Cipher + 1, 1)
    Temp = Temp + Cipher
  Next
  Temp = HexToString(Temp)
  decode = Temp
End Function

' 문자열 -> 16진수
Function StringToHex(pStr)
  Dim i, one_hex, retVal
  For i = 1 To Len(pStr)
    one_hex = Hex(Asc(Mid(pStr, i, 1)))
    retVal = retVal & one_hex
  Next
  StringToHex = retVal
End Function

' 16진수 -> 문자열
Function HexToString(pHex)
  Dim one_hex, tmp_hex, i, retVal
  For i = 1 To Len(pHex)
    one_hex = Mid(pHex, i, 1)
    If IsNumeric(one_hex) Then
            tmp_hex = Mid(pHex, i, 2)
            i = i + 1
    Else
            tmp_hex = Mid(pHex, i, 4)
            i = i + 3
    End If
    retVal = retVal & Chr("&H" & tmp_hex)
  Next
  HexToString = retVal
End Function	

'로그인체크 (개선)
Function ChkblemrsLogin(menusendseq)
	'로그인 만료시 사용이 불가능
	if trim(Request.Cookies("blemrsAgtID")) = "" then		
		url = "/gate.asp"
		response.Redirect url
		response.end
	end if
	
	'로그인 만료시 사용이 불가능
	'보안을 위하여 매번 쿠키에 저장된 아이디와 패스워드를 확인한다
	DBOpen()
	
	ckagtqry = "SELECT COUNT(1) CNT "
	ckagtqry = ckagtqry & " FROM BLEMRS.DBO.T_AGENT_INFO "
	ckagtqry = ckagtqry & " WHERE DEL_YN = 'N' "
	ckagtqry = ckagtqry & " AND AGT_ID = '" & crypt.DecryptStringENC(Request.Cookies("blemrsAgtID")) & "' "
	ckagtqry = ckagtqry & " AND AGT_KEY = '" & crypt.DecryptStringENC(Request.Cookies("blemrsagtkey")) & "' "
	
	if trim(menusendseq) <> "" then
		'화면별 상세 이벤트 권한 설정
		ckrnkqry =  " SELECT RNK_VIEW RNK_VIEW "
		ckrnkqry = ckrnkqry & "     ,RNK_IN RNK_IN "
		ckrnkqry = ckrnkqry & "     ,RNK_UP RNK_UP "
		ckrnkqry = ckrnkqry & "     ,RNK_DEL RNK_DEL "
		ckrnkqry = ckrnkqry & "     ,RNK_SUB RNK_SUB "	
		ckrnkqry = ckrnkqry & "     ,RNK_EXCEL RNK_EXCEL "
		ckrnkqry = ckrnkqry & "     ,RNK_CNCL RNK_CNCL "
		ckrnkqry = ckrnkqry & " FROM BLEMRS.dbo.T_RNK_MENU_MNG "
		ckrnkqry = ckrnkqry & " WHERE DEL_YN = 'N' "
		ckrnkqry = ckrnkqry & " AND RNK_CD =  '" & crypt.DecryptStringENC(Request.Cookies("blemrsagtrk")) & "' "
		ckrnkqry = ckrnkqry & " AND MENU_SEND_SEQ = " & menusendseq & " "
		
		Set ckrnkrs = DBCon.execute(ckrnkqry)
	end if
		
	Set ckagtrs = DBCon.execute(ckagtqry)	
	
	DBClose()
	ckagtcnt = ckagtrs("CNT")
	
	ckagtrs.close
	set ckagtrs = Nothing
	
	if trim(ckagtcnt) = 0 or trim(ckagtcnt) = "0" then
		response.write "<script>location.href='/logout.asp?tp=use2'</script>"
		'url = "/gate.asp?tp=use2" //자바스크립트로 logout.asp 호출방식으로 변경할것
		'response.Redirect url
		response.end
	end if
	
	if trim(menusendseq) <> "" then
		if ckrnkrs.bof and ckrnkrs.eof Then		
			response.cookies("blemrsrnkview") = "Y"
			response.cookies("blemrsrnkin") = "Y"
			response.cookies("blemrsrnkup") = "Y"
			response.cookies("blemrsrnkdel") = "Y"
			response.cookies("blemrsrnksub") = "Y"
			response.cookies("blemrsrnkexcel") = "Y"
			response.cookies("blemrsrnkcncl") = "Y"
	  else		  
			response.cookies("blemrsrnkview") = crypt.EncryptStringENC(ckrnkrs("RNK_VIEW"))
			response.cookies("blemrsrnkin") = crypt.EncryptStringENC(ckrnkrs("RNK_IN"))
			response.cookies("blemrsrnkup") = crypt.EncryptStringENC(ckrnkrs("RNK_UP"))
			response.cookies("blemrsrnkdel") = crypt.EncryptStringENC(ckrnkrs("RNK_DEL"))
			response.cookies("blemrsrnksub") = crypt.EncryptStringENC(ckrnkrs("RNK_SUB"))	
			response.cookies("blemrsrnkexcel") = crypt.EncryptStringENC(ckrnkrs("RNK_EXCEL"))
			response.cookies("blemrsrnkcncl") = crypt.EncryptStringENC(ckrnkrs("RNK_CNCL"))
		end if
		
		ckrnkrs.close
		set ckrnkrs = Nothing
	end if
end Function

'define
JSVER = 1

%>


<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta charset="utf-8">
<title>blue members</title>
<script src="/Script/jquery-1.12.4.js"></script>
<script src="/Script/jquery-ui.js"></script>
<script src="/Script/jquery.form.js"></script>

<script type="text/javascript" src="/script/common.js?ver=8"></script>
<script type="text/javascript" src="/script/layerpopup.js?ver=5"></script>
<script type="text/javascript" src="/script/msg.js?ver=4"></script>
<script type="text/javascript" src="/script/input.js?ver=4"></script>
<script type="text/javascript" src="/script/inputck.js?ver=4"></script>
<script type="text/javascript" src="/script/error.js?ver=4"></script>
<script type="text/javascript" src="/Script/jquery.tablesorter/jquery.tablesorter.js?ver=4"></script> 

