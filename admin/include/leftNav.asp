<%
g_url = Request.ServerVariables("URL")
arr = split(g_url,"/")
path = arr(2)

if path = "main" or path = "deal" then
  menuon = 0
elseif path = "coupon" then
  menuon = 1
elseif path = "customer" then
  menuon = 2
else
  menuon = -1
end if
%>
<div class="left-nav">
  <!-- S: category -->
  <section class="category">
    <!--h2 class="mt-20">ADMIN</h2-->
    <ul class="depth-1">
      <li class="menu">

		<!--a href="#" class="btn menu-item" data-location="design">
          <!-- <span class="img-box mt--2">
            <img src="../../../pmaa/admin/front/imgs/left_menu/icon_people.png" alt width="20">
          </span>>
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">회원</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
		</a>

        <ul class="depth-2">
			<li><a href="<%=global_depth%>/member/member_list.asp" class="btn menu-item" data-location="table">회원리스트</a></li>
        </ul-->
		
		
		<!--a href="#" class="btn menu-item" data-location="design">
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">주문관리</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
		</a>
        <ul class="depth-2">
			<li><a href="<%=global_depth%>/order/order_list.asp" class="btn menu-item" data-location="table">주문리스트(주문별)</a></li>
			<li><a href="<%=global_depth%>/order/order_product_list.asp" class="btn menu-item" data-location="table">주문리스트(상품별)</a></li>
        </ul-->
     
		<a href="#" class="btn menu-item" data-location="design">
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">주문관리</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
		</a>
        <ul class="depth-2">
			<!--li><a href="<%=global_depth%>/order/order_list.asp" class="btn menu-item" data-location="table">주문리스트(주문별)</a></li>
			<li><a href="<%=global_depth%>/order/order_product_list.asp" class="btn menu-item" data-location="table">주문리스트(상품별)</a></li-->
			<li><a href="<%=global_depth%>/order/order_file_list.asp" class="btn menu-item" data-location="table">인증자료리스트</a></li>
        </ul>
     
		
		<a href="#" class="btn menu-item" data-location="design">
          <!-- <span class="img-box mt--2">
            <img src="../../../pmaa/admin/front/imgs/left_menu/icon_people.png" alt width="20">
          </span> -->
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">게시판관리</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
		</a>

		<!-- S: depth-2 -->
        <ul class="depth-2">
			<li><a href="<%=global_depth%>/notice/product_qna_list.asp" class="btn menu-item" data-location="table">상품Q&A 게시판</a></li>
        </ul>

     
		<a href="#" class="btn menu-item" data-location="design">
          <!-- <span class="img-box mt--2">
            <img src="../../../pmaa/admin/front/imgs/left_menu/icon_people.png" alt width="20">
          </span> -->
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">통계</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
		</a>
		<!-- S: depth-2 -->
        <ul class="depth-2">
			<!--li><a href="<%=global_depth%>/state/member_login_state.asp" class="btn menu-item" data-location="table">ID별 로그인통계</a></li-->
			<li><a href="<%=global_depth%>/state/member_email_login_state4.asp" class="btn menu-item" data-location="table">소속회사선택리스트</a></li>
			<li><a href="<%=global_depth%>/state/member_recom_id_cnt_state.asp" class="btn menu-item" data-location="table">추천인아이디순위</a></li>
			<li><a href="<%=global_depth%>/state/member_email_login_state5.asp" class="btn menu-item" data-location="table">인증메일요청리스트</a></li>
			<li><a href="<%=global_depth%>/state/order_code_login_state.asp" class="btn menu-item" data-location="table">업체코드별 로그인횟수</a></li>
			<li><a href="<%=global_depth%>/state/member_email_login_state.asp" class="btn menu-item" data-location="table">Email별 회원가입수</a></li>
			<li><a href="<%=global_depth%>/state/product_view_cnt_state.asp" class="btn menu-item" data-location="table">날짜별 모델 페이지뷰</a></li>
			<li><a href="<%=global_depth%>/state/member_state.asp" class="btn menu-item" data-location="table">회원통계(그래프)</a></li>
			<li><a href="<%=global_depth%>/state/product_view_state.asp" class="btn menu-item" data-location="table">상퓸별 VIEW(그래프)</a></li>
        </ul>
        <!-- E: depth-2 -->
      </li>
      <!--
      <li class="menu">
        <a href="#" class="btn menu-item" data-location="design">
      -->
          <!-- <span class="img-box mt--2">
            <img src="../../../pmaa/admin/front/imgs/left_menu/icon_people.png" alt width="20">
          </span> -->
      <!--
          <span class="ic-deco menu-icon">
            <i class="fas fa-cloud"></i>
          </span>
          <span class="txt">통계관리</span>
          <span class="ic-deco ic-arrow">
            <i class="fa fa-angle-right"></i>
          </span>
        </a>
      -->
        <!-- S: depth-2 -->
      <!--
        <ul class="depth-2">
          <li><a href="javascript:alert('준비중입니다');" class="btn menu-item" data-location="table">통계관리</a></li>
        </ul>
      -->
        <!-- E: depth-2 -->
      <!--
      </li>
      -->
  </section>
  <!-- E: category -->
</div>
<script>
  $(function() {
    var sel = <%= menuon %>;
    $(".menu").each(function(index) {
      if(index == sel) {
        //$(this).find("a").addClass("on");
        //$(this).find(".depth-2").css({ "display" : "block" });
      }
    });
  });
</script>