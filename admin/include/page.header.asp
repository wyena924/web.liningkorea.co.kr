<!-- S: header -->
<div class="header">
  <h1>
    <a href="/admin/customer/notice_list.asp" class="img-box">
      <img src="/admin/front_include/imgs/header/logo_top.png" alt="위드라인">
    </a>
  </h1>

  <!-- S: header-nav -->
  <div class="header-nav">
    <a href="/admin/logout.asp" class="btn">로그아웃</a>
    <!-- S: welcome -->
    <div class="welcome">
      <span class="ic-deco">
        <i class="fas fa-user-circle"></i>
      </span>
      <span class="user-id"><%=GetsCUSTNM()%></span>
      <span class="txt">님 위드라인에 오신 것을 환영합니다!</span>
      <span class="img-box welcome-img">
        <img src="/admin/front/imgs/dummy/welcome_1.gif" alt="랄랄라~">
      </span>
    </div>
    <!-- E: welcome -->
  </div>
  <!-- E: header-nav -->

</div>
<!-- E: header -->