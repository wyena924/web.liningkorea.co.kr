<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
callback = request("CKEditorFuncNum")

Set Upload = Server.CreateObject("TABSUpload4.Upload")
Upload.CodePage = 65001
Upload.MaxBytesToAbort = 10 * 1024 * 1024

root_path = Server.MapPath("/pmaa/admin")
save_path = root_path & "\upload\userfiles\"

'Response.WRITE save_path
'Response.End 
'Upload.Start "c:\Temp"
Upload.Start save_path

Set File = Upload.Form("upload")
	
org_filename = File.FileName
save_filename = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename,3)
ext = LCase(mid(org_filename,InstrRev(org_filename,".")+1))

chkext = array("jpg","jpeg","gif","png")
chk = 0
for i = 0 to UBound(chkext)
  if ext = chkext(i) then
    chk = chk + 1
    exit for
  end if
next

if chk = 0 then
  msg = "JPG, GIF, PNG 이미지 파일만 업로드가 가능합니다."
  %>
  <script type='text/javascript'>
    window.parent.CKEDITOR.tools.callFunction('<%= callback %>','','<%= msg %>');
  </script>
  <%
  response.end
end if
	
File.SaveAs save_path & save_filename , False

Set File = nothing

file_url = "/pmaa/admin/upload/userfiles/" & save_filename
msg = "업로드에 성공하였습니다."
%>
<script type='text/javascript'>
  window.parent.CKEDITOR.tools.callFunction('<%= callback %>','<%= file_url %>','<%= msg %>');
</script>
