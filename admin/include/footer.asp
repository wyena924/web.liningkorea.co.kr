<!--S: custom_scroll -->
<script>
  /**
   * S: perfect-scrollbar 
   */
  const leftNav = document.querySelector(".left-nav");
  const ps_leftNav = new PerfectScrollbar(leftNav, {
    wheelSpeed: 20,
    wheelPropagatio: true,
    minScrollBarLength: 20
  });
  /* E: perfect-scrollbar */

  /**
   * S: mCustomScrollbar
   */
  $(".left-nav").mCustomScrollbar({
    autoHideScrollbar: true,
    theme: "minimal-dark"
  })
  /* E: mCustomScrollbar */
</script>
<!-- E: custom_scroll -->
<!-- 공용 utils.js -->
<script src="<%=virtual_Depth%>/front_include/js/utils.js"></script>
<!-- custom js -->
<script src="<%=global_Depth%>/front/js/main.js"></script>