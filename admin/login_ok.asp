<!--#include virtual="/admin/Library/dbcon.asp"-->
<%

	Response.ContentType = "text/html"
	Response.AddHeader "Content-Type", "text/html;charset=utf-8"
	Response.CodePage = "65001"
	Response.CharSet = "utf-8"
	Response.CacheControl = "no-cache"
	Response.AddHeader "Pragma", "no-cache"
	Response.Expires = -1
	'===============================================================================
	'Function : isLogin()
	'Description : 로그인 여부 반환
	'===============================================================================
	Function isLogin()

		If GetsLOGINID() = "" Then
			isLogin = False
		Else
			isLogin = true
		End IF

	End Function

	'===============================================================================
	'Function : SetSession(UID, UCOMNUM)
	'Description : 로그인 설정
	'===============================================================================
	Function SetSession(sLOGINID, sCUSTNM, sSeq)
		Session("sLOGINID")		 = sLOGINID
		Session("sCUSTNM")		 = sCUSTNM
		Session("sSeq")			 = sSeq		
	End Function

	'===============================================================================
	'Function : GetsLOGINID()
	'Description : 로그인 UID 반환
	'===============================================================================

	Function GetsLOGINID()
		GetsLOGINID = Session("sLOGINID")
	End Function

	Function GetsCUSTNM()
		GetsCUSTNM = Session("sCUSTNM")
	End Function

	Function GetsUNIGRPCD()
		GetsUNIGRPCD = Session("sUNIGRPCD")
	End Function
	
	Function GetsCUSTZIP()
		GetsCUSTZIP = Session("sCUSTZIP")
	End Function

	
	Function GetsUNIGRPNM()
		GetsUNIGRPNM = Session("sUNIGRPNM")
	End Function
	
	Function GetsUCONUM()
		GetsUCONUM = Session("sUCONUM")
	End Function
	
	Function GetsRank()
		GetsRank = Session("sRank")
	End Function
	
	Function GetsSEQ()
		GetsSEQ = Session("sSeq")
	End Function
	

dim strqry
dim rs
dim strloginid
dim strloginpw
dim req_curPage,url,uni_grp_cd

agt_id  = request("agt_id")
agt_pwd = request("agt_pwd")


If Left(uni_grp_cd,1) = "w" Then  

	uni_grp_cd = "W"&MID(uni_grp_cd,2,3)

End If 

If Left(uni_grp_cd,1) = "l" Then  

	uni_grp_cd = "L"&MID(uni_grp_cd,2,3)

End If 

If Left(uni_grp_cd,1) = "t" Then  

	uni_grp_cd = "T"&MID(uni_grp_cd,2,3)

End If 

strqry = "SELECT A.AGT_ID AS AGT_ID"
strqry = strqry & ",A.AGT_NM AS AGT_NM"
strqry = strqry & ",A.UNI_GRP_CD AS UNI_GRP_CD"
strqry = strqry & ",itemcenter.dbo.IC_FN_RETAIL_NM(A.UNI_GRP_CD) AS UNI_GRP_NM"
strqry = strqry & ",B.RTL_COM_NUM AS RTL_COM_NUM "
strqry = strqry & ",A.agt_rank  "
strqry = strqry & ",A.seq  "
strqry = strqry & "FROM itemcenter.dbo.IC_T_AGENT A "
strqry = strqry & "LEFT OUTER JOIN (SELECT UNI_GRP_CD, RTL_COM_NUM "
strqry = strqry & "                   FROM itemcenter.dbo.IC_T_RETAIL "
strqry = strqry & "                  WHERE DEL_YN = 'N') B ON A.UNI_GRP_CD = B.UNI_GRP_CD "     
strqry = strqry & "WHERE A.DEL_YN = 'N' "
strqry = strqry & "AND A.END_TP = 'N'"
strqry = strqry & "AND A.AGT_ID = '" & agt_id & "' "
strqry = strqry & "AND A.AGT_PWD = '" & agt_pwd & "'"
strqry = strqry & "AND A.UNI_GRP_CD = '2001'"


DBOpen()
Set rs = DBcon.Execute(strqry)

if rs.Bof or rs.Eof Then
	rs.Close						
	Set rs = nothing
	DBClose()
	
	Response.write("<script langugae='javascript'>alert('로그인정보가 잘못되었습니다.'); location.href='gate.asp'</script>")
	response.end
else	
	Response.Cookies("widline_admin_login").Path				= "/"
	Response.cookies("widline_admin_login").Secure				= False
	Response.cookies("widline_admin_login").Domain				= Request.ServerVariables("SERVER_NAME")
	Response.Cookies("widline_admin_login")("AGT_ID")			= rs("AGT_ID")
	Response.Cookies("widline_admin_login")("AGT_NM")			= rs("AGT_NM")
	Response.Cookies("widline_admin_login")("ORDER_SEQ")		= rs("seq")

	Call SetSession(trim(rs("AGT_ID")),trim(rs("AGT_NM")),trim(rs("seq")))

	rs.Close						
	Set rs = nothing
	DBClose()

	Response.redirect "/admin/state/member_email_login_state.asp"
End If 

%>																		
