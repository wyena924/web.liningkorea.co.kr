<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<%
mode = request("mode")
seq = request("seq")

if seq = "" then
	%>
	<script>
		parent.alertBox("데이터번호가 존재하지 않습니다.");
	</script>
	<%
	response.end
end if

DBopen()

sql = "USP_WIDLINE_BOARD_NOTICE_ACCESS " & _
			" @MODE				= 'DEL_FILE'" & _
			",@SEQ				=  " & seq 
			'response.write sql&"<br>"
Set rs = DBcon.Execute(sql)
if rs(0) = 0 then
	%>
	<script>
		parent.alertBox("데이터 처리에 문제가 발생했습니다.");
	</script>
	<%
	response.end
end if

DBclose()

%>

<script>
	parent.alertBox("정상 처리되었습니다.",function() {
		parent.location.reload();
	});
</script>