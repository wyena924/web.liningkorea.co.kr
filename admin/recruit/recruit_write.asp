<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
<%
mode = request("mode")
page = request("page")
seq = request("seq")

goParam = "?page=" & page

if mode = "EDIT" Then
	if seq = "" then
		GoBack("데이터 번호가 존재하지 않습니다.")
		response.end
	end if

	DBopen()
	sql = "USP_WIDLINE_RECRUIT_NOTICE_VIEW @MODE='VIEW', @SEQ=" & seq
	Set rs = DBcon.execute(sql)
	if rs.eof or rs.bof then
		GoBack("데이터가 존재하지 않습니다.")
		response.end
	else
		subject = rs(0)
		writer = rs(1)
		content = rs(2)
		filename = rs(3)
		isview = rs(4)
		start_date = rs(7)
		end_date = rs(8)
		cap_gb	= rs(9)
		arr = split(filename,"|")
  	org_filename = arr(0)
  	save_filename = arr(1)
		ext = arr(2)
		
	end if
	DBclose()

	btn_msg = "수정"
else	
	isview = "Y"
	news_gb = "N"

	mode = "ADD"

	btn_msg = "등록"
end If

If start_date = "" Then
	start_date = Date
End If 

If END_DATE = "" Then
	End_DATE = Date + 30
End If 
%>
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->
	<script type="text/javascript" src="/front_include/editor/ckeditor.js"></script>
  <script type='text/javascript'>
		$(function() {
			var fileTarget<%= replace(num,"-","_") %> = $('#file');

			fileTarget<%= replace(num,"-","_") %>.on('change', function(){  // 값이 변경되면
					var filename = "";

					if(window.FileReader){  // modern browser
							filename = $(this)[0].files[0].name;
					} else {  // old IE
							filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
					}

					// 추출한 파일명 삽입
					$('#file_filename').val(filename);
			});
		});

		function addForm() {

			if($("#writer").val() == "") {
				alertBoxFocus("작성자를 입력해 주세요.","writer");
				return;
			}

			if($("#subject").val() == "") {
				alertBoxFocus("제목을 입력해 주세요.","subject");
				return;
			}

			var data = CKEDITOR.instances.content.getData();
			if(!data) {
				alertBoxFocus("내용을 입력해 주세요.","content");
				return;			
			}	

			if($("#file").val() != "") {
				var filename = $("#file_filename").val().toLowerCase();
				var index = filename.lastIndexOf(".");
				var ext = filename.substring(index + 1);
				var chk = 0;
				var chkext = ["jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf"];
				var i = 0;
				for(i = 0; i < chkext.length; i++) {          
					if(chkext[i]== ext) {
						chk++;
						break;
					}
				}
				if(chk == 0) {
					alertBoxFocus("첨부파일은 JPG,GIF,PNG,DOC(DOCX),PPT(PPTX),XLS(XLSX),HWP,ZIP,PDF만 가능합니다.","file");
					return;
				}
			}
			
			confirmBox("저장하시겠습니까?",function() {
					var f = document.thisform;

					<% if mode = "ADD" then %>
						f.mode.value = "ADD";
					<% elseif mode = "EDIT" then %>
						f.mode.value = "EDIT";
					<% end if %>

					f.target = 'hiddenFrame';
					f.action = "recruit_proc.asp";
					f.submit();
			});


		}

		function delForm() {

			confirmBox("삭제하시겠습니까?",function() {
					var f = document.thisform;
					f.mode.value = "DEL";
					f.target = 'hiddenFrame';
					f.action = "recruit_proc.asp";
					f.submit();
			});

		}

		function deleteFile() {
			confirmBox("파일을 삭제하시겠습니까?",function() {
					hiddenFrame.location.href = "notice_delete_file.asp?seq=<%= seq %>";
			});
		}

		$(document).ready(function() {

			var myToolbar =  
									[     
											{ name: 'document', items : [ 'Source','-','DocProps','Preview','Print','-','Templates' ]  },
											{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
											{ name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','Scayt' ] },
											{ name: 'insert', items : [ 'HorizontalRule','Smiley','SpecialChar' ] },
											{ name: 'styles', items : [ 'Font','FontSize' ] },
											{ name: 'colors', items : [ 'TextColor','BGColor' ] },
											{ name: 'basicstyles', items : [ 'Bold','Italic','Strike','-','RemoveFormat' ] },
											{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock' ] },
											{ name: 'links', items : [ 'Link','Unlink' ] },
											{ name: 'img', items : ['Table','Image'] }
									];

				CKEDITOR.replace('content', {
						height: 300,
						uiColor: '#eeeeee',
						enterMode: CKEDITOR.ENTER_BR,
						toolbar: myToolbar,
						filebrowserUploadUrl: '/pmaa/admin/include/image_upload.asp'	
				});    		
				
		});
	  
  </script>
  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">고객센터관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">미디어 뉴스 등록</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
	  <form name="thisform" id="thisform" method="post" action="recruit_proc.asp" enctype="multipart/form-data">
		<input type="hidden" name="mode" id="mode" value="<%= mode %>">
		<input type="hidden" name="seq" id="seq" value="<%= seq %>">
		<input type="hidden" name="page" id="page" value="<%= page %>">
	
		  <!-- S: page-content -->
		  <div class="page-content">
			<!-- S: component 테이블 요소 -->
			<section class="component">
			  <!-- S: title-box -->
			  <!-- S: table -->
				<div class="fixed-table">
				  <h3 class="table-caption">미디어 뉴스 <%=btn_msg%></h3>
				  <!-- S: fix-head-table -->
				  <table class="table fix-head-table">
					<colgroup>
					  <col style="width:10%;">
					  <col style="width:90%;">
					</colgroup>
					<tbody>
					<tr>
						<th class="menu" >작성자</th>
						<th colspan="3">
							 <div class="col-lg-12">
							  <input type="text" class="form-control" placeholder="" id="writer" name="writer" value="<%= writer %>" maxlegth="25">
							</div>
						</th>          
					</tr>
					<tr>
						<th class="menu" >공고기간</th>
						<th colspan="3">
							<div class="col-md-1">
								<input type="text" class="datepicker-here input-block" data-language="ko" placeholder="<%=date%>" name="start_date<%= num %>" id="start_date<%= num %>" value="<%= start_date %>">
							</div>
							<span class="col-md-1" style="width:0px;">
								~
							</span>
							<div class="col-md-1">
							  <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="2099-12-31" name="end_date<%= num %>" id="end_date<%= num %>" value="<%= end_date %>">
							</div>
						</th>          
					</tr>

					<tr>
						<th class="menu" >제목</th>
						<th colspan="3">
							 <div class="col-lg-12">
							  <input type="text" class="form-control" placeholder="" id="subject" name="subject" value="<%= subject %>" maxlength="100">
							</div>
						</th>          
					</tr>
					<tr>
						<th class="menu" >내용</th>
						<th colspan="3">
							 <div class="col-lg-12">
							  <textarea name="content" id="content" style="width:90%;height:300px;"><%= content %></textarea>
							</div>
						</th>          
					</tr>
					<tr>
						<th class="menu" >지원자격</th>
						<th>
						 <div class="col-lg-2">
							<select class="form-control" name="cap_gb">
								<option value='무관' <% If cap_gb = "무관" Then %>selected<% End If %>>무관</option>		
								<option value='신입' <% If cap_gb = "신입" Then %>selected<% End If %>>신입</option>			
								<option value='무관' <% If cap_gb = "경력" Then %>selected<% End If %>>경력</option>			
								<option value='신입/경력' <% If cap_gb = "신입/경력" Then %>selected<% End If %>>신입/경력</option>			
							</select>
						  </div>
						</th>							
					</tr>		
					<tr>
						<th class="menu" >노출여부</th>
						<th>
						 <div class="col-lg-2">
							<label class="radio-box">
							  <input type="radio" name="isview" value="Y" <%if isview="Y" then response.write "checked" end if %> >
							  <span>노출</span>
							</label>
							</div>
							<div class="col-lg-2">
							<label class="radio-box">
							  <input type="radio" name="isview" value="N" <%if isview="N" then response.write "checked" end if %> >
							  <span>미노출</span>
							</label>
						  </div>
						</th>							
					</tr>							
					<tr>
						<th class="menu" >첨부파일</th>
						<th colspan="3">
							<div class="col-lg-12">
								<%
									if filename <> "||" and mode = "EDIT" then
										'response.write "/upload/notice/" & save_filename & "<a href='#' onclick='deleteFile(); return false;'>[파일만 삭재]</a> <br>"
										response.write "<a href='downfile.asp?kbn=notice&seq=" & seq & "'><img src='/pmaa/admin/front/ext/" & ext & ".gif'> /upload/notice/" & save_filename & "</a> <a href='#' onclick='deleteFile(); return false;'>[파일만 삭재]</a> <br>"
									end if
								%>
								<input type="text" name="file_filename" id="file_filename" class="form-control" placeholder="파일을 선택하세요" style="width:80%;display:inline-block;background:#ffffff;" value="<%= org_filename %>" readonly>
								<label type="button" for="file" class="btn btn-light active" style="width:110px;">Browse</label>
								<input type="file" name="file" id="file" style="position: absolute; width: 1px;  height: 1px;  padding: 0;  margin: -1px;  overflow: hidden;  clip:rect(0,0,0,0);  border: 0;">
							</div>
						</th>          
					</tr>
					</tbody>
				  </table>
				  <!-- E: fix-head-table -->					
					<a href="#" class="btn btn-primary" onclick="addForm(); return false;"><%= btn_msg %></a>	 
				  &nbsp;&nbsp;
				  <a href="recruit_list.asp<%= goParam %>" class="btn btn-dark">취소</a>	 
					
					<% if mode = "EDIT" then %>
					<a href="#" class="btn btn-danger" style="float:left;margin-top:10px;margin-right:10px;" onclick="delForm(); return false;">삭제</a>	 					
					<% end if %>

				</div>
			</form>

			<iframe name="hiddenFrame" id="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->


  <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>