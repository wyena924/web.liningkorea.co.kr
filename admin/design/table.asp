<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">어드민 메뉴</a>
      </li>
      <li>
        <a href="#" data-location="design">디자인</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">테이블 및 버튼</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <h2>테이블 디자인</h2>
        <p>어드민에서 사용되는 테이블 디자인 입니다.</p>
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <div class="title-box">
            <h3 class="comp-title">테이블 요소</h3>
            <p class="comp-exp">wrap div에 table-round: 둥근 테두리, table-center: 가운데 정렬</p>
          </div>
          <!-- E: title-box -->

          <!-- S: table -->
          <div class="table-round">
            <table class="table">
              <!-- S: colgroup -->
              <colgroup>
                <col>
              </colgroup>
              <!-- E: colgroup -->
              <thead>
                <tr>
                  <th>번호</th>
                  <th>등록일자</th>
                  <th>발송인</th>
                  <th>연락처</th>
                  <th>회사명</th>
                  <th>발신메일</th>
                  <th>수신확인</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>4</td>
                  <td>2018-05-04</td>
                  <td>
                    <span class="name">박보검</span>
                  </td>
                  <td>010-1234-5678</td>
                  <td>
                    <span class="mail">pbg@naver.com</span>
                  </td>
                  <td class="confirm-data">
                    <a href="#" class="btn btn-primary">내용보기</a>
                  </td>
                  <td class="confirm-data not-yet">
                    <span class="txt">미확인</span>
                    <span class="ic-deco">
                      <i class="far fa-square"></i>
                    </span>
                  </td>
                </tr>
                <tr>
                  <td>4</td>
                  <td>2018-05-04</td>
                  <td>
                    <span class="name">박보검</span>
                  </td>
                  <td>010-1234-5678</td>
                  <td>
                    <span class="mail">pbg@naver.com</span>
                  </td>
                  <td>
                    <a href="#" class="btn btn-primary">내용보기</a>
                  </td>
                  <td class="confirm-data">
                    <span class="txt">확인</span>
                    <span class="ic-deco">
                      <i class="fas fa-check-square"></i>
                    </span>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: table -->
          
          <!-- S: comp-part -->
          <div class="comp-part">
            <!-- S: sub-title-box -->
            <div class="sub-title-box">
              <h4>테이블 제목 고정</h4>
              <p>테이블 title은 고정되고 내용만 스크롤되는 형태</p>
            </div>
            <!-- E: sub-title-box -->
            <!-- S: fixed-table -->
            <div class="fixed-table">
              <h3 class="table-caption">채용공고</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="90">
                  <col width="110">
                  <col width="100">
                  <col width="270">
                  <col width="100">
                  <col width="120">
                  <col width="540">
                  <col width="145">
                  <col width="50">
                </colgroup>
                <tbody>
                  <tr>
                    <th>번호</th>
                    <th>등록일자</th>
                    <th>접수상태</th>
                    <th>접수기간</th>
                    <th>접수방법</th>
                    <th>지원자격</th>
                    <th>채용부분</th>
                    <th>채용담당자</th>
                    <th>HIT</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <table class="table table-striped table-hover table-content">
                  <colgroup>
                    <col width="90">
                    <col width="110">
                    <col width="100">
                    <col width="270">
                    <col width="100">
                    <col width="120">
                    <col width="540">
                    <col width="145">
                    <col width="50">
                  </colgroup>
                  <tbody>
                    <tr>
                      <td>1</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td>
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>신입</td>
                      <td class="part">
                        <span class="cut-eli"><strong>잘 안죽는 사람 우대</strong>영업사원 및 영업지원 부분 신입사원 대 방출!</span>
                      </td>
                      <td>데드풀</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>2</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli"><span class="hurry">[급구]</span>와칸다 포에버! 타노스의 공격에 함께 싸워줄 형제들 모십니다!</span>
                      </td>
                      <td>블랙펜서</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>3</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">Bring me THANOS!!!</span>
                      </td>
                      <td>토르 - 오딘의 아들</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>4</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력 70년 이상</td>
                      <td>
                        <span class="cut-eli"><strong>세계대전 참전용사 우대</strong>모두 함께 힘을 합쳐 우주 평화를 위해 싸울 동료 모집</span>
                      </td>
                      <td>캡틴 아메리카</td>
                      <td>001</td>
                    </tr>
                    <tr>
                      <td>I'm Groot</td>
                      <td>I'm Groot</td>
                      <td>I'm Groot</td>
                      <td class="part">
                        <span class="_from">I'm Groot</span>
                        <span class="_to">I'm Groot</span>
                      </td>
                      <td>I'm Groot</td>
                      <td>I'm Groot</td>
                      <td>
                        <span class="cut-eli">I'm Groot</span>
                      </td>
                      <td>I'm Groot</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>6</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli" style="width: 490px">타노스의 공격에 대비해줄 능력자들 모십니다.^^ 타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>7</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>8</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>9</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>10</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>11</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>12</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>13</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>방문면접</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>14</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>15</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>16</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>17</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>18</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>19</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                    <tr>
                      <td>20</td>
                      <td>2018-02-02</td>
                      <td>접수중</td>
                      <td class="part">
                        <span class="_from">2018-02-02</span>
                        <span>부터</span>
                        <span class="_to">2018-03-02</span>
                        <span>까지</span>
                      </td>
                      <td>이메일</td>
                      <td>경력</td>
                      <td>
                        <span class="cut-eli">타노스의 공격에 대비해줄 능력자들 모십니다.^^</span>
                      </td>
                      <td>어벤져스</td>
                      <td>000</td>
                    </tr>
                  </tbody>
                </table>
                <!-- E: table-content -->
              </div>
              <!-- E: scroll-body -->
            </div>
            <!-- E: fixed-table -->
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-primary table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-success table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-warning table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-danger table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-info table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-teal table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-pink table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-orange table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-dark table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
            <table class="table table-light table-color table-bordered">
              <colgroup>
                <col width="90px">
                <col width="160px">
                <col width="160px">
                <col width="160px">
              </colgroup>
              <thead>
                <tr>
                  <th>구분</th>
                  <th>이름</th>
                  <th>부서</th>
                  <th>직책</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1</td>
                  <td>베트맨</td>
                  <td>영업 973팀</td>
                  <td>사장</td>
                </tr>
                <tr>
                  <td>2</td>
                  <td>슈퍼맨</td>
                  <td>영업 973팀</td>
                  <td>외계인</td>
                </tr>
              </tbody>
            </table>
          </div>
          <!-- E: comp-part -->

          <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              <!-- E: pagination -->
            </div>
            <!-- E: page-index -->

            <!-- S: page-index -->
            <div class="page-index page-teal">
              <!-- S: pagination -->
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              <!-- E: pagination -->
            </div>
            <!-- E: page-index -->
          </div>
          <!-- E: comp-part -->

        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->


  <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>