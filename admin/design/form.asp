<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- E: include common_depth -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">어드민 메뉴</a>
      </li>
      <li>
        <a href="#" data-location="design">디자인</a>
      </li>
      <li>
        <a href="#" data-location="form" class="active">입력 폼</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <h2>form 디자인</h2>
        <p>어드민에서 사용되는 form 디자인 입니다.</p>
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 버튼 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <div class="title-box">
            <h3 class="comp-title">input 요소</h3>
            <p class="comp-exp">form-control에 각각 readonly, disabled 속성을 활용</p>
          </div>
          <!-- E: title-box -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <input type="text" class="form-control" placeholder="form-control">
            </div>
            <!-- E: col-lg-4 -->

            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <input type="text" class="form-control" placeholder="readonly" readonly>
            </div>
            <!-- E: col-lg-4 -->

            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <input type="text" class="form-control" placeholder="disabled" disabled>
            </div>
            <!-- E: col-lg-4 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <textarea class="form-control" placeholder="textarea form-control"></textarea>
            </div>
            <!-- E: col-lg-4 -->

            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <textarea class="form-control" placeholder="readonly form-control" readonly></textarea>
            </div>
            <!-- E: col-lg-4 -->

            <!-- S: col-lg-4 -->
            <div class="col-lg-4">
              <textarea class="form-control" placeholder="disabled form-control" disabled></textarea>
            </div>
            <!-- E: col-lg-4 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-md-4 -->
            <div class="col-md-4">
              <!-- S: file-browser -->
              <div class="file-browser">
                <input type="file" class="file-ipt" multiple>
                <!-- S: file-ipt-view -->
                <span class="ipt-view">
                  <span class="placeholder">파일을 선택하세요</span>
                  <span class="ctr">Browse</span>
                </span>
                <!-- E: file-ipt-view -->
              </div>
              <!-- E: file-browser -->
            </div>
            <!-- E: col-md-4 -->

            <!-- S: col-md-4 -->
            <div class="col-md-4">
              <!-- S: file-browser -->
              <div class="file-browser">
                <input type="file" class="file-ipt">
                <!-- S: file-ipt-view -->
                <span class="ipt-view">
                  <span class="placeholder">파일을 선택하세요</span>
                  <span class="ctr ctr-primary">Browse</span>
                </span>
                <!-- E: file-ipt-view -->
              </div>
              <!-- E: file-browser -->
            </div>
            <!-- E: col-md-4 -->

            <!-- S: col-md-4 -->
            <div class="col-md-4">
              <!-- S: file-browser -->
              <div class="file-browser">
                <input type="file" class="file-ipt" multiple>
                <!-- S: file-ipt-view -->
                <span class="ipt-view">
                  <span class="placeholder">파일을 선택하세요</span>
                  <span class="ctr ctr-black">Browse</span>
                </span>
                <!-- E: file-ipt-view -->
              </div>
              <!-- E: file-browser -->
            </div>
            <!-- E: col-md-4 -->
          </div>
          <!-- E: row -->

          <!-- S: comp-part input checkbox, input radio -->
          <div class="comp-part">
            <!-- S: sub-title-box -->
            <div class="sub-title-box">
              <h4>Input 태그</h4>
              <p>input checkbox, radio 버튼</p>
            </div>
            <!-- E: sub-title-box -->

            <!-- S: row -->
            <div class="row">
              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box">
                  <input type="checkbox">
                  <span>checkbox</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box">
                  <input type="checkbox" checked>
                  <span>checkbox - checked</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box">
                  <input type="checkbox" disabled>
                  <span>checkbox - disabled</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->
            </div>
            <!-- E: row -->

            <!-- S: row -->
            <div class="row">
              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="radio-box">
                  <input type="radio" name="radio_ipt">
                  <span>radio</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="radio-box">
                  <input type="radio" checked name="radio_ipt">
                  <span>radio - checked</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="radio-box">
                  <input type="radio" disabled name="radio_ipt">
                  <span>radio - disabled</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->
            </div>
            <!-- E: row -->

            <!-- S: row -->
            <div class="row">
              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box chk-primary">
                  <input type="checkbox" checked>
                  <span>Checkbox chk-primary</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box chk-success">
                  <input type="checkbox" checked>
                  <span>Checkbox chk-success</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box chk-pink">
                  <input type="checkbox" checked>
                  <span>Checkbox chk-pink</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->

              <!-- S: col-lg-3 -->
              <div class="col-lg-3">
                <label class="chk-box chk-info">
                  <input type="checkbox" checked>
                  <span>Checkbox chk-info</span>
                </label>
              </div>
              <!-- E: col-lg-3 -->
            </div>
            <!-- E: row -->
          </div>
          <!-- E: comp-part input checkbox, input radio -->
        </section> 
        <!-- E: component input 요소 -->

        <!-- S: component select 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <div class="title-box">
            <h3 class="comp-title">select 요소</h3>
            <p class="comp-exp">select 박스를 customizing 하여 브라우저별 형태를 맞춤</p>
          </div>
          <!-- E: title-box -->
          <!-- S: comp-part -->
          <div class="comp-part">
            <!-- S: row -->
            <div class="row">
              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box">
                  <span class="txt">default</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->

              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box sel-beige">
                  <span class="txt">beige</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>beige</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->

              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box sel-white">
                  <span class="txt">white</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->

              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box sel-black">
                  <span class="txt">black</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>beige</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->

              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box sel-blue">
                  <span class="txt">blue</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>beige</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->

              <!-- S: col-lg-2 -->
              <div class="col-lg-2">
                <!-- S: sel-box -->
                <div class="sel-box sel-pink">
                  <span class="txt">pink</span>
                  <select>
                    <option>color</option>
                    <option>red</option>
                    <option>beige</option>
                    <option>blue</option>
                    <option>yellow</option>
                    <option>black</option>
                  </select>
                </div>
                <!-- E: sel-box -->
              </div>
              <!-- E: col-lg-2 -->
            </div>
            <!-- E: row -->
          </div>
          <!-- E: comp-part -->
        </section>
        <!-- E: component select 요소 -->

        <!-- S: component calendar -->
        <section class="component">
          <!-- S: title-box -->
          <div class="title-box">
            <h3 class="comp-title">캘린더</h3>
            <p class="comp-exp">Air Datepicker를 활용한 캘린더 호출</p>
          </div>
          <!-- E: title-box -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-md-2 -->
            <div class="col-md-2">
              <input type="text" class="datepicker-here input-block" data-language="ko" placeholder="2018-05-18">
            </div>
            <!-- E: col-md-2 -->

            <!-- S: col-md-2 -->
            <div class="col-md-2">
              <input type="text" data-range="true" data-multiple-dates-separator=" ~ " data-language="ko" class="datepicker-here input-block" placeholder="2018-05-01 ~ 2018-05-18">
            </div>
            <!-- E: col-md-2 -->
          </div>
          <!-- E: row -->
        </section>
        <!-- E: component calendar -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->


  <!-- S: include footer -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer -->
</body>
</html>