<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">어드민 메뉴</a>
      </li>
      <li>
        <a href="#" data-location="design">디자인</a>
      </li>
      <li>
        <a href="#" data-location="btn" class="active">버튼</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <h2>버튼 디자인</h2>
        <p>어드민에서 사용되는 버튼 디자인 입니다.</p>
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 버튼 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <div class="title-box">
            <h3 class="comp-title">버튼 요소</h3>
            <p class="comp-exp">1열 - 기본상태, 2열 - 활성화 상태, 3열 - 비활성화 상태 &nbsp;&nbsp;&nbsp;&nbsp;  btn-block - 꽉차는 버튼 &nbsp;&nbsp;&nbsp;&nbsp; btn-trans - 애니메이션</p>
          </div>
          <!-- E: title-box -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-default">btn-default</a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-default hover">Hover</a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-default active">Active</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-default">
                  <span class="txt">다운로드</span>
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fa fa-download"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-default">
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fa fa-download"></i>
                  </span>
                  <span class="txt">다운로드</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-default">
                  <span class="txt">다운로드</span>
                  <span class="ic-deco btn-deco">
                    <i class="fa fa-download"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-default">
                  <span class="ic-deco btn-deco">
                    <!-- <i class="fas fa-search"></i> -->
                    <i class="fa fa-download"></i>
                  </span>
                  <span class="txt">다운로드</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-blue">
                  <span class="txt">검색</span>
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-blue">
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                  <span class="txt">검색</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-orange">
                  <span class="txt">검색</span>
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-orange">
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                  <span class="txt">검색</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-green">
                  <span class="txt">검색</span>
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-green">
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                  <span class="txt">검색</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-trans btn-red">
                  <span class="txt">검색</span>
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                </a></li>
                <li><a href="#" class="btn btn-block btn-trans btn-red">
                  <span class="ic-deco btn-deco edge-deco">
                    <i class="fas fa-search"></i>
                  </span>
                  <span class="txt">검색</span>
                </a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-primary">btn-primary</a></li>
                <li><a href="#" class="btn btn-block btn-primary active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-primary disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-success">btn-success</a></li>
                <li><a href="#" class="btn btn-block btn-success active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-success disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-info">btn-info</a></li>
                <li><a href="#" class="btn btn-block btn-info active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-info disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-warning">btn-warning</a></li>
                <li><a href="#" class="btn btn-block btn-warning active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-warning disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-danger">btn-danger</a></li>
                <li><a href="#" class="btn btn-block btn-danger active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-danger disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-teal">btn-teal</a></li>
                <li><a href="#" class="btn btn-block btn-teal active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-teal disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-light">btn-light</a></li>
                <li><a href="#" class="btn btn-block btn-light active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-light disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-dark">btn-dark</a></li>
                <li><a href="#" class="btn btn-block btn-dark active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-dark disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans">btn-empty-primary</a></li>
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-secondary btn-trans">btn-secondary</a></li>
                <li><a href="#" class="btn btn-block btn-secondary btn-trans active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-secondary disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-success btn-trans">btn-empty-success</a></li>
                <li><a href="#" class="btn btn-block btn-empty-success btn-trans active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-success disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-warning btn-trans">btn-empty-warning</a></li>
                <li><a href="#" class="btn btn-block btn-empty-warning btn-trans active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-warning disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->

          <!-- S: sub-title-box -->
          <div class="sub-title-box">
            <h4>Rounded Button</h4>
            <p>둥근 형태 : btn-round</p>
          </div>
          <!-- E: sub-title-box -->

          <!-- S: row -->
          <div class="row">
            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans btn-round">btn-empty-primary</a></li>
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans btn-round active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-primary btn-trans btn-round disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-secondary btn-trans btn-round">btn-secondary</a></li>
                <li><a href="#" class="btn btn-block btn-secondary btn-trans btn-round active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-secondary btn-round disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-success btn-trans btn-round">btn-empty-success</a></li>
                <li><a href="#" class="btn btn-block btn-empty-success btn-trans btn-round active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-success btn-round disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->

            <!-- S: col-sm-6 col-md-3 -->
            <div class="col-sm-6 col-md-3">
              <ul class="btn-list">
                <li><a href="#" class="btn btn-block btn-empty-warning btn-trans btn-round">btn-empty-warning</a></li>
                <li><a href="#" class="btn btn-block btn-empty-warning btn-trans btn-round active">Active</a></li>
                <li><a href="#" class="btn btn-block btn-empty-warning btn-round disabled">Disabled</a></li>
              </ul>
            </div>
            <!-- E: col-sm-6 col-md-3 -->
          </div>
          <!-- E: row -->
        </section>
        <!-- E: component 버튼 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->


  <!-- S: include footer -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer -->
</body>
</html>