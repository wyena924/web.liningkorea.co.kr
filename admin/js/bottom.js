/*=====================메인 슬리아드 배너==============================*/
$(document).ready(function(){
	
	$('.bxslider').bxSlider({
		auto: true, // 자동 슬라이드
		speed: 500, // 슬라이드 정지 기간
		controls: true, // next : prev 화살표 표시
		mode: 'fade', // 화면이 부드럽게 바뀌는 효과입니다.
		nextSelector: '#slider-next', //오른쪽으로 이동 하는 버튼
		prevSelector: '#slider-prev', //왼쪽으로 이동 하는 버튼
		nextText: '',
		prevText: ''
	});
});


