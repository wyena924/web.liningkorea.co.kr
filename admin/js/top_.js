/*===============================모바일 메뉴=======================================*/
jQuery(document).ready(function($){
  var $lateral_menu_trigger = $('#cd-menu-trigger'),
    $close_menu_trigger = $('.close-menu-trigger'),
    $content_wrapper = $('.cd-main-content'),
    $navigation = $('header');
  //open-close lateral menu clicking on the menu icon
  $lateral_menu_trigger.on('click', function(event){
    event.preventDefault();
    
    $lateral_menu_trigger.toggleClass('is-clicked');
    $navigation.toggleClass('lateral-menu-is-open');
    $content_wrapper.toggleClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
      // firefox transitions break when parent overflow is changed, so we need to wait for the end of the trasition to give the body an overflow hidden
      $('body').toggleClass('overflow-hidden');
    });
    $('#cd-lateral-nav').toggleClass('lateral-menu-is-open');
    
    //check if transitions are not supported - i.e. in IE9
    if($('html').hasClass('no-csstransitions')) {
      $('body').toggleClass('overflow-hidden');
    }

    
  });

  //close lateral menu clicking outside the menu itself
  $content_wrapper.on('click', function(event){
    if( !$(event.target).is('#cd-menu-trigger, #cd-menu-trigger span') ) {
      $lateral_menu_trigger.removeClass('is-clicked');
      $navigation.removeClass('lateral-menu-is-open');
      $content_wrapper.removeClass('lateral-menu-is-open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
        $('body').removeClass('overflow-hidden');
      });
      $('#cd-lateral-nav').removeClass('lateral-menu-is-open');
      //check if transitions are not supported
      if($('html').hasClass('no-csstransitions')) {
        $('body').removeClass('overflow-hidden');
      }

    }
  });

  $close_menu_trigger.on('click', function(){
    $('#cd-lateral-nav').removeClass('lateral-menu-is-open');
  });

  //open (or close) submenu items in the lateral menu. Close all the other open submenu items.
  $('.item-has-children').children('a').on('click', function(event){
    event.preventDefault();
    $(this).toggleClass('submenu-open').next('.sub-menu').slideToggle(200).end().parent('.item-has-children').siblings('.item-has-children').children('a').removeClass('submenu-open').next('.sub-menu').slideUp(200);
  });


});


/*input file*/
 function getFile(){
   document.getElementById("upfile").click();
 }
 function sub(obj){
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("yourBtn").innerHTML = fileName[fileName.length-1];
    document.myForm.submit();
    event.preventDefault();
  }


/* head 검색 block */
$(document).ready(function(){
  //Toggle mobile menu & search
  $('.toggle-nav').click(function() {
    console.log('toggle nav');
    $('.mobile-nav').slideToggle(200);
    $('.mobile-search').slideUp(200);
  });
  //Close navigation on anchor tap
  // $('a').click(function() {
  //   console.log('close nav');
  //   $('.mobile-nav').slideUp(200);
  //   $('.mobile-search').slideUp(200);
  // }); 



  //Mobile menu accordion toggle for sub pages
  $('.mobile-nav > ul > li.menu-item-has-children').append('<div class="accordion-toggle"><div class="fa fa-angle-down"></div></div>');
  $('.mobile-nav .accordion-toggle').click(function() {
    $(this).parent().find('> ul').slideToggle(200);
    $(this).toggleClass('toggle-background');
    $(this).find('.fa').toggleClass('toggle-rotate');
  });

  $('.head .toggle-search').toggleSrch('.head .toggle-search'); // 상단 검색바 toggle
  $('.slide_popup').pdOptToggle('.slide_popup'); // 하단 옵션 선택창 toggle
  $('#cd-lateral-nav').gnbArr('#cd-lateral-nav'); // gnb 수정 - body,html scroll 방지, gnb 높이 조절 등
  $('#plus_menu').defBodyScroll(); // modal open 시 스크롤 막기
});

(function($){
  var defOption = {
    el: null,
    playSpeed: 400,
    motion : 'easeInOutQuint'
  }
  /**
   * LRMoving 왼쪽에서 오른쪽으로 이동 시키는 기능
   * @type {Object}
   */
  var LRMoving = {
    /**
     * moving 구현
     * @param  {jQuery Object} el 움직일 요소
     */
    moving : function(option){
      var opts = $.extend({}, defOption, option);
      try {
        opts.el.stop().animate({
          left: 0
        }, opts.playSpeed, opts.motion);
      }
      catch(e) {
        throw console.log(e.message, '사용할 옵션은 el: 요소, playSpeed: animation 시간, motion: 애니메이션 easing 입니다.');
      }
    }
  }
  /* LRMoving */

  /**
   * RLMoving 오른쪽에서 왼쪽으로 이동 시키는 기능
   * @type {Object}
   */
  var RLMoving = {
    /**
     * moving 구현
     * @param  {jQuery Object} el 움직일 요소
     */
    moving: function(option){
      var opts = $.extend({}, defOption, option);
      try {
        opts.el.stop().animate({
          right: 0
        }, opts.playSpeed, opts.motion);
      }
      catch(e) {
        throw console.log(e.message, '사용할 옵션은 el: 요소(필수), playSpeed: animation 시간, motion: 애니메이션 easing 입니다.');
      }
    }
  }
  /* RLMoving */

  /**
   * TBMoving 위에서 아래로 이동 시키는 기능
   * @type {Object}
   */
  var TBMoving = {
    /**
     * moving 구현
     * @param  {jQuery Object} el 움직일 요소
     */
    moving: function(option){
      var opts = $.extend({}, defOption, option);
      try {
        opts.el.stop().animate({
          top: 0
        });
      }
      catch(e) {
        throw console.log(e.message, '사용할 옵션은 el: 요소(필수), playSpeed: animation 시간, motion: 애니메이션 easing 입니다.');
      }
    }
  }
  /* TBMoving */

  /**
   * BTMoving 아래에서 위로 이동 시키는 기능
   * @type {Object}
   */
  var BTMoving = {
    /**
     * moving 구현
     * @param  {jQuery Object} el 움직일 요소
     */
    moving: function(option){
      var opts = $.extend({}, defOption, option);
      opts.el.stop().animate({
        bottom: 0
      },opts.playSpeed, opts.motion);
    }
  }


  /* BTMoving */

  /**
   * 옵션 선택창
   * @param {jQuery obj} el slide_popup 클래스
   */
  function PdOptToggle(el){
    this._$el = null; /* slide_popup */
    this._$slideBtn = null; /* slide_top 버튼 */
    this._$slideOpt = null; /* slide_optoin 버튼 */
    this._basicB = -1;  /* slide_popup 초기 bottom 위치 */

    this._init(el);
    this._evt();
  }

  PdOptToggle.prototype._init = function(el) {
    this._$el = $(el);
    this._$slideBtn = $('.slide_top', this._$el);
    this._$slideOpt = $('.btn_widget .slide_option');
    this._basicB = this._$el.css('bottom');
  };

  PdOptToggle.prototype._evt = function() {
    var that = this;

    this._$slideOpt.on('click', function(e){
      $(this).hide();
      that._$slideBtn.addClass('on').show();;
      BTMoving.moving({
        el: that._$el
      }); // 위로 올리기
    }); // slideOpt click end

    this._$slideBtn.on('click', function(e){
      $(this).toggleClass('on').hide();
      that._$slideOpt.show();
      if ($(this).hasClass('on')) {
        that._$slideOpt.addClass('on');
        BTMoving.moving({
          el: that._$el
        }); // 위로 올리기
      } else {
        that._$slideOpt.removeClass('on');
          that._moveDown({
            el: that._$el
          }); // 아래로 감추기
        }
      e.preventDefault();
    }); // slideBtn click end

  };

  PdOptToggle.prototype._moveDown = function(option) {
    var opt = $.extend({}, defOption, option);
    try {
      opt.el.stop().animate({
        bottom: this._basicB
      }, opt.playSpeed, opt.motion)
    } catch(e) {
      throw console.log(e.message, '사용할 옵션은 el: 요소(필수), playSpeed: animation 시간, motion: 애니메이션 easing 입니다.');
    }
  };

  $.fn.pdOptToggle = function(el){
    this.each(function(){
      var pdOptToggle = new PdOptToggle(el);
    });
    return this;
  }



  /**
   * 검색창 작동
   * @param {jQuery obj} tg 검색 버튼
   */
  function ToggleSearch(tg){
    this._$toggleBtn = null; /* 검색 버튼 */
    this._$mobileSrch = null; /* 검색창 */
    this._$closeBtn = null; /* 검색창 닫기 */

    this._init(tg);
    this._evt();
  }

  ToggleSearch.prototype._init = function(tg) {
    this._$toggleBtn = $(tg);
    this._$mobileSrch = $('.mobile-search');
    this._$closeBtn = $('.sech_box .x_btn'); //검색창 끄기
  };

  ToggleSearch.prototype._evt = function() {
    var that = this;
    // 검색 버튼 터치
    this._$toggleBtn.on('click', function(e){
      that._$mobileSrch.show();
      LRMoving.moving({
        el: that._$mobileSrch
      });
      e.preventDefault();
    }); // 검색 버튼

    // 닫기 버튼 터치
    this._$closeBtn.on('click', function(e){
      that._$mobileSrch.stop().animate({
        'left' : '-100%'
      }, 400, 'easeInOutQuint');
      e.preventDefault();
    }); // 닫기 버튼
  };

  $.fn.toggleSrch = function(tg){
    this.each(function(){
      var toggleSrch = new ToggleSearch(tg);
    });
    return this;
  }


  /**
  * gnb 메뉴 편집
  * tg = #cd-lateral-nav
  */
  function GnbArr(tg){
    this._$tg = null; /* #cd-lateral-nav */
    this._$mMenu = null; /* mMenu */
    this._$navBtn = null; /* cd-menu-trigger 메뉴 열림 버튼 */
    this._$closeNavBtn = null; /* close-menu-trigger 메뉴 닫힘 버튼 */

    this._$body = null; /* body */
    this._$html = null; /* html */
    this._winH = -1; /* window의 높이를 담을 프로퍼티 */

    this._init(tg);
    this._evt();
  }

  GnbArr.prototype._init = function(tg) {
    this._$tg = $(tg);
    this._$mMenu = $('.head').find('.m_menu');
    this._$navBtn = $('#cd-menu-trigger');
    this._$closeNavBtn = $('.close-menu-trigger');

    this._$solveTarget = $('html, body');
    this._winH = $(window).outerHeight(true);
  };

  GnbArr.prototype._evt = function() {
    var that = this;
    // 메뉴 열림 버튼 터치
    this._$navBtn.on('click', function(e){
      that._defBody();
      e.preventDefault();
    }); // 메뉴 열림 버튼 터치

    // 메뉴 닫힘 버튼 터치
    this._$closeNavBtn.on('click', function(e){
      that._solveBody();
      e.preventDefault();
    }); // 메뉴 닫힘 버튼 터치

    this._setNaviHeight(this._$tg);
    if (this._$mMenu)
      this._setNaviHeight(this._$mMenu);
  };

  GnbArr.prototype._defBody = function() {
    this._$solveTarget.css({
      'overflow' : 'hidden'
    });
  };

  GnbArr.prototype._solveBody = function() {
    this._$solveTarget.removeAttr('style');
  };

  GnbArr.prototype._setNaviHeight = function(tg) {
    tg.css({
      'height' : this._winH
    });
  };

  $.fn.gnbArr = function(tg){
    this.each(function(){
      var gnbArr = new GnbArr(tg);
    });
    return this;
  }

  /**
   * modal open 시 body, html 스크롤 방지
   */
  function DefBodyScroll(){
    this._$plusModal = null; /* plus_menu modal */
    this._$HtmlBody = null; /* html & body */

    this._init();
    this._evt();
  };

  DefBodyScroll.prototype._init = function() {
    this._$HtmlBody = $('html, body');
    this._$plusModal = $('#plus_menu');
  };

  DefBodyScroll.prototype._evt = function() {
    this._$plusModal.on('show.bs.modal', $.proxy(function(e){
      this._$HtmlBody.css('overflow','hidden');
      document.body.style.height = (document.documentElement.clientHeight - 10) + 'px';
      this._$HtmlBody.on('scroll touchmove mousewheel', function(e){
        e.preventDefault();
        e.stopPropagation();
        return false;
      });
    }, this));
    this._$plusModal.on('hide.bs.modal', $.proxy(function(e){
      this._$HtmlBody.css('overflow','auto');
      $('body').css('height', 'auto');
      this._$HtmlBody.off('scroll touchmove mousewheel');
    }, this));
  };


  $.fn.defBodyScroll = function(){
    this.each(function(){
      var defBodyScroll = new DefBodyScroll();
    });
    return this;
  }

  $(document).ready(function(){
	
  })

})(jQuery);











/* 장바구니 옵션 슬리드 */
jQuery(document).ready(function(){
  function toggleIcon(e) {
        $(e.target)
            .prev('.panel-heading')
            .find(".more-less")
            .toggleClass('up_icon down_icon');
    }
    $('.panel-group').on('hidden.bs.collapse', toggleIcon);
    $('.panel-group').on('shown.bs.collapse', toggleIcon);
});





/* jquery ui */
$(document).ready(function(){
	kkMallUi();
});


function kkMallUi () {
	card(); // 결제하기 카드 선택 
	Sniff();// 조르기 아이콘 변경
	SeLect(); // 셀렉트 디자인
	SerchBtn();// 검색창 스크롤 정지
	MenuBar();
}

/* 결제하기 카드 선택 */
function card () {
	var $CardSelect =null;
	var $Btn_on = $("#Card_select ul li");
	$Btn_on.eq(0).addClass('on');

	$Btn_on.click(function(){
		
		$Btn_on.removeClass('on');

		if($CardSelect != null){
			$CardSelect.removeClass("on");
		}
		$CardSelect=$(this);
		$CardSelect.addClass("on");
	});

	$('.l_cate').on('click', function(){
	 $('#cd-lateral-nav').addClass('lateral-menu-is-open'); // 메뉴 열림
    })
}

/* 조르기 아이콘 변경 */
function Sniff(){
	var $Snslink = null;
	var $Snsimgbtn = $("#Sns_link ul li");

	$Snsimgbtn.click(function(){
		//툭정 클래스에 해지한다
		if ($(this).hasClass("on")){
			$(this).removeClass("on");
			return;
		}
		if($Snslink != null){
			$Snslink.removeClass("on");
		}
		$Snslink = $(this);
		$Snslink.addClass("on");
	})
}

/* select 디자인 */
function SeLect(){
	$('.select').on('click','.placeholder',function(){
    var parent = $(this).closest('.select');
    if ( ! parent.hasClass('is-open')){
    parent.addClass('is-open');
    $('.select.is-open').not(parent).removeClass('is-open');
    }else{
    parent.removeClass('is-open');
    }
  }).on('click','ul>li',function(){
    var parent = $(this).closest('.select');
    parent.removeClass('is-open').find('.placeholder').text( $(this).text() );
    parent.find('input[type=hidden]').attr('value', $(this).attr('data-value') );
  });
}

/* 달력 */
$(function() {
   $.datepicker.setDefaults({
    dateFormat: 'yy-mm-dd',
    prevText: '이전 달',
    nextText: '다음 달',
    monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
    dayNames: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesShort: ['일', '월', '화', '수', '목', '금', '토'],
    dayNamesMin: ['일', '월', '화', '수', '목', '금', '토'],
    showMonthAfterYear: true,
    yearSuffix: '년'
  });

  $(function() {
    $("#datepicker1, #datepicker2").datepicker();
  });
});

/* 검색창 스크롤 정지 */
function SerchBtn(){
	$(".toggle-search").click(function () {
		$('html, body').css({'overflow': 'hidden', 'height': '100%'});
	});
	$(".x_btn").click(function () {
		$('html, body').css({ 'overflow': 'auto', 'height': 'auto' });
	});
}

function MenuBar(){
	$("#cd-menu-trigger").click(function () {
		$('html, body').css({'overflow': 'hidden', 'height': '100%'});
	});
	$(".close-menu-trigger").click(function () {
		$('html, body').css({ 'overflow': 'auto', 'height': 'auto' });
	});
}