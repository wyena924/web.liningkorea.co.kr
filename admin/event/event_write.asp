<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
<%
mode = request("mode")
page = request("page")
seq = request("seq")


If start_date = "" Then
	start_date = Date
End If 

If END_DATE = "" Then
	End_DATE = Date + 15
End If 


goParam = "?page=" & page

if mode = "EDIT" Then
	if seq = "" then
		GoBack("데이터 번호가 존재하지 않습니다.")
		response.end
	end if

	DBopen()
	sql = "USP_PMAA_BOARD_NOTICE_VIEW @MODE='VIEW', @SEQ=" & seq
	Set rs = DBcon.execute(sql)
	if rs.eof or rs.bof then
		GoBack("데이터가 존재하지 않습니다.")
		response.end
	else
		subject = rs(0)
		writer = rs(1)
		content = rs(2)
		filename = rs(3)
		news_gb = rs(4)
		isview = rs(5)
		
		arr = split(filename,"|")
  	org_filename = arr(0)
  	save_filename = arr(1)
		ext = arr(2)
		
	end if
	DBclose()

	btn_msg = "수정"
else	
	isview = "Y"
	news_gb = "N"

	mode = "ADD"

	btn_msg = "등록"
	writer = "관리자"
end if
%>
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/admin/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->
	<script type="text/javascript" src="/front_include/editor/ckeditor.js"></script>
  <script type='text/javascript'>
		$(function() {
			var fileTarget<%= replace(num,"-","_") %> = $('#file');

			fileTarget<%= replace(num,"-","_") %>.on('change', function(){  // 값이 변경되면
					var filename = "";

					if(window.FileReader){  // modern browser
							filename = $(this)[0].files[0].name;
					} else {  // old IE
							filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
					}

					// 추출한 파일명 삽입
					$('#main_img').val(filename);
			});


			var fileTarget<%= replace(num,"-","_") %> = $('#file2');

			fileTarget<%= replace(num,"-","_") %>.on('change', function(){  // 값이 변경되면
					var filename = "";

					if(window.FileReader){  // modern browser
							filename = $(this)[0].files[0].name;
					} else {  // old IE
							filename = $(this).val().split('/').pop().split('\\').pop();  // 파일명만 추출
					}

					// 추출한 파일명 삽입
					$('#mobile_img').val(filename);
			});

		});

		function addForm() {

			if($("#event_name").val() == "") {
				alertBoxFocus("이벤트명을 입력해 주세요.","event_name");
				return;
			}

			if($("#main_img").val() == "") {
				alertBoxFocus("메인이지지를 선택해주시기 바랍니다..","main_img");
				return;
			}

			if($("#file").val() != "") {
				var filename = $("#main_img").val().toLowerCase();
				var index = filename.lastIndexOf(".");
				var ext = filename.substring(index + 1);
				var chk = 0;
				var chkext = ["jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf"];
				var i = 0;
				for(i = 0; i < chkext.length; i++) {          
					if(chkext[i]== ext) {
						chk++;
						break;
					}
				}
				if(chk == 0) {
					alertBoxFocus("첨부파일은 JPG,GIF,PNG,DOC(DOCX),PPT(PPTX),XLS(XLSX),HWP,ZIP,PDF만 가능합니다.","file");
					return;
				}
			}
			
			if($("#start_date").val() == "") {
				alertBoxFocus("이벤트시작일자를 선택해주시기 바랍니다..","start_date");
				return;
			}

			if($("#end_date").val() == "") {
				alertBoxFocus("이벤트종료일자를 선택해주시기 바랍니다..","start_date");
				return;
			}


			confirmBox("저장하시겠습니까?",function() {
					var f = document.thisform;

					<% if mode = "ADD" then %>
						f.mode.value = "ADD";
					<% elseif mode = "EDIT" then %>
						f.mode.value = "EDIT";
					<% end if %>

					f.target = 'hiddenFrame';
					f.action = "event_proc.asp";
					f.submit();
			});


		}

		function delForm() {

			confirmBox("삭제하시겠습니까?",function() {
					var f = document.thisform;
					f.mode.value = "DEL";
					f.target = 'hiddenFrame';
					f.action = "event_proc.asp";
					f.submit();
			});

		}

		function deleteFile() {
			confirmBox("파일을 삭제하시겠습니까?",function() {
					hiddenFrame.location.href = "notice_delete_file.asp?seq=<%= seq %>";
			});
		}

	  
  </script>
  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">게시판관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">이벤트등록</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
	  <form name="thisform" id="thisform" method="post" action="notice_proc.asp" enctype="multipart/form-data">
		<input type="hidden" name="mode" id="mode" value="<%= mode %>">
		<input type="hidden" name="seq" id="seq" value="<%= seq %>">
		<input type="hidden" name="page" id="page" value="<%= page %>">

		  <!-- S: page-content -->
		  <div class="page-content">
			<!-- S: component 테이블 요소 -->
			<section class="component">
			  <!-- S: title-box -->
			  <!-- S: table -->
				<div class="fixed-table">
				  <h3 class="table-caption">이벤트 <%=btn_msg%></h3>
				  <!-- S: fix-head-table -->
				  <table class="table fix-head-table">
					<colgroup>
					  <col style="width:10%;">
					  <col style="width:90%;">
					</colgroup>
					<tbody>
						  <tr>
							<th class="menu" >이벤트 제목</th>
							<th >
								 <div class="col-lg-12">
								  <input type="text" class="form-control" placeholder="" id="event_name" name="event_name" value="" maxlength="100">
								</div>
							</th>          
						  </tr>
						  <tr>
							<th class="menu" >메인이미지</th>
							<th >
								 <div class="col-lg-12">
									<input type="text" name="main_img" id="main_img" class="form-control" placeholder="파일을 선택하세요" style="width:30%;display:inline-block;background:#ffffff;" value="<%= org_filename %>" readonly>
									<label type="button" for="file" class="btn btn-light active" style="width:110px;">Browse</label>
									<input type="file" name="file" id="file" style="position: absolute; width: 1px;  height: 1px;  padding: 0;  margin: -1px;  overflow: hidden;  clip:rect(0,0,0,0);  border: 0;">
								</div>
							</th>          
						  </tr>
<tr>
							<th class="menu" >메인이미지</th>
							<th >
								 <div class="col-lg-12">
									<input type="text" name="mobile_img" id="mobile_img" class="form-control" placeholder="파일을 선택하세요" style="width:30%;display:inline-block;background:#ffffff;" value="<%= org_filename %>" readonly>
									<label type="button" for="file2" class="btn btn-light active" style="width:110px;">Browse</label>
									<input type="file" name="file2" id="file2" style="position: absolute; width: 1px;  height: 1px;  padding: 0;  margin: -1px;  overflow: hidden;  clip:rect(0,0,0,0);  border: 0;">
								</div>
							</th>          
						  </tr>
						  
						  <tr>
							<th class="menu">이벤트기간</th>
							<th >
								 <div class="col-lg-12">
									  <input type="text" class="form-control datepicker-here input-block"  style="float:left;" data-language="ko" placeholder="<%=start_date%>" name="start_date" id="start_date" value="<%= start_date %>" readonly>
									  <div class="div_2">~</div>
									  <input type="text"  class="form-control datepicker-here input-block" style="float:left;" data-language="ko"  placeholder="<%=end_date%>" name="end_date" id="end_date" value="<%= end_date %>" readonly>
								</div>
							</th>          
						  </tr>	
					</tbody>
				  </table>
				  <!-- E: fix-head-table -->					
					<a href="#" class="btn btn-primary" onclick="addForm(); return false;"><%= btn_msg %></a>	 
				  &nbsp;&nbsp;
				  <a href="notice_list.asp<%= goParam %>" class="btn btn-dark">취소</a>	 
					
					<% if mode = "EDIT" then %>
					<a href="#" class="btn btn-danger" style="float:left;margin-top:10px;margin-right:10px;" onclick="delForm(); return false;">삭제</a>	 					
					<% end if %>

				</div>
			</form>

			<iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->


  <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>