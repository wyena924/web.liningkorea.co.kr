<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page = request("page")

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_B2BGOLFLINE_EVENT_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size 

'Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If


%>
  <script>

  </script>
</head>
<body>
  <!-- S: include page.header -->
	<!--#include virtual="/admin/include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!--#include virtual="/admin/include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">고객센터관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">이벤트관리</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
              <h3 class="table-caption">이벤트관리</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="120">
                  <col width="120">
                  <col width="250">
                  <col width="250">
                  <col width="120">
                  <col width="120">                 
                </colgroup>
                <tbody>
                  <tr>
                    <th>No</th>
                    <th>이벤트제목</th>
                    <th>메인이미지</th>
                    <th>모바일이미지</th>
                    <th>기간</th>
                    <th>등록일</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content">
                  <colgroup>
                    <col width="120">
                    <col width="120">
                    <col width="250">
                    <col width="250">
                    <col width="120">
                    <col width="120">               
                  </colgroup>
                  <tbody id="product_contents">
					<%
						for i = (page-1) * page_size To numList
						  seq			= arrList(2,i)
						  event_name	= arrList(3,i)
						  main_img		= arrList(4,i)
						  mobile_img	= arrList(5,i)
						  starg_date	= arrList(6,i)
						  end_date		= arrList(7,i)
						  reg_date		= arrList(8,i)
					%>
                    <tr>
                      <td><%= seq%></td>
                      <td><a href="notice_write.asp<%= goParam & "&mode=EDIT&seq=" & seq & "&page=" & page %>"><%= event_name%></a></td>
                      <td><%=main_img %></td>
                      <td><%=mobile_img %></td>
                      <td><%=starg_date%>~<%=end_date%></td>
                      <td><%=reg_date %></td>                      
                    </tr>
					<%
						h_number = h_number - 1
						next
					%>	
                  </tbody>
                </table>
                <!-- E: table-content -->
                </form>
              </div>
              <!-- E: scroll-body -->			          
                <a href="event_write.asp" class="btn btn-primary" >등록</a>
                <!--<a href="#" class="btn btn-primary" onclick="searchProduct(); return false;">상품검색/등록</a>-->
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!--#include virtual="/admin/include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>