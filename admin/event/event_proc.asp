<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<%
Set Upload = Server.CreateObject("TABSUpload4.Upload")
Upload.CodePage = 65001
Upload.MaxBytesToAbort = 10 * 1024 * 1024

root_path = Server.MapPath("/admin")


save_path = root_path & "\upload\event\"


'Upload.Start "c:\Temp"
Upload.Start save_path

'response.write save_path&"<br>"
'response.write "seq1 = " & request("seq")&"<br>"
writer  = GetsLOGINID()
mode	= Upload.Form("mode")
page	= Upload.Form("page")
seq		= Upload.Form("seq")

goParam = "?page=" & page

event_name = Upload.Form("event_name")
start_date = Upload.Form("start_date")
end_date   = Upload.Form("end_date")


if seq = "" then seq = 0

if mode <> "DEL" then
	if event_name = "" then
		%>
		<script>
			parent.alertBox("제목을 입력해 주세요.");
		</script>
		<%
		response.end
	end if
	if writer = "" then
		%>
		<script>
			parent.alertBox("작성자를 입력해 주세요.");
		</script>
		<%
		response.end
	end if
	if start_date = "" then
		%>
		<script>
			parent.alertBox("시작을을 입력해 주세요.");
		</script>
		<%
		response.end
	end if

	if end_date = "" then
		%>
		<script>
			parent.alertBox("시작을을 입력해 주세요.");
		</script>
		<%
		response.end
	end If
	
	Set main_img = Upload.Form("file")
	Set mobile_img = Upload.Form("file2")
		
	
	org_filename = main_img.FileName
	save_filename = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename,3)
	ext = LCase(mid(org_filename,InstrRev(org_filename,".")+1))

	org_filename_mobile = mobile_img.FileName
	save_filename_mobile = CStr(year(now))+ZeroCheck(month(now),2)+ZeroCheck(day(now),2)+ZeroCheck(hour(now),2)+ZeroCheck(minute(now),2)+ZeroCheck(second(now),2)+"_"+session.sessionID+"_"+replace(seq,"-","_")+"."+right(org_filename_mobile,3)
	ext_mobile = LCase(mid(org_filename_mobile,InstrRev(org_filename_mobile,".")+1))


	if org_filename <> "" then
		chkext = array("jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf")
		chk = 0
		for i = 0 to UBound(chkext)
		if ext = chkext(i) then
			chk = chk + 1
			exit for
		end if
		next

		if chk = 0 then  
			%>
			<script>
				parent.alertBox("JPG, GIF, PNG, DOC(DOCX), PPT(PPTX), XLS(XLSX), HWP, ZIP, PDF 파일만 <br>업로드가 가능합니다.");
			</script>
			<%
			response.end
		end if

		if org_filename = "" then
			tmp_save_filename= "||"
		else
			tmp_save_filename = org_filename & "|" & save_filename & "|" & ext
		end if
			
		
		main_img.SaveAs save_path & save_filename , False


		Response.Write save_filename

	else
		tmp_save_filename= "||"
	end if
	

	if org_filename_mobile <> "" then
		chkext = array("jpg","jpeg","gif","png","doc","docx","ppt","pptx","xls","xlsx","hwp","zip","pdf")
		chk = 0
		for i = 0 to UBound(chkext)
		if ext = chkext(i) then
			chk = chk + 1
			exit for
		end if
		next

		if chk = 0 then  
			%>
			<script>
				parent.alertBox("JPG, GIF, PNG, DOC(DOCX), PPT(PPTX), XLS(XLSX), HWP, ZIP, PDF 파일만 <br>업로드가 가능합니다.");
			</script>
			<%
			response.end
		end if

		if org_filename_moblie = "" then
			tmp_save_filename_moblie= "||"
		else
			tmp_save_filename_moblie = org_filename_moblie & "|" & save_filename_moblie & "|" & ext_moblie
		end if
			
		mobile_img.SaveAs save_path & save_filename_moblie , False
	else
		tmp_save_filename_moblie= "||"
	end if
	'Response.Write File.FileName & "<br>"
	'Response.Write File.SaveName

	Set File = nothing
end if

DBopen()


sql = "USP_B2B_GOLFLINE_EVENT_ACCESS " & _
			" @MODE				= '" & mode & "'" & _
			",@SEQ				=  " & seq & _
			",@EVENT_NAME		= '" & event_name & "'" & _
			",@WRITER			= '" & writer & "'" & _
			",@MAIN_IMG			= '" & main_img & "'" & _
			",@MOBILE_IMG		= '" & mobile_img & "'" & _
			",@START_DATE		= '" & start_date & "'" & _
			",@END_DATE			= '" & end_date & "'" 
			'response.write sql&"<br>"
			'Response.end
Set rs = DBcon.Execute(sql)
if rs(0) = 0 then
	%>
	<script>
		parent.alertBox("데이터 처리에 문제가 발생했습니다.");
	</script>
	<%
	response.end
end if

DBclose()

%>

<script>
	parent.alertBox("정상 처리되었습니다.",function() {
		parent.location.href = "event_list.asp<%= goParam %>";
	});
</script>