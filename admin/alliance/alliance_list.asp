<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page = request("page")

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()

sql = "USP_PMAA_ALIANCE_NOTICE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size 

Response.write sql
'Response.end
Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If
%>
  <script>
		function alliance_view(seq)
		{
			document.getElementById("content_div_"+ seq).style.display ='block'
		}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">고객센터관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">1:1 문의</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->

      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
              <h3 class="table-caption">1:1 문의</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="120">
				  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">
                  <col width="120">               
                </colgroup>
                <tbody>
                  <tr>
                    <th width="120">No</th>
					<th width="120">제목</th>
                    <th width="120">문의자명</th>
                    <th width="120">이메일</th>
                    <th width="120">내용</th>
                    <th width="120">등록일</th>
                    <th width="120">답변여부</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="align:center;">
                  <colgroup>
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">
                    <col width="120">                  
                  </colgroup>
                  <tbody id="product_contents">
					<%
						for i = (page-1) * page_size To numList
						  seq			= arrList(2,i)
						  subject		= arrList(3,i)
						  writer		= arrList(4,i)
						  email			= arrList(5,i)
						  content		= arrList(6,i)
						  create_date	= arrList(7,i)
						  re_content	= arrList(8,i)	

					%>
                    <tr>
                      <th width="120"><%=seq%></th>
					  <th width="120"><%=subject%></th>
					  <th width="120"><%=writer %></th>
                      <th width="120"><%=email %></th>
                      <th width="120" align="center">			
						<input type="button" value="내용보기" onclick="javascript:location.href='alliance_write.asp?seq=<%=seq%>'">
					  </th>
                      <th width="120"><%=create_date %></th>
                      <th width="120">
						<% If re_content <> "" Then %>
							답변완료
						<% Else %>
							미답변
						<% End If %>
					  </th>                      
                    </tr>
					<%

						h_number = h_number - 1

						next
					%>	
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>