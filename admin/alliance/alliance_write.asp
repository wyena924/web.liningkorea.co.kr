<!--#include virtual="/pmaa/admin/Library/config.asp"-->
<!--#include virtual="/pmaa/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include file = "../include/header.top.asp" -->
<!-- E: include header.top.asp -->
<%
mode = request("mode")
page = request("page")
seq = request("seq")

'1 : 어드민 이름 (로그인한 사용자)
ADMIN_NAME = request.Cookies("widline_admin_login")("AGT_ID")

'Response.write admin_id

goParam = "?page=" & page

DBopen()

'2 : 해당 프로시저 
sql = "USP_PMAA_ALIANCE_NOTICE_VIEW @MODE='VIEW', @SEQ=" & seq  & ", @ADMIN_NAME= '"&ADMIN_NAME &"'"


Set rs = DBcon.execute(sql)

if rs.eof or rs.bof then
	GoBack("데이터가 존재하지 않습니다.")
	response.end
else
	 seq			= rs("Seq")
	 subject		= rs("subject")
	 SEQ			= rs("SEQ")
	 SUBJECT		= rs("SUBJECT")
	 WRITER			= rs("WRITER")
	 CONTENT		= rs("CONTENT")
	 RE_CONTENT		= rs("RE_CONTENT")
	 FILENAME		= rs("FILENAME")
	 ISTOP			= rs("ISTOP")
	 ISVIEW			= rs("ISVIEW")
	 HIT			= rs("HIT")
	 DELETE_YN		= rs("DELETE_YN")
	 CREATE_DATE	= rs("CREATE_DATE")
	 UPDATE_DATE	= rs("UPDATE_DATE")
	 DELETE_DATE	= rs("DELETE_DATE")
	 ONLINE_CD		= rs("ONLINE_CD")
	 USER_SEQ		= rs("USER_SEQ")
	 EMAIL			= rs("EMAIL")
	 ONLINE_ID		= rs("ONLINE_ID")
	 CM_TEL			= RS("CM_TEL")
end if
DBclose()

btn_msg = "수정"

%>
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include common_depth -->
  <!-- #include virtual = "/front_include/common_depth.asp" -->
  <!-- S: include header.setting.asp -->
  <!-- #include file = "../include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->
  <script type="text/javascript" src="/front_include/editor/ckeditor.js"></script>
  <script language='javascript'>
	function write_ok()
	{
		var re_content = document.getElementById("re_content")
		if (re_content.value.length ==0)
		{
			alert('답변내용을 입력해 주시기 바랍니다.');
			re_content.focus();
			return;
		}
		document.bform.action="alliance_proc.asp"
		document.bform.submit();
		
	}
  </script>
  <form name="bform" method="post">
  <input type="hidden" name="seq" id="seq" value="<%=seq%>">
  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">게시판관리</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">1:1문의</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
		  <!-- S: page-content -->
		  <div class="page-content">
			<!-- S: component 테이블 요소 -->
			<section class="component">
			  <!-- S: title-box -->
			  <!-- S: table -->
				<div class="fixed-table">
				  <h3 class="table-caption">1:1문의</h3>
				  <!-- S: fix-head-table -->
				  <table class="table fix-head-table">
					<colgroup>
					  <col style="width:10%;">
					  <col style="width:90%;">
					</colgroup>
					<tbody>
						<tr>
							<th class="menu" >등록일</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<%=CREATE_DATE%>
								</div>
							</th>          
						</tr>
						<tr>
							<th class="menu" >문의자</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<%=writer%>
								</div>
							</th>          
						</tr>
						<tr>
							<th class="menu" >문의자아이디</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<%=ONLINE_ID%>
								</div>
							</th>          
						</tr>
						<tr>
							<th class="menu" >문의자이메일</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<%=email%>
								</div>
							</th>          
						</tr>
						<tr>
							<th class="menu" >내용</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<%=content%>
								</div>
							</th>          
						</tr>
						
						<tr>
							<th class="menu" >답변달기</th>
							<th colspan="3">
								 <div class="col-lg-12">
									<textarea style="width:100%; height:100px;" id="re_content" name="re_content"><%=re_content%></textarea>
								</div>
							</th>          
						</tr>
					</tbody>
				  </table>		
				  <a href="#" class="btn btn-primary" onclick="write_ok();">답변달기</a>	 
				  <a href="alliance_list.asp<%= goParam %>" class="btn btn-dark">뒤로가기</a>	 
				</div>
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->
  </form>

  <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>