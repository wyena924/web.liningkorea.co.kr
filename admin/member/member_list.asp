<!--#include virtual="/admin/Library/config.asp"-->
<!--#include virtual="/admin/Library/config_lhs.asp"-->
<!-- S: include header.top.asp -->
<!-- #include virtual = "/admin/include/header.top.asp" -->
<!-- E: include header.top.asp -->
  <title>삼성삼판 어드민</title>
  <%
    nDepth = 1 'css 및 img, DOM depth'
    virtual_Depth = "" '무조건 최상단'
  %>
  <!-- S: include header.setting.asp -->
  <!-- #include virtual = "/admin/include/header.setting.asp" -->
  <!-- E: include header.setting.asp -->
<%
page		= request("page")
ORDER_NM	= request("ORDER_NM")
CM_TEL		= request("CM_TEL")
CM_EMAIL	= request("CM_EMAIL")
CM_ADDR		= request("CM_ADDR")		

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 10
block_page = 10	

goParam = "?tmp=null"

Dbopen()
sql = "USP_MALL_ADMIN_MEMBER_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size &", @ORDER_NM = '"& ORDER_NM &"',@CM_TEL='"&CM_TEL &"', @CM_EMAIL='"&CM_EMAIL&"' ,@CM_ADDR='"&CM_ADDR &"', @GRP1 = '"&GLOBAL_VAR_GRP1&"' ,@GRP2='"&GLOBAL_VAR_GRP2&"' "

'Response.write sql
'Response.end

Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If
%>
  <script>
	function alliance_view(seq)
	{
		document.getElementById("content_div_"+ seq).style.display ='block'
	}

	function open_popup(or_seq)
	{
		window.open("http://www.itemcenter.co.kr/WEB/ic_order_cust.asp?seq="+or_seq+"&unigrpcd=2001&tp=01&emailkey=&gdscusttp=notview&DEL_YN=N", "window", "scrollbars=YES,width=800,height=800");
	}

	
	
	function cncl_change(ornum,seq) {

	
		if(confirm("주문번호"+ornum+" 을(를) 취소하시겠습니까?")==false)
			return 1;
		//parent.fBottom.popupOpen("","","처리중입니다!");

		var strAjaxUrl="/admin/ajax/order_cncl_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: {ornum:ornum,seq:seq },

			success: function(retDATA) {
				if(retDATA){
					//var strcut = retDATA.split("|>");
					if (retDATA == "TRUE") {
						alert("취소가 완료되었습니다! 재조회 후 확인하십시오.");
						location.reload();
					}

					if (retDATA == "CNCL") {

						alert("이미 취소 상태입니다!");
						parent.fBottom.popupClose("","","");
					}

					if (retDATA != "TRUE" && retDATA != "CNCL") {
						parent.fBottom.popupClose("","","");
						alert ("취소중에 오류가 발생하였습니다!");
					}
				}
			}, error: function(xhr, status, error){
				
				parent.fBottom.popupClose("","","");
				alert ("취소중 에러발생 - 시스템관리자에게 문의하십시오!"+' ['+error+']');
			}
		});
	}
	function search()
	{
		document.search_form.submit();
	}

	function search_keypress()
	{
		if (window.event.keyCode == 13) {
 
             search()
        }
	}

	function login_div_open(order_seq)
	{
		//alert(document.getElementById("login_div_" + order_seq).innerHTML.length );
		var strAjaxUrl="/admin/ajax/member_login_list.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: {
				order_seq:order_seq
			},

			success: function(retDATA) {
				if(retDATA){
					if (document.getElementById("login_div_" + order_seq).innerHTML == '')
					{
						document.getElementById("login_div_" + order_seq).innerHTML = retDATA;
					}
					else
					{	
						document.getElementById("login_div_" + order_seq).innerHTML = "";
					}

				}
			}, error: function(xhr, status, error){
				
				parent.fBottom.popupClose("","","");
				alert ("취소중 에러발생 - 시스템관리자에게 문의하십시오!"+' ['+error+']');
			}
		});
	}

	function excel_down()
	{
		// alert('준비중입니다');
		// return;
		document.excel_form.action="member_excel_list.asp"
		document.excel_form.submit();
	}
  </script>
</head>
<body>
  <!-- S: include page.header -->
  <!-- #include file = "../include/page.header.asp" -->
  <!-- E: include page.header -->

  <!-- S: left-nav -->
  <!-- #include file = "../include/leftNav.asp" -->
  <!-- E: left-nav -->

  <!-- S: main -->
  <div class="main">
    <!-- S: breadcrumb -->
    <ul class="breadcrumb">
      <li>
        <a href="#">HOME</a>
      </li>
      <li>
        <a href="#" data-location="design">회원</a>
      </li>
      <li>
        <a href="#" data-location="table" class="active">회원리스트</a>
      </li>
    </ul>
    <!-- E: breadcrumb -->
    <!-- S: top-search -->
    <div class="top-search">
      
    </div>
    <!-- E: top-search -->

    <!-- S: main-panel -->
    <div class="main-panel">
      <!-- S: page-title -->
      <div class="page-title">
        <!--h2>할인쿠폰 목록</h2-->
        <!--p>어드민에서 사용되는 테이블 디자인 입니다.</p-->
      </div>
      <!-- E: page-title -->
	  <form name="excel_form">
		<input type="hidden" id="e_order_nm" name="e_order_nm" value="<%=ORDER_NM%>">
		<input type="hidden" id="e_cm_tel" name="e_cm_tel" value="<%=CM_TEL%>">
		<input type="hidden" id="e_cm_email" name="e_cm_email" value="<%=CM_EMAIL%>">
		<input type="hidden" id="e_cm_addr" name="e_cm_addr" value="<%=CM_ADDR%>">
	  </form>
	  <form name="search_form" method="post">
	
	
      <!-- S: page-search -->
	  <table class="page-search">
		<tr>
			<td>
				<div class="Palal">회원명</div>
			</td>
			<td>
				<input type="text" class="textBox" id="or_nm" name="order_nm" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">전화번호</div>
			</td>
			<td>
				<input type="text" class="textBox" id="cm_tel" name="cm_tel" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">이메일주소</div>
			</td>
			<td>
				<input type="text" class="textBox" id="cm_email" name="cm_email" onkeypress="javascript:search_keypress();">
			</td>
			<td>
				<div class="Palal">주소</div>
			</td>
			<td>
				<input type="text" class="textBox" id="cm_addr" name="cm_addr" onkeypress="javascript:search_keypress();">
			</td>
		</tr>
	  </table>
	  <input class="search_button"  type="button" value="Search" onclick="javascript:search();">
	  <input class="exel_button"  type="button" value="Excel" onclick="javascript:excel_down();">
	  </form>
      <!-- S: page-content -->
      <div class="page-content">
        <!-- S: component 테이블 요소 -->
        <section class="component">
          <!-- S: title-box -->
          <!-- S: table -->
			<div class="fixed-table">
			  <DIV class="total_div" >TOTAL : <%=total_cnt%></div>
              <h3 class="table-caption">회원리스트</h3>
              <!-- S: fix-head-table -->
              <table class="table fix-head-table">
                <colgroup>
                  <col width="80">
				  <col width="120">
				  <col width="120">
                  <col width="120">
				  <col width="80">
				  <col width="120">
				  <col width="120">
				  <col width="120">
                </colgroup>
                <tbody>
                  <tr>
                    <th width="80">NO</th>
					<!--th width="120" style="text-align:center;">회원명</th-->
					<th width="120" style="text-align:center;">ID</th>
                    <!--th width="120" style="text-align:center;">전화번호</th-->
					<th width="80" style="text-align:center;">로그인횟수</th>
					<th width="120" style="text-align:center;">회원가입일</th>
					<th width="120" style="text-align:center;">마지막로그인</th>
                    <th width="120" style="text-align:center;">로그인보기</th>
                  </tr>
                </tbody>
              </table>
              <!-- E: fix-head-table -->

              <!-- S: scroll-body -->
              <div class="scroll-body">
                <!-- S: table-content -->
                <form name="thisform" id="thisform" method="post" action="main_product_write_proc.asp" >
                <table class="table table-striped table-hover table-content" style="float:center;">
                  <colgroup>
					  <col width="80">
					  <!--col width="120"-->
					  <col width="120">
					  <!--col width="120"-->
					  <col width="80">
					  <col width="120">
					  <col width="120">
					  <col width="120">
                  </colgroup>
                  <tbody>
					<%
						for i = (page-1) * page_size To numList
						  order_seq	    = arrList(2,i)
						  order_nm	    = arrList(3,i)
						  online_id		= arrList(4,i)
						  cm_tel		= arrList(5,i)
						  cm_email		= arrList(6,i)
						  cm_addr 		= arrList(7,i)
						  order_cnt		= arrList(8,i)
						  gd_vol		= arrList(9,i)
						  order_dt		= arrList(10,i)
						  login_cnt		= arrList(11,i)
						  writedate		= arrList(12,i)
						  reg_date		= arrList(13,i)
					%>
					 <tr>
						<th width="80">
							<%=order_seq%>
						</th>
						<!--th width="120" style="text-align:center;">
							<%=order_nm%>
						</th-->
						<th width="120" style="text-align:left;"><%=online_id %></th>
						<!--th width="120" style="text-align:left;"><%=cm_tel %></th-->
						<th width="80" style="text-align:left;">
							<%=login_cnt%>
						</th>
						<th width="120" style="text-align:center;">
							<%=reg_date%>				
						</th>
						<th width="120" style="text-align:center;">
							<%=writedate%>				
						</th>

						<th width="120" style="text-align:center;">
							<input type="button" value="로그인내역" onclick="javascript:login_div_open('<%=order_seq%>');">	<br>
							<div id="login_div_<%=order_seq%>"></div>
						</th>
					  </tr>
					<%

						h_number = h_number - 1

						next
					%>	
                </table>
                <!-- E: table-content -->
                </form>
              </div>
            </div>
            <!-- E: fixed-table -->

            <!-- S: comp-part -->
          <div class="comp-part">
             <!-- S: page-index -->
            <div class="page-index">
              <!-- S: pagination -->
              
              <!--
              <ul class="pagination">
                <li class="page-item"><a href="#" class="page-link">이전</a></li>
                <li class="page-item active"><a href="#" class="page-link">1</a></li>
                <li class="page-item"><a href="#" class="page-link">2</a></li>
                <li class="page-item"><a href="#" class="page-link">3</a></li>
                <li class="page-item"><a href="#" class="page-link">다음</a></li>
              </ul>
              -->
              <% Pageing(goParam) %>

              <!-- E: pagination -->
            </div>
            <iframe name="hiddenFrame" style="wwidth:0px;height:0px;" frameborder="0" border="0" scrolling="no"></iframe>
            <!-- E: page-index -->
          </div>
			
          </div>          
        </section>
        <!-- E: component 테이블 요소 -->
      </div>
      <!-- E: page-content -->
    </div>
    <!-- E: main-panel -->
  </div>
  <!-- E: main -->

    <!-- S: include footer.asp -->
  <!-- #include file = "../include/footer.asp" -->
  <!-- E: include footer.asp -->
</body>
</html>