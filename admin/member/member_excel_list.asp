<!--#include virtual="/admin/Library/config.asp"-->
<%
filename = "member_excel"  & Year(now()) & Month(now()) & Day(now())
Response.Buffer = TRUE
Response.ContentType = "application/vnd.ms-excel"
Response.AddHeader "Content-disposition","attachment;filename=" & filename & ".xls"

page		= request("page")
ORDER_NM	= request("ORDER_NM")
CM_TEL		= request("CM_TEL")
CM_EMAIL	= request("CM_EMAIL")
CM_ADDR		= request("CM_ADDR")		

if page = "" then page = 1

Dim page_size, block_page, total_cnt, total_page, h_number

page_size = 99999
block_page = 10	

goParam = "?tmp=null"

Dbopen()
sql = "USP_MALL_ADMIN_MEMBER_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size &", @ORDER_NM = '"& ORDER_NM &"',@CM_TEL='"&CM_TEL &"', @CM_EMAIL='"&CM_EMAIL&"' ,@CM_ADDR='"&CM_ADDR &"', @GRP1 = '"&GLOBAL_VAR_GRP1&"' ,@GRP2='"&GLOBAL_VAR_GRP2&"' "

'Response.write sql
'Response.end

Set rs = DBcon.Execute(sql)
If rs.Eof or rs.Bof Then
	numList = -1
Else
	total_cnt = rs(0)
	total_page = rs(1)
		
	h_number = total_cnt - ((page -1) * page_size)
			
	arrList = rs.GetRows
	numList = Ubound(arrList,2)
End If
%>
<table BORDER=1 style="font-style: normal; font-weight: normal; width:1200px; ">
  <colgroup>
    <col width="40">
	  <col width="200">
	  <col width="400">
	  <col width="200">
	  <col width="200">
	  <col width="200">
	  <col width="200">
  </colgroup>
  <tbody>
    <tr>
		<th width="80">NO</th>
		<!--th width="200" style="text-align:center;">회원명</th-->
		<th width="400" style="text-align:center;">ID</th>
        <!--th width="200" style="text-align:center;">전화번호</th-->
		<th width="200" style="text-align:center;">로그인횟수</th>
		<th width="200" style="text-align:center;">회원가입일</th>
		<th width="200" style="text-align:center;">마지막로그인</th>
    </tr>
		<%
			for i = (page-1) * page_size To numList
			  order_seq	    = arrList(2,i)
			  order_nm	    = arrList(3,i)
			  online_id		= arrList(4,i)
			  cm_tel		= arrList(5,i)
			  cm_email		= arrList(6,i)
			  cm_addr 		= arrList(7,i)
			  order_cnt		= arrList(8,i)
			  gd_vol		= arrList(9,i)
			  order_dt		= arrList(10,i)
			  login_cnt		= arrList(11,i)
			  writedate		= arrList(12,i)
			  reg_date		= arrList(13,i)
		%>
		 <tr>
			<th width="200">
				<%=order_seq%>
			</th>
			<!--th width="200" style="text-align:center;">
				<%=order_nm%>
			</th-->
			<th width="400" style="text-align:center;"><%=online_id %></th>
			<!--th width="120" style="text-align:center;"><%=cm_tel %></th-->
			<th width="200" style="text-align:center;">
				<%=login_cnt%>
			</th>
			<th width="200" style="text-align:center;">
				<%=reg_date%>				
			</th>
			<th width="200" style="text-align:center;">
				<%=writedate%>				
			</th>

		  </tr>
		<%
			h_number = h_number - 1
			next
		%>	
  </tbody>
</table>