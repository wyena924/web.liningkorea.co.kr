<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	or_num = request("or_num")
	
	If or_num = "" Then 
		or_num = "2001201912914254213"
	End If 

	strqry =  "select	"
	strqry = strqry & "		a.or_num,	"
	strqry = strqry & "		sum(gd_price) as gd_price,	"
	strqry = strqry & "		isnull(c.cnt,0) as cnt ,	"
	strqry = strqry & "		isnull(SUM(PY_IN_PAY),0) AS PY_IN_PAY ,	"
	strqry = strqry & "		MAX(GD_MEMO) AS GD_LINK	"
	strqry = strqry & "	from ic_t_order_gd a	"
	strqry = strqry & "	inner join ic_t_order_gd_sub b	"
	strqry = strqry & "	on a.or_num = b.or_num	"
	strqry = strqry & "	left outer join (select order_gd_seq	"
	strqry = strqry & "						  , count(*) as cnt 	"
	strqry = strqry & "						  , SUM(PY_IN_PAY) AS  PY_IN_PAY 	"
	strqry = strqry & "						  from  itemcenter.dbo.IC_T_ORDER_PAY 	"
	strqry = strqry & "						  group by order_gd_seq ) c	"
	strqry = strqry & "	on a.seq = c.ORDER_GD_SEQ	"
	strqry = strqry & " LEFT OUTER JOIN (SELECT  GD_MEMO, SEQ FROM IC_T_GDS_INFO WHERE GD_GRP_FIRST= 'Z8' AND GD_GRP_MID = '006') D "
	strqry = strqry & "	ON B.GD_SEQ = D.SEQ "
	strqry = strqry & "	where a.or_num ='"&or_num&"'	"
	strqry = strqry & "	group by a.or_num ,c.cnt "
	
	
	'Response.write strqry
	DBopen()
		Set rs = Dbcon.Execute(strqry)
	DBclose()
%>
<script language='javascript'>
	
	//alert('핸드폰 구매를 하신 고객님께서는 하단 휴대폰구매정보입력 버튼을 누르신 후 가입자 정보를 입력해주시기 바랍니다.');

	function on_link(link)
	{
		document.bform.target="_blank"
		document.bform.action=link
		document.bform.submit()
	}
</script>
<form name="bform" method="post">
	
</form>
<div class="container sub">
	<div class="page_tit">
		<h2>주문완료</h2>
		<div class="order_load">
			<span>장바구니</span>
			<span>주문결제</span>
			<span class="active">주문완료</span>
		</div><!-- order_load e -->
	</div><!-- page_tit e -->
	<div class="order_page sub_bg">
		<div class="order_end">
			<p class="tit">"주문이 정상적으로 <br class="sbr"/>완료되었습니다."</p>
			<p class="sub_tit">자세한 구매내역은 <span>주문/배송조회</span>에서 <br class="sbr"/>확인하실 수 있습니다.</p>
			<div class="order_info">
				<table cellpadding="0" cellspacing="0" class="order_info01" summary="" >
					<thead>
						<tr>
							<th scope="">주문번호</th>
							<th scope="">주문금액</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td scope=""><%=rs("or_num")%></td>
							<td scope="" class="point_red"><b><%=FormatNumber(rs("gd_price"),0)%></b>원</td>
						</tr>
					</tbody>
				</table>
				
				<table cellpadding="0" cellspacing="0" class="order_info02" summary="" >
					<tbody>
						<tr>
							<th>입금은행</th>
							<td class="first">기업은행</td>
						</tr>
						<tr>
							<th>입금계좌</th>
							<td><b>220-045977-04-037</b> (예금주: 위드라인)</td>
						</tr>
						<tr>
							<th>입금금액</th>
							<td class="point_red"><%=FormatNumber(rs("gd_price") - RS("PY_IN_PAY") ,0)%></b>원</td>
						</tr>
						<tr>
							<th>입금기한</th>
							<td>주문 날짜로부터 5일 이내에 입금하지 않을 경우 취소됩니다. </td>
						</tr>
					</tbody>
				</table>
				
			</div>
			<div class="btn_wrap" >
				<a href="/" class="btn btn_b" style="width:200px;">쇼핑계속하기</a>
				<a href="/mypage/delivery_detail.asp?or_num=<%=or_num%>" class="btn btn_b btn_agree"  style="width:200px;">주문내역확인</a>
			</div><!-- btn_wrap e -->
		</div>
	</div><!-- order_page e -->
</div><!-- container e -->
<!-- #include virtual = "include/footer.asp" -->
