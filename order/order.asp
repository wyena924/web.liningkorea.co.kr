<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<STYLE type=text/css>
#myProgress {
    position: relative;
    width: 100%;
    height: 50px;
    background-color: grey;
}
#myBar {
    position: absolute;
   width: 0%;
    height: 100%;
   background-color: green;
} 
</style>
<%
	'로그인 체크(로그인후 호출URL)
	'ChkLogin("/kdbmall_new/main/index_"&Request.Cookies("gubun")&".asp")
	Cart_YN  = Request("Cart_YN")
	GD_SEQ   = Request("GD_SEQ")


	If GD_SEQ = "" Then 
		Response.Write "<script>alert('잘못된 경로로 접근하셨습니다.\n올바른 경로로 이용해 주세요.');location.href='../default.asp';</script>"
		Response.End
	End If 


	If Cart_YN = "Y" Then 
		'장바구니에서 구매페이지 이동	
		'장바구니에 담긴 아이템 갯수 체크
		Item_Count = Request("Item_Count")
		ST_SEQ     = Request("ST_SEQ_A")
		WRAP_SEQ   = Request("WRAP_SEQ_A")
		GD_EA    = Request("GD_EA_A")
		'주문후 카트를 비우기 위해 
		Cart_YN    = "Y"
	Else		
		'바로구매 일때 카운트 셋팅
		Item_Count = 1
		ST_SEQ   = Request("ST_SEQ")
		WRAP_SEQ = Request("WRAP_SEQ")
		GD_EA    = Request("GD_VOL")
		Cart_YN    = "N"
	End If 

	dim p_protocol : p_protocol = Request.ServerVariables("HTTPS"	) 
	
	If p_protocol = "off" Then
		Response.Write "<script>alert('잘못된 경로로 접근하셨습니다.\n올바른 경로로 이용해 주세요.');location.href='../default.asp';</script>"
		Response.End
	End If 
%>
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<script language="javascript">
 
	 function openWin(aURL) { 
		  if (!op || op.closed ) { 
			
		  } else{ 
			alert('결제 팝업을 띄운 상태에서는 리스트로 갈 수 없습니다.');
			   op.focus(); 
		  } 
	  } 


	history.pushState(null, null, location.href);
	window.onpopstate = function(event) {
		history.go(1, openWin());
	};


	function noEvent() {
	 if (event.keyCode == 116) {
	 event.keyCode= 2;
		alert('주문페이지 에서는 새로고침을 할 수 없습니다.');
	 return false;
	 }
	 else if(event.ctrlKey && (event.keyCode==78 || event.keyCode == 82))
	 {
		 alert('주문페이지 에서는 새로고침을 할 수 없습니다.');
	 return false;
	 }
	}
	document.onkeydown = noEvent;



function execDaumPostCode(input_type) {
	new daum.Postcode({
	oncomplete: function(data) {
	// 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.
	// 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
	// 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
	var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
	var extraRoadAddr = ''; // 도로명 조합형 주소 변수

	// 법정동명이 있을 경우 추가한다.
	if(data.bname !== ''){
	extraRoadAddr += data.bname;
	}
	// 건물명이 있을 경우 추가한다.
	if(data.buildingName !== ''){
	extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
	}
	// 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
	if(extraRoadAddr !== ''){
	extraRoadAddr = ' (' + extraRoadAddr + ')';
	}
	// 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
	if(fullRoadAddr !== ''){
	fullRoadAddr += extraRoadAddr;
	}

	// 우편번호와 주소 정보를 해당 필드에 넣는다.
	//if(input_type=="1"){
		//document.getElementById("cm_zip").value = data.zonecode;
		//document.getElementById("cm_addr1").value = data.roadAddress;
			//if(data.jibunAddress != ''){
				//document.getElementById("cm_addr1").value = data.jibunAddress;
			//}else{
				//document.getElementById("cm_addr1").value = fullRoadAddr;
			//}
		if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
			document.getElementById("zip").value = data.zonecode;
			document.getElementById("addr1").value = data.roadAddress;
			document.getElementById("addr2").value = '';
			document.getElementById("addr2").focus();

		} else { // 사용자가 지번 주소를 선택했을 경우(J)
			document.getElementById("zip").value = data.postcode1 + data.postcode2;;
			document.getElementById("addr1").value = data.jibunAddress;
			document.getElementById("addr2").value = '';
			document.getElementById("addr2").focus();
		}

	

	// 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
	if(data.autoRoadAddress) {
		//예상되는 도로명 주소에 조합형 주소를 추가한다.
		var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
	} else if(data.autoJibunAddress) {
		var expJibunAddr = data.autoJibunAddress;
	} else {
		}
	}
	}).open();
}

function trim(str) {
	return str.replace(/^\s\s*/, '').replace(/\s\s*$/, '');
}

function order_button()
{
	document.getElementById("order_button").style.display ='block';
}

function order_ok()
	{
		if(confirm("주문 하시겠습니까?")){
			var f= document.getElementById("Orderfrm")
			var user_name = document.getElementById("user_name").value
			var cust_email1 = document.getElementById("cust_email1").value
			var cust_email2 = document.getElementById("cust_email2").value
			var tel1 = document.getElementById("tel1").value
			var tel2 = document.getElementById("tel2").value
			var tel3 = document.getElementById("tel3").value
			var zip = document.getElementById("zip").value
			var addr1 = document.getElementById("addr1").value
			var addr2 = document.getElementById("addr2").value
			var delivery_memo = document.getElementById("delivery_memo").value	
			var gd_seq = document.getElementById("ingdseq").value	  
			var Item_Count = document.getElementById("Item_Count").value
			var P_EA	= document.getElementById("P_EA").value	
			var radio01 = document.getElementById("radio01").checked
			//var radio02 = document.getElementById("radio02").checked
			var or_else_memo = document.getElementById("or_else_memo").value 
			if(trim(user_name).length == 0)
			{
				alert('주문자 명을 입력해 주시기 바랍니다.');
				document.getElementById("user_name").focus();
				return;
			}

			if(trim(cust_email1).length == 0)
			{
				alert('이메일 주소1을 입력해 주시기 바랍니다.');
				document.getElementById("cust_email1").focus();
				return;
			}
			if(trim(cust_email2).length == 0)
			{
				alert('이메일 주소2를 입력해 주시기 바랍니다.');
				document.getElementById("cust_email2").focus();
				return;
			}

			if(trim(tel1).length == 0)
			{
				alert('연락처1을 입력해 주시기 바랍니다.');
				document.getElementById("tel1").focus();
				return;
			}

			if(trim(tel2).length == 0)
			{
				alert('연락처2를 입력해 주시기 바랍니다.');
				document.getElementById("tel2").focus();
				return;
			}
			if(trim(tel3).length == 0)
			{
				alert('연락처3을 입력해 주시기 바랍니다.');
				document.getElementById("tel3").focus();
				return;
			}
			
			if(trim(zip).length == 0)
			{
				alert('주소를 입력해 주시기 바랍니다.');
				return;
			}
			if(trim(addr1).length == 0)
			{
				alert('주소를 입력해 주시기 바랍니다.');
				return;
			}

			//주문 버튼을 누른후 잠깐동안 버튼을 숨깁니다.
			document.getElementById("order_button").style.display ='none';
			

			var agent = navigator.userAgent.toLowerCase();

			//if (agent.indexOf("msie") > -1 || agent.indexOf("trident") > -1)
			//{
			//} else {

			//	alert('현재 접속하신 브라우저는 internet Explorer 버전이 아닙니다.\ninternet Explorer에서 접속하시어 주문해주시기 바랍니다.');
			//	return;
			//}
			var strcut = new Array();
			var strstcnt
			var strgdlist
			var strsumseq
			var strzerocnt		
			if (strzerocnt > 0) {
				alert('주문수량이 0 이거나 빈공백이 존재하여 주문이 불가능합니다!');
				return;
			}
			var lblsendamt = document.getElementById('lblsendamt')
			document.getElementById('inorderlist').value = strgdlist;
			
			var priceSum = document.getElementById('lblsendamt')
			var order_sum = priceSum.innerHTML.replace(/,/gi,'').replace(' 원','') //금액
			
			
			if (document.getElementById('order_term_chk').checked == false)
				{
					alert('약관에 동의하여 주시기 바랍니다. ');
					return;
				}
			
			//document.Orderfrm.target="order_iframe"
			document.Orderfrm.action ="order_step1.asp"
			document.Orderfrm.submit()
			//f= document.getElementById("Orderfrm")
			//f.action ='order_ok.asp';		
			//f.submit();
		}
	}

function getLocalUrl(mypage) 
	{ 
		var myloc = location.href
		return myloc.substring(0, myloc.lastIndexOf('/')) + '/' + mypage;
	} 

function ordersubmit(strisscdnm,strtrno,test,mobile_gb)
{
	var f= document.getElementById("Orderfrm")
	var user_name = document.getElementById("user_name").value
	var cust_email1 = document.getElementById("cust_email1").value
	var cust_email2 = document.getElementById("cust_email2").value
	var tel1 = document.getElementById("tel1").value
	var tel2 = document.getElementById("tel2").value
	var tel3 = document.getElementById("tel3").value
	var zip = document.getElementById("zip").value
	var addr1 = document.getElementById("addr1").value
	var addr2 = document.getElementById("addr2").value
	var delivery_memo = document.getElementById("delivery_memo").value	
	var gd_seq = document.getElementById("ingdseq").value	  
	var Item_Count = document.getElementById("Item_Count").value
	var P_EA	= document.getElementById("P_EA").value	
	var mobile_gb = document.getElementById("mobile_gb").value	
	var or_else_memo = document.getElementById("or_else_memo").value	 
	var company_file= document.getElementById("company_file").value	 
	f.isscdnm.value = strisscdnm;	
	f.trno.value = strtrno;

	if(trim(user_name).length == 0)
	{
		alert('주문자 명을 입력해 주시기 바랍니다.');
		document.getElementById("user_name").focus();
		return;
	}

	if(trim(cust_email1).length == 0)
	{
		alert('이메일 주소1을 입력해 주시기 바랍니다.');
		document.getElementById("cust_email1").focus();
		return;
	}
	if(trim(cust_email2).length == 0)
	{
		alert('이메일 주소2를 입력해 주시기 바랍니다.');
		document.getElementById("cust_email2").focus();
		return;
	}

	if(trim(tel1).length == 0)
	{
		alert('연락처1을 입력해 주시기 바랍니다.');
		document.getElementById("tel1").focus();
		return;
	}

	if(trim(tel2).length == 0)
	{
		alert('연락처2를 입력해 주시기 바랍니다.');
		document.getElementById("tel2").focus();
		return;
	}
	if(trim(tel3).length == 0)
	{
		alert('연락처3을 입력해 주시기 바랍니다.');
		document.getElementById("tel3").focus();
		return;
	}
	
	if(trim(zip).length == 0)
	{
		alert('주소를 입력해 주시기 바랍니다.');
		return;
	}
	if(trim(addr1).length == 0)
	{
		alert('주소를 입력해 주시기 바랍니다.');
		return;
	}
	
	if (trim(company_file).length == 0)
	{
		alert('인증자료를 선택해 주시기 바랍니다.');
		document.getElementById("company_file").focus();
		return;
	}

	var agent = navigator.userAgent.toLowerCase();

	//if (agent.indexOf("msie") > -1 || agent.indexOf("trident") > -1)
	//{
	//} else {

	//	alert('현재 접속하신 브라우저는 internet Explorer 버전이 아닙니다.\ninternet Explorer에서 접속하시어 주문해주시기 바랍니다.');
	//	return;
	//}
	var strcut = new Array();
	var strstcnt
	var strgdlist
	var strsumseq
	var strzerocnt		
	if (strzerocnt > 0) {
		alert('주문수량이 0 이거나 빈공백이 존재하여 주문이 불가능합니다!');
		return;
	}
	var lblsendamt = document.getElementById('lblsendamt')
	document.getElementById('inorderlist').value = strgdlist;

	if (document.getElementById('order_term_chk').checked == false)
		{
			alert('약관에 동의하여 주시기 바랍니다. ');
			return;
		}
		//ar strAjaxUrl="/ajax/order_ok.asp"
		//ar retDATA="";
		///alert(strAjaxUrl);
		//$.ajax({
		//	 type: 'POST',
		//	 url: strAjaxUrl,
		//	 dataType: 'html',
		//	 data: {
		//		user_name:user_name,
		//		cust_email1:cust_email1	,
		//		cust_email2:cust_email2	,
		//		tel1:tel1,
		//		tel2:tel2,
		//		tel3:tel3,
		//		zip:zip,
		//		addr1:addr1,
		//		addr2:addr2,
		//		delivery_memo:delivery_memo,
		//		gdorderlist:strgdlist,
		//		gd_seq:gd_seq,
		//		Item_Count:Item_Count,
		//		P_EA:P_EA,
		//		isscdnm:strisscdnm,
		//		strtrno:strtrno,
		//		mobile_gb:mobile_gb,
		//		or_else_memo:or_else_memo
		//
		//	},						  
		//	success: function(retDATA) {
		//		if(retDATA)
		//			{
		//				if(retDATA!="")
		//				{
		//					alert('주문이 완료 되었습니다');
		//					location.href="order_finish.asp?or_num="+retDATA
		//				}
		//			}
		//	 }
		//}); //close $.ajax(
	document.Orderfrm.target="order_iframe"
	document.Orderfrm.encoding = "multipart/form-data"; 
	document.Orderfrm.action ="order_ok.asp"
	document.Orderfrm.submit()
	//f= document.getElementById("Orderfrm")
	//f.action ='order_ok.asp';		
	//f.submit();

}


//결제 데이터통신을 ajax로 변경 (from 으로 왔다갔다 하는정보가 중간에 손실되는걸 확인하였음 예)www.있고 없고에따라 크롬의 경우 부모영억을 못찾음)

function eparamSet(rcid, rctype, rhash){
	document.getElementById("Orderfrm").reWHCid.value 	= rcid;
	document.getElementById("Orderfrm").reWHCtype.value  = rctype  ;
	document.getElementById("Orderfrm").reWHHash.value 	= rhash  ;
}


function payment()
{
	var f= document.getElementById("Orderfrm");
	var user_name = document.getElementById("user_name").value
	var cust_email1 = document.getElementById("cust_email1").value
	var cust_email2 = document.getElementById("cust_email2").value
	var tel1 = document.getElementById("tel1").value
	var tel2 = document.getElementById("tel2").value
	var tel3 = document.getElementById("tel3").value
	var zip = document.getElementById("zip").value
	var addr1 = document.getElementById("addr1").value
	var addr2 = document.getElementById("addr2").value
	var delivery_memo = document.getElementById("delivery_memo").value	
	var gd_seq = document.getElementById("ingdseq").value	  
	var Item_Count = document.getElementById("Item_Count").value
	var P_EA	= document.getElementById("P_EA").value	
	var or_else_memo = document.getElementById("or_else_memo").value	 
	var company_file= document.getElementById("company_file").value	 
	f.action ="http://kspay.ksnet.to/store/mb2/KSPayPWeb_utf8.jsp"
	f.encoding = "application/x-www-form-urlencoded"; 
	var filter = "win16|win32|win64|mac";
	if(navigator.platform){
	if(0 > filter.indexOf(navigator.platform.toLowerCase()))
		{
			alert('pc를 통해 주문해주시기 바랍니다');
			return;
		}
	else
		{
			
			//2. pc버전
			var priceSum = document.getElementById('lblsendamt')
			document.getElementById('sndGoodname').value	= document.getElementById('Hidden_GD_NM').value
			document.getElementById("sndOrdername").value	= document.getElementById("user_name").value  //사용자명
			document.getElementById("sndAmount").value		=	priceSum.innerHTML.replace(/,/gi,'').replace(' 원','') //금액
			document.getElementById("sndEmail").value		=  document.getElementById("cust_email1").value + '@' + document.getElementById("cust_email2").value  //이메일			
			document.getElementById("sndMobile").value		=  document.getElementById('tel1').value+document.getElementById('tel2').value+document.getElementById('tel3').value
			//document.getElementById("mobile_gb").value		="W"
			var substring_text= "user_name=" + escape(user_name) + "&cust_email1="+cust_email1+"&cust_email2="+cust_email2+"&tel1="+tel1+"&tel2="+tel2+"&tel3="+tel3+"&zip="+zip+"&addr1="+escape(addr1)+"&addr2="+escape(addr2)
	+"&delivery_memo="+escape(delivery_memo)+"&gd_seq="+gd_seq+"&Item_Count="+Item_Count+"&P_EA="+P_EA+"&mobile_gb=W&or_else_memo="+escape(or_else_memo)
			document.getElementById("sndReply").value		= getLocalUrl("/../../Pc_Payment/kspay_wh_rcv.asp?" + substring_text) ;

			var agent		= navigator.userAgent;
			var midx		= agent.indexOf("MSIE");
			var out_size	= (midx != -1 && agent.charAt(midx+5) < '7');
			
			var width_	= 500;
			var height_	= out_size ? 568 : 518;

			var left_	= screen.width;
			var top_	= screen.height;
			
			left_ = left_/2 - (width_/2);
			top_ = top_/2 - (height_/2);
			
			op = window.open('about:blank','AuthFrmUp','height='+height_+',width='+width_+',status=yes,scrollbars=no,resizable=no,left='+left_+',top='+top_+'');
			if (op == null)
			{
				alert("팝업이 차단되어 결제를 진행할 수 없습니다.");
				return false;
			}
			move2();
			document.getElementById("Orderfrm").encoding = "application/x-www-form-urlencoded"
			document.getElementById("Orderfrm").target = 'AuthFrmUp';
			document.getElementById("Orderfrm").action ='https://kspay.ksnet.to/store/KSPayFlashV1.3/KSPayPWeb.jsp?sndCharSet=utf-8';						
			document.getElementById("Orderfrm").submit();

		}

}

}
//숫자입력체크
function chk_Number(){
	if ((event.keyCode<48)||(event.keyCode>57)) event.returnValue=false;
}


//천단위 콤마
function addcomma(n) {
  var reg = /(^[+-]?\d+)(\d{3})/;   // 정규식
  n += '';                          // 숫자를 문자열로 변환

  while (reg.test(n))
    n = n.replace(reg, '$1' + ',' + '$2');

  return n;
}

function select_ch(this_is)
{
	document.getElementById("cust_email2").value = this_is.value;
}

function delivery_select(this_is)
{
	document.getElementById('delivery_memo').value= this_is.value
	if (this_is.value=='')
	{
		document.getElementById('delivery_memo').focus();
	}

}

function detail_location(url)
{
	 if (!op || op.closed ) { 
		location.href= "/product/detail.asp?seq=" + url
	 } else{ 
		alert('결제 팝업을 띄운 상태에서는 상세페이지로 이동할 수 없습니다.');
		  op.focus(); 
	 } 

	
}

function or_else_memo_con(gb)
{
	if (gb=='Y')
	{
		document.getElementById('or_else_memo_div').style.display ='block'
	}
	else
	{
		document.getElementById('or_else_memo_div').style.display ='none'
	}
}

function text_2_con()
{
	var or_else_memo = document.getElementById("or_else_memo").value
	if (or_else_memo =='미발행')
	{
		document.getElementById("text_2").style.display = 'none'
	}
	else
	{
		document.getElementById("text_2").style.display= 'block'
	}
}


function move() 
{

	var elem = document.getElementById("myBar"); 
	var width = 1;
	var id =setInterval(frame, 1000);
	function frame() {
		if (width >= 50) {
		   clearInterval(id);
		} else {
		   width++; 
		   elem.style.width = width + '%'; 
		}
	}
}

function move2() 
{
	var elem = document.getElementById("myBar"); 
	var width = 1;
	var id =setInterval(frame, 1);
	function frame() {
		elem.style.width = '100%'; 
	}
	setTimeout("order_button()",1000);
}


</script>
<iframe class="order_iframe" name="order_iframe" id="order_iframe" width="0" height="0" frameborder=0></iframe>
<form method="post" name="Orderfrm" id="Orderfrm" >		
<input type="hidden" id="gd_cate_result1" name="gd_cate_result1">
<input type="hidden" name="P_EA" id="P_EA" value="<%=GD_EA%>" />
<input type="hidden" class="SPANDATA" id=ingdseq name=GD_SEQ value="<%=GD_SEQ%>">
<input type="hidden" class="SPANDATA" id=GD_SEQ_RE name=GD_SEQ_RE>
<input type="hidden" id=basong name=basong>
<input type="hidden" id=maxprice name=maxprice>
<input type="hidden" id=direct_gubun name=direct_gubun>
<input type="hidden" id=reset_gubun name=reset_gubun value="<%=reset_gubun %>">
<input type="hidden" id=repace1 name=repace1 value="<%=repace1%>">
<input type="hidden" name=main_gb value="<%=main_gb%>">
<!-- 여기부터 결제 관련-->
	<input type="hidden" name="CST_MID" id="CST_MID" value="lgdacomxpay"/>
	<input type="hidden" name="CST_PLATFORM" id="CST_PLATFORM" value="test"/>
	<input type="hidden" name="LGD_BUYER" id="LGD_BUYER" value="홍길동"/>
	<input type="hidden" name="LGD_PRODUCTINFO" id="LGD_PRODUCTINFO" value="myLG070-인터넷전화기"/>
	<input type="hidden" name="LGD_AMOUNT" id="LGD_AMOUNT" value="50000"/>
	<input type="hidden" name="LGD_BUYEREMAIL" id="LGD_BUYEREMAIL" value=""/>
	<input type="hidden" name="LGD_OID" id="LGD_OID" value="test_1234567890020"/>
	<input type="hidden" name="LGD_TIMESTAMP" id="LGD_TIMESTAMP" value=""/>
	<input type="hidden" name="LGD_CUSTOM_USABLEPAY" id="LGD_CUSTOM_USABLEPAY" value=""/>
	<input type="hidden" name="LGD_WINDOW_TYPE" id="LGD_WINDOW_TYPE" value="iframe"/>
	<input type="hidden" name="LGD_CUSTOM_SWITCHINGTYPE" id="LGD_CUSTOM_SWITCHINGTYPE" value="IFRAME"/>
<!-- 여기까지 -->
<div class="container sub">
	<div class="page_tit">
		<h2>주문결제</h2>
		<div class="order_load">
			<span>장바구니</span>
			<span class="active">주문결제</span>
			<span>주문완료</span>
		</div><!-- order_load e -->
	</div><!-- page_tit e -->
	<div class="order_page sub_bg">
		<input type="hidden" name="Item_Count" id="Item_Count" value="<%=Item_Count%>" />
		<div class="order_wrap">
			<h3>주문상품 정보</h3>
			<ul class="order_list">
			  <%
				'상품 갯수가 여러개라면 배열로 반환
				
				If Item_Count > 1 Then 
				  Array_GD_SEQ = Split(Trim(GD_SEQ),",")								
				  Array_ST_SEQ = Split(Trim(ST_SEQ),",")								
				  Array_WRAP_SEQ = Split(Trim(WRAP_SEQ),",")								
				  Array_GD_EA = Split(Trim(GD_EA),",")								
				End If 
				
		  
				'로딩시 총합계금액 
				Total_Price = 0
				Total_Price_Sum = 0
				'면세상품합계
				No_Tax_Price = 0



				For i = 0 To (Item_Count - 1)
				
				  If Item_Count > 1 Then 
					GD_SEQ   = Array_GD_SEQ(i)
					ST_SEQ   = Array_ST_SEQ(i)
					WRAP_SEQ = Array_WRAP_SEQ(i)
					GD_EA    = Array_GD_EA(i)
				  Else
					GD_SEQ   = GD_SEQ
					ST_SEQ   = ST_SEQ
					WRAP_SEQ = WRAP_SEQ
					GD_EA    = GD_EA					
				  End If 
				  
							'Product_View(ONLINE_CD,GD_NM,GD_SEQ,PRICE_TYPE)
				  Set PRs = Product_View(GLOBAL_VAR_ONLINECD,"",GD_SEQ,"")
				  
		  
				  Product_Sum = CLng(PRs("GD_PRICE3"))* CLng(GD_EA)	

							'================== 개별 상품 부가세 면세 관련 금액 합계 처리 =======================================================
							If PRs("Tax_Free") = "Y" Then 
								'공급가액
								Product_Provide = CLng(PRs("GD_PRICE3")) * CLng(GD_EA)
								Product_Tax = "0"
								'비과세 상품 합계
								No_Tax_Price = CDbl(No_Tax_Price) + CDbl(Product_Provide)
							End If 
				'						Response.Write No_Tax_Price
				'						Response.End
							'================== 상품 부가세 면세 관련 금액 합계 처리 =======================================================							

				%>
				<%
					If i = 0 Then 
				%>
					<input type="hidden" name="Hidden_GD_NM" id="Hidden_GD_NM" value="<%=PRs("GD_NM")%>">
				<%
					End If 
				%>
				<input type="hidden" name="Delivery_YN" id="Delivery_YN" value="<%=Delivery_YN%>">
				<li>
					<div class="img_cont">
						<a href="javascript:detail_location('<%=PRs("SEQ")%>');">
							<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" alt=""/>
						</a>
					</div><!-- img_cont e -->
					<div class="list_info">
						<div class="product_name">
							<a href="javascript:detail_location('<%=PRs("SEQ")%>');"><%=PRs("GD_NM")%></a>
						</div>
						<div class="amount_wrap">
							<div class="amount_cont">
								<%=GD_EA%>개
							</div>
							<div class="option">
								<% If PRs("GD_OPTION1") = "" And PRs("GD_OPTION2") <> "" Then %>
										옵션 : <%=PRs("GD_OPTION2")%>
								<% ElseIf PRs("GD_OPTION1") <> "" And  PRs("GD_OPTION2") = ""  Then %>
										옵션 : <%=PRs("GD_OPTION1")%>
								
								<% ElseIf PRs("GD_OPTION1") <> "" And  PRs("GD_OPTION2") <> ""  Then %>
										옵션 : <%=PRs("GD_OPTION1")%>/<%=PRs("GD_OPTION2")%>
								<% End If %>
							</div>
						</div>
					</div><!-- list_info e -->
					<div class="price">
						<strike><%=FormatNumber(PRs("GD_PRICE8"),0) %>원</strike><br/><b><%= FormatNumber(Product_Sum,0)%></b>원
					</div>
				</li><!-- line 1 e -->
				<% 
					  '상품의 합계
					  Total_Price = Total_Price + Product_Sum
					Next
					
					'배송료 계산
					If Total_Price < 30000 Then 
					  Delivery_Price = 0
					  Delivery_YN    = "Y"
					Else
					  Delivery_Price = 0
					  Delivery_YN    = "N"
					End If 
					
					'상품과 배송료 합계
					Total_Price_Sum = Total_Price + Delivery_Price
					Session("INI_PRICE") = Total_Price_Sum '"1004" '결제 금액 =>  결제 처리 페이지에서 체크 하기 위해 세션에 저장 (또는 DB에 저장)하여 다음 결제 처리 페이지 에서 체크)
				 %>
			</ul><!-- cart_list e -->
			<div class="total_wrap">
				<div class="total">
					<dl class="product_price">
						<dt>상품금액</dt>
						<dd><b><%=FormatNumber(Total_Price,0)%></b>원</dd>
					</dl>
					<dl class="delivery_price">
						<dt>배송비</dt>
						<dd><b><%=FormatNumber(Delivery_Price,0)%></b>원</dd>
					</dl>
					<dl class="total_price">
						<dt>총 결제금액</dt>
						<dd><b id="lblsendamt"><%=FormatNumber(Total_Price_Sum,0)%></b>원</dd>
					</dl>
				</div>
			</div><!-- total_wrap e -->
			<div class="order_form">
				<!--h3>주문자 정보</h3>
				<div class="cont">
					<dl>
						<dt>이름</dt>
						<dd>
							<label for="" class="hide">이름</label>
							<input type="text" id="" class="wid_257" title=""  name=""/>
						</dd>
					</dl>
					<dl class="mail_cont">
						<dt>메일주소</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" id="" class="" title=""  name=""/><b class="mg">@</b> 
							<label for="" class="hide"></label>
							<input type="text" id="" class="" title=""  name=""/>&nbsp;&nbsp;
							<div class="select_box">
								<label for="mail01">직접입력</label>
								<select id="mail01" name="">
									<option selected="selected">직접입력</option>
									<option>naver.com</option>
									<option>nate.com</option>
									<option>daum.net</option>
									<option>gmail.com</option>
								</select>
							</div><!-- select_box e -->
						<!--/dd>
					</dl>
					<dl class="phone_cont">
						<dt >연락처</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" id="" class="" title=""  name=""/><b class="mg">-</b>
							<label for="" class="hide"></label>
							<input type="text" id="" class="" title=""  name=""/><b class="mg">-</b>
							<label for="" class="hide"></label>
							<input type="text" id="" class="" title=""  name=""/>
						</dd>
					</dl>
				</div-->

				<h3>
					주문정보&nbsp;<font color="#1428a0">(*&nbsp;필수정보는 반드시 입력해주시기 바랍니다.)</font>
					<!--span>
						<input type="checkbox" id="normal_chk" class="" title=""  name=""/>
						<label for="normal_chk">주문자 정보와 동일</label>
					</span-->
				</h3>
				<div class="cont">
					<dl>
						<dt><font color="#1428a0">*</font>&nbsp;이름</dt>
						<dd>
							<label for="" class="hide">이름</label>
							<input type="text"  class="wid_257" title=""  name="user_name" id="user_name" value="<%=GetsCUSTNM()%>" >
						</dd>
					</dl>
					<%
						If InStr(GetsCUSTEMAIL(),"@") > 0 Then 
							If GetsCUSTEMAIL() = "" Or GetsCUSTEMAIL() = "@" Then 
								cm_email1 = ""
								cm_email2 = ""
							Else 
								Array_Email = Split(GetsCUSTEMAIL(),"@")
								cm_email1 = Array_Email(0)
								cm_email2 = Array_Email(1)
							End If 
						Else
							cm_email1 = ""
							cm_email2 = ""
						End If 
					%>
					<dl class="mail_cont">
						<dt><font color="#1428a0">*</font>&nbsp;메일주소</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" class="" title=""  name="cust_email1" id="cust_email1" maxlength="50" value="<%=cm_email1%>"/><b class="mg">@</b> 
							<label for="" class="hide"></label>
							<input type="text" class="" title=""  name="cust_email2" id="cust_email2"  maxlength="50" value="<%=cm_email2%>"/>&nbsp;&nbsp;
							<div class="select_box">
								<label for="mail02">직접입력</label>
								<select id="mail02" name="email_select" id="email_select" onchange="javascript:select_ch(this);">
									<option  selected="selected">직접입력</option>
									<option value="naver.com">naver.com</option>
									<option value="nate.com">nate.com</option>
									<option value="daum.net">daum.net</option>
									<option value="gmail.com">gmail.com</option>
								</select>
							</div><!-- select_box e -->
						</dd>
					</dl>
					<%
						If GetsCUSTHP() = "" Or GetsCUSTHP() = "-" Or GetsCUSTHP() = "--" Then 
							cm_tel1 = ""
							cm_tel2 = ""
							cm_tel3 = ""
						Else

							cm_tel1 = Left(GetsCUSTHP() ,3)
							cm_tel2 = mid(GetsCUSTHP() ,5,4)
							cm_tel3 = right(GetsCUSTHP() ,4)
						End If  
					%>
					<dl class="phone_cont">
						<dt><font color="#1428a0">*</font>&nbsp;연락처</dt>
						<dd>
							<label for="" class="hide"></label>
							<input type="text" class="" title=""  name="tel1" id="tel1" maxlength="4" value="<%=cm_tel1%>"/><b class="mg"  >-</b>
							<label for="" class="hide"></label>
							<input type="text" class="" title=""  name="tel2" id="tel2" maxlength="4" value="<%=cm_tel2%>"/><b class="mg"  >-</b>
							<label for="" class="hide"></label>
							<input type="text" class="" title=""  name="tel3" id="tel3"  maxlength="4" value="<%=cm_tel3%>"/>
						</dd>
					</dl>
					<%
						cm_zip  = GetsCUSTZIP()
					%>
					<dl class="address_cont">
						<dt><font color="#1428a0">*</font>&nbsp;주소</dt>
						<dd>
							<label for="" class="hide">우편번호</label>
							<input type="text" class="post" title=""   name="zip" id="zip" value="55555" readonly/>
							<input type="button" id="" class="" title="" value="주소검색" name=""  onclick="execDaumPostCode('1');"/>
							<label for="" class="hide">메인주소</label>
							<input type="text" class="wid_l mg" title=""  name="addr1" id="addr1"   maxlength="100"  value="경기도 고양시 덕양구 동산동"/>
							<!--label for="" class="hide">상세주소</label-->
							<input type="hidden" class="wid_l"    title=""  name="addr2" id="addr2"   placeholder="상세주소를 입력해주세요." maxlength="100"  value="351-8 2층 202호"/>
						</dd>
					</dl>
					<dl class="request_cont">
						<dt>배송시 요청사항</dt>
						<dd>
							<div class="select_box">
								<label for="request">배송메세지를 선택해주세요.</label>
								<select id="request" onchange="javascript:delivery_select(this);">
									<option  selected="selected">배송메세지를 선택해주세요.</option>
									<option value="부재시 경비실에 맡겨주세요.">부재시 경비실에 맡겨주세요.</option>
									<option value="부재시 문앞에 놓아주세요.">부재시 문앞에 놓아주세요.</option>
									<option value="파손의 위험이 있는 상품이오니,배송 시 주의해주세요.">파손의 위험이 있는 상품이오니,배송 시 주의해주세요.</option>
									<option value="배송전에 연락주세요.">배송전에 연락주세요.</option>
									<option value="택배함에 넣어주세요.">택배함에 넣어주세요.</option>
									<option value="" >직접입력</option>
								</select>
							</div><!-- select_box e -->
							<label for="request_show" class="hide"></label>
							<input type="text" class="wid_l" title=""  name="delivery_memo" id="delivery_memo"/>
						</dd>
					</dl>
				</div>

				<h3>결제수단</h3>
				<div class="cont">
					<dl class="pay_cont">
						<dt>결제수단</dt>
						<dd>
							<input type="radio" id="radio01" name="input_radio" rel="radio01_tab" checked/>
							<label for="radio01" class="radio">카드결제</label>
							<a href="#pop_info_card" id="radio01_tab" class="btn_card choice_cont open" style="display:inline;">신용카드할부안내</a>
							<br class="sbr"/>
							<!--input type="hidden" name="radio02" id="radio02" value=""onclick="javascript:or_else_memo_con('N');" >
							<input type="radio" id="radio02" name="input_radio" rel="radio02_tab" onclick="javascript:or_else_memo_con('Y');" />
							<label for="radio02" class="radio">무통장 입금</label-->
							<label id="or_else_memo_div" class="radio" style="display:none;">현금영수증&nbsp;
								<div class="select_box">
								<label for="mail02">발행</label>
								<select name="or_else_memo" id="or_else_memo" onchange="javascript:text_2_con();">
									<option value="" selected="selected">발행</option>
									<option value="">미발행</option>
								</select>
								
							</div><!-- select_box e -->
							<br>
								<label id="text_2" style="height:50px;">현금영수증 승인 후 문자안내 드립니다.</label>
							</label>
							<br/>
							<!--span id="radio02_tab" class="choice_cont">입금정보 : 기업은행 220-045977-04-037 위드라인</span-->
							<!--input type="radio" id="radio03" name="input_radio" rel="radio03_tab" />
							<label for="radio03" class="radio">휴대폰구매</label><br/-->
						</dd>
					</dl>
				</div>
				<input type=hidden class="SPANDATA" id="inlistcnt" style="width:100px;top:<%=strtop%>px;left:389px;TEXT-ALIGN:left;" value="<%=strlistcnt%>">
				<input type=hidden class="SPANDATA" id="inorderlist" name="gdorderlist" style="width:100px;top:<%=strtop%>px;left:389px;TEXT-ALIGN:left;" >		
				<h3>약관 동의</h3>
				<div class="cont order_term_wrap">
					<dl>
						<dt class="tit">서비스 이용약관</dt>
						<dd class="cont" ID="service">
㈜위드라인 서비스 이용 약관<br />
㈜위드라인 서비스 이용 약관(&quot;이용 약관&quot;)에는 서비스 공급자인 ㈜위드라인 와 서비스 사용자인 고객(&quot;고객&quot; 또는 &quot;사용자&quot;) 간의 법적 관계를 관할하고 규정하는 조항이 자세히 설명되어 있습니다.<br />
&nbsp;<br />
사용자는 본 이용 약관을 자세히 읽은 후 이용 약관에 동의하지 않을 경우, 본 이용 약관에 동의하거나 서비스에 등록 또는 액세스하거나 이를 이용(서비스 &quot;이용&quot;으로 통칭)하지 말아야 합니다. (주)위드라인은 고객과 체결한 본 이용 약관의 개별 사본을 저장하지 않으므로 고객이 본인 기록용으로 본 약관의 사본을 저장해두는 것이 좋습니다.<br />
&nbsp;<br />
고객이 (a) 민법상 미성년자이거나(단, 서비스에 따라 가입 또는 서비스 가능 연령은 다를 수 있습니다). (b) 대한민국 법률에 따라 서비스를 받을 수 없도록 금지된 사람인 경우에는 일부 서비스 이용이 제한될 수 있습니다.<br />
&nbsp;<br />
본 이용 약관에서 &quot;서비스&quot;란 (주)위드라인의 http://b2bc.samsungbizmall.com에 자세히 설명되어 있고 경우에 따라 업데이트되는 모든 서비스를 의미하며, 별도의 계약에 따라 고객에게 제시 또는 제공되는 서비스는 제외됩니다.<br />
&nbsp;<br />
[이용 약관 동의 방법]<br />
(주)위드라인의 이용 약관 변경 방법 및<br />
(주)위드라인 또는 고객의 이용 약관 해지 방법<br />
&nbsp;<br />
&nbsp;<br />
1. 이용 약관 동의<br />
&nbsp;<br />
1.1. 고객이 서비스 이용 허가를 받으려면 이용 약관 및 경우에 따라 특별 약관에 동의해야 합니다. 일반적으로 이용 약관 및 특별 약관은 각 서비스의 사용자 인터페이스를 통해 구현된 &quot;동의&quot; 버튼을 클릭하시거나, 이와 유사하게 구현된 기능을 활용하여 동의합니다.<br />
&nbsp;<br />
1.2. 또한 고객이 서비스를 실제로 이용하기 시작하면 본 이용 약관 및 특별 약관에 동의하는 것이 됩니다. 이 경우 고객은 서비스를 이용하기 시작하는 동시에 본 이용 약관이 적용되며 (주)위드라인이 본 이용 약관에 따라 고객을 대우한다는 사실을 이해합니다.<br />
&nbsp;<br />
2. 이용 약관에 대한 변경<br />
&nbsp;<br />
2.1. ㈜위드라인은 수시로 본 이용 약관을 수정하거나 변경할 수 있습니다. 약관이 변경되는 경우 ㈜위드라인은 새로운 약관의 사본 및 변경사유를 시행 10일 전(사용자에게 불리하거나 중대한 사항의 변경은 30일 전)에 http://b2bc.samsungbizmall.com에서 공지하고, 기존 회원에게는 필요시 이메일 발송 등 적절한 방법으로 통지합니다. 만약, 회원의 연락처 미기재, 변경 등으로 인하여 개별 통지가 어려운 경우에는 http://b2bc.samsungbizmall.com에 변경 약관을 공지함으로써 개별 통지한 것으로 봅니다.<br />
&nbsp;<br />
2.2. 고객이 변경된 약관의 효력 발생일까지 약관 변경에 대한 거부의사를 명시적으로 표시하지 않거나 약관 변경 이후에 서비스를 이용할 경우 변경된 약관에 동의한 것으로 봅니다. 고객이 변경된 약관에 동의하지 않는 경우, 고객이 서비스 이용이 제한되거나 정지될 수 있습니다.<br />
&nbsp;<br />
3. 이용 약관 해지 및 해지의 결과<br />
&nbsp;<br />
3.1. 본 이용 약관은 고객이나 (주)위드라인이 해지하지 않는 한 해지 시까지 계속 유효합니다.<br />
&nbsp;<br />
3.2. 고객은 ㈜위드라인 계정 홈페이지(http://b2bc.samsungbizmall.com)를 방문, 고객 계정으로 로그인 후 프로필에 접속하여 언제든지 계정 탈퇴를 할 수가 있습니다.<br />
&nbsp;<br />
3.3. ㈜위드라인은 등록 데이터의 일부로 ㈜위드라인에 제공된 이메일 주소를 사용하거나 고객이 해지 통지를 받을 수 있는 기타 적절한 방식을 통해 30일 이전에 해지에 관한 서면 통지를 제공하여 언제든지 본 이용 약관을 해지할 수 있습니다.<br />
&nbsp;<br />
3.4. 다음과 같은 경우 (주)위드라인은 통지 기간을 지키지 않고 언제든지 본 계약을 해지할 수 있습니다.<br />
&nbsp;<br />
a. 고객이 이용 약관을 위반한 경우<br />
&nbsp;<br />
b. 고객이 이용 약관을 준수할 의도가 없음을 명백히 표시한 경우(직접 또는 행동이나 진술 또는 기타 방식을 통하든 관계 없음)<br />
&nbsp;<br />
c. ㈜위드라인 또는 ㈜위드라인에게 또는 ㈜위드라인과 함께 서비스를 제공하는 ㈜위드라인의 공급자 또는 파트너가 서비스 또는 그 일부의 제공을 종료하기로 결정하거나 (전 세계 또는 고객의 거주 국가 또는 서비스 이용 국가에서), (주)위드라인의 공급자나 파트너가 (주)위드라인과의 모든 관계를 해지하기로 결정하는 경우 ( (주)위드라인 또는 (주)위드라인의 공급자나 파트너가 서비스 또는 그 일부를 고객이나 (주)위드라인에 제공하거나 (주)위드라인과 함께 제공하는 일이 더 이상 상업적으로 가능하지 않다고 판단하는 경우를 포함하여 이러한 해지의 사유와 관계 없음)<br />
&nbsp;<br />
d. (주)위드라인 또는 (주)위드라인에게 또는 (주)위드라인과 함께 서비스를 제공하는 (주)위드라인의 공급자나 파트너가 관련 법률에 따라 서비스나 그 일부의 제공을 종료해야 하는 경우(예를 들어 관련 법률의 변경 또는 법원의 판결이나 판정에 따라 서비스 또는 그 일부가 불법이 되거나 불법으로 간주되는 경우)<br />
&nbsp;<br />
3.5. 본 이용 약관의 해지는 본 이용약관의 유효 기간 중 발생하거나 초래된 고객 또는 (주)위드라인의 권리, 의무 및 책임에 아무런 영향을 미치지 않습니다.<br />
&nbsp;<br />
3.6. 고객은 수정된 이용 약관에 동의하지 않거나, 고객이나 (주)위드라인에 의한 해지 등으로 본 이용 약관이 종료된 후에는 서비스를 이용할 수 없습니다.<br />
&nbsp;<br />
3.7. 그러나 고객이 수정된 이용 약관에 동의하지 않기로 결정하거나 3.2항에 따라 고객이 본 이용 약관을 해지한 경우 고객은 서비스를 이용하는 과정에서 (주)위드라인에 저장된 사용자 콘텐츠를 백업할 수 있습니다. 고객은 (주)위드라인이 합리적인 백업 기간(&quot;백업 허용 기간&quot;)이 경과한 후 사용자 콘텐츠를 삭제할 수 있다는 점을 이해합니다. 각 서비스 및 특정 사용자 콘텐츠 백업에 필요한 노력에 따라 다른 백업 허용 기간이 적용될 수 있습니다.<br />
&nbsp;<br />
&nbsp;<br />
[고객의 서비스 이용 방법 및<br />
(주)위드라인의 고객 콘텐츠 사용 방법 또는 고객의 (주)위드라인 콘텐츠 사용 방법]<br />
&nbsp;<br />
4. 서비스 제공 및 이용 제한<br />
&nbsp;<br />
4.1. 서비스는 (주)위드라인이 고객에게 제공합니다.<br />
&nbsp;<br />
4.2. 본 이용 약관에 달리 명시되지 않는 한 고객은 개인적이며 비상업적인 용도와 목적으로만 서비스를 이용할 수 있으며 모든 서비스 또는 그 일부를 가공, 복제, 복사, 판매, 거래 또는 재판매할 수 없습니다.<br />
&nbsp;<br />
4.3. 서비스 이용 시 고객은 항상 본 이용 약관 및 거주지 또는 서비스 이용 관할지를 포함한 관련 관할지의 관련 법률이나 규정을 준수해야 합니다.<br />
&nbsp;<br />
4.4. (주)위드라인은 언제든지 자신의 독자적인 판단에 따라 사전 경고 또는 통지 없이 다음을 수행할 수 있습니다.<br />
&nbsp;<br />
a. 서비스 변경 또는 서비스나 그 일부의 제공을 일시 중단 및 또는 종료<br />
&nbsp;<br />
b. 고객의 사용자 계정 및 고객의 계정에 포함된 파일 또는 기타 콘텐츠에 대한 액세스를 포함한 서비스 이용을 일시적 또는 영구적으로 비활성화 또는 중단<br />
&nbsp;<br />
c. 서비스를 통해 고객이 보내거나 받는 전송 횟수 또는 서비스나 그 일부를 고객에게 제공하는 데 사용되는 저장 공간의 크기 제한<br />
&nbsp;<br />
d. 서비스를 통해 제공되는 일부 또는 모든 콘텐츠를 사전 심사, 검토, 플래그 표시, 필터링, 수정, 거부, 거절, 액세스 차단 또는 제거<br />
&nbsp;<br />
4.5. (주)위드라인은 (a) 부당한 중단, 장애 또는 지연 없이 고객에게 서비스가 제공되도록 하며 (b) 서비스의 중단, 장애 또는 지연을 최소화하기 위해 상업적으로 합리적인 노력을 기울입니다.<br />
&nbsp;<br />
4.6. 고객은 (주)위드라인이 인터페이스를 통해 고객에게 제공하는 것 이외의 다른 서비스를 이용할 수 없으며(이용하려는 시도 포함) 자동화된 도구(소프트웨어 및/또는 하드웨어 포함), 기능, 서비스 또는 기타 방식(스크립트 또는 웹 크롤러 포함)을 통해 서비스를 이용하지 않습니다(이용하려는 시도 포함).<br />
&nbsp;<br />
4.7. 고객은 서비스와 관련하여 (주)위드라인이 고객에게 제공하는 모든 지침을 준수하고 따르며 서비스 또는 서비스에 연결된 모든 서버, 네트워크 또는 기타 장비에 장애나 중단을 초래할 수 있는 행위에 관여하지 않습니다.<br />
&nbsp;<br />
4.8. 일부 서비스는 모바일 네트워크를 통해 이용할 수 있거나 모바일 네트워크를 통해 이용할 때 특히 유용합니다. 고객은 네트워크 공급자가 네트워크 액세스, 휴대폰/모바일 장치의 네트워크 연결 기간 및 서비스 이용을 위해 사용되는 데이터 양에 대해 요금을 부과할 수 있다는 점을 알고 있어야 합니다. 이와 관련하여 서비스 이용 전에 이러한 요금이 적용될 수 있는지 네트워크 공급자에게 확인할 책임은 전적으로 고객에게 있습니다.<br />
&nbsp;<br />
4.9. 고객은 본 이용약관에 따라 (주)위드라인이 제공하는 서비스 또는 기능, 관련 정보 일체를 제3자의 지식재산권, 인격권 등 일체의 권리를 침해하는 방식으로 이용할 수 없으며, (주)위드라인이 제공하는 서비스 또는 기능, 관련 정보 일체를 이용하여 정당한 권한 없이 제3자가 권한을 보유하고 있는 소프트웨어 또는 콘텐츠, 기타 저작물 등에 접근하거나 이를 수정&bull;임대&bull;대여&bull;판매&bull;배포 또는 이에 기반한 파생물을 생성하거나 기타 부당하게 이용할 수 없습니다.<br />
&nbsp;&nbsp;<br />
5. 광고<br />
&nbsp;<br />
5.1. 고객은 (주)위드라인이 서비스의 일부로 고객에게 프로모션 목적의 광고, 프로모션 자료 또는 기타 콘텐츠 및 자료나 제품을 배치하거나 제시한다는 데 동의합니다.<br />
&nbsp;<br />
5.2. (주)위드라인은 고객이 서비스 등록 시점 등에 마케팅 정보 수신을 명시적으로 선택한 경우에 한해 특별한 제안, 멤버십 혜택, 뉴스레터를 보내거나, 전화 연락을 시도할 것입니다.<br />
&nbsp;<br />
[등록 데이터, 계정 데이터 및 개인 정보 보호에 대해 알아두어야 할 내용]&nbsp;<br />
<br />
<br />
11. 등록 데이터 및 사용자 계정&nbsp;<br />
<br />
11.1. 서비스 이용 시 고객은 본인에 대한 정보(&quot;등록 데이터&quot;)를 제공해야만 서비스를 계속 이용할 수 있습니다.&nbsp;<br />
<br />
11.2. 고객은 완전하고 정확한 현재의 등록 데이터를 제공하고, 이를 계속 완전하고 정확한 현재 상태로 유지하기 위해 필요에 따라 등록 데이터를 업데이트하여야 합니다.&nbsp;<br />
<br />
11.3. ㈜위드라인은 서비스 제공을 위하여 또한 정보주체의 권리를 보호하기 위하여 고객이 보유하는 복수의 계정 중 하나만을 이용하여 서비스를 이용하도록 이용범위를 제한할 수 있습니다. 이를 위하여 필요한 경우, ㈜위드라인은 본인확인을 위하여 CI(Connecting Information : 본인인증기관으로부터의 인증결과값)를 활용할 수 있습니다.<br />
<br />
<br />
12. 암호 및 계정 보안&nbsp;<br />
<br />
12.1. 서비스를 이용하기 위해 고객은 사용자 계정을 개설하고 사용자 ID 및 암호(&quot;계정 데이터&quot;)를 제공해야 합니다.&nbsp;<br />
<br />
12.2. 고객은 특히 쉽게 알 수 있는 &#39;ID&#39;나 암호를 피하고 정기적으로 암호를 변경하며 암호를 공개하지 않도록 주의하거나 다른 사용자나 제3자에게 계정 데이터 또는 계정에 대한 액세스를 허용하지 않는 방식으로 항상 계정 데이터를 안전하게 유지하고 계정 데이터 및 계정에 대한 제3자의 무단 액세스를 방지하여야 합니다.&nbsp;<br />
<br />
12.3. 고객은 12.2항에 따라 고객의 계정 데이터를 안전하게 유지하고 관리할 의무가 있으므로, ㈜위드라인은 ㈜위드라인 서비스 내에서 고객의 계정 데이터를 사용하여 이루어지는 모든 행위를 고객의 행위로 간주합니다.<br />
<br />
12.4. 고객은 본인이 아닌 다른 사용자 또는 개인의 계정 데이터 또는 계정을 해당 계정을 보유한 사용자 또는 개인의 허가 없이 사용할 수 없습니다.&nbsp;<br />
<br />
12.5. 고객은 본인의 등록 데이터 또는 계정 데이터의 무단 사용 또는 기타 보안 위반 사례에 대해 알게 되는 경우 ㈜위드라인의 고객지원센터(070-7437-8142)를 통해 즉시 ㈜위드라인에 통지해 주시기 바랍니다.&nbsp;<br />
<br />
12.6. 고객은 고객이 보유한 장치에 설치된 ㈜위드라인 계정 애플리케이션에 액세스하거나 <a href="http://b2bc.samsungbizmall.com/mypage.asp">http://b2bc.samsungbizmall.com/mypage.asp</a>을 방문하여 언제든지 본인의 등록 데이터 또는 계정 데이터에 액세스하여 이를 변경할 수 있습니다.&nbsp;<br />
<br />
<br />
13. 개인 정보 및 개인 데이터 보호&nbsp;<br />
<br />
13.1. ㈜위드라인은 고객의 개인 데이터 보호를 위해 최선을 다합니다. 고객의 서비스 이용 시 ㈜위드라인이 고객의 개인 데이터 및 정보를 보호하고 취급하는 방법은 회사의 개인정보취급방침 [<a href="http://b2bc.samsungbizmall.com/privacy.asp">http://b2bc.samsungbizmall.com/privacy.asp</a>]을 참조하십시오.&nbsp;<br />
<br />
<br />
[본 이용 약관에 따른 고객과 ㈜위드라인의 의무]&nbsp;<br />
<br />
<br />
14. 고객의 보증 및 진술&nbsp;<br />
<br />
14.1. 고객이 제공한 사용자 콘텐츠 및 이러한 사용자 콘텐츠와 관련하여 발생한 모든 결과에 대한 책임은 전적으로 고객에게 있습니다(㈜위드라인에게 발생한 모든 손실이나 손해 포함). 특히 고객은 ㈜위드라인에 다음과 같이 보증하고 진술합니다.&nbsp;<br />
<br />
A. 고객은 사용자 콘텐츠에 대한 모든 권리의 소유자이거나 다른 방식으로 사용자 콘텐츠 라이선스를 ㈜위드라인에 허여할 권한을 가지고 있습니다.&nbsp;<br />
<br />
B. 사용자 콘텐츠는 어떠한 지식 재산권 또는 기타 제3자의 권리도 침해하지 않습니다.&nbsp;<br />
<br />
C. 사용자 콘텐츠에는 해를 끼치거나 부정확하거나 외설적이거나 폭력적이거나 음란하거나 위협적이거나 비방하거나 달리 불법적이거나 관련 법률 또는 ㈜위드라인의 콘텐츠 지침을 준수하지 않는 자료는 포함되지 않습니다.&nbsp;<br />
<br />
D. 사용자 콘텐츠에는 서비스 및/또는 이러한 사용자 콘텐츠에 액세스하는 장치의 기능 및 성능을 손상시키거나 해를 끼치거나 비활성화하거나 다른 방식으로 영향을 미치거나 이를 제한하는 바이러스나 기타 해로운 소프트웨어, 코드 또는 유사한 수단 및 장치가 포함되지 않습니다. 이 장치가 ㈜위드라인 또는 다른 사용자나 서버, 네트워크 노드 또는 이와 유사한 장비를 포함한 제3자에 속하는지 여부는 관계없습니다.&nbsp;<br />
<br />
E. 사용자 콘텐츠는 고객의 거주 국가 또는 서비스 이용 국가를 포함한 모든 국가의 법률에 따른 연령 분류 규칙 및 요구 사항(경우에 따라 정확하고 적절한 사용자 콘텐츠 분류 및 평가 포함)을 준수합니다.&nbsp;<br />
<br />
F. ㈜위드라인이 사용자 콘텐츠를 사용한다고 해서 ㈜위드라인에게 제3자(특히 사용료 징수 단체)에 어떤 종류든 금전적 분담액(라이선스 사용료, 요금 또는 기타 포함)을 지급할 의무가 부과되지 않습니다.&nbsp;<br />
<br />
14.2. 고객은 이러한 보증 위반 결과 ㈜위드라인에게 발생한 모든 손실, 손해, 책임 또는 경비로부터 ㈜위드라인을 면책하며 피해를 주지 않기로 동의합니다.&nbsp;<br />
<br />
<br />
15. ㈜위드라인의 보증 및 진술&nbsp;<br />
<br />
15.1. 본 이용 약관에 달리 명시되지 않는 한 ㈜위드라인 및 그 공급자, 파트너 및/또는 라이선스 허여자는 반드시 최선의 노력을 기울여 서비스를 제공하며, 명시적이든 묵시적이든 어떠한 종류의 보증과 조건도 모두 배제합니다. 특히(이에 국한되지 않음) ㈜위드라인 및 그 공급자, 파트너 및/또는 라이선스 허여자는 다음을 보증하거나 진술하지 않습니다.&nbsp;<br />
<br />
A. 서비스가 모든 목적에 적합하다거나 고객의 요구 사항을 충족한다거나 아무런 오류나 결함이 없이 제공된다거나 경우에 따라 서비스가 모든 품질 수준을 준수한다는 점&nbsp;<br />
<br />
B. 서비스가 고객에게 항상 제공되며 중단이나 장애 또는 지연 없이 제공된다는 점&nbsp;<br />
<br />
C. 서비스가 고객의 거주 국가 또는 세계 어느 곳에서든 저작권을 침해하거나 법률을 위반하지 않는다는 점&nbsp;<br />
<br />
D. 고객이 ㈜위드라인으로부터 획득했거나 서비스(자료 또는 제품 포함)를 이용하여 획득한 모든 정보(구두 또는 서면)가 적합, 정확 또는 완전하다거나 신뢰할 수 있다는 점&nbsp;<br />
<br />
E. 서비스의 일부로서 고객에게 제공되는 서비스 또는 소프트웨어의 성능, 작동 또는 기능상의 결함을 포함하여 서비스의 모든 결함이 수정 또는 교정되거나 다른 방식으로 구제될 것이라는 점&nbsp;<br />
<br />
<br />
16. 고객의 책임&nbsp;<br />
<br />
16.1. 다음 각호의 1에 대한 고객의 의무 위반 책임은 전적으로 고객에게 있습니다.&nbsp;<br />
<br />
A. 본 이용 약관&nbsp;<br />
<br />
B. 고객의 거주지 또는 서비스 이용 관할지를 포함한 관련 관할지의 관련 법률이나 규정&nbsp;<br />
또한 고객은 이러한 위반의 결과(㈜위드라인 또는 제3자가 초래하거나 당할 수 있는 손실이나 손해를 포함)에 대한 전적인 책임을 부담합니다. ㈜위드라인은 이러한 위반과 관련하여 고객이나 제3자에 대해 아무런 책임을 지지 않습니다.&nbsp;<br />
<br />
<br />
17. ㈜위드라인의 책임&nbsp;<br />
<br />
17.1. 17.3항에도 불구하고, ㈜위드라인은 다음에 대해 고객에게 책임을 지지 않으며 이는 법적 근거나, 특히 계약, 불법 행위(과실 포함) 또는 책임 이론과 관계 없으며, ㈜위드라인이 이러한 손해나 손실에 대해 알고 있었던 경우에도 마찬가지입니다.&nbsp;<br />
<br />
A. 어떠한 간접적, 부수적, 특별 또는 결과적 손해&nbsp;<br />
<br />
B. 어떠한 소득, 사업, 실제 또는 기대 이익, 기회, 영업권 또는 평판의 손실(직접적 또는 간접적)&nbsp;<br />
<br />
C. 어떠한 데이터에 대한 손해 및/또는 손상이나 손실(직접적 또는 간접적)&nbsp;<br />
<br />
D. 다음의 결과로 발생한 어떠한 손실 또는 손해&nbsp;<br />
<br />
I. 본 이용 약관 또는 ㈜위드라인과 고객 간 기타 계약의 이용 약관이 ㈜위드라인 및 그 공급자, 파트너 및/또는 라이선스 허여자 측의 과실로 인해 위반된 경우&nbsp;<br />
<br />
II. (a) 서비스(서비스 이용의 일부로 또는 이용 과정에서 고객에게 제공되는 소프트웨어, 정보, 문서, 자료 포함) 또는 (b) 고객의 서비스 이용 시 또는 이의 이용으로 인해 제공된 광고(프로모션 자료 포함)의 광고주나 후원사와 고객 간의 관계 또는 거래의 결과 또는 광고의 적절성, 정확성, 완전성, 신뢰성 또는 존재 사실에 대한 고객의 신뢰&nbsp;<br />
<br />
III. 서비스에 대한 변경, 수정, 확장 또는 제한(서비스 이용, 고객 계정 및 계정 데이터 또는 고객의 등록 데이터에 대한 액세스 중단 포함) 또는 서비스(또는 그 일부) 제공의 영구적이거나 일시적인 중지 또는&nbsp;<br />
<br />
IV. 고객 본인이 아닌 다른 사람의 계정 데이터 사용(본인이 알고 있었는지 여부와 관계 없음). 고객의 계정 데이터를 다른 사람이 사용하여 발생한 손해 및/또는 손실에 대해 ㈜위드라인에 보상할 책임은 전적으로 고객에게 있습니다.&nbsp;<br />
<br />
17.2. ㈜위드라인이 본 이용 약관 위반의 책임이 있는 경우 ㈜위드라인의 책임은 당시 알려진 상황에 의거하여 본 이용 약관 체결 당시 ㈜위드라인이 일반적으로 예상한 손해로 제한됩니다.&nbsp;<br />
<br />
17.3. 본 이용 약관의 어떤 부분도 ㈜위드라인의 다음과 같은 책임을 배제하거나 제한하지 않습니다.&nbsp;<br />
<br />
A. 사망, 상해 또는 사기 또는 제조물 책임법(Product Liability Act)에 따른 책임&nbsp;<br />
<br />
B. 합법적으로 배제되거나 관련 법률에 의해 제한될 수 없는 손해 또는 손실에 대한 보증 또는 책임. 사용자 관할지의 법률이 특정 보증이나 이용 약관의 배제 또는 과실, 계약 위반이나 묵시적 조건 위반 또는 부수적이거나 결과적 손해로 인한 손실이나 손해에 대한 책임의 제한이나 배제를 허용하지 않는 경우 이러한 관할지에서 사용자 및 ㈜위드라인의 책임 및 보증에 적용되는 유일한 합법적인 제한은 관련 법률에 의해 허용되는 최대 한도로 제한됩니다.&nbsp;<br />
<br />
<br />
18. 불가항력&nbsp;<br />
<br />
18.1. ㈜위드라인은 ㈜위드라인의 합리적인 통제력을 벗어난 사건(&quot;불가항력 사태&quot;)으로 인해 본 이용 약관에 따른 ㈜위드라인의 의무를 이행하지 못하거나 이행이 지연될 경우 이에 대해 책임을 지지 않으며, 특히(이에 국한되지 않음) (a) 공중 또는 전용 통신망 사용 불가능, (b) 정부의 법률, 명령, 법령, 규정이나 제한 또는 (c) 파업, 공장 폐쇄 또는 기타 노동 쟁의, 민란, 폭동, 침입, 테러 공격이나 테러 공격 위협, 전쟁(선전포고 여부와 관계 없음) 또는 모든 자연 재해가 이에 해당됩니다.&nbsp;<br />
<br />
18.2. 본 이용 약관에 따른 ㈜위드라인의 의무 이행은 불가항력 사태가 계속되는 기간 동안 일시 중단되는 것으로 간주되며 해당 지속 기간 만큼 이행 시간이 연장됩니다.&nbsp;<br />
<br />
18.3. ㈜위드라인은 불가항력 사태에도 불구하고 불가항력 사태를 끝내고 본 이용 약관에 따른 ㈜위드라인의 의무를 이행할 수 있는 해결책을 찾기 위해 합리적인 노력을 기울입니다.&nbsp;<br />
<br />
<br />
<br />
[기타 알아두어야 할 사항]&nbsp;<br />
<br />
<br />
19. 특별 이용 약관&nbsp;<br />
<br />
19.1. 어떠한 서비스 등록 또는 이용 시 고객에게 당해 서비스의 특수 기능 및 특징을 설명하는 이용약관이 제시될 수 있으며, 이러한 약관(&quot;특별 약관&quot;으로 통칭)은 추가 법적 조항으로 적용됩니다.<br />
<br />
19.2. 모든 특별 약관은 본 이용 약관에 추가하여 이와 함께 적용됩니다. 본 이용 약관과 특별 약관이 충돌할 경우 특별 약관이 본 이용 약관에 우선하여 적용됩니다.&nbsp;<br />
<br />
<br />
20. 지식재산권 및 기타 권리 침해에 대한 청구 및 통지 절차&nbsp;<br />
<br />
20.1. ㈜위드라인은 관련 법률에 따라 저작권 침해 또는 기타 법률 위반 혐의 통지에 대응하여 반복적으로 저작권법을 침해하거나 기타 관련 법률을 위반하는 사용자의 계정을 해지 또는 일시 중단하거나 액세스를 차단(일시적 또는 영구적으로)할 수 있습니다.&nbsp;<br />
<br />
20.2. 서비스 및 서비스와 함께 제공되거나 서비스에 표시되는 콘텐츠 또는 사용자 콘텐츠에 의한 저작권 침해 또는 기타 법률 위반에 관한 통지는 [online@widline.co.kr]로 보내야 합니다.&nbsp;<br />
<br />
<br />
21. 비밀 유지&nbsp;<br />
<br />
21.1. 서비스에는 비밀 정보로 지정되거나 거래 또는 사업 비밀로 인식할 수 있거나 기타 사유로 비밀 정보임을 인식할 수 있거나 고객에게 비밀 정보로 제공되는 모든 정보를 포함하여(이에 국한되지 않음) ㈜위드라인의 비밀 정보가 포함될 수 있습니다.&nbsp;<br />
<br />
21.2. 본 이용 약관에 달리 명시되지 않는 한 고객은 (a) 이러한 정보를 무기한 비밀로 유지하고 ㈜위드라인의 사전 서면 동의 없이 이러한 정보를 공개하지 않으며, (b) 본 이용 약관의 목적상 필요한 경우가 아니면 이러한 정보를 기록하거나 달리 사용하지 않습니다.&nbsp;<br />
<br />
21.3. 관련 법률에 따라 ㈜위드라인의 비밀 정보를 공개해야 하는 경우에는 23.1항이 적용되지 않습니다. 이 경우 관련 법률에서 허용되는 한 고객은 ㈜위드라인의 비밀 정보 공개에 대해 즉시 ㈜위드라인에게 통지합니다.&nbsp;<br />
<br />
<br />
22. 피해보상 및 분쟁해결<br />
<br />
제공되는 서비스 중 콘텐츠사업 또는 콘텐츠 이용과 관련한 피해의 구제 및 분쟁의 조정에 대해서는 콘텐츠산업진흥법 제30조에 따라 콘텐츠분쟁조정위원회에 분쟁 조정 신청을 할 수 있습니다.<br />
&nbsp;
</div><!-- terms_cont e -->
						</dd>
					</dl>
					<input type="checkbox" class="chk_box" title=""  name="order_term_chk" id="order_term_chk" />
					<label for="order_term_chk">약관 동의하기</label>
				</div><!-- order_term_wrap e -->
			</div><!-- order_form e -->
			
			<div class="btn_wrap" id="order_button" style="height:100px;">
				<a href="javascript:link_location2('/','_top')" class="btn btn_b">취소</a>
				<a href="javascript:order_ok();" class="btn btn_b btn_agree" >구매하기</a>
				
			</div><!-- btn_wrap e -->
		</div><!-- order_wrap e -->
		</form>
	</div><!-- order_page e -->
</div><!-- container e -->
<!-- #include virtual = "include/popup/pop_info_card.asp" -->
<script language="javascript" type="text/javascript">
	$(document).ready(function(){
	////결제수단 무통장입금 선택시 표시 문구
	//$(".choice_cont").hide();
	//$(".pay_cont input[name='input_radio']").click(function () {
	//		$(".choice_cont").removeClass("on");
	//		$(this).addClass("on");
	//		$(".choice_cont").hide()
	//		var activeTab = $(this).attr("rel");
	//		$("#" + activeTab).show()
	//});
});
</script>
<!-- #include virtual = "include/footer.asp" -->
