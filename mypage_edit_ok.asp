<!--#include virtual="/Library/config.asp"-->
<%
	'로그인 체크(로그인후 호출URL)
	ChkLogin("/gate.asp")

	now_online_pw = fInject(Request("now_online_pw"))
	new_online_pw = fInject(Request("new_online_pw1"))
	cm_tel1       = fInject(Request("cm_tel1"))
	cm_tel2       = fInject(Request("cm_tel2"))
	cm_tel3       = fInject(Request("cm_tel3"))

	cm_tel        = cm_tel1&"-"&cm_tel2&"-"&cm_tel3

	cm_hp1        = fInject(Request("cm_hp1"))
	cm_hp2        = fInject(Request("cm_hp2"))
	cm_hp3        = fInject(Request("cm_hp3"))

	cm_hp         = cm_hp1&"-"&cm_hp2&"-"&cm_hp3

	cm_email1     = fInject(Request("email1"))
	cm_email2     = fInject(Request("email2"))

	cm_email      = cm_email1&"@"&cm_email2

	'이메일 수신 여부
	RE_EMAIL_YN     = fInject(Request("RE_EMAIL_YN"))
	If RE_EMAIL_YN = "" Then 
		re_email_yn = "N"
	End If 

	cm_zip =  fInject(Request("cust_zip"))
	cm_addr = fInject(Request("cust_addr"))

	'신주소 추가

	cm_zip =  Left(fInject(Request("cust_zip")),3)
	cm_zip2 = Mid(fInject(Request("cust_zip")),4,2)

	Dbopen()

	InfoSQL = "SELECT online_pwd,cm_tel,cm_tel,cm_email,cm_zip,cm_zip2,cm_addr,RE_EMAIL_YN FROM IC_T_ORDER_CUST WHERE DEL_YN='N' AND ISNULL(IN_OK,'Y')='Y' AND ORDER_SEQ='"&GetsCUSTSEQ()&"' AND online_pwd='"&now_online_pw&"'"

	Set InfoRs = dbcon.Execute(InfoSQL)
	
	WON_RE_EMAIL_YN = InfoRs("RE_EMAIL_YN")

	If Not(InfoRs.Eof Or InfoRs.Bof) Then 
		UpSQL = "UPDATE IC_T_ORDER_CUST "

		UpSQL = UpSQL&" Set cm_tel = '"&cm_tel&"'"
		If new_online_pw <> "" Then 
			UpSQL = UpSQL&"    ,online_pwd   = '"&new_online_pw&"'"
		End If 
		UpSQL = UpSQL&"    ,cm_hp       = '"&cm_tel&"'"
		UpSQL = UpSQL&"    ,cm_email    = '"&cm_email&"'"
		UpSQL = UpSQL&"    ,re_sms_yn   = '"&re_sms_yn&"'"
		UpSQL = UpSQL&"    ,re_email_yn   = '"&re_email_yn&"'"
		UpSQL = UpSQL&"    ,cm_zip      = '"&cm_zip&"'"
		UpSQL = UpSQL&"    ,cm_zip2     = '"&cm_zip2&"'"
		UpSQL = UpSQL&"    ,cm_addr     = '"&cm_addr&"'"
		UpSQL = UpSQL&"    ,update_gubun     = 'N'"
		UpSQL = UpSQL&" WHERE ORDER_SEQ ='"&GetsCUSTSEQ()&"'"
		'Response.write UpSQL
		'Response.End 
		Dbcon.Execute(UpSQL)

		'정보수정시 쿠키값 변경
		Response.Cookies("sCUSTZIP").path = "/"
		Response.Cookies("sCUSTZIP").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTZIP") = cm_zip
		Response.cookies("sCUSTZIP").expires = date+1
		
		Response.Cookies("sCUSTZIP2").path = "/"
		Response.Cookies("sCUSTZIP2").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTZIP2") = cm_zip2
		Response.cookies("sCUSTZIP2").expires = date+1
		
		Response.Cookies("sCUSTADDR").path = "/"
		Response.Cookies("sCUSTADDR").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTADDR") = cm_addr
		Response.cookies("sCUSTADDR").expires = date+1
		
		Response.Cookies("sCUSTTEL").path = "/"
		Response.Cookies("sCUSTTEL").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTTEL") = cm_tel
		Response.cookies("sCUSTTEL").expires = date+1
		
		Response.Cookies("sCUSTEMAIL").path = "/"
		Response.Cookies("sCUSTEMAIL").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTEMAIL") = cm_email
		Response.cookies("sCUSTEMAIL").expires = date+1
		
		Response.Cookies("sCUSTHP").path = "/"
		Response.Cookies("sCUSTHP").Domain = Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTHP")  = cm_hp
		Response.cookies("sCUSTHP").expires = date+1

		
		If RE_EMAIL_YN <> WON_RE_EMAIL_YN  Then 
			Set Smtp = Server.CreateObject("TABSUpload4.Smtp")
					
			strContent = "http://b2b.sampsungbizmall.com 에서 전송한 " + GetsCUSTNM() + "님의 마케팅 수신동의 변경메일입니다..<BR><BR>"
			strContent = strContent & "안녕하세요 b2b.sampsungbizmall.com 입니다. <BR><BR>"
			If re_email_yn = "Y" Then 
				strContent = strContent & GetsCUSTNM() & " 고객님께서는 : " & now()  & " 삼성전자에서 수신하는 광고메일에 동의하였습니다.<BR><BR>" 
				strContent = strContent & "감사합니다. " 
			Else
				strContent = strContent & GetsCUSTNM() & " 고객님께서는 : " & now()  & " 삼성전자에서 수신하는 광고메일에 동의하지 않으셨습니다..<BR><BR>" 
				strContent = strContent & "감사합니다. " 		
			End If 

			Smtp.ServerName = "mail.samsungbizmall.com"
			Smtp.ServerPort = 6700
			Smtp.AddHeader "X-TABS-Campaign", "63E3F849-267F-4B6B-BC6E-496295D789B3"
			Smtp.FromAddress = "online@samsungbizmall.com"
			Smtp.AddToAddr cm_email, ""
			Smtp.Subject = GetsCUSTNM() & "님께 b2b.sampsungbizmall.com 에서 발송한 마케팅 수신동의 변경메일입니다.."	          
			Smtp.Encoding = "base64"
			Smtp.Charset = "euc-kr"
			Smtp.BodyHtml = strContent

			Set Result = Smtp.Send()
			If Result.Type = SmtpErrorSuccess Then
				'Response.Write "메일이 올바르게 전달되었습니다.<p>"
			Else
				'Response.Write "오류 종류:" & Result.Type & "<br>"
				'Response.Write "오류 코드:" & Result.Code & "<br>"
				'Response.Write "오류 설명:" & Result.Description
			End If
		End If 

		Response.Write "<script>alert('회원정보가 수정되었습니다..');parent.location.href='/';</script>"
		Response.End
	Else
		Response.Write "<script>alert('현재 비밀번호가 틀립니다.');parent.document.meminfo.now_online_pw.value='';parent.document.meminfo.now_online_pw.focus();</script>"
		Response.End
	End If 

	DbClose()
%>