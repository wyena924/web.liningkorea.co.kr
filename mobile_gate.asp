<!--#include virtual="/Library/config.asp"-->
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta name="Generator" content="EditPlus®">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=2, user-scalable=no">
<title>로그인</title>
<style type="text/css">
/* nanumgothic */
@import url(http://fonts.googleapis.com/earlyaccess/nanumgothic.css);
/*Noto Sans KR*/
@import url(http://fonts.googleapis.com/earlyaccess/notosanskr.css);
body,html,div,form,label,input,button,a,p,table,tr,th,td,textarea,dl,dt,dd,ul,li{margin:0;padding:0;line-height:100%;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;letter-spacing:-1px;font-family:'Noto Sans KR','Nanum Gothic',sans-serif;color:#333;}
input[type="checkbox"]{display:none;}
input[type="checkbox"] + label span{display:inline-block;width:15px;height:15px;margin:-2px 5px 0 0;vertical-align:middle;border:1px solid #c3c3c3;cursor:pointer;}
input[type="checkbox"]:checked + label span{background:url('http://emall.halla.com/mobile/images/chk_on.png') no-repeat left top/100%;}
ul,li{list-style:none;}
.hide{width:1px;height:1px;overflow:hidden;visibility:hidden;line-height:0;font-size:0;position:absolute;left:-999px;}
a[href^=tel]{color:inherit;text-decoration:inherit;font-size:inherit;font-style:inherit;font-weight:inherit;}/* 사파리 밑줄제거 */

.login_wrap input::-webkit-input-placeholder,
.login_wrap textarea::-webkit-input-placeholder{color:#666;font-size:15px;}/* Chrome/Opera/Safari */
.login_wrap input::-moz-placeholder,
.login_wrap textarea::-moz-placeholder{color:#666;font-size:15px;}/* Firefox 19+ */
.login_wrap input:-ms-input-placeholder,
.login_wrap textarea:-ms-input-placeholder{color:#666;font-size:15px;}/* IE 10+ */
.login_wrap input:-moz-placeholder,
.login_wrap textarea:-moz-placeholder{color:#666;font-size:15px;}/* Firefox 18- */

.login_wrap{position:relative;max-width:640px;margin:0 auto;padding:10px;overflow:hidden;}
.login_wrap h1{text-align:center;margin:20% 0;}
.login_wrap h1 img{width:50%; margin-top:20px;} 
.login_wrap .login_form{width:100%;margin:0 auto;}
.login_wrap .login_form .login_box{float:left;width:75%;}
.login_wrap .login_form .login_box input{border:1px solid #e7e7e7;background:#eee;width:100%;height:50px;padding-left:5%;margin-top:1%;}
.login_wrap .login_form .login_box input#get_login_id{margin-top:0;}
.login_wrap .login_form .login_box .hide{width:0;height:0;overflow:hidden;visibility:hidden;line-height:0;font-size:0;position:absolute;left:-999px;}
.login_wrap .login_form .login_btn{float:right;width:24%;}
.login_wrap .login_form .login_btn .button{text-align:center;color:#fff;border:none;background:#0e4194;width:100%;height:102px;font-size:20px;-webkit-appearance:none;/* 사파리버튼리셋 */}
.login_wrap .login_form .save_id{clear:both;position:relative;padding:20px 0 30px;border-bottom:1px solid #333;}
.login_wrap .login_form .save_id label{font-size:17px;color:#333;}
.login_wrap .login_form .find_wrap{margin-top:30px;color:#c3c3c3;}
.login_wrap .login_form .find_wrap p{color:#666;font-size:17px;margin-bottom:10px;}
.login_wrap .login_form .find_wrap a{color:#333;font-size:17px;text-decoration:none;}
.login_wrap .cs_wrap{margin:25% auto 0;}
.login_wrap .cs_wrap p{font-size:17px;color:#333;margin-bottom:5px;text-decoration:none;}
.login_wrap .cs_wrap p:first-child{font-size:25px;margin-bottom:25px;}

/* popup */
.popup_wrap{left:0px;top:0px;width:100%;height:100%;position:absolute;z-index:999;background:rgba(0,0,0,0.5);display:none;}
.popup_wrap button{cursor:pointer;border:none;background:none;}
.popup_wrap .point{color:red;text-indent:-14px;line-height:1.2;padding-left:15px;font-size:13px;}
.popup_wrap input{width:177px;height:29px;line-height:29px;padding-left:5px;border:1px solid #c3c3c3;}
.popup_wrap input[type="number"]{text-align:center;padding-left:0;}
.popup_wrap input[type="checkbox"]{border:1px solid #c3c3c3;background:none;}
.popup_wrap button,.popup_wrap input{vertical-align:middle;}
.popup_wrap label{margin-top:5px;font-size:13px;display:block;}

.popup_wrap .box{background-color:#fff;border:solid 3px #027cc5;border-top:none;position:absolute;left:50%;z-index:10000;margin-left:-47.5%;width:95%;height:auto;top:50%;transform:translate(0,-50%);-webkit-transform:translate(0,-50%);-moz-transform:translate(0,-50%);-ms-transform:translate(0,-50%);-o-transform:translate(0,-50%);}
.popup_wrap h2{position:relative;margin:0;padding:20px 10px;background:#027cc5;color:#fff;font-size:20px;}
.popup_wrap #close_x{font-weight:800;position:absolute;top:0;right:0;padding:15px 10px;font-size:25px;color:#fff;z-index:99991;}

/* 회원가입 */
.join_wrap dl{width:100%;padding:15px 10px;border-bottom:1px solid #eee;}
.join_wrap dt{text-align:left;font-weight:400;letter-spacing:-1px;margin-bottom:10px;}
.join_wrap .btn_join{padding:8px;background:#555;color:#fff;margin-left:5px;}
.join_wrap .num_short{width:50px;}
.join_wrap .num_long{width:100%;margin-top:5px;}
.join_wrap .term{padding-left:10px;}
.join_wrap .term h3{margin-top:0;font-size:15px;font-weight:400;}
.join_wrap form{height:450px;overflow-y:scroll;}
.join_wrap .term .term_cont{font-size:13px;height:100px;padding:5px;overflow-y:scroll;border:1px solid #c3c3c3;}
.join_wrap .term .term_cont p{margin-bottom:15px;line-height:1.3;}

/* 아이디&비번찾기 */
.popup_wrap .find_info{text-indent:-6px;margin-left:6px;padding:10px;}
.popup_wrap .find_info li{font-size:14px;line-height:1.3;margin-top:5px;}
.popup_wrap .find_box{}
.popup_wrap .find_box .find_cont{text-align:center;padding:10px 10px 0;}
.popup_wrap .find_box .find_cont:first-child{margin-top:0;}
.popup_wrap .find_box .find_cont label{display:inline-block;text-align:right;width:81px;margin-right:10px;vertical-align:middle;}
.popup_wrap .find_box .find_cont input{margin-top:10px;}
.popup_wrap .find_box .find_cont input[type="radio"]{width:auto;height:auto;}
#find_id .find_cont > label{width:auto;text-align:left;}
#find_id label{width:81px;}
#find_pw label{width:57px;}

.login_wrap .btn_wrap{margin:30px 0;text-align:center;}
.login_wrap .btn_wrap a{display:inline-block;background:#eee;color:#333;border:1px solid #c3c3c3;width:127px;padding:10px 0;text-decoration:none;}
.login_wrap .btn_wrap a.btn_agree{background:#027cc5;border:1px solid #027cc5;color:#fff;}
.login_wrap .btn_wrap a.btn_agree_button{background:#555;border:1px solid #027cc5;color:#fff;}


.popup_wrap .end_box{left:50%;}
.popup_wrap .end_box span{color:#027cc5;}
.popup_wrap .end_box p{text-align:center;padding:50px 0 0;line-height:1.5;}
.popup_wrap .end_box p.end_box_info{padding-top:20px;line-height:1.3;font-size:13px;color:#555;}

.popup_cont{position:absolute;top:30px;left:50%;margin-left:-180px;z-index:1;}
.popup_cont > a{display:block;}
.popup_cont img{width:100%;}
.popup_cont .btn_close{background:#333;padding:10px;text-align:right;}
.popup_cont .btn_close a{color:#fff;margin-left:10px;}

/*********** media *************/
/*360(브라우저 가로폭)*/
@media screen and (max-width: 360px){
.login_wrap .login_form .login_box input{height:40px;}
.login_wrap .login_form .login_btn .button{height:82px;}
.login_wrap .login_form .save_id input[type="checkbox"] + label:before{width:15px;height:15px;line-height:15px;}
.login_wrap .login_form .find_wrap{margin-top:15px;}
.login_wrap .login_form .save_id label{font-size:12px;}
.login_wrap .login_form .find_wrap p,.login_wrap .login_form .find_wrap a,.login_wrap .cs_wrap p{font-size:15px;}
.login_wrap .cs_wrap p:first-child{font-size:20px;margin-bottom:20px;}
}
/*320(브라우저 가로폭)*/
@media screen and (max-width: 320px){
#popup01{width:87%;margin-left:-43%;}
#popup01 .btn_close{width:100%;}
}

</style>
<script type="text/javascript" src="/admin/Script/jquery.1.11.3.min.js"></script>
<script type="text/javascript" src="/admin/Script/jquery-1.8.3.min.js"></script>
<script type="text/javascript" src="/admin/Script/jquery-ui-1.10.4.custom.min.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>

	function chk_login(){
		var f = document.login_frm;
		if(f.get_login_id.value==""){
			alert("아이디를 입력해 주세요.");
			f.get_login_id.focus();
			return false;
		}
		if(f.online_pw.value==""){
			alert("비밀번호를 입력해 주세요.");
			f.online_pw.focus();
			return false;
		}
		f.action="login_ok.asp?location_gb=Y";
		//f.target = "iSQL";
		f.submit();
		
	}



	//체크박스 클릭시
	function chk_saveid() {
		var f = document.login_frm;
	  var expdate = new Date();
	  // 30일동안 아이디 저장 
	  if (f.saveid.checked)
		expdate.setTime(expdate.getTime() + 1000 * 3600 * 24 * 30);// 30일
	  else
		expdate.setTime(expdate.getTime() - 1);// 쿠키 삭제조건
	  setCookie("saveid", f.get_login_id.value, expdate);
	}

	//폼 로드시 쿠키 아이디 가지고 오기
	function chk_getid() {
		var f = document.login_frm;
	  f.saveid.checked = ((f.get_login_id.value = getCookie("saveid")) != "");
	}

	//쿠키 정보 가지고 오기
	function getCookie(Name) {
	  var search = Name + "="
	  if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
		offset = document.cookie.indexOf(search)
		if (offset != -1){// 쿠키가 존재하면
		  offset += search.length
		  // set index of beginning of value
		  end = document.cookie.indexOf(";", offset)
		  // 쿠키 값의 마지막 위치 인덱스 번호 설정
		  if (end == -1)
			end = document.cookie.length
		  return unescape(document.cookie.substring(offset, end))
		}
	 }
	  return "";
	}
	//쿠키정보 저장
	function setCookie( name, value, expirehours ) { 
		var todayDate = new Date(); 
		todayDate.setHours( todayDate.getHours() + expirehours ); 
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
	} 




	function CheckInfo(){
		
		//이메일
		if(document.id_search_form.selectCheck[0].checked == true){
			if (document.id_search_form.Email_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Email_UserName.focus();
				return;
			}

			if (document.id_search_form.Email.value == "")
			{
				alert("이메일 주소를 입력하시기 바랍니다.");
				document.id_search_form.Email.focus();
				return;
			}
			
		}
		//전화번호
		else if(document.id_search_form.selectCheck[1].checked == true)
		{
			if (document.id_search_form.Phone_UserName.value == "")
			{
				alert("이름을 입력하시기 바랍니다.");
				document.id_search_form.Phone_UserName.focus();
				return;
			}

			if (document.id_search_form.Phone.value == "")
			{
				alert("전화번호를 입력하시기 바랍니다1.");
				document.id_search_form.Phone.focus();
				return;
			}

		}
		if (document.id_search_form.selectCheck[0].checked == true)
		{
			selectCheck= "Email"
		}
		else
		{
			selectCheck= "Phone"
		}

		var strAjaxUrl="search_id_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				Email_UserName: escape(document.id_search_form.Email_UserName.value),
				Email: document.id_search_form.Email.value,
				Phone_UserName: escape(document.id_search_form.Phone_UserName.value),
				Phone: document.id_search_form.Phone.value,
				selectCheck: selectCheck
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else if (retDATA !='null')
				{
					document.getElementById("id_search_div").style.display ='block'
					document.getElementById("sarch_id_span").innerHTML = retDATA;
					return;
				}
			}
		});

	}
	

	function CheckEmail()
	{	
		var PWD_UserID = document.pwd_search_form.PWD_UserID.value;
		var PWD_UserEmail = document.pwd_search_form.PWD_UserEmail.value;
		
		var strAjaxUrl="search_pw_ok.asp";
		$.ajax({
			url: strAjaxUrl,
			type: 'POST',
			dataType: 'html',
			data: 
			{
				UserID: document.pwd_search_form.PWD_UserID.value,
				Email: document.pwd_search_form.PWD_UserEmail.value
			},

			success: function(retDATA) {
				if (retDATA =='null')
				{
					alert('해당 사용자가 존재하지 않습니다');
					return;
				}
				else 
				{
					document.getElementById("pwd_sarch_div").style.display ='block'		
					document.getElementById("sarch_pwdid_span").innerHTML = PWD_UserID;
					document.getElementById("sarch_pwd_span").innerHTML = retDATA;
					return;
				}
			}
		});
	}

	
	function id_checked()
		{
		  var  online_id =document.getElementById('online_id').value 
		  if (online_id  =='')
			 {

				alert("ID 명을 입력해주시기 바랍니다..");
				document.getElementById('online_id').focus();
				return;
			 }

			 var strAjaxUrl="/Main/order_id_ok.asp?online_id="+online_id;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 ID입니다.');
									//document.bform.id_ok.value = ''
									return false;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 ID입니다..');
									document.getElementById('id_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}
	//아이디 있는데 사번을 또 받음? 그럼 멀로 받음?
	//그냥 메모를 활용해서 해야할듯!!!!!
	function memo_checked()
		{
		  var  memo =document.getElementById('memo').value 

		  var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 

		  if (memo  =='')
			 {
				alert("사내이메일를 입력해주시기 바랍니다..");
				document.getElementById('memo').focus();
				return;
			 }
		  if ( !regExp.test( memo ) ) 
			 {
				alert("잘못된 이메일 주소 형식입니다.");
				document.getElementById('memo').focus();
				return;
			 }
			 var strAjaxUrl="/Main/memo_id_ok.asp?memo="+memo;
			//location.href = strAjaxUrl
			//return;
			 var retDATA="";
				//alert(strAjaxUrl);
				 $.ajax({
					 type: 'GET',
					 url: strAjaxUrl,
					 dataType: 'html',
					 success: function(retDATA) {
						if(retDATA)
							{
								if(retDATA=='OK')
								{
									alert('이미 존재하는 사내이메일입니다.');
									//document.bform.id_ok.value = ''
									return;
									

								}
								else if(retDATA=='NO')
								{
									alert('사용가능한 사내이메일입니다.');
									document.getElementById('memo_ok').value  = 'ok'
									return;
								}
							}
					 }
			 }); //close $.ajax(
		}

		function write_ok()
		{ 			
			var custseq = '<%=GetsCUSTSEQ%>'; 	
			var f				= document.reg_frm;
			var online_id		= f.online_id;
			var memo			= f.memo;
			var custnm			= f.cust_cust_custnm;
			var zip1			= f.cust_zip1;
			var zip2			= f.cust_zip2;
			var addr			= f.cust_addr;
			var cm_tel1			= f.cust_cm_tel1;
			var cm_tel2			= f.cust_cm_tel2;
			var cm_tel3			= f.cust_cm_tel3;
			var cm_hp1			= f.cust_cm_hp1;
			var cm_hp2			= f.cust_cm_hp2;
			var cm_hp3			= f.cust_cm_hp3
			var cm_email		= f.cust_cm_email
			var checkboxcpy		= f.cust_checkboxcpy
			var online_pwd		= f.cust_online_pwd;
			var re_online_pwd	= f.cust_re_online_pwd
			var id_ok			= f.id_ok
			var memo_ok			= f.memo_ok
			var regExp = /^[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_\.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,3}$/i; 

			//alert(online_id.value	);
			//alert(custnm.value	);
			//alert(zip1.value		  );
			//alert(zip2.value		  );
			//alert(addr.value		  );
			//alert(cm_tel1.value	);
			//alert(cm_tel2.value	);
			//alert(cm_tel3.value	);
			//alert(cm_hp1.value	);
			//alert(cm_hp2.value	);
			//alert(cm_hp3.value	);
			//alert(cm_email.value	  );
			//alert(checkboxcpy.value);
			//alert(online_pwd.value);
			//alert(re_online_pw.value);
			//alert(id_ok.value		  );
			//필수입력사항체크
			if (online_id.value == "") {
				alert('아이디를 입력해주시기 바랍니다.!');	
				online_id.focus();
				return;
			}
			//if (memo.value == "") {
			//	alert('사내이메일을 입력해주시기 바랍니다.!');	
			//	memo.focus();
			//	return;
			//}
	
			//if ( !regExp.test( memo.value ) ) {

			//	  alert("잘못된 이메일 주소 형식입니다.");
			//	  memo.focus();
			//	  return;
			//}


			if (online_pwd.value == "") {
				alert('비밀번호를 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value.length < 6) {
				alert('비밀번호를 4자 이상 입력해주시기 바랍니다.!');
				online_pwd.focus();
				return;
			}
			
			if (online_pwd.value != re_online_pwd.value) {
				alert('비밀번호가 일치 하지 않습니다.');
				re_online_pwd.focus();
				return;
			}	
			


			if (id_ok.value=='')
			{
				alert('아이디 중복확인을 해주시기 바랍니다.');
				return;
			}
			//if (memo_ok.value=='')
			//{
			//	alert('사내이메일를 중복확인을 해주시기 바랍니다.');
			//	return;
			//}


			if (custnm.value == "") {
				alert('성명을 입력해주시기 바랍니다.!');
				custnm.focus();
				return;
			}

			if (zip1.value == "") {
				alert('우편번호1을 입력해 주시기 바랍니다..');
				zip1.focus();
				return;
			}	
			if (zip2.value == "") {
				alert('우편번호2를 입력해 주시기 바랍니다..');
				zip2.focus();
				return;
			}	


			if (addr.value == "") {
				alert('주소를 입력해 주시기 바랍니다.');
				addr.focus();
				return;
			}	

			if (cm_hp1.value == "") {
				alert('휴대전화1을 입력해 주시기 바랍니다.');
				cm_hp1.focus();
				return;
			}	

			if (cm_hp2.value == "") {
				alert('휴대전화2를 입력해 주시기 바랍니다.');
				cm_hp2.focus();
				return;
			}	
			if (cm_hp3.value == "") {
				alert('휴대전화3을 입력해 주시기 바랍니다.');
				cm_hp3.focus();
				return;
			}	
			
			if (cm_tel1.value == "") {
				alert('전화번호1을 입력해 주시기 바랍니다.');
				cm_tel1.focus();
				return;
			}	

			if (cm_tel2.value == "") {
				alert('전화번호1를 입력해 주시기 바랍니다.');
				cm_tel2.focus();
				return;
			}	
			if (cm_tel3.value == "") {
				alert('전화번호3을 입력해 주시기 바랍니다.');
				cm_tel3.focus();
				return;
			}	
			if (cm_email.value == "") {
				alert('이메일을 입력해 주시기 바랍니다..');
				cm_email.focus();
				return;
			}	


			if (checkboxcpy.checked ==false)
			{
				alert('약관동의에 체크해 주시기 바랍니다.');
				return;
			}
			f.action = "join_ok_new.asp"
			f.submit();		
		}

	function execDaumPostCode(input_type) {
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 도로명 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이  없는 경우엔 공백('')값을 가지므로, 이를 참고하여 분기 한다.
                var fullRoadAddr = data.roadAddress; // 도로명 주소 변수
                var extraRoadAddr = ''; // 도로명 조합형 주소 변수

                // 법정동명이 있을 경우 추가한다.
                if(data.bname !== ''){
                    extraRoadAddr += data.bname;
                }
                // 건물명이 있을 경우 추가한다.
                if(data.buildingName !== ''){
                    extraRoadAddr += (extraRoadAddr !== '' ? ', ' + data.buildingName : data.buildingName);
                }
                // 도로명, 지번 조합형 주소가 있을 경우, 괄호까지 추가한 최종 문자열을 만든다.
                if(extraRoadAddr !== ''){
                    extraRoadAddr = ' (' + extraRoadAddr + ')';
                }
                // 도로명, 지번 주소의 유무에 따라 해당 조합형 주소를 추가한다.
                if(fullRoadAddr !== ''){
                    fullRoadAddr += extraRoadAddr;
                }
                // 우편번호와 주소 정보를 해당 필드에 넣는다.
				if(data.userSelectedType == 'R')
				{
					document.getElementById("cust_zip1").value = left(data.zonecode,3)
					document.getElementById("cust_zip2").value = right(data.zonecode,2)
					
					document.getElementById("cust_addr").value = fullRoadAddr;					
				}
				 
				else if(data.userSelectedType == 'J'){

					 if (data.postcode1 =='')
						 {
							document.getElementById("cust_zip1").value = left(data.zonecode,3)
							document.getElementById("cust_zip2").value = right(data.zonecode,2)
						 }
						 else
						 {
							document.getElementById("cust_zip1").value = data.postcode1;
							document.getElementById("cust_zip2").value = data.postcode2;
						 }
					
					document.getElementById("cust_addr").value = data.jibunAddress;
				}


				document.getElementById("cust_addr").focus()			

                // 사용자가 '선택 안함'을 클릭한 경우, 예상 주소라는 표시를 해준다.
                if(data.autoRoadAddress) {
                    //예상되는 도로명 주소에 조합형 주소를 추가한다.
                    var expRoadAddr = data.autoRoadAddress + extraRoadAddr;
    //                document.getElementById("guide").innerHTML = '(예상 도로명 주소 : ' + expRoadAddr + ')';

                } else if(data.autoJibunAddress) {
                    var expJibunAddr = data.autoJibunAddress;
  //                  document.getElementById("guide").innerHTML = '(예상 지번 주소 : ' + expJibunAddr + ')';

                } else {
//                    document.getElementById("guide").innerHTML = '';
                }
            }
        }).open();
	}

	function left(s,c){
	  return s.substr(0,c);
	}//left("abcd",2)
	function right(s,c){
	  return s.substr(-c);
	}//right("abcd",2)
	function mid(s,c,l){
	  return s.substring(c,l);
	}//mid("abcd",1,2)
	function copy(s,c,l){
	  return s.substr(c,l);
	}//copy("abcd",1,2)

</script>
</head>
<body>
<div class="login_wrap">
	<h1><img src="/front/img/common/logo_police.png" alt="logo"/>
		<br>
		<img src="/front/img/common/logo_samsung.png" alt="logo"/>
	</h1>
	<form method="post" name="login_frm" id="login_frm" action="">
		<div class="login_form">
			<%
				If Gets_saved_id() = "Y" Then
					user_id = gate_GetsLOGINID()
					user_pwd = gate_GetsLOGINPWD()
				Else
					user_id  = ""
					user_pwd = ""
					
				End If 
			%> 

				<div class="login_box">
					<label for="input_id" class="hide">id</label>
					<input type="text" id="get_login_id" name="get_login_id"  placeholder="아이디" value="<%=user_id%>"/>
					<label for="input_pw" class="hide">password</label>
					<input type="password" id="online_pw" name="online_pw" class=""  placeholder="비밀번호" value="<%=user_pwd%>"/>
				</div>
			
			<div class="login_btn">
				<input type="button" class="button" onclick="javascript:chk_login();" value="로그인">
			</div>
			<div class="save_id">
				<input type="checkbox"  name="saveid" id="saveid" value="Y" <% If Gets_saved_id() = "Y" Then %> checked<% End If %> />
				<label for="saveid"><span></span>아이디 저장</label>
			</div>
			<div class="find_wrap">
				<p>로그인 정보를 잊으셨나요?</p>
				<a href="#" onclick="javascript:find_id_open();">아이디찾기</a> | 
				<a href="#" onclick="javascript:find_pw_open();">비밀번호찾기</a> | 
			</div>
		</div>
		<div class="cs_wrap">
			<p>고객센터 <a href="tel:15990745">1544-4268</a></p>
			<p>업무시간 오전 09:00 ~ 오후 18:00</p>
			<p>토요일.일요일.공휴일 휴무</p>
		</div>
	</form>

	<div class="popup_wrap join_wrap" id="join_layer">
		<div class="box">
			<h2>회원가입<button onClick="javascript:div_close();" id="close_x" type="button">X</button></h2>
			<form  name="reg_frm" method="post" >
				<input type="hidden" name="id_ok" id="id_ok">
				<input type="hidden" name="memo_ok" id="memo_ok">

				<dl>
					<dt class="point">※ 포인트가 남아있으신분은 성함과 사내이메일이 매칭되는분들에게 부여해 드리고 있으니 정확한 기입 부탁드립니다.</dt>
				</dl>
				<dl>
					<dt>아이디</dt>
					<dd>
						<label for="id" class="hide">id</label>
						<input type="text" id="online_id" class="" title="" value="" name="online_id"/><button class="btn_join" type="button" onclick="javascript:id_checked();">중복확인</button>
					</dd>
				</dl>
				<dl>
					<dt>사내이메일</dt>
					<dd>
						<label for="email" class="hide">email</label>
						<input type="email" id="memo" class="" title="" value="" name="memo"/><button class="btn_join" type="button" onClick="javascript:memo_checked();">중복확인</button>
					</dd>
				</dl>
				<dl>
					<dt>비밀번호</dt>
					<dd>
						<input type="password" id="cust_online_pwd" class="" title="" value="" name="cust_online_pwd" maxlength="10"/>
						<label for="pw" class="point">특수문자외 / 영문숫자조합 6~10자리</label>
					</dd>
				</dl>
				<dl>
					<dt>비밀번호 확인</dt>
					<dd>
						<input type="password" id="cust_re_online_pwd" class="" title="" value="" name="cust_re_online_pwd" maxlength="10"/>
						<label for="pwc" class="hide"></label>
					</dd>
				</dl>
				<dl>
					<dt>성명</dt>
					<dd>
						<label for="name" class="hide">name</label>
						<input type="text" id="cust_cust_custnm" class="" title="" value="" name="cust_cust_custnm"/>
					</dd>
				</dl>
				<dl>
					<dt>이메일주소</dt>
					<dd>
						<label for="email02" class="hide">email</label>
						<input type="email" id="cust_cm_email" class="" title="" value="" name="cust_cm_email"/>
					</dd>
				</dl>
				<dl>
					<dt>주소입력</dt>
					<dd>
						<label for="post01" class="hide">우편번호앞자리</label>
						<input type="number" id="cust_zip1" class="num_short" title="" value="" name="cust_zip1" maxlength="4"/> - 
						<label for="post02" class="hide">우편번호뒷자리</label>
						<input type="number" id="cust_zip2" class="num_short" title="" value="" name="cust_zip2" maxlength="4"/><button class="btn_join"  type="button" onClick="execDaumPostCode('memFRM','cust_zip1','cust_zip2','MEM_ADDR','cust_write')">검색</button><br/>
						<label for="post02" class="hide">우편번호뒷자리</label>
						<input type="text" id="cust_addr" class="num_long" title="" value="" name="cust_addr"/>
					</dd>
				</dl>
				<dl>
					<dt>휴대폰번호</dt>
					<dd>
						<label for="phone01" class="hide">첫자리</label>
						<input type="number" id="cust_cm_hp1" class="num_short" title="" value="" name="cust_cm_hp1" maxlength="3"/> - 
						<label for="phone02" class="hide">가운데자리</label>
						<input type="number" id="cust_cm_hp2" class="num_short" title="" value="" name="cust_cm_hp2" maxlength="4"/> - 
						<label for="phone03" class="hide">끝자리</label>
						<input type="number" id="cust_cm_hp3" class="num_short" title="" value="" name="cust_cm_hp3" maxlength="4"/>
					</dd>
				</dl>
				<dl>
					<dt>전화번호</dt>
					<dd>
						<label for="tel01" class="hide">지역번호</label>
						<input type="number" id="cust_cm_tel1" class="num_short" title="" value="" name="cust_cm_tel1" maxlength="3"/> - 
						<label for="tel02" class="hide">첫자리</label>
						<input type="number" id="cust_cm_tel2" class="num_short" title="" value="" name="cust_cm_tel2" maxlength="4"/> - 
						<label for="tel03" class="hide">끝자리</label>
						<input type="number" id="cust_cm_tel3" class="num_short" title="" value="" name="cust_cm_tel3" maxlength="4"/>
					</dd>
				</dl>
				<dl class="term">
					<dt>개인정보 취급방침</dt>
					<dd>
						<div class="term_cont">
							<p>한라e몰은 이용자들의 개인정보보호를 매우 중요시하며, 이용자가 회사의 서비스를 이용함과 동시에 온라인상에서 회사에 제공한 개인정보가 보호 받을 수 있도록 최선을 다하고 있습니다. 이에 한라e몰은 통신비밀보호법, 전기통신사업법, 정보통신망 이용촉진 및 정보보호 등에관한 법률 등 정보통신서비스제공자가 준수하여야 할 관련 법규상의 개인정보보호 규정 및 정보통신부가 제정한 개인정보보호지침을 준수하고 있습니다.<br/><br/>
							한라e몰은 개인정보 취급방침을 통하여 이용자들이 제공하는 개인정보가 어떠한 용도와 방식으로 이용되고 있으며 개인정보보호를 위해 어떠한 조치가 취해지고 있는지 알려 드립니다.<br/><br/>
							한라e몰은 개인정보 취급방침을 홈페이지 첫 화면에 공개함으로써 이용자들이 언제나 용이하게 보실수 있도록 조치하고 있습니다.<br/><br/>
							회사의 개인정보 취급방침은 정부의 법률 및 지침 변경이나 회사의 내부 방침 변경 등으로 인하여 수시로 변경될 수 있고, 이에 따른 개인정보 취급방침의 지속적인 개선을 위하여 필요한 절차를 정하고 있습니다. 그리고 개인정보 취급방침을 개정하는 경우나 개인정보 취급방침 변경될 경우 쇼핑몰의 첫페이지의 개인정보취급방침을 통해 고지하고 있습니다. 이용자들께서는 사이트 방문 시 수시로 확인하시기 바랍니다.<br/>
							한라e몰의 개인정보 취급방침은 다음과 같은 내용을 담고 있습니다.</p>

							<p>1. 개인정보 수집에 대한 동의<br/>
							2. 개인정보의 수집목적 및 이용목적<br/>
							3. 수집하는 개인정보 항목 및 수집방법<br/>
							4. 수집하는 개인정보의 보유 및 이용기간<br/>
							5. 수집한 개인정보의 공유 및 제공<br/>
							6. 이용자 자신의 개인정보 관리(열람,정정,삭제 등)에 관한 사항<br/>
							7. 쿠키(cookie)의 운영에 관한 사항<br/>
							8. 비회원 고객의 개인정보 관리<br/>
							9. 개인정보의 위탁처리<br/>
							10. 개인정보관련 의견수렴 및 불만처리에 관한 사항<br/>
							11. 개인정보 관리책임자 및 담당자의 소속-성명 및 연락처<br/>
							12. 고지의 의무</p>

							<p>1. 개인정보 수집에 대한 동의<br/><br/>
							한라e몰은 이용자들이 회사의 개인정보 취급방침 또는 이용약관의 내용에 대하여 「동의」버튼 또는「취소」버튼을 클릭할 수 있는 절차를 마련하여, 「동의」버튼을 클릭하면 개인정보 수집에 대해 동의한 것으로 봅니다.</p>

							<p>2. 개인정보의 수집목적 및 이용목적<br/><br/>
							"개인정보"라 함은 생존하는 개인에 관한 정보로서 당해 정보에 포함되어 있는 성명, 주민등록번호 등의 사항에 의하여 당해 개인을 식별할 수 있는 정보(당해 정보만으로는 특정 개인을 식별할 수 없더라도 다른 정보와 용이하게 결합하여 식별할 수 있는 것을 포함)를 말합니다.<br/>
							대부분의 서비스는 별도의 사용자 등록이 없이 언제든지 사용할 수 있습니다. 그러나 한라e몰은 회원서비스를 통하여 이용자들에게 맞춤식 서비스를 비롯한 보다 더 향상된 양질의 서비스를 제공하기 위하여 이용자 개인의 정보를 수집하고 있습니다. 한라e몰은 이용자의 사전 동의 없이는 이용자의 개인 정보를 공개하지 않으며, 수집된 정보는 아래와 같이 이용하고 있습니다.<br/><br/>

							첫째, 이용자들이 제공한 개인정보를 바탕으로 보다 더 유용한 서비스를 개발할 수 있습니다.<br/>

							한라e몰은 신규 서비스개발이나 컨텐츠의 확충 시에 기존 이용자들이 회사에 제공한 개인정보를 바탕으로 개발해야 할 서비스의 우선 순위를 보다 더 효율적으로 정하고, 한라e몰은 이용자들이 필요로 할 컨텐츠를 합리적으로 선택하여 제공할 수 있습니다.<br/>

							둘째, 수집하는 개인정보 항목과 수집 및 이용목적은 다음과 같습니다.<br/>
							성명, 아이디, 비밀번호 :회원제 서비스 이용에 따른 본인 확인 절차에 이용<br/>
							이메일주소, 전화번호 :고지사항 전달, 불만처리 등을 위한 원활한 의사소통 경로의 확보, 새로운 서비스 및 신상품이나 이벤트 정보 등의 안내<br/>
							은행계좌정보, 신용카드정보 :서비스 및 부가 서비스 이용에 대한 요금 결제<br/>
							주소, 전화번호 :청구서, 물품배송시 정확한 배송지의 확보<br/>
							기타 선택항목 :개인맞춤 서비스를 제공하기 위한 자료<br/>
							IP Address :불량회원의 부정 이용 방지와 비인가 사용 방지</p>

							<p>3. 수집하는 개인정보 항목 및 수집방법<br/><br/>

							한라e몰은 이용자들이 회원서비스를 이용하기 위해 회원으로 가입하실 때 서비스 제공을 위한 필수적인 정보들을 온라인상에서 입력 받고 있습니다. 회원 가입 시에 받는 필수적인 정보는 이름, 연락처, 이메일 주소 등입니다. 또한 양질의 서비스 제공을 위하여 이용자들이 선택적으로 입력할 수 있는 사항으로서 전화번호 등을 입력 받고 있습니다.<br/>
							또한 쇼핑몰 내에서의 설문조사나 이벤트 행사 시 통계분석이나 경품제공 등을 위해 선별적으로 개인정보 입력을 요청할 수 있습니다.
							그러나, 이용자의 기본적 인권 침해의 우려가 있는 민감한 개인정보(인종 및 민족, 사상 및 신조, 출신지 및 본적지, 정치적 성향 및 범죄기록, 건강상태 및 성생활 등)는 수집하지 않으며 부득이하게 수집해야 할 경우 이용자들의 사전동의를 반드시 구할 것입니다. 그리고, 어떤 경우에라도 입력하신 정보를 이용자들에게 사전에 밝힌 목적 이외에 다른 목적으로는 사용하지 않으며 외부로 유출하지 않습니다.</p>

							<p>4. 수집하는 개인정보의 보유 및 이용기간<br/><br/>

							이용자가 쇼핑몰 회원으로서 회사에 제공하는 서비스를 이용하는 동안 한라e몰은 이용자들의 개인정보를 계속적으로 보유하며 서비스 제공 등을 위해 이용합니다. 다만, 아래의 "6. 이용자 자신의 개인정보관리(열람,정정,삭제 등)에 관한 사항" 에서 설명한 절차와 방법에 따라 회원 본인이 직접 삭제하거나 수정한 정보, 가입해지를 요청한 경우에는 재생할 수 없는 방법에 의하여 디스크에서 완전히 삭제하며 추후 열람이나 이용이 불가능한 상태로 처리됩니다.<br/>

							그리고 "3. 수집하는 개인정보 항목 및 수집방법"에서와 같이 일시적인 목적 (설문조사, 이벤트, 본인확인 등)으로 입력 받은 개인정보는 그 목적이 달성된 이후에는 동일한 방법으로 사후 재생이 불가능한 상태로 처리됩니다.<br/>

							귀하의 개인정보는 다음과 같이 개인정보의 수집목적 또는 제공받은 목적이 달성되면 파기하는 것을 원칙으로 합니다. 다만, 한라e몰은 불량 회원의 부정한 이용의 재발을 방지하기 위해, 이용계약 해지일로부터 1년간 해당 회원의 주민등록번호를 보유할 수 있습니다.<br/>

							그리고 상법, 전자상거래 등에서의 소비자보호에 관한 법률 등 관계법령의 규정에 의하여 보존할 필요가 있는 경우 한라e몰은 관계법령에서 정한 일정한 기간 동안 회원정보를 보관합니다. 이 경우 한라e몰은 보관하는 정보를 그 보관의 목적으로만 이용하며 보존기간은 아래와 같습니다.<br/><br/>

							계약 또는 청약철회 등에 관한 기록 :5년<br/>
							대금결제 및 재화 등의 공급에 관한 기록 :5년<br/>
							소비자의 불만 또는 분쟁처리에 관한 기록 :3년<br/>
							한라e몰은 귀중한 회원의 개인정보를 안전하게 처리하며, 유출의 방지를 위하여 다음과 같은 방법을 통하여 개인정보를 파기합니다.<br/>

							종이에 출력된 개인정보는 분쇄기로 분쇄하거나 소각을 통하여 파기합니다.<br/>
							전자적 파일 형태로 저장된 개인정보는 기록을 재생할 수 없는 기술적 방법을 사용하여 삭제합니다.</p>

							<p>5. 수집한 개인정보의 공유 및 제공<br/><br/>

							한라e몰은 이용자들의 개인정보를 "2. 개인정보의 수집목적 및 이용목적"에서 고지한 범위 내에서 사용하며, 이용자의 사전 동의 없이는 동 범위를 초과하여 이용하거나 원칙적으로 이용자의 개인정보를 외부에 공개하지 않습니다. 다만, 아래의 경우에는 예외로 합니다.<br/><br/>

							이용자들이 사전에 공개에 동의한 경우<br/>
							서비스 제공에 따른 요금정산을 위하여 필요한 경우<br/>
							홈페이지에 게시한 서비스 이용 약관 및 기타 회원 서비스 등의 이용약관 또는 운영원칙을 위반한 경우<br/>
							자사 서비스를 이용하여 타인에게 정신적, 물질적 피해를 줌으로써 그에 대한 법적인 조치를 취하기 위하여 개인정보를 공개해야 한다고 판단되는 충분한 근거가 있는 경우<br/>
							기타 법에 의해 요구된다고 선의로 판단되는 경우 (ex. 관련법에 의거 적법한 절차에 의한 정부/수사기관의 요청이 있는 경우 등)<br/>
							통계작성, 학술연구나 시장조사를 위하여 특정개인을 식별할 수 없는 형태로 광고주, 협력업체나 연구단체 등에 제공하는 경우<br/>
							이용자의 서비스 이용에 따른 불만사항 및 문의사항(민원사항)의 처리를 위하여 고객센터를 위탁하는 경우<br/>
							고객센터를 운영하는 전문업체에 해당 민원사항의 처리에 필요한 개인 정보를 제공하는 경우</p>

							<p>6. 이용자 자신의 개인정보 관리(열람,정정,삭제 등)에 관한 사항<br/><br/>

							회원님이 원하실 경우 언제라도 당사에서 개인정보를 열람하실 수 있으며 보관된 필수 정보를 수정하실 수 있습니다.<br/>
							또한 회원 가입 시 요구된 필수 정보 외의 추가 정보는 언제나 열람, 수정, 삭제할 수 있습니다.<br/>
							회원님의 개인정보 변경 및 삭제와 회원탈퇴는 당사의 고객센터에서 로그인(Login) 후 이용하실 수 있습니다.</p>

							<p>7. 쿠키(cookie)의 운영에 관한 사항<br/><br/>

							당사는 회원인증을 위하여 Cookie 방식을 이용하고 있습니다. 이는 로그아웃(Logout)시 자동으로 컴퓨터에 저장되지 않고 삭제되도록 되어 있으므로 공공장소나 타인이 사용할 수 있는 컴퓨터를 사용 하 실 경우에는 로그인(Login)후 서비스 이용이 끝나시면 반드시 로그아웃(Logout)해 주시기 바랍니다.<br/>

							쿠키 설정 거부 방법<br/>
							쿠키 설정을 거부하는 방법으로는 회원님이 사용하시는 웹 브라우저의 옵션을 선택함으로써 모든 쿠키를 허용하거나 쿠키를 저장할 때마다 확인을 거치거나, 모든 쿠키의 저장을 거부할 수 있습니다.<br/>

							설정방법 예(인터넷 익스플로어의 경우) :웹 브라우저 상단의 도구 > 인터넷 옵션 > 개인정보<br/>
							단, 귀하께서 쿠키 설치를 거부하였을 경우 서비스 제공에 어려움이 있을 수 있습니다.</p>

							<p>8. 비회원고객의 개인정보관리<br/><br/>

							당사는 비회원 고객 또한 물품 및 서비스 상품의 구매를 하실 수 있습니다. 당사는 비회원 주문의 경우 배송 및 대금 결제, 상품 배송에 반드시 필요한 개인정보만을 고객님께 요청하고 있습니다.<br/>
							당사에서 비회원으로 구입을 하신 경우 비회원 고객께서 입력하신 지불인 정보 및 수령인 정보는 대금 결제 및 상품 배송에 관련한 용도 외에는 다른 어떠한 용도로도 사용되지 않습니다.</p>

							<p>9. 개인정보의 위탁처리<br/><br/>

							한라e몰은(는) 서비스 향상을 위해서 귀하의 개인정보를 필요한 경우 동의 등 법률상의 요건을 구비하여 외부에 수집 · 취급 · 관리 등을 위탁하여 처리할 있으며, 제 3자에게 제공할 수 있습니다.<br/>

							당사는 개인정보의 처리와 관련하여 아래와 같이 업무를 위탁하고 있으며, 관계 법령에 따라 위탁계약 시 개인정보가 안전하게 관리될 수 있도록 필요한 사항을 규정하고 있습니다. 또한 공유하는 정보는 당해 목적을 달성하기 위하여 필요한 최소한의 정보에 국한됩니다.<br/><br/>

							위탁 대상자 :한진택배<br/>
							위탁업무 내용 :주문상품 배송<br/>
							위탁 대상자 :KSNET<br/>
							위탁업무 내용 :온라인 전자결재<br/>

							당사는 귀하에게 편리하고 혜택이 있는 다양한 서비스를 제공하기 위하여 다음의 업체와 제휴합니다. 제공되는 개인정보의 항목은 회원가입 시 당사에 제공한 개인정보 중 아래와 같이 제공됩니다.<br/>
							제공대상:<br/>
							제공 개인정보 항목:<br/>
							정보 이용목적:<br/>
							정보 보유 및 이용기간:<br/>

							다만, 아래의 경우에는 예외로 합니다.<br/>
							이용자들이 사전에 동의한 경우<br/>
							법령의 규정에 의거하거나, 수사 목적으로 법령에 정해진 절차와 방법에 따라 수사기관의 요구가 있는 경우<br/>

							개인정보의 처리를 위탁하거나 제공하는 경우에는 수탁자, 수탁범위, 공유 정보의 범위 등에 관한 사항을 서면, 전자우편, 전화 또는 홈페이지를 통해 미리 귀하에게 고지합니다.</p>

							<p>10. 개인정보관련 의견수렴 및 불만처리에 관한 사항<br/><br/>

							당사는 개인정보보호와 관련하여 이용자 여러분들의 의견을 수렴하고 있으며 불만을 처리하기 위하여 모든 절차와 방법을 마련하고 있습니다.<br/>
							이용자들은 하단에 명시한 "11. 개인정보관리책임자 및 담당자의 소속-성명 및 연락처"항을 참고하여 전화나 메일을 통하여 불만사항을 신고 할 수 있고, 한라e몰은 이용자들의 신고사항에 대하여 신속하고도 충분한 답변을 해 드릴것입니다.</p>

							<p>11. 개인정보 관리책임자 및 담당자의 소속-성명 및 연락처<br/><br/>

							당사는 귀하가 좋은 정보를 안전하게 이용할 수 있도록 최선을 다하고 있습니다. 개인정보를 보호하는데 있어 귀하께 고지한 사항들에 반하는 사고가 발생할 경우 개인정보관리책임자가 책임을 집니다.<br/>

							이용자 개인정보와 관련한 아이디(ID)의 비밀번호에 대한 보안유지책임은 해당 이용자 자신에게 있습니다.<br/>
							한라e몰은 비밀번호에 대해 어떠한 방법으로도 이용자에게 직접적으로 질문하는 경우는 없으므로 타인에게 비밀번호가 유출되지 않도록 각별히 주의하시기 바랍니다.<br/>
							특히 공공장소에서 온라인상에서 접속해 있을 경우에는 더욱 유의하셔야 합니다.<br/>
							한라e몰은 개인정보에 대한 의견수렴 및 불만처리를 담당하는 개인정보 관리책임자 및 담당자를 지정하고 있고, 연락처는 아래와 같습니다.<br/>

							이 름 :유종갑<br/>
							소속 / 직위　: 전산팀 / 이사<br/>
							E-M A I L 　 :online@widline.co.kr<br/>
							전 화 번 호　 :02-715-0282</p>

							<p>12. 고지의 의무<br/><br/>

							현 개인정보취급방침의 내용은 정부의 정책 또는 보안기술의 변경에 따라 내용의 추가 삭제 및 수정이 있을 시에는 홈페이지의 '공지사항'을 통해 고지할 것입니다.</p>
						</div>
						<input type="checkbox" id="cust_checkboxcpy" name="cust_checkboxcpy" class="input_chk"/><label for="cust_checkboxcpy"><span></span>개인정보 수집 및 이용 동의</label>
					</dd>
				</dl>
			<div class="btn_wrap">
				<a href="javascript:write_ok();" class="btn_agree">등록하기</a>
				<a href="javascript:div_close();" >취소</a>
			</div>
			</form>
		</div>
	</div><!-- join_layer e -->
	
	<form name="id_search_form"  method="post">
		<div class="popup_wrap popup03" id="find_id">
			<div class="box">
				<h2>아이디 찾기<button onClick="javascript:find_id_close();" id="close_x"  type="button">X</button></h2>
				<ul class="find_info">
					<li>- 회원가입시 작성하신 정보를 입력하시면 아이디를 찾을 수 있습니다.</li>
					<li>- 입력정보를 잊으신 경우 고객센터로 문의해주세요. 고객센터( T.1599-6744 )</li>
				</ul>
				<div class="find_box">
					<form method="post" action="">
					<div class="find_cont">
						<input type="radio" id="email" class="input_radio" title="" value="Email" name="selectCheck" checked="checked"/>
						<label for="email">이메일 주소로 찾기</label>
						<div>
							<label for="input_name">이름</label>
							<input type="text" id="input_name" class="input_txt" title="" value="" name="Email_UserName" maxlength="25"/><br/>
							<label for="input_email">이메일 주소</label>
							<input type="text" id="input_email" class="input_txt" title="" value="" name="Email" placeholder="market@halla.com" maxlength="40"/>
						</div>
					</div>
					<div class="find_cont">
						<input type="radio" id="number" class="input_radio" title="" value="Phone" name="selectCheck"/>
						<label for="number">전화번호로 찾기</label>
						<div>
							<label for="input_name">이름</label>
							<input type="text" id="input_name" class="input_txt" title="" value="" name="Phone_UserName"/><br/>
							<label for="input_num">일반전화/휴대폰</label>
							<input type="text" id="input_num" class="input_txt" title="" value="" name="Phone" placeholder="- 없이 입력해 주세요."/>
						</div>
					</div>
					<div class="btn_wrap">
						<a href="javascript:CheckInfo();" class="btn_agree">아이디 찾기</a>
					</div>
					</form>
				</div>
			</div>
			<div class="box end_box" style="display:none;" id="id_search_div">
				<h2>아이디 찾기 완료<button onClick="javascript:id_search_div_close();" id=""  type="button">X</button></h2>
				<p class="end_box_cont">고객님의 아이디는<br/><span id="sarch_id_span">id</span><br/>입니다.</p>
				<div class="btn_wrap">
					<a href="#" class="btn_agree" onClick="javascript:id_search_div_close();">닫기</a>
				</div>
			</div>
		</div><!-- find_id e -->
	</form>
	<form name="pwd_search_form" method="post">
		<div class="popup_wrap popup03" id="find_pw">
			<div class="box">
				<h2>비밀번호 찾기<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
				<ul class="find_info">
					<li>- 회원가입시 작성하신 아이디와 이메일 주소로 임시비밀번호가 발송됩니다.</li>
					<li>- 임시비밀번호로 로그인하신 후 마이페이지 > 회원정보 페이지에서 비밀번호를 변경하시기 바랍니다.</li>
					<li>- 등록되어 있는 이메일 주소와 일치하지 않을 경우 고객센터로 문의해주세요. ( T.1599-6744 )</li>
				</ul>
				<div class="find_box">
					<form method="post" action="">
					<div class="find_cont">
						<label for="input_id">아이디</label>
						<input type="text" id="input_id" class="" title="" value="" name="PWD_UserID"/><br/>
						<label for="input_email">이메일 주소</label>
						<input type="text" id="input_email" class="" title="" value="" name="PWD_UserEmail" placeholder="market@halla.com"/>
					</div>
					<div class="btn_wrap">
						<a href="javascript:CheckEmail();" class="btn_agree" style="width:auto;padding:10px;">임시비밀번호 이메일 발송</a>
					</div>
					</form>
				</div>
			</div>
			<div class="box end_box" style="display:none;" id="pwd_sarch_div">
				<h2>비밀번호 찾기완료<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
				<p class="end_box_cont"><span id="sarch_pwdid_span">id</span>님의 임시비밀번호가<br/><span id="sarch_pwd_span">id@nate.com</span>으로<br/> 발송되었습니다.</p>
				<p class="end_box_info">발송 된 임시비밀번호로 로그인 후<br/>"마이페이지 > 회원정보" 페이지에서<br/>비밀번호를 변경해주세요.</p>
				<div class="btn_wrap">
					<a href="#" class="btn_agree" onClick="javascript:pwd_sarch_div_close();">닫기</a>
				</div>
			</div>
		</div><!-- find_pw e -->
	</form>
</div>
<!--팝업1-->
<div id="popup01" class="popup_cont" style="display:none;">
	<a href="javascript:;" onclick="onCommonOs();"><img src="/front/img/popup/popup_naver_m.jpg" usemap="#pop"/></a>
	<div class="btn_close">
		<a href="#" onclick="todaycloseWin();" alt="오늘 하루 열지 않기">오늘 하루 열지 않기</a>
		<a href="#" onclick="closeWin();" alt="닫기">닫기</a>
	</div>
</div>
<script language="javascript" type="text/javascript">
function pwd_sarch_div_close()
{
	
	document.getElementById("pwd_sarch_div").style.display ='none'
}
function div_close(){
	document.getElementById("join_layer").style.display ='none'
}
function div_open(){
	document.getElementById("join_layer").style.display ='block'
}
function find_id_close(){
	document.getElementById("find_id").style.display ='none'
}
function find_id_open(){
	document.getElementById("id_search_div").style.display ='none' 
	document.getElementById("find_id").style.display ='block'
}
function find_pw_close(){
	document.getElementById("find_pw").style.display ='none'
}
function find_pw_open(){
	document.getElementById("find_pw").style.display ='block'
}

function id_search_div_close()
{
	document.getElementById("id_search_div").style.display ='none' 
	return;
}

/*팝업1*/
function closeWin() { 
document.getElementById('popup01').style.display = "none";
}
function todaycloseWin() { 
setCookie( "ncookie", "done" , 24 ); 
document.getElementById('popup01').style.display = "none";
}
</script>

<script>
function onCommonOs() { // 함수처리
	var userAgent = navigator.userAgent;
	if (userAgent.match(/iPhone|iPad|iPod/)) { // ios
		var appstoreUrl = "http://itunes.apple.com/kr/app/id393499958?mt=8";
		var clickedAt = +new Date;
		location.href = appstoreUrl; 
	} else { // 안드로이드
		location.href = "Intent://#Intent;scheme=naversearchapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nhn.android.search;end"
	}
}
</script>

</body>
</html>

<script language="Javascript">
/*팝업1*/
cookiedata = document.cookie; 
if ( cookiedata.indexOf("ncookie=done") < 0 ){ 
document.getElementById('popup01').style.display = "block";
} else {
document.getElementById('popup01').style.display = "none"; 
}
</script>