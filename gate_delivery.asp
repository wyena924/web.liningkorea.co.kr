<!-- #include virtual = "/include/common.asp" -->
<!-- #include virtual = "include/head.asp" -->
<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta name="Generator" content="EditPlus®">
<meta name="Author" content="">
<meta name="Keywords" content="">
<meta name="Description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1,minimum-scale=1, maximum-scale=2, user-scalable=no">
<title></title>
<link type="text/css" rel="stylesheet" media="screen" href="front/css/gate.css"/>
<script language="javascript" type="text/javascript">
//팝업창
$(document).ready(function(){
	//팝업창
	$('.open').click(function(){
		var $href = $(this).attr('href');
		layer_popup($href);
	});
});

	function layer_popup(pop){
		var $pop = $(pop); //레이어의 id를 $pop 변수에 저장
		$pop.fadeIn();
		$('html,body').addClass('fix');
		$pop.find('.btn_close, .pop_close').click(function(){
			$pop.fadeOut(); // 닫기 버튼을 클릭하면 레이어가 닫힌다.
			$('html,body').removeClass('fix');
			return false;
		});
		$pop.find('.reload').click(function(){
			location.reload();
		});
	}
</script>

<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js"></script>
<script language='javascript'>

	setTimeout(function(){ order_search(1,10,'paging') }, 3000);
 

	function order_search(NowPage,ViewCnt,paging_gb)
		{
			var search_key = document.getElementById("sch"); //서치키 
			var search_keyword = document.getElementById("ip_sch"); //서치키워드
			
			if (paging_gb !='paging')
			{
				if (search_key.value=='')
				{
					alert('주문조건을 화인해주시기 바랍니다');
					search_key.focus();
					return;
				}
				if (search_keyword.value=='')
				{
					alert('검색어를 입력해주시기 바랍니다.');
					search_keyword.focus();
					return;
				}
			}
			//alert(search_key.value);
			//alert(search_keyword.value);
			var strAjaxUrl="/ajax/gate_delivery_ajax_list.asp"
			var retDATA="";
			//alert(strAjaxUrl);
			$.ajax({
				 type: 'POST',
				 url: strAjaxUrl,
				 dataType: 'html',
				 data: {
				    NowPage:NowPage,
					ViewCnt:ViewCnt,
					search_key:search_key.value,
					search_keyword:search_keyword.value
				},						  
				success: function(retDATA) {
					if(retDATA)
						{

							if(retDATA!="")
							{
								document.getElementById("delivery_ajax_list").innerHTML= retDATA;
							}
						}
				 }
			}); //close $.ajax(
		}

	function order_ok(gd_memo,gubun)
		{
			if (gubun=='2')
			{
				alert('휴대폰 가입창으로 이동합니다.');
				document.Orderfrm.target="_blank"
				document.Orderfrm.action = gd_memo;
				document.Orderfrm.submit();
				
			}
			else
			{

				document.Orderfrm.target="_top"
				document.Orderfrm.action = gd_memo;
				document.Orderfrm.submit();
			}
			
		}

	function search_text_set(this_is)
		{
			var sel = document.getElementById('sch');
			var selected = sel.options[sel.selectedIndex];
			var extra = selected.getAttribute('data-foo');
			document.getElementById("search_text").innerHTML = extra
		}

	function link_location(link,target)
		{
			document.bform.target=target
			document.bform.action= link
			document.bform.submit();			
			 
		}
</script>
</head>
<body>
<iframe src="" id="email_iframe" name="email_iframe" style="display:none;"></iframe>
<form method="post" name="Orderfrm" id="Orderfrm"  action="" target="popwin">
</form>
<form name="bform" method="post">
	<input type="hidden" id="get_j_email" name="get_j_email">
	<input type="hidden" id="get_j_email_code" name="get_j_email_code">
</form>
<div class="container sub">
	<div class="location_wrap">
		<a href="gate.asp"><< Gate페이지로이동</a></span>
	</div><!-- location_wrap e -->
	<div class="order_page sub_bg">
		<div class="list_page delivery_page">
			<form method="post" action="">
				<div class="top_wrap">
					<div class="list_wrap">
						<div class="sch_wrap">
							<div class="select_box" style="z-index:9999;">
								<label for="sch" id="search_text">검색조건</label>
								<select id="sch" name="search_key" onclick="javascript:search_text_set(this);">
									<option value=""  data-foo="검색조건" selected="selected" >검색조건</option>
									<option value="or_tel"  data-foo="연락처" >연락처</option>
									<option value="or_num"  data-foo="주문번호">주문번호</option>
								</select>
							</div><!-- select_box e -->
							<div class="sch_cont">
								<label for="ip_sch" class="hide">검색창</label>
								<input type="text" id="ip_sch" name="search_keyword" placeholder="주문데이터를 검색해주세요"/>
							</div><!-- sch_cont e -->
							<div class="ip_btn_wrap" style="float:center;">
								<input type="button" id="" class="ip_btn" title="" value="주문조회" name="" onclick="order_search()"/>
							</div>
						</div><!-- sch_wrap e -->
						<br>
						<div class="sch_info">
							<p class="block_m">· 조회 된 목록을 클릭하시면 상세조회를 하실 수 있습니다.</p>
							<p>· 주문 취소는 [주문 완료] 상태에서만 가능합니다.</p>
							<p>· 그 외에 주문취소를 원하실 경우 고객센터로 문의 바랍니다.</p>
						</div><!-- sch_info e -->
					</div><!-- list_wrap e -->
					<div class="bottom_wrap" ID="delivery_ajax_list">>
					</div><!-- bottom_wrap e -->
				</div>
			</form>
		</div>
	</div>
</div>
<BR><BR>
<div class="footer_wrap_gae">
	<div class="ft_box">
		<div class="footer">
			<div class="ft_nav">
				<a href="#pop_terms" class="open">이용약관</a>
				<a href="#pop_privacy" class="open">개인정보처리방침</a>
				<a href="#pop_faq" class="open">자주하는질문</a>
				<a href="javascript:link_location('http://www.ftc.go.kr/www/bizCommView.do?key=232&apv_perm_no=2005313011830203971&pageUnit=10&searchCnd=bup_nm&searchKrwd=%EC%9C%84%EB%93%9C%EB%9D%BC%EC%9D%B8&pageIndex=1','_blank')">사업자정보확인</a>
			</div>
			<address>
				<span class="company">삼성전자B2B몰 고객센터</span>
				<span>(주)위드라인 대표이사 : 김영석</span><br/>
				<span>E-mail : online@widline.co.kr</span><br/>
				<span>서울시 마포구 삼개로 16 근신빌딩 본관 5층 | 사업자등록번호 108-81-66493</span><br>
				<span>통신판매번호 : 2005-03971</span><br>
<!-- 				<span>고객센터 : 1544-4268</span><br/> -->
	<!-- 			<span>통신판매신고번호: 01-2076호</span> -->
			</address>
			<!--cite>Copyright © 2016 by 경찰공제회. All Rights Reserved</cite-->
		</div>
		<div class="ft_sc_wrap">
			<ul class="info_cont">
				<li>
					<h3>고객센터</h3>
					<div class="detail sc_wrap">
						<p class="call">
							회원가입 및 지점안내 : <span>070-7437-8142</span><br/>
							상품 및 온라인상품문의 : <span>010-9858-7303</span><br/>
							개통휴대폰(SK,LG) 안내 : <span>1811-3191</span><br/>
							개통휴대폰(KT) 안내 : <span>1899-2077</span><br/>
						</p>
						<p class="call">
							조명 상품관련 문의 : <span>02-751-9458</span><br/>
							조명설치 관련 문의 : <span>02-701-6074</span><br/>
							토요일,일요일,공휴일 휴무<br/>
							업무시간 : 오전09:00~오후06:00 
						</p>
					</div>
				</li>
			</ul><!-- info_cont e -->
		</div><!-- ft_sc_wrap e -->
	</div>
</div><!-- footer_wrap e -->

<!-- #include virtual = "/include/popup/pop_terms.asp" -->
<!-- #include virtual = "/include/popup/pop_privacy.asp" -->
<!-- #include virtual = "/include/popup/pop_faq.asp" -->

</body>
</html>
</body>
</html>

