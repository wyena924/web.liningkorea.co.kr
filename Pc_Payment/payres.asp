<!--#include virtual="/Library/ajax_config.asp"-->
<%
    '/*
    ' * [최종결제요청 페이지(STEP2-2)]
	' *
    ' * 매뉴얼 "5.1. Smart XPay 결제 요청 페이지 개발"의 "단계 5. 최종 결제 요청 및 요청 결과 처리" 참조
	' *
    ' * LG유플러스으로 부터 내려받은 LGD_PAYKEY(인증Key)를 가지고 최종 결제요청.(파라미터 전달시 POST를 사용하세요)
    ' */

	' ※ 중요
	' 환경설정 파일의 경우 반드시 외부에서 접근이 가능한 경로에 두시면 안됩니다.
	' 해당 환경파일이 외부에 노출이 되는 경우 해킹의 위험이 존재하므로 반드시 외부에서 접근이 불가능한 경로에 두시기 바랍니다.
	' 예) [Window 계열] C:\inetpub\wwwroot\lgdacom -- 절대불가(웹 디렉토리)

    configPath = "D:\web.liningkorea.co.kr\Pc_Payment\lgdacom"	  'LG유플러스에서 제공한 환경파일("/conf/lgdacom.conf, /conf/mall.conf") 위치 지정.

    '/*
    ' *************************************************
    ' * 1.최종결제 요청 - BEGIN
    ' *  (단, 최종 금액체크를 원하시는 경우 금액체크 부분 주석을 제거 하시면 됩니다.)
    ' *************************************************
    ' */
   CST_PLATFORM               = trim(request("CST_PLATFORM"))             'LG유플러스 결제 서비스 선택(test:테스트, service:서비스)
   CST_MID                    = trim(request("CST_MID"))                  '상점아이디(LG유플러스으로 부터 발급받으신 상점아이디를 입력하세요)
	                                                                 '테스트 아이디는 't'를 반드시 제외하고 입력하세요.
	if CST_PLATFORM = "test" then                                    '상점아이디(자동생성)
      LGD_MID = "t" & CST_MID
	else
	    LGD_MID = CST_MID
	end If


    LGD_PAYKEY                 = trim(request("LGD_PAYKEY"))

    Dim xpay            '결제요청 API 객체
    Dim amount_check    '금액비교 결과
    Dim i, j
    Dim itemName

	'해당 API를 사용하기 위해 setup.exe 를 설치해야 합니다.
	' (1) XpayClient의 사용을 위한 xpay 객체 생성
    Set xpay = server.CreateObject("XPayClientCOM.XPayClient")

	' (2) XPayClient 초기화(환경설정 파일 로드)
	' configPath: 설정파일
	' CST_PLATFORM: - test, service 값에 따라 lgdacom.conf의 test_url(test) 또는 url(srvice) 사용
	'				- test, service 값에 따라 테스트용 또는 서비스용 아이디 생성
    xpay.Init configPath, CST_PLATFORM

	' (3) Init_TX: 메모리에 mall.conf, lgdacom.conf 할당 및 트랜잭션의 고유한 키 TXID 생성
    xpay.Init_TX(LGD_MID)
    xpay.Set "LGD_TXNAME", "PaymentByKey"
    xpay.Set "LGD_PAYKEY", LGD_PAYKEY

    '금액을 체크하시기 원하는 경우 아래 주석을 풀어서 이용하십시요.
	'DB_AMOUNT = "DB나 세션에서 가져온 금액" 	'반드시 위변조가 불가능한 곳(DB나 세션)에서 금액을 가져오십시요.
	'xpay.Set "LGD_AMOUNTCHECKYN", "Y"
	'xpay.Set "LGD_AMOUNT", DB_AMOUNT

    '/*
    ' *************************************************
    ' * 1.최종결제 요청(수정하지 마세요) - END
    ' *************************************************
    ' */

    '/*
    ' * 2. 최종결제 요청 결과처리
    ' *
    ' * 최종 결제요청 결과 리턴 파라미터는 연동메뉴얼을 참고하시기 바랍니다.
    ' */
	' (4) TX: lgdacom.conf에 설정된 URL로 소켓 통신하여 최종 인증요청, 결과값으로 true, false 리턴
    If  xpay.TX() Then
        '1)결제결과 화면처리(성공,실패 결과 처리를 하시기 바랍니다.)
        Response.Write("결제요청이 완료되었습니다.. <br>")
        Response.Write("TX Response_code = " & xpay.resCode & "<br>")				'통신 응답코드("0000" 일 때 통신 성공)
        Response.Write("TX Response_msg = " & xpay.resMsg & "<p>")

        Response.Write("거래번호 : " & xpay.Response("LGD_TID", 0) & "<br>")
        Response.Write("상점아이디 : " & xpay.Response("LGD_MID", 0) & "<br>")
        Response.Write("상점주문번호 : " & xpay.Response("LGD_OID", 0) & "<br>")
        Response.Write("결제금액 : " & xpay.Response("LGD_AMOUNT", 0) & "<br>")
        Response.Write("결과코드 : " & xpay.Response("LGD_RESPCODE", 0) & "<br>")	'LGD_RESPCODE 결제요청 응답 값
        Response.Write("결과메세지 : " & xpay.Response("LGD_RESPMSG", 0) & "<p>")

        Response.Write("[결제요청 결과 파라미터]<br>")

        '아래는 결제요청 결과 파라미터를 모두 찍어 줍니다.
        Dim itemCount
        Dim resCount
        itemCount = xpay.resNameCount
        resCount = xpay.resCount

        For i = 0 To itemCount - 1
          itemName = xpay.ResponseName(i)
          Response.Write(itemName & "&nbsp: ")
          For j = 0 To resCount - 1
    				ACNTNO = xpay.Response("LGD_FINANCEAUTHNUM",0) 'Request("LGD_FINANCEAUTHNUM") '카드사승인번호
    				ACNTNM = xpay.Response("LGD_FINANCENAME",0) '(Request("LGD_FINANCENAME"))'카드사명

            Response.Write(xpay.Response(itemName, j) & "<br>")
    				Response.write "ACNTNO : "&ACNTNO&"<br>"
    				Response.write "ACNTNM : "&ACNTNM&"<br>"
          Next
        Next

		If xpay.resCode = "0000" Then
			scriptstr = "opener.payment_return('" &xpay.Response("LGD_TID", 0)& "', '" & ACNTNO & "', '"& ACNTNM &"'); self.close();"
		Else
			scriptstr = "opener.payment_error_return('"&xpay.resMsg&"');self.close();"
		End if

  Else
    '2)API 요청실패 화면처리
    Response.Write("결제요청이 실패하였습니다. <br>")
    Response.Write("TX Response_code = " & xpay.resCode & "<br>")
    Response.Write("TX Response_msg = " & xpay.resMsg & "<p>")

    '결제요청 결과 실패 상점 DB처리
    Response.Write("결제결제요청 결과 실패 DB처리하시기 바랍니다." & "<br>")
		scriptstr = "opener.payment_error_return('"&xpay.resMsg&"');self.close();"
	End If

	Response.write " 입금자 : "
 %>

<script>
	<%=scriptstr%>
	//opener.payment_return();self.close();
</script>
