<!--#include virtual="/Library/ajax_config.asp"-->
<%
	
  Set payReqMap = Session.Contents("PAYREQ_MAP")

  'payreq_crossplatform.asp 에서 세션에 저장했던 파라미터 값이 유효한지 체크
  '세션 유지 시간(로그인 유지시간)을 적당히 유지 하거나 세션을 사용하지 않는 경우 DB처리 하시기 바랍니다.
	if IsNull(payReqMap)then
		response.write "세션이 만료 되었거나 유효하지 않은 요청 입니다."
	end if 


	GD_SEQ			= request("GD_SEQ")			 '상품코드
	P_EA			= request("P_EA")			 '수량
	user_name		= request("user_name")		 '수령인명
	cust_email1		= request("cust_email1")	 '수령인우편번호1
	cust_email2		= request("cust_email2")	 '수령인우편번호1
	tel1			= request("tel1")			 '수령인전화지역번호
	tel2			= request("tel2")			 '수령인전화국번
	tel3			= request("tel3")			 '수령인전화국번
	zip				= request("zip")			 '수령인우편번호1
	addr1			= request("addr1")			 '수령인 주소1
	addr2			= request("addr2")			 '수령인 주소2
	delivery_memo	= request("delivery_memo")	 '배송메모 
	gdorderlist		= request("gdorderlist")	 '상품구매내역 
	addr			= addr1 &" "&addr2
	Item_Count		= Request("Item_Count") '	주문한 상품수 예)3가지 상품
	isscdnm			= Request("isscdnm")	'카드
	trno			= Request("strtrno")
	cust_email		= cust_email1&"@"&cust_email2
	S_DATE			= Year(Now)&month_type & day_type '주문일 계산
	mobile_gb		= request("mobile_gb")
	POINT			= request("POINT")
	or_else_memo	= request("or_else_memo")

%>

<HTML>
<head>
	<script type="text/javascript">
		function setLGDResult() {
			document.getElementById('LGD_PAYINFO').submit();
		}
	</script>
</head>
<body onload="setLGDResult()">
<%
  LGD_RESPCODE = Trim(Request("LGD_RESPCODE"))
  LGD_RESPMSG  = Trim(Request("LGD_RESPMSG"))
  LGD_PAYKEY	 = ""

  if LGD_RESPCODE = "0000" then
	  LGD_PAYKEY = Trim(Request("LGD_PAYKEY"))
	  payReqMap.item("LGD_RESPCODE") = LGD_RESPCODE
	  payReqMap.item("LGD_RESPMSG")  = LGD_RESPMSG
	  payReqMap.item("LGD_PAYKEY")   = LGD_PAYKEY
%><form method="post" name="LGD_PAYINFO" id="LGD_PAYINFO" action="payres.asp"><%
    For Each eachitem In payReqMap
      response.write "<input type=""hidden"" name="""& eachitem &""" id="""& eachitem &""" value=""" & payReqMap.item(eachitem) & """><br>"
    Next
%></form><%
  }
  else
	  response.write "LGD_RESPCODE:" & LGD_RESPCODE & " ,LGD_RESPMSG:" & LGD_RESPMSG '인증 실패에 대한 처리 로직 추가
  end if
%>
</body>
</html>
<% If LGD_RESPCODE = "S053" Then %>
	<script language='javascript'>
		top.history.go(-3);
	</script>
<% End If %>