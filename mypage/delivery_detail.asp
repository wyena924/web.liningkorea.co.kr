<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	or_num = request("or_num")

	strqry = "	SELECT	  "
	strqry = strqry & "	  A.seq,	"
	strqry = strqry & "	  LEFT(A.OR_DT,4) +'.' + SUBSTRING(A.OR_DT,5,2) + '.' + RIGHT(A.OR_DT,2) AS OR_DT,	"
	strqry = strqry & "	  A.OR_NUM,	"
	strqry = strqry & "	  CONVERT(VARCHAR(10),B.CNT) AS GD_CNT,	"
	strqry = strqry & "	  GD_PRICE AS GD_PRICE,	"
	strqry = strqry & "	  OR_IN_PE2 ,	"
	strqry = strqry & "	  OR_IN_TEL1 + '-' + OR_IN_TEL2 + '-' + OR_IN_TEL3 AS OR_IN_TEL,	"
	strqry = strqry & "	  OR_STATE,	"
	strqry = strqry & "	  CASE WHEN A.OR_STATE = '00' THEN '주문완료'	"
	strqry = strqry & "	  	WHEN A.OR_STATE = '01' THEN '진행중'	"
	strqry = strqry & "	  	WHEN A.OR_STATE = '02' THEN '배송완료'	" 
	strqry = strqry & "	  	WHEN A.OR_STATE = '99' THEN '취소'	"
	strqry = strqry & "	  ELSE '알수없음' END AS OR_STATE_NM,	"
	strqry = strqry & "	  OR_IN_ZIP + '' + OR_IN_ZIP2 as OR_IN_ZIP ,	"	
	strqry = strqry & "	  OR_IN_ADDR,	"	
	strqry = strqry & "	  OR_IN_MEMO,	"
	strqry = strqry & "	  CNCL_ORDER_GD_SEQ,	"
	strqry = strqry & "	  ISNULL(D.CNT, 0) AS PY_IN_CNT ,"
	strqry = strqry & "	  ISNULL(D.PY_IN_BANK,'') AS PY_IN_NM,"
	strqry = strqry & "	  ISNULL(D.PY_IN_PAY,'') AS PY_IN_PAY,"
	strqry = strqry & "	  ISNULL(E.CNT, 0) AS POINT_IN_CNT,"
	strqry = strqry & "	  ISNULL(E.PY_IN_BANK, '') AS POINT_IN_NM, "
	strqry = strqry & "	  ISNULL(E.PY_IN_PAY,'') AS PY_IN_POINT, "
	strqry = strqry & "	  D.PY_IN_ACNT, "
	strqry = strqry & "	  D.PY_TP "
	strqry = strqry & "	FROM IC_T_ORDER_GD A	"
	strqry = strqry & "	INNER JOIN 	"
	strqry = strqry & "	( 	"
	strqry = strqry & "	  SELECT OR_NUM	"
	strqry = strqry & "			,SUM(A.GD_PRICE * A.GD_VOL) AS GD_PRICE	"
	strqry = strqry & "			,COUNT(*) AS CNT  "
	strqry = strqry & "	  FROM IC_T_ORDER_GD_SUB  A  "
	strqry = strqry & "	  INNER JOIN IC_T_GDS_INFO B  "
	strqry = strqry & "	  ON A.GD_SEQ = B.SEQ  "
	strqry = strqry & "	  WHERE A.DEL_YN = 'N'  "
	strqry = strqry & "	  AND  B.DEL_YN = 'N'  "
	strqry = strqry & "	  GROUP BY  OR_NUM	"
	strqry = strqry & "	) B  "
	strqry = strqry & "	ON A.OR_NUM = B.OR_NUM  "
	strqry = strqry & "	LEFT OUTER JOIN (SELECT  ORDER_GD_SEQ	 "
	strqry = strqry & "							,MAX(PY_IN_TP) PY_IN_TP "
	strqry = strqry & "							,MAX(PY_IN_BANK) PY_IN_BANK "
	strqry = strqry & "							,SUM(PY_IN_PAY) PY_IN_PAY "
	strqry = strqry & "							,MAX(PY_TP) PY_TP "
	strqry = strqry & "							,COUNT(*) AS CNT  "
	strqry = strqry & "							,max(PY_IN_ACNT) as PY_IN_ACNT   "
    strqry = strqry & "	                 FROM itemcenter.dbo.IC_T_ORDER_PAY WITH (NOLOCK) "
    strqry = strqry & "	                 WHERE DEL_YN = 'N' "
	strqry = strqry & "					 AND PY_IN_TP <> 'Z6' "
	'strqry = strqry & "					 AND OR_NUM  ='"&OR_NUM&"' "
    strqry = strqry & "	                 GROUP BY ORDER_GD_SEQ "
	strqry = strqry & "	) D "
	strqry = strqry & "	ON A.SEQ = D.ORDER_GD_SEQ "
	strqry = strqry & "	LEFT OUTER JOIN (SELECT  ORDER_GD_SEQ "
	strqry = strqry & "							,MAX(PY_IN_TP) PY_IN_TP "
	strqry = strqry & "							,MAX(PY_IN_BANK) PY_IN_BANK "
	strqry = strqry & "							,SUM(PY_IN_PAY) PY_IN_PAY "
	strqry = strqry & "							,COUNT(*) AS CNT  "
	strqry = strqry & "							,max(PY_IN_ACNT) as PY_IN_ACNT   "
    strqry = strqry & "	                 FROM itemcenter.dbo.IC_T_ORDER_PAY WITH (NOLOCK) "
    strqry = strqry & "	                 WHERE DEL_YN = 'N' "
	strqry = strqry & "					 AND PY_IN_TP = 'Z6' "
	'strqry = strqry & "					 AND OR_NUM  ='"&OR_NUM&"' "
    strqry = strqry & "	                 GROUP BY ORDER_GD_SEQ "
	strqry = strqry & "	) E "
	strqry = strqry & "	ON A.SEQ = E.ORDER_GD_SEQ "
	strqry = strqry & "	WHERE A.DEL_YN = 'N'  "
	strqry = strqry & "	AND A.OR_NUM = '"&OR_NUM&"'  "
'Response.write strqry
	DBOpen()
		Set rs = Dbcon.Execute(strqry)
	DBClose()
%>
<script language="javascript">
function View_Bill(seq,ugc){
	window.open('bill_list.asp?SEQ='+seq+"&unigrpcd="+ugc,'bill','width=850, height=575 scrollbars=yes');
}

function orderCancle(or_num, seq) {
  if (!confirm("해당 주문이 취소됩니다. 동의하시면 확인버튼을 눌러주세요.")){
      return;
	}

	//location.href='delivery_cancle_ok.asp?or_num=' + or_num + '&seq=' + seq;
	var strAjaxUrl="/ajax/delibery_cancle_ok.asp"
	var retDATA="";
	//alert(strAjaxUrl);
	$.ajax({
		 type: 'POST',
		 url: strAjaxUrl,
		 dataType: 'html',
		 data: {
		    or_num:or_num,
			seq:seq,
			url_href:"http://b2bc.samsungbizmall.com/"
		},						  
		success: function(retDATA) {
			if(retDATA)
				{
					alert(retDATA);
					document.c_bform.target="cancle_iframe"
					document.c_bform.action="http://ksnet.itemcenter.co.kr/KSPayCancelPost.asp"
					document.c_bform.submit();
				}
		 }
	}); //close $.ajax(
}


function card_view(t_id)
{
	window.open('https://pgims.ksnet.co.kr/pg_infoc/src/bill/new_credit_view.jsp?tr_no='+ t_id,'bill','width=550, height=875 scrollbars=yes');
}
</script>
<iframe id="cancle_iframe" name="cancle_iframe" width=0 height=0 ></iframe>
<div class="container sub">
	<div class="location_wrap">
		<span><a href="#">HOME</a></span><i></i>
		<span>주문/배송조회</span>
	</div><!-- location_wrap e -->
	<div class="order_page sub_bg">
		<form method="post" action="" name="c_bform" id="c_bform">
		<input type="hidden" id="storeid" name="storeid" value="2001105535">
		<input type="hidden" id="storepasswd" name="storepasswd" value="">
		<input type="hidden" id="authty" name="authty" value="1010">	
		<input type="hidden" id="trno" name="trno" value="<%=rs("PY_IN_ACNT")%>">
		<div class="delivery_detail_wrap">
			<div class="date">
				<span><%=rs("or_dt")%></span>
				<span><%=rs("or_num")%></span>
			</div>
			<ul class="order_list">
				<%
					'상품리스트
					strqry2 = "	SELECT		 MAX(AA.SEQ) SEQ	"
					strqry2 = strqry2 & "	 			,AA.GD_TP GD_TP 	"
					strqry2 = strqry2 & "	 			,MAX(AA.GD_NM) GD_NM 	"
					strqry2 = strqry2 & "	 			,GD_SIZE AS GD_OPTION1	"
					strqry2 = strqry2 & "	 			,GD_COLOR AS GD_OPTION2	"
					strqry2 = strqry2 & "	 			,AA.GD_GRP_BRAND GD_GRP_BRAND 	"
					strqry2 = strqry2 & "	 			,ITEMCENTER.dbo.IC_FN_BRAND_NM(AA.GD_GRP_BRAND) GD_GRP_BRAND_NM															"
					strqry2 = strqry2 & "	 			,ISNULL(MIN(AA.GD_PRICE8),0) END_PRICE 																					"
					strqry2 = strqry2 & "	 			,CASE WHEN RTRIM(LTRIM(MAX(AA.GD_IMG_PATH))) = '' OR MAX(AA.GD_IMG_PATH) IS NULL THEN 'gdsimage/listgdsnull1.jpg' 		"
					strqry2 = strqry2 & "	 				ELSE MAX(AA.GD_IMG_PATH) END IMG_PATH "
					strqry2 = strqry2 & "	 			,AA.UNI_GRP_CD GD_UNI_GRP_CD  "
					strqry2 = strqry2 & "	 			,AA.GD_MIN_VOL  "
					strqry2 = strqry2 & "	 			,MAX(BB.GD_NUM) GD_NUM  "
					strqry2 = strqry2 & "	 			,AA.GD_OPEN_YN GD_OPEN_YN  "
					strqry2 = strqry2 & "	 			,AA.GD_GRP_FIRST GD_GRP_FIRST  "
					strqry2 = strqry2 & "	 			,AA.GD_GRP_MID GD_GRP_MID  "
					strqry2 = strqry2 & "	 			,AA.GD_GRP_DETL GD_GRP_DETL  "
					strqry2 = strqry2 & "	 			,cc.gd_vol  "
					strqry2 = strqry2 & "	 			,CC.GD_PRICE  "
					strqry2 = strqry2 & "	 			,CC.GD_SEND_NUM  "
					strqry2 = strqry2 & "	 FROM ITEMCENTER.dbo.IC_T_GDS_INFO AA " 
					strqry2 = strqry2 & "	 INNER JOIN ITEMCENTER.dbo.IC_T_ONLINE_GDS BB "
					strqry2 = strqry2 & "	 ON AA.SEQ = BB.GD_SEQ  "
					strqry2 = strqry2 & "	 INNER JOIN ic_t_order_gd_sub cc  "
					strqry2 = strqry2 & "	 on bb.gd_seq = cc.gd_seq where 1=1 "
					'strqry2 = strqry2 & "	 WHERE AA.DEL_YN = 'N'  "
					'strqry2 = strqry2 & "	 AND BB.DEL_YN = 'N'  "
					strqry2 = strqry2 & "	 AND cc.del_yn = 'N'  "
					strqry2 = strqry2 & "	 AND AA.GD_BUY_END_YN = 'N'  "
					strqry2 = strqry2 & "	 AND AA.GD_OPEN_YN = 'N'  "
					strqry2 = strqry2 & "	 AND AA.GD_PRICE3 > 0  "
					strqry2 = strqry2 & "	 AND bb.ONLINE_CD ='"&GLOBAL_VAR_ONLINECD&"'  "
					strqry2 = strqry2 & "	 AND cc.OR_NUM ='"&OR_NUM&"'  "
					strqry2 = strqry2 & "	 GROUP BY		 AA.GD_TP  "
					strqry2 = strqry2 & "	 				,AA.GD_GRP_BRAND  "
					strqry2 = strqry2 & "	 				,AA.UNI_GRP_CD  "
					strqry2 = strqry2 & "	 				,AA.GD_MIN_VOL  "
					strqry2 = strqry2 & "	 				,BB.GD_TAX_YN  "
					strqry2 = strqry2 & "	 				,AA.GD_OPEN_YN 																										"
					strqry2 = strqry2 & "	 				,AA.GD_GRP_FIRST  "
					strqry2 = strqry2 & "	 				,AA.GD_GRP_MID    "
					strqry2 = strqry2 & "	 				,AA.GD_GRP_DETL  "
					strqry2 = strqry2 & "	 				,cc.gd_vol  "
					strqry2 = strqry2 & "	 				,gd_size  "
					strqry2 = strqry2 & "	 				,gd_color  "
					strqry2 = strqry2 & "	 				,CC.GD_PRICE  " 
					strqry2 = strqry2 & "	 				,CC.GD_SEND_NUM  " 
				
					DBOpen()
						Set rs2 = Dbcon.Execute(strqry2)
					DBClose()

				%>
				<% If rs2.bof Or rs2.eof Then %>
				<% Else %>
				<% 
					Do Until rs2.EOF 
				%>
				<li>
					<div class="img_cont">
						<a href="/product/detail.asp?seq=<%=rs2("SEQ")%>">
							<img src="http://www.itemcenter.co.kr/<%=rs2("IMG_PATH")%>" alt=""/>
						</a>
					</div><!-- img_cont e -->
					<div class="list_info">
						<div class="product_name"><%=hleft(rs2("GD_NM"),50)%></div>
						<div class="amount_wrap">
							<div class="amount_cont">
								<%=RS2("gd_vol")%>개
							</div>
							<% If RS2("GD_OPTION1") = "" And RS2("GD_OPTION2") <> "" Then %>
								<div class="option">옵션 : <%=RS2("GD_OPTION2")%></div>
							<% ElseIf RS2("GD_OPTION1") <> "" And  RS2("GD_OPTION2") = ""  Then %>
								<div class="option">옵션 : <%=RS2("GD_OPTION1")%></div>
								
							<% ElseIf RS2("GD_OPTION1") <> "" And  RS2("GD_OPTION2") <> ""  Then %>
								<div class="option">옵션 : <%=RS2("GD_OPTION1")%>/<%=RS2("GD_OPTION2")%></div>
							<% End If %> 
							<% If RS2("GD_SEND_NUM") <> "" Then %>
								<div class="amount_cont">&nbsp;&nbsp;&nbsp;/ 배송정보 : <%=RS2("GD_SEND_NUM")%></div>
							<% End If %>
						</div>
					</div><!-- list_info e -->
					<div class="price">
						<strike><%=FormatNumber(RS2("END_PRICE") * CLng(rs2("gd_vol")),0)%>원</strike><br/><b><%=FormatNumber(RS2("GD_PRICE") *  CLng(rs2("gd_vol")),0)%>
						</b>원
					</div>
				</li><!-- line 1 e -->
				<%
					rs2.MoveNext
					x = x + 1
					Loop
					rs2.Close
					Set rs2 = Nothing
					End If 
				%>
			</ul><!-- cart_list e -->
			<div class="order_form">
				<h3>결제정보</h3>
				<div class="cont">
					<dl class="row">
						<dt class="col">결제방식</dt>
						<dd class="col">
							<% If rs("PY_IN_CNT") > 0 And rs("POINT_IN_CNT") = 0 Then %>
								<%= rs("PY_IN_NM") %>
							<% End If %>
							<!--  포은트  결제-->
							<% If rs("PY_IN_CNT") = 0 And rs("POINT_IN_CNT") > 0 Then %>
								<%= rs("POINT_IN_NM") %>
							<% End If %>
							<!--  포은트  + 카드 OR 카드 결제-->
							<% If rs("PY_IN_CNT") > 0 And rs("POINT_IN_CNT") > 0 Then %>
								<%= rs("PY_IN_NM") %>/<%= rs("POINT_IN_NM") %>
							<% End If %>
						</dd>
					</dl>
					<dl class="row">
						<dt class="col">결제금액</dt>
						<dd class="c_red col">
							<% If rs("PY_IN_CNT") > 0 And rs("POINT_IN_CNT") = 0 Then %>
								<%=FormatNumber( rs("PY_IN_PAY"),0) %>원
							<% End If %>
							<!--  포은트  결제-->
							<% If rs("PY_IN_CNT") = 0 And rs("POINT_IN_CNT") > 0 Then %>
								<%= FormatNumber(rs("PY_IN_POINT"),0) %>원
							<% End If %>
							<!--  포은트  + 카드 OR 카드 결제-->
							<% If rs("PY_IN_CNT") > 0 And rs("POINT_IN_CNT") > 0 Then %>
								<%= FormatNumber(rs("PY_IN_PAY"),0) %>원/<%= FormatNumber(rs("PY_IN_POINT"),0) %>원
							<% End If %>
						</dd>
					</dl>
					<% If rs("PY_TP") = "01" Then %>
					<dl class="row">
						<dt class="col">영수증출력</dt>
						<dd class="c_red col">
							<input type="button" id="" class="" title="" value="출력하기" name="" onclick="card_view('<%=rs("PY_IN_ACNT")%>');" />
						</dd>
					</dl>
					<% End If %>
				</div>
				<h3>배송지정보</h3>
				<div class="cont">
					<dl class="row">
						<dt class="col">수령인명</dt>
						<dd class="col"><%=rs("or_in_pe2")%></dd>
					</dl>
					<dl class="row">
						<dt class="col">주소</dt>
						<dd class="col"><%=rs("or_in_zip")%> / <%=rs("or_in_addr")%></dd>
					</dl>
					<dl class="row">
						<dt class="col">휴대폰</dt>
						<dd class="col"><%=rs("or_in_tel")%></dd>
					</dl>
					<dl class="row">
						<dt class="col">배송메세지</dt>
						<dd class="col"><%=rs("or_in_memo")%></dd>
					</dl>
				</div>
			</div>
			<div class="btn_wrap" >

				<%
					CancleCount = 0
				  '주문상태가 미확정일때
					If rs("OR_STATE") = "00" Then

						'해당내역이 출고반품된적 있는지 확인
						CancleSQL = "SELECT COUNT(*) AS CancleCount"
						CancleSQL = CancleSQL & " FROM IC_T_ORDER_GD "
						CancleSQL = CancleSQL & " Where DEL_YN = 'N'"
						CancleSQL = CancleSQL & " AND CNCL_ORDER_GD_SEQ = '" & rs("SEQ") & "'"
						DBOpen()
						Set CancleRs = Dbcon.Execute(CancleSQL)
						DBClose()
						If CancleRs.Eof Or CancleRs.Bof Then
							CancleCount = 0
						Else
							CancleCount = CInt(CancleRs("CancleCount"))
						End If

						'출고반품 된적이 없고 AND (해당내역이 출고반품내역이 아니라면: 출고반품아닌내역은 0 이나 NUll값임)
						If Cint(CancleCount) > 0 Or rs("CNCL_ORDER_GD_SEQ") <> "0" Then
							'주문취소버튼 안보임
						Else
				%>
				<a href="javascript:orderCancle('<%=rs("OR_NUM")%>','<%=rs("Seq")%>');" class="btn btn_b">주문취소</a>
				<%		End If 
					End If
				%>



				<a href="gate.asp" class="btn btn_b btn_agree">뒤로가기</a>
				<br>
				<br>
				<br>
			</div><!-- btn_wrap e -->
		</div><!-- delivery_detail_wrap e -->
		</form>
	</div><!-- order_page e -->

</div><!-- container e -->
<%
	rs.Close
	Set rs = Nothing
%>
<script language="javascript" type="text/javascript">
//$(window).on('load resize',function(){ //브라우저 사이즈 변경될때
//	$('.order_form dt').each(function(){
//		var ddheight = $(this).height();
//		$(this).prev().css('height', ddheight);
//		
//	});
//});
</script>
<!-- #include virtual = "include/footer.asp" -->
