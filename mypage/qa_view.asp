<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	mode = request("mode")
	page = request("page")
	seq = request("seq")

	goParam = "?page=" & page

		if seq = "" then
			GoBack("데이터 번호가 존재하지 않습니다.")
			response.end
		end if

		DBopen()
		sql = "USP_PMAA_QNA_NOTICE_VIEW @MODE='USER_VIEW', @SEQ=" & seq
		'Response.write sql

		Set rs = DBcon.execute(sql)
		if rs.eof or rs.bof then
			GoBack("데이터가 존재하지 않습니다.")
			response.end
		else
			subject = rs(0)
			writer = rs(1)
			content = rs(2)
			re_content = rs(3)
			isview = rs(4)
			create_date = rs(5)
			hit = rs(6)
			
		end if
		DBclose()

		btn_msg = "수정"
%>
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">1:1문의</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page qa_view">
					<div class="sc_table">
						<table cellpadding="0" cellspacing="0" class="t_type_row" summary="">
							<thead>
								<tr>
									<th scope="col">문의제목<span class="c_blue">답변완료</span><span>답변예정</span></th>
								</tr>
							</thead>
							<tbody>
								<tr class="first">
									<td scope="col">
										<dl>
											<dt>번호</dt>
											<dd><%=seq%></dd>
											<dt>조회수</dt>
											<dd><%=hit%></dd>
										</dl>
										<span class="date"><%=create_date%></span>
									</td>
								</tr>
								<tr>
									<td scope="col" class="view_cont question">
										<div><%=content%></div>
									</td>
								</tr>
								<tr>
									<td scope="col" class="view_cont answer">
										<div>
											<dl>
												<dt>답변</dt>
												<dd><%=re_content%>
												</dd>
											</dl>
										</div>
									</td>
								</tr>
								<%
									DBopen()
										sql_top = "USP_PMAA_QNA_NOTICE_VIEW @MODE='CONTENT_TOP', @SEQ=" & seq

										Set rs_top = DBcon.execute(sql_top)
									DBclose()
								%>
								<!-- S: prev-list -->
								<% 
									If rs_top.Eof or rs_top.Bof Then
								%>
								<tr>
									<td class="pg_line">
										<a href="#">
											<dl>
												<dt>다음글</dt>
												<dd>다음글이 존재하지 않습니다.</a></dd>
											</dl>
											<span class="date"></span>
										</a>
									</td>
								</tr>
								<% Else %>
								<tr>
									<td class="pg_line">
										<a href="#">
											<dl>
												<dt>다음글</dt>
												<dd><a href="center_view.asp?seq=<%=rs_top("seq")%>"><%=rs_top("subject")%></a></dd>
											</dl>
											<span class="date"><%=rs_top("CREATE_DATE")%></span>
										</a>
									</td>
								</tr>
								<% End If %>

								<%
									DBopen()
										sql_bottom = "USP_PMAA_QNA_NOTICE_VIEW @MODE='CONTENT_BOTTOM', @SEQ=" & seq

										Set rs_bottom = DBcon.execute(sql_bottom)
									DBclose()
								%>
								<% 
									If rs_bottom.Eof or rs_bottom.Bof Then
								%>
								<tr>
									<td class="pg_line">
										<a href="#">
											<dl>
												<dt>이전글</dt>
												<dd>이전글이 존재하지 않습니다.</dd>
											</dl>
											<span class="date"></span>
										</a>
									</td>
								</tr>
								<% Else %>
								<tr>
									<td class="pg_line">
										<a href="#">
											<dl>
												<dt>이전글</dt>
												<dd><a href="center_view.asp?seq=<%=rs_bottom("seq")%>"><%=rs_bottom("subject")%></a></dd>
											</dl>
											<span class="date"><%=rs_bottom("CREATE_DATE")%></span>
										</a>
									</td>
								</tr>
								<% End If %>
							</tbody>
						</table>
						<div class="btn_wrap">
							<button type="button" class="btn02 btn02_cancel" onclick="javascript:location.href='qa_list.asp'">목록</button>
						</div>
					</div>
				</div>
			</div>
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->

<!-- #include virtual = "include/footer.asp" -->
