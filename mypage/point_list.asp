<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	SQL = "SELECT	WORK_DT,	"
	SQL = SQL&"		PO_MEMO,	"
	SQL = SQL&"		PO_MNY	"
	SQL = SQL&"	FROM IC_T_PMAA_POINT	"
	SQL = SQL&"	WHERE CUST_SEQ = '"&GetsCUSTSEQ()&"'	"
	SQL = SQL&"	AND DEL_YN = 'n'	"
	SQL = SQL&"	ORDER BY SEQ DESC 	"
	DBOpen()
		Set RS = Dbcon.Execute(SQL)
	DBClose()

%>
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">적립금 상세내역</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page">
					<div class="my_point">
						<dl>
							<dt>보유 포인트 현황</dt>
							<dd class="c_blue"><%=FormatNumber(Now_Point(GetsCUSTSEQ()),0)%>P</dd>
						</dl>
					</div>
					<div class="sc_table">
						<table cellpadding="0" cellspacing="0" class="t_type01 point_list" summary="">
							<colgroup>
								<col style="width:33.3%;"/>
								<col style="width:33.3%;"/>
								<col style="width:33.3%;"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="">적립 내용</th>
									<th scope="">적립포인트</th>
									<th scope="">일시</th>
								</tr>
							</thead>
							<tbody>
							<% Do Until RS.EOF  %>
								<tr class="first">
									<td><%=RS("PO_MEMO")%></td>
									<td><%=FormatNumber(RS("PO_MNY"),0)%></td>
									<td><%=RS("WORK_DT")%></td>
								</tr>
							<%
								RS.MoveNext
								
								Loop
								RS.Close
								Set RS = Nothing
							%>
							</tbody>
						</table>
						<!--div class="paging_wrap">
							<div class="paging">
								<a href="#" class="prev"></a>
								<a href="#">1</a>
								<a href="#">2</a>
								<a href="#">3</a>
								<a href="#">4</a>
								<a href="#">5</a>
								<a href="#" class="next"></a>
							</div>
						</div-->
						<!-- paging_wrap e -->
					</div><!-- paging e -->
				</div><!-- sc_table e -->
			</div><!-- sc_page e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->

<!-- #include virtual = "include/footer.asp" -->
