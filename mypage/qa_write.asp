<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<script language='javascript'>
	
	function write_ok()
	{
		var subject = document.getElementById("subject");
		var writer = document.getElementById("writer");
		var email = document.getElementById("email");
		var content = document.getElementById("content");

		if(subject.value.length ==0)
		{
			alert('제목을 입력해주시기 바랍니다');
			subject.focus();
			return;
		}
		if(writer.value.length ==0)
		{
			alert('작성자를 입력해주시기 바랍니다');
			writer.focus();
			return;
		}

		if(email.value.length ==0)
		{
			alert('이메일을 입력해주시기 바랍니다');
			email.focus();
			return;
		}
		if(content.value.length ==0)
		{
			alert('문의내용을 입력해주시기 바랍니다');
			content.focus();
			return;
		}
		
		document.bform.action="qa_write_ok.asp"
		document.bform.submit();		
	}
</script>
<form name="bform" method="post">
<input type="hidden" id="user_seq" name="user_seq" value="<%=GetsCUSTSEQ()%>">
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">1:1문의</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page">
					<div class="qa_write">
						<dl class="tit_cont01">
							<dt>
								<label for="">제목</label>
								<input type="text" id="subject" class="" title="" value="" name="subject"  maxlength="100"/><br/>
								<label for="">작성자</label>
								<input type="text" id="writer" class="" title="" value="<%=GetsCUSTNM()%>" <% If GetsCUSTNM() <> "" Then %> readonly <% End If %> name="writer" maxlength="25"/>
							</dt>
						</dl><!-- tit_cont e -->
						<dl class="tit_cont02">
							<dd>
								<label for="">이메일</label>
								<input type="text" id="email" class="" title="" value="<%=GetsCUSTEMAIL()%>" <% If GetsCUSTEMAIL() <> "" Then %> readonly <% End If %>  name="email" maxlength="100"/><br/>
								<label for="">작성일</label>
								<input type="text" id="create_date" class="" title="" value="<%=DATE%>" name="create_date" disabled />
							</dd>
						</dl><!-- tit_cont e -->
						<div class="write_cont">
							<textarea name="content"  id="content" title="" placeholder="상품/배송/결제/기타 궁금하신 사항을 알려주시면 신속하게 답변드립니다."></textarea>
						</div><!-- write_cont e -->
					</div><!-- qa_write e -->
					<div class="btn_wrap">
						<a href="/mypage/qa_list.asp" class="btn02 btn02_cancel">취소</a>
						<a href="javascript:write_ok();" class="btn02 btn02_agree">등록하기</a>
					</div><!-- btn_wrap e -->
				</div><!-- sc_page e -->
			</div><!-- list_wrap e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->
</font>
<!-- #include virtual = "include/footer.asp" -->
