<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
	page = request("page")
	
	search_key = request("search_key")
	search_keyword = request("search_keyword")

	if page = "" then page = 1

	Dim page_size, block_page, total_cnt, total_page, h_number

	page_size = 10
	block_page = 10	

	goParam = "?tmp=null"

	Dbopen()
	
	user_seq = GetsCUSTSEQ() 
	If user_seq = "" Then
		user_seq = 0
	End if
	
	sql = "USP_PMAA_USER_QNA_NOTICE_LIST @PAGE=" & page & ", @PAGE_SIZE=" & page_size & ", @USER_SEQ='" & user_seq & "'"

	'Response.write sql
	'Response.end
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numList = -1
	Else
		total_cnt = rs(0)
		total_page = rs(1)
			
		h_number = total_cnt - ((page -1) * page_size)
				
		arrList = rs.GetRows
		numList = Ubound(arrList,2)
	End If

	sql = "USP_PMAA_QNA_NOTICE_VIEW @mode='TOP'"
	Set rs = DBcon.Execute(sql)
	If rs.Eof or rs.Bof Then
		numTopList = -1
	Else
		arrTopList = rs.GetRows
		numTopList = Ubound(arrTopList,2)
	End If


	TotalPage=Int((CInt(numTopList)-1)/CInt(page_size)) +1
	
	'Response.write total_cnt

%>
<div class="container sub">
	<div class="list_page">
		<div class="top_wrap">
			<div class="list_wrap">
				<h3 class="page_tit">1:1문의</h3>
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap">
			<div class="list_wrap">
				<div class="sc_page">
					<!--div class="sch_box">
						<div class="select_box select02">
							<label for="sb">전체</label>
							<select id="sb" name="email_select">
								<option selected="selected">전체</option>
								<option value="">번호</option>
								<option value="">제목</option>
								<option value="">작성자</option>
								<option value="">작성일</option>
							</select>
						</div><!-- select_box e -->
						<!--input type="text" id="ip_sch"/>
						<input type="button" id="btn_sch" value="검색"/>
					</div--><!-- sch_box e -->
					<div class="sc_table">
						<table cellpadding="0" cellspacing="0" class="t_type01 qa_list" summary="">
							<colgroup>
								<col class="num"/>
								<col class="tit"/>
								<col class="yes"/>
								<col class="name hide_s"/>
								<col class="date hide_s"/>
								<col class="see hide_s"/>
							</colgroup>
							<thead>
								<tr>
									<th scope="" class="num">번호</th>
									<th scope="" class="tit">제목</th>
									<th scope="">답변여부</th>
									<th scope="" class="hide_s">작성자</th>
									<th scope="" class="hide_s">작성일</th>
									<th scope="" class="hide_s">조회</th>
								</tr>
							</thead>
							<tbody>
								<% If numList < 0 Then %>
									문의내용이 없습니다.
								<% End If %>
								<%
									for i = (page-1) * page_size To numList
									  seq			= arrList(2,i)
									  subject		= arrList(3,i)
									  writer		= arrList(4,i)
									  content		= arrList(5,i)
									  re_content	= arrList(6,i)
									  filename		= arrList(7,i)
									  isview		= arrList(8,i)
									  hit			= arrList(9,i)
									  create_date	= arrList(10,i)
								%>
								<tr class="first">
									<td class="num"><%=seq%></td>
									<td class="tit"><a href="qa_view.asp?seq=<%=seq%>"><%=subject%></a></td>
									<td>
										<% If re_content <> "" Then %>
											<span class="end">
											답변완료
											</span>
										<% Else %>
											<span class="ing">
											답변예정
											</span>	
										<% End If %>
									</td>
									<td class="hide_s"><%=writer%></td>
									<td><%=create_date%></td>
									<td class="hide_s"><%=hit%></td>
								</tr>
								<%
									h_number = h_number - 1
									next
								%>
							</tbody>
						</table>
						<div class="btn_wrap" style="text-align:right;margin-top:10px;">
							<% If Request.Cookies("sCUSTSEQ") <> "" Then %>
								<button type="button" class="btn02 btn02_agree" onclick="javascript:location.href='qa_write.asp'">문의하기</button>
							<% Else %>
								<button type="button" class="btn02 btn02_agree" onclick="javascript:alert('개인회원 가입하신 후 이용 가능합니다');">문의하기</button>
							<% End If %>
						</div><!-- btn_wrap e -->
						<%=PT_qna_Ajax_PageLink(page,page_size,"paging")%>
						<!-- paging_wrap e -->
					</div><!-- paging e -->
				</div><!-- sc_table e -->
			</div><!-- sc_page e -->
		</div><!-- bottom_wrap e -->
	</div><!-- list_page e -->

<!-- #include virtual = "include/footer.asp" -->
