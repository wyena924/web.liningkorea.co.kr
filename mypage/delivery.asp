<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
			
	NowPage			= Request("NowPage")
	ViewCnt			= Request("ViewCnt")	
	Order_type		= Request("Order_type")
	Search_key		= Request("Search_key")
	Search_keyWord	= Request("Search_keyWord")
	location_yn		= Request("location_yn")
	user_seq		= GetsCUSTSEQ()

	'Response.write user_seq
	If NowPage = "" Then
		NowPage = "1"
	End If 

			
	If ViewCnt = "" Then 
		ViewCnt = "10"
	End If 	


	If user_seq = "" Then 

		user_seq = "1111"
	End If 
	
	'정렬순서가 존재하지 않으면 최신 등록순으로 
	If Order_type = ""  Then 
		Order_type = "date_desc" 
	End If 

	strqry4 = "	sp_mall_delivery_list "
	strqry4 = strqry4 & "	 @PAGE = '"&NowPage&"'"
	strqry4 = strqry4 & "	,@PAGESIZE='"&ViewCnt&"'"
	strqry4 = strqry4 & "	,@ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Search_key='"&Search_key&"' "
	strqry4 = strqry4 & "	,@Search_keyword='"&Search_keyWord&"' "
	strqry4 = strqry4 & "	,@user_seq='"&user_seq&"' "
	strqry4 = strqry4 & "	,@OR_WRITE_ID='"&GLOBAL_VAR_PEID&"' "
	
	'Response.write strqry4 &"<br>"
	DBOpen()
		Set rs = Dbcon.Execute(strqry4)
	DBClose()

	strqry4 = "	sp_mall_delivery_cnt "
	strqry4 = strqry4 & "	 @ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' "
	strqry4 = strqry4 & "	,@Search_key='"&Search_key&"' "
	strqry4 = strqry4 & "	,@Search_keyword='"&Search_keyWord&"' "
	strqry4 = strqry4 & "	,@user_seq='"&user_seq&"' "
	strqry4 = strqry4 & "	,@OR_WRITE_ID='"&GLOBAL_VAR_PEID&"' "
	'Response.write strqry4
	'Response.End 
	DBOpen()
		Set rs_cnt = Dbcon.Execute(strqry4)
	DBClose()

	CountRs = rs_cnt(0)
	
	TotalPage=Int((CInt(CountRs)-1)/CInt(ViewCnt)) +1

	'Response.write TotalPage
If cust_tp_rs(User_SEQ) = "N" Then
	Response.write("<script language='javascript'>alert('로그인이 필요한 서비스 입니다.'); location.href='/login.asp?location_yn="&location_yn&"' </script>")
End If 
%>
<script language='javascript'>

	setTimeout(function(){ order_search(1,10,'paging') }, 3000);
 

	function order_search(NowPage,ViewCnt,paging_gb)
		{
			var search_key = document.getElementById("sch"); //서치키 
			var search_keyword = document.getElementById("ip_sch"); //서치키워드
			
			if (paging_gb !='paging')
			{
				if (search_key.value=='')
				{
					alert('주문조건을 화인해주시기 바랍니다');
					search_key.focus();
					return;
				}
				if (search_keyword.value=='')
				{
					alert('검색어를 입력해주시기 바랍니다.');
					search_keyword.focus();
					return;
				}
			}
			//alert(search_key.value);
			//alert(search_keyword.value);
			var strAjaxUrl="/ajax/delivery_ajax_list.asp"
			var retDATA="";
			//alert(strAjaxUrl);
			$.ajax({
				 type: 'POST',
				 url: strAjaxUrl,
				 dataType: 'html',
				 data: {
				    NowPage:NowPage,
					ViewCnt:ViewCnt,
					search_key:search_key.value,
					search_keyword:search_keyword.value
				},						  
				success: function(retDATA) {
					if(retDATA)
						{

							if(retDATA!="")
							{
								document.getElementById("delivery_ajax_list").innerHTML= retDATA;
							}
						}
				 }
			}); //close $.ajax(
		}

	function order_ok(gd_memo,gubun)
	{
		if (gubun=='2')
		{
			alert('휴대폰 가입창으로 이동합니다.');
			document.Orderfrm.target="_blank"
			document.Orderfrm.action = gd_memo;
			document.Orderfrm.submit();
			
		}
		else
		{

			document.Orderfrm.target="_top"
			document.Orderfrm.action = gd_memo;
			document.Orderfrm.submit();
		}
		
	}
</script>
<form method="post" name="Orderfrm" id="Orderfrm"  action="" target="popwin">
</form>
<div class="container sub">
	<div class="location_wrap">
		<span><a href="#">HOME</a></span><i></i>
		<span>주문/배송조회</span>
	</div><!-- location_wrap e -->
	<div class="list_page delivery_page">
		<form method="post" action="">
		<div class="top_wrap">
			<div class="list_wrap">
				<div class="sch_wrap">
					<div class="select_box">
						<label for="sch">검색조건</label>
						<select id="sch" name="search_key" >
							<option value="" selected="selected">검색조건</option>
							<option value="or_tel" >연락처</option>
							<option value="or_num" >주문번호</option>
						</select>
					</div><!-- select_box e -->
					<div class="sch_cont">
						<label for="ip_sch" class="hide">검색창</label>
						<input type="text" id="ip_sch" name="search_keyword" placeholder="주문데이터를 검색해주세요"/>
					</div><!-- sch_cont e -->
					<div class="ip_btn_wrap" style="float:center;">
						<input type="button" id="" class="ip_btn" title="" value="주문조회" name="" onclick="order_search()"/>
					</div>
				</div><!-- sch_wrap e -->
				<div class="sch_info">
					<p class="block_m">· 조회 된 목록을 클릭하시면 상세조회를 하실 수 있습니다.</p>
					<p>· 주문 취소는 [주문 완료] 상태에서만 가능합니다.</p>
					<p>· 그 외에 주문취소를 원하실 경우 고객센터로 문의 바랍니다.</p>
				</div><!-- sch_info e -->
			</div><!-- list_wrap e -->
		</div><!-- top_wrap e -->
		<div class="bottom_wrap" ID="delivery_ajax_list">>
		</div><!-- bottom_wrap e -->
		</form>
	</div><!-- list_page e -->
	</form>
<!-- #include virtual = "include/footer.asp" -->
