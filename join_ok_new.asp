<!--#include virtual="/Library/config.asp"-->
<%
	Dim GRP1,CUST_TP,ORDER_NM,CM_NUM,CM_NUM_SUB,CM_JONG,CM_UPTAE,CM_PE,CM_TEL1,CM_TEL2,CM_TEL3,CM_TEL,CM_FAX1,CM_FAX2,CM_FAX3
	Dim CM_FAX,CM_EMAIL,CM_ZIP,CM_ZIP2,CM_ADDR,ONLINE_ID,ONLINE_PWD,RE_NM,PE_DEPT,RE_TEL1,RE_TEL2,RE_TEL3,RE_HAND_TEL1	
	Dim RE_HAND_TEL2,RE_HAND_TEL3,RE_FAX1,RE_FAX2,RE_FAX3,RE_EMAIL,RE_ACC_EMAIL,RE_PE,SENT_TEL1,SENT_TEL2,SENT_TEL3,RE_ZIP1,RE_ZIP2,RE_ADDR			
	Dim req_curPage,cust_cm_hp1,cust_cm_hp2,cust_cm_hp3,CM_ADDR2,gate_gb,licensee_nm
	dim grp2_select	
	dim grp3_select		
	dim grp4_select
	dim grp5_select

	GRP1				=	request("GRP1")						'고객분류코드(NUMERIC:18,0) *필수                   
	CUST_TP				=   "1"									'고객구분('1':개인,'2':법인,'3':개인사업자)(VARCHAR:1) *필수	                          
	ORDER_NM			=	request("cust_cust_custnm")			'고객명(VARCHAR:100) *필수                         
	CM_NUM				=	request("cm_num")					'사업자번호(VARCHAR:20)                             
	CM_NUM_SUB			=	""									'사업자종번호(VARCHAR:20)                    
	CM_JONG				=	request("cm_jong")					'종목(VARCHAR:100)               
	CM_UPTAE			=	request("cm_uptae")					'업태(VARCHAR:100)                          
	CM_PE				=	request("cm_pe")					'대표자명(VARCHAR:50)             
	CM_TEL1				=	request("CM_TEL1")					
	CM_TEL2				=	request("CM_TEL2")
	CM_TEL3				=	request("CM_TEL3")
	CM_FAX1				=   request("CM_FAX1")     
	CM_FAX2				=   request("CM_FAX2")
	CM_FAX3				=   request("CM_FAX3")	
	CM_FAX				=	CM_FAX1&"-"&CM_FAX2&"-"&CM_FAX3     '팩스번호(VARCHAR:20) *필수
	CM_EMAIL			=   request("ONLINE_ID")	  '이메일(VARCHAR:50) *필수
	CM_ZIP				=   request("cust_zip")					'우편번호1(VARCHAR:3) *필수
	CM_ADDR				=   request("cust_addr")				'주소(VARCHAR:200) *필수
	CM_ADDR2			=   request("cust_addr2")				'주소(VARCHAR:200) *필수
	ONLINE_ID			=	request("ONLINE_ID")				'온라인아이디 *필수                 
	ONLINE_PWD			=	request("cust_online_pwd")   	    '온라인비밀번호 *필수		            
	RE_NM				=	request("RE_NM")   					'담당자명 
	RE_DEPT				=	request("RE_DEPT")   				'소속부서명(VARCHAR:100)
	RE_TEL1				=	request("RE_TEL1")   				'전화번호지역번호(VARCHAR:4) 
	RE_TEL2				=   request("RE_TEL2")					'전화번호국번(VARCHAR:4) *필수
	RE_TEL3				=   request("RE_TEL3")					'전화번호(VARCHAR:4) *필수
	RE_HAND_TEL1		=   request("RE_HAND_TEL1")				'휴대폰앞자리(VARCHAR:4) *필수
	RE_HAND_TEL2		=   request("RE_HAND_TEL2")				'휴대폰국번(VARCHAR:4) *필수
	RE_HAND_TEL3		=   request("RE_HAND_TEL3")				'휴대폰번호(VARCHAR:4) *필수
	RE_FAX1				=   request("RE_FAX1")					'팩스지역번호(VARCHAR:4)
	RE_FAX2				=   request("RE_FAX2")					'팩스국번(VARCHAR:4)
	RE_FAX3				=   request("RE_FAX3")					'팩스국번(VARCHAR:4)
	RE_EMAIL			=   request("RE_EMAIL")					'담당자이메일주소(VARCHAR:100) *필수
	RE_ACC_EMAIL		=   request("RE_ACC_EMAIL")				'회계담당자이메일주소(VARCHAR:100) *필수							
	RE_PE				=   request("RE_PE")					'수령인명(VARCHAR:50) *필수
	SENT_TEL1			=   request("SENT_TEL1")				'수령인전화번호앞자리(VARCHAR:3) 
	SENT_TEL2			=   request("SENT_TEL2")				'수령인전화번호국번(VARCHAR:3) *필수
	SENT_TEL3			=   request("SENT_TEL3")				'수령인전화번호(VARCHAR:3) *필수
	RE_ZIP1				=   request("RE_ZIP1")					'수령인우편번호앞3자리(VARCHAR:3) *필수
	RE_ZIP2				=   request("RE_ZIP2")					'수령인우편번호뒤3자리(VARCHAR:3) *필수
	RE_ADDR				=   request("RE_ADDR")					'수령인상세주소(VARCHAR:200) *필수
	cust_cm_hp1			=   request("cust_cm_hp1")				'핸드폰번호1
	cust_cm_hp2			=   request("cust_cm_hp2")				'핸드폰번호1
	cust_cm_hp3			=   request("cust_cm_hp3")	
	gate_gb				=   request("gate_gb")
	licensee_nm			=   request("licensee_nm")
	grp2_select			=	request("grp2_select2")
	grp3_select			=	request("grp3_select2")
	grp4_select			=	request("grp4_select2")
	grp5_select			=	request("grp5_select2")
	cust_checkboxcpy	=	request("cust_checkboxcpy")
	cust_checkboxcpy2	=	request("cust_checkboxcpy2")
	cust_checkboxcpy3	=	request("cust_checkboxcpy3")	
	cust_checkboxcpy4	=	request("cust_checkboxcpy4")	
	'핸드폰번호1
	CM_HP				=   cust_cm_hp1&"-"&cust_cm_hp2&"-"&cust_cm_hp3		'핸드폰번호 합처짐
	CM_TEL				=   CM_TEL1&"-"&CM_TEL2&"-"&CM_TEL3		'핸드폰번호 합처짐
	saveid = ""
	Online_Pw = ""
	memo			=	request("company_name")				'메모를 사원번호로 활용  -> 메모를 회사명으로 표시
	get_id			=	request("get_id")


	If ONLINE_ID = "" Then 
		Response.Write "<script language='javascript'>alert('필요한 정보가 없습니다. 다시 확인해 주시기 바랍니다.'); </script>"
		Response.End
	End If 

	If cust_checkboxcpy = "" Then 
		cust_checkboxcpy = "N"
	End If 

	If cust_checkboxcpy2 = "" Then 
		cust_checkboxcpy2 = "N"
	End If 

	If cust_checkboxcpy3 = "" Then 
		cust_checkboxcpy3 = "N"
	End If 
	
	CM_ADDR = CM_ADDR&" "&CM_ADDR2
	
	CM_ZIP = Left(request("cust_zip"),3)
	CM_ZIP2 = mid(request("cust_zip"),4,3)

	'response.write "GRP1		:"&GRP1			&"<br>"  
	'response.write "CUST_TP		:"&CUST_TP		&"<br>"    
	'response.write "ORDER_NM	:"&ORDER_NM	  &"<br>"  
	'response.write "CM_NUM		:"&CM_NUM		&"<br>"    
	'response.write "CM_NUM_SUB	:"&CM_NUM_SUB	&"<br>"    
	'response.write "CM_JONG		:"&CM_JONG		 &"<br>"   
	'response.write "CM_UPTAE	:"&CM_UPTAE	  &"<br>"  
	'response.write "CM_PE		:"&CM_PE		 &"<br>"   
	'response.write "CM_TEL1		:"&CM_TEL1		  &"<br>"  
	'response.write "CM_TEL2		:"&CM_TEL2		  &"<br>"  
	'response.write "CM_TEL3		:"&CM_TEL3		  &"<br>"  
	'response.write "CM_TEL		:"&CM_TEL		  &"<br>"  
	'response.write "CM_FAX1		:"&CM_FAX1		  &"<br>"  
	'response.write "CM_FAX2		:"&CM_FAX2		  &"<br>"  
	'response.write "CM_FAX3		:"&CM_FAX3		  &"<br>"  
	'response.write "CM_FAX		:"&CM_FAX		  &"<br>"  
	'response.write "CM_EMAIL	:"&CM_EMAIL	  &"<br>"  
	'response.write "CM_ZIP		:"&CM_ZIP		 &"<br>"   
	'response.write "CM_ZIP2		:"&CM_ZIP2		  &"<br>"  
	'response.write "CM_ADDR		:"&CM_ADDR		  &"<br>"  
	'response.write "ONLINE_ID	:"&ONLINE_ID	  &"<br>"  
	'response.write "ONLINE_PWD	:"&ONLINE_PWD	  &"<br>"  
	'response.write "RE_NM		:"&RE_NM		  &"<br>"  
	'response.write "RE_DEPT		:"&RE_DEPT		  &"<br>"  
	'response.write "RE_TEL1		:"&RE_TEL1		  &"<br>"  
	'response.write "RE_TEL2		:"&RE_TEL2		  &"<br>"  
	'response.write "RE_TEL3		:"&RE_TEL3		  &"<br>"  
	'response.write "RE_HAND_TEL1:"&RE_HAND_TEL1  &"<br>"  
	'response.write "RE_HAND_TEL2:"&RE_HAND_TEL2  &"<br>"  
	'response.write "RE_HAND_TEL3:"&RE_HAND_TEL3  &"<br>"  
	'response.write "RE_FAX1		:"&RE_FAX1		  &"<br>"  
	'response.write "RE_FAX2		:"&RE_FAX2		  &"<br>"  
	'response.write "RE_FAX3		:"&RE_FAX3		  &"<br>"  
	'response.write "RE_EMAIL	:"&RE_EMAIL	  &"<br>"  
	'response.write "RE_ACC_EMAIL:"&RE_ACC_EMAIL  &"<br>"  
	'response.write "RE_PE		:"&RE_PE		  &"<br>"  
	'response.write "SENT_TEL1 	:"&SENT_TEL1 	  &"<br>"  
	'response.write "SENT_TEL2	:"&SENT_TEL2	  &"<br>"  
	'response.write "SENT_TEL3 	:"&SENT_TEL3 	  &"<br>"  
	'response.write "RE_ZIP1 	:"&RE_ZIP1 	  &"<br>"  
	'response.write "RE_ZIP2		:"&RE_ZIP2		  &"<br>"  
	'response.write "RE_ADDR		:"&RE_ADDR		  &"<br>"  
											  
	'RESPONSE.End
	
	Dim strqry,strqry2 , rs,re_dept,CM_HP
	
	

	strqry =  "SELECT COUNT(*) cnt"
	strqry = strqry & " FROM itemcenter.DBO.IC_T_ORDER_CUST "
	strqry = strqry & " WHERE DEL_YN = 'N'   "
	strqry = strqry & " AND GRP1 = '77'    "
	strqry = strqry & " AND GRP2 = '113'    "
	strqry = strqry & " AND ONLINE_ID = '" & ONLINE_ID & "'"
	
	DBOpen()	

	Set rs = DBcon.Execute(strqry)
	
	If rs("cnt") = 0 Then 
		
		strqry2 =  "  Exec itemcenter.DBO.IC_PROC_CUST_WR_BAEMIN "
		strqry2 = strqry2 & "      @GRP1 = '77' "  
		strqry2 = strqry2 & "     ,@GRP2 = '113' "  
		strqry2 = strqry2 & "     ,@CUST_TP ='"& CUST_TP &"' "  
		strqry2 = strqry2 & "     ,@ORDER_NM ='"& ORDER_NM &"' "  
		strqry2 = strqry2 & "     ,@CM_NUM  ='"& CM_NUM &"' "  
		strqry2 = strqry2 & "     ,@CM_NUM_SUB ='"& CM_NUM_SUB &"' "  
		strqry2 = strqry2 & "     ,@CM_JONG ='"& CM_JONG &"' "  
		strqry2 = strqry2 & "     ,@CM_UPTAE ='"& CM_UPTAE &"' "  
		strqry2 = strqry2 & "     ,@CM_PE ='"& CM_PE &"' "  
		strqry2 = strqry2 & "     ,@CM_TEL ='"& CM_TEL &"' "  
		strqry2 = strqry2 & "     ,@CM_HP  ='"& CM_HP &"' "  
		strqry2 = strqry2 & "     ,@CM_FAX ='"& CM_FAX &"' "  
		strqry2 = strqry2 & "     ,@CM_EMAIL ='"& CM_EMAIL &"' "  
		strqry2 = strqry2 & "     ,@CM_ZIP ='"& CM_ZIP &"' "  
		strqry2 = strqry2 & "     ,@CM_ZIP2  ='"& CM_ZIP2 &"' "  
		strqry2 = strqry2 & "     ,@CM_ADDR ='"& CM_ADDR &"' "  
		strqry2 = strqry2 & "     ,@RE_RTN_YN = 'N' "  
		strqry2 = strqry2 & "     ,@GL_PE = 'mall_b2bc' "  
		strqry2 = strqry2 & "     ,@IN_TP   ='삼성전자온라인b2b몰' " 			
		strqry2 = strqry2 & "     ,@IN_OK = 'Y'  "
		strqry2 = strqry2 & "     ,@ONLINE_ID  ='"& ONLINE_ID &"' "  
		strqry2 = strqry2 & "     ,@ONLINE_PWD  = '"& ONLINE_PWD &"' "  
		strqry2 = strqry2 & "     ,@WORK_NM = ''	"
		strqry2 = strqry2 & "     ,@RE_NM   = '"& RE_NM &"' "  
		strqry2 = strqry2 & "     ,@RE_DEPT = '"& RE_DEPT &"' "  
		strqry2 = strqry2 & "     ,@RE_TEL1 = '"& RE_TEL1 &"' "  
		strqry2 = strqry2 & "     ,@RE_TEL2  = '"& RE_TEL2 &"' "  
		strqry2 = strqry2 & "     ,@RE_TEL3= '"& RE_TEL3 &"' "  
		strqry2 = strqry2 & "     ,@RE_HAND_TEL1 = '"& RE_HAND_TEL1 &"' "  
		strqry2 = strqry2 & "     ,@RE_HAND_TEL2 = '"& RE_HAND_TEL2 &"' "  
		strqry2 = strqry2 & "     ,@RE_HAND_TEL3 = '"& RE_HAND_TEL3 &"' "  
		strqry2 = strqry2 & "     ,@RE_FAX1	   = '"& RE_FAX1 &"' "  
		strqry2 = strqry2 & "     ,@RE_FAX2	   = '"& RE_FAX2 &"' "  
		strqry2 = strqry2 & "     ,@RE_FAX3	   = '"& RE_FAX3 &"' "  
		strqry2 = strqry2 & "     ,@RE_EMAIL	   = '"& CM_EMAIL &"' "  
		strqry2 = strqry2 & "     ,@RE_ACC_EMAIL = '"& CM_EMAIL &"' "  
		strqry2 = strqry2 & "     ,@RE_EVENT_YN = 'Y' "
		strqry2 = strqry2 & "     ,@RE_EMAIL_YN = 'Y' "
		strqry2 = strqry2 & "     ,@RE_EMAIL_YN_DT = '' "
		strqry2 = strqry2 & "     ,@RE_BIRTHDAY = '' 	"					
		strqry2 = strqry2 & "	  ,@RE_PE = '"& RE_PE &"' "  
		strqry2 = strqry2 & "	  ,@SENT_TEL1 = '"& SENT_TEL1 &"' "  
		strqry2 = strqry2 & "	  ,@SENT_TEL2 = '"& SENT_TEL2 &"' "  
		strqry2 = strqry2 & "	  ,@SENT_TEL3 = '"& SENT_TEL3 &"' "  
		strqry2 = strqry2 & "	  ,@RE_ZIP1 = '"& RE_ZIP1 &"' "  
		strqry2 = strqry2 & "	  ,@RE_ZIP2 = '"& RE_ZIP2 &"' "  
		strqry2 = strqry2 & "	  ,@RE_ADDR = '"& RE_ADDR &"' "  
		strqry2 = strqry2 & "	  ,@UNI_GRP_CD = '"&GLOBAL_VAR_UNIGRPCD&"'"	
		strqry2 = strqry2 & "	  ,@licensee_nm = '"&licensee_nm&"'"	
		strqry2 = strqry2 & "	  ,@MEMO = '"&memo&"'"	
		strqry2 = strqry2 & "	  ,@ch1 = '"&cust_checkboxcpy&"'"	
		strqry2 = strqry2 & "	  ,@ch2 = '"&cust_checkboxcpy2&"'"	
		strqry2 = strqry2 & "	  ,@ch3 = '"&cust_checkboxcpy3&"'"	
		strqry2 = strqry2 & "	  ,@ch4 = '"&cust_checkboxcpy4&"'"	
		strqry2 = strqry2 & "	  ,@get_id = '"&get_id&"'"	


		Dim RS2 
		Set rs2 =  DBcon.Execute(strqry2)
	
		LoginSQL = "SELECT ONLINE_ID,GL_PE,ORDER_SEQ, ORDER_NM, ISNULL(CM_ZIP,'') AS CM_ZIP, ISNULL(CM_ZIP2,'') AS CM_ZIP2, ISNULL(CM_ADDR,'') AS CM_ADDR, ISNULL(CM_TEL,'') AS CM_TEL, ISNULL(CM_EMAIL,'') AS CM_EMAIL, ISNULL(CM_HP,'') AS CM_HP, GRP1, GRP2, GRP3, GRP4, GRP5, GRP6, Update_GuBun FROM IC_T_ORDER_CUST "
		LoginSQL = LoginSQL&" WHERE DEL_YN='N' AND ISNULL(IN_OK,'Y') = 'Y'"
		LoginSQL = LoginSQL&" AND ONLINE_ID = '"&ONLINE_ID&"'"
		
		LoginSQL = LoginSQL&" AND GL_PE = 'mall_b2bc'"
		
		'Response.write LoginSQL
		DBopen()
		Set LoginRs = dbcon.Execute(LoginSQL)
		

		If Not (LoginRs.Eof Or LoginRs.Bof) Then 

			'//===================================================================================================
			'//====================================   로그인 로그 체크 ================================================
			'//===================================================================================================

			Function Login_Log(inDate,inTime,inMinute,Mall_Code,Cust_Seq,Cust_Grp1,Cust_Grp2,Cust_Grp3,Cust_Grp4,Cust_Grp5,Cust_Grp6)
				'inDate,inTime,inMinute,Mall_Code,Cust_Seq,Cust_Grp1,Cust_Grp2,Cust_Grp3,Cust_Grp4,Cust_Grp5,Cust_Grp6,Cust_Referer,Cust_Ip,Cust_Agent,Cust_Type
					 Cust_Type = "W"
					 InSQL = "Insert Into IC_T_Online_Login_Log "
					 InSQL = InSQL&"(inDate,inTime,inMinute,mall_code,cust_seq,cust_grp1,cust_grp2,cust_grp3,cust_grp4,cust_grp5,cust_grp6,cust_referer,cust_ip,cust_agent,cust_type) Values"
					 InSQL = InSQL&"('"&inDate&"','"&inTime&"','"&inMinute&"','"&mall_code&"','"&cust_seq&"','"&cust_grp1&"','"&cust_grp2&"','"&cust_grp3&"','"&cust_grp4&"','"&cust_grp5&"','"&cust_grp6&"','"&Request.ServerVariables ("HTTP_REFERER")&"','"&Request.ServerVariables("REMOTE_ADDR")&"','"&Request.ServerVariables("HTTP_USER_AGENT")&"','W')"
					 Dbcon.Execute(InSQL)
			End Function 
			inDate   = Year(now)&AddZero(Month(now))&AddZero(Day(now))'주문일자 예)20150609			
			inTime   = AddZero(Hour(now))
			inMinute = AddZero(Minute(now))
			Mall_Code = GLOBAL_VAR_PEID
			Cust_Seq  = LoginRs("ORDER_SEQ")
			Cust_Grp1 = LoginRs("GRP1")
			Cust_Grp2 = LoginRs("GRP2")
			Cust_Grp3 = LoginRs("GRP3")
			Cust_Grp4 = LoginRs("GRP4")
			Cust_Grp5 = LoginRs("GRP5")
			Cust_Grp6 = LoginRs("GRP6")
			Login_Log inDate,inTime,inMinute,Mall_Code,Cust_Seq,Cust_Grp1,Cust_Grp2,Cust_Grp3,Cust_Grp4,Cust_Grp5,Cust_Grp6
			'//===================================================================================================
			'//====================================   로그인 로그 체크 ================================================
			'//===================================================================================================


			'로그인 로그 생성=======================================================================================================================
			LogSQL = "INSERT INTO tblLoginLog "
			LogSQL = LogSQL&"(UserID, UserGubun, Referer, Agent, UserIp, WriteDate, ONLINECD, GRP1, GRP2, GRP3, GRP4, GRP5, GRP6)"
			LogSQL = LogSQL&"	VALUES "
			LogSQL = LogSQL&"('"&online_id&"','"&LoginRs("GRP1")&"','"&Request.ServerVariables("HTTP_REFERER")&"','"&Request.ServerVariables("HTTP_USER_AGENT")&"','"&Request.ServerVariables("REMOTE_HOST")&"',Getdate(),'"&GLOBAL_VAR_ONLINECD&"','"&LoginRs("GRP1")&"','"&LoginRs("GRP2")&"','"&LoginRs("GRP3")&"','"&LoginRs("GRP4")&"','"&LoginRs("GRP5")&"','"&LoginRs("GRP6")&"')"
			'Response.Write LogSQL
			'Response.End
			Dbcon.Execute(LogSQL)				
			'로그인 로그 생성=======================================================================================================================

			'세션 셋팅
			'SetSession online_id,LoginRs("ORDER_SEQ"),LoginRs("GL_PE"),LoginRs("ORDER_NM"),LoginRs("CM_ZIP"),LoginRs("CM_ZIP2"),LoginRs("CM_ADDR"),LoginRs("CM_TEL"),LoginRs("CM_EMAIL"),LoginRs("CM_HP"),saveid, Online_Pw


			'response.write ("<script langauge='javascript'>alert('회원 등록에 성공했습니다'); location.href='/'</script>")
			response.write ("<script langauge='javascript'>alert('사전가입해주셔서 감사합니다 행사시작은 1월 7일부터 진행예정입니다.'); location.href='/gate.asp'</script>")
			
		End if
	ElseIf rs("cnt") > 0 Then 
		response.write ("<script langauge='javascript'>alert('이미 등록된 회원입니다. 다시 확인해 주시기 바랍니다.'); location.href='/'</script>")
		response.end
	Else	
		response.write ("<script langauge='javascript'>alert('회원 등록에 실패 했습니다.'); location.href='/'</script>")
		response.end
	End If	
	
		rs.close
		set rs = nothing  	
		rs2.close
		set rs2 = nothing  
 
		DBClose() 


%>