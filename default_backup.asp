<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<style type="text/css">

.popup_wrap .end_box{left:50%;}
.popup_wrap .end_box span{color:#027cc5;}
.popup_wrap .end_box p{text-align:center;padding:50px 0 0;line-height:1.5;}
.popup_wrap .end_box p.end_box_info{padding-top:20px;line-height:1.3;font-size:13px;color:#555;}

.popup_cont{position:absolute;top:127px;left:50%;transform:translateX(-50%);z-index:1;}
.popup_cont > a{display:block;}
.popup_cont img{width:100%;min-width:300px;max-width:700px;}
.popup_cont .btn_close{background:#333;padding:10px;text-align:right;}
.popup_cont .btn_close a{color:#fff;margin-left:10px;}


/*320(브라우저 가로폭)*/
@media screen and (max-width: 320px){
#popup01{width:87%;margin-left:10%;}
#popup01 .btn_close{width:100%;}
}

</style>
<script language="javascript">
	
	
	function default_ajax(start_price, end_price)
	{
		//alert(ViewCnt);
		var strAjaxUrl="/ajax/default_ajax_list.asp"
		var retDATA="";
		//alert(strAjaxUrl);
		
		 $.ajax({
			 type: 'POST',
			 url: strAjaxUrl,
			 dataType: 'html',
			 data: {
				start_price:start_price,
				end_price:end_price
			},						  
			success: function(retDATA) {
				if(retDATA)
					{
						if(retDATA!="")
						{
							document.getElementById("default_ajax_list").innerHTML= retDATA;
						}
					}
			 }
		 }); //close $.ajax(
	}


</script>
<div class="container main">
	<div class="top_wrap">
		<ul id="bxslider" class="list_wrap visual_wrap">
			<li class="slide">		
 				<a href="javascript:void(0)">
					<img src="/front/img/main_banner_20190821_w_2.jpg" alt="" class="bn_main_w"/>
					<img src="/front/img/main_banner_20190821_m2.jpg" alt="" class="bn_main_m"/>
				</a>
			</li>
		</ul>		

		<div class="main_tab_banner list_wrap">
			<div class="helpper">
				<div class="tab_wrap">
					<div class="tab">
						<a href="javascript:void(0); default_ajax('best','');" rel="tab01" class="active">판매베스트상품</a>
						<a href="javascript:void(0); default_ajax('5000000','6999999');" rel="tab02">500만원대 패키지추천</a>
						<a href="javascript:void(0); default_ajax('7000000','9999999');" rel="tab03">700만원대 패키지추천</a>
						<a href="javascript:void(0); default_ajax('10000000','99999999');" rel="tab04">1,000만원대 패키지추천</a>
					</div>
				</div>
			</div>
			<div id="tab01" class="tab_cont detail_cont">
				<!--a href="#">
					<img src="/front/img/500_banner.jpg" alt="">
				</a-->
			</div>
			<div id="tab02" class="tab_cont detail_cont">
				<img src="/front/img/500_banner.jpg" alt="">
				<div class="link_tab02">
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392954"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392710"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=366284"></a>
				</div>
			</div>
			<div id="tab03" class="tab_cont detail_cont">
				<img src="http://b2bc.samsungbizmall.com/front/img/700_banner.jpg">
				<div class="link_tab03">
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392955"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392706"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=366284"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=366322"></a>
				</div>
			</div>
			<div id="tab04" class="tab_cont detail_cont">
				<img src="http://b2bc.samsungbizmall.com/front/img/1000_banner.jpg">
				<div class="link_tab04">
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392955"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392706"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392955"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=366322"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=382990"></a>
					<a href="http://b2bc.samsungbizmall.com/product/detail2.asp?seq=392271"></a>
				</div>
			</div>
			<style type="text/css">
				.main_tab_banner .tab_cont{position:relative;padding-top:0;overflow:hidden;}
				.link_tab02 a{position:absolute;bottom:15%;width:20%;height:58%;}
				.link_tab02 a:nth-child(1){left:2.5%;}
				.link_tab02 a:nth-child(2){left:24.5%;}
				.link_tab02 a:nth-child(3){left:46.5%;}

				.link_tab03 a{position:absolute;bottom:14%;width:18%;height:40%;}
				.link_tab03 a:nth-child(1){bottom:56%;left:50.5%;}
				.link_tab03 a:nth-child(2){bottom:56%;left:71.5%;}
				.link_tab03 a:nth-child(3){left:50.5%;}
				.link_tab03 a:nth-child(4){left:71.5%;}

				.link_tab04 a{position:absolute;bottom:14%;width:15.5%;height:40%;}
				.link_tab04 a:nth-child(1){bottom:56%;left:42%;}
				.link_tab04 a:nth-child(2){bottom:56%;left:60%;}
				.link_tab04 a:nth-child(3){bottom:56%;left:78%;}
				.link_tab04 a:nth-child(4){left:42%;}
				.link_tab04 a:nth-child(5){left:60%;}
				.link_tab04 a:nth-child(6){left:78%;}
			</style>

		</div><!-- main_tab_banner e -->
	</div><!-- top_wrap e -->
	<div class="bottom_wrap" id="default_ajax_list">
		<div class="list_wrap">
			<div class="list_box">
				<ul>
					<%
						strqry4 = "		SELECT			 SEQ "
						strqry4 = strqry4 & "			,ORDER_SEQ "
						strqry4 = strqry4 & "			,GD_TP "
						strqry4 = strqry4 & "			,GD_NM "
						strqry4 = strqry4 & "			,GD_GRP_BRAND "
						strqry4 = strqry4 & "			,GD_GRP_BRAND_NM "
						strqry4 = strqry4 & "			,END_PRICE "
						strqry4 = strqry4 & "			,round(PRICE + 40 ,-2) GD_PRICE3 "
						strqry4 = strqry4 & "			,(round(PRICE + 40 ,-2) * GD_MIN_VOL) TOT_PRICE "
						strqry4 = strqry4 & "			,IMG_PATH GD_IMG_PATH "
						strqry4 = strqry4 & "			,GD_UNI_GRP_CD "
						strqry4 = strqry4 & "			,GD_MIN_VOL "
						strqry4 = strqry4 & "			,GD_GRP_FIRST "
						strqry4 = strqry4 & "			,GD_GRP_MID "
						strqry4 = strqry4 & "			,GD_GRP_DETL "
						strqry4 = strqry4 & "			,GD_OPEN_YN "
						strqry4 = strqry4 & "			,GD_NUM "
						strqry4 = strqry4 & "	FROM (SELECT MAX(AA.SEQ) SEQ "
						strqry4 = strqry4 & "		        ,max(bb.seq) ORDER_SEQ "
						strqry4 = strqry4 & "		        ,AA.GD_TP GD_TP "
						strqry4 = strqry4 & "		        ,AA.GD_NM GD_NM "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_BRAND GD_GRP_BRAND "
						strqry4 = strqry4 & "		        ,ITEMCENTER.dbo.IC_FN_BRAND_NM(AA.GD_GRP_BRAND) GD_GRP_BRAND_NM "
						strqry4 = strqry4 & "		        ,ISNULL(MIN(AA.GD_PRICE8),0) END_PRICE "
						strqry4 = strqry4 & "		        ,MIN(ISNULL(CASE WHEN ISNULL(BB.GD_TAX_YN,'N') = 'N' THEN ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) "
						strqry4 = strqry4 & "		        	ELSE (ISNULL(AA.GD_PRICE3,0)/((100-ISNULL(BB.GD_PER,0))/100) * 1.1)END,0)) PRICE "
						strqry4 = strqry4 & "		        ,CASE WHEN RTRIM(LTRIM(MAX(AA.GD_IMG_PATH))) = '' OR MAX(AA.GD_IMG_PATH) IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
						strqry4 = strqry4 & "		        	ELSE MAX(AA.GD_IMG_PATH) END IMG_PATH "
						strqry4 = strqry4 & "		        ,AA.UNI_GRP_CD GD_UNI_GRP_CD "
						strqry4 = strqry4 & "		        ,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "		        ,MAX(BB.GD_NUM) GD_NUM "
						strqry4 = strqry4 & "		        ,AA.GD_OPEN_YN GD_OPEN_YN "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_FIRST GD_GRP_FIRST "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_MID GD_GRP_MID "
						strqry4 = strqry4 & "		        ,AA.GD_GRP_DETL GD_GRP_DETL "
						strqry4 = strqry4 & "	FROM ITEMCENTER.dbo.IC_T_GDS_INFO AA "
						strqry4 = strqry4 & "	 ,ITEMCENTER.dbo.IC_T_ONLINE_GDS BB "
						strqry4 = strqry4 & "	WHERE AA.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND BB.DEL_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.SEQ = BB.GD_SEQ "
						strqry4 = strqry4 & "	AND AA.GD_BUY_END_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_OPEN_YN = 'N' "
						strqry4 = strqry4 & "	AND AA.GD_PRICE3 > 0 "
						strqry4 = strqry4 & "	AND GD_CATE IN ('e') "
						strqry4 = strqry4 & "	AND bb.ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"'"
						strqry4 = strqry4 & "	GROUP BY		 AA.GD_TP "
						strqry4 = strqry4 & "					,AA.GD_GRP_BRAND "
						strqry4 = strqry4 & "					,AA.UNI_GRP_CD "
						strqry4 = strqry4 & "					,BB.GD_PER "
						strqry4 = strqry4 & "					,AA.GD_MIN_VOL "
						strqry4 = strqry4 & "					,BB.GD_TAX_YN "
						strqry4 = strqry4 & "					,AA.GD_OPEN_YN "
						strqry4 = strqry4 & "					,AA.GD_GRP_FIRST "
						strqry4 = strqry4 & "					,AA.GD_GRP_MID "
						strqry4 = strqry4 & "					,AA.GD_GRP_DETL	"
						strqry4 = strqry4 & "					,AA.GD_NM  "				
						strqry4 = strqry4 & ") A1 "
						strqry4 = strqry4 & "	ORDER BY ISNULL(GD_NUM,99999) ASC "
						'Response.write strqry4
						DBOpen()
							Set rs_product = Dbcon.Execute(strqry4)
						DBClose()
					%>
					<% If rs_product.bof Or rs_product.eof Then %>
					<% Else %>
					<%
						Do Until rs_product.EOF

						PERCENT = round(((CLng(rs_product("END_PRICE")) - CLng(rs_product("GD_PRICE3"))) / CLng(rs_product("END_PRICE"))) * 100)
					%>
					<li>
					<% If Not (rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006") Then %>
						<a href="/product/detail2.asp?seq=<%=rs_product("SEQ")%>">
					<% Else %>
						<a href="/product/detail.asp?seq=<%=rs_product("SEQ")%>">
					<% End If %>
							<div class="product_img"><img src="http://www.itemcenter.co.kr/<%=rs_product("GD_IMG_PATH")%>" alt="<%=rs_product("GD_NM")%>" /></div>
							<div class="product_info ss_order_mall">
								<dl>
									<dt class="name"><%=rs_product("GD_NM")%></dt>
									<% If Not (rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006")  Then %>
										<% If CLng(rs_product("GD_PRICE3")) >= 1000000 Then %>
											<dd class="test">
												<!--span>해당상품은 지정행사매장에 문의 부탁드립니다.</span-->
												<span>삼성카드 플러스페이 100만원이상 결제시 추가혜택 증정</span>
											</dd>
										<% End If %>
									<% End If %>
									<dd class="price">
<!-- 										<span><em>소비자가</em><strike><%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike></span> -->
										<span>
											<!--strike>표시가 : <%=FormatNumber(rs_product("END_PRICE"),0) %>원</strike><br/>
											<b>혜택가 : <%= FormatNumber(rs_product("GD_PRICE3"),0)%>원</b-->
											<!--b class="percent"><%=PERCENT%>%</b-->
											<strike>표시가 : 0원</strike><br/>
											<b>혜택가 : 0원</b>
											<!--strike>표시가 : 0원</strike><br/>
											<b>혜택가 : 0원</b-->
										</span>
									</dd>
								</dl>
							</div><!-- product_info e -->
						</a>
						<% If rs_product("gd_grp_mid") = "007" Or rs_product("gd_grp_mid") = "006"  Then %>
							<div class="mark">
								<% If rs_product("gd_grp_mid") = "007" Then %>
									<span class="online">온라인<br/>구매가능</span>
								<% elseIf rs_product("gd_grp_mid") = "006" Then %>
									<span class="online">온라인<br/>전용</span>
								<% Else %>
									<span>매장판매<br/>전용상품</span>
								<% End If %>
							</div>
						<% End If %>
					</li>
					<%
						rs_product.MoveNext
						x = x + 1
						Loop
						rs_product.Close
						Set rs_product = Nothing
						End If
					%>
				</ul>

			</div>
			<!-- list_box e -->
		</div><!-- list_wrap e -->
	</div><!-- cont_bottom_wrap e -->
</div><!-- container main e -->

<!-- #include virtual = "include/footer.asp" -->

<script>
$(function(){
	/*팝업1*/
	cookiedata = document.cookie; 
	if ( cookiedata.indexOf("ncookie=done1") < 0 ){ 
		document.getElementById('popup').style.display = "block";
	} else {
		document.getElementById('popup').style.display = "none"; 
	}
});
/*팝업1*/
function closeWin(){
document.getElementById('popup').style.display = "none";
}
function todaycloseWin(){
setCookie( "ncookie", "done1" , 24 ); 
document.getElementById('popup').style.display = "none";
}

function onCommonOs() { // 함수처리
	var userAgent = navigator.userAgent;
	if (userAgent.match(/iPhone|iPad|iPod/)) { // ios
		var appstoreUrl = "http://itunes.apple.com/kr/app/id393499958?mt=8";
		var clickedAt = +new Date;
		location.href = appstoreUrl; 
	} else { // 안드로이드
		location.href = "Intent://#Intent;scheme=naversearchapp;action=android.intent.action.VIEW;category=android.intent.category.BROWSABLE;package=com.nhn.android.search;end"
	}
}
//쿠키정보 저장
function setCookie( name, value, expirehours ) { 
	var todayDate = new Date(); 
	todayDate.setHours( todayDate.getHours() + expirehours ); 
	document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";" 
} 
//쿠키 정보 가지고 오기
function getCookie(Name) {
	var search = Name + "="
	if (document.cookie.length > 0){// 쿠키가 설정되어 있다면
	offset = document.cookie.indexOf(search)
	if (offset != -1){// 쿠키가 존재하면
		offset += search.length
		// set index of beginning of value
		end = document.cookie.indexOf(";", offset)
		// 쿠키 값의 마지막 위치 인덱스 번호 설정
		if (end == -1)
		end = document.cookie.length
		return unescape(document.cookie.substring(offset, end))
	}
 }
	return "";
}
</script>

<!--팝업1-->
<div id="popup" class="popup_cont" style="border:1px solid #555;">
	<a href="javascript:;" onclick="onCommonOs();">
		<img src="/front/img/popup/popup_naver_m.jpg" usemap="#pop"/>
	</a>
	<div class="btn_close">
		<button type="button" onclick="todaycloseWin();">오늘 하루 열지 않기</button>
		<button type="button" onclick="closeWin();">닫기</button>
	</div>
</div>

