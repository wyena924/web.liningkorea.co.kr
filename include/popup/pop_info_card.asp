<div class="popup_layer popup03 card_info" id="pop_info_card">
	<div class="group">
		<h2>신용카드할부안내</h2>
		<div class="cont_box">
			<p>※ 전자결제(PG) 서비스(KSPAY)를 이용하는 고객들에 대한 카드 무이자 할부개월 에 대한  안내입니다.</p>
				<ol>
					<li>1. 대상카드  : 기존 신한카드/BC/삼성/현대/KB국민/NH농협/하나카드(법인, 체크, 선불, 기프트카드 제외)</li>
					<li>2. 대상기간  : 2020.01.01 ~ 2020.01.31</li>
					<li>3. 할부조건  : 별첨 자료 참고 요망.</li>
				</ol>
				<br/>
				<p>[별첨] 할부 조건</p>
				<table cellpadding="0" cellspacing="0">
					<colgroup>
						<col style="width:20%;"/>
						<col style="width:auto;"/>
						<col style="width:auto;"/>
					</colgroup>
					<thead>
						<tr>
							<th scope="">대상 카드사</th>
							<th scope="">무이자할부</th>
							<th scope="">부분무이자 - 고객부담</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th scope="">NH농협카드</th>
							<td rowspan="6">2~6개월</td>
							<td>
								7-12개월 부분무이자는	ARS 사전 등록시에만 적용됩니다.<br/>
								(NH농협카드 : 1644-2009)<br/>
								7~10개월 : 1,2회차 부담<br/>
								11~12개월 : 1,2,3회차 부담
							</td>
						</tr>
						<tr>
							<th scope="">BC카드</th>
							<td>
								7-12개월 부분무이자는	ARS 사전 등록시에만 적용됩니다.<br/>
								(BC카드 : 1899-5772)<br/>
								7~10개월 : 1,2회차 부담<br/>
								11~12개월 : 1,2,3회차 부담
							</td>
						</tr>
						<tr>
							<th scope="">삼성카드</th>
							<td>
								10개월 : 1,2,3,4회차 부담<br/>
								12개월 : 1,2,3,4,5회차 부담<br/>
								18개월 : 1,2,3,4,5,6,7회차 부담<br/>
								24개월 : 1,2,3,4,5,6,7,8,9회차 부담							
							</td>
						</tr>
						<tr>
							<th scope="">현대카드</th>
							<td>없음</td>
						</tr>
						<tr>
							<th scope="">하나카드</th>
							<td>없음</td>
						</tr>
						<tr>
							<th scope="">KB국민카드</th>
							<td>10개월 : 1,2,3회차 부담</td>
						</tr>
						<tr>
							<th scope="">신한카드</th>
							<td>2~5개월</td>
							<td>10개월:1,2,3회차 부담</td>
						</tr>
						
						
						<tr>
							<th scope="">롯데카드</th>
							<td>없음</td>
							<td>없음</td>
						</tr>
					</tbody>
				</table>
				<br/>
			    <p>* 현대카드 : 하이브리드 카드 제외 (PG쇼핑몰/환금성/상품권 업종만 해당)</p>
			    <p>* 삼성카드 : 소셜/오픈마켓 등 포함 (세금, 병원업종, 홈쇼핑 제외)</p>
			    <p>* 5만원 이상 할부 결제 시 적용 됨.</p>
			    <p>* 온라인 PG업종에 해당되는 무이자로 일부 업종은 제외 됨.</p>
			    <p>* 제외 업종 : 홈쇼핑, 유류, 제세공과금, 등록금, 우편요금, 상품권, 도시가스요금 등.</p>
			    <p>* 제약 업종은 일부 카드사만 적용 가능 : BC카드(2~6개월), 국민카드(2~3개월), 현대카드(2~3개월)</p>
			    <p>* BC카드 : 해당월 사전 등록한 회원만 부분무이자(7~12개월) 적용 가능.</p>
			    <p>* NH농협카드 : 해당월 사전 등록한 회원만 부분무이자(7~12개월) 적용 가능.</p>
			    <p>* 하나카드 : 환금성 가맹점 및 학원업종은 제외 (온라인 PG업종만 해당)</p>
		</div>
		<!-- cont_box e -->
		<div class="btn_wrap">
			<a href="#" class="btn_agree pop_close">닫기</a>
		</div>
		<button type="button" class="close_x pop_close"></button>
	</div>
	<!-- group e -->
</div>
<!-- popup_wrap e -->
