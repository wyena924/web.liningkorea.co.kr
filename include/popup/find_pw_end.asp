<div class="popup_wrap popup03" id="find_pw_end" style="display:none;">
	<div class="box end_box" id="pwd_sarch_div">
		<h2>비밀번호 찾기완료</h2>
		<p class="end_box_cont"><span id="sarch_pwdid_span">id</span>님의 비밀번호가<br/><span id="sarch_pwd_span">id@nate.com</span>으로<br/> 발송되었습니다.</p>
		<p class="end_box_info">발송 된 비밀번호로 로그인 후<br/>"마이페이지 > 회원정보" 페이지에서<br/>비밀번호를 변경해주세요.</p>
		<div class="btn_wrap">
			<a href="javascript:div_close();" class="btn_agree pop_close">닫기</a>
		</div>
		<button type="button" type="button" class="pop_close"></button>
	</div><!-- box e -->
</div><!-- find_pw e -->
