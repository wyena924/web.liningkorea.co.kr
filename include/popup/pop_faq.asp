<div class="popup_layer popup03 qna_cont" id="pop_faq">
	<div class="group" >
		<h2>자주 하는 질문 Q&A</h2>
		<div class="cont_box etc_page qna_page">
			<div class="qna_cate">
				<a href="#" title="all" class="active">전체보기</a>
				<a href="#" title="site">사이트관련</a>
				<a href="#" title="pay">결제관련</a>
				<a href="#" title="ship">배송관련</a>
				<a href="#" title="etc">교환,반품,취소관련</a>
			</div><!-- qna_tab e -->
			<div class="qna_cont" style="font-family: 'NanumBarunGothic', 'serif'; line-height:200%; ">
				<dl class="all site">
					<dt>1. ID/PW를 분실했습니다. 어떻게 찾아야 하나요?</dt>
					<dd>
						<p>ID/PW분실 시 ID/PW 찾기 등을 통해서 확인 가능합니다.</p>
					</dd>
				</dl>
				
				<dl class="all site">
					<dt>2. 최소 구매수량 이하로는 주문이 불가능한가요?</dt>
					<dd>
						<p>원칙적으로 최소구매수량 이하로 주문은 불가합니다.</p>
					</dd>
				</dl>
				
				<dl class="all site">
					<dt>3. 장바구니에 들어있는 상품을 계속 보관할 수 있나요?</dt>
					<dd>
						<p>장바구니에 담긴 상품은 3일 동안 보관 가능합니다</p>
					</dd>
				</dl>
				<dl class="all pay">
					<dt>4. 결제방법은 무엇이 있나요?</dt>
					<dd>
						<p>카드결제만 결제가 가능합니다.</p>
					</dd>
				</dl>
			
				<!--dl class="all pay">
					<dt>5. 무통장 입금으로는 결제가 안되나요?</dt>
					<dd>
						<p>무통장 입금은 웹사이트 하단에 계좌번호가 표시되어 있으며 입금 후 담당자가 확인 후 상품 배송을 시작합니다.</p>
					</dd>
				</dl-->
				<dl class="all ship">
					<dt>5. 배송기간은 어떻게 되나요?</dt>
					<dd>
						<p>삼성전자 상품은 로지텍 물류에서 직배송되며</p>
						<p>주문결제 후 평균 2~3주 소요됩니다.</p>
						<p>기타상품은 영업일 기준 4~5일 소요됩니다.</p>
						<p>가입상품의 경우 가입 신청 후 영업일 기준 2일 이내</p>
						<p>전화를 드립니다.</p>
					</dd>
				</dl>
				<dl class="all ship">
					<dt>6. 배송비는 어떻게 되나요?</dt>
					<dd>
						<p>배송비는 추가로 발생하지 않습니다.</p>
						<p>(설치제품의 경우, 설치환경에 따라 추가비용이 발생할 수 있습니다.)</p>
					</dd>
				</dl>
				<dl class="all ship">
						<dt>7. 배송확인을 하고싶어요.</dt>

						<dd>
							<p>배송확인은 상품의 고객센터로 문의 부탁드립니다.</p>
							<p>삼성전자 상품 및 배송문의 : 010-9858-7303</p>
							<p>의료기기 상품 및 배송문의 : 010-9858-7303</p>
							<p>교보 E-Book 상품 및 배송문의 : 031-926-3780~1</p>
							<p>조명상품관련 문의 : 02-751-9458</p>
							<p>조명설치관련 문의 : 02-701-6074</p>
							<p>대명상조 가입문의  : 010-9858-7303</p>
							<p>업무시간 : 오전09:00~오후06:00(점심시간:오후12:00~13:00)</p>
							<p>토요일, 일요일, 공휴일 휴무</p>
						</dd>
					</dl>
				<dl class="all etc">
					<dt>8. 주문취소를 하고싶어요.</dt>
					<dd>
						<p>주문상품이 발송 전인 경우에 취소/변경이 가능합니다.</p>
						<p>고객센터로 연락 주시면 확인 후 취소/변경처리를 도와드립니다.</p>
					</dd>
				</dl>
				<dl class="all etc">
					<dt>9. 주문한 상품을 교환또는 반품하고 싶어요.</dt>
					<dd>
						<p>전자상거래등에서의 소비자보호에 관한 법률에 의거, 상품 인도 후 7일 이내에 교환, 반품 및 환불을 보장하고 있습니다.</p>
						<p>	" 단, 상품안내 페이지에 표시된 교환/반품 기간이 7일 보다 긴 경우에는 그 기간을 따릅니다.</p>
					</dd>
				</dl>
				<dl class="all etc">
					<dt>10. 교환또는 반품이 불가능한경우가 있나요? </dt>
					<dd>
						<p>교환 또는 반품이 불가능한 경우는 다음과 같습니다.</p>
						<p>1. 고객님의 책임 있는 사유로 상품이 멸실 또는 훼손된 경우</p>
						<p>(이미 설치된 가전제품의 경우, 제품의 박스가 개봉된 경우, 제품에 전원이 들어간 경우)</p>
						<p>2. 고객님의 사용으로 상품가치가 현저히 감소된 경우</p>
						<p>3. 패키지 상품의 경우 구성상품 일부의 개봉/설치/사용으로 상품가치가 현저히 감소된 경우</p>
						<p>(특히, 잉크/토너의 경우 상품박스 훼손 및 내부 비닐포장 개봉시 교환/반품 불가. 상품 주문 전 호환 가능상품을 반드시 확인 후 주문해주시기 바랍니다.)</p>
						<p>4. 시간이 경과되어 재판매가 곤란할 정도로 상품가치가 상실된 경우</p>
						<p>5. 복제가 가능한 상품의 경우 그 원본인 상품의 포장이 훼손된 경우</p>
						<p>6. 설치완료 상품에 하자가 없는 경우</p>
						<p>7. 상품 등의 내용이 표시/광고 내용 및 계약내용과 같고, 별도의 하자가 없음에도 단순변심으로 인한 교환을 요구하는 경우</p>
						<p>8. 기타, '전자상거래 등에서의 소비자보호에 관한 법률'등 관계법령이 정하는 교환/반품 제한사유에 해당되는 경우"</p>
					</dd>
				</dl>
			</div><!-- qna_cont e -->
		</div><!-- cont_box e -->
		<button type="button" class="close_x pop_close"></button>
	</div>
</div><!-- terms_cont e -->
<script language="javascript" type="text/javascript">
//category
$('.qna_cate a').click(function(e){
	e.preventDefault();

	$('.qna_cate a').each(function(i){
		$(this).removeClass('active');
	});
	$(this).addClass('active');

	var category = $(this).attr('title');
	$('.qna_cont dl').hide();

	if( category == 'all'){
		$('.qna_cont dl').show();
	}else{
		$('.qna_cont dl[class*='+category+']').show();
	}
});
</script>
