<form name="id_search_form"  method="post">
	<div class="popup_wrap popup03" id="find_id" style="display:none;">
		<div class="box">
			<h2>아이디 찾기<button onClick="javascript:find_id_close();" id="close_x"  type="button">X</button></h2>
			<ul class="find_info">
				<li>- 회원가입시 작성하신 정보를 입력하시면 아이디를 찾을 수 있습니다.</li>
				<li>- 입력정보를 잊으신 경우 고객센터로 문의해주세요. 고객센터( T.070-7437-8145 )</li>
			</ul>
			<div class="find_box">
				<form method="post" action="">
				<div class="find_cont">
					<input type="radio" id="email" class="input_radio" title="" value="Email" name="selectCheck" checked="checked"/>
					<label for="email" class="chk_label">이메일 주소로 찾기</label>
					<div>
						<label for="input_name">이름</label>
						<input type="text" id="input_name" class="input_txt" title="" value="" name="Email_UserName" maxlength="25"/><br/>
						<label for="input_email">이메일</label>
						<input type="text" id="input_email" class="input_txt" title="" value="" name="Email" placeholder="" maxlength="40"/>
					</div>
				</div>
				<div class="find_cont">
					<input type="radio" id="number" class="input_radio" title="" value="Phone" name="selectCheck"/>
					<label for="number" class="chk_label">전화번호로 찾기</label>
					<div>
						<label for="input_name">이름</label>
						<input type="text" id="input_name" class="input_txt" title="" value="" name="Phone_UserName"/><br/>
						<label for="input_num">일반전화/휴대폰</label>
						<input type="text" id="input_num" class="input_txt" title="" value="" name="Phone" placeholder="- 없이 입력해 주세요."/>
					</div>
				</div>
				<div class="btn_wrap">
					<a href="javascript:CheckInfo();" class="btn_agree">아이디 찾기</a>
				</div>
				</form>
			</div>
		</div>
		<div class="box end_box" style="display:none;" id="id_search_div">
			<h2>아이디 찾기 완료<button onClick="javascript:id_search_div_close();" id=""  type="button">X</button></h2>
			<p class="end_box_cont">고객님의 아이디는<br/><span id="sarch_id_span">id</span><br/>입니다.</p>
			<div class="btn_wrap">
				<a href="#" class="btn_agree" onClick="javascript:id_search_div_close();">닫기</a>
			</div>
		</div>
	</div><!-- find_id e -->
</form>
