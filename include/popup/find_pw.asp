<form name="pwd_search_form" method="post">
	<div class="popup_wrap popup03" id="find_pw" style="display:none;">
		<div class="box">
			<h2>비밀번호 찾기<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
			<ul class="find_info">
				<li>- 회원가입시 작성하신 사내메일로 비밀번호가 발송됩니다.</li>
				<li>- 등록되어 있는 이메일 주소와 일치하지 않을 경우 고객센터로 문의해주세요. ( T.070-7437-8142 )</li>
			</ul>
			<div class="find_box">
				<form method="post" action="">
				<div class="find_cont">
					<label for="input_id">사내메일</label>
					<input type="text" id="input_id" class="" title="" value="" name="PWD_UserID"/><br/>
					<input type="hidden" id="input_email" class="" title="" value="" name="PWD_UserEmail" placeholder=""/>
				</div>
				<div class="btn_wrap">
					<a href="javascript:CheckEmail();" class="btn_agree" style="width:auto;padding:10px;">비밀번호 이메일 발송</a>
				</div>
				</form>
			</div>
		</div>
		<div class="box end_box" style="display:none;" id="pwd_sarch_div">
			<h2>비밀번호 찾기완료<button onClick="javascript:find_pw_close();" id="close_x"  type="button">X</button></h2>
			<p class="end_box_cont"><span id="sarch_pwdid_span">id</span>님의 비밀번호가<br/><span id="sarch_pwd_span">id@nate.com</span>으로<br/> 발송되었습니다.</p>
			<p class="end_box_info">발송 된 비밀번호로 로그인 후<br/>"마이페이지 > 회원정보" 페이지에서<br/>비밀번호를 변경해주세요.</p>
			<div class="btn_wrap">
				<a href="#" class="btn_agree" onClick="javascript:find_pw_close();">닫기</a>
			</div>
		</div>
	</div><!-- find_pw e -->
</form>
