﻿<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1" />
<title>삼성전자온라인b2b몰</title>
<meta name="naver-site-verification" content="486e79a469a389b01187832021b48ab02f28de88" />
<meta name="description" content="삼성전자B2B몰">
<!--link rel="shortcut icon" href="../images/favicon.ico" /-->
<script src="../common/js/script.js"></script>
<!--프로그램 관련 include file -->
<!--#include virtual="/Library/config.asp"-->
<!--프로그램 관련 include file -->
<%
'### if mobile, switch to mobileweb


Dim mPageUrl, mPageArg
mPageUrl = Request.ServerVariables("URL")
mPageArg = Request.ServerVariables("QUERY_STRING")

If mPageArg <> ""  Then
	mPageUrl =  mPageUrl + "?" + mPageArg
ElseIf mPageUrl = "/gate.asp" Then
	mPageUrl = "/"
End If

Function is_mobile()
  Dim Regex, match
  Set Regex = New RegExp
     With Regex
        .Pattern = "(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|windows ce|pda|mobile|mini|palm|ipad|Android|BlackBerry|iPhone|iPod|Palm|Symbian)"
        .IgnoreCase = True
        .Global = True
      End With
   match = Regex.test(Request.ServerVariables("HTTP_USER_AGENT"))
   If match then
      is_mobile=True
   Else
      is_mobile=False
   End If
End Function

'If Is_Mobile() Then
	'Response.Redirect "/mobile/gate.asp"
'End If

	'Const Ref = "D7EFGBCHL640MN598OIJKPQRSATWXVYZ123U"
	Const Ref = "CHLD7EFGB64098OIJMN5KPQRSATWZ123UXVY"

	' 암호화
	Function encode(str, chipVal)
	        Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
	        
	        chipVal = CInt(chipVal)
	        str = StringToHex(str)
	        For i = 0 To Len(str) - 1
	                TempChar = Mid(str, i + 1, 1)
	                Conv = InStr(Ref, TempChar) - 1
	                Cipher = Conv Xor chipVal
	                Cipher = Mid(Ref, Cipher + 1, 1)
	                Temp = Temp + Cipher
	        Next
	        encode = Temp
	End Function
	
	' 복호화
	Function decode(str, chipVal)	      
			Dim Temp, TempChar, Conv, Cipher, i: Temp = ""
	
	        chipVal = CInt(chipVal)
	        For i = 0 To Len(str) - 1
	                TempChar = Mid(str, i + 1, 1)
	                Conv = InStr(Ref, TempChar) - 1
	                Cipher = Conv Xor chipVal
	                Cipher = Mid(Ref, Cipher + 1, 1)
	                Temp = Temp + Cipher
	        Next
	        Temp = HexToString(Temp)
	        decode = Temp
	End Function
	
	' 문자열 -> 16진수
	Function StringToHex(pStr)
	        Dim i, one_hex, retVal
	        For i = 1 To Len(pStr)
	                one_hex = Hex(Asc(Mid(pStr, i, 1)))
	                retVal = retVal & one_hex
	        Next
	        StringToHex = retVal
	End Function
	
	' 16진수 -> 문자열
	Function HexToString(pHex)
	        Dim one_hex, tmp_hex, i, retVal
	        For i = 1 To Len(pHex)
	                one_hex = Mid(pHex, i, 1)
	                If IsNumeric(one_hex) Then
	                        tmp_hex = Mid(pHex, i, 2)
	                        i = i + 1
	                Else
	                        tmp_hex = Mid(pHex, i, 4)
	                        i = i + 3
	                End If
	                retVal = retVal & Chr("&H" & tmp_hex)
	        Next
	        HexToString = retVal
	End Function

%>