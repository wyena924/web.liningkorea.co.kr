<% @CODEPAGE="65001" language="vbscript" %>
<%
	Response.ContentType = "text/html"
	Response.AddHeader "Content-Type", "text/html;charset=utf-8"
	Response.CodePage = "65001"
	Response.CharSet = "utf-8"
'===============================================================================
'ASP 공통 상수
'===============================================================================
	GLOBAL_VAR_UNIGRPCD						  =	"2001"
	Const GLOBAL_VAR_ONLINECD				  =	"1128"
 	GLOBAL_VAR_PEID						      =	"mall_b2bc"
	GLOBAL_VAR_CUST_SEQ						  ="212576" '(PC)
	GLOBAL_VAR_CUST_MOBILE_SEQ				  ="221257" '(모바일)
	GLOBAL_VAR_PENM						      =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_COM_NM   					  =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_ZIP1     					  =	"157"
	GLOBAL_VAR_ZIP2     					  =	"010"
	GLOBAL_VAR_ADDR     					  =	"서울시 강서구 화곡동 24-48 102호"
	GLOBAL_VAR_PE      						  =	"삼성전자"
	GLOBAL_VAR_TEL1     					  =	"02"
	GLOBAL_VAR_TEL2     					  =	"2693"
	GLOBAL_VAR_TEL3     					  =	"0184"
	GLOBAL_VAR_GRP1     					  =	"77"
	GLOBAL_VAR_GRP2     					  =	"113"
	CONST GLOBAL_VAL_TITLE					  =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_FRONT_ROOT					  =	"/gate.asp"
	GLOBAL_GRP1								  = "77"
	GLOBAL_GRP2								  = "113"

	if Request.ServerVariables("HTTP_HOST") = "widmall.co.kr" Then 
		Response.redirect "http://www.widmall.co.kr/"
	End If 

	'로그인체크
	Function ChkLogin(Refer_URL)
		If GetsLOGINID() = "" Then
			If Refer_URL <> "" Then 
				Response.Write "<script>alert('로그인을 해주시기 바랍니다'); location.href='/gate.asp?Refer_Url="&Refer_URL&"'</script>"
			Else 
				Response.Write "<script>alert('로그인을 해주시기 바랍니다'); location.href='/gate.asp';</script>"
			End If 
			Response.End
		End If 
	End Function
	
	'경찰공제회 사이트를 통해서 들어왔는지를 구분하는 체크
	Function Ch_location_gb(S_URL_LOCATION)
		If S_URL_LOCATION <> "Y" Then
			'Response.Write "<script>alert('접근하실수 없습니다.'); location.href='gate.asp'</script>"
		Else
			Gets_url_location()
		End If 	
	End Function


	'===============================================================================
	'Function : SetSession(UID, UCOMNUM)
	'Description : 로그인 설정
	'===============================================================================
	Function SetSession(sLOGINID, sCUSTSEQ,  sCUSTNM, sCUSTZIP, sCUSTADDR, sCUSTTEL, sCUSTEMAIL,sCUSTHP, saved_id, sCUSTPW)

		Response.Cookies("sLOGINID").path = "/"
		Response.Cookies("sLOGINID").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sLOGINID") = sLOGINID
		Response.cookies("sLOGINID").Expires=date+1 


		Response.Cookies("sLOGINID2").path = "/"
		Response.Cookies("sLOGINID2").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sLOGINID2") = sLOGINID
		Response.cookies("sLOGINID2").Expires=date+1 


		Response.Cookies("sCUSTSEQ").path = "/"
		Response.Cookies("sCUSTSEQ").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTSEQ") = sCUSTSEQ
		Response.cookies("sCUSTSEQ").Expires=date+1 
		
		
		Response.Cookies("sCUSTNM").path = "/"
		Response.Cookies("sCUSTNM").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTNM") = sCUSTNM
		Response.cookies("sCUSTNM").Expires=date+1 


		
		Response.Cookies("sCUSTZIP").path = "/"
		Response.Cookies("sCUSTZIP").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTZIP") = sCUSTZIP
		Response.cookies("sCUSTZIP").Expires=date+1

			
		Response.Cookies("sCUSTADDR").path = "/"
		Response.Cookies("sCUSTADDR").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTADDR") = sCUSTADDR
		Response.cookies("sCUSTADDR").Expires=date+1

		
		Response.Cookies("sCUSTTEL").path = "/"
		Response.Cookies("sCUSTTEL").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTTEL") = sCUSTTEL
		Response.cookies("sCUSTTEL").Expires=date+1

		
		Response.Cookies("sCUSTEMAIL").path = "/"
		Response.Cookies("sCUSTEMAIL").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTEMAIL") = sCUSTEMAIL
		Response.cookies("sCUSTEMAIL").Expires=date+1


		
		Response.Cookies("sCUSTHP").path = "/"
		Response.Cookies("sCUSTHP").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sCUSTHP")          = sCUSTHP
		Response.cookies("sCUSTHP").Expires=date+1
	

		Response.Cookies("sWebGubun").path = "/"
		Response.Cookies("sWebGubun").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sWebGubun") = "web"
		Response.cookies("sWebGubun").Expires=date+1


		Response.Cookies("s_saved_id").path = "/"
		Response.Cookies("s_saved_id").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("s_saved_id") = saved_id
		Response.cookies("s_saved_id").Expires=date+1


		Response.Cookies("sLOGINPWD").path = "/"
		Response.Cookies("sLOGINPWD").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("sLOGINPWD") = sCUSTPW
		Response.cookies("sLOGINPWD").Expires=date+1


		Response.Cookies("S_URL_LOCATION").path = "/"
		Response.Cookies("S_URL_LOCATION").Domain= Request.ServerVariables("SERVER_NAME")
		Response.Cookies("S_URL_LOCATION") = "Y"
		Response.cookies("S_URL_LOCATION").Expires=date+1


	End Function

	'===============================================================================
	'Function : GetsLOGINID()
	'Description : 회원 아이디 반환
	'===============================================================================
	Function GetsLOGINID()
		GetsLOGINID = Request.Cookies("sLOGINID")
	End Function
	

	Function gate_GetsLOGINID()
		gate_GetsLOGINID = Request.Cookies("sLOGINID2")
	End Function


	Function gate_GetsLOGINPWD()
		gate_GetsLOGINPWD = Request.Cookies("sLOGINPWD")
	End Function

	'===============================================================================
	'Function : GetsCUSTSEQ()
	'Description : 회원 SEQ 반환
	'===============================================================================
	Function GetsCUSTSEQ()
		GetsCUSTSEQ = Request.Cookies("sCUSTSEQ")
	End Function
	'===============================================================================
	'Function : GetsCUSTNM()
	'Description : 사용자 이름 반환
	'===============================================================================
	Function GetsCUSTNM()
		GetsCUSTNM = Request.Cookies("sCUSTNM")
	End Function
	'===============================================================================
	'Function : GetsCUSTZIP()
	'Description : 우편번호1 반환
	'===============================================================================
	Function GetsCUSTZIP()
		GetsCUSTZIP = Request.Cookies("sCUSTZIP")
	End Function

	'===============================================================================
	'Function : GetsCUSTADDR()
	'Description : 주소 반환
	'===============================================================================	
	Function GetsCUSTADDR()
		GetsCUSTADDR = Request.Cookies("sCUSTADDR")
	End Function
	'===============================================================================
	'Function : GetsCUSTTEL()
	'Description : 전호번호 반환
	'===============================================================================	
	Function GetsCUSTTEL()
		GetsCUSTTEL = Request.Cookies("sCUSTTEL")
	End Function
	'===============================================================================
	'Function : GetsCUSTEMAIL()
	'Description : 이메일 반환
	'===============================================================================	
	Function GetsCUSTEMAIL()
		GetsCUSTEMAIL = Request.Cookies("sCUSTEMAIL")
	End Function
	'===============================================================================
	'Function : GetsCUSTHP()
	'Description : 휴대폰번호 반환
	'===============================================================================
	Function GetsCUSTHP()
		GetsCUSTHP = Request.Cookies("sCUSTHP")
	End Function

	Function GetsWebGubun()
		GetsWebGubun = Request.Cookies("sWebGubun")
	End Function

	Function Gets_url_location()
		Gets_url_location = Request.Cookies("S_URL_LOCATION") 
	End function
	
	Function Gets_saved_id()
		Gets_saved_id = Request.Cookies("s_saved_id") 
	End function

	
	If Not (Request.ServerVariables("URL") = "/gate.asp" Or Request.ServerVariables("URL") = "/Mypage/login_ok.asp" Or Request.ServerVariables("URL") = "/join_ok_new.asp" ) Then 
		If GetsWebGubun() = "mobile" Then 
			Response.write("<script language='javascript'>location.href='/gate.asp'</script>")
		End If 
	End If 
	'Response.write GetsWebGubun()
	'===============================================================================
	'Function : SetEditSession()
	'Description : 회원정보 수정시 정보변경
	'===============================================================================
	Function SetEditSession(sCUSTZIP, sCUSTZIP2, sCUSTADDR, sCUSTTEL, sCUSTEMAIL,sCUSTHP)
		Response.Cookies("sCUSTZIP").path = "/"
		Response.Cookies("sCUSTZIP").Domain= Request.ServerVariables("SERVER_NAME")
		Response.cookies("sCUSTZIP").expires =""
		Response.Cookies("sCUSTZIP") = sCUSTZIP
		Response.Cookies("sCUSTZIP2") = sCUSTZIP2
		Response.Cookies("sCUSTADDR") = sCUSTADDR
		Response.Cookies("sCUSTTEL") = sCUSTTEL
		Response.Cookies("sCUSTEMAIL") = sCUSTEMAIL
		Response.Cookies("sCUSTHP") =	 sCUSTHP
  End Function

%>
<!--#include virtual="Library/dbcon.asp"-->
<!--#include virtual="Library/common_function.asp"-->
