<%
'AddZero
Function AddZero(Str)
 IF len(Str)=1 Then
  AddZero="0"&Str
 Else
  AddZero=Str
 End IF
End Function

	'#################################################################################
	'# 페이징 관련 함수
	'#################################################################################

	' 페이징시 필요한 전역변수 2개
	'Dim G_PAGE_SIZE : G_PAGE_SIZE = 25  ' 뿌려질 레코드 개수
	Dim G_PAGE_SIZE   ' 뿌려질 레코드 개수
	Dim G_TOTAL_RECORD                   ' 전체 레코드 수

	'/* 쿼리문 + 페이징 */
	Public Function ExecutePage(ByVal pSql, ByVal pPage, ByVal gPage)
			Dim rs : Set rs = Server.CreateObject("ADODB.RecordSet")
			Dim strSQL
			Dim nPage
			Dim cut, l_sql, r_sql

			G_PAGE_SIZE = gPage

			If pPage = "" or isNull(pPage) Then pPage = 1

			pSql = UCase(pSql)                  ' 대문자로 변환
			pSql = Replace(pSql, vbTab, " ")    ' 쿼리문의 Tab은 Space로
			pSql = Replace(pSql, vbCr, " ")     ' 쿼리문의 개행은 Space로

			cut = InStr(1, pSql, " TOP ")
			' top 절이 없으면 order by에서 오류 - top절 있는지 검사
			If cut = 0 Then
					cut = InStr(1, pSql, " DISTINCT ")
					If cut > 0 Then
							' distinct 가 있을 경우
							cut = cut + 8
							l_sql = Left(pSql, cut)
							r_sql = Right(pSql, Len(pSql) - cut - 1)

							pSql = l_sql & " TOP 1000000000 " & r_sql
					Else
							'distinct 가 없을 경우
							cut = InStr(1, pSql, "SELECT ")
							If cut > 0 Then
									cut = cut + 5
									l_sql = Left(pSql, cut)
									r_sql = Right(pSql, Len(pSql) - cut - 1)
									pSql = l_sql & " TOP 1000000000 " & r_sql
							End If
					End If
			End If

			nPage = pPage * CLng(G_PAGE_SIZE)
			strSQL = "Select TOP " & CStr(nPage) & " * From (" & pSql & ") AS _TEMP_PAGE_TABLE"

			G_TOTAL_RECORD = ExecuteCount(pSql)     ' 쿼리문의 전체 레코드 수 구함

			rs.CursorType = 1
			rs.PageSize = G_PAGE_SIZE
			rs.Open strSQL, DBCon

			If Not (rs.EOF Or rs.BOF) Then rs.AbsolutePage = pPage

			Set ExecutePage = rs
	End Function

	'/* 쿼리문을 주면 그 쿼리문의 레코드 수를 반환 */
	Public Function ExecuteCount(ByVal pSql)
			Dim rs : Set rs = Server.CreateObject("ADODB.RecordSet")

			pSql = "Select Count(*) From (" & pSql & ") AS _TEMP_PAGE_TABLE_CNT"
			Set rs = DBCon.Execute(pSql)

			ExecuteCount = CLng(rs(0))
			rs.Close
			Set rs = nothing
	End Function

	'/* 페이징시 전체 페이지수 계산 */
	Function GetPageCount(ByVal pTotalRecord)
			Dim retVal

			pTotalRecord = CLng(pTotalRecord)
			retVal = Fix(pTotalRecord / G_PAGE_SIZE)
			If (pTotalRecord Mod G_PAGE_SIZE) > 0 Then
					retVal = retVal + 1
			End If
			GetPageCount = CLng(retVal)
	End Function

	'/* 페이지 네비게이션을 뿌려주는 함수 */
	Public Function ShowPageBar(ByVal pCurPage, ByVal pPreImg, ByVal pNextImg, ByVal param)
			Dim nPREV
			Dim nCUR
			Dim nNEXT
			Dim i
			Dim nPageCount
			Dim retVal
			Dim strLink
			Dim pageKubun

			If pCurPage = "" or isNull(pCurPage) Then pCurPage = 1

			nPageCount = GetPageCount(G_TOTAL_RECORD)

			If pPreImg = "" Then
					pPreImg = "[이전]"
			Else
					pPreImg = "<a class='next' title='다음'><span class='blind'>[다음]</span></a>"
			End If

			If pNextImg = "" Then
					pNextImg = "[다음]"
			Else
					pNextImg = "<a class='prev'><span class='blind'>[이전]</span></a>"
			End If

			nPREV = (Fix((pCurPage - 1) / 10) - 1) * 10 + 1
			nCUR = (Fix((pCurPage - 1) / 10)) * 10 + 1
			nNEXT = (Fix((pCurPage - 1) / 10) + 1) * 10 + 1

			' [이전] 페이지 조합
			If nPREV > 0 Then
					'strLink = "?curPage=" & nPREV & param
					'retVal = "전체 : " & G_TOTAL_RECORD & "&nbsp;&nbsp;&nbsp;" & "<a href=""" & strLink & """>" & pPreImg & "</a>&nbsp;"
					strLink = param & "&curPage=" & nPREV
					'retVal = "전체 : " & G_TOTAL_RECORD & "&nbsp;&nbsp;&nbsp;" & "<a href=""javascript:loadUrl('"  & strLink &"');"">" & pPreImg & "</a>&nbsp;&nbsp;"
					retVal = "&nbsp;&nbsp;&nbsp;" & "<a href=""javascript:loadUrl('"  & strLink &"');"">" & pPreImg & "</a>&nbsp;&nbsp;"
			Else
					'retVal = "전체 : " & G_TOTAL_RECORD & "&nbsp;&nbsp;&nbsp;" & "" & pPreImg & "&nbsp;&nbsp;"
					 retVal = "&nbsp;&nbsp;&nbsp;" & "" & pPreImg & "&nbsp;&nbsp;"
			End If
			i = 1
			Do While i < 11 And nCUR <= nPageCount
					If nCUR = nPageCount Or i = 10 Then
							pageKubun = " "
					Else
							pageKubun = " . "
					End If

					If CInt(pCurPage) = CInt(nCUR) Then
							retVal = retVal & "" & nCUR & "" & pageKubun
					Else
							'strLink = "?curPage=" & nCUR & param
							'retVal = retVal & "<a href=""" & strLink & """>" & nCUR & "</a>" & pageKubun
							strLink = param & "&curPage=" & nCUR
							retVal = retVal & "<a href=""javascript:loadUrl('" & strLink & "');"">" & nCUR & "</a>" & pageKubun
					End If
					nCUR = nCUR + 1
					i = i + 1
			Loop
			' [다음] 페이지 조합
			If nNEXT <= nPageCount Then
					'strLink = "?curPage=" & nNEXT & param
					'retVal = retVal & "&nbsp;<a href=""" & strLink & """>" & pNextImg & "</a>"
					strLink = param & "&curPage=" & nNEXT
					retVal = retVal & "&nbsp;<a href=""javascript:loadUrl('"  & strLink & "');"">" & pNextImg & "</a>"
			Else
					retVal = retVal & pNextImg & ""
			End If

			ShowPageBar = retVal
	End Function

Function cart_count(User_SEQ,ONLINE_CD)
	Chk_SQL = "SELECT COUNT(*) AS CNT  FROM "
	Chk_SQL = Chk_SQL&" ITEMCENTER.DBO.IC_T_ONLINE_CART "
	Chk_SQL = Chk_SQL&" WHERE User_SEQ = '"&User_SEQ&"' "
	Chk_SQL = Chk_SQL&" AND ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"' "
	Chk_SQL = Chk_SQL&" AND DEL_YN = 'N' "

	dbopen()
		Set PNRs = Dbcon.Execute(Chk_SQL)
	dbclose()
	cart_count = PNRs("CNT")
	'Response.write PNSQL

End Function 



'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력  =========================================================================
'=========================================================================================================================================================
Function PT_Ajax_PageLink(NowPage,ViewCnt,Order_type,page_url)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging prev""></a>"	
		Response.Write "<a href="""&page_url&"?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');""class=""btn_paging prev""></a>"
		Response.Write "<a href="""&page_url&"?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			'Response.Write "<a href=""javascript:product_list('"&blockpage&"','"&ViewCnt&"','"&Order_type&"');"">"&blockpage&"</a>"
			Response.Write "<a href="""&page_url&"?NowPage="&blockpage&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type & """>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function



'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력  =========================================================================
'=========================================================================================================================================================
Function PT_PageLink(linkpage,str)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1

	
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href='javascript:product_list('"&linkpage&"','"&blockpage&"','"&str&"'); class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href='javascript:product_list('"&linkpage&"','"&blockpage&"','"&str&"'); class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href='#' class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href='#' class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href='"&linkpage&"?NowPage="&blockpage&"&"&str&"'>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href='#' class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href='#' class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href='"&linkpage&"?NowPage="&blockpage&"&"&str&"' class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href='"&linkpage&"?NowPage="&blockpage&"&"&str&"' class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function



Function PT_PageLink2(linkpage,str)

	Dim BlockPage,t
	blockpage=int((Page-1)/5)*5+1
	'blockpage=int((Page-1)/10)*10+1

	
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?Page=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href='javascript:product_list('"&linkpage&"','"&blockpage&"','"&str&"'); class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href='"&linkpage&"&Page="&blockpage - 1&"&"&str&"' class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href='#' class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href='#' class=""btn_paging prev""></a>"
	End If

	do until t>4 or blockpage > TotalPage
		If blockpage=int(Page) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href='"&linkpage&"&Page="&blockpage&"&"&str&"'>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href='#' class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href='#' class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href='"&linkpage&"?Page="&blockpage&"&"&str&"' class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href='"&linkpage&"&Page="&blockpage&"&"&str&"' class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?Page="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function
' ***************************************************************************************
' * 함수설명 : HTML BR 태그 변환 함수
' * 변수설명 : str : 변환할 변수 값
' ***************************************************************************************
FUNCTION ReplaceBr(str)
	IF ISNULL(str) = False THEN
		str = REPLACE(str,CHR(13) & CHR(10),"<BR/>")
		str = REPLACE(str,CHR(10),"<BR/>")
		ReplaceBr = str
	END IF
End Function

' ***************************************************************************************
' * 함수설명 : HTML BR 태그 변환 함수
' * 변수설명 : str : 변환할 변수 값
' ***************************************************************************************
FUNCTION ReplaceBr(str)
	IF ISNULL(str) = False THEN
		str = REPLACE(str,CHR(13) & CHR(10),"<BR/>")
		str = REPLACE(str,CHR(10),"<BR/>")
		ReplaceBr = str
	END IF
End Function





'현재 포인트
Function Now_Point(Cust_SEQ)					

		CSQL = CSQL & " SELECT  ROUND(ISNULL(SUM(A1.PO_MNY),0),0)  As PY_USE		"		
		CSQL = CSQL & " FROM (    										 "
		CSQL = CSQL & " 	SELECT      								 "
		CSQL = CSQL & " 		CUST_SEQ,  								"
		CSQL = CSQL & " 		SUM(PO_MNY) AS PO_MNY,  				"
		CSQL = CSQL & " 		SUM(PY_IN_PAY) AS PY_IN_PAY   			"
		CSQL = CSQL & " 	FROM      									 "
		CSQL = CSQL & " 	(    										 "
		CSQL = CSQL & " 		SELECT  CUST_SEQ CUST_SEQ    			"
		CSQL = CSQL & " 				 ,ISNULL(SUM(PO_MNY),0) PO_MNY   "
		CSQL = CSQL & " 				 ,0 PY_IN_PAY    				"
		CSQL = CSQL & " 		FROM itemcenter.dbo.IC_T_PMAA_POINT   	"
		CSQL = CSQL & " 		WHERE UNI_GRP_CD = '2001'    			"
		CSQL = CSQL & " 		AND DEL_YN = 'N'    					 "
		CSQL = CSQL & " 		AND PO_TP = 'Z6'    					 "
		CSQL = CSQL & " 		GROUP BY CUST_SEQ    					 "
		CSQL = CSQL & " 		UNION ALL  								"
		CSQL = CSQL & " 		SELECT	  CUST_SEQ CUST_SEQ    			"
		CSQL = CSQL & " 				 ,-SUM(PY_IN_PAY)AS  PO_MNY    	 "
		CSQL = CSQL & " 				 ,0 PY_IN_PAY    				 "
		CSQL = CSQL & " 		FROM itemcenter.dbo.IC_T_ORDER_PAY    	"
		CSQL = CSQL & " 		WHERE UNI_GRP_CD = '2001'    			"
		CSQL = CSQL & " 		AND DEL_YN = 'N'    					 "
		CSQL = CSQL & " 		AND PY_IN_TP IN ('Z6')    				"
		CSQL = CSQL & " 		GROUP BY CUST_SEQ   					 "
		CSQL = CSQL & " 		 										 "
		CSQL = CSQL & " 	)A1    										 "
		CSQL = CSQL & " 	GROUP BY CUST_SEQ    						 "
		CSQL = CSQL & " ) A1   											 "
		CSQL = CSQL & " WHERE CUST_SEQ ='"&CUST_SEQ&"'						 "

		dbopen()
			Set CRs = Dbcon.Execute(CSQL)
		dbclose()
		
	If Not(CRs.Eof Or CRs.Bof) Then
		Now_Point = CRs("PY_USE")
	Else
		Now_Point = 0
	End If
End Function



'============================================================**현재 포인트 조회 ==============================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================
'============================================================**포인트 입력 사용 ==============================================================================
'=========================================================================================================================================================
Function Insert_Point(Cust_SEQ,PI_IDX,Sum_Price,Auth_Code)
	CSQL = "SELECT PI_IDX,PI_Point,PI_Point_NM,PI_Type FROM ITEMCENTER.DBO.IC_T_ONLINE_Point_Info WHERE PI_IDX='"&PI_IDX&"'"

	Set CRs = Dbcon.Execute(CSQL)
	If Not(CRs.Eof Or CRs.Bof) Then
		PI_Point = CRs("PI_Point")
		PI_Type  = CRs("PI_Type")
		'Response.WRite  PI_type
		'적립 타입이 포인트면 포인트적립
		If PI_Type = "point" Then
			In_Point = PI_Point
			P_InOut = "I"
		'적립 타입이 퍼센트면 합계금액의 퍼센트 적립
		ElseIf PI_Type = "percent" Then
			In_Point = (CInt(PI_Point)/100)*Sum_Price
			P_InOut = "I"
		ElseIf PI_Type = "use" Then
			In_Point = "-"&Sum_Price
			P_InOut = "O"
		End If
		N_DT = Year(now)&AddZero(Month(now))&AddZero(Day(now))
		InSQL = "Insert INTO IC_T_ONLINE_Point (PO_TP,PO_MNY,Cust_SEQ,PO_WR_DT,PO_MEMO,UNI_GRP_CD,PO_NM,AuthCode,Online_CD,Work_NM,Mall_Code) "
		InSQL = InSQL& " VALUES ('"&PI_IDX&"','"&In_Point&"','"&Cust_SEQ&"','"&N_DT&"','"&CRs("PI_Point_NM")&"','"&GLOBAL_VAR_UNIGRPCD&"','"&CRs("PI_Point_NM")&"','"&Auth_Code&"','"&GLOBAL_VAR_ONLINECD&"','"&GetsLOGINID()&"','"&Request.Cookies("sGLPE")&"')"
		Dbcon.Execute(InSQL)
	End If
End Function
'==========================================================================================================================================================
'============================================================**포인트 입력 사용 ===============================================================================
'==========================================================================================================================================================
'==========================================================================================================================================================
'============================================================**주문시 포인트 입력 사용 ==========================================================================
'==========================================================================================================================================================
Function Insert_Point_Out(Cust_SEQ,PI_IDX,Sum_Price,Auth_Code,Or_Num)
	CSQL = "SELECT PI_IDX,PI_Point,PI_Point_NM,PI_Type FROM ITEMCENTER.DBO.IC_T_ONLINE_Point_Info WHERE PI_IDX='"&PI_IDX&"'"
	'Response.Write CSQL
	'Response.End
	DBOpen()
	Set CRs = Dbcon.Execute(CSQL)
	DBClose()
	If Not(CRs.Eof Or CRs.Bof) Then
		PI_Point = CRs("PI_Point")
		PI_Type  = CRs("PI_Type")
		'Response.WRite  PI_type
		'적립 타입이 포인트면 포인트적립
		If PI_Type = "point" Then
			In_Point = PI_Point
			P_InOut = "I"
		'적립 타입이 퍼센트면 합계금액의 퍼센트 적립
		ElseIf PI_Type = "percent" Then
			In_Point = (CInt(PI_Point)/100)*Sum_Price
			P_InOut = "I"
		ElseIf PI_Type = "use" Then
			In_Point = "-"&Sum_Price
			P_InOut = "O"
		End If
		N_DT = Year(now)&AddZero(Month(now))&AddZero(Day(now))
		InSQL = "Insert INTO IC_T_ONLINE_Point (PO_TP,PO_MNY,Cust_SEQ,PO_WR_DT,PO_MEMO,UNI_GRP_CD,PO_NM,AuthCode,Online_CD,Work_NM,Or_Num,Mall_Code) "
		InSQL = InSQL& " VALUES ('"&PI_IDX&"','"&In_Point&"','"&Cust_SEQ&"','"&N_DT&"','"&CRs("PI_Point_NM")&"','"&GLOBAL_VAR_UNIGRPCD&"','"&CRs("PI_Point_NM")&"','"&Auth_Code&"','"&GLOBAL_VAR_ONLINECD&"','"&GetsLOGINID()&"','"&Or_Num&"','"&Request.Cookies("sGLPE")&"')"

		'Response.Write InSQL
		'Response.End
		DBOpen()
			Dbcon.Execute(InSQL)
		DBClose()
	End If
End Function
'=========================================================================================================================================================
'============================================================**주문시 포인트 입력 사용 =========================================================================
'=========================================================================================================================================================
'=========================================================================================================================================================

Sub Pageing(filename)
	blockpage = Int((page - 1) / 10) * 10 + 1

	response.write "<ul class=""pagination"">"

	If blockpage = 1 then
		response.write "<li class=""page-item""><a href=""#"" class=""page-link"">이전</a></li>"
	Else
		response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage - 1 & """ class=""page-link"">이전</a></li>"
	End if

	i = 1
	Do Until i > 10 or blockpage > total_page
		if blockpage = int(page) then
			response.write "<li class=""page-item active""><a href=""#"" class=""page-link"">" & blockpage & "</a></li>"
		Else
			response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage & """ class=""page-link"">" & blockpage & "</a></li>"
		End if

		blockpage = blockpage + 1
		i = i + 1
	Loop

	If blockpage > total_page then
		response.write "<li class=""page-item""><a href=""#"" class=""page-link"">다음</a></li>"
	Else
		response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage & """ class=""page-link"">다음</a></li>"
	End if

	response.write "</ul>"
End Sub

'상품상세 정보를 지고 올때 사용하는 쿼리(상품 옵션)
'몰코드,상품명,금액계산타입
Function Option_View(ONLINE_CD,GD_NM,PRICE_TYPE)
'상품명으로 검색
O_View_SQL = "SELECT "
O_View_SQL = O_View_SQL & " GI.SEQ AS SEQ, GI.GD_NM, GI.GD_GRP_BRAND,ITEMCENTER.DBO.IC_FN_BRAND_NM(GI.GD_GRP_BRAND) AS GD_GRP_BRAND_NM, GI.GD_TP "
'원산지
O_View_SQL = O_View_SQL & ", GI.GD_GRP_MADE"
O_View_SQL = O_View_SQL & ", ITEMCENTER.DBO.IC_FN_MADE_NM(GI.GD_GRP_MADE) AS GD_GRP_MADE_NM "
O_View_SQL = O_View_SQL & ", ITEMCENTER.DBO.IC_FN_DETL_NM(GI.GD_GRP_DETL,GI.GD_GRP_FIRST,GI.GD_GRP_MID) AS GD_GRP_DETL_NM "
'상품 이미지
O_View_SQL = O_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH)) = '' OR GI.GD_IMG_PATH IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
O_View_SQL = O_View_SQL & " ELSE GI.GD_IMG_PATH END GD_IMG_PATH "
O_View_SQL = O_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_2)) = '' OR GI.GD_IMG_PATH_2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
O_View_SQL = O_View_SQL & " ELSE GI.GD_IMG_PATH_2 END GD_IMG_PATH_2 "
O_View_SQL = O_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_3)) = '' OR GI.GD_IMG_PATH_3 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
O_View_SQL = O_View_SQL & " ELSE GI.GD_IMG_PATH_3 END GD_IMG_PATH_3 "
O_View_SQL = O_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_4)) = '' OR GI.GD_IMG_PATH_4 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
O_View_SQL = O_View_SQL & " ELSE GI.GD_IMG_PATH_4 END GD_IMG_PATH_4 "
'상세설명 이미지
O_View_SQL = O_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH2)) = '' OR GI.GD_IMG_PATH2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
O_View_SQL = O_View_SQL & " ELSE GI.GD_IMG_PATH2 END GD_IMG_PATH2 "
'옵션1,2
O_View_SQL = O_View_SQL & " ,GI.GD_SIZE AS GD_OPTION1 "
O_View_SQL = O_View_SQL & " ,GI.GD_COLOR AS GD_OPTION2 "

'===상품 최소 구매수량
O_View_SQL = O_View_SQL & " ,GI.GD_MIN_VOL "
'===상품 최소 구매수량
'===일시품절
O_View_SQL = O_View_SQL & " ,GI.GD_OPEN_YN "
'===일시품절
'===가격 정책에 따른 금액 계산
'2017.10.18 수정
O_View_SQL = O_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
O_View_SQL = O_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) GD_PRICE3 "
'2017.10.18 수정
O_View_SQL = O_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
O_View_SQL = O_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Sell_Price "

'O_View_SQL = O_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
'O_View_SQL = O_View_SQL & " ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) * 1.1 END,0) AS Sell_Price "

O_View_SQL = O_View_SQL & " ,GI.GD_PRICE7 "
O_View_SQL = O_View_SQL & " ,GI.GD_PRICE8 "

'===마진율
O_View_SQL = O_View_SQL & " ,OG.GD_PER "
'===마진율

O_View_SQL = O_View_SQL & " FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
O_View_SQL = O_View_SQL & " ON GI.SEQ = OG.GD_SEQ "
O_View_SQL = O_View_SQL & " WHERE GI.DEL_YN = 'N' "
O_View_SQL = O_View_SQL&"   AND OG.ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"' "
O_View_SQL = O_View_SQL & " AND GI.GD_BUY_END_YN = 'N' "
'O_View_SQL = O_View_SQL & " AND GI.GD_OPEN_YN = 'N' "
O_View_SQL = O_View_SQL & " AND OG.DEL_YN = 'N' "
O_View_SQL = O_View_SQL & " AND OG.UNI_GRP_CD = '2001' "
'옵션값이 없으면 옵션정보를 보이지 않기 위해 추가
O_View_SQL = O_View_SQL & " AND (GI.GD_SIZE <> '' OR GI.GD_COLOR <> '') "

'상품명 검색
O_View_SQL = O_View_SQL & " AND GI.GD_NM = '" & GD_NM & "' "
O_View_SQL = O_View_SQL & " ORDER BY GI.SEQ asc "
'Response.write O_View_SQL
dbopen()
	Set OptionRs = Dbcon.Execute(O_View_SQL)
dbclose()

If Not(OptionRs.Eof Or OptionRs.Bof) Then
Option_View = OptionRs.GetRows()
Else
'데이터가 없다면 강제로 첫레코드 값을 NODATA로 해서 GetRows()에 담는다
dbopen()
	Option_View = Dbcon.Execute("SELECT 'NODATA'").GetRows()
dbclose()
End If
End Function



'고객정보
Function Order_Cust_Info(Order_SEQ)
	InfoSQL =" SELECT "
	InfoSQL = InfoSQL & " ORDER_NM, "
	InfoSQL = InfoSQL & " CM_HP, "
	InfoSQL = InfoSQL & " CM_TEL, "
	InfoSQL = InfoSQL & " CM_ZIP, "
	InfoSQL = InfoSQL & " CM_ZIP2, "
	InfoSQL = InfoSQL & " CM_ADDR, "
	InfoSQL = InfoSQL & " CM_ADDR1, "
	InfoSQL = InfoSQL & " CM_ADDR2, "
	InfoSQL = InfoSQL & " CM_EMAIL, "
	InfoSQL = InfoSQL & " online_id, "
	InfoSQL = InfoSQL & " GL_PE, "
	'세금계산서 항목 관련 검색 필드 추가
	InfoSQL = InfoSQL & " licensee_nm, "
	InfoSQL = InfoSQL & " cm_num, "
	InfoSQL = InfoSQL & " cm_pe, "
	InfoSQL = InfoSQL & " cm_uptae, "
	InfoSQL = InfoSQL & " cm_jong, "
	InfoSQL = InfoSQL & " re_nm, "
	InfoSQL = InfoSQL & " re_hand_tel1, "
	InfoSQL = InfoSQL & " re_hand_tel2, "
	InfoSQL = InfoSQL & " re_hand_tel3, "
	InfoSQL = InfoSQL & " re_email, "
	InfoSQL = InfoSQL & " CP.re_email_yn, "
	InfoSQL = InfoSQL & " re_event_yn "
	'세금계산서 항목 관련 검색 필드 추가
	InfoSQL = InfoSQL & " FROM ITEMCENTER.DBO.IC_T_ORDER_CUST OC "
	InfoSQL = InfoSQL & " LEFT OUTER JOIN ITEMCENTER.DBO.IC_T_CUST_PE CP ON OC.Order_seq = CP.Re_cust_SEQ "
	InfoSQL = InfoSQL & " WHERE ORDER_SEQ = '" & Order_SEQ & "' "
	InfoSQL = InfoSQL & " order by Re_SEQ DESC "
	'Response.Write InfoSQL
	'Response.End
	dbopen()
		Order_Cust_Info = Dbcon.Execute(InfoSQL)
	dbclose()
	End Function



Function Product_View(ONLINE_CD,GD_NM,GD_SEQ,PRICE_TYPE)
	'상품명으로 검색시 여러 데이터가 나오므로 TOP 1 정렬은 GD_PRICE3 낮은 데이터로 정렬
	P_View_SQL = "SELECT TOP 1 "
	P_View_SQL = P_View_SQL & " GI.SEQ AS SEQ "
	P_View_SQL = P_View_SQL & " ,GI.GD_NM "
	P_View_SQL = P_View_SQL & " ,GI.GD_GRP_BRAND "
	P_View_SQL = P_View_SQL & " ,ITEMCENTER.DBO.IC_FN_BRAND_NM(GI.GD_GRP_BRAND) AS GD_GRP_BRAND_NM "
	P_View_SQL = P_View_SQL & " ,GI.GD_TP "


	'원산지
	P_View_SQL = P_View_SQL & ", GI.GD_GRP_MADE"
	'상품제공업체코드
	P_View_SQL = P_View_SQL & ", GI.UNI_GRP_CD"
	'셋트상품구성
	P_View_SQL = P_View_SQL & ", GI.GD_SET"

	P_View_SQL = P_View_SQL & ", ITEMCENTER.DBO.IC_FN_MADE_NM(GI.GD_GRP_MADE) AS GD_GRP_MADE_NM "

	P_View_SQL = P_View_SQL & ", ITEMCENTER.DBO.IC_FN_DETL_NM(GI.GD_GRP_DETL,GI.GD_GRP_FIRST,GI.GD_GRP_MID) AS GD_GRP_DETL_NM "
	'
	'상품 이미지
	P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH)) = '' OR GI.GD_IMG_PATH IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
	P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH END GD_IMG_PATH "
	P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_2)) = '' OR GI.GD_IMG_PATH_2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
	P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_2 END GD_IMG_PATH_2 "
	P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_3)) = '' OR GI.GD_IMG_PATH_3 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
	P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_3 END GD_IMG_PATH_3 "
	P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_4)) = '' OR GI.GD_IMG_PATH_4 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
	P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_4 END GD_IMG_PATH_4 "
	'상세설명 이미지
	P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH2)) = '' OR GI.GD_IMG_PATH2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
	P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH2 END GD_IMG_PATH2 "
	'옵션1,2
	P_View_SQL = P_View_SQL & " ,GI.GD_SIZE AS GD_OPTION1 "
	P_View_SQL = P_View_SQL & " ,GI.GD_COLOR AS GD_OPTION2 "
	'===상품 최소 구매수량
	P_View_SQL = P_View_SQL & " ,GI.GD_MIN_VOL AS GD_MIN_VOL "
	'===상품 최소 구매수량
	'===알파메모
	P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_ALPHA_MEMO,'') AS GD_ALPHA_MEMO "
	'===알파메모
	'===온라인 GDS테이블 SEQ
	P_View_SQL = P_View_SQL & " ,OG.SEQ AS OG_SEQ "
	P_View_SQL = P_View_SQL & " ,Excel_Path "

	'===가격 정책에 따른 금액 계산

	'P_View_SQL = P_View_SQL & " ,ROUND(ISNULL(GI.GD_PRICE3,0) / ((100-ISNULL(OG.GD_PER,0))/100 )+40,-2) AS GD_PRICE3 "
	'운영회원가 * 1.1
	P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE,0) AS GD_PRICE "
	P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE7,0) AS GD_PRICE7 "
	P_View_SQL = P_View_SQL & " ,GI.GD_PRICE8 AS GD_PRICE8 "
	'P_View_SQL = P_View_SQL & " ,0 AS GD_PRICE "            
	'P_View_SQL = P_View_SQL & " ,0 AS GD_PRICE7 "          
	'P_View_SQL = P_View_SQL & " ,0 AS GD_PRICE8 "                    	'2017.10.18 변경
	P_View_SQL = P_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
	P_View_SQL = P_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Sell_Price "

	'2017.10.18 변경
	P_View_SQL = P_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
	P_View_SQL = P_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) GD_PRICE3 "

		'2017.10.18 변경
	P_View_SQL = P_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
	P_View_SQL = P_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) * GD_MIN_VOL TOT_PRICE "


'P_View_SQL = P_View_SQL & "	,0 as Sell_Price "                        
                                                                                                                                                                                                                                                                                                     
'P_View_SQL = P_View_SQL & "	,0 as GD_PRICE3 "                         
                                                                                                                                                           
	'2017.10.18 변경                                                                                                                                         
'P_View_SQL = P_View_SQL & "	,0 as TOT_PRICE "            



'===가격 정책에 따른 금액 계산
'기존 삼공몰 가격1
'P_View_SQL = P_View_SQL & " ,(round(GD_PRICE3 + 40 ,-2) * GD_MIN_VOL) TOT_PRICE "

'스티커사용여부
P_View_SQL = P_View_SQL & " ,OG.ST_YN AS ST_YN "
'포장지사용여부
P_View_SQL = P_View_SQL & " ,OG.WRAP_YN AS WRAP_YN "
'박스사용여부
P_View_SQL = P_View_SQL & " ,OG.RIBON_YN AS RIBON_YN "
'부가세포함여부
P_View_SQL = P_View_SQL & " ,OG.GD_TAX_YN AS GD_TAX_YN "
'마진율
P_View_SQL = P_View_SQL & " ,OG.GD_PER AS GD_PER "
'상품대분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_FIRST AS GD_GRP_FIRST "
'상품중분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_MID AS GD_GRP_MID "
'상품소분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL AS GD_GRP_DETL "
'상품세세분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL2 AS GD_GRP_DETL2 "
'GD_CATE
P_View_SQL = P_View_SQL & " ,OG.GD_CATE AS GD_CATE "
'면세상품여부
P_View_SQL = P_View_SQL & " ,GI.GD_Tax_Free_YN AS Tax_Free "
P_View_SQL = P_View_SQL & " ,GI.Catalogue_Code AS Catalogue_Code "
'일시품절
P_View_SQL = P_View_SQL & " ,GI.GD_OPEN_YN "
'상품설명
P_View_SQL = P_View_SQL & " ,Replace(gd_memo, CHAR(13)+CHAR(10),'') AS GD_Memo "

P_View_SQL = P_View_SQL & " FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS OG "
P_View_SQL = P_View_SQL & " ON GI.SEQ = OG.GD_SEQ "
P_View_SQL = P_View_SQL & " WHERE GI.DEL_YN = 'N'"
P_View_SQL = P_View_SQL&" AND OG.ONLINE_CD = '"&ONLINE_CD&"' "
P_View_SQL = P_View_SQL & " AND GI.GD_BUY_END_YN = 'N' "
'P_View_SQL = P_View_SQL & " AND GI.GD_OPEN_YN = 'N' "
P_View_SQL = P_View_SQL & " AND OG.DEL_YN = 'N' "
'P_View_SQL = P_View_SQL & " AND OG.UNI_GRP_CD = '2001'"

'상품명 검색
If GD_NM <> "" Then
	P_View_SQL = P_View_SQL & " AND GI.GD_NM = '" & GD_NM & "' "
Else
	'상품명이 없다면 SEQ값이 있는걸로 간주함
	P_View_SQL = P_View_SQL & " AND GI.SEQ = '" & GD_SEQ & "' "
End If

If GD_NM <> "" Then
	P_View_SQL = P_View_SQL & " ORDER BY GI.GD_OPEN_YN ASC, GI.GD_PRICE3 ASC, GI.SEQ ASC "
End If
'Response.write P_View_SQL

dbopen()
	Product_View = Dbcon.Execute(P_View_SQL)
dbclose()

'Response.write P_View_SQL
End Function



Function Buy_Product_View(ONLINE_CD,GD_NM,GD_SEQ,PRICE_TYPE)
'상품명으로 검색시 여러 데이터가 나오므로 TOP 1 정렬은 GD_PRICE3 낮은 데이터로 정렬
P_View_SQL = "SELECT TOP 1 "
P_View_SQL = P_View_SQL & " GI.SEQ AS SEQ, GI.GD_NM, GI.GD_GRP_BRAND, ITEMCENTER.DBO.IC_FN_BRAND_NM(GI.GD_GRP_BRAND) AS GD_GRP_BRAND_NM, GI.GD_TP "
'원산지
P_View_SQL = P_View_SQL & ", GI.GD_GRP_MADE"
'상품제공업체코드
P_View_SQL = P_View_SQL & ", GI.UNI_GRP_CD"
'셋트상품구성
P_View_SQL = P_View_SQL & ", GI.GD_SET"


P_View_SQL = P_View_SQL & ", ITEMCENTER.DBO.IC_FN_MADE_NM(GI.GD_GRP_MADE) AS GD_GRP_MADE_NM "

P_View_SQL = P_View_SQL & ", ITEMCENTER.DBO.IC_FN_DETL_NM(GI.GD_GRP_DETL,GI.GD_GRP_FIRST,GI.GD_GRP_MID) AS GD_GRP_DETL_NM "
'
'상품 이미지
P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH)) = '' OR GI.GD_IMG_PATH IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH END GD_IMG_PATH "
P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_2)) = '' OR GI.GD_IMG_PATH_2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_2 END GD_IMG_PATH_2 "
P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_3)) = '' OR GI.GD_IMG_PATH_3 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_3 END GD_IMG_PATH_3 "
P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_4)) = '' OR GI.GD_IMG_PATH_4 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_4 END GD_IMG_PATH_4 "
'상세설명 이미지
P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH2)) = '' OR GI.GD_IMG_PATH2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH2 END GD_IMG_PATH2 "
'옵션1,2
P_View_SQL = P_View_SQL & " ,GI.GD_SIZE AS GD_OPTION1 "
P_View_SQL = P_View_SQL & " ,GI.GD_COLOR AS GD_OPTION2 "

'===상품 최소 구매수량
P_View_SQL = P_View_SQL & " ,GI.GD_MIN_VOL AS GD_MIN_VOL "
'===상품 최소 구매수량
'===알파메모
P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_ALPHA_MEMO,'') AS GD_ALPHA_MEMO "
'===알파메모

'===가격 정책에 따른 금액 계산
If PRICE_TYPE = "" Then
	'P_View_SQL = P_View_SQL & " ,ROUND(ISNULL(GI.GD_PRICE3,0) / ((100-ISNULL(OG.GD_PER,0))/100 )+40,-2) AS GD_PRICE3 "
	'운영회원가 * 1.1
	P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE,0) AS GD_PRICE "

	'P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) * 1.1 'ELSE				ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) * 1.1 END,0) AS Sell_Price "

	'2017.10.18 변경
	P_View_SQL = P_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
	P_View_SQL = P_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Sell_Price "
	
	'P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE				ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 "

	'2017-07-26 변경
	'P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)* '1.1)+40,-2) ELSE ROUND(ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) * 1.1 + 40,-2) END,0) AS GD_PRICE3 "

	'2017.10.18 변경
	P_View_SQL = P_View_SQL & "	,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
	P_View_SQL = P_View_SQL & "	ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) GD_PRICE3 "


	P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE7,0) AS GD_PRICE7 "
	P_View_SQL = P_View_SQL & " ,GI.GD_PRICE8 AS GD_PRICE8 "
End  If
'===가격 정책에 따른 금액 계산

'스티커사용여부
P_View_SQL = P_View_SQL & " ,OG.ST_YN AS ST_YN "
'포장지사용여부
P_View_SQL = P_View_SQL & " ,OG.WRAP_YN AS WRAP_YN "
'박스사용여부
P_View_SQL = P_View_SQL & " ,OG.RIBON_YN AS RIBON_YN "
'부가세포함여부
P_View_SQL = P_View_SQL & " ,OG.GD_TAX_YN AS GD_TAX_YN "
'마진율
P_View_SQL = P_View_SQL & " ,OG.GD_PER AS GD_PER "

'상품대분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_FIRST AS GD_GRP_FIRST "
'상품중분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_MID AS GD_GRP_MID "
'상품소분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL AS GD_GRP_DETL "
'상품세세분류
P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL2 AS GD_GRP_DETL2 "
'GD_CATE
P_View_SQL = P_View_SQL & " ,OG.GD_CATE AS GD_CATE "

'상품마스터삭제여부 Y 삭제
P_View_SQL = P_View_SQL & " ,GI.DEL_YN AS GI_DEL_YN "
'온라인상품삭제여부 Y 삭제
P_View_SQL = P_View_SQL & " ,OG.DEL_YN AS OG_DEL_YN "
'단종여부 Y 단종
P_View_SQL = P_View_SQL & " ,GI.GD_BUY_END_YN AS BUY_END_YN "
'판매중 N 판매중
P_View_SQL = P_View_SQL & " ,GI.GD_OPEN_YN AS OPEN_YN "
'인증여부 Y " 인증 N:미인증
P_View_SQL = P_View_SQL & " ,GI.ICAuthYN AS Auth_YN "
'상품설명
P_View_SQL = P_View_SQL & " ,GI.GD_Memo AS GD_Memo "
'면세상품여부
P_View_SQL = P_View_SQL & " ,GI.GD_Tax_Free_YN AS Tax_Free "

'위드라인 판매원가계산
'할인율 계산을 위한 위드라인 원가
P_View_SQL = P_View_SQL & " ,ROUND(ISNULL(GI.GD_PRICE3,0) * 1.1 +40,-2) AS GD_Origin_Price "

P_View_SQL = P_View_SQL & " FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
P_View_SQL = P_View_SQL & " ON GI.SEQ = OG.GD_SEQ "
P_View_SQL = P_View_SQL & " WHERE "
'P_View_SQL = P_View_SQL & " AND GI.GD_BUY_END_YN = 'N' "
'P_View_SQL = P_View_SQL & " AND GI.GD_OPEN_YN = 'N' "
'P_View_SQL = P_View_SQL & " AND OG.UNI_GRP_CD = '2001' "

'상품명 검색
If GD_NM <> "" Then
	P_View_SQL = P_View_SQL & " GI.GD_NM = '" & GD_NM & "' "
Else
	'상품명이 없다면 SEQ값이 있는걸로 간주함
	P_View_SQL = P_View_SQL & " GI.SEQ = '" & GD_SEQ & "' "
End If

'몰코드
If Left(GetsLOGINID,1) = "N" then
	'P_View_SQL = P_View_SQL & " AND OG.ONLINE_CD = '0050' "
else
	'P_View_SQL = P_View_SQL & " AND OG.ONLINE_CD = '0031' "
End If

P_View_SQL = P_View_SQL & " ORDER BY OG.SEQ DESC "
'Response.WRite P_View_SQL
'Response.End
dbopen()
	Buy_Product_View = Dbcon.Execute(P_View_SQL)
dbclose()
End Function



Function Chk_KB_Product(GD_SEQ)
  If GD_SEQ = "241601" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241602" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "240528" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241623" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241621" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241620" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241626" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241625" Then
		Chk_KB_Product = "Y"
	ElseIf GD_SEQ = "241624" Then
		Chk_KB_Product = "Y"
	Else
		Chk_KB_Product = "N"
	End If
End Function



'회원번호,세션,상품번호,사이트구분값,수량,스티커,포장지,전용박스
Function Cart_Add(User_SEQ,User_Session,GD_SEQ,ONLINE_CD,GD_VOL,ST_SEQ,WRAP_SEQ,Tb_Type,gd_grp_mid,gd_grp_detl)
	Tb_Name = "ITEMCENTER.DBO.IC_T_ONLINE_CART"
	'기존에 장바구니에 담긴 상품인지 체크
	Chk_SQL = "SELECT COUNT(GD_SEQ) FROM "
	Chk_SQL = Chk_SQL&Tb_Name
	Chk_SQL = Chk_SQL&" WHERE User_Session = '"&User_Session&"' "
	Chk_SQL = Chk_SQL&" AND GD_SEQ = '"&GD_SEQ&"' "
	Chk_SQL = Chk_SQL&" AND ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"' "
	Chk_SQL = Chk_SQL&" AND DEL_YN = 'N' "
	'Response.write Chk_SQL
	'Response.End 
	dbopen()
		Set CartCount = DBcon.Execute(Chk_SQL)
	dbclose()

	If CartCount(0)  = 0 Then
		InSQL = "INSERT "
		InSQL = InSQL&Tb_Name
		InSQL = InSQL&"(User_Seq,User_Session,GD_SEQ,ONLINE_CD,GD_EA,Regdate,DEL_YN,USER_IP,ST_SEQ,WRAP_SEQ,Sub_Gubun) "
		InSQL = InSQL&" VALUES ('"&User_Seq&"','"&User_Session&"','"&GD_SEQ&"','"&ONLINE_CD&"','"&GD_VOL&"',Getdate(),'N','"&Request.ServerVariables("remote_addr")&"','"&ST_SEQ&"','"&WRAP_SEQ&"','')"
		'Response.write InSQL
		'Response.end		
		dbopen()
			Dbcon.Execute(InSQL)
		dbclose()
		'If Tb_Type = "Cart" Then
			Response.Write "<script>if(confirm('장바구니에 상품이 저장되었습니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp?gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"'}</script>"
		'End If
			Response.End
	Else
		'If Tb_Type = "Cart" Then
			'해당스티커 옵션이 바뀔수도 있으므로 해당 카드 업데이트
			UpSQL = "UPDATE "
			UpSQL = UPSQL&Tb_Name
			UpSQL = UpSQL&" Set GD_EA='"&GD_VOL&"',Regdate=getdate(),ST_SEQ='"&ST_SEQ&"',WRAP_SEQ='"&WRAP_SEQ&"' WHERE GD_SEQ='"&GD_SEQ&"' AND ONLINE_CD='"&ONLINE_CD&"' AND User_SEQ='"&User_Seq&"'"
			'Response.write UpSQL
			'Response.end
			dbopen()
				Dbcon.Execute(UpSQL)
			dbclose()
			'Response.Write "<script>if(confirm('이미 장바구니에 담겨있는 상품입니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp';}</script>"
			Response.Write "<script>if(confirm('장바구니에 상품이 저장되었습니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp?gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"'}</script>"
		'End If
		Response.End
	End If
	
End Function



'회원번호,세션아이디,몰코드,카트현재페이지,카트한페이지당 보여줄 아이템 수
Function Cart_List(User_SEQ,User_Session,ONLINE_CD,Tb_Type)

		CartSQL = "SELECT GD_SEQ,IDX,Convert(varchar(10),Regdate,102) AS RegDate,GD_EA,ST_SEQ,WRAP_SEQ FROM "
		CartSQL = CartSQL&"ITEMCENTER.DBO.IC_T_ONLINE_CART a "
		CartSQL = CartSQL&"INNER JOIN IC_T_GDS_INFO B  "
		CartSQL = CartSQL&" ON A.GD_SEQ = B.SEQ "
		CartSQL = CartSQL&" WHERE user_seq = '" & User_SEQ & "' "
		CartSQL = CartSQL&" AND ONLINE_CD = '"&GLOBAL_VAR_ONLINECD&"' "
		CartSQL = CartSQL&" AND A.DEL_YN = 'N' "
		CartSQL = CartSQL&" AND B.DEL_YN = 'N' "
		'CartSQL = CartSQL&" AND Sub_Gubun='"&Request.Cookies("gubun")&"'"

		'Response.write Cart_List
		'Response.end

		dbopen()
			Set CartRs = dbcon.Execute(CartSQL)
		dbclose()

			If Not(CartRs.Bof Or CartRs.Eof) Then
				Cart_List = CartRs.GetRows()
			Else
				dbopen()
					Cart_List = Dbcon.Execute("SELECT 'NODATA'").GetRows()
				dbclose()
			End If

End Function



Function Cart_Del_Check(User_SEQ,User_Session,ONLINE_CD,SEQ,Tb_TYPE,gd_grp_mid,gd_grp_detl)

DelSQL = "UPDATE "

DelSQL = DelSQL&"ITEMCENTER.DBO.IC_T_ONLINE_CART "
DelSQL = DelSQL&" SET DEL_YN = 'Y' "

If Tb_Type = "Cart" Then
	DelSQL = DelSQL&" WHERE GD_SEQ in (" & SEQ & ") "
Else
	DelSQL = DelSQL&" WHERE IDX in (" & SEQ & ") "
End If
DelSQL = DelSQL&" AND User_SEQ = '" & User_SEQ & "' "
DelSQL = DelSQL&" AND ONLINE_CD = '"&ONLINE_CD&"' "

dbopen()
	DbCon.Execute(DelSQL)
dbclose()

If Tb_Type = "Cart" Then
	Response.Write "<script>parent.location.href='cart.asp';</script>"
Else
	Response.Write "<script>parent.location.href='zzim.asp';</script>"
End If
Response.End
End Function



Function EA_Change(Cart_SEQ,Chnage_EA,Tb_Type)
		UpSQL = "UPDATE "
		If Tb_Type = "Cart" Then
			UpSQL = UpSQL&"ITEMCENTER.DBO.IC_T_ONLINE_CART "
		Else
			UpSQL = UpSQL&"ITEMCENTER.DBO.IC_T_ONLINE_Zzim "
		End If
		UpSQL = UpSQL&" SET GD_EA = '" & Chnage_EA & "' "
		UpSQL = UpSQL&" WHERE IDX = '" & Cart_SEQ & "' "

		dbopen()
			DbCon.Execute(UpSQL)
		dbclose()

		Response.Write "<script>parent.location.href='cart.asp';</script>"
		Response.End
End Function



Function Cart_Del_One(User_SEQ,User_Session,Cart_SEQ,ONLINE_CD,Tb_Type)
DelSQL = "UPDATE "
If Tb_Type = "Cart" Then
	DelSQL = DelSQL&"ITEMCENTER.DBO.IC_T_ONLINE_CART "
Else
	DelSQL = DelSQL&"ITEMCENTER.DBO.IC_T_ONLINE_Zzim "
End If

DelSQL = DelSQL&"SET DEL_YN = 'Y' "

If User_SEQ <> "" Then
	DelSQL = DelSQL&" WHERE User_SEQ = '" & User_SEQ & "' "
Else
	DelSQL = DelSQL&" WHERE User_Session = '" & User_Session & "' "
End If
DelSQL = DelSQL&" AND IDX = '" & Cart_SEQ & "' "
If Left(GetsLOGINID,1) = "N" then
	DelSQL = DelSQL&" AND ONLINE_CD = '0050' "
Else
	DelSQL = DelSQL&" AND ONLINE_CD = '0031' "
End If
dbopen()
	DbCon.Execute(DelSQL)
dbclose()
If Tb_Type = "Cart" Then
	Response.Write "<script>parent.location.href='../order/cart.asp';</script> "
	Response.End
Else
	Response.Write "<script>parent.location.href='../mypage/zzim.asp';</script> "
	Response.End
End If
End Function




Function Order_Cart_Del(User_SEQ,User_Session,GD_SEQ,ONLINE_CD)
DelSQL = "UPDATE "
DelSQL = DelSQL&"ITEMCENTER.DBO.IC_T_ONLINE_CART "
DelSQL = DelSQL&" SET DEL_YN='Y' "
DelSQL = DelSQL&" WHERE User_SEQ = '" & User_SEQ & "' "
DelSQL = DelSQL&" AND GD_SEQ in (" & GD_SEQ & ") "
If Left(GetsLOGINID,1) = "N" then
	DelSQL = DelSQL&" AND ONLINE_CD = '0050' "
Else
	DelSQL = DelSQL&" AND ONLINE_CD = '0031' "
End If
dbopen()
	DbCon.Execute(DelSQL)
dbclose()
End Function 


Function Product_Name(GD_SEQ)
PNSQL = "SELECT GD_NM FROM ITEMCENTER.DBO.IC_T_GDS_INFO WHERE SEQ = '" & GD_SEQ & "' "
dbopen()
	Set PNRs = Dbcon.Execute(PNSQL)
dbclose()
Product_Name = PNRs("GD_NM")
'Response.write PNSQL
End Function



Function Product_Option(GD_SEQ)
PNSQL1 = "SELECT GD_SIZE FROM ITEMCENTER.DBO.IC_T_GDS_INFO WHERE SEQ = '" & GD_SEQ & "' "
dbopen()
	Set PNRo = Dbcon.Execute(PNSQL1)
dbclose()
Product_Option = PNRo("GD_SIZE")
'Response.write PNSQL
End Function

Function mobile_order_link(User_SEQ)

	DelSQL = "		select	"
	DelSQL = DelSQL&"  	a.or_num,	"
	DelSQL = DelSQL&"  	sum(B.gd_price) as gd_price,	"
	DelSQL = DelSQL&"  	isnull(c.cnt,0) as cnt ,	"
	DelSQL = DelSQL&"  	isnull(SUM(PY_IN_PAY),0) AS PY_IN_PAY ,"
	DelSQL = DelSQL&"  	replace(MAX(GD_MEMO),'＆','&') AS GD_MEMO"
	DelSQL = DelSQL&"  	,a.or_in_com_seq"
	DelSQL = DelSQL&"  from ic_t_order_gd a	"
	DelSQL = DelSQL&"  inner join ic_t_order_gd_sub b	"
	DelSQL = DelSQL&"  on a.or_num = b.or_num	"
	DelSQL = DelSQL&"  left outer join (select order_gd_seq	"
	DelSQL = DelSQL&"  					  , count(*) as cnt 	"
	DelSQL = DelSQL&"  					  , SUM(PY_IN_PAY) AS  PY_IN_PAY 	"
	DelSQL = DelSQL&"  					  from  itemcenter.dbo.IC_T_ORDER_PAY 	"
	DelSQL = DelSQL&"  					  group by order_gd_seq ) c	"
	DelSQL = DelSQL&"  on a.seq = c.ORDER_GD_SEQ	"
	DelSQL = DelSQL&"  LEFT OUTER JOIN (SELECT  GD_MEMO, SEQ FROM IC_T_GDS_INFO WHERE GD_GRP_FIRST= 'Z8' AND GD_GRP_MID = '006') D"
	DelSQL = DelSQL&"  ON B.GD_SEQ = D.SEQ"
	DelSQL = DelSQL&"  where or_in_com_seq = '"&User_SEQ&"'"
	DelSQL = DelSQL&"  and gd_memo <> ''"
	DelSQL = DelSQL&"  and a.del_yn = 'N' and b.del_yn = 'N' "
	DelSQL = DelSQL&"  group by a.or_num ,c.cnt ,or_in_com_seq order by min(a.seq) desc "
 
	dbopen()
	Set PNRo = Dbcon.Execute(DelSQL)
	
	
	If Not(PNRo.Eof Or PNRo.Bof) Then
			
				mobile_order_link = PNRo("GD_MEMO")
			
		Else
			'데이터가 없다면 강제로 첫레코드 값을 NODATA로 해서 GetRows()에 담는다
				mobile_order_link =""

		End If

dbclose()
'Response.write PNSQL
End Function


Function Cart_Copy(User_SEQ,User_Session,ONLINE_CD,Cart_SEQ,GD_SEQ)
'기존에 장바구니에 담긴 상품인지 체크
Chk_SQL = "SELECT COUNT(GD_SEQ) FROM ITEMCENTER.DBO.IC_T_ONLINE_CART "

If User_SEQ <> "" Then
	'로그인한 회원일 경우 회원 SEQ로 검색
	Chk_SQL = Chk_SQL&" WHERE User_Seq = '" & User_SEQ & "' "
Else
	'비로그인인 경우 Session.SessionID로 검색
	Chk_SQL = Chk_SQL&" WHERE User_Session = '" & User_Session & "' "
End If
Chk_SQL = Chk_SQL&" AND GD_SEQ = '" & GD_SEQ & "' "
If Left(GetsLOGINID,1) = "N" then
	Chk_SQL = Chk_SQL&" AND ONLINE_CD = '0050'"
Else
	Chk_SQL = Chk_SQL&" AND ONLINE_CD = '0031'"
End If
Chk_SQL = Chk_SQL&" AND DEL_YN = 'N' "
Chk_SQL = Chk_SQL&" AND Sub_Gubun = '' "

dbopen()
	Set CartCount = Dbcon.Execute(Chk_SQL)
dbclose()

If CartCount(0)  = 0 Then
	InSQL = "INSERT INTO IC_T_ONLINE_CART SELECT USER_SEQ,USER_SESSION,GD_SEQ,ONLINE_CD,GD_EA,ST_SEQ,WRAP_SEQ,getdate(),'N','"&Request.ServerVariables("remote_addr")&"',sub_gubun FROM IC_T_ONLINE_Zzim WHERE IDX='"&Cart_SEQ&"'"

	dbopen()
		Dbcon.Execute(InSQL)
	dbclose()
	Response.Write "<script>if(confirm('장바구니에 상품이 저장되었습니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp';}</script>"
	Response.End
Else
	Response.Write "<script>if(confirm('이미 장바구니에 담겨있는 상품입니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp';}</script>"
	Response.End
End If

End Function



Function Product_View_COUNT(OG_SEQ)
	USQL = "UPDATE ITEMCENTER.DBO.IC_T_ONLINE_GDS SET View_Cnt = View_Cnt + 1 WHERE SEQ = '" & OG_SEQ & "' "
	dbopen()
		Dbcon.Execute(USQL)
	dbclose()
End Function



'인젝션 처리
Function fInject(argData)
  Dim strCheckArgSQL
  Dim arrSQL
  Dim i

	strCheckArgSQL = LCase(Trim(argData))
	''arrSQL = Array("'",";","--","exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","and ","or ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(")
	arrSQL = Array(";","--","exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt")

	For i=0 To ubound(arrSQL) Step 1
		If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
					arrSQL(i) ="홑따옴표"
			   Case "char("
					arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If

		If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
			   Select Case  arrSQL(i)
			   Case "'"
				arrSQL(i) ="홑따옴표"
			   Case "char("
				arrSQL(i) ="char"
			   End SELECT
			response.write "<SCRIPT LANGUAGE='JavaScript'>"
			response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
			response.write "  history.go(-1);"
			response.write "</SCRIPT>"
			response.end
		End If
	Next

	'Xss 필터링
	'argData = Replace(argData,"&","&amp;")
	argData = Replace(argData,"\","&quot;")
	argData = Replace(argData,"<","&lt;")
	argData = Replace(argData,">","&gt;")
	argData = Replace(argData,"'","&#39;")
	argData = Replace(argData,"""","&#34;")

  fInject = argData
End Function



Function Product_List_Search(ONLINE_CD,Site_Cate_Code,GD_TP,GD_GRP_BRAND,GD_NM,ORDER_TYPE,NowPage,ViewCnt,PriceType)

		If NowPage = "" Then
			NowPage = "1"
		End If

		'상품 가격대별 검색===========================================================================================
		If PriceType <> "" Then
			If PriceType = "1" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990 "
			ElseIf PriceType = "2" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990 "
			ElseIf PriceType = "3" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990 "
			ElseIf PriceType = "4" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990 "
			ElseIf PriceType = "5" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990 "
			ElseIf PriceType = "6" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990 "
			ElseIf PriceType = "7" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990 "
			ElseIf PriceType = "8" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990 "
			ElseIf PriceType = "9" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990 "
			ElseIf PriceType = "10" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990 "
			ElseIf PriceType = "11" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 300000 "
			ElseIf PriceType = "12" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000 "
			ElseIf PriceType = "13" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000 "
			ElseIf PriceType = "14" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000 "
			ElseIf PriceType = "15" Then
				PriceType_SQL = PriceType_SQL&" AND ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 200000 "

			Else


				PriceType_SQL = ""
			End If
		End If
		'Response.Write PriceType_SQL
		'Response.End
		'상품 가격대별 검색===========================================================================================

		'최종 검색해서 나오는 결과 필드
		P_List_SQL = "SELECT TOP " & ViewCnt & " PL.GD_NM"
		P_List_SQL = P_List_SQL&" ,PL.GD_PRICE3 "
		P_List_SQL = P_List_SQL&" ,PL.Work_DT "

		P_List_SQL = P_List_SQL&" FROM ("
		'여기가 기본쿼리
		P_List_SQL = P_List_SQL&" SELECT DISTINCT(GI.GD_NM) AS GD_NM " '동일상품명을 제외한 상품명
		'상품가격
		P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 "
		P_List_SQL = P_List_SQL&" FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join ITEMCENTER.DBO.IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "

		If Left(GetsLOGINID,1) = "N" Then
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
		Else
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
		End If

		P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' Order BY GD_PRICE3 ASC ) AS GD_PRICE3 "
		'상품수정일
		P_List_SQL = P_List_SQL&",(SELECT TOP 1 B.Work_DT FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "

		If Left(GetsLOGINID,1) = "N" Then
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
		Else
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
		End If

		P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY Work_DT DESC) AS Work_DT "
		P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.View_Cnt FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "

		If Left(GetsLOGINID,1) = "N" Then
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
		Else
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
		End If

		P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY View_Cnt DESC) AS View_Cnt "
		P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.Sell_Cnt FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "

		If Left(GetsLOGINID,1) = "N" Then
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
		Else
			P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
		End If

		P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY Sell_Cnt DESC) AS Sell_Cnt "
'		P_List_SQL = P_List_SQL&",OG.View_Cnt AS View_Cnt "
'		P_List_SQL = P_List_SQL&",OG.Sell_Cnt AS Sell_Cnt "
		'여기가 기본쿼리 
		P_List_SQL = P_List_SQL&" FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS OG ON GI.SEQ = OG.GD_SEQ "
		P_List_SQL = P_List_SQL&" LEFT OUTER JOIN ITEMCENTER.DBO.IC_T_HP_CATEGORY A ON GI.GD_GRP_FIRST = A.GD_GRP_FIRST AND GI.GD_GRP_MID = A.GD_GRP_MID AND GI.GD_GRP_DETL = A.GD_GRP_DETL "
		'============================================================================================================================================
		'조건절
		P_List_SQL = P_List_SQL&" WHERE GI.DEL_YN = 'N' AND GI.GD_BUY_END_YN = 'N' AND OG.DEL_YN = 'N' "

		'온라인몰코드
		If Left(GetsLOGINID,1) = "N" Then
			P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0050' "
			P_LIST_SQL = P_List_SQL&" AND A.ONLINE_CD = '0050' "
		Else
			P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0031' "
			P_LIST_SQL = P_List_SQL&" AND A.ONLINE_CD = '0031' "
		End If

		'대분류검색
		'If GD_GRP_FIRST <> "" Then
		  P_LIST_SQL = P_LIST_SQL &" AND A.HP_FIST_NUM IN (" & Site_Cate_Code & ")"
		'End If

		'중분류검색
		'If GD_GRP_MID <> "" Then

			'If InStr(GD_GRP_MID,"'") < 1 Then
				'Page_GD_GRP_MID = "'" & Replace(GD_GRP_MID,",","','") & "'"
			'Else
				'Page_GD_GRP_MID = GD_GRP_MID
			'End If

			'P_LIST_SQL = P_LIST_SQL &" AND GI.GD_GRP_MID IN ("&Page_GD_GRP_MID&")"
		'End If

		'소분류 코드 검색
		'If GD_GRP_DETL <> "" Then

			'If InStr(GD_GRP_DETL,"'") < 1 Then
				'Page_GD_GRP_DETL = "'" & Replace(GD_GRP_DETL,",","','") & "'"
			'Else
				'Page_GD_GRP_DETL = GD_GRP_DETL
			'End If

			'P_LIST_SQL = P_LIST_SQL &" AND GI.GD_GRP_DETL IN ("&Page_GD_GRP_DETL&")"
		'End If

		'세세분류 코드 검색
		'If GRP_DETL2 <> "" Then
			'Page_GRP_DETL2 = "'" & Replace(GRP_DETL2,",","','") & "'"
			'P_LIST_SQL = P_LIST_SQL &" AND GI.GD_GRP_DETL2 IN ("&Page_GRP_DETL2&")"
		'End If

		'셋트일반여부
		If GD_TP <> "" Then
			P_List_SQL = P_List_SQL &" AND GI.GD_TP = '" & GD_TP & "' "
		End If

		'브랜드검색
		'If GD_GRP_BRAND <> "" Then
		  'If GD_GRP_BRAND = "etc"  Or GD_GRP_BRAND = "'etc'" Then
				'P_List_SQL = P_List_SQL &" AND GI.GD_GRP_BRAND NOT IN('00055','00050','00818','00033','00028','1381','00062','00038','00003','00065','2345')"
			'Else 
				'P_List_SQL = P_List_SQL &" AND GI.GD_GRP_BRAND IN ("&GD_GRP_BRAND&")"
			'End If
		'End If

		'상품명 검색
		If GD_NM <> "" Then
			P_List_SQL = P_List_SQL &" AND GI.GD_NM LIKE '%" & GD_NM & "%' "
		End If

		'온라인 상품 카테
		If GD_CATE <> "" Then
			P_List_SQL = P_List_SQL &" AND OG.GD_CATE = '" & GD_CATE & "' "
		End If

		'상품 가격대별 검색===========================================================================================
		If PriceType <> "" Then
			If PriceType = "1" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990"
			ElseIf PriceType = "2" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990"
			ElseIf PriceType = "3" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990"
			ElseIf PriceType = "4" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990"
			ElseIf PriceType = "5" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990"
			ElseIf PriceType = "6" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990"
			ElseIf PriceType = "7" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990"
			ElseIf PriceType = "8" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990"
			ElseIf PriceType = "9" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990"
			ElseIf PriceType = "10" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990"
			ElseIf PriceType = "11" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) >300000"
			ElseIf PriceType = "12" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000"
			ElseIf PriceType = "13" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000"
			ElseIf PriceType = "14" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000"
			ElseIf PriceType = "15" Then
				P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) >200000"
			End If
		End If
		'상품 가격대별 검색===========================================================================================

		'============================================================================================================================================
		P_List_SQL = P_List_SQL&" ) PL "
    '여기가 기본쿼리 끝

		'page not in 을 위한 쿼리

		'page not in 을 위한 쿼리 끝

		'PL2정렬조건
		'If ORDER_TYPE = "" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 ASC "
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.Work_DT desc "
		'ElseIf ORDER_TYPE = "Price_ASC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 ASC "
		'ElseIf ORDER_TYPE = "Price_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 DESC "
		'ElseIf ORDER_TYPE = "WORK_DT" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.Work_DT DESC "
		'ElseIf ORDER_TYPE = "View_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.View_Cnt DESC "
		'ElseIf ORDER_TYPE = "Sell_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL2.Sell_Cnt DESC "
		'End If
		'P_List_SQL = P_List_SQL&" ) "

		'PL정렬조건
		'If ORDER_TYPE = "" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.Work_DT desc "
			'P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 ASC "
		'ElseIf ORDER_TYPE = "Price_ASC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 ASC "
		'ElseIf ORDER_TYPE = "Price_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 DESC "
		'ElseIf ORDER_TYPE = "WORK_DT" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.Work_DT DESC "
		'ElseIf ORDER_TYPE = "View_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.View_Cnt DESC "
		'ElseIf ORDER_TYPE = "Sell_DESC" Then
			'P_List_SQL = P_List_SQL&" ORDER BY PL.Sell_Cnt DESC "
		'End If

		'Response.write P_List_SQL

		dbopen()
			Set ListRs = Dbcon.Execute(P_List_SQL)
		dbclose()

		If Not(ListRs.Eof Or ListRs.Bof) Then
			Product_List_Search = ListRs.GetRows()
		Else
			'데이터가 없다면 강제로 첫레코드 값을 NODATA로 해서 GetRows()에 담는다
			dbopen()
			Product_List_Search = Dbcon.Execute("SELECT 'NODATA'").GetRows()
			dbclose()
		End If
End Function



'몰코드,상품명,금액계산타입
Function Product_Search_View(ONLINE_CD,GD_NM,GD_SEQ,PRICE_TYPE,PriceType)
	'상품명으로 검색시 여러 데이터가 나오므로 TOP 1 정렬은 GD_PRICE3 낮은 데이터로 정렬
		P_View_SQL = "SELECT TOP 1 "
		P_View_SQL = P_View_SQL & " GI.SEQ AS SEQ, GI.GD_NM, GI.GD_GRP_BRAND, ITEMCENTER.dbo.IC_FN_BRAND_NM(GI.GD_GRP_BRAND) AS GD_GRP_BRAND_NM, GI.GD_TP "
		'원산지
		P_View_SQL = P_View_SQL & ", GI.GD_GRP_MADE"
		'상품제공업체코드
		P_View_SQL = P_View_SQL & ", GI.UNI_GRP_CD"
		'셋트상품구성
		P_View_SQL = P_View_SQL & ", GI.GD_SET"

		P_View_SQL = P_View_SQL & ", ITEMCENTER.dbo.IC_FN_MADE_NM(GI.GD_GRP_MADE) AS GD_GRP_MADE_NM "

		P_View_SQL = P_View_SQL & ", ITEMCENTER.dbo.IC_FN_DETL_NM(GI.GD_GRP_DETL,GI.GD_GRP_FIRST,GI.GD_GRP_MID) AS GD_GRP_DETL_NM "

		'상품 이미지 
		P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH)) = '' OR GI.GD_IMG_PATH IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
		P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH END GD_IMG_PATH "
		P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_2)) = '' OR GI.GD_IMG_PATH_2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
		P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_2 END GD_IMG_PATH_2 "
		P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_3)) = '' OR GI.GD_IMG_PATH_3 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
		P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_3 END GD_IMG_PATH_3 "
		P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_4)) = '' OR GI.GD_IMG_PATH_4 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
		P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH_4 END GD_IMG_PATH_4 "
		'상세설명 이미지
		P_View_SQL = P_View_SQL & " ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH2)) = '' OR GI.GD_IMG_PATH2 IS NULL THEN 'gdsimage/listgdsnull1.jpg' "
		P_View_SQL = P_View_SQL & " ELSE GI.GD_IMG_PATH2 END GD_IMG_PATH2 "
		'옵션1,2
		P_View_SQL = P_View_SQL & " ,GI.GD_SIZE AS GD_OPTION1 "
		P_View_SQL = P_View_SQL & " ,GI.GD_COLOR AS GD_OPTION2 "

		'===상품 최소 구매수량
		P_View_SQL = P_View_SQL & " ,GI.GD_MIN_VOL AS GD_MIN_VOL "
		'===상품 최소 구매수량
		'===알파메모
		P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_ALPHA_MEMO,'') AS GD_ALPHA_MEMO "
		'===알파메모
		'===온라인 GDS테이블 SEQ
		P_View_SQL = P_View_SQL & " ,OG.SEQ AS OG_SEQ "
		P_View_SQL = P_View_SQL & " ,GI.Catalogue_Code AS Catalogue_Code "

		'===가격 정책에 따른 금액 계산
		If PRICE_TYPE = "" Then
			'P_View_SQL = P_View_SQL & " ,ROUND(ISNULL(GI.GD_PRICE3,0) / ((100-ISNULL(OG.GD_PER,0))/100 )+40,-2) AS GD_PRICE3 "
			'운영회원가 * 1.1
			P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE,0) AS GD_PRICE "
			'P_View_SQL = P_View_SQL & " ,ROUND((ISNULL(GI.GD_PRICE3,0) * 1.1) * 1.1 +40,-2) AS GD_PRICE3 "
			'기존 쿼리
			'P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) * 1.1 ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) * 1.1 END,0) AS Sell_Price "
			'P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) 'ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 "
			
			'2017.10.18 수정
			P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
			P_View_SQL = P_View_SQL & " ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Sell_Price "

			'2017.10.18 수정
			P_View_SQL = P_View_SQL & " ,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) "
			P_View_SQL = P_View_SQL & " ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) GD_PRICE3 "

			P_View_SQL = P_View_SQL & " ,ISNULL(GI.GD_PRICE7,0) AS GD_PRICE7 "
			P_View_SQL = P_View_SQL & " ,GI.GD_PRICE8 AS GD_PRICE8 "
		End If
		'===가격 정책에 따른 금액 계산
		'스티커사용여부
		P_View_SQL = P_View_SQL & " ,OG.ST_YN AS ST_YN "
		'포장지사용여부
		P_View_SQL = P_View_SQL & " ,OG.WRAP_YN AS WRAP_YN "
		'박스사용여부
		P_View_SQL = P_View_SQL & " ,OG.RIBON_YN AS RIBON_YN "
		'부가세포함여부
		P_View_SQL = P_View_SQL & " ,OG.GD_TAX_YN AS GD_TAX_YN "
		'마진율
		P_View_SQL = P_View_SQL & " ,OG.GD_PER AS GD_PER "
		'상품대분류
		P_View_SQL = P_View_SQL & " ,GI.GD_GRP_FIRST AS GD_GRP_FIRST "
		'상품중분류
		P_View_SQL = P_View_SQL & " ,GI.GD_GRP_MID AS GD_GRP_MID "
		'상품소분류
		P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL AS GD_GRP_DETL "
		'상품세세분류
		P_View_SQL = P_View_SQL & " ,GI.GD_GRP_DETL2 AS GD_GRP_DETL2 "
		'GD_CATE
		P_View_SQL = P_View_SQL & " ,OG.GD_CATE AS GD_CATE "
		'면세상품여부
		P_View_SQL = P_View_SQL & " ,GI.GD_Tax_Free_YN AS Tax_Free "
		'일시품절여부
		P_View_SQL = P_View_SQL & " ,GI.GD_OPEN_YN AS GD_OPEN_YN "
		P_View_SQL = P_View_SQL & " FROM ITEMCENTER.dbo.IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
		P_View_SQL = P_View_SQL & " ON GI.SEQ = OG.GD_SEQ "
		P_View_SQL = P_View_SQL & " WHERE GI.DEL_YN = 'N' "

		'몰코드
		If Left(GetsLOGINID,1) = "N" Then
			P_View_SQL = P_View_SQL&"   AND OG.ONLINE_CD = '0050' "
		Else
			P_View_SQL = P_View_SQL&"   AND OG.ONLINE_CD = '0031' "
		End If

		P_View_SQL = P_View_SQL & " AND GI.GD_BUY_END_YN = 'N' "
		'P_View_SQL = P_View_SQL & " AND GI.GD_OPEN_YN = 'N' "
		P_View_SQL = P_View_SQL & " AND OG.DEL_YN = 'N' "
		P_View_SQL = P_View_SQL & " AND OG.UNI_GRP_CD = '2001' "

		'상품명 검색
		If GD_NM <> "" Then
			P_View_SQL = P_View_SQL & " AND GI.GD_NM = '" & GD_NM & "' "
		Else
		'상품명이 없다면 SEQ값이 있는걸로 간주함
			P_View_SQL = P_View_SQL & " AND GI.SEQ = '" & GD_SEQ & "' "
		End If

		'상품 가격대별 검색===========================================================================================
		If PriceType <> "" Then
			If PriceType = "1" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990"
			ElseIf PriceType = "2" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990"
			ElseIf PriceType = "3" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990"
			ElseIf PriceType = "4" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990"
			ElseIf PriceType = "5" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990"
			ElseIf PriceType = "6" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990"
			ElseIf PriceType = "7" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990"
			ElseIf PriceType = "8" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990"
			ElseIf PriceType = "9" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990"
			ElseIf PriceType = "10" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990"
			ElseIf PriceType = "11" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) >300000"
			ElseIf PriceType = "12" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000"
			ElseIf PriceType = "13" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000"
			ElseIf PriceType = "14" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000"
			ElseIf PriceType = "15" Then
				P_View_SQL = P_View_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 200000"
			End If
		End If
		'상품 가격대별 검색===========================================================================================
		'P_View_SQL = P_View_SQL &" ORDER BY GD_PRICE3"
		P_View_SQL = P_View_SQL & " ORDER BY GI.GD_OPEN_YN ASC, GI.GD_PRICE3 ASC, GI.SEQ ASC "
		'Response.Write P_View_SQL
		dbopen()
			Product_Search_View = Dbcon.Execute(P_View_SQL)
		dbclose()

End Function



Function Product_List_Search_Text(ONLINE_CD,GD_GRP_FIRST,GD_GRP_MID,GD_GRP_DETL,GRP_DETL2,GD_TP,GD_GRP_BRAND,GD_NM,GD_CATE,ORDER_TYPE,NowPage,ViewCnt,PriceType,P_Type)
if NowPage = "" Then
	NowPage = "1"
End If 
'최종 검색해서 나오는 결과 필드
P_List_SQL = "SELECT TOP "&ViewCnt&" PL.GD_NM "
P_List_SQL = P_List_SQL&" ,PL.GD_PRICE3 "
P_List_SQL = P_List_SQL&" ,PL.Work_DT "
P_List_SQL = P_List_SQL&" FROM ( "
'여기가 기본쿼리 
P_List_SQL = P_List_SQL&" SELECT DISTINCT(GI.GD_NM) AS GD_NM " '동일상품명을 제외한 상품명
'상품가격
P_List_SQL = P_List_SQL&",(SELECT TOP 1 ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 FROM IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ=B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY GD_PRICE3 ASC) AS GD_PRICE3 "
'상품수정일
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.Work_DT FROM IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN='N' AND A.GD_BUY_END_YN='N' AND B.DEL_YN='N' ORDER BY Work_DT DESC) AS Work_DT "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.View_Cnt FROM IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN='N' AND A.GD_BUY_END_YN='N' AND B.DEL_YN='N' ORDER BY View_Cnt DESC) AS View_Cnt "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.Sell_Cnt FROM IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN='N' AND A.GD_BUY_END_YN='N' AND B.DEL_YN='N' ORDER BY Sell_Cnt DESC) AS Sell_Cnt "
'여기가 기본쿼리
P_List_SQL = P_List_SQL&" FROM IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG ON GI.SEQ = OG.GD_SEQ "
'============================================================================================================================================
'조건절
P_List_SQL = P_List_SQL&" WHERE GI.DEL_YN = 'N' AND GI.GD_BUY_END_YN = 'N' AND OG.DEL_YN = 'N' "

'포장지 제외
P_List_SQL = P_List_SQL&" AND GI.SEQ NOT IN (SELECT GD_SEQ FROM IC_T_ONLINE_LABEL)"

'온라인몰코드
If Left(GetsLOGINID,1) = "N" then
	P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0050' "
Else
	P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0031' "
End If

'대분류검색
If GD_GRP_FIRST <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_FIRST IN ("&GD_GRP_FIRST&") "
End If

'중분류검색
If GD_GRP_MID <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_MID IN ("&GD_GRP_MID&") "
End If

'소분류 코드 검색
If GD_GRP_DETL <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_DETL IN ("&GD_GRP_DETL&") "
End If

'세세분류 코드 검색
If GRP_DETL2 <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_DETL2 IN ("&GRP_DETL2&") "
End If

'셋트일반여부
If GD_TP <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_TP = '"&GD_TP&"' "
End If

'브랜드검색
If GD_GRP_BRAND <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_BRAND = '"&GD_GRP_BRAND&"' "
End If

'상품명 검색
If GD_NM <> "" Then
	If S_Type = "P" Then
		P_List_SQL = P_List_SQL &" AND GI.GD_NM LIKE '%"&Replace(GD_NM,"[","[[]")&"%' "
	ElseIf S_Type = "C" Then
		P_List_SQL = P_List_SQL &" AND GI.SEQ = '"&GD_NM&"' "
	End If
End If

'온라인 상품 카테고리
If GD_CATE <> "" Then
		P_List_SQL = P_List_SQL&" AND OG.GD_CATE = '"&GD_CATE&"' "
End If

'상품 가격대별 검색===========================================================================================
If PriceType<>"" Then
	If PriceType = "1" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990 "
	ElseIf PriceType = "2" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990 "
	ElseIf PriceType = "3" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990 "
	ElseIf PriceType = "4" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990 "
	ElseIf PriceType = "5" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990 "
	ElseIf PriceType = "6" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990 "
	ElseIf PriceType = "7" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990 "
	ElseIf PriceType = "8" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990 "
	ElseIf PriceType = "9" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990 "
	ElseIf PriceType = "10" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990 "
	ElseIf PriceType = "11" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 300000 "
	ElseIf PriceType = "12" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000 "
	ElseIf PriceType = "13" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000 "
	ElseIf PriceType = "14" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000 "
	ElseIf PriceType = "15" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 200000 "
	End If
End If

'상품 가격대별 검색===========================================================================================

P_List_SQL = P_List_SQL&" ) PL "
'여기가 기본쿼리 끝

'page not in 을 위한 쿼리
P_List_SQL = P_List_SQL&" WHERE PL.GD_NM NOT IN ( "

'(NowPage-1)*ViewCnt
P_List_SQL = P_List_SQL&" SELECT TOP "&(NowPage-1)*ViewCnt&" PL2.GD_NM "
P_List_SQL = P_List_SQL&" FROM ( "

'상위PL검색과 같은 항목
P_List_SQL = P_List_SQL&" SELECT DISTINCT(GI.GD_NM) AS GD_NM "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 FROM IC_T_GDS_INFO A Join IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End If

P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY GD_PRICE3 ASC) AS GD_PRICE3 "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.Work_DT FROM IC_T_GDS_INFO A Join ITEMCENTER.DBO.IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY Work_DT DESC) AS Work_DT "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.View_Cnt FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join ITEMCENTER.DBO.IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY View_Cnt DESC) AS View_Cnt "
P_List_SQL = P_List_SQL&" ,(SELECT TOP 1 B.Sell_Cnt FROM ITEMCENTER.DBO.IC_T_GDS_INFO A Join ITEMCENTER.DBO.IC_T_Online_GDS B ON A.SEQ = B.GD_SEQ WHERE A.GD_NM = GI.GD_NM "
If Left(GetsLOGINID,1) = "N" then
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0050' "
Else
	P_List_SQL = P_List_SQL&" AND B.ONLINE_CD = '0031' "
End if
P_List_SQL = P_List_SQL&" AND A.DEL_YN = 'N' AND A.GD_BUY_END_YN = 'N' AND B.DEL_YN = 'N' ORDER BY Sell_Cnt DESC) AS Sell_Cnt "
'============================================================================================================================================
'테이블조인
P_List_SQL = P_List_SQL&" FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS OG ON GI.SEQ = OG.GD_SEQ "

'조건절
P_List_SQL = P_List_SQL&" WHERE GI.DEL_YN = 'N' AND GI.GD_BUY_END_YN = 'N' AND OG.DEL_YN = 'N' "

'포장지 제외
P_List_SQL = P_List_SQL&" AND GI.SEQ NOT IN (SELECT GD_SEQ FROM IC_T_ONLINE_LABEL) "

'온라인몰코드
If Left(GetsLOGINID,1) = "N" then
	P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0050' "
Else
	P_LIST_SQL = P_List_SQL&" AND OG.ONLINE_CD = '0031' "
End If

'대분류검색
If GD_GRP_FIRST <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_FIRST IN (" & GD_GRP_FIRST & ") "
End If

'중분류검색
If GD_GRP_MID <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_MID IN (" & GD_GRP_MID & ") "
End If

'소분류 코드 검색
If GD_GRP_DETL <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_DETL IN (" & GD_GRP_DETL & ") "
End If 

'세세분류 코드 검색
If GRP_DETL2 <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_DETL2 IN (" & GRP_DETL2 & ") "
End If

'셋트일반여부
If GD_TP <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_TP = '" & GD_TP & "' "
End If

'브랜드검색
If GD_GRP_BRAND <> "" Then
	P_List_SQL = P_List_SQL &" AND GI.GD_GRP_BRAND = '" & GD_GRP_BRAND & "' "
End If

'상품명 검색
If GD_NM <> "" Then
	If S_Type = "P" Then
		P_List_SQL = P_List_SQL &" AND GI.GD_NM LIKE '%"&Replace(GD_NM,"[","[[]")&"%' "
	ElseIf S_Type = "C" Then
		P_List_SQL = P_List_SQL &" AND GI.SEQ = '" & GD_NM & "' "
	End If
End If

'온라인 상품 카테
If GD_CATE <> "" Then 
	P_List_SQL = P_List_SQL&" AND OG.GD_CATE = '" & GD_CATE & "' "
End If

'상품 가격대별 검색===========================================================================================
If PriceType<>"" Then
	If PriceType = "1" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990 "
	ElseIf PriceType = "2" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990 "
	ElseIf PriceType = "3" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990 "
	ElseIf PriceType = "4" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990 "
	ElseIf PriceType = "5" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990 "
	ElseIf PriceType = "6" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990 "
	ElseIf PriceType = "7" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990 "
	ElseIf PriceType = "8" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990 "
	ElseIf PriceType = "9" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990 "
	ElseIf PriceType = "10" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990 "
	ElseIf PriceType = "11" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 300000 "
	ElseIf PriceType = "12" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000 "
	ElseIf PriceType = "13" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000 "
	ElseIf PriceType = "14" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000 "
	ElseIf PriceType = "15" Then
		P_List_SQL = P_List_SQL&" AND ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 200000 "
	End If
End If
'상품 가격대별 검색===========================================================================================

P_List_SQL = P_List_SQL&" )PL2 "

'page not in 을 위한 쿼리 끝

'PL2정렬조건
If ORDER_TYPE = "" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 ASC "
ElseIf ORDER_TYPE = "Price_ASC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 ASC "
ElseIf ORDER_TYPE = "Price_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.GD_PRICE3 DESC "
ElseIf ORDER_TYPE = "WORK_DT" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.Work_DT DESC "
ElseIf ORDER_TYPE = "View_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.View_Cnt DESC "
ElseIf ORDER_TYPE = "Sell_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL2.Sell_Cnt DESC "
End If
P_List_SQL = P_List_SQL&" )"

'PL정렬조건
If ORDER_TYPE = "" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 ASC "
ElseIf ORDER_TYPE = "Price_ASC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 ASC "
ElseIf ORDER_TYPE = "Price_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.GD_PRICE3 DESC "
ElseIf ORDER_TYPE = "WORK_DT" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.Work_DT DESC "
ElseIf ORDER_TYPE = "View_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.View_Cnt DESC "
ElseIf ORDER_TYPE = "Sell_DESC" Then
	P_List_SQL = P_List_SQL&" ORDER BY PL.Sell_Cnt DESC "
End If
'Response.write P_List_SQL
'Response.End

dbopen()
	Set ListRs = Dbcon.Execute(P_List_SQL)
dbclose()

If Not(ListRs.Eof Or ListRs.Bof) Then
	Product_List_Search_Text = ListRs.GetRows()
Else
	'데이터가 없다면 강제로 첫레코드 값을 NODATA로 해서 GetRows()에 담는다
	dbopen()
		Product_List_Search_Text = Dbcon.Execute("SELECT 'NODATA'").GetRows()
	dbclose()
End If
End Function



Function Cart_Option_Change(Cart_SEQ,Change_GD_SEQ)
		UpSQL = "UPDATE "
		If Tb_Type = "Cart" Then 
			UpSQL = UpSQL&"IC_T_ONLINE_CART "
		Else
			UpSQL = UpSQL&"IC_T_ONLINE_Zzim "
		End If			
		UpSQL = UpSQL&" SET GD_SEQ='"&Change_GD_SEQ&"' "
		UpSQL = UpSQL&" WHERE IDX = '"&Cart_SEQ&"'"
'Response.Write UpSQL
'Response.End
		DbCon.Execute(UpSQL)

			Response.Write "<script>parent.location.href='cart.asp';</script>"
			Response.End
End Function 


Function Product_TotalCount(HP_FIST_NUM,MID_NM,DETL_NM)

		P_Count_SQL = " SELECT COUNT(PL.GD_NM) AS PL_Count "
		P_Count_SQL = P_Count_SQL &" FROM (SELECT DISTINCT(A.GD_NM) AS GD_NM "
		P_Count_SQL = P_Count_SQL &" 			 FROM ITEMCENTER.DBO.IC_T_GDS_INFO A "
		P_Count_SQL = P_Count_SQL &" 			 LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS B ON A.SEQ = B.GD_SEQ "
		P_Count_SQL = P_Count_SQL &" 			 LEFT JOIN ITEMCENTER.dbo.IC_T_HP_CATEGORY C ON C.GD_GRP_FIRST = A.GD_GRP_FIRST AND C.GD_GRP_MID = A.GD_GRP_MID AND C.GD_GRP_DETL = A.GD_GRP_DETL "
		P_Count_SQL = P_Count_SQL &" WHERE A.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND B.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND C.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND A.GD_BUY_END_YN = 'N' "
		'P_Count_SQL = P_Count_SQL &" AND A.GD_OPEN_YN = 'N' "
		'P_Count_SQL = P_Count_SQL &" AND B.GD_OPEN_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND B.UNI_GRP_CD = '2001' "

		'온라인몰코드
		If Left(GetsLOGINID,1) = "N" then
			P_Count_SQL = P_Count_SQL &" AND B.ONLINE_CD = '0050' "
			P_Count_SQL = P_Count_SQL &" AND C.ONLINE_CD = '0050' "
		Else
			P_Count_SQL = P_Count_SQL &" AND B.ONLINE_CD = '0031' "
			P_Count_SQL = P_Count_SQL &" AND C.ONLINE_CD = '0031' "
		End If

		P_Count_SQL = P_Count_SQL &" AND C.HP_FIST_NUM = '" & HP_FIST_NUM & "' "

		If MID_NM <> "" Then
			P_Count_SQL = P_Count_SQL &" AND C.MID_NM = '" & MID_NM & "' "
		End If

		If DETL_NM <> "" then
			P_Count_SQL = P_Count_SQL &" AND C.DETL_NM = '" & DETL_NM & "' "
		End If

		P_Count_SQL = P_Count_SQL &") PL "

    '여기가 기본쿼리 끝
		'Response.Write P_Count_SQL
		'Response.End
		dbopen()
			Set CountRs = DBcon.Execute(P_Count_SQL)
		dbclose()
		If Not(CountRs.Eof Or CountRs.Bof) Then
			Product_TotalCount = CountRs("PL_Count")
		End If
End Function



Function Product_TotalCount_PriceSearch(HP_FIST_NUM,MID_NM,DETL_NM,PriceType)

		P_Count_SQL = " SELECT COUNT(PL.GD_NM) AS PL_Count "
		P_Count_SQL = P_Count_SQL &" FROM (SELECT DISTINCT(A.GD_NM) AS GD_NM "
		P_Count_SQL = P_Count_SQL &" 			 FROM ITEMCENTER.DBO.IC_T_GDS_INFO A "
		P_Count_SQL = P_Count_SQL &" 			 LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS B ON A.SEQ = B.GD_SEQ "
		P_Count_SQL = P_Count_SQL &" 			 LEFT JOIN ITEMCENTER.dbo.IC_T_HP_CATEGORY C ON C.GD_GRP_FIRST = A.GD_GRP_FIRST AND C.GD_GRP_MID = A.GD_GRP_MID AND C.GD_GRP_DETL = A.GD_GRP_DETL "
		P_Count_SQL = P_Count_SQL &" WHERE A.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND B.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND C.DEL_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND A.GD_BUY_END_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND A.GD_OPEN_YN = 'N' "
		'P_Count_SQL = P_Count_SQL &" AND B.GD_OPEN_YN = 'N' "
		P_Count_SQL = P_Count_SQL &" AND B.UNI_GRP_CD = '2001' "

		'온라인몰코드
		If Left(GetsLOGINID,1) = "N" then
			P_Count_SQL = P_Count_SQL &" AND B.ONLINE_CD = '0050' "
			P_Count_SQL = P_Count_SQL &" AND C.ONLINE_CD = '0050' "
		Else
			P_Count_SQL = P_Count_SQL &" AND B.ONLINE_CD = '0031' "
			P_Count_SQL = P_Count_SQL &" AND C.ONLINE_CD = '0031' "
		End If

		P_Count_SQL = P_Count_SQL &" AND C.HP_FIST_NUM IN ('" & Replace(Replace(HP_FIST_NUM," ",""),",","','") & "') "

		If MID_NM <> "" Then
			P_Count_SQL = P_Count_SQL &" AND C.MID_NM = '" & MID_NM & "' "
		End If

		If DETL_NM <> "" then
			P_Count_SQL = P_Count_SQL &" AND C.DETL_NM = '" & DETL_NM & "' "
		End If

		If PriceType <> "" Then
			If PriceType = "1" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 1 AND 990 "
			ElseIf PriceType = "2" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 1000 AND 4990 "
			ElseIf PriceType = "3" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 5000 AND 9990 "
			ElseIf PriceType = "4" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 10000 AND 19990 "
			ElseIf PriceType = "5" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 20000 AND 29990 "
			ElseIf PriceType = "6" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 30000 AND 49990 "
			ElseIf PriceType = "7" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 50000 AND 69990 "
			ElseIf PriceType = "8" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 70000 AND 99990 "
			ElseIf PriceType = "9" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 100000 AND 199990 "
			ElseIf PriceType = "10" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 200000 AND 299990 "
			ElseIf PriceType = "11" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) > 300000 "
			ElseIf PriceType = "12" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 50000 AND 100000 "
			ElseIf PriceType = "13" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 100000 AND 150000 "
			ElseIf PriceType = "14" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) Between 150000 AND 200000 "
			ElseIf PriceType = "15" Then
				P_Count_SQL = P_Count_SQL&" AND ((round(ISNULL(CASE WHEN ISNULL(B.GD_TAX_YN,'N') = 'N' THEN ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) ELSE (ISNULL(A.GD_PRICE3,0)/((100-ISNULL(B.GD_PER,0))/100) * 1.1) END,0) + 40 ,-2)) * GD_MIN_VOL) > 200000 "
			End If
		End If

		P_Count_SQL = P_Count_SQL &") PL "

    '여기가 기본쿼리 끝
		'Response.Write P_Count_SQL
		'Response.End
		dbopen()
			Set CountRs = DBcon.Execute(P_Count_SQL)
		dbclose()
		If Not(CountRs.Eof Or CountRs.Bof) Then
			Product_TotalCount_PriceSearch = CountRs("PL_Count")
		End If
End Function



Function Product_TotalCount_Search_Text(ONLINE_CD,GD_GRP_FIRST,GD_GRP_MID,GD_GRP_DETL,GRP_DETL2,GD_TP,GD_GRP_BRAND,GD_NM,GD_CATE,ORDER_TYPE,NowPage,ViewCnt,PriceType,S_Type)

P_Count_SQL = "SELECT COUNT(PL.GD_NM) AS PL_Count "
P_Count_SQL = P_Count_SQL&" FROM ( SELECT DISTINCT(GI.GD_NM) AS GD_NM "
P_Count_SQL = P_Count_SQL&" FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS OG ON GI.SEQ = OG.GD_SEQ "
'============================================================================================================================================
'조건절
P_Count_SQL = P_Count_SQL&" WHERE GI.DEL_YN = 'N' AND GI.GD_BUY_END_YN = 'N' AND OG.DEL_YN = 'N' AND OG.UNI_GRP_CD = '2001' "

'포장지 제외
P_Count_SQL = P_Count_SQL&" AND GI.SEQ NOT IN (SELECT GD_SEQ FROM ITEMCENTER.DBO.IC_T_ONLINE_LABEL)"

'온라인몰코드
If Left(GetsLOGINID,1) = "N" then
	P_Count_SQL = P_Count_SQL&" AND OG.ONLINE_CD = '0050' "
Else
	P_Count_SQL = P_Count_SQL&" AND OG.ONLINE_CD = '0031' "
End If

'대분류검색
If GD_GRP_FIRST <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_FIRST IN (" & GD_GRP_FIRST & ") "
End If

'중분류검색
If GD_GRP_MID <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_MID IN (" & GD_GRP_MID & ") "
End If

'소분류 코드 검색
If GD_GRP_DETL <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_DETL IN (" & GD_GRP_DETL & ") "
End If

'세세분류 코드 검색
If GRP_DETL2 <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_DETL2 IN (" & GRP_DETL2 & ") "
End If

'셋트일반여부
If GD_TP <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_TP = '" & GD_TP & "' "
End If

'브랜드검색
If GD_GRP_BRAND <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_BRAND = '" & GD_GRP_BRAND & "' "
End If

'상품명 검색
If GD_NM <> "" Then
	If S_Type = "P" Then
		P_Count_SQL = P_Count_SQL &" AND GI.GD_NM LIKE '%" & GD_NM & "%' "
	ElseIf S_Type = "C" Then
		P_Count_SQL = P_Count_SQL &" AND GI.SEQ = '" & GD_NM & "' "
	End If
End If

'온라인 상품 카테고리
If GD_CATE <> "" Then
	P_Count_SQL = P_Count_SQL&" AND OG.GD_CATE = '" & GD_CATE & "' "
End If

'상품 가격대별 검색===========================================================================================
If PriceType<>"" Then
	If PriceType = "1" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990 "
	ElseIf PriceType = "2" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990 "
	ElseIf PriceType = "3" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990 "
	ElseIf PriceType = "4" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990 "
	ElseIf PriceType = "5" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990 "
	ElseIf PriceType = "6" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990 "
	ElseIf PriceType = "7" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990 "
	ElseIf PriceType = "8" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990 "
	ElseIf PriceType = "9" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990 "
	ElseIf PriceType = "10" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990 "
	ElseIf PriceType = "11" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 300000 "
	ElseIf PriceType = "12" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000 "
	ElseIf PriceType = "13" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000 "
	ElseIf PriceType = "14" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000 "
	ElseIf PriceType = "15" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) > 200000 "
	End If
End If

'상품 가격대별 검색===========================================================================================
'============================================================================================================================================
P_Count_SQL = P_Count_SQL&" ) PL "
'여기가 기본쿼리 끝
'Response.Write P_Count_SQL
'Response.End
dbopen()
	Set CountRs = DBcon.Execute(P_Count_SQL)
dbclose()

If Not(CountRs.Eof Or CountRs.Bof) Then
	Product_TotalCount_Search_Text = CountRs("PL_Count")
End If
End Function



Function Product_TotalCount_Search(ONLINE_CD,GD_GRP_FIRST,GD_GRP_MID,GD_GRP_DETL,GRP_DETL2,GD_TP,GD_GRP_BRAND,GD_NM,GD_CATE,ORDER_TYPE,NowPage,ViewCnt,PriceType)

P_Count_SQL = "SELECT COUNT(PL.GD_NM) AS PL_Count "
P_Count_SQL = P_Count_SQL&" FROM ( SELECT DISTINCT(GI.GD_NM) AS GD_NM "
P_Count_SQL = P_Count_SQL&" FROM ITEMCENTER.DBO.IC_T_GDS_INFO GI LEFT JOIN ITEMCENTER.DBO.IC_T_ONLINE_GDS OG ON GI.SEQ = OG.GD_SEQ "
'============================================================================================================================================
'조건절
P_Count_SQL = P_Count_SQL&" WHERE GI.DEL_YN = 'N' AND GI.GD_BUY_END_YN = 'N' AND OG.DEL_YN = 'N' AND OG.UNI_GRP_CD = '2001' "

'온라인몰코드
If Left(GetsLOGINID,1) = "N" then
	P_Count_SQL = P_Count_SQL&" AND OG.ONLINE_CD = '0050' "
Else
	P_Count_SQL = P_Count_SQL&" AND OG.ONLINE_CD = '0031' "
End If

'대분류검색
If GD_GRP_FIRST <> "" Then
	P_Count_SQL = P_Count_SQL &"    AND GI.GD_GRP_FIRST IN (" & GD_GRP_FIRST & ") "
End If

'중분류검색
If GD_GRP_MID <> "" Then
	If InStr(GD_GRP_MID,"'") < 1 Then
		Page_GD_GRP_MID = "'" & Replace(GD_GRP_MID,",","','") & "' "
	Else
		Page_GD_GRP_MID = GD_GRP_MID
	End If
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_MID IN (" & Page_GD_GRP_MID & ") "
End If
'소분류 코드 검색
If GD_GRP_DETL <> "" THEN
	If InStr(GD_GRP_DETL,"'") < 1 Then
		Page_GD_GRP_DETL = "'" & Replace(GD_GRP_DETL,",","','") & "' "
	Else
		Page_GD_GRP_DETL = GD_GRP_DETL
	End If
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_DETL IN (" & Page_GD_GRP_DETL & ") "
End If

'세세분류 코드 검색
If GRP_DETL2 <> "" THEN
	If InStr(GRP_DETL2,"'") < 1 Then
		Page_GRP_DETL2 = "'" & Replace(GRP_DETL2,",","','") & "' "
	Else
		Page_GRP_DETL2 = GRP_DETL2
	End If
	P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_DETL2 IN (" & Page_GRP_DETL2 & ") "
End If

'셋트일반여부
If GD_TP <> "" THEN
	P_Count_SQL = P_Count_SQL &" AND GI.GD_TP = '" & GD_TP & "' "
End If

'브랜드검색
If GD_GRP_BRAND <> "" Then
	If GD_GRP_BRAND = "etc" Or GD_GRP_BRAND = "'etc'" Then
		P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_BRAND NOT IN('00055','00050','00818','00033','00028','1381','00062','00038','00003','00065','2345') "
	Else
		P_Count_SQL = P_Count_SQL &" AND GI.GD_GRP_BRAND IN (" & GD_GRP_BRAND & ") "
	End If
End If

'상품명 검색
If GD_NM <> "" Then
	P_Count_SQL = P_Count_SQL &" AND GI.GD_NM LIKE '%" & GD_NM & "%' "
End If

'온라인 상품 카테
If GD_CATE <> "" Then
	P_Count_SQL = P_Count_SQL&" AND OG.GD_CATE = '" & GD_CATE & "' "
End If

'상품 가격대별 검색===========================================================================================
If PriceType <> "" Then
	If PriceType = "1" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1 AND 990"
	ElseIf PriceType = "2" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 1000 AND 4990"
	ElseIf PriceType = "3" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 5000 AND 9990"
	ElseIf PriceType = "4" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 10000 AND 19990"
	ElseIf PriceType = "5" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 20000 AND 29990"
	ElseIf PriceType = "6" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 30000 AND 49990"
	ElseIf PriceType = "7" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 69990"
	ElseIf PriceType = "8" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 70000 AND 99990"
	ElseIf PriceType = "9" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 199990"
	ElseIf PriceType = "10" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 200000 AND 299990"
	ElseIf PriceType = "11" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) >300000"
	ElseIf PriceType = "12" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 50000 AND 100000"
	ElseIf PriceType = "13" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 100000 AND 150000"
	ElseIf PriceType = "14" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) Between 150000 AND 200000"
	ElseIf PriceType = "15" Then
		P_Count_SQL = P_Count_SQL&" AND  ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) >200000"
	End If
End If

'상품 가격대별 검색===========================================================================================
'============================================================================================================================================
P_Count_SQL = P_Count_SQL&" ) PL "
'여기가 기본쿼리 끝
'Response.Write P_Count_SQL
'Response.End

dbopen()
	Set CountRs = DBcon.Execute(P_Count_SQL)
dbclose()
	If Not(CountRs.Eof Or CountRs.Bof) Then
		Product_TotalCount_Search = CountRs("PL_Count")
	End If
End Function


Function Get_Price(PriceType)
	If PriceType = "1" Then
		Get_Price = "1P ~ 990P"
	ElseIf PriceType = "2" Then
		Get_Price = "1,000P ~ 4,990P"
	ElseIf PriceType = "3" Then
		Get_Price = "5,000P ~ 9,990P"
	ElseIf PriceType = "4" Then
		Get_Price = "10,000P ~ 19,990P"
	ElseIf  PriceType = "5" Then
		Get_Price = "20,000P ~ 29,990P"
	ElseIf PriceType = "6" Then
		Get_Price = "30,000P ~ 49,990P"
	ElseIf PriceType = "7" Then
		Get_Price = "50,000P ~ 69,990P"
	ElseIf PriceType = "8" Then
		Get_Price = "70,000P ~ 99,990P"
	ElseIf PriceType = "9" Then
		Get_Price = "100,000P ~ 199,990P"
	ElseIf PriceType = "10" Then
		Get_Price = "200,000P ~ 299,990P"
	ElseIf PriceType = "11" Then
		Get_Price = "300,000P ~ 이상"
	ElseIf PriceType = "12" Then
		Get_Price = "50,000P ~ 100,000P"
	ElseIf PriceType = "13" Then
		Get_Price = "100,000P ~ 150,000P"
	ElseIf PriceType = "14" Then
		Get_Price = "150,000P ~ 200,000P"
	ElseIf PriceType = "15" Then
		Get_Price = "200,000P ~ 이상"
	End If
End Function

' ***************************************************************************************
' * 함수설명 : HTML BR 태그 변환 함수
' * 변수설명 : str : 변환할 변수 값
' ***************************************************************************************
Function ReplaceBr(str)
	If ISNULL(str) = False Then
		str = REPLACE(str,CHR(13) & CHR(10),"<BR/>")
		str = REPLACE(str,CHR(10),"<BR/>")
		ReplaceBr = str
	End If
End Function



Function ReplaceChr(str)
	If ISNULL(str) = False Then
'		str = REPLACE(str,CHR(13) & CHR(10),"<BR/>")
		str = REPLACE(str,CHR(10),"<BR/>")
		ReplaceChr = str
	End If
End Function



'회원번호,세션,상품번호,사이트구분값,수량,스티커,포장지,전용박스
Function Cart_Add_Popup(User_SEQ,User_Session,GD_SEQ,ONLINE_CD,GD_VOL,ST_SEQ,WRAP_SEQ,Tb_Type)
	If Tb_Type = "Cart" Then
		Tb_Name = "ITEMCENTER.DBO.IC_T_ONLINE_CART "
	Else
		Tb_Name = "ITEMCENTER.DBO.IC_T_ONLINE_Zzim "
	End If

	'기존에 장바구니에 담긴 상품인지 체크
	Chk_SQL = "SELECT COUNT(GD_SEQ) FROM "

	Chk_SQL = Chk_SQL&Tb_Name

	If User_SEQ <> "" Then
		'로그인한 회원일 경우 회원 SEQ로 검색
		Chk_SQL = Chk_SQL&" WHERE User_Seq = '"&User_SEQ&"' "
	Else
		'비로그인인 경우 Session.SessionID로 검색
		Chk_SQL = Chk_SQL&" WHERE User_Session = '"&User_Session&"' "
	End If

	Chk_SQL = Chk_SQL&" AND GD_SEQ = '"&GD_SEQ&"' "

	If Left(GetsLOGINID,1) = "N" then
		Chk_SQL = Chk_SQL&" AND ONLINE_CD = '0050' "
	Else
		Chk_SQL = Chk_SQL&" AND ONLINE_CD = '0031' "
	End If

	Chk_SQL = Chk_SQL&" AND DEL_YN = 'N' "
	Chk_SQL = Chk_SQL&" AND Sub_Gubun = '' "

	Set CartCount = DBcon.Execute(Chk_SQL)

	If CartCount(0)  = 0 Then
		InSQL = "INSERT "
		InSQL = InSQL&Tb_Name
		InSQL = InSQL&"(User_Seq,User_Session,GD_SEQ,ONLINE_CD,GD_EA,Regdate,DEL_YN,USER_IP,ST_SEQ,WRAP_SEQ,Sub_Gubun) "
		InSQL = InSQL&" VALUES ('"&User_Seq&"','"&User_Session&"','"&GD_SEQ&"','"&ONLINE_CD&"','"&GD_VOL&"',Getdate(),'N','"&Request.ServerVariables("remote_addr")&"','"&ST_SEQ&"','"&WRAP_SEQ&"','') "

		Dbcon.Execute(InSQL)

		If Tb_Type = "Cart" Then
			Response.Write "<script>if(confirm('장바구니에 상품이 저장되었습니다.\n장바구니로 이동하시겠습니까?')){opener.location.href='/order/cart.asp';window.close();}else{flase;}</script>"
		Else
			Response.Write "<script>alert('관심상품이 저장되었습니다.')</script>"
		End If
			Response.End
	Else
		If Tb_Type = "Cart" Then
			'해당스티커 옵션이 바뀔수도 있으므로 해당 카드 업데이트
			UpSQL = "UPDATE "
			UpSQL = UPSQL&Tb_Name
			UpSQL = UpSQL&" Set GD_EA='"&GD_VOL&"',Regdate=getdate(),ST_SEQ='"&ST_SEQ&"',WRAP_SEQ='"&WRAP_SEQ&"' WHERE GD_SEQ='"&GD_SEQ&"' AND ONLINE_CD='"&ONLINE_CD&"' AND User_SEQ='"&User_Seq&"' "
			Dbcon.Execute(UpSQL)
			'Response.Write "<script>if(confirm('이미 장바구니에 담겨있는 상품입니다.\n장바구니로 이동하시겠습니까?')){parent.location.href='/order/cart.asp';}</script>"
			Response.Write "<script>if(confirm('장바구니에 상품이 저장되었습니다.\n장바구니로 이동하시겠습니까?')){opener.location.href='/order/cart.asp';window.close();}</script>"
		Else
			Response.Write "<script>alert('이미 관심상품에 담겨있는 상품입니다.')</script>"
		End If
		Response.End
	End If

End Function

'----- 문자열 자르기------------------------------------------------------------------------
Function HLeft(ByVal pStr, ByVal pELength)
	Dim i, j
	Dim Temp, OutStr

	IF HLen(pStr) <= pELength THEN
		OutStr = pStr
	ELSE
		For i = 1 To pELength
				Temp = Mid(pStr, i, 1)
			IF Asc(Temp) < 0 Then
				j = j + 2
				Else
				j = j + 1
			End If

			OutStr = OutStr & Temp
			IF j >= pELength THEN exit For
		Next
		OutStr = OutStr & "..."
	END IF

	HLeft = OutStr 
End Function

'문자길이 리턴(한글/영문)
Function HLen(ByVal pStr)		
	Dim i, j
	If pStr <> "" Then 
	For i = 1 To Len(pStr)
		IF Asc(Mid(pStr, i, 1)) < 0 Then
			j = j + 2
		Else
			j = j + 1
		End If
	Next
	End if
	HLen = j
End Function

Function PT_Order_Ajax_PageLink(NowPage,ViewCnt,PAGING)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:order_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function


Function PT_center_Ajax_PageLink(NowPage,ViewCnt,PAGING)
	
	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1

	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""prev""></a>"
	Else
		'Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');""class=""prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:center_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function


Function PT_QNA_Ajax_PageLink(NowPage,ViewCnt,PAGING)
	
	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1

	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""prev""></a>"
	Else
		'Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');""class=""prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:qna_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function

Sub Pageing(filename)
		blockpage = Int((page - 1) / 10) * 10 + 1

		response.write "<div class=""paging"">"

		If blockpage = 1 then
			response.write "<a href=""#"" class=""prev""></a>"
		Else
			response.write "<a href=""#"">1</a>"
		End if

		i = 1
		Do Until i > 10 or blockpage > total_page
			if blockpage = int(page) then
				response.write "<li class=""page-item active""><a href=""#"" class=""page-link"">" & blockpage & "</a></li>"
			Else
				response.write "<li class=""page-item""><a href=""" & Filename & "&page=" & blockpage & """ class=""page-link"">" & blockpage & "</a></li>"
			End if

			blockpage = blockpage + 1
			i = i + 1
		Loop

		If blockpage > total_page then
			response.write "<li class=""page-item""><a href=""#"" class=""page-link"">다음</a></li>"
		Else
			response.write "<li class=""page-item""><a href=""" & Filename & "&h_page=" & blockpage & """ class=""page-link"">다음</a></li>"
		End if

		response.write "</div>"
	End Sub
%>