<% @CODEPAGE="65001" language="vbscript" %>
<%
	Response.ContentType = "text/html"
	Response.AddHeader "Content-Type", "text/html;charset=utf-8"
	Response.CodePage = "65001"
	Response.CharSet = "utf-8"

	GLOBAL_VAR_UNIGRPCD						  =	"2001"
	Const GLOBAL_VAR_ONLINECD				  =	"LIN01"
 	GLOBAL_VAR_PEID						      =	"mall_b2bc"
	GLOBAL_VAR_CUST_SEQ						  ="212576" '(PC)
	GLOBAL_VAR_CUST_MOBILE_SEQ				  ="221257" '(모바일)
	GLOBAL_VAR_PENM						      =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_COM_NM   					  =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_ZIP1     					  =	"157"
	GLOBAL_VAR_ZIP2     					  =	"010"
	GLOBAL_VAR_ADDR     					  =	"서울시 강서구 화곡동 24-48 102호"
	GLOBAL_VAR_PE      						  =	"삼성전자"
	GLOBAL_VAR_TEL1     					  =	"02"
	GLOBAL_VAR_TEL2     					  =	"2693"
	GLOBAL_VAR_TEL3     					  =	"0184"
	GLOBAL_VAR_GRP1     					  =	"77"
	GLOBAL_VAR_GRP2     					  =	"113"
	CONST GLOBAL_VAL_TITLE					  =	"삼성전자온라인b2b몰"
	GLOBAL_VAR_FRONT_ROOT					  =	"/gate.asp"
	GLOBAL_GRP1								  = "77"
	GLOBAL_GRP2								  = "113"

	Const Ref = "GPQRSATWXVYBCHL640MN598OIJKZ12D7EF3U"
	' 암호화
	
	 '/////////////////////////////////////////////////////////////////////////////////
	'// 제로체크 ==> data : 원본데이터 , chknum : 체크할개수
	Function ZeroCheck(data,chknum)
		dim temp,temp_len,i
		temp=""
		temp_len=len(data)
		if temp_len < chknum then
			for i=1 to chknum-temp_len
				temp=temp+"0"
			next
			temp=temp+CStr(data)
		else
			temp=CStr(data)
		end if
		ZeroCheck=temp
	End Function


	Function encode(str, chipVal)
			Dim Temp, TempChar, Conv, Cipher, i: Temp = ""

			chipVal = CInt(chipVal)
			str = StringToHex(str)
			For i = 0 To Len(str) - 1
			  TempChar = Mid(str, i + 1, 1)
			  Conv = InStr(Ref, TempChar) - 1
			  Cipher = Conv Xor chipVal
			  Cipher = Mid(Ref, Cipher + 1, 1)
			  Temp = Temp + Cipher
			Next
			encode = Temp
	End Function

	' 복호화
	Function decode(str, chipVal)
			Dim Temp, TempChar, Conv, Cipher, i: Temp = ""

			chipVal = CInt(chipVal)
			For i = 0 To Len(str) - 1
			  TempChar = Mid(str, i + 1, 1)
			  Conv = InStr(Ref, TempChar) - 1
			  Cipher = Conv Xor chipVal
			  Cipher = Mid(Ref, Cipher + 1, 1)
			  Temp = Temp + Cipher
			Next
			Temp = HexToString(Temp)
			decode = Temp
	End Function

	' 문자열 -> 16진수
	Function StringToHex(pStr)
			Dim i, one_hex, retVal
			For i = 1 To Len(pStr)
			  one_hex = Hex(Asc(Mid(pStr, i, 1)))
			  retVal = retVal & one_hex
			Next
			StringToHex = retVal
	End Function

	' 16진수 -> 문자열
	Function HexToString(pHex)
			Dim one_hex, tmp_hex, i, retVal
			For i = 1 To Len(pHex)
			  one_hex = Mid(pHex, i, 1)
			  If IsNumeric(one_hex) Then
					  tmp_hex = Mid(pHex, i, 2)
					  i = i + 1
			  Else
					  tmp_hex = Mid(pHex, i, 4)
					  i = i + 3
			  End If
			  retVal = retVal & Chr("&H" & tmp_hex)
			Next
			HexToString = retVal
	End Function

	Function fInject(argData)
		Dim strCheckArgSQL
		Dim arrSQL
	  Dim i

		strCheckArgSQL = LCase(Trim(argData))

		arrSQL = Array("exec ","sp_","xp_","insert ","update ","delete ","drop ","select ","union ","truncate ","script","object ","applet","embed ","iframe ","where ","declare ","sysobject","@variable","1=1","null","carrige return","new line","onload","char(","xmp","javascript","script","iframe","document","vbscript","applet","embed","object","frame","frameset","bgsound","alert","onblur","onchange","onclick","ondblclick","onerror","onfocus","onload","onmouse","onscroll","onsubmit","onunload","ptompt","</div>")

		For i=0 To ubound(arrSQL) Step 1
			If(InStr(strCheckArgSQL,arrSQL(i)) > 0) Then
				Select Case  arrSQL(i)
				Case "'"
						arrSQL(i) ="홑따옴표"
				Case "char("
					arrSQL(i) ="char"
			  End SELECT

				response.write "<SCRIPT LANGUAGE='JavaScript'>"
				response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
				response.write "  history.go(-1);"
				response.write "</SCRIPT>"
				response.end
			End If

			If(InStr(strCheckArgSQL,server.urlencode(arrSQL(i))) > 0) Then
				   Select Case  arrSQL(i)
				   Case "'"
					arrSQL(i) ="홑따옴표"
				   Case "char("
					arrSQL(i) ="char"
				   End SELECT
				response.write "<SCRIPT LANGUAGE='JavaScript'>"
				response.write "  alert('허용되지 않은 문자열이 있습니다. [" & arrSQL(i) & "]') ; "
				response.write "  history.go(-1);"
				response.write "</SCRIPT>"
				response.end
			End If

		Next

		'Xss 필터링
		'argData = Replace(argData,"&","&amp;")
		'argData = Replace(argData,"\","&quot;")
		'argData = Replace(argData,"<","&lt;")
		'argData = Replace(argData,">","&gt;")
		'argData = Replace(argData,"'","&#39;")
		'argData = Replace(argData,"""","&#34;")

		fInject = argData
	End Function

	'===============================================================================
	'Function : isLogin()
	'Description : 로그인 여부 반환
	'===============================================================================
	Function isLogin()
		If GetUserid() = "" Then
			isLogin = False
		Else
			isLogin = true
		End If
	End Function

	'회원로그인체크 현재창
	Function ChkLogin(Refer_URL)
		If GetUserid() = "" Then
			Response.Write "<script>location.href='/Main/gate.asp?Refer_Url="&Refer_URL&"'</script>"
			Response.End
		End If
	End Function

	'회원로그인체크 부모창
	Function ChkLogin_Parent(Refer_URL)
		If GetUserid() = "" Then
			Response.Write "<script>parent.location.href='/Main/gate.asp?Refer_Url="&Refer_URL&"'</script>"
			Response.End
		End If
	End Function

	'회원로그인체크 현재창
	Function ChkAgtLogin(Refer_URL)
		If GetAgtId() = "" Then
			Response.Write "<script>top.location.href='/gate.asp?Refer_Url="&Refer_URL&"'</script>"
			Response.End
		End If
	End Function


	Function StrConvert(StrConData)
		StrConvert = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(StrConData, vbNewLine, "ㆍ"), "'", "＇"), "<", "＜"), ">", "＞"), ",", "－"), "&", "＆"), """", "¨"), "|", " | ")
	End Function

  'Function StrConvert(sData)
  'StrConvert = Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(sData, vbNewLine, "ㆍ"), "'", "＇"), "<", "＜"), ">", "＞"), ",", "－"), "&", "＆"), """", "¨"), "|", " | ")

  	'Chr(9)값:
	'Chr(10)값:
	'Chr(11)값:
	'Chr(12)값:
	'Chr(13)값:

  'End Function




Function PT_QNA_Ajax_PageLink(NowPage,ViewCnt,PAGING)
	
	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1

	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""prev""></a>"
	Else
		'Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:qna_search('1','"&ViewCnt&"','"&PAGING&"');""class=""prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:qna_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:qna_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function


'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력  =========================================================================
'=========================================================================================================================================================
Function PT_Ajax_Search_PageLink(NowPage,ViewCnt,Order_type,SearchText, page_url)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging prev""></a>"	
		Response.Write "<a href="""&page_url&"?search_box="&SearchText&"&NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');""class=""btn_paging prev""></a>"
		Response.Write "<a href="""&page_url&"?search_box="&SearchText&"&NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			'Response.Write "<a href=""javascript:product_list('"&blockpage&"','"&ViewCnt&"','"&Order_type&"');"">"&blockpage&"</a>"
			Response.Write "<a href="""&page_url&"?search_box="&SearchText&"&NowPage="&blockpage&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type & """>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?search_box="&SearchText&"&NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?search_box="&SearchText&"&NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function




'=========================================================================================================================================================
'============================================================**상품 개별정보 호출 ========================================================================
'=========================================================================================================================================================
'상품상세 정보를 지고 올때 사용하는 쿼리
'몰코드,상품명,금액계산타입
Function Product_View(ONLINE_CD,GD_NM,GD_SEQ,PRICE_TYPE)
	'상품명으로 검색시 여러 데이터가 나오므로 TOP 1 정렬은 GD_PRICE3 낮은 데이터로 정렬
		P_View_SQL = "SELECT TOP 1 "
		P_View_SQL = P_View_SQL & " GI.SEQ AS SEQ, GI.GD_NM, GI.GD_GRP_BRAND, ITEMCENTER.dbo.IC_FN_BRAND_NM(GI.GD_GRP_BRAND) AS GD_GRP_BRAND_NM, GI.GD_TP "
		'원산지
		P_View_SQL = P_View_SQL & ", GI.GD_GRP_MADE"
		'상품제공업체코드
		P_View_SQL = P_View_SQL & ", GI.UNI_GRP_CD"
		'셋트상품구성
		P_View_SQL = P_View_SQL & ", GI.GD_SET"
		
		P_View_SQL = P_View_SQL & ", ITEMCENTER.dbo.IC_FN_MADE_NM(GI.GD_GRP_MADE) AS GD_GRP_MADE_NM "

		P_View_SQL = P_View_SQL & ", ITEMCENTER.dbo.IC_FN_DETL_NM(GI.GD_GRP_DETL,GI.GD_GRP_FIRST,GI.GD_GRP_MID) AS GD_GRP_DETL_NM "
		'
		'상품 이미지 
		P_View_SQL = P_View_SQL & "     ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH)) = '' OR GI.GD_IMG_PATH IS NULL THEN 'gdsimage/listgdsnull1.jpg'     "
		P_View_SQL = P_View_SQL & "      ELSE GI.GD_IMG_PATH END GD_IMG_PATH    "
		P_View_SQL = P_View_SQL & "     ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_2)) = '' OR GI.GD_IMG_PATH_2 IS NULL THEN 'gdsimage/listgdsnull1.jpg'     "
		P_View_SQL = P_View_SQL & "      ELSE GI.GD_IMG_PATH_2 END GD_IMG_PATH_2    "
		P_View_SQL = P_View_SQL & "     ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_3)) = '' OR GI.GD_IMG_PATH_3 IS NULL THEN 'gdsimage/listgdsnull1.jpg'     "
		P_View_SQL = P_View_SQL & "      ELSE GI.GD_IMG_PATH_3 END GD_IMG_PATH_3    "
		P_View_SQL = P_View_SQL & "     ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH_4)) = '' OR GI.GD_IMG_PATH_4 IS NULL THEN 'gdsimage/listgdsnull1.jpg'     "
		P_View_SQL = P_View_SQL & "      ELSE GI.GD_IMG_PATH_4 END GD_IMG_PATH_4    "
		'상세설명 이미지
		P_View_SQL = P_View_SQL & "     ,CASE WHEN RTRIM(LTRIM(GI.GD_IMG_PATH2)) = '' OR GI.GD_IMG_PATH2 IS NULL THEN 'gdsimage/listgdsnull1.jpg'     "
		P_View_SQL = P_View_SQL & "      ELSE GI.GD_IMG_PATH2 END GD_IMG_PATH2    "
		'옵션1,2
		P_View_SQL = P_View_SQL & "		,GI.GD_SIZE AS GD_OPTION1" 
		P_View_SQL = P_View_SQL & "		,GI.GD_COLOR AS GD_OPTION2" 

		'===상품 최소 구매수량
		P_View_SQL = P_View_SQL & "		,GI.GD_MIN_VOL AS GD_MIN_VOL " 
		'===상품 최소 구매수량
		'===알파메모
		P_View_SQL = P_View_SQL & "		,ISNULL(GI.GD_ALPHA_MEMO,'') AS GD_ALPHA_MEMO " 
		'===알파메모		

		'===가격 정책에 따른 금액 계산
		If PRICE_TYPE = "" Then
'			P_View_SQL = P_View_SQL & "		,ROUND(ISNULL(GI.GD_PRICE3,0) / ((100-ISNULL(OG.GD_PER,0))/100 )+40,-2) AS GD_PRICE3 " 		
			'운영회원가 * 1.1
			P_View_SQL = P_View_SQL & "		,ISNULL(GI.GD_PRICE3,0) AS GD_PRICE "
'			P_View_SQL = P_View_SQL & "		,ROUND((ISNULL(GI.GD_PRICE3,0)  * 1.1) * 1.1 +40,-2) AS GD_PRICE3 " 			
			P_View_SQL = P_View_SQL & "		,ISNULL(CASE WHEN ISNULL(OG.GD_TAX_YN,'N') = 'N' THEN ROUND(ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100)+40,-2) ELSE				ROUND((ISNULL(GI.GD_PRICE3,0)/((100-ISNULL(OG.GD_PER,0))/100) * 1.1)+40,-2) END,0) AS GD_PRICE3 " 			
			P_View_SQL = P_View_SQL & "		,ISNULL(GI.GD_PRICE7,0) AS GD_PRICE7"
			P_View_SQL = P_View_SQL & "		,GI.GD_PRICE8 AS GD_PRICE8 "
		End  If
		'===가격 정책에 따른 금액 계산
		'스티커사용여부
		P_View_SQL = P_View_SQL & "		,OG.ST_YN AS ST_YN "
		'포장지사용여부
		P_View_SQL = P_View_SQL & "		,OG.WRAP_YN AS WRAP_YN "
		'박스사용여부
		P_View_SQL = P_View_SQL & "		,OG.RIBON_YN AS RIBON_YN "
		'부가세포함여부
		P_View_SQL = P_View_SQL & "		,OG.GD_TAX_YN AS GD_TAX_YN "
		'마진율
		P_View_SQL = P_View_SQL & "		,OG.GD_PER AS GD_PER "
		'면세상품여부
		P_View_SQL = P_View_SQL & " ,GI.GD_Tax_Free_YN AS Tax_Free"
		P_View_SQL = P_View_SQL & " FROM IC_T_GDS_INFO GI LEFT JOIN IC_T_ONLINE_GDS OG "
		P_View_SQL = P_View_SQL & " ON GI.SEQ = OG.GD_SEQ  "
		P_View_SQL = P_View_SQL & " WHERE GI.DEL_YN='N'"
		'몰코드
		P_View_SQL = P_View_SQL&"   AND   OG.ONLINE_CD='"&ONLINE_CD&"'"			
		P_View_SQL = P_View_SQL & " AND   GI.GD_BUY_END_YN='N'"
		P_View_SQL = P_View_SQL & " AND   GI.GD_OPEN_YN='N'"
		P_View_SQL = P_View_SQL & " AND   OG.DEL_YN='N'"
		P_View_SQL = P_View_SQL & " AND   OG.UNI_GRP_CD='2001'" 

		'상품명 검색 
		If GD_NM <> "" Then 
			P_View_SQL = P_View_SQL & " AND GI.GD_NM = '"&GD_NM&"'"
		Else
		'상품명이 없다면 SEQ값이 있는걸로 간주함
			P_View_SQL = P_View_SQL & " AND GI.SEQ = '"&GD_SEQ&"'"
		End If 

		Product_View = Dbcon.Execute(P_View_SQL)


End Function 
'=========================================================================================================================================================
'============================================================**상품 개별정보 호출 ========================================================================
'=========================================================================================================================================================


'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력  =========================================================================
'=========================================================================================================================================================
Function PT_Ajax_PageLink(NowPage,ViewCnt,Order_type,page_url)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging prev""></a>"	
		Response.Write "<a href="""&page_url&"?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"');""class=""btn_paging prev""></a>"
		Response.Write "<a href="""&page_url&"?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			'Response.Write "<a href=""javascript:product_list('"&blockpage&"','"&ViewCnt&"','"&Order_type&"');"">"&blockpage&"</a>"
			Response.Write "<a href="""&page_url&"?NowPage="&blockpage&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type & """>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href="""&page_url&"?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&""" class=""btn_paging next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function

'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력(카테고리)  =========================================================================
'=========================================================================================================================================================
Function PT_Ajax_Category_PageLink(NowPage,ViewCnt,Order_type,gd_grp_mid,gd_grp_detl,gd_grp_detl2)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""/></a>"
		Response.Write "<a href=""category_list.asp?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&"&gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"&gd_grp_detl2="&gd_grp_detl2&""" class=""btn_paging prev""></a>"
	Else
		'Response.Write "<a href=""javascript:product_list('1','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""/></a>"
		Response.Write "<a href=""category_list.asp?NowPage=1&ViewCnt="&ViewCnt&"&Order_type="&Order_type&"&gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"&gd_grp_detl2="&gd_grp_detl2&""" class=""btn_paging prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			'Response.Write "<a href=""javascript:product_list('"&blockpage&"','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"">"&blockpage&"</a>"
			Response.Write "<a href=""category_list.asp?NowPage="&blockpage&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&"&gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"&gd_grp_detl2="&gd_grp_detl2 & """>"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""/></a>"&vbcrlf
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href=""category_list.asp?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&"&gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"&gd_grp_detl2="&gd_grp_detl2&""" class=""btn_paging next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""/></a>"&vbcrlf
		'Response.Write "<a href=""javascript:product_list('"&blockpage-1&"','"&ViewCnt&"','"&Order_type&"','"&gd_grp_mid&"','"&gd_grp_detl&"','"&gd_grp_detl2&"');"" class=""btn_paging next""></a>"&vbcrlf
		Response.Write "<a href=""category_list.asp?NowPage="&blockpage-1&"&ViewCnt="&ViewCnt&"&Order_type="&Order_type&"&gd_grp_mid="&gd_grp_mid&"&gd_grp_detl="&gd_grp_detl&"&gd_grp_detl2="&gd_grp_detl2&""" class=""btn_paging next""></a>"&vbcrlf

	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function



'=========================================================================================================================================================
'============================================================* 페이징 네비게이션 출력(주문내역)  =========================================================================
'=========================================================================================================================================================
Function PT_Order_Ajax_PageLink(NowPage,ViewCnt,PAGING)

	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1
	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
	Else
		Response.Write "<a href=""javascript:order_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:order_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
	Else
		'다음페이지
		Response.Write "<a href=""javascript:order_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function



Function PT_center_Ajax_PageLink(NowPage,ViewCnt,PAGING)
	
	Dim BlockPage,t
	'blockpage=int((NowPage-1)/5)*5+1
	blockpage=int((NowPage-1)/10)*10+1

	Response.Write "<div class=""paging_wrap""><div class=""paging"">"
	'첫페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage=1&"&str&"'>이전</a></li>"

	if blockpage<>1 Then
		'Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');"" class=""prev""></a>"
	Else
		'Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');""class=""btn_paging prev""><img src=""/front/img/common/icon_paging_prev.png"" alt=""""/></a>"
		Response.Write "<a href=""javascript:center_search('1','"&ViewCnt&"','"&PAGING&"');""class=""prev""></a>"
	End If

	do until t>9 or blockpage > TotalPage
		If blockpage=int(NowPage) Then
			'현재페이지
			Response.Write "<a href=""#"" class=""active"">"&blockpage&"</a>"
		Else
			'나머지페이지
			Response.Write "<a href=""javascript:center_search('"&blockpage&"','"&ViewCnt&"','"&PAGING&"');"">"&blockpage&"</a>"
		End If
		blockpage = blockpage+1
		t=t+1
	loop
	If blockpage > totalpage Then
		'Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	Else
		'다음페이지
		'Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""btn_paging next""><img src=""/front/img/common/icon_paging_next.png"" alt=""""/></a>"&vbcrlf
		Response.Write "<a href=""javascript:center_search('"&blockpage-1&"','"&ViewCnt&"','"&PAGING&"');"" class=""next""></a>"&vbcrlf
	End If
	'마지막페이지
	'Response.Write "<li class='pre'><a href='"&linkpage&"?NowPage="&totalpage&"&"&str&"'>다음</a></li>"&vbcrlf	

	Response.Write "</div></div>"&vbcrlf
End Function



'----- 문자열 자르기------------------------------------------------------------------------
Function HLeft(ByVal pStr, ByVal pELength)
	Dim i, j
	Dim Temp, OutStr

	IF HLen(pStr) <= pELength THEN
		OutStr = pStr
	ELSE
		For i = 1 To pELength
				Temp = Mid(pStr, i, 1)
			IF Asc(Temp) < 0 Then
				j = j + 2
				Else
				j = j + 1
			End If

			OutStr = OutStr & Temp
			IF j >= pELength THEN exit For
		Next
		OutStr = OutStr & "..."
	END IF

	HLeft = OutStr 
End Function


'문자길이 리턴(한글/영문)
Function HLen(ByVal pStr)		
	Dim i, j
	If pStr <> "" Then 
	For i = 1 To Len(pStr)
		IF Asc(Mid(pStr, i, 1)) < 0 Then
			j = j + 2
		Else
			j = j + 1
		End If
	Next
	End if
	HLen = j
End Function



'===============================================================================
	'Function : GetsLOGINID()
	'Description : 회원 아이디 반환
	'===============================================================================
	Function GetsLOGINID()
		GetsLOGINID = Request.Cookies("sLOGINID")
	End Function

	Function gate_GetsLOGINID()
		gate_GetsLOGINID = Request.Cookies("sLOGINID2")
	End Function


	Function gate_GetsLOGINPWD()
		gate_GetsLOGINPWD = Request.Cookies("sLOGINPWD")
	End Function

	'===============================================================================
	'Function : GetsCUSTSEQ()
	'Description : 회원 SEQ 반환
	'===============================================================================
	Function GetsCUSTSEQ()
		GetsCUSTSEQ = Request.Cookies("sCUSTSEQ")
	End Function
	'===============================================================================
	'Function : GetsCUSTNM()
	'Description : 사용자 이름 반환
	'===============================================================================
	Function GetsCUSTNM()
		GetsCUSTNM = Request.Cookies("sCUSTNM")
	End Function
	'===============================================================================
	'Function : GetsCUSTZIP()
	'Description : 우편번호1 반환
	'===============================================================================
	Function GetsCUSTZIP()
		GetsCUSTZIP = Request.Cookies("sCUSTZIP")
	End Function
	'===============================================================================
	'Function : GetsCUSTZIP2()
	'Description : 우편번호2 반환
	'===============================================================================
	Function GetsCUSTZIP2()
		GetsCUSTZIP2 = Request.Cookies("sCUSTZIP2")
	End Function
	'===============================================================================
	'Function : GetsCUSTADDR()
	'Description : 주소 반환
	'===============================================================================	
	Function GetsCUSTADDR()
		GetsCUSTADDR = Request.Cookies("sCUSTADDR")
	End Function
	'===============================================================================
	'Function : GetsCUSTTEL()
	'Description : 전호번호 반환
	'===============================================================================	
	Function GetsCUSTTEL()
		GetsCUSTTEL = Request.Cookies("sCUSTTEL")
	End Function
	'===============================================================================
	'Function : GetsCUSTEMAIL()
	'Description : 이메일 반환
	'===============================================================================	
	Function GetsCUSTEMAIL()
		GetsCUSTEMAIL = Request.Cookies("sCUSTEMAIL")
	End Function
	'===============================================================================
	'Function : GetsCUSTHP()
	'Description : 휴대폰번호 반환
	'===============================================================================
	Function GetsCUSTHP()
		GetsCUSTHP = Request.Cookies("sCUSTHP")
	End Function

	Function GetsWebGubun()
		GetsWebGubun = Request.Cookies("sWebGubun")
	End Function

	Function Gets_url_location()
		Gets_url_location = Request.Cookies("s_location_gb") 
	End function
	
	Function Gets_saved_id()
		Gets_saved_id = Request.Cookies("s_saved_id") 
	End Function
	
%>
<!--#include virtual="/Library/dbcon.asp"-->
<%
	Dbopen()
%>