<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, width=device-width"/>
<title>KakaoLink v2 Demo(Default / Feed) - Kakao JavaScript SDK</title>
<script src="//developers.kakao.com/sdk/js/kakao.min.js"></script>

</head>
<body>
<a id="kakao-link-btn" href="javascript:;">
<img src="//developers.kakao.com/assets/img/about/logos/kakaolink/kakaolink_btn_medium.png"/>
</a>
<script type='text/javascript'>
  //<![CDATA[
    // // 사용할 앱의 JavaScript 키를 설정해 주세요.
    Kakao.init('22cdc666c2c9d413ef3316caa8abc101');
    // // 카카오링크 버튼을 생성합니다. 처음 한번만 호출하면 됩니다.
    Kakao.Link.createDefaultButton({
      container: '#kakao-link-btn',
      objectType: 'feed',
      content: {
        title: '위드라인테스트',
        description: '#페이지공유하기 #로그인',
        imageUrl: 'http://b2bc.samsungbizmall.com/front/img/samsung_20191114_2_mobile.jpg',
        link: {
          mobileWebUrl: 'http://b2bc.samsungbizmall.com',
          webUrl: 'http://b2bc.samsungbizmall.com'
        }
      },
      social: {
        likeCount: 286,
        commentCount: 45,
        sharedCount: 845
      },
      buttons: [
        {
          title: '웹으로 보기',
          link: {
            mobileWebUrl: 'http://b2bc.samsungbizmall.com',
            webUrl: 'http://b2bc.samsungbizmall.com'
          }
        }
      ]
    });
  //]]>
</script>

</body>
</html>