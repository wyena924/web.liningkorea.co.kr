<!-- #include virtual = "include/head.asp" -->
<!-- #include virtual = "include/header.asp" -->
<%
'카트검색 ERP에서 상품이 삭제된 제품은 삭제처리
Response.Cookies("cart_session_id").path = "/"
Response.Cookies("cart_session_id").Domain= Request.ServerVariables("SERVER_NAME")
Response.Cookies("cart_session_id") = Session.SessionID
Response.cookies("cart_session_id").expires = date+365

If GetsCUSTSEQ() <> "" Then 
	User_SEQ      = GetsCUSTSEQ()
Else
	User_SEQ      = Request.Cookies("cart_session_id")
End If

If User_SEQ = "" Then
	User_SEQ      = Request.Cookies("cart_session_id")
End If 

gd_grp_mid	  = Request("gd_grp_mid")
gd_grp_detl	  = Request("gd_grp_detl")


Chk_Cart = "SELECT GD_SEQ from IC_T_ONLINE_CART  where ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' and USER_SEQ='"&User_SEQ&"'"

'Response.write Chk_Cart

DBopen()
Set CCRs = Dbcon.Execute(Chk_Cart)

If Not (CCRs.Eof Or CCRs.Bof) Then 
	Do Until CCRs.Eof 
			Chk_GI = "SELECT COUNT(GD_NM) AS CGCount FROM IC_T_GDS_INFO GI JOIN IC_T_ONLINE_GDS OG on GI.SEQ=OG.GD_SEQ WHERE (GI.DEL_YN='Y' AND GI.GD_BUY_END_YN='Y' AND GI.GD_OPEN_YN='Y' AND OG.DEL_YN='Y') AND GI.SEQ='"&CCRS("GD_SEQ")&"'"
			CGRs = Dbcon.Execute(Chk_GI)
			'삭제나 품절처리 된상품으로 장바구니 삭제 처리
			If CGRs("CGCount") > 0 Then 
				'삭제될때는 상품을 다 삭제 시킨다.
				Chk_Cart_Del = "DELETE from IC_T_ONLINE_CART  where ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' and GD_SEQ='"&CCRs("GD_SEQ")&"'"
				DBcon.Execute(Chk_Cart_Del)
			End If 
		CCRs.MoveNext
	Loop 
End If 


'3일지난 상품 삭제 데이터 완전 삭제
Chk_3Days = "DELETE FROM IC_T_ONLINE_CART WHERE ONLINE_CD='"&GLOBAL_VAR_ONLINECD&"' AND DATEDIFF(DD,regDate,getdate())>7"
Dbcon.Execute(Chk_3Days)


'현재 페이지
NowPage = Request("NowPage")
If NowPage = "" Then 
	NowPage = 1
End If 
'페이지당 보여줄 아이템 수
ViewCnt = 200

DBclose()

If cust_tp_rs(User_SEQ) = "N" Then
	Response.write("<script language='javascript'>alert('로그인이 필요한 서비스 입니다.'); location.href='/login.asp'</script>")
End If 

%>
<script language='javascript'>


	//콤마찍기
	function comma(str) {
		str = String(str);
		return str.replace(/(\d)(?=(?:\d{3})+(?!\d))/g, '$1,');
	}
	 
	//콤마풀기
	function uncomma(str) {
		str = String(str);
		return str.replace(/[^\d]+/g, '');
	}
	 



	//전체상품선택해제
	function AllCheck_Click(){
		var f = document.frm
		var chk_seq = document.getElementsByName("chkidx");
		//상품번호
		var GD_SEQ     = ""
		//스티커
		var ST_SEQ_A   = ""
		//포장지
		var WRAP_SEQ_A = ""
		var GD_EA_A    = ""
		var Tot = 0

		if(document.getElementById("AllCheck").checked==true){
			for(var i=0; i<chk_seq.length;i++){                                                                    
				chk_seq[i].checked = true;     //모두 체크	
			}
			
			document.getElementById("ToTal_Price").innerHTML = addcomma(Tot);		
			if(Tot>=30000){
				document.getElementById("Delivery_Price_div").innerHTML = 0;			
				document.getElementById("ToTal_Price_Sum").innerHTML = addcomma(Tot);			
			}else{
				document.getElementById("Delivery_Price_div").innerHTML = addcomma(2500);			
				document.getElementById("ToTal_Price_Sum").innerHTML = addcomma(Tot+2500);			
			}

		}else{
			for(var i=0; i<chk_seq.length;i++){                                                                    
				chk_seq[i].checked = false;     //모두 해제
			}	
		}
	}

	//카트 선택 삭제
	function Cart_Del_Check(){
		var f = document.frm;
		if(f.GD_SEQ.value==""){
			alert("삭제하실 상품을 선택해 주세요.");
		}else{
			if(confirm("선택된 상품을 삭제하시겠습니까?")){
				f.CART_TYPE.value = "Cart_Del_Check";
				document.frm.target = "iSQL"
				document.frm.action = "cart_ok.asp"
				document.frm.submit();							
			}
		}
	}

	function minus(Seq,Min_Vol,idx)
	{
		var price_sum = 0; //상품금액의 합계
		var chk_seq = document.getElementsByName("chkidx"); //각 상품별 체크박스 객체
		var product_sum= uncomma(document.getElementById("product_sum_" + idx).innerHTML); //각 상품별 금액
		var gd_ea = document.getElementById("GD_EA_" + idx).value; //각 상품별 수량
		var product_price3 = document.getElementById("GD_Price3_" + idx).value; //상품 금액의 원금
		var last_price_div = document.getElementById("last_price_div").innerHTML; //총 결제금액 합계
		if (document.getElementById("GD_EA_" +idx).value <= 1 ) //상품 수량이 0 이하로 넘어가지 못하게 하는 부분
		{
			alert('최소 주문수량 이하의 수량은 선택하실 수 없습니다.');
			return;
		}
		document.getElementById("GD_EA_" +idx).value = Number(document.getElementById("GD_EA_" +idx).value) - 1 //수량 체크 박스 -1
		document.getElementById("product_sum_" + idx).innerHTML = comma(product_price3 * document.getElementById("GD_EA_" +idx).value); //각 상품별 금액 계산하기
		for(var i=0; i<chk_seq.length;i++){                                                                    
			price_sum = Number(price_sum) + Number( Number(document.getElementById("GD_EA_" +i).value) * Number(document.getElementById("GD_Price3_" + i).value  ))	//상품금액합계 계산
			document.getElementById("Total_Price_div").innerHTML= comma(price_sum); //상품금액 표시
			
		}
		if (price_sum < 30000)//상품금액의 합이 30000원 이하일때 배송비 2500원 붙이는 부분
		{
			document.getElementById("delivery_price_div").innerHTML = "2500"
		}
		//총 결제금액 합계 표시
		document.getElementById("last_price_div").innerHTML = comma(Number(uncomma(document.getElementById("Total_Price_div").innerHTML)) + Number(uncomma(document.getElementById("delivery_price_div").innerHTML)))
		
	}

	function plus(Seq,Min_Vol,idx)
	{
		var price_sum = 0; //상품금액의 합계
		var chk_seq = document.getElementsByName("chkidx"); //각 상품별 체크박스 객체
		var product_sum= uncomma(document.getElementById("product_sum_" + idx).innerHTML);//각 상품별 금액
		var gd_ea = document.getElementById("GD_EA_" + idx).value;  //각 상품별 수량
		var product_price3 = document.getElementById("GD_Price3_" + idx).value;  //상품 금액의 원금 
		var last_price_div = document.getElementById("last_price_div").innerHTML; //총 결제금액 합계
		document.getElementById("GD_EA_" +idx).value = Number(document.getElementById("GD_EA_" +idx).value) + 1 //수량 체크 박스 +1
		document.getElementById("product_sum_" + idx).innerHTML = comma(product_price3 * document.getElementById("GD_EA_" +idx).value);//각 상품별 금액 계산하기
		//P_Sum(Seq,Min_Vol)
		for(var i=0; i<chk_seq.length;i++){                                                                    
			price_sum = Number(price_sum) + Number( Number(document.getElementById("GD_EA_" +i).value) * Number(document.getElementById("GD_Price3_" + i).value  ))	//상품금액합계 계산
			document.getElementById("Total_Price_div").innerHTML= comma(price_sum);//상품금액 표시
		}
		if (price_sum < 30000) //상품금액의 합이 30000원 이하일때 배송비 2500원 붙이는 부분
		{
			document.getElementById("delivery_price_div").innerHTML = "2500"
		}
		//총 결제금액 합계 표시
		document.getElementById("last_price_div").innerHTML = comma(Number(uncomma(document.getElementById("Total_Price_div").innerHTML)) + Number(uncomma(document.getElementById("delivery_price_div").innerHTML)))
	}

	function mid(s,c,l){
	  return s.substring(c,l);
	}//


	function Cart_Order(){
		var f = document.frm;
		var item_count = 0;
		var GD_EA = "";
		var gd_seq ="" ;
		if(f.GD_SEQ.value==""){
			alert("장바구니에 담긴 상품이 없습니다.");
			return;
		}else{
			for (i=0;i <= document.getElementById("Item_Count").value -1 ; ++i )
			{

				if(document.getElementById("chkidx_"+ i).checked == true)
				{
					item_count = Number(item_count) + Number(1)
					gd_seq = gd_seq+ ',' + document.getElementById("chkidx_"+ i).value
					GD_EA = GD_EA+ ',' +document.getElementById("GD_EA_"+ i).value 
				}				
			}
			gd_seq = mid(gd_seq,1,1000)
			GD_EA  = mid(GD_EA,1,1000)
			
			
			//주문페이지 이동
			location.href="../order/order.asp?Cart_YN="+encodeURI("Y")+"&GD_SEQ="+encodeURI(gd_seq)+"&ST_SEQ_A="+encodeURI(f.ST_SEQ_A.value)+"&WRAP_SEQ_A="+encodeURI(f.WRAP_SEQ_A.value)+"&GD_EA_A="+encodeURI(GD_EA)+"&Item_Count="+item_count;
			/*f.action="order.asp";
			f.submit();*/
		}	
	}	
	
	function cart_history()
	{
		if ('<%=gd_grp_mid%>' != '')
		{
			location.href='/product/category_list.asp?gd_grp_mid=<%=gd_grp_mid%>&gd_grp_detl=<%=gd_grp_detl%>'
		}
		else
		(
			location.href='/default.asp'
		)
	}
</script>
<form method="post" name="frm" action="">
	<input type="hidden" name="Cart_YN" value="Y">
	<input type="hidden" name="CART_TYPE" value="">
	<!--수량변경시 변경수량-->
	<input type="hidden" name="Change_EA" value="">
	<!--옵션변경시 변경되는 상품코드-->
	<input type="hidden" name="Change_GD_SEQ" value="">
	<!--수량옵션변경시 CART idx 값-->
	<input type="hidden" name="Cart_SEQ" value="">
	<div class="container sub">
		<div class="page_tit">
			<h2>장바구니</h2>
			<div class="order_load">
				<span class="active">장바구니</span>
				<span>주문결제</span>
				<span>주문완료</span>
			</div><!-- order_load e -->
		</div><!-- page_tit e -->
		<div class="cart_page sub_bg">
			<form method="post" action="">
			<div class="order_wrap">
				<div class="cart_menu">
					<input type="checkbox" id="AllCheck" name="AllCheck" class="chk_box" title="" value="" checked onclick="javascript:AllCheck_Click();"/>
					<label for="AllCheck" class="chk_la"></label>
					<button class="btn_del" type="button" onclick="javascript:Cart_Del_Check();">선택삭제</button>
					<!--span class="info">30,000원 이상 구매시 무료배송</span-->
				</div>

				<ul class="order_list">
					 <%
						'카트 리스트 불러오기 
						'회원번호,세션아이디,몰코드,"테이블값"
						CartRs = Cart_List(User_SEQ,Session.SessionID,GLOBAL_VAR_ONLINECD,"Cart") 
						'주문상품 번호 초기화
						ORDER_SEQ = ""
						Product_Sum = 0
						Total_Price_Sum = 0
						ST_SEQ_A = ""
						WRAP_SEQ_A = ""
						GD_EA_A    = ""
						If CartRs(0,0) <> "NODATA" Then 
							j = 0
							For i = 0 To Ubound(CartRs,2)
								'GD_SEQ,IDX,Convert(varchar(10),Regdate,102) AS RegDate,GD_EA,ST_SEQ,WRAP_SEQ
								Cart_GD_SEQ = CartRs(0,i)
								'카트번호
								Cart_SEQ = CartRs(1,i)
								'주문수량
								GD_EA    = CartRs(3,i)
								'스티커번호
								ST_SEQ   = CartRs(4,i)
								'포장지번호
								WRAP_SEQ = CartRs(5,i)

								
								'상품 상세보기 함수 Product_View(상품명,상품SEQ,가격옵션)
								Set PRs = Product_View(GLOBAL_VAR_ONLINECD,"",Cart_GD_SEQ,"")	 
					%>
						
					<li>
						<div class="chk_wrap">
							<input type="checkbox" id="chkidx_<%=j%>" name="chkidx" class="chk_box" title="" value="<%=Cart_GD_SEQ%>"  checked/>
							<label for="chkidx_<%=j%>"></label>
						</div><!-- chk_box e -->
						<div class="img_cont">
							<a href="/product/detail.asp?seq=<%=PRs("SEQ")%>&gd_grp_mid=<%=gd_grp_mid%>&gd_grp_detl=<%=gd_grp_detl%>">
								<img src="http://www.itemcenter.co.kr/<%=PRs("GD_IMG_PATH")%>" alt=""/>
							</a>
						</div><!-- img_cont e -->
						<div class="list_info">
							<div class="product_name"><%=hleft(PRs("GD_NM"),50)%></div>
							<div class="amount_wrap">
								<div class="amount_cont">
									<button class="minus" type="button" onclick="javascript:minus('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>','<%=j%>');"><span></span></button>
									<label for="" class="hide">수량</label>
									<input type="text" class="input_amount" id="GD_EA_<%=i%>" name="GD_EA" title="" value="<%=GD_EA%>" readonly/>
									<button class="plus" type="button" onclick="javascript:plus('<%=PRs("SEQ")%>','<%=PRs("GD_MIN_VOL")%>','<%=j%>');">+</button>
								</div><br class="sbr"/>
								<div class="option">
									<% If PRs("GD_OPTION1") = "" And PRs("GD_OPTION2") <> "" Then %>
										옵션 : <%=PRs("GD_OPTION2")%>
									<% ElseIf PRs("GD_OPTION1") <> "" And  PRs("GD_OPTION2") = ""  Then %>
										옵션 : <%=PRs("GD_OPTION1")%>
									<% ElseIf PRs("GD_OPTION1") <> "" And  PRs("GD_OPTION2") <> ""  Then %>
										옵션 : <%=PRs("GD_OPTION1")%> / <%=PRs("GD_OPTION2")%>
									<% End If %>

								</div>
							</div>
						</div><!-- list_info e -->
						<%
							 Product_Sum = CDbl(GD_EA)*CDbl(PRs("GD_Price3"))
						%>
						<input type="hidden" id="GD_Price3_<%=j%>" value="<%=PRs("GD_Price3")%>">
						<input type="hidden" name="Product_Price" value="<%=Product_Sum%>">
						<input type="hidden" name="ST_SEQ" value="<%=ST_SEQ%>">
						<input type="hidden" name="WRAP_SEQ" value="<%=WRAP_SEQ%>">
						<div class="price">
							<strike><%=FormatNumber(PRs("GD_PRICE8"),0) %>원</strike><br/><b id="product_sum_<%=j%>"><%= FormatNumber(Product_Sum,0)%></b>원
							<!--strike>0원</strike><br/><b id="product_sum_<%=j%>">0</b>원-->
						</div>
					</li><!-- line 1 e -->
					<%			
								'기본 전체선택 GD_SEQ hidden값에 상품 코드 
								If i <> Ubound(CartRs,2) Then 
									GD_SEQ     = GD_SEQ&PRs("SEQ")&","
									ST_SEQ_A   = ST_SEQ_A&ST_SEQ&","
									WRAP_SEQ_A = WRAP_SEQ_A&WRAP_SEQ&","
									GD_EA_A    =  GD_EA_A&GD_EA&","
								Else 
									GD_SEQ     = GD_SEQ&PRs("SEQ")
									ST_SEQ_A   = ST_SEQ_A&ST_SEQ
									WRAP_SEQ_A = WRAP_SEQ_A&WRAP_SEQ
									GD_EA_A    =  GD_EA_A&GD_EA
								End If 
								j = j + 1
								'상품의 합계
								Total_Price = Total_Price + Product_Sum
							Next

							'배송료 계산
							If Total_Price < 30000 Then 
							  Delivery_Price = 2500
							  Delivery_YN    = "Y"
							Else
							  Delivery_Price = 0
							  Delivery_YN    = "N"
							End If 

							'상품과 배송료 합계
							Total_Price_Sum = Total_Price + Delivery_Price
					else
				%>
				<li class="cont_none">
					장바구니에 상품이 존재하지 않습니다.	
				</li><!-- line 1 e -->
				<%
					End If
				%>
				<!--상품값 배열 처리-->
				<input type="hidden" name="GD_SEQ" id="GD_SEQ" value="<%=GD_SEQ%>">
				<!--장바구니에 담긴 아이템 갯수-->
				<input type="hidden" name="Item_Count" id="Item_Count" value="<%=j%>">
				<!--스티커SEQ배열-->
				<input type="hidden" name="ST_SEQ_A" value="<%=ST_SEQ_A%>">
				<!--포장지SEQ배열-->
				<input type="hidden" name="WRAP_SEQ_A" value="<%=WRAP_SEQ_A%>">
				<!--장바구니에 담긴 아이템 갯수-->
				<!--주문수량-->
				<input type="hidden" name="GD_EA_A" value="<%=GD_EA_A%>">
				<!-- 주문상품정보  끝 _상품루프돌릴부분-->
				</ul><!-- cart_list e -->
				<div class="total_wrap">
					<div class="total">
						<dl class="product_price">
							<dt>상품금액</dt>
							<dd><b id="Total_Price_div"><%=FormatNumber(Total_Price,0)%></b>원</dd>
						</dl>
						<dl class="delivery_price">
							<dt>배송비</dt>
							<dd><b id="delivery_price_div"><%=FormatNumber(Delivery_Price,0)%></b>원</dd>
						</dl>
						<dl class="total_price">
							<dt>총 결제금액</dt>
							<dd><b id="last_price_div"><%=FormatNumber(Total_Price_Sum,0)%></b>원</dd>
						</dl>
					</div>
				</div><!-- total_wrap e -->
				<div class="btn_wrap">
					<a href="javascript:cart_history();" class="btn btn_b">쇼핑계속하기</a>
					<a href="javascript:Cart_Order();" class="btn btn_b btn_agree">구매하기</a>
				</div><!-- btn_wrap e -->
			</div><!-- order_wrap e -->
			</form>
		</div><!-- cart_page e -->
	</div><!-- container e -->
</form>
<!-- #include virtual = "include/footer.asp" -->
